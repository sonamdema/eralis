package bt.gov.rsta.framework.vo;

import java.io.Serializable;
import java.util.List;

public class EmailModelVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String emailSlNo;
	private String moduleType;
	private String templateModule;
	private String templateSerialNumber;
	private String mailType;
	private List<String> recipentList;
	private String mailBody;
	private String mailSubject;
	private String emailId;
	private String userName;
	private String loginId;
	private String password;
	private String customerName;
	private String vehicleNo;
	private String renewalDueDate;
	private String licenseNo;
	private String topNo;
	private String requestType;

	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	private String applicationNo;
	private String approvalDate;
	
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getTopNo() {
		return topNo;
	}
	public void setTopNo(String topNo) {
		this.topNo = topNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getRenewalDueDate() {
		return renewalDueDate;
	}
	public void setRenewalDueDate(String renewalDueDate) {
		this.renewalDueDate = renewalDueDate;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getEmailSlNo() {
		return emailSlNo;
	}
	public void setEmailSlNo(String emailSlNo) {
		this.emailSlNo = emailSlNo;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getTemplateModule() {
		return templateModule;
	}
	public void setTemplateModule(String templateModule) {
		this.templateModule = templateModule;
	}
	public String getTemplateSerialNumber() {
		return templateSerialNumber;
	}
	public void setTemplateSerialNumber(String templateSerialNumber) {
		this.templateSerialNumber = templateSerialNumber;
	}
	public String getMailType() {
		return mailType;
	}
	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	public List<String> getRecipentList() {
		return recipentList;
	}
	public void setRecipentList(List<String> recipentList) {
		this.recipentList = recipentList;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
