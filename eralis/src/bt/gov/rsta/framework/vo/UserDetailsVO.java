package bt.gov.rsta.framework.vo;

import java.io.Serializable;

public class UserDetailsVO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String serviceId;
	private String actorId;
	private String actorName;
	private String roleId;
	private String roleName;
	private String assignedGroupId;
	private String assignedUserId;
	private String taskStateId;
	private String taskRemarks;
	private String assignedPrivId;
	private String roleCode;
	private String userCode;
	private String jurisdictionId;
	private String jurisdictionTypeId;
	private String regionId;
	private String baseId;
	
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getBaseId() {
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getActorId() {
		return actorId;
	}
	public void setActorId(String actorId) {
		this.actorId = actorId;
	}
	public String getActorName() {
		return actorName;
	}
	public void setActorName(String actorName) {
		this.actorName = actorName;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getAssignedGroupId() {
		return assignedGroupId;
	}
	public void setAssignedGroupId(String assignedGroupId) {
		this.assignedGroupId = assignedGroupId;
	}
	public String getAssignedUserId() {
		return assignedUserId;
	}
	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}
	public String getTaskStateId() {
		return taskStateId;
	}
	public void setTaskStateId(String taskStateId) {
		this.taskStateId = taskStateId;
	}
	public String getTaskRemarks() {
		return taskRemarks;
	}
	public void setTaskRemarks(String taskRemarks) {
		this.taskRemarks = taskRemarks;
	}
	public String getAssignedPrivId() {
		return assignedPrivId;
	}
	public void setAssignedPrivId(String assignedPrivId) {
		this.assignedPrivId = assignedPrivId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getJurisdictionId() {
		return jurisdictionId;
	}
	public void setJurisdictionId(String jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}
	public String getJurisdictionTypeId() {
		return jurisdictionTypeId;
	}
	public void setJurisdictionTypeId(String jurisdictionTypeId) {
		this.jurisdictionTypeId = jurisdictionTypeId;
	}
}
