package bt.gov.rsta.framework.vo;

import java.io.Serializable;

public class WorkflowDetailsVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String applicationNo;
    private String serviceId;
    private String actorId;
    private String actorName;
    private String roleId;
    private String roleName;
    private String assignedGroupId;
    private String assignedUserId;
    private String taskStateId;
    private String taskRemarks;
    private String taskInstanceId;
    private String seqDetailsId;
    private String taskId;
    private String Assigned_Priv_Id;
    private String cid_number;
    private String jurisId;
    private String jurisTypeId;
    
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getActorId() {
		return actorId;
	}
	public void setActorId(String actorId) {
		this.actorId = actorId;
	}
	public String getActorName() {
		return actorName;
	}
	public void setActorName(String actorName) {
		this.actorName = actorName;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getAssignedGroupId() {
		return assignedGroupId;
	}
	public void setAssignedGroupId(String assignedGroupId) {
		this.assignedGroupId = assignedGroupId;
	}
	public String getAssignedUserId() {
		return assignedUserId;
	}
	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}
	public String getTaskStateId() {
		return taskStateId;
	}
	public void setTaskStateId(String taskStateId) {
		this.taskStateId = taskStateId;
	}
	public String getTaskRemarks() {
		return taskRemarks;
	}
	public void setTaskRemarks(String taskRemarks) {
		this.taskRemarks = taskRemarks;
	}
	public String getTaskInstanceId() {
		return taskInstanceId;
	}
	public void setTaskInstanceId(String taskInstanceId) {
		this.taskInstanceId = taskInstanceId;
	}
	public String getSeqDetailsId() {
		return seqDetailsId;
	}
	public void setSeqDetailsId(String seqDetailsId) {
		this.seqDetailsId = seqDetailsId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getAssigned_Priv_Id() {
		return Assigned_Priv_Id;
	}
	public void setAssigned_Priv_Id(String assigned_Priv_Id) {
		Assigned_Priv_Id = assigned_Priv_Id;
	}
	public String getCid_number() {
		return cid_number;
	}
	public void setCid_number(String cid_number) {
		this.cid_number = cid_number;
	}
	public String getJurisId() {
		return jurisId;
	}
	public void setJurisId(String jurisId) {
		this.jurisId = jurisId;
	}
	public String getJurisTypeId() {
		return jurisTypeId;
	}
	public void setJurisTypeId(String jurisTypeId) {
		this.jurisTypeId = jurisTypeId;
	}
}
