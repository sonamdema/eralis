package bt.gov.rsta.framework.vo;

import java.io.Serializable;

public class DocumentVO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String serviceName;
	private String uploadURL;
	private String uuid;
	private byte[] fileContent;
	private String documentType;
	
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUploadURL() {
		return uploadURL;
	}
	public void setUploadURL(String uploadURL) {
		this.uploadURL = uploadURL;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public byte[] getFileContent() {
		return fileContent;
	}
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
}
