package bt.gov.rsta.framework.vo;

import java.io.Serializable;
import java.util.List;

public class MenuVO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String menuId;
	private String menuName;
	private String menuIcon;
	private List<SubMenuVO> subMenuList;
	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuIcon() {
		return menuIcon;
	}
	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}
	public List<SubMenuVO> getSubMenuList() {
		return subMenuList;
	}
	public void setSubMenuList(List<SubMenuVO> subMenuList) {
		this.subMenuList = subMenuList;
	}
}
