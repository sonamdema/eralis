package bt.gov.rsta.framework.vo;

import java.io.Serializable;
import java.util.List;

public class SubMenuVO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String menuId;
	private String menuName;
	private String menuLink;
	private List<ThirdLevelMenuVo> thirdLvlMenuList;
	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuLink() {
		return menuLink;
	}
	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}
	public List<ThirdLevelMenuVo> getThirdLvlMenuList() {
		return thirdLvlMenuList;
	}
	public void setThirdLvlMenuList(List<ThirdLevelMenuVo> thirdLvlMenuList) {
		this.thirdLvlMenuList = thirdLvlMenuList;
	}
}
