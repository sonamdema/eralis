package bt.gov.rsta.framework.vo;

import java.io.Serializable;

public class ApplicationDataVO implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String uid;
	private String cpassword;
	private String npassword;
	private String securityQuestionId;
	private String securityAnswer;
	private String errorCode;
	private String errorDetails;
	private String status;
	private String name;
	private String cidNumber;
	private String customerId;
	private String region;
	private String dzongkhag;
	private String type;
	private String ownerType;
	private String vehicleType;
	private String vehicleNumber;
	private String ownerName;
	private String engineNumber;
	private String chasisNumber;
	private String ownerID;
	private String Phone;
	private String drivername;
	private String permitno;
	private String licenseno;
	private String licensetype;
	private String applicationNumber;
	private String fullName;
	private String dob;
	private String gender;
	private String houseNumber;
	private String thramNumber;
	private String permVillageId;
	private String permVillage;
	private String permGewogId;
	private String permDzongkhagId;
	private String fatherName;
	private String prevApplyFlag;
	private String driveType;
	private String theoryMarksObtained;
	private String formType;
	private String ministryId;
	private String departmentId;
	private String email;
	private String address;
	private String remarks;
	private String make;
	private String model;
	private String manufacturerYear;
	private String mobileNumber;
	private String vehicleId;
	private String learnerLicenseId;
	private String drivingLicenseId;
	private String maxApplicants;
	private String applicants;
	private String organizationName;
	private String message;
	private String flag;
	private String isTest;
	private String testStatus;
	private String vehicleModel;
	private String vehicleCompany;
	private String country;
	
	
	
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getVehicleCompany() {
		return vehicleCompany;
	}
	public void setVehicleCompany(String vehicleCompany) {
		this.vehicleCompany = vehicleCompany;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getTestStatus() {
		return testStatus;
	}
	public void setTestStatus(String testStatus) {
		this.testStatus = testStatus;
	}
	public String getIsTest() {
		return isTest;
	}
	public void setIsTest(String isTest) {
		this.isTest = isTest;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getMaxApplicants() {
		return maxApplicants;
	}
	public void setMaxApplicants(String maxApplicants) {
		this.maxApplicants = maxApplicants;
	}
	public String getApplicants() {
		return applicants;
	}
	public void setApplicants(String applicants) {
		this.applicants = applicants;
	}
	public String getDrivingLicenseId() {
		return drivingLicenseId;
	}
	public void setDrivingLicenseId(String drivingLicenseId) {
		this.drivingLicenseId = drivingLicenseId;
	}
	public String getLearnerLicenseId() {
		return learnerLicenseId;
	}
	public void setLearnerLicenseId(String learnerLicenseId) {
		this.learnerLicenseId = learnerLicenseId;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getMinistryId() {
		return ministryId;
	}
	public void setMinistryId(String ministryId) {
		this.ministryId = ministryId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getManufacturerYear() {
		return manufacturerYear;
	}
	public void setManufacturerYear(String manufacturerYear) {
		this.manufacturerYear = manufacturerYear;
	}
	public String getDriveType() {
		return driveType;
	}
	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}
	public String getTheoryMarksObtained() {
		return theoryMarksObtained;
	}
	public void setTheoryMarksObtained(String theoryMarksObtained) {
		this.theoryMarksObtained = theoryMarksObtained;
	}
	
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getPrevApplyFlag() {
		return prevApplyFlag;
	}
	public void setPrevApplyFlag(String prevApplyFlag) {
		this.prevApplyFlag = prevApplyFlag;
	}
	public String getPermVillage() {
		return permVillage;
	}
	public void setPermVillage(String permVillage) {
		this.permVillage = permVillage;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getThramNumber() {
		return thramNumber;
	}
	public void setThramNumber(String thramNumber) {
		this.thramNumber = thramNumber;
	}
	public String getPermVillageId() {
		return permVillageId;
	}
	public void setPermVillageId(String permVillageId) {
		this.permVillageId = permVillageId;
	}
	public String getPermGewogId() {
		return permGewogId;
	}
	public void setPermGewogId(String permGewogId) {
		this.permGewogId = permGewogId;
	}
	public String getPermDzongkhagId() {
		return permDzongkhagId;
	}
	public void setPermDzongkhagId(String permDzongkhagId) {
		this.permDzongkhagId = permDzongkhagId;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public String getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(String licensetype) {
		this.licensetype = licensetype;
	}
	public String getLicenseno() {
		return licenseno;
	}
	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}
	public String getPermitno() {
		return permitno;
	}
	public void setPermitno(String permitno) {
		this.permitno = permitno;
	}
	public String getDrivername() {
		return drivername;
	}
	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}
	public String getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	public String getChasisNumber() {
		return chasisNumber;
	}
	public void setChasisNumber(String chasisNumber) {
		this.chasisNumber = chasisNumber;
	}
	public String getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCidNumber() {
		return cidNumber;
	}
	public void setCidNumber(String cidNumber) {
		this.cidNumber = cidNumber;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getCpassword() {
		return cpassword;
	}
	public void setCpassword(String cpassword) {
		this.cpassword = cpassword;
	}
	public String getNpassword() {
		return npassword;
	}
	public void setNpassword(String npassword) {
		this.npassword = npassword;
	}
	public String getSecurityQuestionId() {
		return securityQuestionId;
	}
	public void setSecurityQuestionId(String securityQuestionId) {
		this.securityQuestionId = securityQuestionId;
	}
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDetails() {
		return errorDetails;
	}
	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
