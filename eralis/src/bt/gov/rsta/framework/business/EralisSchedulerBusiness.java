package bt.gov.rsta.framework.business;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.framework.dao.EralisSchedulerDAO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EmailUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.SMSUtil;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class EralisSchedulerBusiness 
{
	private static EralisSchedulerBusiness schedulerBusiness = null;
	
	public static EralisSchedulerBusiness getInstance()
	{
		if(schedulerBusiness == null)
			schedulerBusiness = new EralisSchedulerBusiness();
		
		return schedulerBusiness;
	}
	
	public void sendVehicleRenewalNotification() throws ERALISException, ERALISSystemException
	{
		try 
		{
			List<NotificationDTO> notificationList =  EralisSchedulerDAO.getInstance().sendVehicleRenewalNotification();
			List<EmailModelVO> toBeSentEmailList = new ArrayList<EmailModelVO>();
			List<SMSModelVO> toBeSentSMSList = new ArrayList<SMSModelVO>();
			List<String> receipentList = new ArrayList<String>();
			
			if(notificationList.size() > 0)
			{
				for (NotificationDTO dto : notificationList) 
				{
					//setting email properties
					 EmailModelVO mailVO = new EmailModelVO();
					 mailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_RENEWAL_REMINDER);
					 mailVO.setCustomerName(dto.getCustomerName());
					 mailVO.setVehicleNo(dto.getVehicleNo());
					 mailVO.setRenewalDueDate(dto.getRenewalDueDate());
					 receipentList.add(dto.getEmailAddress());
					 mailVO.setRecipentList(receipentList);
					 toBeSentEmailList.add(mailVO);
					 
					 //setting sms properties
					 SMSModelVO smsVO = new SMSModelVO();
					 smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_RENEWAL_REMINDER);
					 smsVO.setCustomerName(dto.getCustomerName());
					 smsVO.setVehicleNo(dto.getVehicleNo());
					 smsVO.setRenewalDueDate(dto.getRenewalDueDate());
					 smsVO.setRecipentList(receipentList);
					 toBeSentSMSList.add(smsVO);
				}
				
				sendScheduleEmail(toBeSentEmailList);
				sendScheduleSMS(toBeSentSMSList);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendVehicleRenewalNotification]:"+e);
		}
	}
	
	public void sendLicenseRenewalNotification() throws ERALISException, ERALISSystemException
	{
		try 
		{
			List<NotificationDTO> notificationList =  EralisSchedulerDAO.getInstance().sendLicenseRenewalNotification();
			
			List<EmailModelVO> toBeSentEmailList = new ArrayList<EmailModelVO>();
			List<SMSModelVO> toBeSentSMSList = new ArrayList<SMSModelVO>();
			List<String> receipentList = new ArrayList<String>();
			
			if(notificationList.size() > 0)
			{
				for (NotificationDTO dto : notificationList) 
				{
					//setting email properties
					 EmailModelVO mailVO = new EmailModelVO();
					 mailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_LICENSE_RENEWAL_REMINDER);
					 mailVO.setCustomerName(dto.getCustomerName());
					 mailVO.setVehicleNo(dto.getVehicleNo());
					 mailVO.setRenewalDueDate(dto.getRenewalDueDate());
					 receipentList.add(dto.getEmailAddress());
					 mailVO.setRecipentList(receipentList);
					 toBeSentEmailList.add(mailVO);
					 
					 //setting sms properties
					 SMSModelVO smsVO = new SMSModelVO();
					 smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_LICENSE_RENEWAL_REMINDER);
					 smsVO.setCustomerName(dto.getCustomerName());
					 smsVO.setVehicleNo(dto.getVehicleNo());
					 smsVO.setRenewalDueDate(dto.getRenewalDueDate());
					 smsVO.setRecipentList(receipentList);
					 toBeSentSMSList.add(smsVO);
				}
				
				sendScheduleEmail(toBeSentEmailList);
				sendScheduleSMS(toBeSentSMSList);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendLicenseRenewalNotification]:"+e);
		}
	}
	
	public void sendTOPRenewalNotification() throws ERALISException, ERALISSystemException
	{
		try 
		{
			List<NotificationDTO> notificationList =  EralisSchedulerDAO.getInstance().sendTOPRenewalNotification();
			
			List<EmailModelVO> toBeSentEmailList = new ArrayList<EmailModelVO>();
			List<SMSModelVO> toBeSentSMSList = new ArrayList<SMSModelVO>();
			List<String> receipentList = new ArrayList<String>();
			
			if(notificationList.size() > 0)
			{
				for (NotificationDTO dto : notificationList)   
				{
					//setting email properties
					 EmailModelVO mailVO = new EmailModelVO();
					 mailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_TOP_RENEWAL_REMINDER);
					 mailVO.setCustomerName(dto.getCustomerName());
					 mailVO.setTopNo(dto.getTopNo());
					 mailVO.setRenewalDueDate(dto.getRenewalDueDate());
					 receipentList.add(dto.getEmailAddress());
					 mailVO.setRecipentList(receipentList);
					 toBeSentEmailList.add(mailVO);
					 
					 //setting sms properties
					 SMSModelVO smsVO = new SMSModelVO();
					 smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_TOP_RENEWAL_REMINDER);
					 smsVO.setCustomerName(dto.getCustomerName());
					 mailVO.setTopNo(dto.getTopNo());
					 smsVO.setRenewalDueDate(dto.getRenewalDueDate());
					 smsVO.setRecipentList(receipentList);
					 toBeSentSMSList.add(smsVO);
				}
				
				sendScheduleEmail(toBeSentEmailList);
				sendScheduleSMS(toBeSentSMSList);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendLicenseRenewalNotification]:"+e);
		}
	}
	
	public void revokeSuspensionPeriod() throws ERALISException, ERALISSystemException
	{
		EralisSchedulerDAO.getInstance().revokeSuspensionPeriod();
	}
	
	public void dispatchLicenseApplication() throws ERALISException, ERALISSystemException
	{
		EralisSchedulerDAO.getInstance().dispatchLicenseApplication();
	}
	
	public void sendVehicleOutstandingJob() throws ERALISException, ERALISSystemException
	{
		try 
		{
			List<NotificationDTO> notificationList =  EralisSchedulerDAO.getInstance().sendVehicleOutstandingJob();
			
			List<EmailModelVO> toBeSentEmailList = new ArrayList<EmailModelVO>();
			List<SMSModelVO> toBeSentSMSList = new ArrayList<SMSModelVO>();
			List<String> receipentList = new ArrayList<String>();
			
			if(notificationList.size() > 0)
			{
				for (NotificationDTO dto : notificationList)   
				{
					//setting email properties
					 EmailModelVO mailVO = new EmailModelVO();
					 mailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_VEHICLE_OUTSTANDING_NOTIFICATION);
					 mailVO.setCustomerName(dto.getCustomerName());
					 mailVO.setVehicleNo(dto.getVehicleNo());
					 mailVO.setRenewalDueDate(dto.getRenewalDueDate());
					 receipentList.add(dto.getEmailAddress());
					 mailVO.setRecipentList(receipentList);
					 toBeSentEmailList.add(mailVO);
					 
					 //setting sms properties
					 SMSModelVO smsVO = new SMSModelVO();
					 smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_VEHICLE_OUTSTANDING_NOTIFICATION);
					 smsVO.setCustomerName(dto.getCustomerName());
					 mailVO.setVehicleNo(dto.getVehicleNo());
					 smsVO.setRenewalDueDate(dto.getRenewalDueDate());
					 receipentList.add(dto.getMobileNumber());
					 smsVO.setRecipentList(receipentList);
					 toBeSentSMSList.add(smsVO);
				}
				
				sendScheduleEmail(toBeSentEmailList);
				sendScheduleSMS(toBeSentSMSList);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendLicenseRenewalNotification]:"+e);
		}
	}
	
	public void cancelOutstandingVehicleJob() throws ERALISException, ERALISSystemException
	{
		EralisSchedulerDAO.getInstance().cancelOutstandingVehicleJob();
	}
	
	public static void sendScheduleEmail(List<EmailModelVO> toBeSentEmailList) {
		Log.info("Enter EmailUtil.sendScheduleEmail():");
		Connection conn = null;
		conn = ConnectionManager.getConnection();
		
		try 
		{
			if (null != toBeSentEmailList && toBeSentEmailList.size() > 0) 
			{
				for (EmailModelVO mailVO : toBeSentEmailList) 
				{
					EmailUtil.prepareEmailObject(mailVO, conn);
					EmailUtil.sendScheduleMail(mailVO);
				}
			}
			else
			{
				Log.info("At EmailUtil.sendScheduleEmail(): There are no email id to be sent. ");
			}
		} 
		catch (Exception e) 
		{
			Log.error("At EmailUtil.sendScheduleEmail(): exception is -> "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		Log.info("Exit EmailUtil.sendScheduleEmail():");
	}
	
	public static void sendScheduleSMS(List<SMSModelVO> toBeSentSMSList) {
		Log.info("Enter EmailUtil.sendScheduleEmail():");
		Connection conn = null;
		conn = ConnectionManager.getConnection();
		
		try 
		{
			if (null != toBeSentSMSList && toBeSentSMSList.size() > 0) 
			{
				for (SMSModelVO smsVO : toBeSentSMSList) 
				{
					SMSUtil.prepareSMSObject(smsVO, conn);
					SMSUtil.sendScheduleSMS(smsVO);
				}
			}
			else
			{
				Log.info("At EmailUtil.sendScheduleEmail(): There are no mobile number to be sent. ");
			}
		} 
		catch (Exception e) 
		{
			Log.error("At EmailUtil.sendScheduleEmail(): exception is -> "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		Log.info("Exit EmailUtil.sendScheduleEmail():");
	}
}
