package bt.gov.rsta.framework.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletResponse;

import bt.gov.dcrc.ws.business.InvokeCitizenDetailsWebService;
import bt.gov.dcrc.ws.dto.CitizenDetails;
import bt.gov.rsta.eralis.business.administration.AdministrationBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dao.license.LicenseDAO;
import bt.gov.rsta.eralis.dto.administration.UserDTO;
import bt.gov.rsta.eralis.dto.common.DailyTaskUserDTO;
import bt.gov.rsta.eralis.dto.common.PrintDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.faq.FaqDTO;
import bt.gov.rsta.eralis.dto.license.DriveTypeDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.master.MasterDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusTrailDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.dto.token.TokenCounterDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dao.EmailDAO;
import bt.gov.rsta.framework.dao.PopulateDropDownDAO;
import bt.gov.rsta.framework.dao.ServiceDAO;
import bt.gov.rsta.framework.dto.AlertDTO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EmailConfigDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.dto.PagePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.ldapconfig.LdapPasswordUpdate;
import bt.gov.rsta.framework.ldapconfig.LdapUserDetails;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.vo.MenuVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.actionform.service.serviceActionForm;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class CommonBusiness {
	private static CommonBusiness commonBusiness;

	public static CommonBusiness getInstance() {
		if (commonBusiness == null) {
			commonBusiness = new CommonBusiness();
		}

		return commonBusiness;
	}

	public String insertErrorLog(String errorDetails, String owner) {
		return CommonDAO.getInstance().insertErrorLog(errorDetails, owner);
	}
	
	public PaymentDTO calculatePayableAmount(PaymentDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().calculatePayableAmount(dto);
	}
	
	public List<LicenseDTO> getTOPSearchList(LicenseDTO dto,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTOPSearchList(dto,searchType);
	}
	
	public String dispatch(String applicationNo, String requestType, String serviceType, UserDetailsVO vo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = "FAILURE";
		NotificationDTO dto = new NotificationDTO();
		try
		{
			conn = ConnectionManager.getConnection();
		
			if(conn != null)
			{
				conn.setAutoCommit(false);
				dto = CommonDAO.getInstance().dispatch(applicationNo, requestType, serviceType, vo);
				
				if(dto.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(dto.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_DISPATCH_NOTIFICATION);
						
						if(dto.getRequestType().equalsIgnoreCase("VEHICLE"))
							emailVO.setRequestType("Vehicle Registration Certificate");
						else if(dto.getRequestType().equalsIgnoreCase("LICENSE"))
							emailVO.setRequestType("Driving License");
						else if(dto.getRequestType().equalsIgnoreCase("LEARNER"))
							emailVO.setRequestType("Learner License");
						else if(dto.getRequestType().equalsIgnoreCase("TOP") || dto.getRequestType().equalsIgnoreCase("TOPR"))
							emailVO.setRequestType("Taxi Operating Permit");
						
						VehicleBusiness.getInstance().sendMailToUser(emailVO, dto);
					}
					
					if(dto.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_DISPATCH_NOTIFICATION);
						
						if(dto.getRequestType().equalsIgnoreCase("VEHICLE"))
							smsVO.setRequestType("Vehicle Registration Certificate");
						else if(dto.getRequestType().equalsIgnoreCase("LICENSE"))
							smsVO.setRequestType("Driving License");
						else if(dto.getRequestType().equalsIgnoreCase("LEARNER"))
							smsVO.setRequestType("Learner License");
						else if(dto.getRequestType().equalsIgnoreCase("TOP") || dto.getRequestType().equalsIgnoreCase("TOPR"))
							smsVO.setRequestType("Taxi Operating Permit");
						
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, dto);
					}
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "FAILURE";
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn);
			result = dto.getStatus();
		}
		
		return result;
	}
	
	public ArrayList<DocumentVO> getUploadedFilesByApplicationNo(String applicationNo, String serviceName) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getUploadedFilesByApplicationNo(applicationNo, serviceName);
	}
	
	public String changeUserAccountInfo(String identifier, ApplicationDataVO appVO) throws ERALISException, ERALISSystemException, SQLException
	{
		return CommonDAO.getInstance().changeUserAccountInfo(identifier, appVO);
	}
	
	public byte[] downloadFile(String uuid, String fileName, HttpServletResponse response) throws Exception 
	{
		return CommonDAO.getInstance().downloadFile(uuid, fileName, response);
    } 

	public EralisUserRolePriviledge populateUserRolePriviledgeHierarchy(
			EralisUserRolePriviledge userRolePriv) throws ERALISException, 
			ERALISSystemException { 
		try {
 
			//boolean isPresent = true;
			boolean isPresent = LdapUserDetails.authenticateUserFromLDAP(userRolePriv.getUserId(), userRolePriv.getPassword());

			if (isPresent) { 
				userRolePriv = CommonDAO.getInstance().populateUserRolePriviledgeHierarchy(userRolePriv);
			}
		} catch (Exception e) {  
			throw new ERALISException(
					"### Error at CommonBusiness[populateUserRolePriviledgeHierarchy]: exception:: "
							+ e);
		}
		return userRolePriv;
	}

	public String getLastLoginDate(String userId) throws ERALISException,
			ERALISSystemException {
		return CommonDAO.getInstance().getLastLoginDate(userId);
	}

	public void updateLoginDate(String uid) throws ERALISException,
			ERALISSystemException {
		CommonDAO.getInstance().updateLoginDate(uid);
	}

	public String updateFirstLoginDtls(ApplicationDataVO appVO)
			throws ERALISException, ERALISSystemException, SQLException {
		Log.info("### Entering CommonBusiness[updateFirstLoginDtls]");
		String result = null;
		try {
			result = LdapPasswordUpdate.updateUserPassword(appVO.getUid(),
					appVO.getNpassword(), appVO.getCpassword());

			if (result == "UPDATED")
				result = CommonDAO.getInstance().updateFirstLoginDtls(appVO);
		} catch (Exception e) {
			throw new ERALISException();
		}
		return result;
	}

	public List<DropDownDTO> getDropDownList(String fieldConstructor,String parentId) throws ERALISException, ERALISSystemException {
		PopulateDropDownDAO populateDropDownDAO = PopulateDropDownDAO.getInstance();
		List<DropDownDTO> list;
		list = populateDropDownDAO.getDropDownList(fieldConstructor, parentId);
		return list;
	}
	

	public List<TokenCounterDTO> getCounterList(String jurisTypeId, String jurisId, String userId) throws ERALISSystemException, ERALISException
	{
		return CommonDAO.getInstance().getCounterList(jurisTypeId,jurisId,userId);
	}

	public List<MasterDTO> getMasterTableList(String identifier) throws ERALISSystemException, ERALISException
	{
		return CommonDAO.getInstance().getMasterTableList(identifier);
	}
	
	public EmailConfigDTO getEmailConfigDetails() throws ERALISException, ERALISSystemException
	{
		Connection conn = ConnectionManager.getConnection();
		return EmailDAO.getEmailConfigDetails(conn);
	}

	public List<MenuVO> getMenuList(String roleId) throws ERALISException,
			ERALISSystemException {
		return CommonDAO.getInstance().getMenuList(roleId);
	}

	public PagePriviledge getPagePriviledge(String roleId, String pageId)
			throws ERALISException, ERALISSystemException {
		return CommonDAO.getInstance().getPagePriviledge(roleId, pageId);
	}
	
	public UserDTO getUserDetails(String userId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getUserDetails(userId);
	}
	
	public List<FaqDTO> get_faq_list() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().get_faq_list();
	}
	
	public List<DropDownDTO> getTCBDriveTypeList(String licenseId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTCBDriveTypeList(licenseId);
	}
	
	public String resetPassword(String loginId, String questionId, String answer) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = "FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			NotificationDTO dto = CommonDAO.getInstance().resetPassword(loginId, questionId, answer, conn);
			
			if(dto.getStatus().equalsIgnoreCase("SUCCESS"))
			{
				EmailModelVO emailVO = new EmailModelVO();
				emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_USER_PASSWORD_RESET);
				AdministrationBusiness.getInstance().sendMailToUser(emailVO, dto);
				
				SMSModelVO smsVO = new SMSModelVO();
				smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_USER_PASSWORD_RESET);
				AdministrationBusiness.getInstance().sendSMSToUser(smsVO, dto);
				
				result = dto.getStatus();
			}
		} 
		catch (Exception e)
		{
			result = "FAILURE";
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		 
		return result;
	}
	public String clearPendingTask(String pendingId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().clearPendingTask(pendingId, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonBusiness[learner_and_license_resubmit]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String do_resubmit(String serviceCode,ServiceDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().do_resubmit(serviceCode,dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonBusiness[learner_and_license_resubmit]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String deleteApplication(String serviceCode,ServiceDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().deleteApplication(serviceCode,dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonBusiness[learner_and_license_resubmit]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String reject_application(ServiceDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{	
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().reject_application(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonBusiness[application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String savePendingPrint(PrintDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{	
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().savePendingPrint(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonBusiness[application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String publishReportQuery(ServiceDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{	
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().publishReportQuery(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonBusiness[application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public ApplicationDataVO getCidDetails(ApplicationDataVO applVO) throws ERALISSystemException, ERALISException
	{
		Log.info("#### Enter CommonBusiness[getCitizenDetailsByCid] for cid = " + applVO.getCidNumber());

        CitizenDetails citizenDetails = null;
        ResourceBundle bundle = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.wsEndPointURL_en_US");
        InvokeCitizenDetailsWebService invokeWS = new InvokeCitizenDetailsWebService(bundle.getString("getCitizenDetails.endPointURL"));
        citizenDetails = invokeWS.getCitizenDetails(applVO.getCidNumber());

		if (citizenDetails.getGender() != null)
			applVO.setGender(citizenDetails.getGender());
		if (citizenDetails.getDob() != null)
			applVO.setDob(citizenDetails.getDob());

		if (citizenDetails.getFirstName() != null)
			applVO.setFirstName(citizenDetails.getFirstName());
		if(citizenDetails.getMiddleName() != null)
			applVO.setMiddleName(citizenDetails.getMiddleName());
		if(citizenDetails.getLastName() != null)
			applVO.setLastName(citizenDetails.getLastName());
		if (citizenDetails.getPermDzongkhagId() != null)
			applVO.setPermDzongkhagId(citizenDetails.getPermDzongkhagId());
		if (citizenDetails.getPermGewogId() != null)
			applVO.setPermGewogId(citizenDetails.getPermGewogId());
		if (citizenDetails.getPermVillageSerialNo() != null)
			applVO.setPermVillageId(citizenDetails.getPermVillageSerialNo());

		if (citizenDetails.getPermHouseNo() != null)
			applVO.setHouseNumber(citizenDetails.getPermHouseNo());
		if (citizenDetails.getPermThramNo() != null)
			applVO.setThramNumber(citizenDetails.getPermThramNo());
		
		if(citizenDetails.getFatherName() != null)
			applVO.setFatherName(citizenDetails.getFatherName());
		if(citizenDetails.getPermvillageName() != null)
			applVO.setPermVillage(citizenDetails.getPermvillageName());

		return applVO;
	}
	
	/*public ApplicationDataVO getLearnerTestDetails(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLearnerTestDetails(vo);
	}*/


	public ApplicationDataVO getLearnerTestDetails(ApplicationDataVO vo, EralisUserRolePriviledge user) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLearnerTestDetails(vo,user);
	}


	public EralisCommonDTO getTokenDetails(String tokenId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTokenDetails(tokenId);
	}
	

	public ApplicationDataVO getVehicleAccidentDtls(String vehicleNumber) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getVehicleAccidentDtls(vehicleNumber);
	}
	
	
	public ApplicationDataVO getOwnerInformation(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOwnerInformation(vo);
	}
	
	public ApplicationDataVO getPrevApplyDetails(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPrevApplyDetails(vo);
	}
	
	public EralisCommonDTO getQuestionDtls(String questionId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getQuestionDtls(questionId);
	}
	
	public List<EralisCommonDTO> getPersonalInfoList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPersonalInfoList(vo);
	}
	
	public EralisCommonDTO getPersonalInfoDtls(String personalInfoId,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPersonalInfoDtls(personalInfoId,searchType);
	}
	
	public EralisCommonDTO getPersonalDtlsByCid(String personalInfoId,String dob) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPersonalDtlsByCid(personalInfoId,dob);
	}
	
	public EralisCommonDTO getPersonalInfoDtlsForEdit(String personalInfoId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPersonalInfoDtlsForEdit(personalInfoId);
	}
	
	public List<EralisCommonDTO> getPersonalInfoList1(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPersonalInfoList(vo);
	}
	public List<EralisCommonDTO> getPendingList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPendingList(vo);
	}
	
	//public EralisCommonDTO getPersonalInfoDtls1(String personalInfoId1) throws ERALISException, ERALISSystemException
	//{
	//	return CommonDAO.getInstance().getPersonalInfoDtls1(personalInfoId1);
	//}
	
	public List<EralisCommonDTO> getOrganisationInfoList1(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOrganisationInfoList(vo);
	}
	
	
	public ApplicationDataVO getLicenseInformation(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOwnerInformation(vo);
	}
	
	public ApplicationDataVO getLearnerInformation(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLearnerInformation(vo);
	}
	public ApplicationDataVO getLicenseInfo(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLicenseInfo(vo);
	}
	public ApplicationDataVO getcheckCID(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getcheckCID(vo);
	}
	public ApplicationDataVO getchecklicense(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getchecklicense(vo);
	}
	public ApplicationDataVO getcheckcommerciallicense(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getcheckcommerciallicense(vo);
	}
	public List<EralisCommonDTO> getOrganisationInfoList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOrganisationInfoList(vo);
	}
	public EralisCommonDTO getOrganisationInfoDtls(String organisationInfoId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOrganisationInfoDtls(organisationInfoId);
	}


	public List<EralisCommonDTO> getAccidentRecord(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getAccidentRecord(vo);
	}

	
	public List<EralisCommonDTO> searchAccidentVehicle(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().searchAccidentVehicle(vo);
	}
	
	public List<EralisCommonDTO> getRenewalInfoList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getRenewalInfoList(vo);
	}
	
	public EralisCommonDTO getRenewalInfoDtls(String renewalInfoId, String type) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getRenewalInfoDtls(renewalInfoId,type);
	}
	
	public String getFitnessId(String vehicleId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getFitnessId(vehicleId);
	}

	public String checkDoubleOffence(String offenceTypeId,String identificationNo,String identificationType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().checkDoubleOffence(offenceTypeId,identificationNo,identificationType);
	}

	public String checkDoubleTin(String tinNo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().checkDoubleTin(tinNo);
	}
	

	public String getRepeatedOffenceType() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getRepeatedOffenceType();
	}

	public EralisCommonDTO getOffenceDtls(String offenceId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOffenceDtls(offenceId);
	}
	
	public List<EralisCommonDTO> getRenewalHistoryList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getRenewalHistoryList(vo);
	}
	
	public List<EralisCommonDTO> getRoutePermitDtls(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getRoutePermitDtls(vo);
	}
	
	public List<EralisCommonDTO> fetchVehCancellationHistroy(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().fetchVehCancellationHistroy(vo);
	}
	public List<EralisCommonDTO> fetchTopDetailsHistory(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().fetchTopDetailsHistory(vo);
	}
	
	public List<EralisCommonDTO> fetchTopCancellationHistory(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().fetchTopCancellationHistory(vo);
	}
	public List<EralisCommonDTO> getDuplicationHistoryList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getDuplicationHistoryList(vo);
	}
	public List<EralisCommonDTO> getTransferHistoryList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTransferHistoryList(vo);
	}
	
	public List<EralisCommonDTO> getConversionHistoryList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getConversionHistoryList(vo);
	}
	
	public List<EralisCommonDTO> getFitnessHistoryList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getFitnessHistoryList(vo);
	}
	
	public List<EralisCommonDTO> getEmissionHistoryList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getEmissionHistoryList(vo);
	}
	
	public List<EralisCommonDTO> getOffenceHistoryList(String id, String type) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getOffenceHistoryList(id, type);
	}
	
	public List<EralisCommonDTO> getStatusList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getStatusList(vo);
	}
	
	public List<EralisCommonDTO> getPermitInfoList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPermitInfoList(vo);
	}
	
	public EralisCommonDTO getPermitInfoDtls(String permitDetailsId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getPermitInfoDtls(permitDetailsId);
	}

	public List<LicenseDTO> getLearnerLicenseInfoList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLearnerLicenseInfoList(dto);
	}
	
	public EralisCommonDTO getlearnerRenewalInfoDtls(String PersonalInfoId,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlearnerRenewalInfoDtls(PersonalInfoId,searchType);
	}
	public String bookedTestDtls(String applicationNo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().bookedTestDtls(applicationNo);
	}
	
	public List<EralisCommonDTO> getlicenserenewalList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlicenserenewalList(vo);
	}
	
	public EralisCommonDTO getlicenserenewalDtls(String drivinglicenseId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlicenserenewalDtls(drivinglicenseId);
	}
	
	public List<LicenseDTO> getlearnerlicense(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlearnerlicense(vo);
	}
	public List<LicenseDTO> getLearnerLicenseDtls(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLearnerLicenseDtls(vo);
	}
	public List<LicenseDTO> getlearnerduplicationlist(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlearnerduplicationlist(vo);
	}
	public LicenseDTO getlearnerlicenserenewalList(String learnerlicneseno) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlearnerlicenserenewalList(learnerlicneseno);
	}
	
	public List<LicenseDTO> getdrivinglicense(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getdrivinglicense(vo);
	}
	
	public List<LicenseDTO> getListOfTop(String customerId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getListOfTop(customerId);
	}
	
	public List<LicenseDTO> getTopReplacementHistory(String searchBy,String indentifer) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTopReplacementHistory(searchBy,indentifer);
	}
	
	public List<LicenseDTO> getdrivinglicenseduplication(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getdrivinglicenseduplication(vo);
	}
	public List<LicenseDTO> getsuspensionlist(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getsuspensionlist(vo);
	}
	public List<LicenseDTO> getdrivetypeSuspensionList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getdrivetypeSuspensionList(vo);
	}
	public List<LicenseDTO> getLicenseCancellationHistory(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getLicenseCancellationHistory(vo);
	}
	public List<LicenseDTO> getlicenseEndorsementList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlicenseEndorsementList(vo);
	}
	public List<LicenseDTO> license_suspension_list(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().license_suspension_list(dto);
	}

	public List<LicenseDTO> license_suspensionFOR_list(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().license_suspensionFOR_list(dto);
	}
	public List<LicenseDTO> searchVehicleRoutePermitDtls(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().searchVehicleRoutePermitDtls(dto);
	}
	

	public EralisCommonDTO getSuspensionDtls(String drivinglicenseId,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getSuspensionDtls(drivinglicenseId,searchType);
	}
	public EralisCommonDTO getpermitDetails(String permitDetailsId,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getpermitDetails(permitDetailsId,searchType);
	}
	
	public EralisCommonDTO getRoutePermitHistory(String permitDetailsId,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getpermitDetails(permitDetailsId,searchType);
	}
	
	public VehicleDTO getaccidentDetails(String vehicleNo,String vehicleType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getaccidentDetails(vehicleNo,vehicleType);
	}
	
	public EralisCommonDTO getaccidentPersonalDetails(String licenseNo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getaccidentPersonalDetails(licenseNo);
	}
	
	public List<LicenseDTO> learner_license_list(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().learner_license_list(dto);
	}
	public EralisCommonDTO getlearnerDtls(String learnerlicenseinfoid,String searchType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getlearnerDtls(learnerlicenseinfoid,searchType);
	}
	public String drivetype_list(String learnerLicenseId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().drivetype_list(learnerLicenseId);
	}
	public String drivetype_licenselist(String customerId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().drivetype_licenselist(customerId);
	}
	
	public LicenseDTO getDriveTypeTestMarks(String driveTypeId,String licenseNO,String testDate,String customerID, String issueType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getDriveTypeTestMarks(driveTypeId,licenseNO,testDate,customerID,issueType);
	}
	
	public String getStatusCode(String applicationNo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getStatusCode(applicationNo);
	}
	
	public LicenseDTO testValidation(String driveTypeId,String drivinglicenseId,String customerID,String testDate) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().testValidation(driveTypeId,drivinglicenseId,customerID,testDate);
	}
	public LicenseDTO validateCommercialCriteria(String driveTypeId,String drivinglicenseId,String customerID,String testDate) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().validateCommercialCriteria(driveTypeId,drivinglicenseId,customerID,testDate);
	}
	public LicenseDTO validateNonCommercialCriteria(String driveTypeId,String learnerLicenseId,String issueType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().validateNonCommercialCriteria(driveTypeId,learnerLicenseId,issueType);
	}
	public ApplicationDataVO organisationCustomerId(String organisationName,String organisationType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().organisationCustomerId(organisationName,organisationType);
	}
	public List<EralisCommonDTO> getTestDate(String locationId, String jurisTypeId, String issueType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTestDate(locationId, jurisTypeId,issueType);
	}
	
	public ApplicationDataVO getMaxApplicants(String testDate, String testLocation,String issueType,String learnerNo,String customerId,String testJurisTypeId,String searchBy) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getMaxApplicants(testDate, testLocation,issueType,learnerNo,customerId,testJurisTypeId,searchBy);
	}
	
	public EralisCommonDTO gettopDtls(String CID) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().gettopDtls(CID);
	}
	public String TotalDLIssued(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().TotalDLIssued(currentRole,jurisId, jurisTypeId);
	}
	public String vehicleRgistered(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().vehicleRgistered(currentRole,jurisId, jurisTypeId);
	}
	public String vehicleRenewed(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().vehicleRenewed(currentRole,jurisId, jurisTypeId);
	}
	public String licenseRenewed(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().licenseRenewed(currentRole,jurisId, jurisTypeId);
	}
	public String newlicensePrinted(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().newlicensePrinted(currentRole,jurisId, jurisTypeId);
	}
	
	public String newLearnerIssued(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().newLearnerIssued(currentRole,jurisId,jurisTypeId);
	}
	
	public String getTotalPrintings(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTotalPrintings(currentRole,jurisId, jurisTypeId);
	}
	public String totalEarning(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().totalEarning(currentRole,jurisId, jurisTypeId);
	}
	public String totalTaskCompletion(Role currentRole,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().totalTaskCompletion(currentRole,jurisId,jurisTypeId);
	}
	
	public String getTotalEmission() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTotalEmission();
	}
	
	public String getTotalPassedEmission() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTotalPassedEmission();
	}
	
	public String getTotalLicenseAlert() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTotalLicenseAlert();
	}
	
	public String getTotalApplicationAlert() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTotalApplicationAlert();
	}
	
	public List<AlertDTO> getAlertList(String identifier) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getAlertList(identifier);
	}
	
	public String getTotalFailedEmission() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTotalFailedEmission();
	}
	
	public List<EralisCommonDTO> getvehicleList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getvehicleList(vo);
	}
	
	public List<LicenseDTO> getdrivinglicenseRenewal(String licenseId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getdrivinglicenseRenewal(licenseId);
	}
	
	public List<LicenseDTO> getdrivingduplication(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getdrivingduplication(vo);
	}
	public String generateTOPNoFormat(String regionId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().generateTOPNoFormat(regionId);
	}
	
	public List<ServiceDTO> getTestLocation() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTestLocation();
	}
	
	public List<ServiceDTO> getTestLocationForAdmin() throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getTestLocationForAdmin();
	}
	
	public EralisCommonDTO getDriveTypeTestMarks(String drivinglicenseId) throws ERALISException, ERALISSystemException
	{
		String searchType	=	"";
		return CommonDAO.getInstance().getSuspensionDtls(drivinglicenseId,searchType);
	}
	
	public List<DriveTypeDTO> get_learner_drive_type_for_resubmit(String applicationNo,String serviceCode) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().get_learner_drive_type_for_resubmit(applicationNo,serviceCode);
	}
	public List<DriveTypeDTO> get_customer_drive_type(String conditionValue,String driveTypeApplicationTable,String whereClause) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().get_customer_drive_type(conditionValue,driveTypeApplicationTable,whereClause);
	}
	
	public List<DriveTypeDTO> get_customer_drive_type1(String conditionValue) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().get_customer_drive_type1(conditionValue);
	}

	public List<LicenseDTO> searchCustomerOffence(LicenseDTO dto,String offenceType) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().searchCustomerOffence(dto,offenceType);
	}
	public List<LicenseDTO> get_offence_list(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().get_offence_list(dto);
	}
	
	
	public String daily_task(String startDate, String endDate, String requestType, String reportType, EralisUserRolePriviledge userRolePriv) throws ERALISException, ERALISSystemException
	{
		List<DailyTaskUserDTO> userList = new ArrayList<DailyTaskUserDTO>();
		DailyTaskUserDTO dto = new DailyTaskUserDTO();
		String reportList = "";
		String jurisId = userRolePriv.getJurisdictionId();
		String jurisTypeId = userRolePriv.getJurisdictionTypeId();
		String regionName = userRolePriv.getRegionName();
		
		try 
		{
			if(!requestType.equalsIgnoreCase("INDIVIDUAL")) {
				userList = CommonDAO.getInstance().getUserList(jurisId, jurisTypeId);
			}
			else if(requestType.equalsIgnoreCase("INDIVIDUAL")) {
				dto.setEmployeeName(userRolePriv.getUserName());
				dto.setRegionName(regionName);
				dto.setUserId(userRolePriv.getUserId());
				dto.setJurisId(jurisId);
				dto.setJurisTypeId(jurisTypeId);
				userList.add(dto);
			}
			
			reportList = CommonDAO.getInstance().daily_task(userList, startDate, endDate, requestType, reportType);
		} 
		catch (Exception e) 
		{
			Log.error("######## Error in CommonBusiness[daily_task] :", e);
			throw new ERALISException(e);
		}
		
		return reportList;
	}
	
	public List<PrintDTO> print_task(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().print_task(jurisId, jurisTypeId);
	}
	
	public String checkIfExists(String licenseNo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().checkIfExists(licenseNo);
	}
	
	public List<ApplicationStatusTrailDTO> getApplicationStatus(String requestType, String serviceType, String identityNo) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getApplicationStatus(requestType, serviceType, identityNo);
	}
	public String getCustomerDtls(String cid) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().getCustomerDtls(cid);
	}
	
	public String validateOTP(String cid, String otp) throws ERALISException, ERALISSystemException
	{
		return CommonDAO.getInstance().validateOTP(cid, otp);
	}
	
	public String bookDrivingTest(ServiceDTO dto,String issueType,String cid,String learnerNo) throws ERALISException, ERALISSystemException
	{
	
		String result = null;
		Connection conn = null;
		conn = ConnectionManager.getConnection();

		try 
		{
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = CommonDAO.getInstance().bookDrivingTest(dto,issueType,cid,learnerNo,conn);
				String[] temp = result.split("#");
				if(temp[0].equalsIgnoreCase("SUCCESS")){
					conn.commit();
				}
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		return result;
	}

	 
}
