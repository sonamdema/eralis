package bt.gov.rsta.framework.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dao.ServiceDAO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.SMSUtil;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class ServiceBusiness 
{
	Connection conn = null;
	private static ServiceBusiness serviceBusiness;
	
	public static ServiceBusiness getInstance()
	{
		if(serviceBusiness == null)
			serviceBusiness = new ServiceBusiness();
		return serviceBusiness;
	}
	

	/*public List<ServiceDTO> getPersonalInfo(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getPersonalInfo(dto);
	}*/
	public String editPersonalInfo(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = ServiceDAO.getInstance().editPersonalInfo(dto, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ServiceBusiness[editPersonalInfo]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String getCustomerId(String cid) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getCustomerId(cid);
	}
	
	
	
	public ServiceDTO getPersonalInfo(String cid,String dob) throws ERALISException, ERALISSystemException
	{
		ServiceDTO dto = new ServiceDTO();
		try {
			dto = ServiceDAO.getInstance().getPersonalInfo(cid,dob);
			
			if(dto.getPresentPhoneNo() != null){
				
				int minimum = 100000;
				int maximum = 999999;
				int randomNum = 0;
				randomNum = minimum + (int)(Math.random() * ((maximum - minimum) + 1));
				dto.setOtpNumber(Integer.toString(randomNum));
				
				String result = ServiceDAO.getInstance().saveOTP(dto.getCid(), Integer.toString(randomNum));
				
				if(result.equalsIgnoreCase("SUCCESS")){
					SMSModelVO smsVO = new SMSModelVO();
					smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_OTP_NOTIFICATION);
					sendSMSToUser(smsVO, dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dto;
	}
	public ServiceDTO getLearnerLicense(String learnerNo,String cid, String testType,String issueType) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getLearnerLicense(learnerNo,cid,testType,issueType);
	}
	public ServiceDTO getOtherIssueTypeBookDtls(String learnerNo,String cid, String testType,String issueType) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getOtherIssueTypeBookDtls(learnerNo,cid,testType,issueType);
	}
	public ServiceDTO getLicenseDetails(String tinNo,String licenseNo) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getLicenseDetails(tinNo,licenseNo);
	}
	public ServiceDTO getPersonalInfoId(String cid, String dlNo) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getPersonalInfoId(cid,dlNo);
	}

	public ServiceDTO checkApplicationSubmitted(String identityId,String formType) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().checkApplicationSubmitted(identityId,formType);
	}
	
	public ServiceDTO vehicleDetails(String vehicleNo,String cid) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().vehicleDetails(vehicleNo,cid);
	}
	public ServiceDTO generateAcknowledgementReceipt(String applicaitonNo) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().generateAcknowledgementReceipt(applicaitonNo);
	}
	
	public ServiceDTO getVehicleId(String vehicleNo,String vehicleType) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getVehicleId(vehicleNo,vehicleType);
	}
	public ServiceDTO organizationVehicleRenewaldtls(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().organizationVehicleRenewaldtls(dto);
	}
	public String bookDrivingTest(ServiceDTO dto,String issueType,String cid,String learnerNo) throws ERALISException, ERALISSystemException
	{
	
		String result = null;
		conn = ConnectionManager.getConnection();

		try 
		{
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ServiceDAO.getInstance().bookDrivingTest(dto,issueType,cid,learnerNo,conn);
				String[] temp = result.split("#");
				if(temp[0].equalsIgnoreCase("SUCCESS")){
					conn.commit();
				}
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		return result;
	}

	public String adminBookingEtest(ServiceDTO dto,String issueType,String cid,String learnerNo,String userId) throws ERALISException, ERALISSystemException
	{
	
		String result = null;
		conn = ConnectionManager.getConnection();

		try 
		{
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ServiceDAO.getInstance().adminBookingEtest(dto,issueType,cid,learnerNo,userId,conn);
				String[] temp = result.split("#");
				if(temp[0].equalsIgnoreCase("SUCCESS")){
					conn.commit();
				}
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		return result;
	}
	
	public String passengerBusPermit(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
	
		String result = null;
		conn = ConnectionManager.getConnection();

		try 
		{
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ServiceDAO.getInstance().passengerBusPermit(dto,conn);
				String[] dataArray = result.split("#");
				//result = dataArray[1];
				if(dataArray[0].equalsIgnoreCase("SUCCESS")){
					conn.commit();
				}
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		return result;
	}
	
	public String cancelDrivingTest(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().cancelDrivingTest(dto);
	}
	
	public List<EralisCommonDTO> getRenewalHistoryList(ServiceDTO vo) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getRenewalHistoryList(vo);
	}
	
	/*
	 * @method : fetchServiceId
	 * @purpose : gets the service short description by application number
	 */
	public String getServiceCode(String appNo) throws ERALISException, ERALISSystemException
	{
		String serviceCode = null;
		
		appNo=appNo.trim();
		if(appNo!=null && !appNo.equals(""))
		{
			serviceCode = ServiceDAO.getInstance().getServiceCode(appNo);
		}
		
		return serviceCode;
	}
	public String paymentResponse(String applicationNo,String paymentDate,String txnId,String txnAmount,String paymentStatus) throws ERALISException, ERALISSystemException
	{
		String result = null;
		Connection conn1 = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ServiceDAO.getInstance().paymentResponse(applicationNo, paymentDate, txnId, txnAmount, paymentStatus, conn);
				String[] temp = result.split("#");
				if(temp[0].equalsIgnoreCase("SUCCESS")){
					conn.commit();
					
					conn1 = ConnectionManager.getOnlineDbConnection();
					conn1.setAutoCommit(false);
					ServiceDAO.getInstance().updatePaymentDB(applicationNo,paymentDate,txnId,txnAmount,"Y", conn1);
					conn1.commit();
					
				}
				else if(!temp[0].equalsIgnoreCase("ALREADY_APPLIED"))
				{
					ConnectionManager.close(conn);
					conn1 = ConnectionManager.getOnlineDbConnection();
					conn1.setAutoCommit(false);
					ServiceDAO.getInstance().updatePaymentDB(applicationNo,paymentDate,txnId,txnAmount,"N", conn1);
					conn1.commit();
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionManager.close(conn);
			ConnectionManager.close(conn1);
		}
		return result;
	}
	public ApplicationStatusDTO retrieveApplicationStatus(String applicationNo,String serviceType,String cid,String LLNO,String dlNo,String VNo,String vehicleType) throws ERALISException, ERALISSystemException
	{
		ApplicationStatusDTO appStatusDto = ServiceDAO.getInstance().retrieveApplicationStatus(applicationNo,serviceType,cid,LLNO,dlNo,VNo,vehicleType);
		return appStatusDto;
		
	}
	public List<LicenseDTO> searchCustomerOffence(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().searchCustomerOffence(dto);
	}
	
	public String getLatestExpiryDate(String id, String requestType) throws ERALISException, ERALISSystemException
	{
		return ServiceDAO.getInstance().getLatestExpiryDate(id, requestType);
	}
	
	/**
	 * @param smsObject the sms object
	 * @param NotificationDTO
	 * @throws ERALISException the system exception
	 */  
	public void sendSMSToUser(SMSModelVO smsObject, ServiceDTO dto)
			throws ERALISException {
	String mobileNo = dto.getPresentPhoneNo();
	
	if (mobileNo != null && !mobileNo.trim().isEmpty()) {
		List<String> recipientList = new ArrayList<String>();
		recipientList.add(mobileNo);
		smsObject.setRecipentList(recipientList);
		smsObject.setOtpNumber(dto.getOtpNumber());
		
		Log.info("Before calling SMSUtil.sendSMS");
		new SMSUtil().sendSMS(smsObject);
		Log.info("After calling SMSUtil.sendSMS");
		}
	 }
	
}
