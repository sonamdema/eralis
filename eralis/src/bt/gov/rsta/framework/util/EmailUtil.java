package bt.gov.rsta.framework.util;

import java.io.FileInputStream;
import java.sql.Connection;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import bt.gov.rsta.framework.dao.EmailDAO;
import bt.gov.rsta.framework.dto.EmailConfigDTO;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class EmailUtil implements Runnable
{
	private static EmailModelVO emailObject = new EmailModelVO();

	/**
	 * This method retrieves the email template from DB for a given module name
	 * and populates the data from the business object
	 * 
	 * @param emailObj
	 * @param conn
	 * @author poojan
	 * @throws Exception
	 */

	public static void prepareEmailObject(EmailModelVO emailObj, Connection conn) throws ERALISException 
	{
		Log.info("Inside EmailUtil::prepareEmailObject");
		try 
		{
			EmailDAO.getMailTemplate(emailObj, conn);
			String emailBody = emailObj.getMailBody();
			String emailSubject = emailObj.getMailSubject();
			
			if (emailBody != null && !emailBody.trim().equals("")) 
			{
				emailBody = emailBody.replaceAll("#userName#", emailObj.getUserName());
				emailBody = emailBody.replaceAll("#loginId#", emailObj.getLoginId());
				emailBody = emailBody.replaceAll("#password#", emailObj.getPassword());
				emailBody = emailBody.replaceAll("#CUSTOMER_NAME#", emailObj.getCustomerName());
				emailBody = emailBody.replaceAll("#REQUEST_TYPE#", emailObj.getRequestType());
				emailBody = emailBody.replaceAll("#VEHICLE_NUMBER#", emailObj.getVehicleNo());
				emailBody = emailBody.replaceAll("#APPLICATION_NO#", emailObj.getApplicationNo());
				emailBody = emailBody.replaceAll("#APPROVAL_DATE#", emailObj.getApprovalDate());
				emailBody = emailBody.replaceAll("#LICENSE_NO#", emailObj.getLicenseNo());
				emailBody = emailBody.replaceAll("#LAST_RENEWAL_DATE#", emailObj.getRenewalDueDate());
				emailObj.setMailBody(emailBody);
			}
			emailObj.setMailSubject(emailSubject);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(e);
		}
	}

	/**
	 * Calls the prepareEmailObject() to prepare mail data Sends the mail and
	 * stores the email in DB
	 * 
	 * @param emailObj
	 * @param templateID
	 * 
	 * @throws Exception
	 */

	public static void sendMail(EmailModelVO emailObj) throws ERALISException {
		Log.info("Inside EmailUtil::sendMail");
		Connection conn = null;

		try {
			conn = ConnectionManager.getConnection();
			prepareEmailObject(emailObj, conn);

			// store in database. as IS_MAIL_SENT flag N
			String emailSlNo = storeEmailInDatabase(emailObj, conn);
			
			emailObj.setEmailSlNo(emailSlNo);
			emailObject.setRecipentList(emailObj.getRecipentList());
			emailObject.setMailBody(emailObj.getMailBody());
			emailObject.setMailSubject(emailObj.getMailSubject());
			emailObject.setEmailSlNo(emailObj.getEmailSlNo());
			emailObject.setEmailId(emailObj.getEmailId());
			emailObject.setMailType(emailObj.getMailType());
			emailObject.setModuleType(emailObj.getModuleType());
			emailObject.setTemplateModule(emailObj.getTemplateModule());
			emailObject.setTemplateSerialNumber(emailObj.getTemplateSerialNumber());
			
			emailObject.setUserName(emailObj.getUserName());
			emailObject.setLoginId(emailObj.getLoginId());
			emailObject.setPassword(emailObj.getPassword());
			
			Thread thread = new Thread(new EmailUtil());
			thread.start();

		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(e);
		}

		finally {
			ConnectionManager.close(conn, null, null, null);
		}
	}

	/**
	 * This method stores the email information in DB after sending email to
	 * recipients
	 * 
	 * @param emailObj
	 * @param conn
	 * @throws Exception
	 */
	public static String storeEmailInDatabase(EmailModelVO emailObj,Connection conn) throws ERALISException
	{
		Log.info("Inside EmailUtil::insertMail");
		String emailSlNo = EmailDAO.insertMail(emailObj, conn);
		return emailSlNo;
	}

	public static void updateEmailInDatabase(EmailModelVO emailObj,Connection conn) throws ERALISException
	{
		Log.info("Inside EmailUtil::updateMail");
		EmailDAO.updateMail(emailObj, conn);
	}

	/**
	 * Gets the given property from the resource bundle
	 * 
	 * @param propertyName
	 * @return String the property
	 * 
	 */
	public static String getProperty(String propsFile, String propertyName)
	{
		String value = "";
		ResourceBundle res = null;
		try {
			res = ResourceBundle.getBundle(propsFile);
			value = res.getString(propertyName);
		} catch (Exception e) {
			Log.fatal("could not create local RB:" + e.toString());
		}
		return value;
	}

	/**
	 * This method loads the properties files
	 * 
	 * @param propsFile
	 * @return
	 * @author prasanta
	 * @throws SystemException
	 */

	public static Properties loadPropertyFile(String propsFile)
			throws ERALISException {
		Properties props = null;
		try {
			if (propsFile != null) {
				props = new Properties();
				FileInputStream fis = new java.io.FileInputStream(
						new java.io.File(propsFile));
				props.load(fis);
				Log.info(propsFile + ":" + props);
			}
		} catch (Exception e) {
			throw new ERALISException(e);
		}
		return props;
	}

	/**
	 * This method returns the property value for a key
	 * 
	 * @param props
	 * @param key
	 * @author prasanta
	 * @return
	 */

	public static String readProperty(Properties props, String key) {
		String value = null;
		value = props.getProperty(key, "");
		return value;
	}

	/**
	 * This method converts a CommaSeparated list of email IDs to InternetAdress
	 * array
	 * 
	 * @param toMailCommaSepList
	 * @author prasanta
	 * @return
	 */

	public static InternetAddress[] convertAddressToMailArray(
			String toMailCommaSepList) {
		InternetAddress addressTo[] = null;
		if (toMailCommaSepList != null && !toMailCommaSepList.trim().equals("")) {
			StringTokenizer strTok = new StringTokenizer(toMailCommaSepList,
					",");
			addressTo = new InternetAddress[strTok.countTokens()];

			try {
				addressTo = InternetAddress.parse(toMailCommaSepList, false);
			} catch (AddressException aex) {
				Log.fatal("could not create InternetAdress for addressTo:"
						+ aex.toString());
			}
		}
		return addressTo;
	}

	/**
	 * This method loads local properties file of the application in the context
	 * 
	 * @author prasanta
	 */

/*	public static Properties loadLocalPropsFile() {
		Properties localprops = null;

		try {
			localprops = loadPropertyFile(MAIL_PROPERTIES_FILE);
			Log.info("loaded local props:" + MAIL_PROPERTIES_FILE);
		} catch (Exception fnex2) {
			Log.fatal("could not find file:" + fnex2.toString());
		}
		return localprops;
	}*/

	/**
	 * This method loads the Resource Bundle of the app
	 * 
	 * @return
	 * @author prasanta
	 */

	/*public static ResourceBundle loadLocalRBFile() {
		ResourceBundle localRB = null;

		try {
			localRB = ResourceBundle.getBundle(MAIL_PROPERTIES_FILE);
			Log.info("loaded local loadLocalRBFile:" + MAIL_PROPERTIES_FILE);
		} catch (MissingResourceException resex2) {
			Log.fatal("could not find file:" + resex2.toString());
		}
		return localRB;
	}*/

	/**
	 * This method loads Property from the resource bundle
	 * 
	 * @param localRB
	 * @param propertyName
	 * @param defaultValue
	 * @author prasanta
	 * @return
	 */

	/*public static Properties loadProps(ResourceBundle localRB,
			String propertyName, String defaultValue) {
		Properties props = null;
		try {
			props = loadPropertyFile(localRB.getString(propertyName));
			Log.info("loaded props:" + localRB.getString(propertyName));
		} catch (Exception fnex) {
			Log.fatal("could not find file:" + fnex.toString());
		}
		return props;
	}*/

	/**
	 * This method gets properties from the local Resource Bundle
	 * 
	 * @param localRB
	 * @author prasanta
	 * @return
	 */

	/*public static Properties getPropsFromRB(ResourceBundle localRB) {
		String key = null;
		Properties localProps = new Properties();
		if (localRB != null) {
			Enumeration<String> localenum = localRB.getKeys();
			while (localenum.hasMoreElements()) {
				key = localenum.nextElement();
				localProps.put(key, localRB.getString(key));
			}
		}
		return localProps;
	}*/

	/**
	 * Gets a list of strings and returns a string of all elements delimmited by
	 * comma
	 * 
	 * @param list
	 * @return String
	 */

	public static String prepareCommaSepStrFromList(List<String> list) {

		java.util.Iterator<String> itr = list.iterator();
		StringBuffer strBuff = new StringBuffer();
		while (itr.hasNext()) {
			strBuff.append(itr.next());
			strBuff.append(",");
		}
		return strBuff.toString();

	}

	private EmailUtil() {

	}

	public void run() {
		try {
			sendEMail();
		} catch (ERALISException e) {
			Log.error("At EmailUtil.run(): exception is -> "+e);
		}

	}

	public static void sendEMail() throws ERALISException {
		Connection conn = null;
		try {
			conn = ConnectionManager.getConnection();
			if (null != conn) {
				
				String toMailCommaSepList = prepareCommaSepStrFromList(emailObject.getRecipentList());
				
				EmailConfigDTO dto = EmailDAO.getEmailConfigDetails(conn);
				
				final String userName = dto.getUser();
				final String password = dto.getPassword();
				
				Properties localProps = new Properties();
				localProps.put("mail.smtp.host", dto.getSmtpHost());
				localProps.put("mail.smtp.port", dto.getSmtpPort());
				localProps.put("mail.debug", dto.getMailDebug());
				localProps.put("mail.smtp.starttls.enable", dto.getSmtpStartTLS());
				localProps.put("mail.fromAddress", dto.getFromAddress());
				localProps.put("mail.auth.user", dto.getUser());
				localProps.put("mail.auth.pwd", dto.getPassword());
				localProps.put("mail.smtp.auth", dto.getSmtpAuth());
				
				String fromAddress = localProps.getProperty("mail.fromAddress");
				
				MimeMessage message = null;
				if (new Boolean(dto.getSmtpAuth())) {
					message = new MimeMessage(Session.getInstance(localProps,
							new javax.mail.Authenticator() {
								protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(userName,
											password);
								}
							}));
				} else {
					message = new MimeMessage(Session.getInstance(localProps));
				}

				message.setContent(emailObject.getMailBody(), "text/html");
				InternetAddress addressFrom[] = null;
				addressFrom = new InternetAddress[1];
				addressFrom = InternetAddress.parse(fromAddress, false);
				message.addFrom(addressFrom);
				InternetAddress[] addressTo = null;
				if (toMailCommaSepList != null
						&& !toMailCommaSepList.equals("")) {
					addressTo = convertAddressToMailArray(toMailCommaSepList);
				}
				if (addressTo != null) {
					if (addressTo.length > 1) {
						message.addRecipients(Message.RecipientType.TO,
								addressTo);
					} else {
						message.addRecipient(Message.RecipientType.TO,
								new InternetAddress(toMailCommaSepList));
					}

					message.setSubject(emailObject.getMailSubject());
					Transport.send(message);

					updateEmailInDatabase(emailObject, conn);
				}
			} else {
				throw new ERALISException(
						"connection can not be null at EmailUtil.sendEMail()");
			}

		} catch (Exception e) {
			throw new ERALISException(e);
		} finally {
			ConnectionManager.close(conn, null, null, null);
		}
	}
	
	public static void sendScheduleMail(EmailModelVO emailObj) throws ERALISException
	{
		Connection conn = null;
		try {
			conn = ConnectionManager.getConnection();
			if (null != conn) {
				
				String toMailCommaSepList = prepareCommaSepStrFromList(emailObj.getRecipentList());
				
				EmailConfigDTO dto = EmailDAO.getEmailConfigDetails(conn);
				
				final String userName = dto.getUser();
				final String password = dto.getPassword();
				
				Properties localProps = new Properties();
				localProps.put("mail.smtp.host", dto.getSmtpHost());
				localProps.put("mail.smtp.port", dto.getSmtpPort());
				localProps.put("mail.debug", dto.getMailDebug());
				localProps.put("mail.smtp.starttls.enable", dto.getSmtpStartTLS());
				localProps.put("mail.fromAddress", dto.getFromAddress());
				localProps.put("mail.auth.user", dto.getUser());
				localProps.put("mail.auth.pwd", dto.getPassword());
				localProps.put("mail.smtp.auth", dto.getSmtpAuth());
				
				String fromAddress = localProps.getProperty("mail.fromAddress");
				
				MimeMessage message = null;
				if (new Boolean(dto.getSmtpAuth())) {
					message = new MimeMessage(Session.getInstance(localProps,
							new javax.mail.Authenticator() {
								protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(userName,
											password);
								}
							}));
				} else {
					message = new MimeMessage(Session.getInstance(localProps));
				}

				message.setContent(emailObj.getMailBody(), "text/html");
				InternetAddress addressFrom[] = null;
				addressFrom = new InternetAddress[1];
				addressFrom = InternetAddress.parse(fromAddress, false);
				message.addFrom(addressFrom);
				InternetAddress[] addressTo = null;
				if (toMailCommaSepList != null
						&& !toMailCommaSepList.equals("")) {
					addressTo = convertAddressToMailArray(toMailCommaSepList);
				}
				if (addressTo != null) {
					if (addressTo.length > 1) {
						message.addRecipients(Message.RecipientType.TO,
								addressTo);
					} else {
						message.addRecipient(Message.RecipientType.TO,
								new InternetAddress(toMailCommaSepList));
					}

					message.setSubject(emailObj.getMailSubject());
					Transport.send(message);

					updateEmailInDatabase(emailObj, conn);
				}
			} else {
				throw new ERALISException(
						"connection can not be null at EmailUtil.sendEMail()");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(e);
		} finally {
			ConnectionManager.close(conn, null, null, null);
		}
	}
	
	public static void updateEmailSentInformation(EmailModelVO emailObj,
			Connection conn) throws ERALISException
	{
		Log.info("Enter EmailUtil::updateEmailSentInformation");
		EmailDAO.updateMail(emailObj, conn);
		Log.info("Exit EmailUtil::updateEmailSentInformation");
	}
}
