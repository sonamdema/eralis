package bt.gov.rsta.framework.util;

import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import bt.gov.rsta.framework.dao.SMSDAO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;

/**
 * The Class SMSUtil.
 */
public final class SMSUtil implements Runnable {

	/** The Constant SMS_PROPERTIES_FILE. */
	public static final String SMS_PROPERTIES_FILE = "bt.gov.rsta.framework.properties.sms";

	public static final String SMS_ENCONDING_TYPE = "UTF-8";
	public static final String URL_MIDDLE_PART = "&msg=";

	/** The SMS object. */
	private SMSModelVO smsObject = null;

	/**
	 * This method retrieves the SMS template from DB for a given module name
	 * and populates the data from the business object.
	 * 
	 * @param SMSObj
	 *            the SMS obj
	 * @param conn
	 *            the conn
	 * @throws SystemException
	 *             the system exception
	 * @author prasanta
	 */

	public static void prepareSMSObject(SMSModelVO SMSObj, Connection conn)
			throws ERALISException {
		Log.info("Inside SMSUtil::prepareSMSObject");
		try 
		{
			SMSDAO.getMailTemplate(SMSObj, conn);
			String SMSBody = SMSObj.getSmsContent();
			if (SMSBody != null && !SMSBody.trim().equals("")) 
			{
				SMSBody = SMSBody.replaceAll("#loginId#", SMSObj.getLoginId());
				SMSBody = SMSBody.replaceAll("#password#", SMSObj.getPassword());
				SMSBody = SMSBody.replaceAll("#userName#", SMSObj.getUserName());
				SMSBody = SMSBody.replaceAll("#REQUEST_TYPE#", SMSObj.getRequestType());
				SMSBody = SMSBody.replaceAll("#VEHICLE_NUMBER#", SMSObj.getVehicleNo());
				SMSBody = SMSBody.replaceAll("#APPROVAL_DATE#", SMSObj.getApprovalDate());
				SMSBody = SMSBody.replaceAll("#LICENSE_NO#", SMSObj.getLicenseNo());
				SMSBody = SMSBody.replaceAll("#LAST_RENEWAL_DATE#", SMSObj.getRenewalDueDate());
				SMSBody = SMSBody.replaceAll("#OTP#", SMSObj.getOtpNumber());
				SMSBody = SMSBody.replaceAll("#TIN_NO#", SMSObj.getTinNo());
				SMSBody = SMSBody.replaceAll("#TEST_DATE#", SMSObj.getTestDate());
				SMSBody = SMSBody.replaceAll("#TEST_LOCATION#", SMSObj.getTestLocation());
				SMSBody = SMSBody.replaceAll("#DRIVE_TYPE#", SMSObj.getDriveType());
				SMSObj.setSmsContent(SMSBody);
			}

		} 
		catch (Exception e) 
		{
			throw new ERALISException(e);
		}
	}

	/**
	 * Gets the given property from the resource bundle.
	 * 
	 * @param propsFile
	 *            the props file
	 * @param propertyName
	 *            the property name
	 * @return String the property
	 */
	public static String getProperty(String propsFile, String propertyName) {
		String value = "";
		ResourceBundle res = null;
		try {
			res = ResourceBundle.getBundle(propsFile);
			value = res.getString(propertyName);
		} catch (Exception e) {
			Log.fatal("could not create local RB:" + e.toString());
		}
		return value;
	}

	/**
	 * This method loads the properties files.
	 * 
	 * @param propsFile
	 *            the props file
	 * @return the properties
	 * @throws SystemException
	 *             the system exception
	 * @author prasanta
	 */

	public static Properties loadPropertyFile(String propsFile)
			throws ERALISException {
		Properties props = null;
		try {
			if (propsFile != null) {
				props = new Properties();
				FileInputStream fis = new java.io.FileInputStream(
						new java.io.File(propsFile));
				props.load(fis);
				Log.info(propsFile + ":" + props);
			}
		} catch (Exception e) {
			throw new ERALISException(e);
		}
		return props;
	}

	/**
	 * This method returns the property value for a key.
	 * 
	 * @param props
	 *            the props
	 * @param key
	 *            the key
	 * @return the string
	 * @author prasanta
	 */

	public static String readProperty(Properties props, String key) {
		String value = null;
		value = props.getProperty(key, "");
		return value;
	}

	/**
	 * This method loads local properties file of the application in the
	 * context.
	 * 
	 * @return the properties
	 * @author prasanta
	 */

	public static Properties loadLocalPropsFile() {
		Properties localprops = null;

		try {
			localprops = loadPropertyFile(SMS_PROPERTIES_FILE);
			Log.info("loaded local props:" + SMS_PROPERTIES_FILE);
		} catch (Exception fnex2) {
			Log.fatal("could not find file:" + fnex2.toString());
		}
		return localprops;
	}

	/**
	 * This method loads the Resource Bundle of the app.
	 * 
	 * @return the resource bundle
	 * @author prasanta
	 */

	public static ResourceBundle loadLocalRBFile() {
		ResourceBundle localRB = null;

		try {
			localRB = ResourceBundle.getBundle(SMS_PROPERTIES_FILE);
			Log.info("loaded local loadLocalRBFile:" + SMS_PROPERTIES_FILE);
		} catch (MissingResourceException resex2) {
			Log.fatal("could not find file:" + resex2.toString());
		}
		return localRB;
	}

	/**
	 * This method loads Property from the resource bundle.
	 * 
	 * @param localRB
	 *            the local rb
	 * @param propertyName
	 *            the property name
	 * @param defaultValue
	 *            the default value
	 * @return the properties
	 * @author prasanta
	 */

	public static Properties loadProps(ResourceBundle localRB,
			String propertyName, String defaultValue) {
		Properties props = null;
		try {
			props = loadPropertyFile(localRB.getString(propertyName));
			Log.info("loaded props:" + localRB.getString(propertyName));
		} catch (Exception fnex) {
			Log.fatal("could not find file:" + fnex.toString());
		}
		return props;
	}

	/**
	 * This method gets properties from the local Resource Bundle.
	 * 
	 * @param localRB
	 *            the local rb
	 * @return the props from rb
	 * @author prasanta
	 */

	public static Properties getPropsFromRB(ResourceBundle localRB) {
		String key = null;
		Properties localProps = new Properties();
		if (localRB != null) {
			Enumeration<String> localenum = localRB.getKeys();
			while (localenum.hasMoreElements()) {
				key = localenum.nextElement();
				localProps.put(key, localRB.getString(key));
			}
		}
		return localProps;
	}

	/**
	 * Gets a list of strings and returns a string of all elements delimmited by
	 * comma.
	 * 
	 * @param list
	 *            the list
	 * @return String
	 */

	public static String prepareCommaSepStrFromList(List<String> list) {

		java.util.Iterator<String> itr = list.iterator();
		StringBuffer strBuff = new StringBuffer();
		while (itr.hasNext()) {
			strBuff.append(itr.next());
			strBuff.append(",");
		}
		
		return strBuff.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			sendSms();
		} catch (ERALISException e) {
			Log.error("At SMSUtil.run(): exception is -> ");
		}

	}

	/**
	 * Send e mail.
	 * 
	 * @throws SystemException
	 *             the system exception
	 */
	public void sendSMS(SMSModelVO smsObj) throws ERALISException {
		Log.info("Inside SMSUtil::sendSMS");
		Connection conn = null;

		try {
			conn = ConnectionManager.getConnection();
			prepareSMSObject(smsObj, conn);
			smsObject = new SMSModelVO();
			smsObject.setRecipentList(smsObj.getRecipentList());
			smsObject.setSmsContent(smsObj.getSmsContent());
			smsObject.setSmsType(smsObj.getSmsType());
			smsObject.setModuleType(smsObj.getModuleType());
			smsObject.setTemplateModule(smsObj.getTemplateModule());
			smsObject.setPassword(smsObj.getPassword());
			smsObject.setUserName(smsObj.getUserName());
			smsObject.setOtpNumber(smsObj.getOtpNumber());
			smsObject.setTinNo(smsObj.getTinNo());
			smsObject.setTestDate(smsObj.getTestDate());
			smsObject.setTestLocation(smsObj.getTestLocation());
			smsObject.setDriveType(smsObj.getDriveType());
			
			Thread thread = new Thread(new SMSUtil(smsObject));
			thread.start();

		} catch (Exception e) {
			throw new ERALISException(e);
		}

		finally {
			ConnectionManager.close(conn);
		}
	}

	/**
	 * Send sms.
	 * 
	 * @throws SystemException
	 *             the system exception
	 */
	private void sendSms() throws ERALISException {
		boolean result = false;
		String encodedMobileNo = null;
		String encodedSMScontent = null;
		String fullURLStr = null;
		URL url = null;
		java.net.HttpURLConnection connection = null;
		String responseMSg = null;
		String smsContent = smsObject.getSmsContent();
		try 
		{
			ResourceBundle localRB = loadLocalRBFile();
			Properties localProps = getPropsFromRB(localRB);

			String smsUrl = localProps.getProperty("sms.url.firstPart");

			for (String mobileNo : smsObject.getRecipentList()) {

				encodedMobileNo = URLEncoder.encode(mobileNo,
						SMS_ENCONDING_TYPE);
				Log.debug("######## SMSUtil[sendSMS] encoded Mobile Number = "
						+ encodedMobileNo);

				encodedSMScontent = URLEncoder.encode(smsContent,
						SMS_ENCONDING_TYPE);
				Log.debug("######## SMSUtil[sendSMS] encoded SMS content = "
						+ encodedSMScontent);

				fullURLStr = smsUrl + encodedMobileNo + URL_MIDDLE_PART
						+ encodedSMScontent;
				url = new URL(fullURLStr);
				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(false);
				connection.setDoInput(true);
				responseMSg = connection.getResponseMessage();
				Log.info("responseMSg::" + responseMSg);

				int code = connection.getResponseCode();
				if (code == HttpURLConnection.HTTP_OK) {
					connection.disconnect();
					result = true;
				}
			}
		} catch (Exception e) {
			Log.error("######## Error in SMSUtil[sendSMS] : " + e.toString());
		}
		if (result) {
			Log.info("SMS sent successfully");
		} else {
			Log.fatal("SMS could not be sent");
		}
	}
	
	/**
	 * Send sms.
	 * 
	 * @throws SystemException
	 *             the system exception
	 */
	public static void sendScheduleSMS(SMSModelVO smsObj) throws ERALISException {
		boolean result = false;
		String encodedMobileNo = null;
		String encodedSMScontent = null;
		String fullURLStr = null;
		URL url = null;
		java.net.HttpURLConnection connection = null;
		String responseMSg = null;
		String smsContent = smsObj.getSmsContent();
		try {
			ResourceBundle localRB = loadLocalRBFile();
			Properties localProps = getPropsFromRB(localRB);

			String smsUrl = localProps.getProperty("sms.url.firstPart");

			for (String mobileNo : smsObj.getRecipentList()) {

				encodedMobileNo = URLEncoder.encode(mobileNo,
						SMS_ENCONDING_TYPE);
				Log.debug("######## SMSUtil[sendSMS] encoded Mobile Number = "
						+ encodedMobileNo);

				encodedSMScontent = URLEncoder.encode(smsContent,
						SMS_ENCONDING_TYPE);
				Log.debug("######## SMSUtil[sendSMS] encoded SMS content = "
						+ encodedSMScontent);

				fullURLStr = smsUrl + encodedMobileNo + URL_MIDDLE_PART
						+ encodedSMScontent;
				url = new URL(fullURLStr);
				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(false);
				connection.setDoInput(true);
				responseMSg = connection.getResponseMessage();
				Log.info("responseMSg::" + responseMSg);

				int code = connection.getResponseCode();
				if (code == HttpURLConnection.HTTP_OK) {
					connection.disconnect();
					result = true;
				}
			}
		} catch (Exception e) {
			Log.error("######## Error in SMSUtil[sendSMS] : " + e.toString());
		}
		if (result) {
			Log.info("SMS sent successfully");
		} else {
			Log.fatal("SMS could not be sent");
		}
	}

	/**
	 * Instantiates a new SMS util.
	 */
	public SMSUtil() {

	}

	/**
	 * Instantiates a new SMS util.
	 */
	public SMSUtil(SMSModelVO smsObj) {
		this.smsObject = smsObj;
	}
}
