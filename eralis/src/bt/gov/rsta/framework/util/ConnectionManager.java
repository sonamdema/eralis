package bt.gov.rsta.framework.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
/**
 * 
 * @author Poojan Sharma
 * @date Sept 17, 2012
 * ConnectionManager.java
 */
public class ConnectionManager {

	public static DataSource g2cDataSource = null;
	public static DataSource alfrescoDataSource = null;
	public static final String DATASOURCE_NAME_ERALISDB = "java:comp/env/jdbc/ERALIS_DB";
	public static final String DATASOURCE_NAME_ERALISONLINEDB = "java:comp/env/jdbc/ERALIS_ONLINE_PAYMENT_DB";
	public static final String DATASOURCE_NAME_OTSDB = "java:comp/env/jdbc/OTS_DB";
	
	//public static final String DATASOURCE_NAME_ERALISDB = "java:/ERALIS_DB";
	
	public static DataSource getDatasourceName(String dataSourceName) {

		try {
			InitialContext ctx = new InitialContext();
			if (dataSourceName != null && !dataSourceName.equals("")) {
				try {
					g2cDataSource = (DataSource) ctx.lookup(dataSourceName);
				} catch (Exception e) {
					Log.error(
							" Exception occured within if block of getDatasourceName method ",
							e);
				}
			}
		} catch (NamingException e) {
			Log.error(
					" NamingException occured in getDatasourceName method ", e);
		}
		return g2cDataSource;

	}

	public static final Connection getConnection(String dataSourceName) 
	{
		Connection conn = null;
		try 
		{
			g2cDataSource = getDatasourceName(dataSourceName);
			if (g2cDataSource == null) 
			{
				Log.debug(" Problem %%%%%%%%%%%%%%%%%%%%% ");
			} 
			else 
			{
				conn = g2cDataSource.getConnection();
			}
		} 
		catch (SQLException e) 
		{
			Log.error(" SQLException occured in getConnection method ", e);
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			Log.error(" Exception occured  in getConnection method ", e);
			e.printStackTrace();
		}
		
		return conn;
	}

	public static final Connection getConnection() {

		return getConnection(DATASOURCE_NAME_ERALISDB);
	}

	public static final Connection getOnlineDbConnection() {

		return getConnection(DATASOURCE_NAME_ERALISONLINEDB);
	}

	public static final Connection getOtsDbConnection() {

		return getConnection(DATASOURCE_NAME_OTSDB);
	}

	/**
	 * This method will rollback the provided Connection.
	 * 
	 * @param conn
	 *            the conn
	 */
	public static void rollbackConnection(Connection conn) {
		try {
			if (null != conn && !conn.isClosed()) {
				conn.rollback();
			}
		} catch (SQLException e) {
			Log.error(
					" Exception occured in rollbackConnection  ",
					e);
		}
	}

	/**
	 * Returns all JDBC resources associated with the given connection to to the
	 * pool. All exceptions are handled by being logged. Should be called in
	 * 'finally{}' clause wherever JDBC connections are used.
	 * 
	 * @param conn
	 *            connection to be closed, or null
	 * @param stmt
	 *            statement to be closed, or null
	 * @param rset
	 *            ResultSet to be closed, or null
	 */
	public static void close(Connection conn, Statement stmt, ResultSet rset,
			PreparedStatement ps) // throws Exception
	{
		if (rset != null) {
			try {
				rset.close();
			} catch (Throwable ex) {
				Log.error(
						" Exception occured in if block(reset) of close method  ",
						ex);
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (Throwable ex) {
				Log.error(
						" Exception occured in if block(stmt) of close method  ",
						ex);
			}
		}
		if (ps != null) {
			try {
				ps.close();
			} catch (Throwable ex) {
				Log.error(
						" Exception occured in if block(ps) of close method  ",
						ex);
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (Throwable ex) {
				Log.error(
						" Exception occured in if block(conn) of close method  ",
						ex);
			}
		}

	}
	
	public static final Connection getDirectConnection() 
    {
		
		Connection connection = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/eralis_db", "root", "");
		}
		catch (SQLException e) 
		{
			System.out.println(" SQLException occured in getConnection method ");
		}
		catch (Exception e) 
		{
			System.out.println(" Exception occured  in getConnection method ");
		}
		return connection;
		
	}

	public static void close(Connection conn) // throws Exception
	{

		if (conn != null) {
			try {
				conn.close();
			} catch (Throwable ex) {
				Log.error(
						" Exception occured in if block(conn) of close method  ",
						ex);
			}
		}
	}
}
