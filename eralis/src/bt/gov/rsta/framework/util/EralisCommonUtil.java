package bt.gov.rsta.framework.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Calendar;
import java.util.ResourceBundle;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Months;
import org.joda.time.Years;

import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;

public class EralisCommonUtil 
{
	private static final int ERALIS_SL_NO_PORTION_LENGTH = 4;
	
	/**
	 * Java method to return random integer between 0 and * given number. 
	 * Pay attention to brackets while casting * (int) 
	 * Math.random*max will return incorrect result. 
	 */ 
	public static int getRandom(int max){ 
		return (int) (Math.random()*max); 
	}
	
	/**
	 * Gets the exception stack trace as a string.
	 * 
	 * @param exception
	 * @return String
	 */
	public static boolean notNullOrEmpty(String content) {

		boolean result = false;

		if (null != content && content.length() > 0) {
			result = true;
		}

		return result;
	}
	
	public static boolean isStringPresent(String s) {
		boolean ret = false;
		for (int i = 0; i < s.length(); i++) {
			if ((int) s.charAt(i) < 48 || (int) s.charAt(i) > 57) {
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	/**
	 * Gets the properties file and loads it into application context
	 * 
	 * @return ResourceBundle
	 */
	public static ResourceBundle createResourceBundle() {
		ResourceBundle applicationProp = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.ApplicationResources");
		return applicationProp;
	}
	
	/**
	 * Gets the key from the properties file
	 * 
	 * @param key
	 * @return String
	 */
	public static String getProperty(String key) {
		String value = createResourceBundle().getString(key);
		return value;
	}
	
	/**
	 * Gets the properties file and loads it into application context
	 * 
	 * @return ResourceBundle
	 */
	public static ResourceBundle createNotificationResourceBundle() {
		ResourceBundle applicationProp = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.eralis");
		return applicationProp;
	}
	
	/**
	 * Gets the key from the properties file
	 * 
	 * @param key
	 * @return String
	 */
	public static String getNotificationProperty(String key) {
		String value = createNotificationResourceBundle().getString(key);
		return value;
	}
	
	public static final String getStackTraceAsString(Throwable exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.println();
		exception.printStackTrace(pw);
		return sw.toString();
	}
	
	public static final String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1)
        {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }
	
	/**
	 * Calculates the date difference in no. of days, months or years 
	 * using Joda Time API
	 * 
	 * @param startDate - format yyyy-mm-dd
	 * @param endDate - format yyyy-mm-dd
	 * @param requestType - what kind of diff eg: no. of days, months or years
	 * @return int - difference in days, months or years
	 */
	public static final int dateDifferenceAsInt(String startDate, String endDate, String requestType) throws Exception
	{
		int diff = 0;
		
		try 
		{
			int startYear = Integer.parseInt(startDate.substring(0, 4));
			int startMonth = Integer.parseInt(startDate.substring(5, 7));
			int startDay = Integer.parseInt(startDate.substring(8, 10));
			
			int endYear = Integer.parseInt(endDate.substring(0, 4));
			int endMonth = Integer.parseInt(endDate.substring(5, 7));
			int endDay = Integer.parseInt(endDate.substring(8, 10));
			
			DateTime start = new DateTime(startYear, startMonth, startDay, 0,0,0,0);
			DateTime end = new DateTime(endYear, endMonth, endDay, 0,0,0,0);
					
			Days days = Days.daysBetween(start, end);
			Months months = Months.monthsBetween(start, end);
			Years years = Years.yearsBetween(start, end);
			
			if(requestType.equals("D"))
				diff = days.getDays();
			else if(requestType.equals("M"))
				diff = months.getMonths();
			else if(requestType.equals("Y"))
				diff = years.getYears();
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		
		return diff;
	}
	
	public static final String getServiceIdAsString(String pageId, Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String serviceId = null;
		
		try
		{
			if(conn != null)
			{
				String query = "SELECT Service_Id FROM t_service_master WHERE Page_Id=?";
				pst = conn.prepareStatement(query);
				pst.setString(1, pageId);
				rs = pst.executeQuery();
				rs.first();
				serviceId = rs.getString("Service_Id");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		
		return serviceId;
	}
	
	public static final String generateETestApplicationNumber(Connection conn) throws Exception
	{
		String finalStrSlNo = null;
		String lastApplNo = getLastTestApplNo(conn);
		
		if(null != lastApplNo)
		{
			String slNo = lastApplNo;//lastApplNo.substring(8, lastApplNo.length());
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				String newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
			else if(isStringPresent(slNo))
			{
				
				
				
				
			}
		}
		else 
		{
			finalStrSlNo = generateNewTestApplNo();
		}
		
		StringBuffer newApplNo = new StringBuffer();
		newApplNo.append("DL");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
		String dateString = simpleDateFormat.format(new Date());
		String year = dateString.split("/")[2];
		String month = dateString.split("/")[1];
		String day = dateString.split("/")[0];
		newApplNo.append(year);
		newApplNo.append(month);
		newApplNo.append(day);
		newApplNo.append(finalStrSlNo);
		
		//DL201608050001
		
		return newApplNo.toString();
	}
	
	public static String generateNewTestApplNo()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastTestApplNo(Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String applNo = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT MAX(Application_Number) maxApplNo FROM t_etest_application";
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					applNo = rs.getString("maxApplNo");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return applNo;
	}
	
	public static final String generateNOCFormat(String jurisId, Connection conn) throws Exception
	{
		String finalStrSlNo = null;
		String lastNOCNo = getLastNOCNo(conn, jurisId);
		
		if(null != lastNOCNo)
		{
			String slNo = lastNOCNo.substring(lastNOCNo.lastIndexOf('/')+1, lastNOCNo.length());
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				String newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
		}
		else 
		{
			finalStrSlNo = generateNewNOCNo();
		}
		
		StringBuffer newApplNo = new StringBuffer();
		newApplNo.append("RSTA");
		newApplNo.append("/");
		
		String regionId = getRegionCode1(conn, jurisId);
		
		newApplNo.append(regionId);
		newApplNo.append("/");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = simpleDateFormat.format(new Date());
		String year = dateString.split("/")[2];
		
		newApplNo.append("RG-19");
		newApplNo.append("/");
		newApplNo.append(year);
		newApplNo.append("/");
		newApplNo.append(finalStrSlNo);
		
		return newApplNo.toString();
	}
	
	private static String generateNewNOCNo()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastNOCNo(Connection conn, String jurisId) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String nocId = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT NOC_ID FROM t_noc_dtls a WHERE a.`location_id`=? ORDER BY a.`NOC_ID` DESC LIMIT 1";
				pst = conn.prepareStatement(query);
				pst.setString(1, jurisId);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					nocId = rs.getString("NOC_ID");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return nocId;
	}
	
	public static final String generateApplicationNumberFormat(String tableName, String pageId,String bfsNo, Connection conn) throws Exception
	{
		String finalStrSlNo = null;
		
		String serviceType = getServiceIdentifier(pageId, conn);
		String lastApplNo = getLastApplNo(conn, tableName, serviceType);
		
		if(null != lastApplNo)
		{
			String slNo = lastApplNo.substring(11, lastApplNo.length());
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				String newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
		}
		else
		{
			finalStrSlNo = generateNewApplNo();
		}
		
		StringBuffer newApplNo = new StringBuffer();
		if(bfsNo!=null)
			newApplNo.append("PN");
		else
			newApplNo.append("RN");
		
		String serviceId = getServiceIdForAppNo(pageId, conn);
		
		newApplNo.append(serviceId);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
		String dateString = simpleDateFormat.format(new Date());
		String day = dateString.split("/")[0];
		String month = dateString.split("/")[1];
		String year = dateString.split("/")[2];
		
		newApplNo.append(year);
		newApplNo.append(month);
		newApplNo.append(day);
		newApplNo.append(finalStrSlNo);
		
		return newApplNo.toString();
	}
	
	private static String getServiceIdentifier(String pageId, Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String serviceIdentifier = null;
		
		try
		{
			String query = "SELECT Service_Identifier FROM t_service_master WHERE Page_Id=?";
			
			pst = conn.prepareStatement(query);
			pst.setString(1, pageId);
			rs = pst.executeQuery();
			rs.first();
			serviceIdentifier = rs.getString("Service_Identifier");
		}
		catch (Exception e)
		{
			throw new Exception();
		}
		
		return serviceIdentifier;
	}
	
	private static String generateNewApplNo()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastApplNo(Connection conn, String table_name, String serviceIdentifier) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String applNo = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT `Application_Number` FROM " + table_name + " WHERE `App_Submission_Date` LIKE ? AND `Application_Type`=? ORDER BY `Application_Number` DESC LIMIT 1";
				pst = conn.prepareStatement(query);
				String dateformat = getDateFormatForLastAppNo(new Date());
				pst.setString(1, "%" + dateformat + "%");
				pst.setString(2, serviceIdentifier);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					applNo = rs.getString("Application_Number");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return applNo;
	}
	
	private static String getServiceIdForAppNo(String pageId, Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String serviceId = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT Service_Id FROM t_service_master WHERE Page_Id=?";
				pst = conn.prepareStatement(query);
				pst.setString(1, pageId);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					serviceId = rs.getString("Service_Id");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return serviceId;
	}
	
	private static String getDateFormatForLastAppNo(Date currentDate) 
	{
		StringBuffer strBuf = new StringBuffer("");
		if (null != currentDate) 
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = simpleDateFormat.format(currentDate);
			String[] strArra = dateString.split("-");
			strBuf.append(strArra[0]);
			strBuf.append("-");
			strBuf.append(strArra[1]);
			strBuf.append("-");
			strBuf.append(strArra[2]);
		}
		
		return strBuf.toString();
	}

	
	public static final String getCustomerCode(String regionCode) throws Exception
	{
		String customerCode = null;
		
		Calendar cal = Calendar.getInstance();
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		
		Random randomGenerator = new Random();
	    int randomInt = randomGenerator.nextInt(1000);
	    
	    customerCode = regionCode + year + (month+1) + Integer.toString(randomInt);
	    
		return customerCode;
	}
	
	public static final String generateCustomerCodeFormat(String categoryCode, Connection conn) throws Exception
	{
		String finalStrSlNo = null, tableName = null, columnName = null;
		
		if(categoryCode.equalsIgnoreCase("P")){
			tableName = "t_personal_dtls";
			columnName = "Personal_Info_Id";
		}
		else {
			tableName = "t_organization_info";
			columnName = "Organization_Info_Id";
		}
		
		String lastCustomerNo = getLastCustomerCode(conn, columnName, tableName);
		
		if(null != lastCustomerNo)
		{
			String slNo = lastCustomerNo.substring(7, lastCustomerNo.length());
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				String newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
		}
		else{
			finalStrSlNo = generateNewCustomerCode();
		}
		
		StringBuffer newCustomerNo = new StringBuffer();
		newCustomerNo.append(categoryCode.toUpperCase());
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
		String dateString = simpleDateFormat.format(new Date());
		String day = dateString.split("/")[0];
		String month = dateString.split("/")[1];
		String year = dateString.split("/")[2];
		
		newCustomerNo.append(year);
		newCustomerNo.append(month);
		newCustomerNo.append(day);
		newCustomerNo.append(finalStrSlNo);
		
		System.out.println(newCustomerNo.toString());
		return newCustomerNo.toString();
	}
	
	private static String generateNewCustomerCode()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastCustomerCode(Connection conn, String columnName, String table_name) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String id = null, customerId = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT MAX("+columnName+") id FROM " + table_name;
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					id = rs.getString("id");
				
				String query1 = "SELECT Customer_Id FROM " + table_name + " WHERE " + columnName + " = ?";
				pst = conn.prepareStatement(query1);
				pst.setString(1, id);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					customerId = rs.getString("Customer_Id");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return customerId;
	}
	
	public static String generatePermitNoFormat(Connection conn, String regionId) throws Exception
	{
		String finalStrSlNo = null;
		
		String lastPermitNo = getLastPermitNo(conn, regionId);
		String regionCode = getRegionCode(conn, regionId);
		
		if(null != lastPermitNo)
		{
			String slNo = lastPermitNo.substring(12, lastPermitNo.length());
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				String newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
		}
		else{
			finalStrSlNo = generateNewPermitNo();
		}
		
		StringBuffer newPermitNo = new StringBuffer();
		newPermitNo.append("RSTA");
		newPermitNo.append("/");
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = simpleDateFormat.format(new Date());
		String year = dateString.split("/")[2];
		
		newPermitNo.append(regionCode);
		newPermitNo.append("-");
		newPermitNo.append(year);
		newPermitNo.append("/");
		newPermitNo.append(finalStrSlNo);
		
		System.out.println(newPermitNo.toString());
		return newPermitNo.toString();
	}
	
	private static String generateNewPermitNo()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastPermitNo(Connection conn, String regionId) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String permitNo = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT a.`Permit_No` AS permitNo FROM `t_permit_dtls` a "
								+ "WHERE a.`Permit_Year`=(SELECT MAX(`Permit_Year`) FROM `t_permit_dtls` WHERE `Region_Id`=a.`Region_Id`) "
								+ "AND a.`Permit_Details_Id`=(SELECT MAX(`Permit_Details_Id`) FROM `t_permit_dtls` WHERE `Region_Id`=a.`Region_Id`) "
								+ "AND a.`Region_Id`=?;";
				pst = conn.prepareStatement(query);
				pst.setString(1, regionId);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					permitNo = rs.getString("permitNo");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return permitNo;
	}
	
	public static String getRegionCode(Connection conn, String regionId) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String regionCode = null;
		
		try 
		{
			if(conn != null)
			{
				String query = "SELECT region_desc FROM t_region_master WHERE region_id=?";
				pst = conn.prepareStatement(query);
				pst.setString(1, regionId);
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					regionCode = rs.getString("region_desc");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return regionCode;
	}
	
	//Learner License Number Generation
	public static String generateLearnerLicenseNoFormat(Connection conn, String regionId) throws Exception
	{
		PreparedStatement pst = null;
		String newSlNo = "0";
		String finalStrSlNo = null;
		
		String lastLearnerLicenseNo = getLastLearnerLicenseNo(conn, regionId);
		String regionCode = getRegionCode(conn, regionId);
		
		if(null != lastLearnerLicenseNo)
		{
			String slNo = lastLearnerLicenseNo;//lastLearnerLicenseNo.substring(5, lastLearnerLicenseNo.length());
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
			
			pst = conn.prepareStatement(UPDATE_LATEST_SL_NO);
			pst.setString(1, newSlNo);
			pst.setString(2, regionId);
			pst.setString(3, "3");
			pst.executeUpdate();
		}
		else
		{
			finalStrSlNo = generateNewLearnerLicenseNo();
		}
		
		StringBuffer NewLearnerLicenseNo = new StringBuffer();
		NewLearnerLicenseNo.append(regionCode);
		NewLearnerLicenseNo.append("/");
		NewLearnerLicenseNo.append("LL");
		NewLearnerLicenseNo.append("-");
		NewLearnerLicenseNo.append(finalStrSlNo);
		
		return NewLearnerLicenseNo.toString();
	}
	private static String generateNewLearnerLicenseNo()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	private static String getLastLearnerLicenseNo(Connection conn, String regionId) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String LearnerLicenseNo = null;
		
		try 
		{
			if(conn != null)
			{
				String query = GET_MAX_LICENSE_SL_NO;
				pst = conn.prepareStatement(query);
				pst.setString(1, regionId);
				pst.setString(2, "3");
				rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					LearnerLicenseNo = rs.getString("Sl_No");
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return LearnerLicenseNo;
	}
	
	//Driving License No
	public static String generateDrivingLicenseNoFormat(Connection conn,String regionId, String table_name) throws Exception
	{
		PreparedStatement pst = null;
		String finalStrSlNo = null;
		String newSlNo = "0";
		String lastDrivingLicenseNo = getLastDrivingLicenseNo(conn,regionId,table_name);
		String regionCode = getRegionCode(conn, regionId);
		
		if(null != lastDrivingLicenseNo)
		{
			String slNo = lastDrivingLicenseNo;
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
		}
		else
		{
			finalStrSlNo = generateNewDrivingLicenseNo();
		}
		
		StringBuffer NewDrivingLicenseNo = new StringBuffer();
		NewDrivingLicenseNo.append(regionCode);
		NewDrivingLicenseNo.append("-");
		NewDrivingLicenseNo.append(finalStrSlNo);
		
		String licenseNo = NewDrivingLicenseNo.toString();
		
		/*
		 * code to handle already issued license number
		 */
		ResultSet rs = null;
		boolean licenseNoExistsFlag = true;
		while(licenseNoExistsFlag)
		{
			String query = "SELECT COUNT(*) rowCount FROM t_driving_license_dtls a WHERE a.`Driving_License_No`=?";
			pst = conn.prepareStatement(query);
			pst.setString(1, licenseNo);
			rs = pst.executeQuery();
			rs.first();
			int rowCount = rs.getInt("rowCount");
			
			if(rowCount > 0)
			{
				newSlNo	=	String.valueOf(((new Integer(newSlNo)) + 1));
				licenseNo = regionCode+"-"+newSlNo;
				
				continue;
			}
			else {
				break;
			}
		}
		
		pst = conn.prepareStatement(UPDATE_LATEST_SL_NO);
		pst.setString(1, newSlNo);
		pst.setString(2, regionId);
		pst.setString(3, "2");
		pst.executeUpdate();
		
		return licenseNo;
	}
	
	private static String generateNewDrivingLicenseNo()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastDrivingLicenseNo(Connection conn,String regionId, String table_name) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String DrivingLicenseNo = null;
		
		try 
		{
			if(conn != null)
			{
				String query = GET_MAX_LICENSE_SL_NO;
				pst = conn.prepareStatement(query);
				pst.setString(1, regionId);
				pst.setString(2, "2");
				rs = pst.executeQuery(); 
				while(rs.next()){
					DrivingLicenseNo = rs.getString("Sl_No");
				}
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return DrivingLicenseNo;
	}
	
	public static EralisCommonDTO generateVehicleNumberFormat(Connection conn, String vehicleRegistrationId, String regionId,String vehicleTypeId) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		EralisCommonDTO dto= new EralisCommonDTO();
		String finalStrSlNo = null;
		String newVehicleNo	=	null;
		String VehicleCode = getVehicleCode(conn, vehicleRegistrationId);
		String regionCode = getRegionCode1(conn, regionId);
		//String VehicleType = getVehicleType(conn, vehicleTypeId);
		
		String vehicleSequenceDtls = getLastVehicleNumber(conn, vehicleRegistrationId,regionId,vehicleTypeId);
		String[] vehicleNumberDtls = vehicleSequenceDtls.split("#");
		String LastVehicleNumber	=	vehicleNumberDtls[0];
		String vPrefix	=	vehicleNumberDtls[1];
		
		if(null != LastVehicleNumber)
		{
			if(LastVehicleNumber.equalsIgnoreCase("9999"))
			{
				if(!"null".equals(vPrefix))
				{	
					char newPrefix	=	vPrefix.charAt(0);
					newPrefix++;
					newVehicleNo	=	"0001";
					vPrefix=String.valueOf(newPrefix);
				}
				else
				{	
					vPrefix="A";
					newVehicleNo	=	"0001";
				}
			}
			else
			{
				newVehicleNo	=	String.valueOf(((new Integer(LastVehicleNumber)) + 1));
				int dgtCount = String.valueOf(newVehicleNo).length();
				StringBuffer sbf = new StringBuffer();
				for(int i=dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newVehicleNo);
				
				newVehicleNo = sbf.toString();
			}
		}
		else
		{
			newVehicleNo = "1";
			finalStrSlNo = generateNewVehicleNumber();
		}
		if("null".equalsIgnoreCase(vPrefix))
		{	
			vPrefix="";
		}
		
		/*if(VehicleType .equalsIgnoreCase("10"))
		{
			finalStrSlNo	=	VehicleCode+"-"+regionCode+"-"+vPrefix+newVehicleNo;	
		}
		else
		{
			finalStrSlNo	=	VehicleCode+"-"+regionCode+VehicleType+"-"+vPrefix+newVehicleNo;	
		}*/
		
		finalStrSlNo = VehicleCode+"-"+regionCode+"-"+vPrefix+newVehicleNo;	
		
		/*
		 * code to handle already issued vehicle number
		 */
		boolean vehicleNoExistsFlag = true;
		while(vehicleNoExistsFlag)
		{
			String query = "SELECT COUNT(*) rowCount FROM t_vehicle_registration_dtls a WHERE a.`Vehicle_Number`=? AND a.`Vehicle_Type_Id`=? AND a.`Region_Id`=? AND a.`Vehicle_Registration_Id`=?";
			pst = conn.prepareStatement(query);
			pst.setString(1, finalStrSlNo);
			pst.setString(2, vehicleTypeId);
			pst.setString(3, regionId);
			pst.setString(4, vehicleRegistrationId);
			rs = pst.executeQuery();
			rs.first();
			int rowCount = rs.getInt("rowCount");
			
			if(rowCount > 0)
			{
				newVehicleNo	=	String.valueOf(((new Integer(newVehicleNo)) + 1));
				int dgtCount = String.valueOf(newVehicleNo).length();
				StringBuffer sbf = new StringBuffer();
				for(int i=dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newVehicleNo);
				newVehicleNo = sbf.toString();
				
				finalStrSlNo = VehicleCode+"-"+regionCode+"-"+vPrefix+newVehicleNo;	
				continue;
			}
			else {
				break;
			}
		}
		
		dto.setNewVehicleNo(newVehicleNo);
		dto.setvPrefix(vPrefix);
		dto.setLatestVehicleNumber(finalStrSlNo);
		
		return dto;
	}
    
	private static String generateNewVehicleNumber()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastVehicleNumber(Connection conn, String VehicleCode ,String regionCode, String VehicleType) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String vehicleNumber = null;
		String prefix = "";
		
		try 
		{
			if(conn != null)
			{
				String query ="SELECT "
							+ "MAX(a.Sl_No) vehicleNumber, "
							+ "a.Vehicle_Prefix "
							+ "FROM "
							+ "t_vehicle_number_sequence a "
							+ "WHERE a.Registration_Code_Id = ? "
							+ "AND a.Vehicle_Type_Id = ? "
							+ "AND a.Region_Id = ?";
				
					pst = conn.prepareStatement(query);
					pst.setString(1, VehicleCode);
					pst.setString(2, VehicleType);
					pst.setString(3, regionCode);
					rs = pst.executeQuery();
				
				if(rs != null && rs.first())
					vehicleNumber = rs.getString("vehicleNumber");
				    prefix = rs.getString("Vehicle_Prefix");
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return vehicleNumber+'#'+prefix;
	}
	
	private static String getVehicleCode(Connection conn, String vehicleRegistrationId) throws Exception
    {
	PreparedStatement pst = null;
	ResultSet rs = null;
	String VehicleCode = null;
	
	try 
	 {
		if(conn != null)
		{
			String query = "SELECT Vehicle_Reg_Code_Name FROM t_vehicle_registration_code_master WHERE Vehicle_Registration_Id=?";
			pst = conn.prepareStatement(query);
			pst.setString(1, vehicleRegistrationId);
			rs = pst.executeQuery();
			
			if(rs != null && rs.first())
				VehicleCode = rs.getString("Vehicle_Reg_Code_Name");
		}
	} 
	catch (Exception e) 
	{
		throw new Exception();
	}
	finally
	{
		ConnectionManager.close(null, null, rs, pst);
	}
	
	return VehicleCode;
}

private static String getRegionCode1(Connection conn, String regionId) throws Exception
{
	PreparedStatement pst = null;
	ResultSet rs = null;
	String regionCode = null;
	
	try 
	{
		if(conn != null)
		{
			String query = "SELECT region_number FROM t_region_master WHERE region_id=?";
			pst = conn.prepareStatement(query);
			pst.setString(1, regionId);
			rs = pst.executeQuery();
			
			if(rs != null && rs.first())
				regionCode = rs.getString("region_number");
		}
	} 
	catch (Exception e) 
	{
		throw new Exception();
	}
	finally
	{
		ConnectionManager.close(null, null, rs, pst);
	}
	
	return regionCode;
}

	//COMMERCIAL LICENSE NO
	public static String generateCommercialLicenseNoFormat(Connection conn, String table_name) throws Exception
	{
		PreparedStatement pst = null;
		String finalStrSlNo = null;
		String newSlNo = "0";
		String lastCommercialLicenseNo = getLastCommercialLicenseNo(conn, table_name);
		
		if(null != lastCommercialLicenseNo)
		{
			String slNo = lastCommercialLicenseNo;
			if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
			{
				newSlNo = String.valueOf(((new Integer(slNo)) + 1));
				int dgtCount = String.valueOf(newSlNo).length();
				StringBuffer sbf = new StringBuffer();
				for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
					sbf.append("0");
				sbf.append(newSlNo);
				finalStrSlNo = sbf.toString();
			}
			
			pst = conn.prepareStatement(UPDATE_LATEST_SL_NO);
			pst.setString(1, newSlNo);
			pst.setString(2, "0");
			pst.setString(3, "1");
			pst.executeUpdate();
		}
		else
		{
			finalStrSlNo = generateNewCommercialLicenseNoFormat();
		}
		
		StringBuffer NewCommercialLicenseNo = new StringBuffer();
		NewCommercialLicenseNo.append("PD");
		NewCommercialLicenseNo.append("-");
		NewCommercialLicenseNo.append(finalStrSlNo);
		
		return NewCommercialLicenseNo.toString();
	}
	
	private static String generateNewCommercialLicenseNoFormat()
	{
		String finalStrSlNo = null;
		String newSlNo = "1";
		int dgtCount = String.valueOf(newSlNo).length();
		StringBuffer sbf = new StringBuffer();
		
		for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
			sbf.append("0");
		sbf.append(newSlNo);
		finalStrSlNo = sbf.toString();
		return finalStrSlNo;
	}
	
	private static String getLastCommercialLicenseNo(Connection conn, String table_name) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String DrivingLicenseNo = null;
		
		try 
		{
			if(conn != null)
			{
				String query = GET_MAX_LICENSE_SL_NO;
				pst = conn.prepareStatement(query);
				pst.setString(1, "0");
				pst.setString(2, "1");
				rs = pst.executeQuery(); 
				while(rs.next()){
					DrivingLicenseNo = rs.getString("Sl_No");
				}
			}
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return DrivingLicenseNo;
	}
	
		//TOP number generation
		public static String generateTOPNoFormat(Connection conn,String regionId) throws Exception
		{
			PreparedStatement pst = null;
			String finalStrSlNo = null, newSlNo = "0";
			String lastTOPNo = getLastTOPNo(conn, regionId);
			String regionCode = getRegionCode(conn, regionId);
			
			if(null != lastTOPNo)
			{
				String slNo = lastTOPNo;//lastTOPNo.substring(7,lastTOPNo.length());
				if(null != slNo && !"".equalsIgnoreCase(slNo) && !isStringPresent(slNo))
				{
					newSlNo = String.valueOf(((new Integer(slNo)) + 1));
					int dgtCount = String.valueOf(newSlNo).length();
					StringBuffer sbf = new StringBuffer();
					for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
						sbf.append("0");
					sbf.append(newSlNo);
					finalStrSlNo = sbf.toString();
				}
				
				pst = conn.prepareStatement(UPDATE_LATEST_SL_NO_FOR_TOP);
				pst.setString(1, newSlNo);
				pst.setString(2, regionId);
				pst.executeUpdate();
			}
			else{
				finalStrSlNo = generateNewTOPNoFormat();
			}
			
			StringBuffer NewtopNoFormat = new StringBuffer();
			NewtopNoFormat.append("TOP");
			NewtopNoFormat.append("/");
			NewtopNoFormat.append(regionCode);
			NewtopNoFormat.append("R");
			NewtopNoFormat.append("-");
			NewtopNoFormat.append(finalStrSlNo);
			
			return NewtopNoFormat.toString();
		}
		private static String generateNewTOPNoFormat()
		{
			String finalStrSlNo = null;
			String newSlNo = "1";
			int dgtCount = String.valueOf(newSlNo).length();
			StringBuffer sbf = new StringBuffer();
			
			for(int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
				sbf.append("0");
			sbf.append(newSlNo);
			finalStrSlNo = sbf.toString();
			return finalStrSlNo;
		}
		private static String getLastTOPNo(Connection conn, String regionId) throws Exception
		{
			PreparedStatement pst = null;
			ResultSet rs = null;
			String TOP_Number = null;
			
			try 
			{
				if(conn != null)
				{
					String query = "SELECT Sequence FROM t_top_sequence WHERE Region_Id=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, regionId);
					rs = pst.executeQuery(); 
					while(rs.next()){
						TOP_Number = rs.getString("Sequence");
					}
				}
			}
			catch (Exception e) 
			{
				throw new Exception();
			}
			finally
			{
				ConnectionManager.close(null, null, rs, pst);
			}
			
			return TOP_Number;
		}

		private static final String GET_MAX_LICENSE_SL_NO = "SELECT `Sl_No` FROM `t_license_sequence_constant` a WHERE a.`Region_Id` = ? AND a.`License_Type_Id` = ?";

		private static final String UPDATE_LATEST_SL_NO = "UPDATE `t_license_sequence_constant` a SET a.`Sl_No`=? WHERE a.`Region_Id`=? AND a.`License_Type_Id`=?";
		
		private static final String UPDATE_LATEST_SL_NO_FOR_TOP = "UPDATE `t_top_sequence` a SET a.`Sequence`=? WHERE a.`Region_Id`=?";
}
