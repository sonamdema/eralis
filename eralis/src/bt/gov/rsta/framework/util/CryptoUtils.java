package bt.gov.rsta.framework.util;

/**
 * 
 * @author Poojan Sharma
 * @date Apr 02, 2012
 * PasswordEncryption.java
 * @address NGN Technologies Private Limited, Thimphu
 */
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;
import sun.misc.BASE64Encoder;

public class CryptoUtils 
{
		/*
		 * This method encrypts a plain string into hashValues
		 * for the algorithm and the encoding which the user
		 * has specified.
		 * @param String - plainText
		 * @param String - algorithm
		 * @param String - encoding
		 * @return String - hashValue
		 */
	    public synchronized String encrypt(String plaintext,String algorithm, String encoding) throws ERALISException, ERALISSystemException 
	    {
	    	
	        MessageDigest msgDigest = null;
	        String hashValue = null;
	        try 
	        {
	            msgDigest = MessageDigest.getInstance(algorithm);
	            msgDigest.update(plaintext.getBytes(encoding));
	            byte rawByte[] = msgDigest.digest();
	            hashValue = (new BASE64Encoder()).encode(rawByte);
	 
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            Log.debug("No Such Algorithm Exists");
	        } 
	        catch (UnsupportedEncodingException e) 
	        {
	            Log.debug("The Encoding Is Not Supported");
	        }
	        return "{SHA}"+hashValue;
	    }
}
