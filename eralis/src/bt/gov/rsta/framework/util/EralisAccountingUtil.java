package bt.gov.rsta.framework.util;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import bt.gov.rsta.eralis.dto.payment.PaymentDTO;

public class EralisAccountingUtil 
{
	public static PaymentDTO calculateVehicleRegistrationAmount(double amount, Date purchaseDate, double penaltyPerDay, double maxPenalty, int validity) throws Exception
	{
		PaymentDTO dto = new PaymentDTO();
		double actualPayableAmount = amount;
		double penalty = 0.0;
		
		// Create an instance of SimpleDateFormat used for formatting 
		// the string representation of date (yyyy-MM-dd HH:mm:ss)
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date today = new Date(System.currentTimeMillis());
		
		String purchaseDateStr = formatter.format(purchaseDate);
		String todayStr = formatter.format(today);
		
		int dayDiff = EralisCommonUtil.dateDifferenceAsInt(purchaseDateStr, todayStr, "D");
		
		if(dayDiff > validity)
		{
			int liablePenaltyDays = dayDiff - validity;
			penalty = liablePenaltyDays * penaltyPerDay;
			
			//if(penalty > maxPenalty)
			//	penalty = maxPenalty;
		}
		
		actualPayableAmount = amount + penalty;
		
		dto.setTotalAmount(Double.toString(actualPayableAmount));
		dto.setAmount(Double.toString(amount));
		dto.setPenalty(Double.toString(penalty));
		
		return dto;
	}
	
	/**
	 * Calculates the amount payable along with penalty if any
	 * 
	 * @param renewalAmount
	 * @param penaltyPerday
	 * @param maxPenalty
	 * @param expiryDate - format yyyy-MM-dd HH:mm:ss
	 * @param renewalDuration
	 * @return double actualPayableAmount
	 */
	
	public static PaymentDTO calculatePayableAmount(double amount,double penaltyPerDay, double maxPenalty, Date expiryDate, int durationInMonths) throws Exception
	{
		
		PaymentDTO dto = new PaymentDTO();
		double actualPayableAmount = amount;
		double penalty = 0.0;
		
		
		// Create an instance of SimpleDateFormat used for formatting 
		// the string representation of date (yyyy-MM-dd HH:mm:ss)
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date today = new Date(System.currentTimeMillis());

	    Date today=new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-28");  

		int dateDiff = today.compareTo(expiryDate);
		
		
		if(dateDiff == 1)
		{
			String expiryDateStr = formatter.format(expiryDate);
			String todayStr = formatter.format(today);
			
			int dayDiff = EralisCommonUtil.dateDifferenceAsInt(expiryDateStr, todayStr, "D");
			
			penalty = dayDiff * penaltyPerDay;
			
			if(penalty > maxPenalty)
				penalty = maxPenalty;
			
			int monthDiff = EralisCommonUtil.dateDifferenceAsInt(expiryDateStr, todayStr, "M");
			float noOfAspectedRenewals = (monthDiff/durationInMonths)+1;
			
			if(noOfAspectedRenewals < 1)
				noOfAspectedRenewals = 1;
			
			//actualPayableAmount = (noOfAspectedRenewals * amount) + penalty;
			actualPayableAmount = (amount) + penalty;
		}
		
		dto.setTotalAmount(Double.toString(actualPayableAmount));
		dto.setAmount(Double.toString(amount));
		dto.setPenalty(Double.toString(penalty));
		
		return dto;
	}
	
	public static PaymentDTO calculateRWCPayableAmount(double amount,double penaltyPerDay, double maxPenalty, Date expiryDate, int durationInMonths) throws Exception
	{
		PaymentDTO dto = new PaymentDTO();
		double actualPayableAmount = amount;
		double penalty = 0.0;
		
		// Create an instance of SimpleDateFormat used for formatting 
		// the string representation of date (yyyy-MM-dd HH:mm:ss)
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date today = new Date(System.currentTimeMillis());

	    Date today=new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-28");  
		
		int dateDiff = today.compareTo(expiryDate);
		
		if(dateDiff == 1)
		{
			String expiryDateStr = formatter.format(expiryDate);
			String todayStr = formatter.format(today);
			
			int dayDiff = EralisCommonUtil.dateDifferenceAsInt(expiryDateStr, todayStr, "D");
			
			penalty = dayDiff * penaltyPerDay;
			
			if(penalty > maxPenalty)
				penalty = maxPenalty;
			
			int monthDiff = EralisCommonUtil.dateDifferenceAsInt(expiryDateStr, todayStr, "M");
			float noOfAspectedRenewals = (monthDiff/durationInMonths)+1;
			
			if(noOfAspectedRenewals < 1)
				noOfAspectedRenewals = 1;
			
			actualPayableAmount = amount + penalty;
		}
		
		dto.setTotalAmount(Double.toString(actualPayableAmount));
		dto.setAmount(Double.toString(amount));
		dto.setPenalty(Double.toString(penalty));
		
		return dto;
	}
	
	/**
	 * Calculates the amount payable for vehicle transfer
	 * 
	 * @param initialRegistrationDate
	 * @param initialPurchasePrice
	 * @param transferTax
	 * @param saleDeedAmount
	 * @return double transferPayableAmount
	 */
	public static PaymentDTO calculatePayableTransferAmount(double transferFees, Date initialRegistrationDate, double initialPurchasePrice, int transferTax, double saleDeedAmount, Date saleDeedDate, int transferGracePeriod, double penaltyPerDay, double maxPenalty) throws Exception
	{
		SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
		
		PaymentDTO dto = new PaymentDTO();
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date today = new Date(System.currentTimeMillis());

	    Date today=new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-28");  
		String todayStr = formatter.format(today);
		
		double actualDepreciation = 0.0, marketValue = 0.0, penalty = 0.0;
		int noOfDays = 0, yearIncrementor = 0, noOfDaysSubtractor = 0;
		
		String saleDeedDateStr = formatter.format(saleDeedDate);
		String initialRegDateStr = formatter.format(initialRegistrationDate);
		
		double depreciationRate1 = Integer.parseInt(EralisCommonUtil.getNotificationProperty("vehicle-transfer-depreciation-rate-for-below-two-years"));
		double depreciationRate2 = Integer.parseInt(EralisCommonUtil.getNotificationProperty("vehicle-transfer-depreciation-rate-for-three-six-years"));
		//double depreciationRate3 = Integer.parseInt(EralisCommonUtil.getNotificationProperty("vehicle-transfer-depreciation-rate-for-above-six-years"));
		
		int vehicleAge = EralisCommonUtil.dateDifferenceAsInt(initialRegDateStr, saleDeedDateStr, "D");
		int yearDiff = EralisCommonUtil.dateDifferenceAsInt(initialRegDateStr, saleDeedDateStr, "Y");
		
		for(int i = 0; i <= yearDiff; i++)
		{
			if(i == 0){
				yearIncrementor = 365;
			}
			else 
			{
				yearIncrementor += 365;
				
				if(i == 1)
					noOfDaysSubtractor = 365;
				else
					noOfDaysSubtractor += 365;
			}
			
			if(vehicleAge > yearIncrementor){
				noOfDays = 365;
			}
			else {
				noOfDays = vehicleAge - noOfDaysSubtractor;
			}
			
			if(noOfDays >= 0)
			{
				if(i == 0)
				{
					actualDepreciation = (initialPurchasePrice * (depreciationRate1/100) * noOfDays)/365;
					marketValue = initialPurchasePrice - actualDepreciation;
				}
				else if(i == 1)
				{
					actualDepreciation = (marketValue * (depreciationRate1/100) * noOfDays)/365;
					marketValue = marketValue - actualDepreciation;
				}
				else if(i >= 2 && i <= 5)
				{
					actualDepreciation = (marketValue * (depreciationRate2/100) * noOfDays)/365;
					marketValue = marketValue - actualDepreciation;
				}
				else
				{
					actualDepreciation = 0.9 * initialPurchasePrice;
					double tempMarketValue = marketValue;
					
					if(vehicleAge < 2190)
						marketValue = tempMarketValue;
					else
						marketValue = initialPurchasePrice - actualDepreciation;
					
					break;
				}
			}
		}
		
		double actualMarketValue = marketValue, valueForTT = 0.0;
		
		if(actualMarketValue > saleDeedAmount)
			valueForTT = actualMarketValue;
		else
			valueForTT = saleDeedAmount;
		
		double transferTaxDbl = (double)transferTax/100;
		double taxedAmount = valueForTT * transferTaxDbl;
		double payableAmount = taxedAmount;
		
		
		int dayDiff = EralisCommonUtil.dateDifferenceAsInt(saleDeedDateStr, todayStr, "D");
		
		if(dayDiff > transferGracePeriod)
		{
			int liablePenaltyDays = dayDiff - transferGracePeriod;
			penalty = liablePenaltyDays * penaltyPerDay;
			
			if(penalty > maxPenalty)
				penalty = maxPenalty;
		}
		
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		
		java.util.Date initialRegDate = targetSdf.parse(initialRegDateStr);
		String initialRegistrationDateStr = sourceSdf.format(initialRegDate);
		
		dto.setInitialPurchasePrice(Double.toString(initialPurchasePrice));
		dto.setRegistrationDate(initialRegistrationDateStr);
		dto.setAmount(numberFormat.format(payableAmount+transferFees));
		dto.setPenalty(Double.toString(penalty));
		dto.setTotalAmount(numberFormat.format(payableAmount + penalty + transferFees));
		dto.setMarketValue(numberFormat.format(marketValue));
		dto.setValueForTT(numberFormat.format(valueForTT));
		String taxableAmount = Double.toString((((Double) Math.floor(taxedAmount ) ).intValue()));
		dto.setTaxedAmount(taxableAmount);
		dto.setTransferFees(numberFormat.format(transferFees));
		dto.setSaleDeedAmount(numberFormat.format(saleDeedAmount));
		dto.setTotalAmount(numberFormat.format((((Double) Math.floor( payableAmount ) ).intValue()) + penalty + transferFees));
 		
		return dto;
	}
	
	/**
	 * Calculates the amount payable for vehicle transfer
	 * 
	 * @param initialRegistrationDate
	 * @param initialPurchasePrice
	 * @param transferTax
	 * @param saleDeedAmount
	 * @return double transferPayableAmount
	 */
	public static PaymentDTO calculatePayableTransferAmountForFamilyMember(double transferFees, Date initialRegistrationDate, double initialPurchasePrice, int transferTax, double saleDeedAmount, Date saleDeedDate, int transferGracePeriod, double penaltyPerDay, double maxPenalty) throws Exception
	{
		SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
		
		PaymentDTO dto = new PaymentDTO();
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date today = new Date(System.currentTimeMillis());

	    Date today=new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-28");  
		String todayStr = formatter.format(today);
		
		double marketValue = 0.0, penalty = 0.0;
		
		String saleDeedDateStr = formatter.format(saleDeedDate);
		String initialRegDateStr = formatter.format(initialRegistrationDate);
		
		int dayDiff = EralisCommonUtil.dateDifferenceAsInt(saleDeedDateStr, todayStr, "D");
		
		if(dayDiff > transferGracePeriod)
		{
			int liablePenaltyDays = dayDiff - transferGracePeriod;
			penalty = liablePenaltyDays * penaltyPerDay;
			
			if(penalty > maxPenalty)
				penalty = maxPenalty;
		}
		
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		
		java.util.Date initialRegDate = targetSdf.parse(initialRegDateStr);
		String initialRegistrationDateStr = sourceSdf.format(initialRegDate);
		
		dto.setInitialPurchasePrice(Double.toString(initialPurchasePrice));
		dto.setRegistrationDate(initialRegistrationDateStr);
		dto.setAmount(numberFormat.format(transferFees));
		dto.setPenalty(Double.toString(penalty));
		dto.setTotalAmount(numberFormat.format(penalty + transferFees));
		dto.setMarketValue(numberFormat.format(marketValue));
		dto.setValueForTT("0.00");
		String taxableAmount = "0.00";
		dto.setTaxedAmount(taxableAmount);
		dto.setTransferFees(numberFormat.format(transferFees));
		dto.setSaleDeedAmount(numberFormat.format(saleDeedAmount));
		dto.setTotalAmount(numberFormat.format(penalty + transferFees));
 		
		return dto;
	}
	
	/**
	 * Calculates the amount payable for vehicle transfer
	 * 
	 * @param initialRegistrationDate
	 * @param initialPurchasePrice
	 * @param transferTax
	 * @param saleDeedAmount
	 * @return double transferPayableAmount
	 */
	public static PaymentDTO calculatePayableTransferAmountForAuction(double transferFees, Date initialRegistrationDate, double initialPurchasePrice, int transferTax, double saleDeedAmount, Date saleDeedDate, int transferGracePeriod, double penaltyPerDay, double maxPenalty) throws Exception
	{
		SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
		
		PaymentDTO dto = new PaymentDTO();
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date today = new Date(System.currentTimeMillis());

	    Date today=new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-28");  
		String todayStr = formatter.format(today);
		
		double marketValue = 0.0, penalty = 0.0;
		
		String saleDeedDateStr = formatter.format(saleDeedDate);
		String initialRegDateStr = formatter.format(initialRegistrationDate);
		
		//double transferTaxDbl = (double)5/100;
		double transferTaxDbl = (double)transferTax/100;
		double taxedAmount = saleDeedAmount * transferTaxDbl;
		double payableAmount = taxedAmount;
		
		
		int dayDiff = EralisCommonUtil.dateDifferenceAsInt(saleDeedDateStr, todayStr, "D");
		
		if(dayDiff > transferGracePeriod)
		{
			int liablePenaltyDays = dayDiff - transferGracePeriod;
			penalty = liablePenaltyDays * penaltyPerDay;
			
			if(penalty > maxPenalty)
				penalty = maxPenalty;
		}
		
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		
		java.util.Date initialRegDate = targetSdf.parse(initialRegDateStr);
		String initialRegistrationDateStr = sourceSdf.format(initialRegDate);
		
		dto.setInitialPurchasePrice(Double.toString(initialPurchasePrice));
		dto.setRegistrationDate(initialRegistrationDateStr);
		dto.setAmount(numberFormat.format(payableAmount+transferFees));
		dto.setPenalty(Double.toString(penalty));
		dto.setTotalAmount(numberFormat.format(payableAmount + penalty + transferFees));
		dto.setMarketValue(numberFormat.format(marketValue));
		dto.setValueForTT("0.00");
		String taxableAmount = Double.toString((((Double) Math.floor(taxedAmount ) ).intValue()));
		dto.setTaxedAmount(taxableAmount);
		dto.setTransferFees(numberFormat.format(transferFees));
		dto.setSaleDeedAmount(numberFormat.format(saleDeedAmount));
		dto.setTotalAmount(numberFormat.format((((Double) Math.floor( payableAmount ) ).intValue()) + penalty + transferFees));
 		
		return dto;
	}
	
	/*public static void main(String[] args) throws Exception 
	{
		String initialRegDateStr = "2013-01-01";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date initialRegDate = format.parse(initialRegDateStr);
		int initialPurchasePrice = 300000;
		int transferTa x = 5;
		int saleDeedPrice = 150000;
		
		double payableAmount = EralisAccountingUtil.calculatePayableTransferAmount(initialRegDate, initialPurchasePrice, transferTax, saleDeedPrice);
		System.out.println(payableAmount); 
	}*/
	
	/*public static void main(String[] args) throws Exception 
	{
		double renewalAmount = 17000;
		double penaltyPerDay = 10;
		double maxPenalty = 3000;
		String expiryDateStr = "2014-01-01";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date expiryDate = formatter.parse(expiryDateStr);
		int renewalDuration = 12;
		
		double payable = EralisAccountingUtil.calculatePayableAmount(renewalAmount, penaltyPerDay, maxPenalty, expiryDate, renewalDuration);
		System.out.println(payable);
	}*/
	
	public static void main(String[] args) throws Exception 
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		double initialPurchasePrice =  230000;
		String initialRegDateStr = "2001-01-26";
		String saleDeedDateStr = "2016-07-20";
		double saleDeedAmount =  30000.00;
		int transferTax = 5;
		int transferGracePeriod = 15;
		int penaltyPerDay = 100;
		int maxPenalty = 3000;
		double transferFees = 500+500+60;
		Date initialRegistrationDate = formatter.parse(initialRegDateStr);
		Date saleDeedDate = formatter.parse(saleDeedDateStr);
		
		PaymentDTO dto = EralisAccountingUtil.calculatePayableTransferAmount(transferFees, initialRegistrationDate, initialPurchasePrice, 
				transferTax, saleDeedAmount, saleDeedDate, transferGracePeriod, penaltyPerDay, maxPenalty);
		
		System.out.println("Amount: "+dto.getAmount());
		System.out.println("Penalty: "+dto.getPenalty());
		System.out.println("Total Amount: "+dto.getTotalAmount());
		
	}
	
}
