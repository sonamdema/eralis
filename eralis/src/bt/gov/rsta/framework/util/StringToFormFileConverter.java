package bt.gov.rsta.framework.util;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.struts.upload.FormFile;

public class StringToFormFileConverter implements Converter
{
	static 
	{
		Log.info("registering StringToFormFileConverter");
		ConvertUtils.register(new StringToFormFileConverter(), FormFile.class);
	}

	public StringToFormFileConverter() 
	{
		
	}

	public Object convert(Class type, Object value) 
	{
		if (value == null)
			return null;
		if (value instanceof String)
			return null;
		if (value instanceof FormFile)
			return value;
		else
			throw new ConversionException("Cannot convert " + type + " to FormFile");
	}
}
