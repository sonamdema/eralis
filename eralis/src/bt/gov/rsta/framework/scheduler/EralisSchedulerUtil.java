package bt.gov.rsta.framework.scheduler;

import bt.gov.rsta.framework.business.EralisSchedulerBusiness;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class EralisSchedulerUtil 
{
	public void sendVehicleRenewalNotification() throws ERALISException, ERALISSystemException{
		EralisSchedulerBusiness.getInstance().sendVehicleRenewalNotification();
	}
	
	public void sendLicenseRenewalNotification()throws ERALISException, ERALISSystemException{
		EralisSchedulerBusiness.getInstance().sendLicenseRenewalNotification();
	}
	
	public void sendTOPRenewalNotification() throws ERALISException, ERALISSystemException {
		EralisSchedulerBusiness.getInstance().sendTOPRenewalNotification();
	}
	
	public void revokeSuspensionPeriod() throws ERALISException, ERALISSystemException {
		EralisSchedulerBusiness.getInstance().revokeSuspensionPeriod();
	}
	
	public void dispatchLicenseApplication() throws ERALISException, ERALISSystemException {
		EralisSchedulerBusiness.getInstance().dispatchLicenseApplication();
	}
	
	public void sendVehicleOutstandingJob() throws ERALISException, ERALISSystemException {
		EralisSchedulerBusiness.getInstance().sendVehicleOutstandingJob();
	}
	
	public void cancelOutstandingVehicleJob() throws ERALISException, ERALISSystemException {
		EralisSchedulerBusiness.getInstance().cancelOutstandingVehicleJob();
	}
}
