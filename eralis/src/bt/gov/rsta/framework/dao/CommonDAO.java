package bt.gov.rsta.framework.dao;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;

import bt.gov.rsta.eralis.dto.DuplicateDTO;
import bt.gov.rsta.eralis.dto.administration.UserDTO;
import bt.gov.rsta.eralis.dto.common.DailyTaskUserDTO;
import bt.gov.rsta.eralis.dto.common.PrintDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.faq.FaqDTO;
import bt.gov.rsta.eralis.dto.license.DriveTypeDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.master.MasterDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusTrailDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.dto.token.TokenCounterDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.eralis.util.MasterSQLConstants;
import bt.gov.rsta.eralis.util.PaymentSQLConstant;
import bt.gov.rsta.framework.dto.AlertDTO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.dto.PagePriviledge;
import bt.gov.rsta.framework.dto.Priviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.dto.Service;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.ldapconfig.LdapPasswordUpdate;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisAccountingUtil;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.WorkflowManager;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.MenuVO;
import bt.gov.rsta.framework.vo.SubMenuVO;
import bt.gov.rsta.framework.vo.ThirdLevelMenuVo;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.vo.WorkflowDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class CommonDAO 
{
	private static CommonDAO commonDao;
	private static int duration = 0;
	private static int validity = 0;
	private static int applicationAge = 0;
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date(System.currentTimeMillis());

	public static CommonDAO getInstance() {
		if (commonDao == null) {
			commonDao = new CommonDAO();
		}
		return commonDao;
	}

	public String insertErrorLog(String errorDetails, String owner) {
		String errorCode = "0";
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = ConnectionManager.getConnection();
			if (null != connection) {
				connection.setAutoCommit(false);
				prepareStatement = connection.prepareStatement(INSERT_INTO_T_ERROR_DETAILS,PreparedStatement.RETURN_GENERATED_KEYS);
				prepareStatement.setString(1, errorDetails);
				prepareStatement.setString(2, owner);

				prepareStatement.executeUpdate();

				resultSet = prepareStatement.getGeneratedKeys();
				while (resultSet.next()) {
					errorCode = resultSet.getString(1);
				}
			} else {
				Log.error("At CommonDAO.insertErrorLog(): connection can not be null");
			}

			connection.commit();
			connection.setAutoCommit(true);
		} catch (Exception e) {
			Log.error("At CommonDAO.insertErrorLog(): Exception is - > ");
			Log.error(EralisCommonUtil.getStackTraceAsString(e), null);
			try {
				if (null != connection)
					connection.rollback();
			} catch (Exception ex) {
				Log.error("At CommonDAO.insertErrorLog(): Exception is - > ");
				Log.error(EralisCommonUtil.getStackTraceAsString(ex), null);
			}
		} finally {
			ConnectionManager.close(connection, null, resultSet,prepareStatement);
		}
		return errorCode;
	}

	public String changeUserAccountInfo(String identifier,ApplicationDataVO appVO) throws ERALISException,ERALISSystemException, SQLException {
		Connection conn = null;
		PreparedStatement pst = null;

		String result = "FAILURE", query = null;

		if (identifier.equalsIgnoreCase("CHANGE_EMAIL"))
			query = UPDATE_USER_EMAIL_ADDRESS;
		else if (identifier.equalsIgnoreCase("CHANGE_MOBILE_NUMBER"))
			query = UPDATE_USER_MOBILE_NUMBER;

		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(query);

			if (identifier.equalsIgnoreCase("CHANGE_EMAIL")) {
				pst.setString(1, appVO.getEmail());
				pst.setString(2, appVO.getUid());
			} else if (identifier.equalsIgnoreCase("CHANGE_MOBILE_NUMBER")) {
				pst.setString(1, appVO.getMobileNumber());
				pst.setString(2, appVO.getUid());
			}

			int count = pst.executeUpdate();

			if (count > 0)
				result = "SUCCESS";
		} catch (Exception e) {
			result = "FAILURE";
			conn.rollback();
			throw new ERALISException("#Error at CommonDAO[changeUserAccountInfo]: exception:: "+ e);
		} finally {
			ConnectionManager.close(conn, null, null, pst);
		}
		return result;
	}
	
	public byte[] downloadFile(String uuid, String filename, HttpServletResponse response) throws Exception 
	{
		Connection connection = ConnectionManager.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try 
		{
			pst = connection.prepareStatement(GET_FILE_PATH);
			pst.setString(1, uuid);
			rs = pst.executeQuery();
			rs.first();
			String filePath = rs.getString("Document_Path");
			File f = new File (filePath);
			
			response.setHeader("Content-Disposition", "inline; filename=\""+filename+"\"");
		    InputStream in = new FileInputStream(f);

		    int bit = 256;
		    try 
		    {
			    while ((bit) >= 0) 
			    {
			    	bit = in.read();
			    	out.write(bit);
			    }
		    } 
		    catch (IOException ioe) 
		    {
		    	ioe.printStackTrace(System.out);
	    	}
		} 
		catch (Exception e) {
		} 
		finally {
			ConnectionManager.close(connection, null, rs, pst);
		}
		return out.toByteArray();
	}
	public EralisUserRolePriviledge populateUserRolePriviledgeHierarchy(EralisUserRolePriviledge userRolePriv) throws ERALISException,ERALISSystemException 
	{
		Log.info("### Entering CommonDAO[populateUserRolePriviledgeHierarchy] for user: "+ userRolePriv.getUserId());
		Connection conn = null;
		PreparedStatement pst = null, pst1 = null, pst2 = null, pst3 = null;
		ResultSet rs = null, rs1 = null, rs2 = null, rs3 = null;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_USER_EXISTS);
				pst.setString(1, userRolePriv.getUserId());
				rs = pst.executeQuery();
				rs.first();
				int count = rs.getInt("rowCount");
				if (count > 0) {
					pst1 = conn.prepareStatement(GET_ALL_ROLES_FOR_UID);
					pst1.setString(1, userRolePriv.getUserId());
					rs1 = pst1.executeQuery();

					List<Role> roles = new ArrayList<Role>();
					while (rs1.next()) {
						Role role = new Role();
						role.setRoleId(rs1.getString("role_id"));
						role.setRoleName(rs1.getString("role_name"));
						role.setRoleCode(rs1.getString("role_short_desc"));
						pst2 = conn.prepareStatement(GET_ALL_SERVICES_FOR_ROLE);
						pst2.setInt(1, Integer.parseInt(role.getRoleId()));
						rs2 = pst2.executeQuery();
						List<Service> services = new ArrayList<Service>();

						while (rs2.next()) {
							Service service = new Service();
							service.setServiceId(rs2.getString("Service_Id"));
							service.setServiceName(rs2.getString("Service_Name"));
							service.setServiceCode(rs2.getString("Service_Short_Desc"));
							pst3 = conn.prepareStatement(GET_ALL_PRIVILEGES_FOR_SERVICE);
							pst3.setInt(1, Integer.parseInt(role.getRoleId()));
							pst3.setInt(2,Integer.parseInt(service.getServiceId()));
							rs3 = pst3.executeQuery();

							List<Priviledge> privileges = new ArrayList<Priviledge>();
							while (rs3.next()) {
								Priviledge privilege = new Priviledge();
								privilege.setPriviledgeId(rs3.getString("Priv_Id"));
								privilege.setPriviledgeName(rs3.getString("Priv_Name"));
								privilege.setPriviledgeCode(rs3.getString("Priv_Short_Desc"));
								privileges.add(privilege);
							}
							service.setPriviledges(privileges.toArray(new Priviledge[privileges.size()]));
							services.add(service);
						}
						role.setServices(services.toArray(new Service[services.size()]));
						roles.add(role);
					}

					userRolePriv.setRoles(roles.toArray(new Role[roles.size()]));

					if (roles != null && roles.size() == 1) {
						userRolePriv.setCurrentRole(roles.get(0));
					}

					pst = conn.prepareStatement(GET_USER_DETAILS);
					pst.setString(1, userRolePriv.getUserId());
					rs = pst.executeQuery();
					rs.first();

					userRolePriv.setUserName(rs.getString("name"));
					userRolePriv.setEmailId(rs.getString("email_id"));
					userRolePriv.setMobileNumber(rs.getString("mobile_number"));
					userRolePriv.setIsActive(rs.getString("is_active"));
					userRolePriv.setLastLoginDate(rs.getString("last_login_date"));
					userRolePriv.setShowTaskList(rs.getString("show_tasklist"));
					userRolePriv.setAgencyCode(rs.getString("agency_code"));

					pst = conn.prepareStatement(GET_JURISDICTION_FOR_USERID);
					pst.setString(1, userRolePriv.getUserId());
					rs = pst.executeQuery();
					while (rs.next()) {
						userRolePriv.setJurisdictionId(rs.getString("jurisdiction_id"));
						userRolePriv.setJurisdictionTypeId(rs.getString("juris_type_id"));
						userRolePriv.setRegionId(rs.getString("region_id"));
						userRolePriv.setRegionName(rs.getString("regionName"));
						userRolePriv.setBaseId(rs.getString("base_office_id"));
						userRolePriv.setBaseName(rs.getString("baseName"));
					}
				}
			}
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[populateUserRolePriviledgeHierarchy]: exception:: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
			ConnectionManager.close(null, null, rs1, pst1);
			ConnectionManager.close(null, null, rs2, pst2);
			ConnectionManager.close(null, null, rs3, pst3);
		}
		Log.info("### Exiting CommonDAO[populateUserRolePriviledgeHierarchy] for user: "+ userRolePriv.getUserId());
		return userRolePriv;
	}
	public PaymentDTO calculatePayableAmount(PaymentDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dto.setStatus("FAILURE");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				/*
				 * This section will calculate the payment to be made for all
				 * the learner related transactions
				 */
				if (dto.getRequestType().equalsIgnoreCase("LEARNER")) {
					pst = conn.prepareStatement(PaymentSQLConstant.GET_LICENSE_PAYMENT_DETAILS);
					pst.setString(1, dto.getRequestType());
					pst.setString(2, dto.getServiceType());
					pst.setString(3, dto.getIdentityTypeId());
					rs = pst.executeQuery();
					rs.first();
					double serviceFee = rs.getDouble("Amount");
					double cardCost = rs.getDouble("License_Card_Cost");
					if (dto.getServiceType().equalsIgnoreCase("RENEWAL")) {
						pst = conn.prepareStatement(PaymentSQLConstant.GET_LAST_LEARNER_EXPIRY_DATE);
						pst.setString(1, dto.getIdentityNo());
						pst.setString(2, dto.getIdentityNo());
						pst.setString(3, dto.getIdentityNo());
						rs = pst.executeQuery();
						rs.first();
						Date expiryDate = rs.getDate("expiryDate");
						int durationInMonths = 0;
						Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date today = new Date(System.currentTimeMillis());
						String expiryDateStr = formatter.format(expiryDate);
						String todayStr = formatter.format(today);
						int dateDiff = today.compareTo(expiryDate);
						/*if (dateDiff == 1) {
							int yearDiff = EralisCommonUtil.dateDifferenceAsInt(expiryDateStr,todayStr, "Y");
							serviceFee = yearDiff * serviceFee;
						}*/
						durationInMonths	=	Integer.parseInt(dto.getRenewalDuration());
						int noOfYears = Integer.parseInt(dto.getRenewalDuration()) / 12;
						serviceFee = (noOfYears * serviceFee);
					}
					double totalAmount = serviceFee + cardCost;
					dto.setAmount(Double.toString(serviceFee));
					dto.setTotalAmount(Double.toString(totalAmount));
					dto.setPenaltyFlag("FALSE");
					dto.setStatus("SUCCESS");
				} 
				else if (dto.getRequestType().equalsIgnoreCase("LICENSE"))
				{
					pst = conn.prepareStatement(PaymentSQLConstant.GET_LICENSE_PAYMENT_DETAILS);
					pst.setString(1, dto.getRequestType());
					pst.setString(2, dto.getServiceType());
					pst.setString(3, dto.getIdentityTypeId());
					rs = pst.executeQuery();
					rs.first();
					System.out.println(rs.getDouble("Amount"));
					double serviceFee = rs.getDouble("Amount");
					double cardCost = rs.getDouble("License_Card_Cost");
					double totalAmount = serviceFee + cardCost;

					int durationInMonths = 0;
					if (dto.getIdentityTypeId().equals("N"))
						durationInMonths = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-ordinary-license"));
					else
						durationInMonths = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-commercial-driving-license"));

					//String renewalDuration	=	dto.getRenewalDuration();
					/*if(!renewalDuration.equalsIgnoreCase("") || !renewalDuration.equalsIgnoreCase("undefined"))
					{
						durationInMonths	=	Integer.parseInt(renewalDuration);
					}*/
					if (dto.getServiceType().equalsIgnoreCase("RENEWAL")) 
					{

						double noOfYears	=	0;
						double renewalAmount	=	0;
						String[] duration	=	dto.getRenewalDuration().split("_");
						if(duration[1].equalsIgnoreCase("month"))
						{
							durationInMonths	=	Integer.parseInt(duration[0]);
							noOfYears =  Double.parseDouble(duration[0]) / 12;
						}
						else if(duration[1].equalsIgnoreCase("days"))
						{
							durationInMonths	=	Integer.parseInt(duration[0]);
							noOfYears =  Double.parseDouble(duration[0]) / 365;
						}
						totalAmount = Math.ceil((noOfYears * serviceFee) + cardCost);
						renewalAmount	=	Math.ceil(noOfYears * serviceFee);
						pst = conn.prepareStatement(PaymentSQLConstant.GET_PENALTY_DETAILS);
						pst.setString(1, dto.getRequestType());
						pst.setString(2, dto.getServiceType());
						rs = pst.executeQuery();
						rs.first();
						double ODLRenewalAmount = 00;
						double PDRenewalAmount = 00;
						double penaltyPerDay = rs.getDouble("Penalty_Per_Day");
						double maxPenalty = rs.getDouble("Max_Penalty");

						pst = conn.prepareStatement(PaymentSQLConstant.GET_MAX_EXPIRY_FOR_LICENSE);
						pst.setString(1, dto.getIdentityNo());
						pst.setString(2, dto.getIdentityNo());
						pst.setString(3, dto.getIdentityNo());
						rs = pst.executeQuery();
						rs.first();
						String stringExpireDate = rs.getString("expiryDate");
						Date expiryDate = rs.getDate("expiryDate");
						if (dto.getIdentityTypeId().equals("C"))
						{
							pst = conn.prepareStatement(PaymentSQLConstant.GET_CUSTOMER_ID);
							pst.setString(1, dto.getIdentityNo());
							rs = pst.executeQuery();
							rs.first();
							String customerId = rs.getString("Customer_Id");
							pst = conn.prepareStatement(PaymentSQLConstant.GET_ORDINARY_LICENSE_ID);
							pst.setString(1, customerId);
							pst.setString(2, dto.getIdentityNo());
							rs = pst.executeQuery();
							rs.first();
							String ordinaryLicenseId = rs.getString("Driving_License_Id");
							pst = conn.prepareStatement(PaymentSQLConstant.GET_MAX_EXPIRY_FOR_LICENSE);
							pst.setString(1, ordinaryLicenseId);
							pst.setString(2, ordinaryLicenseId);
							pst.setString(3, ordinaryLicenseId);
							rs = pst.executeQuery();
							rs.first();
							java.util.Date ordinaryExpiryDate = rs.getDate("expiryDate");
							int dayDiff = ordinaryExpiryDate.compareTo(expiryDate);
							String odStringExpiry= rs.getString("expiryDate");
							
							
							int yearDiff = EralisCommonUtil.dateDifferenceAsInt( stringExpireDate,odStringExpiry, "Y");
							/*if(dayDiff < 1)
							{
								ODLRenewalAmount = 300;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							} */

							if(yearDiff<=0)
							{
								ODLRenewalAmount = 300;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							}
							else if(yearDiff==1)
							{
								ODLRenewalAmount = 200;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							}
							else if(yearDiff==2)
							{
								ODLRenewalAmount = 100;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							}
							else
							{
								PDRenewalAmount	= renewalAmount;
							}
							/*else if(yearDiff<0)
							{
								ODLRenewalAmount = 300;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							}
							else if(yearDiff==-2)
							{
								ODLRenewalAmount = 500;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							}
							else if(yearDiff<-2)
							{
								ODLRenewalAmount = 500;
								totalAmount = totalAmount + ODLRenewalAmount;
								PDRenewalAmount	= renewalAmount;
							}*/
						}
						else
						{
							ODLRenewalAmount = renewalAmount;
						}
						dto = EralisAccountingUtil.calculatePayableAmount(totalAmount, penaltyPerDay, maxPenalty,expiryDate, durationInMonths);
						dto.setPenaltyFlag("TRUE");
						dto.setCardCost(cardCost);
						dto.setODLRenewalCost(ODLRenewalAmount);
						dto.setPDRenewalCost(PDRenewalAmount);
					} 
					else if (dto.getServiceType().equalsIgnoreCase("NEW")) 
					{
						durationInMonths	=	Integer.parseInt(dto.getRenewalDuration());
						int noOfYears = durationInMonths / 12;
						double amount = (noOfYears * serviceFee) + cardCost;
						if (dto.getIdentityTypeId().equals("C"))
						{
							Date today = new Date(System.currentTimeMillis());
							pst = conn.prepareStatement(PaymentSQLConstant.GET_MAX_EXPIRY_FOR_LICENSE);
							pst.setString(1, dto.getIdentityNo());
							pst.setString(2, dto.getIdentityNo());
							pst.setString(3, dto.getIdentityNo());
							rs = pst.executeQuery();
							rs.first();
							java.util.Date ordinaryExpiryDate = rs.getDate("expiryDate");
							int dayDiff = ordinaryExpiryDate.compareTo(today);
							if(dayDiff < 1)
							{
								amount = amount + 300;
							}
						}
						dto.setAmount(Double.toString(amount));
						dto.setTotalAmount(Double.toString(amount));
						dto.setPenaltyFlag("FALSE");
					}
					else 
					{
						dto.setAmount(Double.toString(totalAmount));
						dto.setTotalAmount(Double.toString(totalAmount));
						dto.setPenaltyFlag("FALSE");
					}
					dto.setStatus("SUCCESS");
				}
				else if (dto.getRequestType().equalsIgnoreCase("VEHICLE"))
				{
					double penaltyPerDay = 0.0, maxPenalty = 0.0;
					String query = null, vehicleTypeDesc = "ELECTRIC";
					
					query = PaymentSQLConstant.GET_VEHICLE_PAYMENT_DETAILS+ " ";
					
					if(!dto.getIdentityTypeId().equals("0"))
					{
						vehicleTypeDesc = getVehicleTypeDesc(dto.getIdentityTypeId(), conn);
					}
					
					if(!dto.getServiceType().equalsIgnoreCase("RENEWAL_AND_RWC"))
					{
						pst = conn.prepareStatement(PaymentSQLConstant.GET_PENALTY_DETAILS);
						pst.setString(1, dto.getRequestType());
						pst.setString(2, dto.getServiceType());
						rs = pst.executeQuery();
						rs.first();
						penaltyPerDay = rs.getDouble("Penalty_Per_Day");
						maxPenalty = rs.getDouble("Max_Penalty");
					}
					
					if (vehicleTypeDesc.equalsIgnoreCase("TWO_WHEELER")
							|| vehicleTypeDesc.equalsIgnoreCase("EARTH_MOVING_EQUIPMENT")
							|| vehicleTypeDesc.equalsIgnoreCase("ELECTRIC")
							|| vehicleTypeDesc.equalsIgnoreCase("POWER_TILLER")) 
					{
						pst = conn.prepareStatement(PaymentSQLConstant.GET_VEHICLE_PAYMENT_DETAILS);
						pst.setString(1, dto.getIdentityTypeId());
					} 
					else if (vehicleTypeDesc.equalsIgnoreCase("HEAVY_VEHICLE") || 
							vehicleTypeDesc.equalsIgnoreCase("MEDIUM_VEHICLE")) 
					{
						String where = "AND (((a.`Loading_Capacity` >= ? AND a.`Flag` = 0) OR (a.`Loading_Capacity` < ? AND a.`Flag` = 1))) ORDER BY a.`Loading_Capacity` LIMIT 1";
						query = query + where;
						pst = conn.prepareStatement(query);
						pst.setString(1, dto.getIdentityTypeId());
						pst.setString(2, dto.getLoadingCapacity());
						pst.setString(3, dto.getLoadingCapacity());
					} 
					else if (vehicleTypeDesc.equalsIgnoreCase("MEDIUM_BUS") || 
							vehicleTypeDesc.equalsIgnoreCase("HEAVY_BUS")) 
					{
						String where = "AND (((a.`Seating_Capacity` >= ? AND a.`Flag` = 0) OR (a.`Seating_Capacity` < ? AND a.`Flag` = 1))) ORDER BY a.`Seating_Capacity` LIMIT 1";
						query = query + where;

						pst = conn.prepareStatement(query);
						pst.setString(1, dto.getIdentityTypeId());
						pst.setString(2, dto.getSeatingCapacity());
						pst.setString(3, dto.getSeatingCapacity());
					} 
					else if (vehicleTypeDesc.equalsIgnoreCase("TRACTOR")) 
					{
						String where = "AND (((a.`Vehicle_HP` >= ? AND a.`Flag` = 0) OR (a.`Vehicle_HP` < ? AND a.`Flag` = 1))) ORDER BY a.`Vehicle_HP` LIMIT 1";
						query = query + where;
						pst = conn.prepareStatement(query);
						pst.setString(1, dto.getIdentityTypeId());
						pst.setString(2, dto.getVehicleHP());
						pst.setString(3, dto.getVehicleHP());
					} 
					else if (vehicleTypeDesc.equalsIgnoreCase("LIGHT_VEHICLE"))
					{
						String where = "AND (((a.`Engine_CC` >= ? AND a.`Flag` = 0) OR (a.`Engine_CC` < ? AND a.`Flag` = 1))) ORDER BY a.`Engine_CC` LIMIT 1";
						query = query + where;
						pst = conn.prepareStatement(query);
						pst.setString(1, dto.getIdentityTypeId());
						pst.setString(2, dto.getVehicleEngineCC());
						pst.setString(3, dto.getVehicleEngineCC());
					} 
					else if (vehicleTypeDesc.equalsIgnoreCase("TAXI")) 
					{
						String where = "AND a.`Seating_Capacity` = ? ORDER BY a.`Seating_Capacity` LIMIT 1";
						query = query + where;
						pst = conn.prepareStatement(query);
						pst.setString(1, dto.getIdentityTypeId());
						pst.setString(2, dto.getSeatingCapacity());
					}
					
					rs = pst.executeQuery();
					rs.first();
					double fees = rs.getDouble("Fees");
					double mvt = rs.getDouble("MVT");
					double amount = 0.0;
					
					if("2".equals(dto.getRegistrationCode()))
						amount = fees;
					else
						amount = fees + mvt;//rs.getDouble("Amount");
					
					double rcCost = rs.getDouble("RC_Cost");
					double fitnessAmount = rs.getDouble("Fitness_Amount");
					double ownership_transfer_fee = rs.getDouble("ownership_transfer_fee");
					double conversion_fee = rs.getDouble("conversion_fee");
					int transfer_tax = rs.getInt("transfer_tax");
					int duration = 0;

					if (dto.getServiceType().equalsIgnoreCase("RWC")) 
					{
						if (vehicleTypeDesc.equalsIgnoreCase("TAXI") || vehicleTypeDesc.equalsIgnoreCase("HEAVY_BUS") || vehicleTypeDesc.equalsIgnoreCase("MEDIUM_BUS")) 
						{
							duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("renewal-expirydate-commercial-duration"));
						} 
						else 
						{
							duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("renewal-expirydate-noncommercial-duration"));
						}
						/*pst = conn.prepareStatement(PaymentSQLConstant.GET_MAX_EXPIRY_FOR_VEHICLE);
						pst.setString(1, dto.getIdentityNo());
						pst.setString(2, dto.getIdentityNo());
						pst.setString(3, dto.getIdentityNo());
						
						pst = conn.prepareStatement("SELECT "+
								" MAX(Validity) expiryDate "+
								" FROM  "+
								" `t_vehicle_fitness_test_dtls` a "+ 
								" WHERE a.`Vehicle_Id`=? ");*/
						
						
						pst = conn.prepareStatement("SELECT "
								+ "IF( "
								+ "a.`Vehicle_Reg_Dtls_Id` IN "
								+ "(SELECT "
								+ "`Vehicle_Id` "
								+ "FROM "
								+ "`t_vehicle_fitness_test_dtls` WHERE Vehicle_Id=?), (SELECT "
								+ "MAX(Validity) "
								+ "FROM "
								+ "`t_vehicle_fitness_test_dtls` WHERE Vehicle_Id=? AND `Receipt_Number` <>'NA'), "
								+ "a.`Registration_Date` "
								+ ") expiryDate "
								+ "FROM "
								+ "`t_vehicle_registration_dtls` a "
								+ "WHERE a.`Vehicle_Reg_Dtls_Id`=?");

						pst.setString(1, dto.getIdentityNo());
						pst.setString(2, dto.getIdentityNo());
						pst.setString(3, dto.getIdentityNo());
						rs = pst.executeQuery();
						rs.first();
						Date expiryDate = rs.getDate("expiryDate");
						double renewalAmount = fitnessAmount;
						dto = EralisAccountingUtil.calculateRWCPayableAmount(renewalAmount, penaltyPerDay, maxPenalty,expiryDate, duration);
						dto.setPenaltyFlag("TRUE");
						dto.setStatus("SUCCESS");
					}
					else if (dto.getServiceType().equalsIgnoreCase("RRWC")) 
					{
						/*if (vehicleTypeDesc.equalsIgnoreCase("TAXI") || vehicleTypeDesc.equalsIgnoreCase("HEAVY_BUS") || vehicleTypeDesc.equalsIgnoreCase("MEDIUM_BUS")) 
						{
							duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("renewal-expirydate-commercial-duration"));
						} 
						else 
						{
							duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("renewal-expirydate-noncommercial-duration"));
						}
						pst = conn.prepareStatement(PaymentSQLConstant.GET_MAX_EXPIRY_FOR_VEHICLE);
						pst.setString(1, dto.getIdentityNo());
						pst.setString(2, dto.getIdentityNo());
						pst.setString(3, dto.getIdentityNo());
						rs = pst.executeQuery();
						rs.first();
						Date expiryDate = rs.getDate("expiryDate");
						*/
						//double renewalAmount = fitnessAmount;
						dto.setTotalAmount(Double.toString(fitnessAmount));
						dto.setAmount(Double.toString(fitnessAmount));
						dto.setPenalty(Double.toString(0.0));
						
						//dto = EralisAccountingUtil.calculateRWCPayableAmount(renewalAmount, penaltyPerDay, maxPenalty,expiryDate, duration);
						dto.setPenaltyFlag("TRUE");
						dto.setStatus("SUCCESS");
					}
					else if (dto.getServiceType().equalsIgnoreCase("REGISTRATION")) 
					{
						duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("vehicle-registration-grace-period-in-days"));
						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
						java.util.Date purchaseDate = format.parse(dto.getPurchaseDate());
						if(vehicleTypeDesc.equalsIgnoreCase("TWO_WHEELER") ||
								vehicleTypeDesc.equalsIgnoreCase("LIGHT_VEHICLE"))
						{
							int noOfYears = Integer.parseInt(dto.getRenewalDuration())/12;
							amount = amount * noOfYears;
						}
						else
						{
							if(dto.getRenewalDuration().equals("6"))
							{
								amount = amount/2;
							}
							else
							{
								int noOfYears = Integer.parseInt(dto.getRenewalDuration())/12;
								amount = amount * noOfYears;
							}
						}
						double registrationAmount = amount + rcCost + fitnessAmount;
						dto = EralisAccountingUtil.calculateVehicleRegistrationAmount(registrationAmount, purchaseDate,penaltyPerDay, maxPenalty, duration);
						dto.setPenaltyFlag("TRUE");
					} 
					else if (dto.getServiceType().equalsIgnoreCase("RENEWAL")) 
					{
						if (vehicleTypeDesc.equalsIgnoreCase("TAXI") || 
								vehicleTypeDesc.equalsIgnoreCase("HEAVY_BUS")|| 
								vehicleTypeDesc.equalsIgnoreCase("MEDIUM_BUS")) 
						{
							duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("renewal-expirydate-commercial-duration"));
						} 
						else 
						{
							duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("renewal-expirydate-noncommercial-duration"));
						}
						pst = conn.prepareStatement(PaymentSQLConstant.GET_MAX_EXPIRY_FOR_VEHICLE);
						pst.setString(1, dto.getIdentityNo());
						pst.setString(2, dto.getIdentityNo());
						pst.setString(3, dto.getIdentityNo());
						rs = pst.executeQuery();
						rs.first();
						Date expiryDate = rs.getDate("expiryDate");
						
						if(vehicleTypeDesc.equalsIgnoreCase("TWO_WHEELER") || vehicleTypeDesc.equalsIgnoreCase("LIGHT_VEHICLE"))
						{
							int noOfYears = Integer.parseInt(dto.getRenewalDuration())/12;
							amount = amount * noOfYears;
						}
						else
						{
							if(dto.getRenewalDuration().equals("6"))
							{
								amount = amount/2;
							}
							else
							{
								int noOfYears = Integer.parseInt(dto.getRenewalDuration())/12;
								amount = amount * noOfYears;
							}
						}
						double renewalAmount = amount;
						dto = EralisAccountingUtil.calculatePayableAmount(renewalAmount, penaltyPerDay, maxPenalty,expiryDate, duration);
						dto.setPenaltyFlag("TRUE");
					} 
					else if (dto.getServiceType().equalsIgnoreCase("TRANSFER")) 
					{
						duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("vehicle-transfer-grace-period-in-days"));
						pst = conn.prepareStatement(PaymentSQLConstant.GET_INITIAL_REGISTRATION_DETAILS);
						pst.setString(1, dto.getIdentityNo());
						rs = pst.executeQuery();
						rs.first();
						Date initialRegistrationDate = rs.getDate("Registration_Date");
						double initialPurchasePrice = rs.getDouble("Price");
						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
						java.util.Date saleDeedDate = format.parse(dto.getSaleDeedDate());
						double transferAmount = rcCost + ownership_transfer_fee;
						
						if(dto.getRegistrationCode().equalsIgnoreCase("NORMAL"))
							dto = EralisAccountingUtil.calculatePayableTransferAmount(transferAmount,initialRegistrationDate,initialPurchasePrice, transfer_tax,Double.parseDouble(dto.getSaleDeedAmount()),saleDeedDate, duration,penaltyPerDay, maxPenalty);
						else if(dto.getRegistrationCode().equalsIgnoreCase("PARENT_TO_CHILD") ||
								dto.getRegistrationCode().equalsIgnoreCase("WITHIN_SPOUSE")||
								dto.getRegistrationCode().equalsIgnoreCase("PRIVILEGE"))
							dto = EralisAccountingUtil.calculatePayableTransferAmountForFamilyMember(transferAmount,initialRegistrationDate,initialPurchasePrice, transfer_tax,Double.parseDouble(dto.getSaleDeedAmount()),saleDeedDate, duration,penaltyPerDay, maxPenalty);
						else if(dto.getRegistrationCode().equalsIgnoreCase("AUCTIONED"))
							dto = EralisAccountingUtil.calculatePayableTransferAmountForAuction(transferAmount,initialRegistrationDate,initialPurchasePrice, transfer_tax,Double.parseDouble(dto.getSaleDeedAmount()),saleDeedDate, duration,penaltyPerDay, maxPenalty);
						
						dto.setPenaltyFlag("TRUE");
					} 
					else if (dto.getServiceType().equalsIgnoreCase("CONVERSION")) 
					{
						double conversionAmount = 0.0;
						
						if(dto.getRegistrationCode().equals("2"))
							conversionAmount = fitnessAmount+ conversion_fee;
						else
							conversionAmount = rcCost + fitnessAmount+ conversion_fee;
						
						dto.setAmount(Double.toString(conversionAmount));
						dto.setTotalAmount(Double.toString(conversionAmount));
						dto.setPenaltyFlag("FALSE");
					} 
					else 
					{
						dto.setAmount(Double.toString(rcCost));
						dto.setTotalAmount(dto.getAmount());
						dto.setPenaltyFlag("FALSE");
					}
					dto.setStatus("SUCCESS");
				}
				else if(dto.getRequestType().equalsIgnoreCase("COMMON"))
				{
					DecimalFormat numberFormat = new DecimalFormat("#.00");
					String vehicleTypeDesc = getVehicleTypeDesc(dto.getIdentityTypeId(), conn);
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					java.util.Date dateOfIssue = format.parse(dto.getPurchaseDate());
					java.util.Date validUpto = format.parse(dto.getSaleDeedDate());
					
					Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String dateOfIssueStr = formatter.format(dateOfIssue);
					String validUptoStr = formatter.format(validUpto);
					
					int durationDays = EralisCommonUtil.dateDifferenceAsInt(dateOfIssueStr, validUptoStr, "D");
					int durationMonths = EralisCommonUtil.dateDifferenceAsInt(dateOfIssueStr, validUptoStr, "M");
					String query = PaymentSQLConstant.GET_PERMIT_PAYMENT_DETAILS;
					durationDays = durationDays+1;
					if(dto.getIdentityNo().equals("FV"))
					{
						if (vehicleTypeDesc.equalsIgnoreCase("LIGHT_VEHICLE")) 
						{
							String where = "AND (((a.`Engine_CC` >= ? AND a.`Flag` = 0) OR (a.`Engine_CC` < ? AND a.`Flag` = 1))) ORDER BY a.`Engine_CC` LIMIT 1";
							query = query + where;

							pst = conn.prepareStatement(query);
							pst.setString(1, dto.getIdentityTypeId());
							pst.setString(2, dto.getVehicleEngineCC());
							pst.setString(3, dto.getVehicleEngineCC());
						}
						else if (vehicleTypeDesc.equalsIgnoreCase("TWO_WHEELER")) 
						{
							pst = conn.prepareStatement(query);
							pst.setString(1, dto.getIdentityTypeId());
						}
						else
						{
							String where = "AND a.Vehicle_Type = ?";
							query = query + where;
							pst = conn.prepareStatement(query);
							pst.setString(1, "0");
							pst.setString(2, "COMMERCIAL");
						}
						rs = pst.executeQuery();
						rs.first();
						double amountPerMonth = rs.getDouble("Amount_Per_Month");
						double amountPerDay = rs.getDouble("Amount_Per_Day");
						if (vehicleTypeDesc.equalsIgnoreCase("LIGHT_VEHICLE") || vehicleTypeDesc.equalsIgnoreCase("TWO_WHEELER")) 
						{
							if(durationMonths == 0)
							{
								//dto.setAmount(Double.toString(amountPerMonth));
								dto.setAmount(numberFormat.format(amountPerDay*durationDays));
							}
							else
							{
								dto.setAmount(numberFormat.format(amountPerMonth*durationMonths));
							}
						}
						else
							dto.setAmount(numberFormat.format(amountPerDay*durationDays));
					}
					else if(dto.getIdentityNo().equals("BV"))
					{
						String where = "AND a.Vehicle_Type = ?";
						query = query + where;
						pst = conn.prepareStatement(query);
						pst.setString(1, "0");
						pst.setString(2, "PASSENGERSERVICE");
						rs = pst.executeQuery();
						rs.first();
						double amountPerDay = rs.getDouble("Amount_Per_Day");
						dto.setAmount(numberFormat.format(amountPerDay*durationDays));
					}
					dto.setTotalAmount(dto.getAmount());
					dto.setPenaltyFlag("FALSE");
					dto.setStatus("SUCCESS");
				}
				else if(dto.getRequestType().equalsIgnoreCase("TOP"))
				{
					DecimalFormat numberFormat = new DecimalFormat("#.00");
					pst = conn.prepareStatement(PaymentSQLConstant.GET_LICENSE_PAYMENT_DETAILS);
					pst.setString(1, dto.getRequestType());
					pst.setString(2, dto.getServiceType());
					pst.setString(3, dto.getIdentityTypeId());
					rs = pst.executeQuery();
					rs.first();
					double serviceFee = rs.getDouble("Amount");
					double cardCost = rs.getDouble("License_Card_Cost");
					dto.setAmount(numberFormat.format(serviceFee+cardCost));
					dto.setTotalAmount(dto.getAmount());
					dto.setPenaltyFlag("FALSE");
					dto.setStatus("SUCCESS");
				}
				else
				{
					dto.setAmount("0.0");
					dto.setTotalAmount("0.0");
					dto.setPenaltyFlag("TRUE");
					dto.setStatus("SUCCESS");
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			dto.setStatus("FAILURE");
			throw new ERALISException();
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}

	private String getVehicleTypeDesc(String vehicleTypeId, Connection conn)throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String vehicleTypeDesc = null;

		try {
			if (vehicleTypeId.equals("0"))
				vehicleTypeDesc = "ELECTRIC";
			else {
				pst = conn.prepareStatement(PaymentSQLConstant.GET_VEHICLE_TYPE_DESC);
				pst.setString(1, vehicleTypeId);
				rs = pst.executeQuery();
				rs.first();
				vehicleTypeDesc = rs.getString("Description");
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		return vehicleTypeDesc;
	}
	public NotificationDTO resetPassword(String loginId, String questionId,String answer, Connection conn) throws ERALISException,ERALISSystemException 
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		NotificationDTO dto = new NotificationDTO();
		try 
		{
			pst = conn.prepareStatement(GET_FORGOT_PASSWORD_DETAILS);
			pst.setString(1, loginId);
			rs = pst.executeQuery();
			rs.first();
			String questionIdDB = rs.getString("question_id");
			String answerDB = rs.getString("answer");

			if (questionIdDB.equalsIgnoreCase(questionId) && answerDB.equalsIgnoreCase(answer)) 
			{
				String password = Constants.ERALIS_PASSWORD_BUILDER + "@" + loginId;
				boolean flag = LdapPasswordUpdate.resetUserPassword(loginId,password);

				if (flag)
				{
					pst = conn.prepareStatement(GET_USER_DETAILS);
					pst.setString(1, loginId);
					rs = pst.executeQuery();
					rs.first();

					dto.setCustomerName(rs.getString("Name"));
					dto.setPassword(password);
					dto.setEmailAddress(rs.getString("email_id"));
					dto.setMobileNumber(rs.getString("mobile_number"));
					dto.setLoginId(loginId);
					dto.setStatus("SUCCESS");
				}
			}
		} 
		catch (Exception e) {
			throw new ERALISException();
		} 
		finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		return dto;
	}

	public DocumentVO imageUploader(DocumentVO documentVo, String customerId)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String filePath = null, uuid = null;

		try {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
			String urlAppender = calendar.get(Calendar.YEAR) + "/"+ dateFormat.format(calendar.getTime()) + "/";
			filePath = EralisCommonUtil.getProperty("config.system.imageWritePath")+ "/"+ urlAppender + customerId + "_" + documentVo.getName().replace(" ", "_").trim();
			Log.debug("filePath = " + filePath);
			// check if the directory structure exists or not, if not then
			// create the directory structure
			File file = new File(urlAppender);
			if (!file.exists()) {
				new File(EralisCommonUtil.getProperty("config.system.imageWritePath")+ "/" + urlAppender).mkdirs();
			}
			File newFile = new File(filePath);
			FileOutputStream fileOutputStream;
			fileOutputStream = new FileOutputStream(newFile);
			fileOutputStream.write(documentVo.getFileContent());
			fileOutputStream.flush();
			fileOutputStream.close();
			uuid = UUID.randomUUID().toString();
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[imageUploader]: exception:: "+ e.getMessage());
		} finally {
			documentVo.setUploadURL(filePath);
			documentVo.setUuid(uuid);
			ConnectionManager.close(conn, null, null, pst);
		}
		return documentVo;
	}
	public DocumentVO roadBlockImageUploader(DocumentVO documentVo, int id)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String filePath = null, uuid = null;
		try {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
			String urlAppender = calendar.get(Calendar.YEAR) + "/"+ dateFormat.format(calendar.getTime()) + "/";
			filePath = EralisCommonUtil.getProperty("config.system.roadblockWritePath")+ "/"+ urlAppender + id + "_" + documentVo.getName();
			Log.debug("filePath = " + filePath);
			// check if the directory structure exists or not, if not then
			// create the directory structure
			File file = new File(urlAppender);
			if (!file.exists()) {
				new File(EralisCommonUtil.getProperty("config.system.roadblockWritePath")+ "/" + urlAppender).mkdirs();
			}
			File newFile = new File(filePath);
			FileOutputStream fileOutputStream;
			fileOutputStream = new FileOutputStream(newFile);
			fileOutputStream.write(documentVo.getFileContent());
			fileOutputStream.flush();
			fileOutputStream.close();
			uuid = UUID.randomUUID().toString();
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[imageUploader]: exception:: "+ e.getMessage());
		} finally {
			documentVo.setUploadURL(filePath);
			documentVo.setUuid(uuid);
			ConnectionManager.close(conn, null, null, pst);
		}
		return documentVo;
	}
	public void fileUploader(DocumentVO documentVo, String applicationNo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		Log.debug("*******inside fileUploader method of CommonDAO class");
		try {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
			String urlAppender = calendar.get(Calendar.YEAR) + "/"+ dateFormat.format(calendar.getTime()) + "/";
			String filePath = EralisCommonUtil.getProperty("config.system.fileWritePath")
					+ "/"
					+ documentVo.getServiceName()
					+ "/"
					+ urlAppender
					+ applicationNo + "_" + documentVo.getName();
			// check if the directory structure exists or not, if not then
			// create the directory structure
			File file = new File(urlAppender);
			if (!file.exists()) {
				new File(EralisCommonUtil.getProperty("config.system.fileWritePath")+ "/"+ documentVo.getServiceName()+ "/"+ urlAppender).mkdirs();
			}

			File newFile = new File(filePath);
			FileOutputStream fileOutputStream;
			fileOutputStream = new FileOutputStream(newFile);
			fileOutputStream.write(documentVo.getFileContent());
			fileOutputStream.flush();
			fileOutputStream.close();

			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(INSERT_T_DOCUMENT_DTLS);
			pst.setString(1, applicationNo);
			pst.setString(2, documentVo.getDocumentType());
			pst.setString(3, documentVo.getName());
			pst.setString(4, filePath);
			pst.setString(5, UUID.randomUUID().toString());
			pst.executeUpdate();
		} catch (Exception e) {
			Log.error("######## Error in CommonDAO[uploadDocuments] :" + e);
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("### Error at CommonDAO[fileUploader]: exception:: "+ e.getMessage());
		} finally {
			ConnectionManager.close(conn, null, null, pst);
		}
	}
	/*
	 * DOWNLOADING THE FILES
	 */
		public ArrayList<DocumentVO> getUploadedFilesByApplicationNo(String applicationNo, String serviceName) throws ERALISException,ERALISSystemException 
		{
			Connection conn = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			ArrayList<DocumentVO> arrayList = new ArrayList<DocumentVO>();
			try {
				conn = ConnectionManager.getConnection();
				pst = conn.prepareStatement(FETCH_SELECTIVE_DOCUMENT_DATA);
				pst.setString(1, applicationNo);
				pst.setString(2, serviceName);
				rs = pst.executeQuery();
				while (rs.next()) {
					DocumentVO documentInfoVO = new DocumentVO();
					documentInfoVO.setUuid(rs.getString("UUID"));
					documentInfoVO.setName(rs.getString("Document_Name"));
					documentInfoVO.setUploadURL(rs.getString("Document_Path"));
					documentInfoVO.setDocumentType(rs.getString("Document_Type"));
					arrayList.add(documentInfoVO);
				}
			} catch (Exception exception) {
				Log.error("" + exception.fillInStackTrace());
			} finally {
				ConnectionManager.close(conn, null, rs, pst);
			}
			return arrayList;
		}

		 
	public ApplicationDataVO getPrevApplyDetails(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_CID_EXISTS);
				pst.setString(1, vo.getCidNumber());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0)
					vo.setPrevApplyFlag("TRUE");
				else
					vo.setPrevApplyFlag("FALSE");
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	
	/*public ApplicationDataVO getLearnerTestDetails(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		vo.setStatus("FAILURE");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				pst = conn.prepareStatement(CHECK_IF_LEARNER_NO_EXISTS);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				pst = conn.prepareStatement(CHECK_FOR_CID);
				pst.setString(1, vo.getLicenseno());
				rs1 = pst.executeQuery();
				rs1.first();
				if (rs.getInt("rowCount") > 0)
				{
					if(rs.getString("Test_Type").equalsIgnoreCase("NEW"))
					{
						pst = conn.prepareStatement(GET_LEARNER_INFORMATION);
						pst.setString(1, vo.getLicenseno());
						rs = pst.executeQuery();
						if (rs != null && rs.first()) {
							vo.setApplicationNumber(rs.getString("Application_Number"));
							vo.setName(rs.getString("pname"));
							vo.setCidNumber(rs.getString("CID_Number"));
							vo.setDriveType(rs.getString("driveType"));
							vo.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
							if (rs.getString("Theory_Test_Status").equals("F"))
								vo.setStatus("THEORY_FAILED");
							else
							vo.setStatus("SUCCESS");
						}
					}
					else if(rs.getString("Test_Type").equalsIgnoreCase("ENDORSEMENT"))
					{
						pst = conn.prepareStatement(GET_LICENSE_INFORMATION);
						pst.setString(1, vo.getLicenseno());
						rs = pst.executeQuery();

						if (rs != null && rs.first()) {
							vo.setApplicationNumber(rs.getString("Application_Number"));
							vo.setName(rs.getString("pname"));
							vo.setCidNumber(rs.getString("CID_Number"));
							vo.setDriveType(rs.getString("driveType"));
							vo.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
							if (rs.getString("Theory_Test_Status").equals("F"))
								vo.setStatus("THEORY_FAILED");
							else
							vo.setStatus("SUCCESS");
						}
					}
				} 
				else if(rs1.getInt("rowCount") > 0)
				{

					pst = conn.prepareStatement(GET_PERSONAL_CUSTOMER_INFORMATION);
					pst.setString(1, rs1.getString("Customer_Id"));
					rs = pst.executeQuery();
					if (rs != null && rs.first()) {
						vo.setName(rs.getString("pname"));
						vo.setCidNumber(vo.getLicenseno());
						if (rs.getString("Theory_Test_Status").equals("F"))
							vo.setStatus("THEORY_FAILED");
						else
						vo.setStatus("SUCCESS");
						vo.setType("CID");
					}
					
					
				}
				else 
					vo.setStatus("LICENSE_DOESNOT_EXIST");
			}
		} catch (Exception e) {e.printStackTrace();
			vo.setStatus("FAILURE");
			throw new ERALISException("###Error at CommonDAO[getLearnerTestDetails]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}*/
	

	
	public ApplicationDataVO getVehicleAccidentDtls(String vehicleNumber)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ApplicationDataVO vo = new ApplicationDataVO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				pst = conn.prepareStatement( "SELECT COUNT(*)rowCount," +
						" (SELECT b.`Country_Name` FROM t_country_master b WHERE b.`Country_Id`=3)Country_Name, "
						+ "c.`Vehicle_Company_Name`,d.`Vehicle_Model_Name`,a.`Owner_Type`," +
								"(SELECT e.`Vehicle_Type_Name` FROM t_vehicle_type_master e WHERE e.vehicle_type_id=a.`Vehicle_Type`)Vehicle_Type_Name "
						+ " FROM t_accident_driver_vehicle_dtls  a "
						+ "LEFT JOIN t_vehicle_company_master c ON a.`Vehicle_Company`=c.`Vehicle_Company_Id` "
						+ "LEFT JOIN t_vehicle_model_master d ON a.`Vehicle_Model`=d.`Vehicle_Model_Id` "
						+ " WHERE a.`Vehicle_No`=?");
				pst.setString(1, vehicleNumber);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
				{
					vo.setVehicleType(rs.getString("Vehicle_Type_Name"));
					vo.setOwnerType(rs.getString("Owner_Type"));
					vo.setVehicleModel(rs.getString("Vehicle_Model_Name"));
					vo.setVehicleCompany(rs.getString("Vehicle_Company_Name"));
					vo.setCountry(rs.getString("Country_Name"));
				}
			}
		} catch (Exception e) {e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getLearnerTestDetails]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	
	
	public EralisCommonDTO getTokenDetails(String tokenId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		Connection conn1 = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getOtsDbConnection();
			
			if (conn != null)
			{ 
				conn1 = ConnectionManager.getConnection();
				pst = conn.prepareStatement("select a.token_no,a.customer_id,a.transaction_type,a.identity_number,a.service_type From t_generate_token a where a.id=?");
				pst.setString(1, tokenId);
				rs = pst.executeQuery();
				rs.first();
				
					pst1 = conn1.prepareStatement("select a.Customer_Id,concat(a.First_Name,' ',a.Middle_Name,' ',a.Last_Name)customerName," +
							"a.Image_Path,a.CID_Number,a.Present_Phone_No from t_personal_dtls a where a.Customer_Id=?");
					pst1.setString(1, rs.getString("customer_id"));
					rs1 = pst1.executeQuery();
					rs1.first();
					dto.setCustomerID(rs1.getString("Customer_Id"));
					dto.setName(rs1.getString("customerName"));
					dto.setImagePath(rs1.getString("Image_Path"));
					dto.setCID(rs1.getString("CID_Number"));
					dto.setPhone(rs1.getString("Present_Phone_No"));
						
					dto.setApplicationNo(rs.getString("token_no"));
					dto.setId(rs.getString("identity_number"));
					dto.setServiceType(rs.getString("service_type"));
				
			}
		} catch (Exception e) {e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[tokenId]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
			ConnectionManager.close(conn1, null, rs, pst);
		}
		return dto;
	}
	
	
	

	public ApplicationDataVO getLearnerTestDetails(ApplicationDataVO vo,EralisUserRolePriviledge user)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		vo.setStatus("FAILURE");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				pst = conn.prepareStatement(CHECK_IF_LEARNER_NO_EXISTS);
				pst.setString(1, vo.getLicenseno());
				pst.setString(2, user.getJurisdictionTypeId());
				pst.setString(3, user.getJurisdictionId());
				rs = pst.executeQuery();
				rs.first();
				pst = conn.prepareStatement(CHECK_FOR_CID_FOR_ETEST);
				pst.setString(1, vo.getLicenseno());
				pst.setString(2, user.getJurisdictionTypeId());
				pst.setString(3, user.getJurisdictionId());
				rs1 = pst.executeQuery();
				rs1.first();
				if (rs.getInt("rowCount") > 0)
				{
					vo.setType(rs.getString("Test_Type"));
					if(rs.getString("Test_Type").equalsIgnoreCase("NEW"))
					{
						pst = conn.prepareStatement(GET_LEARNER_INFORMATION);
						pst.setString(1, vo.getLicenseno());
						rs = pst.executeQuery();
						if (rs != null && rs.first()) {
							vo.setApplicationNumber(rs.getString("Application_Number"));
							vo.setName(rs.getString("pname"));
							vo.setCidNumber(rs.getString("CID_Number"));
							vo.setDriveType(rs.getString("driveType"));
							vo.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
							vo.setIsTest(rs.getString("Is_etest"));
							vo.setTestStatus(rs.getString("Theory_Test_Status"));
							
							/*if (rs.getString("Theory_Test_Status").equals("F"))
								vo.setStatus("THEORY_FAILED");
							else*/
							vo.setStatus("SUCCESS");
						}
					}
					else if(rs.getString("Test_Type").equalsIgnoreCase("ENDORSEMENT"))
					{
						pst = conn.prepareStatement(GET_LICENSE_INFORMATION);
						pst.setString(1, vo.getLicenseno());
						rs = pst.executeQuery();

						if (rs != null && rs.first()) {
							vo.setApplicationNumber(rs.getString("Application_Number"));
							vo.setName(rs.getString("pname"));
							vo.setCidNumber(rs.getString("CID_Number"));
							vo.setDriveType(rs.getString("driveType"));
							vo.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
							vo.setTestStatus(rs.getString("Theory_Test_Status"));
							
							/*if (rs.getString("Theory_Test_Status").equals("F"))
								vo.setStatus("THEORY_FAILED");
							else*/
							vo.setStatus("SUCCESS");
						}
					}
					
				} 
				else if(rs1.getInt("rowCount")>0)
				{
					pst = conn.prepareStatement(GET_PERSONAL_CUSTOMER_INFORMATION);
					pst.setString(1, rs1.getString("Customer_Id"));
					rs = pst.executeQuery();
					if (rs != null && rs.first()) {
						vo.setName(rs.getString("pname"));
						vo.setCidNumber(vo.getLicenseno());
						/*if (rs.getString("Theory_Test_Status").equals("F"))
							vo.setStatus("THEORY_FAILED");
						else*/
						vo.setStatus("SUCCESS");
						vo.setType("CID");
						vo.setApplicationNumber(rs1.getString("Application_Number"));
						
						
					}
				}
				else 
					vo.setStatus("LICENSE_DOESNOT_EXIST");
			}
		} catch (Exception e) {e.printStackTrace();
			vo.setStatus("FAILURE");
			throw new ERALISException("###Error at CommonDAO[getLearnerTestDetails]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	
	/*
	public ApplicationDataVO getLearnerTestDetails(ApplicationDataVO vo,EralisUserRolePriviledge user)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		vo.setStatus("FAILURE");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				pst = conn.prepareStatement(CHECK_IF_LEARNER_NO_EXISTS);
				pst.setString(1, vo.getLicenseno());
				pst.setString(2, user.getJurisdictionTypeId());
				pst.setString(3, user.getJurisdictionId());
				rs = pst.executeQuery();
				rs.first();
				pst = conn.prepareStatement(CHECK_FOR_CID);
				pst.setString(1, vo.getLicenseno());
				rs1 = pst.executeQuery();
				rs1.first();
				if (rs.getInt("rowCount") > 0)
				{
					if(rs.getString("Test_Type").equalsIgnoreCase("NEW"))
					{
						pst = conn.prepareStatement(GET_LEARNER_INFORMATION);
						pst.setString(1, vo.getLicenseno());
						rs = pst.executeQuery();
						if (rs != null && rs.first()) {
							vo.setApplicationNumber(rs.getString("Application_Number"));
							vo.setName(rs.getString("pname"));
							vo.setCidNumber(rs.getString("CID_Number"));
							vo.setDriveType(rs.getString("driveType"));
							vo.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
							vo.setIsTest(rs.getString("Is_etest"));
							vo.setTestStatus(rs.getString("Theory_Test_Status"));

							vo.setType("NEW");
							if (rs.getString("Theory_Test_Status").equals("F"))
								vo.setStatus("THEORY_FAILED");
							else
							vo.setStatus("SUCCESS");
						}
					}
					else if(rs.getString("Test_Type").equalsIgnoreCase("ENDORSEMENT"))
					{
						pst = conn.prepareStatement(GET_LICENSE_INFORMATION);
						pst.setString(1, vo.getLicenseno());
						rs = pst.executeQuery();

						if (rs != null && rs.first()) {
							vo.setApplicationNumber(rs.getString("Application_Number"));
							vo.setName(rs.getString("pname"));
							vo.setCidNumber(rs.getString("CID_Number"));
							vo.setDriveType(rs.getString("driveType"));
							vo.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
							vo.setTestStatus(rs.getString("Theory_Test_Status"));
							vo.setType("ENDORSEMENT");
							
							if (rs.getString("Theory_Test_Status").equals("F"))
								vo.setStatus("THEORY_FAILED");
							else
							vo.setStatus("SUCCESS");
						}
					}
				} 
				else if(rs1.getInt("rowCount") > 0)
				{
					pst = conn.prepareStatement(GET_PERSONAL_CUSTOMER_INFORMATION);
					pst.setString(1, rs1.getString("Customer_Id"));
					rs = pst.executeQuery();
					if (rs != null && rs.first()) {
						vo.setName(rs.getString("pname"));
						vo.setCidNumber(vo.getLicenseno());
						if (rs.getString("Theory_Test_Status").equals("F"))
							vo.setStatus("THEORY_FAILED");
						else
						vo.setStatus("SUCCESS");
						vo.setType("CID");
					}
					
					
				}
				else 
					vo.setStatus("LICENSE_DOESNOT_EXIST");
			}
		} catch (Exception e) {e.printStackTrace();
			vo.setStatus("FAILURE");
			throw new ERALISException("###Error at CommonDAO[getLearnerTestDetails]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}*/
	
	
	public EralisCommonDTO getQuestionDtls(String questionId)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {

		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getQuestionDtls]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public String getStatusCode(String applicationNO) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String statusCode = "";
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_STATUS_CODE);
			pst.setString(1, applicationNO);
			rs = pst.executeQuery();
			if (rs.first()) {
				statusCode = rs.getString("STATUS_TYPE_SHORT_DESC");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		return statusCode;
	}
	public ApplicationDataVO organisationCustomerId(String organisationName,String organisationType) 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ApplicationDataVO vo = new ApplicationDataVO();
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_ORGANISATION_CUSTOMER_ID);
			pst.setString(1, organisationName);
			pst.setString(2, organisationType);
			rs = pst.executeQuery();
			if (rs.first()) {
				vo.setCustomerId(rs.getString("Customer_Id"));
				vo.setMinistryId(rs.getString("Ministry_Id"));
				vo.setDepartmentId(rs.getString("Department_Id"));
				vo.setRegion(rs.getString("Region_Id"));
				vo.setDzongkhag(rs.getString("Dzongkhag_Id"));
				vo.setAddress(rs.getString("Address"));
				vo.setPhone(rs.getString("Phone_Number"));
				vo.setRemarks(rs.getString("Remarks"));
				vo.setEmail(rs.getString("Email_Id"));
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		return vo;
	}
	public ApplicationDataVO getOwnerInformation(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		vo.setStatus("FAILED");
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_VEHICLE_IS_CANCELLED);
				pst.setString(1, vo.getVehicleNumber());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0) {
					vo.setStatus("CANCELLED");
				} else {
					pst = conn
							.prepareStatement(CHECK_IF_VEHICLE_WAS_TRANSFERED);
					pst.setString(1, vo.getVehicleNumber());
					rs = pst.executeQuery();
					rs.first();
					int transferCount = rs.getInt("rowCount");

					if (transferCount > 0) {
						pst = conn
								.prepareStatement(GET_TRANSFEREE_TYPE_AND_CUSTOMER_CODE);
						pst.setString(1, vo.getVehicleNumber());
						rs = pst.executeQuery();
						rs.first();

						String transfereeType = rs.getString("Transferee_Type");
						String transfereeCustomerId = rs
								.getString("Transferee_Customer_Id");

						vo.setOwnerType(transfereeType);
						vo.setCustomerId(transfereeCustomerId);

						if (transfereeType.equalsIgnoreCase("P")) {
							pst = conn.prepareStatement(GET_PERSONAL_DETAILS1);
							pst.setString(1, transfereeCustomerId);
							rs = pst.executeQuery();
							rs.first();

							vo.setOwnerName(rs.getString("pname"));
						} else if (transfereeType.equalsIgnoreCase("O")) {
							String organizationName = null, department = null, ministry = null;

							pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS);
							pst.setString(1, transfereeCustomerId);
							rs = pst.executeQuery();
							rs.first();
							if (rs.getString("Organization_Name") != null)
								organizationName = rs.getString("Organization_Name");
							else
								organizationName = "";
							if (rs.getString("department") != null)
								department = rs.getString("department");
							else
								department = "";
							if (rs.getString("ministry") != null)
								ministry = rs.getString("ministry");
							else
								ministry = "";
							vo.setOwnerName(organizationName + " " + department+ " " + ministry);
						}
						vo.setStatus("SUCCESS");
					} else {
						pst = conn.prepareStatement(GET_REG_TYPE_CUSTOMER_ID_FROM_VEHICLE_REGISTRATION);
						pst.setString(1, vo.getVehicleNumber());
						rs = pst.executeQuery();
						rs.first();
						vo.setOwnerType(rs.getString("Vehicle_Registration_Type"));
						String customerId = rs.getString("Customer_Id");
						vo.setCustomerId(customerId);
						if (vo.getOwnerType().equalsIgnoreCase("P")) {
							pst = conn.prepareStatement(GET_PERSONAL_DETAILS1);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
							vo.setOwnerName(rs.getString("pname"));
						} else if (vo.getOwnerType().equalsIgnoreCase("O")) {
							String organizationName = null, department = null, ministry = null;

							pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
							if (rs.getString("Organization_Name") != null)
								organizationName = rs.getString("Organization_Name");
							else
								organizationName = "";
							if (rs.getString("department") != null)
								department = rs.getString("department");
							else
								department = "";

							if (rs.getString("ministry") != null)
								ministry = rs.getString("ministry");
							else
								ministry = "";
							vo.setOwnerName(organizationName + " " + department+ " " + ministry);
						}
						vo.setStatus("SUCCESS");
					}
					pst = conn.prepareStatement(GET_VEHICLE_DETAILS);
					pst.setString(1, vo.getVehicleNumber());
					rs = pst.executeQuery();
					rs.first();
					vo.setModel(rs.getString("vehicleModel"));
					vo.setMake(rs.getString("make"));
					vo.setEngineNumber(rs.getString("Engine_Number"));
					vo.setChasisNumber(rs.getString("Chassis_Number"));
					vo.setManufacturerYear(rs.getString("Manufacture_Year"));
				}
			}
		} catch (Exception e) {
			vo.setStatus("FAILED");
			throw new ERALISException("###Error at CommonDAO[getOwnerInformation]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public String getLastLoginDate(String userId) throws ERALISException,ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = null;
		try {
			
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_LAST_LOGIN_DATE);
				pst.setString(1, userId);
				rs = pst.executeQuery();
				while(rs.next())
				{
					result = rs.getString("last_login_date");
				}
			}
			
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getLastLoginDate]:exception:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	public void updateLoginDate(String uid) throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		Date date = new Date(System.currentTimeMillis());
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(UPDATE_T_USER_MASTER_WITH_LOGIN_DATE);
				pst.setDate(1, date);
				pst.setString(2, uid);
				pst.executeUpdate();
			}
		} catch (Exception e) {
			throw new ERALISException(
					"### Error at CommonDAO[updateLoginDate]:exception:: " + e);
		} finally {
			ConnectionManager.close(conn, null, null, pst);
		}
	}
	public String updateFirstLoginDtls(ApplicationDataVO appVO)throws ERALISException, ERALISSystemException, SQLException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FIRST_UPDATE_FAILURE";
		Date date = new Date(System.currentTimeMillis());

		try {
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			if (conn != null) {
				pst = conn.prepareStatement(UPDATE_T_USER_MASTER_WITH_LOGIN_DATE);
				pst.setDate(1, date);
				pst.setString(2, appVO.getUid());
				int count = pst.executeUpdate();
				if (count > 0) {
					pst = conn.prepareStatement(INSERT_INTO_T_PASSWORD_RETRIEVAL_DTLS);
					pst.setString(1, appVO.getUid());
					pst.setString(2, appVO.getSecurityQuestionId());
					pst.setString(3, appVO.getSecurityAnswer());
					count = pst.executeUpdate();
					if (count > 0) {
						result = "FIRST_UPDATE_SUCCESS";
						conn.commit();
					} else
						conn.rollback();
				}
			}
		} catch (Exception e) {
			result = "FIRST_UPDATE_FAILURE";
			conn.rollback();
			throw new ERALISException("###Error at CommonDAO[updateFirstLoginDtls]:exception:: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	public List<MenuVO> getMenuList(String roleId) throws ERALISException,ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<MenuVO> menuList = new ArrayList<MenuVO>();
		MenuVO menuVO;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_MENU_LIST);
				//pst.setString(1, "Y");
				pst.setString(1, roleId);
				rs = pst.executeQuery();
				while (rs.next()) {
					menuVO = new MenuVO();
					menuVO.setMenuId(rs.getString("menu_id"));
					menuVO.setMenuName(rs.getString("menu_name"));
					menuVO.setMenuIcon(rs.getString("menu_icon"));
					List<SubMenuVO> subMenuList = getSubMenuList(menuVO.getMenuId(), roleId);
					menuVO.setSubMenuList(subMenuList);
					menuList.add(menuVO);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[getMenuList]: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return menuList;
	}
	private List<SubMenuVO> getSubMenuList(String parentId, String roleId)	throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null, pst1 = null;
		ResultSet rs = null, rs1 = null;
		List<SubMenuVO> subMenuList = new ArrayList<SubMenuVO>();
		SubMenuVO menuVO;
		int rowCount = 1;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_SUB_MENU_LIST);
				pst.setString(1, parentId);
				rs = pst.executeQuery();
				while (rs.next()) 
				{
					menuVO = new SubMenuVO();
					if (null != rs.getString("page_link")) 
					{
						pst1 = conn.prepareStatement(CHECK_IF_MENU_DISABLED);
						pst1.setString(1, rs.getString("page_id"));
						pst1.setString(2, roleId);
						rs1 = pst1.executeQuery();
						rs1.first();
						rowCount = rs1.getInt("rowCount");
						if (rowCount > 0) 
						{
							menuVO.setMenuId(rs.getString("page_id"));
							menuVO.setMenuName(rs.getString("page_name"));
							menuVO.setMenuLink(rs.getString("page_link"));
						}
					} 
					else 
					{
						String pageId = rs.getString("page_id");
						pst1 = conn.prepareStatement(CHECK_IF_PAGE_DISABLED);
						pst1.setString(1, pageId);
						pst1.setString(2, roleId);
						rs1 = pst1.executeQuery();
						rs1.first();
						rowCount = rs1.getInt("rowCount");

						if (rowCount > 0) 
						{
							menuVO.setMenuId(rs.getString("page_id"));
							menuVO.setMenuName(rs.getString("page_name"));
							List<ThirdLevelMenuVo> thirdLvlMenuList = getThirdLvlMenuList(menuVO.getMenuId(), roleId);
							menuVO.setThirdLvlMenuList(thirdLvlMenuList);
						}
					}
					if (rowCount > 0)
						subMenuList.add(menuVO);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[getSubMenuList]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
			ConnectionManager.close(null, null, rs1, pst1);
		}
		return subMenuList;
	}
	private List<ThirdLevelMenuVo> getThirdLvlMenuList(String parentId,String roleId) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ThirdLevelMenuVo> thirdLvlMenuList = new ArrayList<ThirdLevelMenuVo>();
		ThirdLevelMenuVo menuVO;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_THIRD_LVL_MENU_LIST);
				pst.setString(1, roleId);
				pst.setString(2, parentId);
				rs = pst.executeQuery();
				while (rs.next()) {
					menuVO = new ThirdLevelMenuVo();
					menuVO.setMenuId(rs.getString("page_id"));
					menuVO.setMenuName(rs.getString("page_name"));
					menuVO.setMenuLink(rs.getString("page_link"));
					thirdLvlMenuList.add(menuVO);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[getThirdLvlMenuList]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return thirdLvlMenuList;
	}
	public PagePriviledge getPagePriviledge(String roleId, String pageId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		PagePriviledge priv = new PagePriviledge();
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_PAGE_PRIVILEDGE);
				pst.setString(1, roleId);
				pst.setString(2, pageId);
				rs = pst.executeQuery();
				rs.first();
				priv.setIsNew(rs.getString("is_new"));
				priv.setIsEdit(rs.getString("is_edit"));
				priv.setIsDelete(rs.getString("is_delete"));
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getPagePriviledge]:" + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return priv;
	}
	public UserDTO getUserDetails(String userId) throws ERALISException,ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		UserDTO dto = new UserDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_USER_DETAILS);
				pst.setString(1, userId);
				rs = pst.executeQuery();
				rs.first();
				dto.setLoginId(rs.getString("login_id"));
				dto.setUserName(rs.getString("Name"));
				dto.setEmailAddress(rs.getString("email_id"));
				dto.setMobileNumber(rs.getString("mobile_number"));
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public List<FaqDTO> get_faq_list() throws ERALISException,ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<FaqDTO> faqList = new ArrayList<FaqDTO>();
		FaqDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_FAQ_LIST);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new FaqDTO();
					dto.setFaqTitle(rs.getString("faq_title"));
					dto.setFaqDesc(rs.getString("faq_description"));
					dto.setFaqType(rs.getString("faq_type"));
					faqList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[get_faq_list]:"+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return faqList;
	}
	
	public List<DropDownDTO> getTCBDriveTypeList(String licenseId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		List<DropDownDTO> dropDownList = new ArrayList<DropDownDTO>();
		DropDownDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date today = new Date(System.currentTimeMillis());
				String todayStr = formatter.format(today);
				
				pst = conn.prepareStatement(GET_CUSTOMER_ID_ENDORSEMENT);
				pst.setString(1, licenseId);
				rs = pst.executeQuery();
				rs.first();
				String customerId = rs.getString("Customer_Id");
				String licenseType = rs.getString("Driving_License_Type_Id");
				
				if(licenseType.equalsIgnoreCase("N"))
				{
					pst = conn.prepareStatement(GET_AUTHORIZED_DRIVE_TYPE_LIST);
					pst.setString(1, licenseId);
				}
				else if(licenseType.equalsIgnoreCase("C"))
				{
					pst = conn.prepareStatement(GET_AUTHORIZED_DRIVE_TYPE_LIST_FOR_COMMERCIAL);
					pst.setString(1, customerId);
				}
				
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new DropDownDTO();
					boolean flag = false;
					
					if(rs.getString("Description").equals("LIGHT_VEHICLE"))
					{
						/*pst = conn.prepareStatement(CHECK_FOR_ISSUE_DATE);
						pst.setString(1, licenseId);
						pst.setString(2, licenseId);
						pst.setString(3, rs.getString("Description"));
						pst.setString(4, licenseId);
						rs1 = pst.executeQuery();
						rs1.first();
						if(rs1.getInt("rowCount") > 0)
						{
							Date Issue_Date = rs1.getDate("issue_date");
							String non_comm_issue_date = formatter.format(Issue_Date);
							int issued_duration = EralisCommonUtil.dateDifferenceAsInt(non_comm_issue_date,todayStr, "Y");
							if(issued_duration>=3)
							{*/
								pst1 = conn.prepareStatement(GET_TOURIST_DRIVE_TYPE);
								pst1.setString(1, "TOURIST_LIGHT_VEHICLE");
								rs1 = pst1.executeQuery();
								rs1.first();
								dto.setHeaderId(rs1.getString("HEADER_ID"));
								dto.setHeaderName(rs1.getString("HEADER_NAME"));
								flag = true;
							/*}
						}*/
					}
					if(rs.getString("Description").equals("MEDIUM_BUS"))
					{
						/*pst = conn.prepareStatement(CHECK_FOR_ISSUE_DATE);
						pst.setString(1, licenseId);
						pst.setString(2, licenseId);
						pst.setString(3, rs.getString("Description"));
						pst.setString(4, licenseId);
						rs1 = pst.executeQuery();
						rs1.first();
						if(rs1.getInt("rowCount") > 0)
						{
							Date Issue_Date = rs1.getDate("issue_date");
							String non_comm_issue_date = formatter.format(Issue_Date);
							int issued_duration = EralisCommonUtil.dateDifferenceAsInt(non_comm_issue_date,todayStr, "Y");
							if(issued_duration>=3)
							{*/
								pst1 = conn.prepareStatement(GET_TOURIST_DRIVE_TYPE);
								pst1.setString(1, "TOURIST_MEDIUM_BUS");
								rs1 = pst1.executeQuery();
								rs1.first();
								dto.setHeaderId(rs1.getString("HEADER_ID"));
								dto.setHeaderName(rs1.getString("HEADER_NAME"));
								flag = true;
							/*}
						}*/
					}
					if(rs.getString("Description").equals("HEAVY_BUS"))
					{
						/*pst = conn.prepareStatement(CHECK_FOR_ISSUE_DATE);
						pst.setString(1, licenseId);
						pst.setString(2, licenseId);
						pst.setString(3, rs.getString("Description"));
						pst.setString(4, licenseId);
						rs1 = pst.executeQuery();
						rs1.first();
						if(rs1.getInt("rowCount") > 0)
						{
							Date Issue_Date = rs1.getDate("issue_date");
							String non_comm_issue_date = formatter.format(Issue_Date);
							int issued_duration = EralisCommonUtil.dateDifferenceAsInt(non_comm_issue_date,todayStr, "Y");
							if(issued_duration>=3)
							{*/
								pst1 = conn.prepareStatement(GET_TOURIST_DRIVE_TYPE);
								pst1.setString(1, "TOURIST_HEAVY_BUS");
								rs1 = pst1.executeQuery();
								rs1.first();
								dto.setHeaderId(rs1.getString("HEADER_ID"));
								dto.setHeaderName(rs1.getString("HEADER_NAME"));
								flag = true;
							/*}
						}*/
					}
					
					if(flag)
						dropDownList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
			ConnectionManager.close(null, null, rs1, pst1);
		}
		
		return dropDownList;
	}
	
	public List<EralisCommonDTO> getPersonalInfoList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> personalList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String where = "";
		int count = 1;
		String query = null;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				// CHECKING NON EMPTY VARIABLE
				if (!vo.getCidNumber().equalsIgnoreCase("NA")) {
					where = " AND p.CID_Number = ? ";
				}
				if (!vo.getCustomerId().equalsIgnoreCase("NA")) {
					where = where + " AND p.Customer_Id = ? ";
				} 
				query = GET_PERSONAL_LIST;
				pst = conn.prepareStatement(query + where);
				if (!vo.getCidNumber().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getCidNumber());
					count = count + 1;
				}
				if (!vo.getCustomerId().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getCustomerId());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					if(vo.getFormType().equalsIgnoreCase("LEARNER_NEW"))
					{
						dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
						dto.setCID(rs.getString("CID_Number"));
						dto.setCustomerID(rs.getString("Customer_Id"));
						dto.setBloodGroup(rs.getString("blood_group_type"));
						dto.setRegion(rs.getString("region_name"));
						dto.setDzongkhag(rs.getString("dzongkhag_name"));
						dto.setGewog(rs.getString("Gewog_Name"));
						dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
						personalList.add(dto);
						dto.setFormType(vo.getFormType());
					}
					else
					{
						dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
						dto.setCID(rs.getString("CID_Number"));
						dto.setCustomerID(rs.getString("Customer_Id"));
						dto.setBloodGroup(rs.getString("blood_group_type"));
						dto.setRegion(rs.getString("region_name"));
						dto.setDzongkhag(rs.getString("dzongkhag_name"));
						dto.setGewog(rs.getString("Gewog_Name"));
						dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
						personalList.add(dto);
						dto.setFormType(vo.getFormType());
					}
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return personalList;
	}
	public List<EralisCommonDTO> getPendingList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> pendingList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		int count = 1;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				// CHECKING NON EMPTY VARIABLE
				pst = conn.prepareStatement(GET_PENDING_LIST);
				pst.setString(count, vo.getCustomerId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();

					dto.setPendingId(rs.getString("Pending_Id"));
					dto.setRemarks(rs.getString("Remarks"));
					dto.setDateOfissue(rs.getString("pendingDate"));
					dto.setName(rs.getString("userName"));
					pendingList.add(dto);
					dto.setFormType(vo.getFormType());
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return pendingList;
	}
	
	public EralisCommonDTO getPersonalDtlsByCid(String personalInfoId, String dob)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_PERSONAL_DETAILS_BY_CID);
				pst.setString(1, personalInfoId);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
				{
					dto.setStatus("VALID_CID");
					dto.setName(rs.getString("customerName"));
					dto.setCID(rs.getString("CID_Number"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					dto.setGender(rs.getString("Gender"));
					dto.setDzongkhag(rs.getString("Permanent_Dzongkhag_Id"));
					dto.setGewog(rs.getString("Permanent_Gewog_Id"));
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setPhone(rs.getString("Present_Phone_No"));
					dto.setEmail(rs.getString("Present_Email"));
					dto.setAddress(rs.getString("Present_Contact_Address"));
					if(!rs.getString("dob").equalsIgnoreCase(dob))
					{
						dto.setStatus("NOT_MATCHED");
					}
				}
				else
				{
					dto.setStatus("INVALID_CID");
				}
				
			}
		} catch (Exception e) {
			throw new ERALISException(
					"###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public String checkClearance(String customerId,String customerType) throws ERALISException
	{
		String result = "CLEAR";
		
		try {
			if(customerType.equalsIgnoreCase("P"))
			{
				result = vehicleOutstanding(customerId,customerType);
				if(result.equalsIgnoreCase("CLEAR"))
				{
					result = drivingLicenseOutstanding(customerId);
				}
			}
			else if(customerType.equalsIgnoreCase("O"))
			{
				result = vehicleOutstanding(customerId,customerType);
				
			}
			else if(customerType.equalsIgnoreCase("DL"))
			{
				result = vehicleOutstanding(customerId,customerType);
			}
			
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[vehicleOutstanding]:: " + e);
		}  
		return result; 
		
	}
	
	public String drivingLicenseOutstanding(String personalInfoId) throws ERALISException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "CLEAR";
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(CHECK_CUSTOMER_DRIVING_LICENSE_OUTSTANDING);
			pst.setString(1, personalInfoId);
			rs = pst.executeQuery();
			while(rs.next())
			{
				if(rs.getInt("isExpired")>0)
				{
					result = "DRIVING_LICENSE_OUTSTANDING/"+rs.getString("Driving_License_No");
				}
			}
			
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[drivingLicenseOutstanding]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}

	
	public String vehicleOutstanding(String personalInfoId,String customerType) throws ERALISException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "CLEAR";
		try {
			conn = ConnectionManager.getConnection();
			String query = "";
			if(customerType.equalsIgnoreCase("P"))
			{
				query = CHECK_CUSTOMER_VEHICLE_CANCELLED;
			}
			else if(customerType.equalsIgnoreCase("O"))
			{
				query = CHECK_ORGANISATION_VEHICLE_CANCELLED;
			}
			else if(customerType.equalsIgnoreCase("DL"))
			{
				query = CHECK_VEHICLE_OUTSTANDING_FROM_LICENSE_ID;
			}
			pst = conn.prepareStatement(query);
			pst.setString(1, personalInfoId);
			rs = pst.executeQuery();
			int i =0;
			while(rs.next())
			{
				if(i==0)
				{
					result="VEHICLE_OUTSTANDING/"+rs.getString("Vehicle_Number")+"/"+rs.getString("Vehicle_Type_Name");
				}
				else
				{
					result = result+"~"+"VEHICLE_OUTSTANDING/"+rs.getString("Vehicle_Number")+"/"+rs.getString("Vehicle_Type_Name");
					
				}
				i++;
			}
			
			
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[vehicleOutstanding]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	
	
	public EralisCommonDTO getPersonalInfoDtls(String personalInfoId,String searchType)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			String clearanceData = checkClearance(personalInfoId,"P");
			if(!clearanceData.equalsIgnoreCase("CLEAR"))
			{
				String[] dataArray = clearanceData.split("/");
				dto.setStatus(dataArray[0]);
				if(dataArray[0].equalsIgnoreCase("VEHICLE_OUTSTANDING"))
				{
					dto.setVehicleNo(dataArray[1]);
					dto.setVehicleType(dataArray[2]);
				}
				else if(dataArray[0].equalsIgnoreCase("DRIVING_LICENSE_OUTSTANDING"))
				{
					dto.setLicenseNo(dataArray[1]);
				}
				
			}
			else if(clearanceData.equalsIgnoreCase("CLEAR"))
			{
				conn = ConnectionManager.getConnection();
				if (conn != null) {
					String customerId = null;
					duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-age-for-learner-application"));
					pst = conn.prepareStatement(GET_PERSONAL_DETAILS);
					pst.setString(1, personalInfoId);
	
					rs = pst.executeQuery();
					rs.first();
					customerId = rs.getString("Customer_Id");
					dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setAddress(rs.getString("Present_Contact_Address"));
					dto.setCID(rs.getString("CID_Number"));
					dto.setCountry(rs.getString("Country_Name"));
					dto.setTitleOfcourtesy(rs.getString("Title_Of_Courtesy_Id"));
					dto.setOccupation(rs.getString("Occupation_Name"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					dto.setBloodGroup(rs.getString("blood_group_type"));
					dto.setDOB(rs.getString("dob"));
					dto.setNationality(rs.getString("Nationality"));
					dto.setFathersName(rs.getString("Fathers_Name"));
					dto.setIdentificationMarks(rs.getString("Identification_Mark"));
					dto.setRemarks(rs.getString("Remarks"));
					dto.setBloodGroupId(rs.getString("Blood_Group_Id"));
					dto.setGender(rs.getString("Gender"));
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setLicenseNo(rs.getString("Driving_License_No"));
					dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
					dto.setIsInternational(rs.getString("Is_International"));
					
					pst = conn.prepareStatement(GET_APPLICANT_AGE);
					pst.setString(1, personalInfoId);
					rs = pst.executeQuery();
					rs.first();
					dto.setAge(rs.getString("age"));
					if(searchType!=null && searchType.equalsIgnoreCase("LEARNER_NEW"))
					{
						if(rs.getInt("age")<17 )
						{
							dto.setStatus("UNDER_AGE");
						}
						pst = conn.prepareStatement(CHECK_IF_LEARNER_LICENSE_HAS_BEEN_ISSUED);
						pst.setString(1, dto.getCID());
						rs = pst.executeQuery();
						rs.first();
						int licenseCount = rs.getInt("licenseCount");
						int applicationCount = rs.getInt("applicationCount");
						if (licenseCount > 0) {
							dto.setStatus("ISSUED");
						} 
						else if (applicationCount > 0) {
							dto.setStatus("ISSUED");
						} 
						else 
						{
							dto.setStatus("SUCCESS");
						}
					}	
					else if(searchType!=null && searchType.equalsIgnoreCase("LICENSE_NEW"))
					{
						if(rs.getInt("age")<18 )
						{
							dto.setStatus("UNDER_AGE");
						}
						else
						{
							pst = conn.prepareStatement(CHECK_IF_LICENSE_HAS_BEEN_ISSUED);
							pst.setString(1, dto.getCID());
							rs = pst.executeQuery();
							rs.first();
							
							int licenseCount = rs.getInt("licenseCount");
							int applicationCount = rs.getInt("applicationCount");
							if (licenseCount > 0) {
								dto.setStatus("ISSUED");
							} 
							else if (applicationCount > 0) {
								dto.setStatus("ISSUED");
							} 
							else 
							{
								pst = conn.prepareStatement(GET_ETEST_OVERALL_MARKS_BY_CID);
								pst.setString(1, dto.getCID());
								rs = pst.executeQuery();
								rs.first();
								if(rs.getInt("rowCount")>0)
								{
									dto.setOverallObtainedMark(rs.getString("Overall_Marks_Obtained"));
									dto.setStatus("PASSED");
									dto.setDriverName(rs.getString("Drive_Type_Name"));
									dto.setDrivetype(rs.getString("Drive_Type_Id"));
									dto.setTheoryMarks(rs.getString("Theory_Marks_Obtained"));
									dto.setPracticalMarks(rs.getString("Practical_Marks_Obtained"));
									dto.setIssueType(rs.getString("Issue_Type"));
								}
								else
								{
									dto.setStatus("FAILED");
								}
							}
						}
					}
					
					dto.setLearnerapplicationage(duration);
					
					pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					int rowCount1 = rs.getInt("rowCount");
					if(rowCount1>0)
					{
						dto.setStatus("PENDING_APPLICATION");
						dto.setReason(rs.getString("remarks"));
					}
				}
			}
		} catch (Exception e) {
			throw new ERALISException(
					"###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	
	public EralisCommonDTO getPersonalInfoDtlsForEdit(String personalInfoId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_PERSONAL_DTLS_FOR_EDIT);
				pst.setString(1, personalInfoId);
				rs = pst.executeQuery();
				rs.first();
				
				dto.setMothersName(rs.getString("Mothers_Name"));
				dto.setGuardianNo(rs.getString("Guardian_Contact_No"));
				dto.setCustomerID(rs.getString("Customer_Id"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setTitleOfcourtesy(rs.getString("Title_Of_Courtesy_Id"));
				dto.setFirstname(rs.getString("First_Name"));
				dto.setMiddlename(rs.getString("Middle_Name"));
				dto.setLastName(rs.getString("Last_Name"));
				dto.setOccupation(rs.getString("Occupation_Id"));
				dto.setNationality(rs.getString("Nationality_Id"));
				dto.setDOB(rs.getString("dob"));
				dto.setFathersName(rs.getString("Fathers_Name"));
				dto.setBloodGroupId(rs.getString("Blood_Group_Id"));
				dto.setGender(rs.getString("Gender"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setIsInternational(rs.getString("Is_International"));
				dto.setDzongkhag(rs.getString("Permanent_Dzongkhag_Id"));
				dto.setGewog(rs.getString("Permanent_Gewog_Id"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setCountry(rs.getString("Permanent_Country_Id"));
				dto.setAddress(rs.getString("Permanent_Address"));
				dto.setPresentDzongkhag(rs.getString("Present_Dzongkhag_Id"));
				dto.setContactAddress(rs.getString("Present_Contact_Address"));
				dto.setPhone(rs.getString("Present_Phone_No"));
				dto.setEmail(rs.getString("Present_Email"));
				dto.setPersonalInfoId(personalInfoId);
				dto.setImagePath(rs.getString("Image_Path"));
				dto.setName(rs.getString("name"));
				dto.setDzongkhagName(rs.getString("dzongkhag_name"));
				dto.setBloodTypeName(rs.getString("blood_group_type"));
				dto.setGewogName(rs.getString("Gewog_Name"));
				dto.setContactRadio(rs.getString("Contact_Type"));
				dto.setMinistry(rs.getString("Ministry_Id"));
				dto.setDepartment(rs.getString("Department_Id"));
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtlsForEdit]:: " + e);
		} 
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	//fetching the organization list
	public List<EralisCommonDTO> getOrganisationInfoList(ApplicationDataVO vo) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> organisationList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String where = "";

		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) {

				if (!vo.getName().equalsIgnoreCase("NA")) {
					where = where + " AND (a.`Organization_Name` LIKE ? OR b.`Private_Name` LIKE ? OR c.`Ministry_Name` LIKE ? OR d.`Department_Name` LIKE ? OR h.`Name` LIKE ?) ";
				}
			/*	if (!vo.getType().equalsIgnoreCase("NA")) {
					where = where + " AND g.`Owner_Type_Id` = ?";
				}*/
				if (!vo.getCustomerId().equalsIgnoreCase("NA")) {
					where = where + " AND a.`Customer_Id` = ? ";
				}
				
				String query = GET_ORGANISATION_DETAILS_LIST + where;

				pst = conn.prepareStatement(query);
				
				int count = 1;
				
				if (!vo.getName().equalsIgnoreCase("NA"))
				{
					pst.setString(count, vo.getName()+"%");
					count++;
					pst.setString(count, vo.getName()+"%");
					count++;
					pst.setString(count, vo.getName()+"%");
					count++;
					pst.setString(count, vo.getName()+"%");
					count++;
					pst.setString(count, vo.getName()+"%");
					count++;
				}
				/*if (!vo.getType().equalsIgnoreCase("NA"))
				{
					pst.setString(count, vo.getType());
					count++;
				}*/
				if (!vo.getCustomerId().equalsIgnoreCase("NA"))
				{
					pst.setString(count, vo.getCustomerId());
				}
				
				rs = pst.executeQuery();

				while (rs.next()) 
				{
					dto = new EralisCommonDTO();
					dto.setName(rs.getString("Organization_Name"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					dto.setOwnerName(rs.getString("Owner_Name"));
					dto.setRegion(rs.getString("region_name"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setOrganizationInfoId(rs.getString("Organization_Info_Id"));
					organisationList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return organisationList;
	}
	public EralisCommonDTO getOrganisationInfoDtls(String organisationInfoId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		String name = "";
		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				String clearanceData = checkClearance(organisationInfoId,"O");
				if(!clearanceData.equalsIgnoreCase("CLEAR"))
				{
					String[] dataArray = clearanceData.split("/");
					dto.setStatus(dataArray[0]);
					if(dataArray[0].equalsIgnoreCase("VEHICLE_OUTSTANDING"))
					{
						dto.setVehicleNo(dataArray[1]);
						dto.setVehicleType(dataArray[2]);
					}
				}
				else
				{
				
					pst = conn.prepareStatement(GET_ORGANISATION_DETAILS);
					pst.setString(1, organisationInfoId);
					rs = pst.executeQuery();
					rs.first();
					String organizationType	=	rs.getString("organizationType");
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setAddress(rs.getString("Address"));
					dto.setRegion(rs.getString("region_name"));
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					if(null != rs.getString("Remarks"))
						dto.setRemarks(rs.getString("Remarks"));
					else
						dto.setRemarks("");
					dto.setType(organizationType);
					dto.setOrganizationTypeId(rs.getString("Organization_Type_Id"));
					dto.setRegionId(rs.getString("Region_Id"));
					dto.setDzongkhagId(rs.getString("Dzongkhag_Id"));
					dto.setDiplomatId(rs.getString("Diplomat_Id"));
					dto.setMinistry(rs.getString("Ministry_Id"));
					dto.setDepartment(rs.getString("Department_Id"));
					dto.setPrivateCompany(rs.getString("Private_Id"));
					dto.setOrganisationInfoId(organisationInfoId);
					dto.setOwnerName(rs.getString("Owner_Name"));
					if(organizationType.equalsIgnoreCase("GOVERNMENT"))
					{
						String ministryId = rs.getString("Ministry_Id");
						String departmentId = rs.getString("Department_Id");
						pst = conn.prepareStatement(GET_MINISTRY_NAME);
						pst.setString(1, ministryId);
						rs = pst.executeQuery();
						rs.first();
						name = rs.getString("Ministry_Name");
						if(Integer.parseInt(departmentId) != 0)
						{
							pst = conn.prepareStatement(GET_DEPARTMENT_NAME);
							pst.setString(1, departmentId);
							rs = pst.executeQuery();
							rs.first();
							name = name + "," + rs.getString("Department_Name");
						}
					}
					else if(organizationType.equalsIgnoreCase("PRIVATE"))
					{
						pst = conn.prepareStatement(GET_PRIVATE_NAME);
						pst.setString(1, rs.getString("Private_Id"));
						rs = pst.executeQuery();
						rs.first();
						name = rs.getString("Private_Name");
					}
					else if(rs.getString("Diplomat_Id")!=null && organizationType.equalsIgnoreCase("DIPLOMATS"))
					{
						pst = conn.prepareStatement(GET_DIPLOMAT_NAME);
						pst.setString(1, rs.getString("Diplomat_Id"));
						rs = pst.executeQuery();
						rs.first();
						name = rs.getString("Name");
					}
					else if(rs.getString("organizationType").equalsIgnoreCase("ROYAL_FAMILY"))
					{
						name = rs.getString("Organization_Name");
					}
					dto.setName(name);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getOrganisationInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public List<EralisCommonDTO> getAccidentRecord(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> accidentRecod = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_ACCIDENT_RECORD);
				pst.setString(1, vo.getVehicleNumber());
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setPoliceStation(rs.getString("Police_Station_Name"));
					dto.setDateOfOccurrence(rs.getString("Date_Of_Occurence"));
					dto.setPlaceOfOccurrence(rs.getString("Place_Of_Occurence"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setNationality(rs.getString("driverNationality"));
					dto.setDriverName(rs.getString("Driver_Name"));
					dto.setCID(rs.getString("CID"));
					dto.setLLNo(rs.getString("LL_DL_No"));
					dto.setAccidentType(rs.getString("Type_Of_Accident"));
					dto.setAccidentNature(rs.getString("Nature_Of_Accident"));
					dto.setAccidentCause(rs.getString("accidentCause"));
					dto.setAccidentType(rs.getString("Type_Of_Accident"));
					dto.setInjuredDtls(rs.getString("totalInjured"));
					dto.setKilledDtls(rs.getString("totalKilled"));
					dto.setRemarks(rs.getString("Remarks"));
					accidentRecod.add(dto);
				}
		   }
		} 
		catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getAccidentRecord]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return accidentRecod;
	}

	public List<EralisCommonDTO> searchAccidentVehicle(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		List<EralisCommonDTO> accidentVehicleList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String query = "";
		String where = "";
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_ACCIDENT_VEHICLE);
				pst.setString(1, vo.getVehicleNumber());
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setVehicleNo(rs.getString("Vehicle_No"));
					dto.setDriverName(rs.getString("Driver_Name"));
					dto.setLLNo(rs.getString("LL_DL_No"));
					dto.setVehicleType(rs.getString("Vehicle_Type"));
					accidentVehicleList.add(dto);
				}
		   }
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[searchAccidentVehicle]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return accidentVehicleList;
	}
	


	public List<EralisCommonDTO> getRenewalInfoList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		List<EralisCommonDTO> renewalList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String query = "";
		String where = "";
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				//if (!vo.getVehicleNumber().equalsIgnoreCase("NA"))
				//{
					pst = conn.prepareStatement(GET_OFFENCE_DTLS_FORIEGN);
					pst.setString(1, vo.getVehicleNumber());
					rs = pst.executeQuery();
					rs.first();
					int rowCount = rs.getInt("rowCount");
					if(rowCount>0)
					{
						dto = new EralisCommonDTO();
						dto.setOffenceId(rs.getString("Offence_Id"));
						dto.setOffenceLicenseType(rs.getString("Is_Foreign"));
						dto.setVehicleNo(rs.getString("Vehicle_Number"));
						dto.setLicenseNo(rs.getString("License_Number"));
						dto.setMobileNo(rs.getString("Mobile_Number"));
						dto.setPersonalOwnerName(rs.getString("Owner_Name"));
						dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
						dto.setTIN(rs.getString("TIN_No"));
						dto.setOwnerType(rs.getString("Is_Foreign"));
						dto.setRenewalInfoId(rs.getString("Offence_Id"));
						renewalList.add(dto);
					}
					else
					{
						if (!vo.getOrganizationName().equalsIgnoreCase("NA"))
						{
							if (!vo.getOrganizationName().equalsIgnoreCase("NA"))
							{
								where = where + " AND (a.`Organization_Name` LIKE ? OR b.`Private_Name` LIKE ? OR c.`Ministry_Name` LIKE ? OR d.`Department_Name` LIKE ?) ";
							}
							query = GET_ORGANISATION_DETAILS_LIST + where;
		
							pst = conn.prepareStatement(query);
							int count = 1;
							if (!vo.getOrganizationName().equalsIgnoreCase("NA"))
							{
								pst.setString(count, vo.getOrganizationName()+"%");
								count++;
								pst.setString(count, vo.getOrganizationName()+"%");
								count++;
								pst.setString(count, vo.getOrganizationName()+"%");
								count++;
								pst.setString(count, vo.getOrganizationName()+"%");
								count++;
							}
							rs = pst.executeQuery();
		
							while (rs.next()) 
							{
								where	=	"";
								if (!vo.getCidNumber().equalsIgnoreCase("NA")){
									where = where + " AND p.CID_Number=? ";
								}
								if (!vo.getVehicleNumber().equalsIgnoreCase("NA"))
								{
									where = where + " AND r.Vehicle_Number=? ";
								}
								if (!vo.getEngineNumber().equalsIgnoreCase("NA"))
								{
									where = where + " AND r.Engine_Number=? ";
								}
								if (!vo.getChasisNumber().equalsIgnoreCase("NA"))
								{
									where = where + " AND r.Chassis_Number=? ";
								}
								if (!vo.getOrganizationName().equalsIgnoreCase("NA"))
								{
									where = where + " AND r.Customer_Id=? ";
								}
								
								where = where+ " ORDER BY r.`Vehicle_Reg_Dtls_Id` DESC)"; 
								query =  GET_VEHICLE_REGISTRATION_RENEWAL + where;
								pst = conn.prepareStatement(query);
								
								int count1 = 1;
								if (!vo.getCidNumber().equalsIgnoreCase("NA")) {
									pst.setString(count1, vo.getCidNumber());
									count1 = count1 + 1;
								}
								if (!vo.getVehicleNumber().equalsIgnoreCase("NA")) {
									pst.setString(count, vo.getVehicleNumber());
									count1 = count1 + 1;
								}
								if (!vo.getEngineNumber().equalsIgnoreCase("NA")) {
									pst.setString(count1, vo.getEngineNumber());
									count1 = count1 + 1;
								}
								if (!vo.getChasisNumber().equalsIgnoreCase("NA")) {
									pst.setString(count1, vo.getChasisNumber());
									count1 = count1 + 1;
								}
								if (!vo.getOrganizationName().equalsIgnoreCase("NA")) {
									pst.setString(count1, rs.getString("Customer_Id"));
									count1 = count1 + 1;
								}
								rs1 = pst.executeQuery();
								while (rs1.next()) 
								{
									dto = new EralisCommonDTO();
									dto.setApplicationNo(rs1.getString("Application_Number"));
									if ("P".equalsIgnoreCase(rs1.getString("Vehicle_Registration_Type"))) {
										dto.setPersonalOwnerName(rs1.getString("personalOwnerName"));
									} else if ("O".equalsIgnoreCase(rs1.getString("Vehicle_Registration_Type"))) {
										dto.setPersonalOwnerName(rs1.getString("Organization_Name"));
									}
									
									dto.setEngineNumber(rs1.getString("Engine_Number"));
									dto.setVehicleNo(rs1.getString("Vehicle_Number"));
									dto.setVehicleType(rs1.getString("Vehicle_Type_Name"));
									dto.setEngineNumber(rs1.getString("Engine_Number"));
									dto.setChasisNumber(rs1.getString("Chassis_Number"));
									dto.setOwnerType(rs1.getString("Vehicle_Registration_Type"));
									dto.setCID(rs1.getString("CID_Number"));
									dto.setRenewalInfoId(rs1.getString("Vehicle_Reg_Dtls_Id"));
									renewalList.add(dto);
								}
							}
						}
						else
						{
						if (!vo.getCidNumber().equalsIgnoreCase("NA")){
							where = where + " AND p.CID_Number=? ";
						}
						if (!vo.getVehicleNumber().equalsIgnoreCase("NA"))
						{
							where = where + " AND r.Vehicle_Number=? ";
						}
						if (!vo.getEngineNumber().equalsIgnoreCase("NA"))
						{
							where = where + " AND r.Engine_Number=? ";
						}
						if (!vo.getChasisNumber().equalsIgnoreCase("NA"))
						{
							where = where + " AND r.Chassis_Number=? ";
						}
						
						where = where+ " ORDER BY r.`Vehicle_Reg_Dtls_Id` DESC)"; 
						query =  GET_VEHICLE_REGISTRATION_RENEWAL + where;
						pst = conn.prepareStatement(query);
						
						int count = 1;
						if (!vo.getCidNumber().equalsIgnoreCase("NA")) {
							pst.setString(count, vo.getCidNumber());
							count = count + 1;
						}
						if (!vo.getVehicleNumber().equalsIgnoreCase("NA")) {
							pst.setString(count, vo.getVehicleNumber());
							count = count + 1;
						}
						if (!vo.getEngineNumber().equalsIgnoreCase("NA")) {
							pst.setString(count, vo.getEngineNumber());
							count = count + 1;
						}
						if (!vo.getChasisNumber().equalsIgnoreCase("NA")) {
							pst.setString(count, vo.getChasisNumber());
							count = count + 1;
						}
						
						rs1 = pst.executeQuery();
						while (rs1.next()) 
						{
							dto = new EralisCommonDTO();
							dto.setApplicationNo(rs1.getString("Application_Number"));
							if ("P".equalsIgnoreCase(rs1.getString("Vehicle_Registration_Type"))) {
								dto.setPersonalOwnerName(rs1.getString("personalOwnerName"));
							} else if ("O".equalsIgnoreCase(rs1.getString("Vehicle_Registration_Type"))) {
								dto.setPersonalOwnerName(rs1.getString("Organization_Name"));
							}
							
							dto.setEngineNumber(rs1.getString("Engine_Number"));
							dto.setVehicleNo(rs1.getString("Vehicle_Number"));
							dto.setVehicleType(rs1.getString("Vehicle_Type_Name"));
							dto.setEngineNumber(rs1.getString("Engine_Number"));
							dto.setChasisNumber(rs1.getString("Chassis_Number"));
							dto.setOwnerType(rs1.getString("Vehicle_Registration_Type"));
							dto.setCID(rs1.getString("CID_Number"));
							dto.setRenewalInfoId(rs1.getString("Vehicle_Reg_Dtls_Id"));
							renewalList.add(dto);
						}
				//  }
				}
			  }
		   }
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getRenewalInfoList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return renewalList;
	}
	
	//getRepeatedOffenceType
	public String getRepeatedOffenceType()throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = null;
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{ 
				pst = conn.prepareStatement(GET_REPEATED_OFFENCE_ID);
				rs = pst.executeQuery();
				rs.first();
				result = rs.getString("Offence_Type_Id");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at CommonDAO[getRenewalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	

	public String checkDoubleTin(String tinNo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "NOT_DUPLICATE";
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{ 
				pst = conn.prepareStatement(CHECK_DOUBLE_TIN);
				pst.setString(1, tinNo);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
					result = "DUPLICATE";
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at CommonDAO[getRenewalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	

	public String checkDoubleOffence(String offenceTypeId,String identificationNo,String identificationType)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "NOT-REPEATED";
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{ 
				String where = "";
				if(identificationType.equalsIgnoreCase("DL"))
				{
					where = " a.`License_Id`=?";
				}
				else if(identificationType.equalsIgnoreCase("LL"))
				{
					where = " a.`Learner_License_Id`=?";
				}
				else if(identificationType.equalsIgnoreCase("CID"))
				{
					where = " a.`Customer_Id`=?";
				}
				
				pst = conn.prepareStatement(CHECK_DOUBLE_OFFENCE+where);
				pst.setString(1, offenceTypeId);
				pst.setString(2, identificationNo);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
					result = "REPEATED";
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at CommonDAO[getRenewalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	
	public EralisCommonDTO getOffenceDtls(String offenceId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{ 
				pst = conn.prepareStatement(GET_OFFENCE_DTLS);
				pst.setString(1, offenceId);
				rs = pst.executeQuery();
				rs.first();
				dto.setTIN(rs.getString("TIN_No"));
				dto.setTimeOfInspection(rs.getString("Time_Of_Inspection"));
				dto.setOffenceDate(rs.getString("offenceDate"));
				dto.setInspectedBy(rs.getString("Inspected_By"));
				dto.setInspectionType(rs.getString("Inspection_Type"));
				dto.setTrafficBranch(rs.getString("Traffic_Branch"));
				dto.setPlaceOfInspection(rs.getString("Place_Of_Inspection"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setVehicleNo(rs.getString("Vehicle_Number"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setLearnerLicenseId(rs.getString("Learner_License_No"));
				dto.setIsBluebookSeized(rs.getString("Is_Bluebook_Seized"));
				dto.setIsLicenseSeized(rs.getString("Is_License_Seized"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setOffenceId(rs.getString("Offence_Id"));
				dto.setAmount(rs.getString("total_amount"));
				dto.setTrafficBranch(rs.getString("Traffic_Branch"));
				
				
				Format formatter1 = new SimpleDateFormat("yyyy-MM-dd");
				Date today = new Date(System.currentTimeMillis());
				String todayStr = formatter1.format(today);
				
				String sourceDate = rs.getString("Offence_Date");  // Start date
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(sdf.parse(sourceDate)); // parsed date and setting to calendar
				calendar.add(Calendar.DATE, 1);  // number of days to add
				String regDate = sdf.format(calendar.getTime());  // End date
				
				
				int totalDays	=	EralisCommonUtil.dateDifferenceAsInt(regDate,todayStr, "D");
				int totalWeeks	=	totalDays/7;
				dto.setTotalDuration(Integer.toString(totalWeeks));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at CommonDAO[getRenewalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public EralisCommonDTO getRenewalInfoDtls(String renewalInfoId, String type)	throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null, rs1 = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_FOREIGN_DTLS);
				pst.setString(1, renewalInfoId);
				rs = pst.executeQuery();
				rs.first();
				int rowCount1 = rs.getInt("rowCount");
				if(rowCount1 >0)
				{
					dto.setVehicleRegistrationType(rs.getString("Is_Foreign"));
					dto.setVehicleId(rs.getString("Vehicle_Number"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setVehicleNumber(rs.getString("Vehicle_Number"));
					dto.setLicenseNo(rs.getString("License_Number"));
					dto.setName(rs.getString("Owner_Name"));
					dto.setOffenceDate(rs.getString("Offence_Date"));
					dto.setAmount(rs.getString("Amount"));
					dto.setTIN(rs.getString("TIN_No"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));	
					dto.setType("FOREIGN");
				}
				else
				{
					String busType	=	"";
					String registrationDate	=	null;
					pst = conn.prepareStatement(GET_VEHICLE_DTLS);
					pst.setString(1, renewalInfoId);
					rs = pst.executeQuery();
					rs.first();
					dto.setValidUpto(rs.getString("fitnessValidity"));
					dto.setApplicationNo(rs.getString("Application_Number"));
					dto.setAmount(rs.getString("Price"));
					dto.setInitialAmount(rs.getString("Price"));
					dto.setInitialReceiptDate(rs.getString("Receipt_Date"));
					dto.setInitialReceiptNo(rs.getString("Receipt_Number"));
					
					registrationDate = rs.getString("rgndate");
					String vehicleRegistrationType = rs.getString("Vehicle_Registration_Type");
					
					dto.setColorName(rs.getString("colourName"));
					dto.setPassengerBusType(rs.getString("passengerBusType"));
					dto.setLifeSpanExpiryDate(rs.getString("lifeExpiryDate"));
					dto.setOwnerName(rs.getString("ownerName"));
					dto.setCompany(rs.getString("Vehicle_Company_Name"));
					dto.setModel(rs.getString("Vehicle_Model_Name"));
					dto.setChasisNumber(rs.getString("Chassis_Number"));
					dto.setEngineNumber(rs.getString("Engine_Number"));
					dto.setEngineType(rs.getString("Engine_Name"));
					dto.setSeatCapacity(rs.getString("Seat_Capacity"));
					dto.setLoadCapacity(rs.getString("Load_Capacity"));
					dto.setRegistrationDate(rs.getString("rgndate"));
					dto.setVehicleRegistrationType(rs.getString("Vehicle_Registration_Id"));
					dto.setRegionId(rs.getString("Region_Id"));
					dto.setExpiryDate(rs.getString("expdate"));
					dto.setColor(rs.getString("Colour"));
					dto.setStatus(rs.getString("Status"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Id"));
					dto.setVehicleHorsePower(rs.getString("Vehicle_Horse_Power"));
					dto.setVehicleKiloWatt(rs.getString("Vehicle_Kilowatt"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setManufactureYear(rs.getString("Manufacture_Year"));
					dto.setVehicleTypeName(rs.getString("Vehicle_Type_Name"));
					dto.setVehicleId(renewalInfoId);
					dto.setVehicleTypeDesc(rs.getString("vehicleTypeDesc"));
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setType("BHUTANESE");
					// dto.setLatestExpiryDate(rs.getString("fdate"));
					dto.setTransferExpiryDate(rs.getString("tdate"));
					busType	=	rs.getString("bus_type_desc");
				
				dto.setBusTypeId(rs.getString("Bus_Type_Id"));
				dto.setBusTypeDesc(rs.getString("bus_type_desc"));
				int rowCount	=	rs.getInt("rowCount");
				String customerId = rs.getString("Customer_Id");
				
				if (vehicleRegistrationType.equals("P"))
				{
					String clearanceData = checkClearance(rs.getString("Personal_Info_Id"),"P");
					//if(!clearanceData.equalsIgnoreCase("CLEAR") && !type.equalsIgnoreCase("VEHICLE_RENEWAL") && !type.equalsIgnoreCase("OFFENCE"))
					if(!clearanceData.equalsIgnoreCase("CLEAR") && !type.equalsIgnoreCase("OFFENCE")  && !type.equalsIgnoreCase("WITHDRAW_VEH_CANCELLATION"))
					{
						String[] dataArray = clearanceData.split("/");
						dto.setStatus(dataArray[0]);
						if(dataArray[0].equalsIgnoreCase("VEHICLE_OUTSTANDING"))
						{
							/***start****/
							
							String[] dataArrayList = clearanceData.split("~");
							String vehicleNo = "";
							String vehicleType = "";
							for(int i=0;i<dataArrayList.length;i++){
								String[] dataArray1 = dataArrayList[i].split("/");
								dto.setStatus(dataArray[0]);
								if(dataArray[0].equalsIgnoreCase("VEHICLE_OUTSTANDING"))
								{
									vehicleNo = vehicleNo+"/"+dataArray1[1];
									vehicleType = vehicleType+"/"+dataArray1[2];
								}
							}
							
							
							/**end***/
							
							dto.setOutstandingVehicleNo(dataArray[1]);
							dto.setOutstandingVehicleType(dataArray[2]);
						}
						else if(dataArray[0].equalsIgnoreCase("DRIVING_LICENSE_OUTSTANDING"))
						{
							dto.setLicenseNo(dataArray[1]);
						}
					}
					
					
						pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
						pst.setString(1, customerId);
						rs1 = pst.executeQuery();
						rs1.first();
						dto.setCustomerID(rs1.getString("Customer_Id"));
						dto.setName(rs1.getString("NAME"));
						dto.setCID(rs1.getString("CID_Number"));
						dto.setPhone(rs1.getString("Present_Phone_No"));
						dto.setOwnerType("Personal");
						dto.setDzongkhag(rs1.getString("dzongkhag_name"));
						dto.setGewog(rs1.getString("Gewog_Name"));
						dto.setVillage(rs1.getString("Village_Name"));
						dto.setCountry(rs1.getString("Country_Name"));
						dto.setAddress(rs1.getString("Present_Contact_Address"));
						dto.setEmail(rs1.getString("Present_Email"));
						dto.setRegion(rs1.getString("region_name"));
						dto.setNational(rs1.getString("Is_International"));
						dto.setOwner("Personal");
						
						pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
						pst.setString(1, rs1.getString("Customer_Id"));
						rs = pst.executeQuery();
						rs.first();
						rowCount1 = rs.getInt("rowCount");
						if(rowCount1>0)
						{
							dto.setStatus("PENDING_APPLICATION");
							dto.setReason(rs.getString("remarks"));
						}
					
				} 
				else 
				{
					
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();

					String clearanceData = checkClearance(rs.getString("Organization_Info_Id"),"O");
					if(!clearanceData.equalsIgnoreCase("CLEAR") &&   !type.equalsIgnoreCase("OFFENCE")  && !type.equalsIgnoreCase("WITHDRAW_VEH_CANCELLATION"))
					{	
						String[] dataArrayList = clearanceData.split("~");
						String vehicleNo = "";
						String vehicleType = "";
						for(int i=0;i<dataArrayList.length;i++){
							String[] dataArray = dataArrayList[i].split("/");
							dto.setStatus(dataArray[0]);
							if(dataArray[0].equalsIgnoreCase("VEHICLE_OUTSTANDING"))
							{
								vehicleNo = vehicleNo+"/"+dataArray[1];
								vehicleType = vehicleType+"/"+dataArray[2];
							}
						}
						//dto.setVehicleNo(vehicleNo);
						//dto.setVehicleType(vehicleType);
						
						dto.setOutstandingVehicleNo(vehicleNo);
						dto.setOutstandingVehicleType(vehicleType);
						
						return dto;
					}
					
					dto.setOwnerId(rs.getString("Customer_Id"));
					dto.setName(rs.getString("Organization_Name"));
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setOwnerType("Organization");
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Village_Name"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setAddress(rs.getString("Address"));
					
					dto.setMinistry(rs.getString("Ministry_Name"));
					dto.setDepartment(rs.getString("Department_Name"));
					dto.setPrivateCompany(rs.getString("Private_Name"));
					dto.setOwner(rs.getString("Owner_Name"));
					dto.setOwnerTypeDesc(rs.getString("Description"));
				}
				
				pst = conn.prepareStatement(CHECK_IF_VEHICLE_HAS_OFFENCE);
				pst.setString(1, renewalInfoId);
				pst.setString(2, "N");
				rs = pst.executeQuery();
				rs.first();
				int rowCount3 = rs.getInt("rowCount");
				if (rowCount3 > 0) {
					dto.setStatus("HAS_OFFENCE");
				} 
				
				if(rowCount>0)
				{
					pst = conn.prepareStatement(GET_VEHICLE_CANCELLATION_DTLS);
					pst.setString(1, renewalInfoId);
					rs = pst.executeQuery();
					rs.first();
					dto.setCancellationDate(rs.getString("cancellation_date"));
					dto.setReason(rs.getString("Reason"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setStatus("CANCELLED");
				}
				
				if("EMISSION".equalsIgnoreCase(type))
				{
					if(null!=registrationDate)
					{
						String[] regDate = registrationDate.split("-");
						dto.setManufactureYear(regDate[2]);
					}
					
					pst = conn.prepareStatement(CHECK_EMISSION_TEST_RETESTED);
					pst.setString(1, renewalInfoId);
					rs = pst.executeQuery();
					rs.first();
					dto.setTestResult(rs.getString("Test_Result"));
				}
				else if("FITNESS_REPLACEMENT".equalsIgnoreCase(type))
				{
					pst = conn.prepareStatement(GET_FITNESS_VALIDITY);
					pst.setString(1, renewalInfoId);
					rs = pst.executeQuery();
					if (rs.next() ) {
					    dto.setStatus(rs.getString("validityStatus"));
					    dto.setValidUpto(rs.getString("Validity"));
					}
					else
					{
						 dto.setStatus("NO-RECORD");
					}
				}
			}
		}
		}
		catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getRenewalInfoDtls]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public String getFitnessId(String vehicleId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String fitnessId = null;
		
		try {
			conn = ConnectionManager.getConnection();
			
			if(conn != null){
				pst = conn.prepareStatement(GET_FITNESS_ID);
				pst.setString(1, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				
				if(rs.getInt("rowCount") > 0)
					fitnessId = rs.getString("Vehicle_Fitness_Test_Id");
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getFitnessId]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return fitnessId;
	}
	
	
	public List<EralisCommonDTO> getRoutePermitDtls(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> routePermitDtls = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_ROUTE_PERMIT_DTLS);
				pst.setString(1, vo.getVehicleNumber());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setPermitNo(rs.getString("Permit_No"));
					dto.setDateOfissue(rs.getString("Issue_Date"));
					dto.setValidUpto(rs.getString("Validity"));
					dto.setAmount(rs.getString("Amount"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setJourneyPurpose(rs.getString("Journey_Purpose"));
					dto.setRouteBetween(rs.getString("Route_Between"));
					dto.setTo(rs.getString("Route_To"));
					dto.setDriverName(rs.getString("Driver_Name"));
					dto.setLicenseNo(rs.getString("Driver_License_No"));
					dto.setRemarks(rs.getString("Remarks"));
					dto.setRegion(rs.getString("region_name"));
					dto.setBaseoffice(rs.getString("base_office_name"));
					routePermitDtls.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getRoutePermitDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return routePermitDtls;
	}
	

	public List<EralisCommonDTO> getRenewalHistoryList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> renewalHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_RENEWAL_HISTORY_LIST);
				pst.setString(1, vo.getVehicleId());
				pst.setString(2, vo.getVehicleId()); 
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setRenewalDate(rs.getString("Renewal_Date"));
					dto.setExpiryDate(rs.getString("Expiry_Date"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					
					renewalHistory.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getRenewalHistoryDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return renewalHistory;
	}
	public List<EralisCommonDTO> fetchVehCancellationHistroy(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> cancellationHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_VEHICLE_CANCELLATION_HISTORY);
				pst.setString(1, vo.getVehicleId());
				pst.setString(2, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setCancellationdate(rs.getString("Cancellation_Date")); 
					dto.setCancellationReason(rs.getString("Reason")); 
					dto.setWithdrawnReason(rs.getString("Withdrawn_Reason"));
					dto.setWithdrawnDate(rs.getString("Withdrawn_Date"));

					dto.setCanDOCName(rs.getString("Can_Document_Name"));
					dto.setCanDOCPath(rs.getString("Can_Document_Path"));
					dto.setCanDOCUUID(rs.getString("Can_UUID"));
					dto.setWithDOCName(rs.getString("With_Document_Name"));
					dto.setWithDOCPath(rs.getString("With_Document_Path"));
					dto.setWithDOCUUID(rs.getString("With_UUID"));
					
					dto.setStatus(rs.getString("STATUS"));
					cancellationHistory.add(dto);
				}
			}
		}
		catch (Exception e) 
		{e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getRenewalHistoryDtls]:: " + e);
		} finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return cancellationHistory;
	}
	
	//fetchTopDetailsHistory
	public List<EralisCommonDTO> fetchTopDetailsHistory(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> topDetailsHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_TOP_DETAILS_HISTORY);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setTopNumber(rs.getString("TOP_Number")); 
					dto.setRegion(rs.getString("region_name")); 
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setExactLocation(rs.getString("Exact_Location"));
					dto.setDateOfissue(rs.getString("Issue_Date"));
					dto.setExpiryDate(rs.getString("Expiry_Date"));
                    topDetailsHistory.add(dto);
				}
			}
		}
		catch (Exception e) 
		{e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[fetchTopDetailsHistory]:: " + e);
		} finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return topDetailsHistory;
	}
	
	//fetchTopCancellationHistory
	
	public List<EralisCommonDTO> fetchTopCancellationHistory(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> topCancellationHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_TOP_CANCELLATION_HISTORY);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setTopNumber(rs.getString("TOP_Number")); 
					dto.setCancellationdate(rs.getString("Cancellation_Date")); 
					dto.setReason(rs.getString("Reason"));
					dto.setRegion(rs.getString("region_name"));
					dto.setBaseoffice(rs.getString("base_office_name"));
					topCancellationHistory.add(dto);
				}
			}
		}
		catch (Exception e) 
		{e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[fetchTopDetailsHistory]:: " + e);
		} finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return topCancellationHistory;
	}
	
	public List<EralisCommonDTO> getDuplicationHistoryList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> duplicationHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_DUPLICATION_HISTORY_LIST);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setDuplicateIssueDate(rs.getString("Duplication_Date"));
					dto.setReceiptDate(rs.getString("duplicate_receipt_date"));
					dto.setReceiptNo(rs.getString("duplicate_receipt_no"));

					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					
					duplicationHistory.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getDuplicationHistoryList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return duplicationHistory;
	}
	public List<EralisCommonDTO> getTransferHistoryList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> transferHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_TRANSFER_HISTORY_LIST);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setTransferDate(rs.getString("Transfer_Date"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setTransfereeName(rs.getString("transfereeName"));
					dto.setTransferorName(rs.getString("transferorName"));
					dto.setAmount(rs.getString("Amount_Paid"));
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					transferHistory.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getTransferHistoryList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return transferHistory;
	}
	
	public List<EralisCommonDTO> getConversionHistoryList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> conversionHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_CONVERSION_HISTORY_LIST);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setConversionDate(rs.getString("conversionDate"));
					dto.setReceiptDate(rs.getString("receiptDate"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setConvertedFrom(rs.getString("convertedFrom"));
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					conversionHistory.add(dto);
				}
			}
		} 
		catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getConversionHistoryList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return conversionHistory;
	}
	
	public List<EralisCommonDTO> getFitnessHistoryList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> fitnessHistoryList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_FITNESS_HISTORY_LIST);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();
				while (rs.next()) 
				{
					dto = new EralisCommonDTO();
					dto.setTestDate(rs.getString("test_date"));
					dto.setValidUpto(rs.getString("validity"));
					dto.setReceiptDate(rs.getString("receiptDate"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setInspectedBy(rs.getString("Inspected_By"));
					dto.setRegion(rs.getString("regionName"));
					dto.setBaseoffice(rs.getString("baseName"));
					dto.setRemarks(rs.getString("Remarks"));
					fitnessHistoryList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return fitnessHistoryList;
	}

	public List<EralisCommonDTO> getEmissionHistoryList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> emissionHistoryList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_EMISSION_HISTORY_LIST);
				pst.setString(1, vo.getVehicleId());
				rs = pst.executeQuery();

				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setTestDate(rs.getString("testDate"));
					dto.setValidUpto(rs.getString("validity"));
					dto.setDzongkhag(rs.getString("testLocation"));
					dto.setCO(rs.getString("CO"));
					dto.setHSU(rs.getString("HSU"));
					dto.setTestResult(rs.getString("Test_Result"));
					dto.setAmount(rs.getString("Price"));
					dto.setEmissionId(rs.getString("Emission_Test_Id"));
					emissionHistoryList.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return emissionHistoryList;
	}
	public List<EralisCommonDTO> getOffenceHistoryList(String id, String type)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> offenceHistoryList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				String query = GET_OFFENCE_HISTORY_LIST;
				if (type.equalsIgnoreCase("VEHICLE"))
					query = query + " a.`Vehicle_Id` = ? ";
				else if (type.equalsIgnoreCase("LICENSE"))
					query = query + " a.`License_Id` = ? ";
				else if (type.equalsIgnoreCase("LEARNER"))
					query = query + " a.`Learner_License_Id` = ? ";
				pst = conn.prepareStatement(query);
				pst.setString(1, id);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					if(null!=rs.getString("Is_paid"))
					{
						if(rs.getString("Is_paid").equalsIgnoreCase("Y"))
							dto.setIsPaid("Paid");
						else
							dto.setIsPaid("Not Paid");
					}
					if(null!=rs.getString("Inspected_By"))
					{
						if(rs.getString("Inspected_By").equalsIgnoreCase("2"))
							dto.setInspectedBy("Traffic Police");
						else if(rs.getString("Inspected_By").equalsIgnoreCase("TRAFFIC_POLICE"))
							dto.setInspectedBy("Traffic Police");
						else
							dto.setInspectedBy("RSTA");
						
					}
					if(null!=rs.getString("Inspection_Type"))
					{
						if(rs.getString("Inspection_Type").equalsIgnoreCase("1"))
							dto.setInspectionType("Normal");
						else if(rs.getString("Inspection_Type").equalsIgnoreCase("NORMAL"))
							dto.setInspectionType("Normal");
						else
							dto.setInspectionType("Highway");
					}
					dto.setOffenceDate(rs.getString("offense_date"));
					dto.setPlaceOfInspection(rs.getString("Place_Of_Inspection"));
					dto.setTIN(rs.getString("TIN_No"));
					dto.setOffence1(rs.getString("Offence_Name"));
					dto.setAmount(rs.getString("Amount"));
					dto.setTimeOfInspection(rs.getString("Time_Of_inspection"));
					dto.setTrafficBranch(rs.getString("Traffic_Branch"));

					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					offenceHistoryList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return offenceHistoryList;
	}
	public List<EralisCommonDTO> getStatusList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet vs = null;
		List<EralisCommonDTO> status = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String queries = null;
		PreparedStatement query = null;
		String vehicleRegistrationType = "";
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement("SELECT r.Vehicle_Type_Id,r.Vehicle_Reg_Dtls_Id,r.Vehicle_Registration_Type,r.Customer_Id,"
								+ "r.Engine_Number,r.Chassis_Number,r.Vehicle_Number FROM t_vehicle_registration_dtls r");

				vs = pst.executeQuery();
				while (vs.next()) {
					dto = new EralisCommonDTO();
					if (vs.getString("Vehicle_Registration_Type").equalsIgnoreCase("P")) {
						queries = "SELECT m.Vehicle_Type_Name,CONCAT( p.Middle_Name,p.Last_Name) AS ownerName FROM t_personal_dtls p "
								+ "LEFT JOIN t_vehicle_type_master m ON m.Vehicle_Type_Id=? "
								+ " WHERE p.Customer_Id=?";
						query = conn.prepareStatement(queries);
						vehicleRegistrationType = "Personal";
					} else if (vs.getString("Vehicle_Registration_Type").equalsIgnoreCase("O")) {
						query = conn.prepareStatement("SELECT m.Vehicle_Type_Name,o.Organization_Name AS ownerName "
										+ "FROM t_organization_info o "
										+ "LEFT JOIN t_vehicle_type_master m ON m.Vehicle_Type_Id=? "
										+ "WHERE o.Customer_Id=?");
						vehicleRegistrationType = "Organization";
					}
					query.setString(1, vs.getString("Vehicle_Type_Id"));
					query.setString(2, vs.getString("Customer_Id"));
					rs = query.executeQuery();
					rs.first();
					dto.setName(rs.getString("ownerName"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
					dto.setVehicleNo(vs.getString("Vehicle_Number"));
					dto.setVehicleRegistrationType(vehicleRegistrationType);
					dto.setEngineNumber(vs.getString("Engine_Number"));
					dto.setChasisNumber(vs.getString("Chassis_Number"));
					status.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getStatusList]:: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return status;
	}
	public List<EralisCommonDTO> getPermitInfoList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> permitList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_PERMIT_DETAILS_LIST);
				pst.setString(1, vo.getDrivername());
				pst.setString(2, vo.getPermitno());
				pst.setString(3, vo.getRegion());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setDriverName(rs.getString("Driver_Name"));
					dto.setPermitID(rs.getString("Permit_No"));

					dto.setPermitDetailsId(rs.getString("Permit_Details_Id"));
					permitList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getPermitInfoList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return permitList;
	}
	public EralisCommonDTO getPermitInfoDtls(String permitDetailsId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_PERMIT_DETAILS);
				pst.setString(1, permitDetailsId);
				rs = pst.executeQuery();
				rs.first();
				dto.setPermitID(rs.getString("Permit_No"));
				dto.setDriverName(rs.getString("Driver_Name"));
				dto.setIdentificationMarks(rs.getString("Driver_Identification_Mark"));
				dto.setAddress(rs.getString("Driver_Address"));
				dto.setPhone(rs.getString("Driver_Phone_No"));
				dto.setRegistrationNo(rs.getString("Registration_No"));
				dto.setRegion(rs.getString("Region_Id"));
				dto.setBaseoffice(rs.getString("Base_Office_Id"));
				dto.setRouteBetween(rs.getString("Route_Between"));
				dto.setTo(rs.getString("Route_To"));
				dto.setVehicleType(rs.getString("Vehicle_Type_Id"));
				dto.setCarryingCapacity(rs.getString("Carrying_Capacity"));
				dto.setSeatCapacity(rs.getString("Seat_Capacity"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setDateOfissue(rs.getString("issuedate"));
				dto.setValidUpto(rs.getString("validity"));
				dto.setRemarks(rs.getString("Remarks"));
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getPermitInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public List<TokenCounterDTO> getCounterList(String jurisTypeId, String jurisId, String userId) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<TokenCounterDTO> dataList = new ArrayList<TokenCounterDTO>();
		try {
			TokenCounterDTO dto;
			conn = ConnectionManager.getOtsDbConnection();
			if (conn != null)
			{
				String customerId = null;
				pst = conn.prepareStatement(GET_LIST_COUNTERS_TOKEN);
				pst.setString(1, jurisTypeId);
				pst.setString(2, jurisId);
				pst.setString(3, userId);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new TokenCounterDTO();
					dto.setId(rs.getString("id"));
					dto.setAppointment_time_from(rs.getString("appointment_time_from"));
					dto.setAppointment_time_to(rs.getString("appointment_time_to"));
					dto.setCustomer_id(rs.getString("customer_id"));
					dto.setTransaction_type(rs.getString("transaction_type"));
					dto.setService_type(rs.getString("service_type"));
					dto.setIdentity_number(rs.getString("identity_number"));
					dto.setToken_no(rs.getString("token_no"));
					dataList.add(dto);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dataList;
	}
			
			
			
			

	public List<MasterDTO> getMasterTableList(String identifier)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = null;
		List<MasterDTO> masterList = new ArrayList<MasterDTO>();
		MasterDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				if (identifier.equalsIgnoreCase("master_region"))
					query = MasterSQLConstants.GET_REGION_DATA;
				else if (identifier.equalsIgnoreCase("master_dzongkhag"))
					query = MasterSQLConstants.GET_DZONGKHAG_DATA;
				else if (identifier.equalsIgnoreCase("master_gewog"))
					query = MasterSQLConstants.GET_GEWOG_DATA;
				else if (identifier.equalsIgnoreCase("master_ministry"))
					query = MasterSQLConstants.GET_MINISTRY_DATA;
				else if (identifier.equalsIgnoreCase("master_department"))
					query = MasterSQLConstants.GET_DEPARTMENT_DATA;
				else if (identifier.equalsIgnoreCase("master_country"))
					query = MasterSQLConstants.GET_COUNTRY_DATA;
				else if (identifier.equalsIgnoreCase("master_offence"))
					query = MasterSQLConstants.GET_OFFENCE_TYPE_DATA;
				else if (identifier.equalsIgnoreCase("master_accident"))
					query = MasterSQLConstants.GET_ACCIDENT_CAUSE_DATA;
				else if (identifier.equalsIgnoreCase("master_action"))
					query = MasterSQLConstants.GET_ACTION_TAKEN_DATA;
				else if (identifier.equalsIgnoreCase("master_designation"))
					query = MasterSQLConstants.GET_DESIGNATION_DATA;
				else if (identifier.equalsIgnoreCase("master_drive"))
					query = MasterSQLConstants.GET_DRIVETYPE_DATA;
				else if (identifier.equalsIgnoreCase("master_engine"))
					query = MasterSQLConstants.GET_ENGINETYPE_DATA;
				else if (identifier.equalsIgnoreCase("master_hypo"))
					query = MasterSQLConstants.GET_HYPOTHECATED_DATA;
				else if (identifier.equalsIgnoreCase("master_occupation"))
					query = MasterSQLConstants.GET_OCCUPATION_DATA;
				else if (identifier.equalsIgnoreCase("master_owner"))
					query = MasterSQLConstants.GET_OWNERTYPE_DATA;
				else if (identifier.equalsIgnoreCase("master_courtesy"))
					query = MasterSQLConstants.GET_COURTESY_DATA;
				else if (identifier.equalsIgnoreCase("master_company"))
					query = MasterSQLConstants.GET_VEHICLE_COMPANY_DATA;
				else if (identifier.equalsIgnoreCase("master_model"))
					query = MasterSQLConstants.GET_VEHICLE_MODEL_DATA;
				else if (identifier.equalsIgnoreCase("master_vehicle_type"))
					query = MasterSQLConstants.GET_VEHICLE_TYPE_DATA;
				else if (identifier.equalsIgnoreCase("master_registration_code"))
					query = MasterSQLConstants.GET_REGISTRATION_CODE_DATA;
				else if (identifier.equalsIgnoreCase("master_dealer"))
					query = MasterSQLConstants.GET_DEALER_DATA;
				else if (identifier.equalsIgnoreCase("master_cancellation_reasons"))
					query = MasterSQLConstants.GET_REASON_DATA;
				else if (identifier.equalsIgnoreCase("master_base_office"))
					query = MasterSQLConstants.GET_BASE_OFFICE_DATA;
				else if (identifier.equalsIgnoreCase("master_blood_group"))
					query = MasterSQLConstants.GET_BLOOD_GROUP_DATA;
				else if (identifier.equalsIgnoreCase("master_practical_test_criteria"))
					query = MasterSQLConstants.GET_PRACTICAL_CRITERIA_DATA;
				else if (identifier.equalsIgnoreCase("master_village"))
					query = MasterSQLConstants.GET_VILLAGE_DATA;
				else if (identifier.equalsIgnoreCase("master_colour"))
					query = MasterSQLConstants.GET_COLOUR_MASTER_DATA;
				else if (identifier.equalsIgnoreCase("master_diplomats"))
					query = MasterSQLConstants.DIPLOMAT_DATA;
				else if (identifier.equalsIgnoreCase("master_private_company"))
					query = MasterSQLConstants.GET_PRIVATE_COMPANY_MASTER;
				else if (identifier.equalsIgnoreCase("master_route"))
					query = MasterSQLConstants.GET_ROUTE_MASTER;
				
				
				
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new MasterDTO();
					dto.setId(rs.getString("HEADER_ID"));
					dto.setName(rs.getString("HEADER_NAME"));
					if (identifier.equalsIgnoreCase("master_region")
							|| identifier.equalsIgnoreCase("master_ministry")
							|| identifier.equalsIgnoreCase("master_country")
							|| identifier.equalsIgnoreCase("master_accident")
							|| identifier.equalsIgnoreCase("master_drive")
							 || identifier.equalsIgnoreCase("master_colour")
							 || identifier.equalsIgnoreCase("master_diplomats")) {
						dto.setDesc(rs.getString("HEADER_DESC"));
						if (identifier.equalsIgnoreCase("master_region"))
							dto.setNumber(rs.getString("NUMBER"));
					} else if (identifier.equalsIgnoreCase("master_dzongkhag")
							|| identifier.equalsIgnoreCase("master_gewog")
							|| identifier.equalsIgnoreCase("master_department")
							|| identifier.equalsIgnoreCase("master_base_office")
							|| identifier.equalsIgnoreCase("master_village")) {
						dto.setOtherId(rs.getString("OTHER_ID"));
						dto.setOtherName(rs.getString("OTHER_NAME"));
					} else if (identifier.equalsIgnoreCase("master_offence")
							|| identifier.equalsIgnoreCase("master_vehicle_type")
							|| identifier.equalsIgnoreCase("master_practical_test_criteria")) {
						dto.setNumber(rs.getString("NUMBER"));
					} else if (identifier.equalsIgnoreCase("master_model")) {
						dto.setOtherId(rs.getString("OTHER_ID"));
						dto.setOtherName(rs.getString("OTHER_NAME"));
						dto.setEngineCC(rs.getString("Engine_CC_Kilowatt_Horsepower"));
						dto.setSeatCapacity(rs.getString("Seating_Capacity"));
						dto.setUnladenWeight(rs.getString("Unladen_Weight"));
					}
					masterList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return masterList;
	}
	public List<LicenseDTO> getLearnerLicenseInfoList(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		List<LicenseDTO> licenseList = new ArrayList<LicenseDTO>();
		try {
			int count = 1;
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				// CHECKING NON EMPTY VARIABLE  
				if(!dto.getFormType().equalsIgnoreCase("LEARNER_HISTORY"))
				{
					where = "  AND p.`Customer_Id` NOT IN (SELECT `Customer_Id` FROM `t_driving_license_dtls`) ";
				}
				if (!dto.getCID().equalsIgnoreCase("NA")) {
					where = where+" AND p.CID_Number = ? ";
				}
				if (!dto.getLearnerlicenseno().equalsIgnoreCase("NA")) {
					where = where+" AND dl.Learner_License_No = ? ";
				}
				query = GET_LICENSE_HOLDER_DETAILS1;
				pst = conn.prepareStatement(query + where);
				// CLAUSE VALUE
				if (!dto.getCID().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getCID());
					count = count + 1;
				}
				if (!dto.getLearnerlicenseno().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getLearnerlicenseno());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setCID(rs.getString("CID_Number"));
					if (rs.getString("Middle_Name").equalsIgnoreCase("")) {
						dto.setName(rs.getString("First_Name") + " "+ rs.getString("Last_Name"));
					} else {
						dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
					}
					dto.setLicenseNo(rs.getString("Learner_License_No"));
					dto.setRegion(rs.getString("region_name"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					licenseList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getLearnerLicenseInfoList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseList;
	}
	public EralisCommonDTO getlearnerRenewalInfoDtls(String learnerlicenseInfoId,String searchType) throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {

			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				String customerId = null;
				pst = conn.prepareStatement(GET_LICENSE_DETAILS);
				pst.setString(1, learnerlicenseInfoId);
				rs = pst.executeQuery();	
				rs.first();
				customerId = rs.getString("Customer_Id");
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setReceiptDate(rs.getString("Receipt_Date"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				 
				dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setAddress(rs.getString("Present_Contact_Address"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setCountry(rs.getString("Country_Name"));
				dto.setTitleOfcourtesy(rs.getString("Title_Of_Courtesy_Id"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setCustomerID(rs.getString("Customer_Id"));
				dto.setBloodGroup(rs.getString("blood_group_type"));
				dto.setDOB(rs.getString("dob"));
				dto.setNationality(rs.getString("Nationality"));
				dto.setFathersName(rs.getString("Fathers_Name"));
				dto.setIdentificationMarks(rs.getString("Identification_Mark"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setBloodGroupId(rs.getString("Blood_Group_Id"));
				dto.setGender(rs.getString("Gender"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setRegion(rs.getString("regionName"));
				dto.setIsInternational(rs.getString("Is_International"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setLLNo(rs.getString("Learner_License_No"));
				dto.setDateOfissue(rs.getString("IssueDate"));
				dto.setExpiryDate(rs.getString("ExpiryDate"));
				String expiryDate = rs.getString("ExpiryDate");
				//SimpleDateFormat stringDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				
				
				dto.setPhone(rs.getString("Present_Phone_No"));
				dto.setLearnerLicenseId(rs.getString("Learner_License_Info_Id"));
				
				pst = conn.prepareStatement(CHECK_IF_LEARNER_LICENSE_HAS_OFFENCE);
				pst.setString(1, dto.getLearnerLicenseId());
				rs = pst.executeQuery();
				rs.first();  
				
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0) {
					dto.setStatus("HAS_OFFENCE");
				} else {
					pst = conn.prepareStatement(CHECK_LEARNER_CANCELLATION);
					pst.setString(1, dto.getLearnerLicenseId());
					rs = pst.executeQuery();
					rs.first();
					rowCount = rs.getInt("rowCount");
					if (rowCount > 0)
					{
						dto.setStatus("LEARNER_CANCELLED");
						dto.setCancellationReason(rs.getString("reason"));
						dto.setCancellationDate(rs.getString("canelledDate"));
					}
					else
					{
						pst = conn.prepareStatement(CHECK_LEARNER_SUSPENSE);
						pst.setString(1, dto.getLearnerLicenseId());
						rs = pst.executeQuery();
						rs.first();
						rowCount = rs.getInt("rowCount");
						if (rowCount > 0)
						{
							dto.setStatus("LEARNER_SUSPENDED");
						} else
						{
							dto.setStatus("SUCCESS");
							
							pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
							int rowCount1 = rs.getInt("rowCount");
							if(rowCount1>0)
							{
								dto.setStatus("PENDING_APPLICATION");
								dto.setCancellationReason(rs.getString("remarks"));
							}
							if(!searchType.equalsIgnoreCase("LL_RENEWAL") && !searchType.equalsIgnoreCase("ENDORSEMENT") && !searchType.equalsIgnoreCase("renew_learner_search"))
							{
								
								SimpleDateFormat stringDateFormat = new SimpleDateFormat("yyyy-MM-dd");
								java.util.Date stringExpiryDate = stringDateFormat.parse(expiryDate);
								Date today = new Date(System.currentTimeMillis());
								
								int dateDiff = today.compareTo(stringExpiryDate);
								if(dateDiff==1)
								{
									dto.setStatus("LICENSE_EXPIRED");
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getlearnerRenewalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public String bookedTestDtls(String applicationNo) throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		String testDtls=null;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_BOOKED_TEST_DTLS);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				testDtls = rs.getString("Drive_Type_Id")+"#"+rs.getString("Test_Location_Id")+"#"+rs.getString("testDate")+"#"+rs.getString("Juris_Type_Id");
				 
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlearnerRenewalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return testDtls;
	}
	public List<EralisCommonDTO> getlicenserenewalList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> licenseRenewalList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String query = "", Customer_Id = "";
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				if (!("NA".equals(vo.getName()))) {
					query = "SELECT count(*) rowCount, a.Customer_Id FROM t_personal_dtls a WHERE a.First_Name LIKE '%"+ vo.getName() + "%';";
					pst = conn.prepareStatement(query);
					rs = pst.executeQuery();
					rs.first();
					if (rs.getInt("rowCount") > 0)
						Customer_Id = rs.getString("Customer_Id");
					pst = conn.prepareStatement(GET_DRIVING_LICENSE_DETAILS_LIST);
					pst.setString(1, Customer_Id);
					rs = pst.executeQuery();
					while (rs.next()) {
						dto = new EralisCommonDTO();
						dto.setName(rs.getString("name"));
						dto.setCID(rs.getString("CID_Number"));
						dto.setCustomerID(rs.getString("Customer_Id"));
						dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
						licenseRenewalList.add(dto);
					}
				} else if (!("NA").equals(vo.getCidNumber())) {
					query = "SELECT COUNT(*) rowCount, a.Customer_Id FROM t_personal_dtls a WHERE a.CID_Number="+ vo.getCidNumber();
					pst = conn.prepareStatement(query);
					rs = pst.executeQuery();
					rs.first();
					if (rs.getInt("rowCount") > 0)
						Customer_Id = rs.getString("Customer_Id");
					else
						Customer_Id = "NA";
					pst = conn.prepareStatement(GET_DRIVING_LICENSE_DETAILS_LIST);
					pst.setString(1, Customer_Id);
					rs = pst.executeQuery();

					while (rs.next()) {
						dto = new EralisCommonDTO();
						dto.setName(rs.getString("name"));
						dto.setCID(rs.getString("CID_Number"));
						dto.setCustomerID(rs.getString("Customer_Id"));
						dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
						licenseRenewalList.add(dto);
					}
				} else {
					pst = conn.prepareStatement(GET_DRIVING_LICENSE_DETAILS);
					pst.setString(1, vo.getLicenseno());
					pst.setString(2, vo.getRegion());
					pst.setString(3, vo.getLicensetype());
					pst.setString(4, vo.getName());
					pst.setString(5, vo.getCidNumber());
					rs = pst.executeQuery();
					while (rs.next()) {
						dto = new EralisCommonDTO();
						dto.setLicenseNo(rs.getString("Driving_License_No"));
						dto.setRegion(rs.getString("Region_Id"));
						dto.setLicensetype(rs.getString("Driving_License_Type_Id"));
						dto.setName(rs.getString("name"));
						dto.setCID(rs.getString("CID_Number"));
						dto.setLearnerlicenseInfoId(rs.getString("Driving_License_Id"));
						licenseRenewalList.add(dto);
					}
				}
			}
		}

		catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlicenserenewalList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseRenewalList;
	}
	public EralisCommonDTO getlicenserenewalDtls(String drivinglicenseId)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVING_LICENSE);
				pst.setString(1, drivinglicenseId);
				rs = pst.executeQuery();
				rs.first();
				dto.setCustomerID(rs.getString("Customer_Id"));
				dto.setName(rs.getString("NAME"));
				dto.setBloodGroup(rs.getString("blood_group_type"));
				dto.setDOB(rs.getString("dob"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setGender(rs.getString("Gender"));
				dto.setFathersName(rs.getString("Fathers_Name"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setVillage(rs.getString("Village_Name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setRegion(rs.getString("Region_Id"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlicenserenewalDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public List<LicenseDTO> getlearnerlicense(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> learnerLicenseRenewalList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LEARNER_LICENSE);
				pst.setString(1, vo.getLearnerLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setApplicationNo(rs.getString("Application_Number"));
					dto.setReceiptDate(rs.getString("receipt"));
					dto.setExpiryDate(rs.getString("expiry"));
					dto.setRenewaldate(rs.getString("Renewal_Date"));
					dto.setRemarks(rs.getString("Remarks"));
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					learnerLicenseRenewalList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlearnerlicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return learnerLicenseRenewalList;
	}
	public List<LicenseDTO> getlearnerduplicationlist(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> learnerLicenseRenewalList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LEARNER_LICENSE_DUPLICATION);
				pst.setString(1, vo.getLearnerLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setReceiptDate(rs.getString("receipt"));
					dto.setDeliveredon(rs.getString("delivered"));
					dto.setIssuedate(rs.getString("issue"));
					dto.setAmount(rs.getString("amount_paid"));
					dto.setReceiptNo(rs.getString("learner_receipt_no"));
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					learnerLicenseRenewalList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlearnerlicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return learnerLicenseRenewalList;
	}
	public List<EralisCommonDTO> getEndorsementLicenseDetails(ApplicationDataVO vo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> personalList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		String where = null;
		int count = 1;
		String query = null;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				if (!vo.getCidNumber().equalsIgnoreCase("NA")) {
					where = " p.CID_Number = ? ";
				}
				if (!vo.getCustomerId().equalsIgnoreCase("NA") && !vo.getCidNumber().equalsIgnoreCase("NA")) {
					where = where + " AND p.Customer_Id = ? ";
				} else if (!vo.getCustomerId().equalsIgnoreCase("NA")) {
					where = " p.Customer_Id = ? ";
				}
				if (!vo.getDzongkhag().equalsIgnoreCase("NA") && (!vo.getCidNumber().equalsIgnoreCase("NA") || !vo.getCustomerId().equalsIgnoreCase("NA"))) 
				{
					where = where + " AND p.Permanent_Dzongkhag_Id = ? ";
				} else if (!vo.getDzongkhag().equalsIgnoreCase("NA")) {
					where = " p.Permanent_Dzongkhag_Id = ? ";
				}
				if (!vo.getName().equalsIgnoreCase("NA") && (!vo.getDzongkhag().equalsIgnoreCase("NA") || !vo.getCidNumber().equalsIgnoreCase("NA") || !vo.getCustomerId().equalsIgnoreCase("NA"))) {
					where = where + " AND p.First_Name LIKE ? ";
				} else if (!vo.getName().equalsIgnoreCase("NA")) {
					where = "  p.First_Name LIKE ? ";
				}
				if (!vo.getRegion().equalsIgnoreCase("NA") && vo.getDzongkhag().equalsIgnoreCase("NA")) {
					if (!vo.getRegion().equalsIgnoreCase("NA") && vo.getDzongkhag().equalsIgnoreCase("NA") && (!vo.getName().equalsIgnoreCase("NA")|| !vo.getCidNumber().equalsIgnoreCase("NA") || !vo.getCustomerId().equalsIgnoreCase("NA"))) {
						where = where + " AND d.region_id=? ";
					} else if (!vo.getRegion().equalsIgnoreCase("NA")) {
						where = "  d.region_id=? ";
					}
					query = GET_REGION_PERSONAL_DETAILS_LIST;
				} else {
					query = GET_PERSONAL_LIST;
				}
				pst = conn.prepareStatement(query + where);
				if (!vo.getCidNumber().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getCidNumber());
					count = count + 1;
				}
				if (!vo.getCustomerId().equalsIgnoreCase("NA") && !vo.getCidNumber().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getCustomerId());
					count = count + 1;
				} else if (!vo.getCustomerId().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getCustomerId());
					count = count + 1;
				}
				if (!vo.getDzongkhag().equalsIgnoreCase("NA") && (!vo.getCidNumber().equalsIgnoreCase("NA") || !vo.getCustomerId().equalsIgnoreCase("NA"))) {
					pst.setString(count, vo.getDzongkhag());
					count = count + 1;
				} else if (!vo.getDzongkhag().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getDzongkhag());
					count = count + 1;
				}
				if (!vo.getName().equalsIgnoreCase("NA") && (!vo.getDzongkhag().equalsIgnoreCase("NA") || !vo.getCidNumber().equalsIgnoreCase("NA") || !vo.getCustomerId().equalsIgnoreCase("NA"))) {
					pst.setString(count, vo.getName());
					count = count + 1;
				} else if (!vo.getName().equalsIgnoreCase("NA")) {
					pst.setString(count, vo.getName());
					count = count + 1;
				}
				if (!vo.getRegion().equalsIgnoreCase("NA") && vo.getDzongkhag().equalsIgnoreCase("NA")) {
					if (!vo.getRegion().equalsIgnoreCase("NA") && vo.getDzongkhag().equalsIgnoreCase("NA") && (!vo.getName().equalsIgnoreCase("NA") || !vo.getCidNumber().equalsIgnoreCase("NA") || !vo.getCustomerId().equalsIgnoreCase("NA"))) {
						pst.setString(count, vo.getRegion());
						count = count + 1;
					} else if (!vo.getRegion().equalsIgnoreCase("NA")) {
						pst.setString(count, vo.getRegion());
						count = count + 1;
					}
				}
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
					dto.setCID(rs.getString("CID_Number"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					dto.setBloodGroup(rs.getString("blood_group_type"));
					dto.setRegion(rs.getString("region_name"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					personalList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return personalList;
	}
	public LicenseDTO getlearnerlicenserenewalList(String learnerlicneseno)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		LicenseDTO dto = new LicenseDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LEARNER_LICENSE_RENEWAL_DETAILS);
				pst.setString(1, learnerlicneseno);
				rs = pst.executeQuery();
				rs.first();
				dto.setReceiptDate(rs.getString("receipt"));
				dto.setExpiryDate(rs.getString("expiry"));
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlearnerlicenserenewalList]:: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public List<LicenseDTO> getdrivinglicense(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseRenewalList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVING_LICENSE_RENEWAL_LISTS);
				pst.setString(1, vo.getDrivingLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setRenewaldate(rs.getString("renewal"));
					dto.setReceiptDate(rs.getString("receipt"));
					dto.setExpiryDate(rs.getString("expiry"));
					dto.setDeliveredon(rs.getString("delivered"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setFileUUID(rs.getString("UUID"));
					dto.setFileName(rs.getString("Document_Name"));
					licenseRenewalList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseRenewalList;
	}

	public List<LicenseDTO> getListOfTop(String customerId)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> topList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LIST_OF_TOP);
				pst.setString(1, customerId);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setRegion(rs.getString("region"));
					dto.setBaseoffice(rs.getString("base"));
					dto.setIssuedate(rs.getString("Issue_Date"));
					dto.setExpiryDate(rs.getString("Expiry_Date"));
					dto.setAmount(rs.getString("Amount"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setTopNo(rs.getString("TOP_Number"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setVehicleCompany(rs.getString("vehicleCompany"));
					dto.setVehicleModel(rs.getString("vehicleModel"));
					dto.setVehicleColor(rs.getString("Colour"));
					dto.setEngineNumber(rs.getString("Engine_Number"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setChassisNumber(rs.getString("Chassis_Number"));
					dto.setEngineType(rs.getString("engineType"));
					dto.setSeatingCapacity(rs.getString("Seat_Capacity"));
					if(rs.getInt("topCancelled")>0)
					{
						dto.setStatus("Cancelled");
					}
					else
					{
						dto.setStatus("Active");
					}
					topList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return topList;
	}

	public List<LicenseDTO> getTopReplacementHistory(String searchBy,String indentifer)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> topList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LIST_OF_TOP);
				pst.setString(1, indentifer);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setRegion(rs.getString("region"));
					dto.setBaseoffice(rs.getString("base"));
					dto.setIssuedate(rs.getString("Issue_Date"));
					dto.setExpiryDate(rs.getString("Expiry_Date"));
					dto.setAmount(rs.getString("Amount"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setTopNo(rs.getString("TOP_Number"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setVehicleCompany(rs.getString("vehicleCompany"));
					dto.setVehicleModel(rs.getString("vehicleModel"));
					dto.setVehicleColor(rs.getString("Colour"));
					dto.setEngineNumber(rs.getString("Engine_Number"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setChassisNumber(rs.getString("Chassis_Number"));
					dto.setEngineType(rs.getString("engineType"));
					dto.setSeatingCapacity(rs.getString("Seat_Capacity"));
					if(rs.getInt("topCancelled")>0)
					{
						dto.setStatus("Cancelled");
					}
					else
					{
						dto.setStatus("Active");
					}
					topList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return topList;
	}
	
	
	//getTopReplacementHistory
	
	
	public List<LicenseDTO> getdrivinglicenseduplication(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseDuplicationList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVING_LICENSE_DUPLICATION_LISTS);
				pst.setString(1, vo.getDrivingLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setDuplicationdate(rs.getString("duplication"));
					dto.setReceiptDate(rs.getString("receipt"));
					dto.setDeliveredon(rs.getString("delivered"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					licenseDuplicationList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseDuplicationList;
	}
	
	public List<LicenseDTO> getlicenseEndorsementList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseEndorsementList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				pst = conn.prepareStatement(GET_CUSTOMER_ID);
				pst.setString(1, vo.getDrivingLicenseId());
				rs = pst.executeQuery();
				rs.first();
				String customerId = rs.getString("Customer_Id");
				pst = conn.prepareStatement(GET_DRIVING_LICENSE_ENDORSEMENT_LISTS);
				pst.setString(1, customerId);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					dto.setEndorseddate(rs.getString("endorseddate"));
					dto.setReceiptDate(rs.getString("receipt"));
					dto.setDeliveredon(rs.getString("delivered"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setIid(rs.getString("IID"));
					dto.setDrivetype(rs.getString("Drive_Type_Name"));
					dto.setLicenseNo(rs.getString("drivingLicenseNo"));
					licenseEndorsementList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseEndorsementList;
	}
	
	public List<LicenseDTO> getsuspensionlist(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseSuspensionList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				if(vo.getFormType().equalsIgnoreCase("LL"))
				{
					pst = conn.prepareStatement(GET_LEARNER_SUSPENSION_LIST);
					pst.setString(1, vo.getDrivingLicenseId());
				}
				else if(vo.getFormType().equalsIgnoreCase("DL"))
				{
					pst = conn.prepareStatement(GET_LICENSE_SUSPENSION_LISTS);
					pst.setString(1, vo.getDrivingLicenseId());
				}
				
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setStartDate(rs.getString("startDate"));
					dto.setEndDate(rs.getString("endDate"));
					dto.setReissuedate(rs.getString("reIssueDate"));
					dto.setReason(rs.getString("Reason_For_Suspension"));
					dto.setStatus(rs.getString("suspendStatus"));
					dto.setReason(rs.getString("Reason_For_Suspension"));

					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					//dto.setSuspensionName(rs.getString("Document_Name"));
					//dto.setSuspensionPath(rs.getString("Document_Path"));
					//dto.setSuspensionType(rs.getString("Document_Type"));
					//dto.setSuspensionUUID(rs.getString("UUID"));
					licenseSuspensionList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getsuspensionlist]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseSuspensionList;
	}
	public List<LicenseDTO> getdrivetypeSuspensionList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> drivetypeSuspensionList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVE_TYPE_SUSPENSION_LISTS);
				pst.setString(1, vo.getDrivingLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setStartDate(rs.getString("startDate"));
					dto.setEndDate(rs.getString("endDate"));
					dto.setReissuedate(rs.getString("reIssueDate"));
					dto.setDrivetype(rs.getString("Drive_Type_Name"));

					dto.setFileName(rs.getString("Document_Name"));
					dto.setFileUUID(rs.getString("UUID"));
					drivetypeSuspensionList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return drivetypeSuspensionList;
	}
	public List<LicenseDTO> getLicenseCancellationHistory(ApplicationDataVO vo)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseCancellation = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_LICENSE_CANCELLATION_HISTORY);
				pst.setString(1, vo.getDrivingLicenseId());
				pst.setString(2, vo.getDrivingLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) 
				{
					dto = new LicenseDTO();
					dto.setCancellationdate(rs.getString("Cancellation_Date")); 
					dto.setCancellationReason(rs.getString("Cancellation_Reason")); 
					dto.setWithdrawnReason(rs.getString("Withdrawn_Reason"));
					dto.setWithdrawnDate(rs.getString("Withdrwan_Date"));
					

					dto.setCanDOCName(rs.getString("Can_Document_Name"));
					dto.setCanDOCPath(rs.getString("Can_Document_Path"));
					dto.setCanDOCUUID(rs.getString("Can_UUID"));
					dto.setWithDOCName(rs.getString("With_Document_Name"));
					dto.setWithDOCPath(rs.getString("With_Document_Path"));
					dto.setWithDOCUUID(rs.getString("With_UUID"));
					
					dto.setStatus(rs.getString("STATUS"));
					licenseCancellation.add(dto);
				}
			}
		}
		catch (Exception e) 
		{
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseCancellation;
	}
	
	public List<LicenseDTO> license_suspension_list(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		String formType	=	"";
		List<LicenseDTO> licenseList = new ArrayList<LicenseDTO>();
		try {
			int count = 1;
			conn = ConnectionManager.getConnection();
			if (conn != null) {
					pst = conn.prepareStatement(GET_LICENSE_OFFENCE_DTLS_FORIEGN);
					pst.setString(1, dto.getLicenseNo());
					rs = pst.executeQuery();
					rs.first();
					int rowCount = rs.getInt("rowCount");
					if(rowCount>0)
					{
						dto.setOffenceId(rs.getString("Offence_Id"));
						dto.setVehicleNo(rs.getString("Vehicle_Number"));
						dto.setLicenseNo(rs.getString("License_Number"));
						dto.setDrivinglicenseId(rs.getString("Offence_Id"));
						licenseList.add(dto);
					}
					else
					{
					
					formType = dto.getFormType();
					// CHECKING NON EMPTY VARIABLE
					if (!dto.getCID().equalsIgnoreCase("NA")) {
						where = where + " AND p.CID_Number = ? ";
					}
					if (!dto.getLicenseNo().equalsIgnoreCase("NA")) {
						where = where + " AND dl.Driving_License_No = ? ";
					}
					query = GET_LICENSE_HOLDER_DETAILS;
					pst = conn.prepareStatement(query + where);
					// CLAUSE VALUE
					if (!dto.getCID().equalsIgnoreCase("NA")) {
						pst.setString(count, dto.getCID());
						count = count + 1;
					}
					if (!dto.getLicenseNo().equalsIgnoreCase("NA")) {
						pst.setString(count, dto.getLicenseNo());
						count = count + 1;
					}
					rs = pst.executeQuery();
					while (rs.next()) 
					{
						dto = new LicenseDTO();
						dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
						dto.setCID(rs.getString("CID_Number"));
						if (rs.getString("Middle_Name").equalsIgnoreCase("")) {
							dto.setName(rs.getString("First_Name") + " "+ rs.getString("Last_Name"));
						} else {
							dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
						}
						dto.setLicenseNo(rs.getString("Driving_License_No"));
						dto.setRegion(rs.getString("region_name"));
						dto.setDzongkhag(rs.getString("dzongkhag_name"));
						dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
						dto.setIssuedate(rs.getString("Issue_Date"));
						dto.setExpiryDate(rs.getString("Expiry_Date"));
	
						licenseList.add(dto);
					}
				 }
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at ToolsDAO[license_holder_list]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseList;
	}
	
	public List<LicenseDTO> searchVehicleRoutePermitDtls(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		String formType	=	"";
		List<LicenseDTO> licenseList = new ArrayList<LicenseDTO>();
		try {
			
			int count = 1;
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				formType = dto.getFormType();
				// CHECKING NON EMPTY VARIABLE
				if (!dto.getPermitNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.Permit_No = ? ";
				}
				if (!dto.getLicenseNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.Driver_License_No = ? ";
				}
				if (!dto.getVehicleNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.Registration_No = ? ";
				}
				
				query = SEARCH_ROUTE_PERMIT_VEHICLE;
				pst = conn.prepareStatement(query + where);
				// CLAUSE VALUE
				if (!dto.getPermitNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getPermitNo());
					count = count + 1;
				}
				if (!dto.getLicenseNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getLicenseNo());
					count = count + 1;
				}
				if (!dto.getVehicleNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getVehicleNo());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while (rs.next()) 
				{
					dto = new LicenseDTO();
					dto.setRegisterNo(rs.getString("Registration_No"));
					dto.setPermitDetailsId(rs.getString("Permit_Details_Id"));
					dto.setSeatingCapacity(rs.getString("Seat_Capacity"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
					licenseList.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at ToolsDAO[license_holder_list]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseList;
	}

	
	public List<LicenseDTO> license_suspensionFOR_list(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		String formType	=	"";
		List<LicenseDTO> licenseList = new ArrayList<LicenseDTO>();
		try {
			
			int count = 1;
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				formType = dto.getFormType();
				// CHECKING NON EMPTY VARIABLE
				if (!dto.getPermitNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.Permit_No = ? ";
				}
				if (!dto.getLicenseNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.Driver_License_No = ? ";
				}
				if (!dto.getVehicleNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.Registration_No = ? ";
				}
				
				query = GET_PERMIT_NO_DETAILS;
				pst = conn.prepareStatement(query + where);
				// CLAUSE VALUE
				if (!dto.getPermitNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getPermitNo());
					count = count + 1;
				}
				if (!dto.getLicenseNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getLicenseNo());
					count = count + 1;
				}
				if (!dto.getVehicleNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getVehicleNo());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while (rs.next()) 
				{
					dto = new LicenseDTO();
					
					dto.setPermitNo(rs.getString("Permit_No"));
					dto.setName(rs.getString("Driver_Name"));
					dto.setLicenseNo(rs.getString("Driver_License_No"));
					dto.setRegisterNo(rs.getString("Registration_No"));
					dto.setPermitDetailsId(rs.getString("Permit_Details_Id"));
					dto.setIssuedate(rs.getString("Issue_Date"));
					dto.setExpiryDate(rs.getString("Validity"));

					licenseList.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at ToolsDAO[license_holder_list]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseList;
	}
	
	public EralisCommonDTO getlearnerDtls(String learnerlicenseinfoid,String searchType)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				String customerId = null;
				validity = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-age-for-license-application"));
				pst = conn.prepareStatement(GET_LEARNER_LICENSE_DETAILS);
				pst.setString(1, learnerlicenseinfoid);
				rs = pst.executeQuery();
				rs.first();
				customerId = rs.getString("Customer_Id");
				dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setAddress(rs.getString("Present_Contact_Address"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setCountry(rs.getString("Country_Name"));
				dto.setTitleOfcourtesy(rs.getString("Title_Of_Courtesy_Id"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setCustomerID(rs.getString("Customer_Id"));
				dto.setBloodGroup(rs.getString("blood_group_type"));
				dto.setDOB(rs.getString("dob"));
				dto.setNationality(rs.getString("Nationality"));
				dto.setFathersName(rs.getString("Fathers_Name"));
				dto.setIdentificationMarks(rs.getString("Identification_Mark"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setBloodGroupId(rs.getString("Blood_Group_Id"));
				dto.setGender(rs.getString("Gender"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setRegion(rs.getString("Region_Id"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setLLNo(rs.getString("Learner_License_No"));
				dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
				dto.setLearnerLicenseId(rs.getString("Learner_License_Info_Id"));
				dto.setIsInternational(rs.getString("Is_International"));
				pst = conn.prepareStatement(GET_APPLICANT_AGE);
				pst.setString(1, rs.getString("Personal_Info_Id"));
				rs = pst.executeQuery();
				rs.first();
				dto.setAge(rs.getString("age"));
				dto.setLicenseApplicationAge(validity);
				
				if(searchType.equalsIgnoreCase("ISSUE_NON_COMMERCIAL_LICENSE"))
				{
					pst = conn.prepareStatement(CHECK_IF_NON_COMM_LICENSE_ISSUED);
					pst.setString(1,learnerlicenseinfoid);
					rs = pst.executeQuery();
					rs.first();
					int applicationRow = rs.getInt("applicationRow");
					int drivinggRow  = rs.getInt("drivinggRow");
	
					if (applicationRow > 0) {
						dto.setStatus("ISSUED");
					}
					else if (drivinggRow > 0) {
						dto.setStatus("ISSUED");
					}
					else {
						pst = conn.prepareStatement(GET_ETEST_OVERALL_MARKS);
						pst.setString(1, dto.getLLNo());
						rs = pst.executeQuery();
						rs.first();
						if(rs.getInt("rowCount")>0)
						{
							dto.setOverallObtainedMark(rs.getString("Overall_Marks_Obtained"));
							dto.setStatus("PASSED");
							dto.setDriverName(rs.getString("Drive_Type_Name"));
							dto.setDrivetype(rs.getString("Drive_Type_Id"));
							dto.setTheoryMarks(rs.getString("Theory_Marks_Obtained"));
							dto.setPracticalMarks(rs.getString("Practical_Marks_Obtained"));
							dto.setIssueType(rs.getString("Issue_Type"));
						}
						else
						{
							dto.setStatus("FAILED");
						}
					}
				}
				
				pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
				pst.setString(1, customerId);
				rs = pst.executeQuery();
				rs.first();
				int rowCount1 = rs.getInt("rowCount");
				if(rowCount1>0)
				{
					dto.setStatus("PENDING_APPLICATION");
					dto.setReason(rs.getString("remarks"));
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlearnerDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public List<LicenseDTO> learner_license_list(LicenseDTO dto)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		String formType = "";
		List<LicenseDTO> learnerlicenseList = new ArrayList<LicenseDTO>();
		try {
			int count = 1;
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				formType = dto.getFormType();
				if (!dto.getCID().equalsIgnoreCase("NA")) {
					where = where + " AND p.CID_Number = ? ";
				}
				if (!dto.getLLNo().equalsIgnoreCase("NA")) {
					where = where + " AND dl.Learner_License_No = ? ";
				}
				query = GET_LEARNER_LICENSE_HOLDER_DETAILS;
				pst = conn.prepareStatement(query + where);
				if (!dto.getCID().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getCID());
					count = count + 1;
				}
				if (!dto.getLLNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getLLNo());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setCustomerId(rs.getString("Customer_Id"));
					Date dateOfBirth = rs.getDate("Date_Of_Birth");
					Format formatter = new SimpleDateFormat("yyyy-MM-dd");
					Date today = new Date(System.currentTimeMillis());
					String dob = formatter.format(dateOfBirth);
					String todayStr = formatter.format(today);
					int age = EralisCommonUtil.dateDifferenceAsInt(dob,todayStr, "Y");
					
					if (formType.equalsIgnoreCase("ISSUE_NON_COMMERCIAL_LICENSE"))
					{
						if (age >= 0)
						{
							/*query = GET_ETEST_OVERALL_MARKS;
							pst = conn.prepareStatement(query);
							pst.setString(1, rs.getString("Learner_License_No"));
							ResultSet rs1 = pst.executeQuery();
							rs1.first();
							if ("P".equalsIgnoreCase(rs1.getString("Practical_Test_Status"))) 
							{*/
							dto.setAge(Integer.toString(age));
							dto.setCID(rs.getString("CID_Number"));
							String customerName	=	"";
							if (null!=rs.getString("First_Name"))
							{
								customerName	=	rs.getString("First_Name");
								dto.setName(rs.getString("First_Name")+ " " + rs.getString("Last_Name"));
							} 
							if(null!=rs.getString("First_Name"))
							{
								customerName	=	customerName+rs.getString("First_Name");
							}
							if(null!=rs.getString("Last_Name"))
							{
								rs.getString("Last_Name");
							}
							dto.setLLNo(rs.getString("Learner_License_No"));
							dto.setRegion(rs.getString("region_name"));
							dto.setDzongkhag(rs.getString("dzongkhag_name"));
							dto.setLearnerlicenseinfoid(rs.getString("Learner_License_Info_Id"));
							dto.setIssuedate(rs.getString("Issue_Date"));
							dto.setExpiryDate(rs.getString("Expiry_Date"));
							learnerlicenseList.add(dto);
						}
					}
					else 
					{ 
						dto.setAge(Integer.toString(age));
						dto.setCID(rs.getString("CID_Number"));
						if (null == rs.getString("Middle_Name") || "".equals(rs.getString("Middle_Name"))) {
							dto.setName(rs.getString("First_Name") + " "+ rs.getString("Last_Name"));
						} else {
							dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
						}
						dto.setLLNo(rs.getString("Learner_License_No"));
						dto.setRegion(rs.getString("region_name"));
						dto.setDzongkhag(rs.getString("dzongkhag_name"));
						dto.setLearnerlicenseinfoid(rs.getString("Learner_License_Info_Id"));
						dto.setIssuedate(rs.getString("Issue_Date"));
						dto.setExpiryDate(rs.getString("Expiry_Date"));
						learnerlicenseList.add(dto);
					 }
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at CommonDAO[license_holder_list]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return learnerlicenseList;
	}

	public EralisCommonDTO getSuspensionDtls(String drivinglicenseId , String searchType)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_LICENSE_FOREIGN_DTLS);
				pst.setString(1, drivinglicenseId);
				rs = pst.executeQuery();
				rs.first();
				int rowCount1 = rs.getInt("rowCount"); 
				if(rowCount1 >0)
				{
					dto.setDrivinglicenseId(rs.getString("License_Number"));
					dto.setVehicleRegistrationType(rs.getString("Is_Foreign"));
					dto.setVehicleId(rs.getString("Vehicle_Number"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setVehicleNumber(rs.getString("Vehicle_Number"));
					dto.setLicenseNo(rs.getString("License_Number"));
					dto.setName(rs.getString("Owner_Name"));
					dto.setOffenceDate(rs.getString("Offence_Date"));
					dto.setAmount(rs.getString("Amount"));
					dto.setTIN(rs.getString("TIN_No"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));	
					dto.setType("FOREIGN");
				}
				else{


						
						String customerId = null;
						applicationAge = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-age-for-commercial-license-application"));
						pst = conn.prepareStatement(GET_SUSPENSION_DETAILS);
						pst.setString(1, drivinglicenseId);
						dto.setLicenseStatus("SUCCESS");
						rs = pst.executeQuery();
						rs.first();
						
						String clearanceData = "CLEAR";
						
						if(/*!searchType.equalsIgnoreCase("LICENSE_HISTORY") && */!searchType.equalsIgnoreCase("LICENSE_OFFENCE")
							&& !searchType.equalsIgnoreCase("LICENSE_CANCELLATION") && !searchType.equalsIgnoreCase("WITHDRAW_LICENSE_CANCELLATION")
							 && !searchType.equalsIgnoreCase("LICENSE_SUSPENSION") && !searchType.equalsIgnoreCase("DRIVETYPE_SUSPENSION"))
						
							
						{
							if(searchType.equalsIgnoreCase("LICENSE_RENEWAL"))
							{
								clearanceData = checkClearance(drivinglicenseId,"DL");
							}
							else
							{	
								clearanceData = checkClearance(rs.getString("Personal_Info_Id"),"P");
							}
							
							if(!clearanceData.equalsIgnoreCase("CLEAR"))
							{
								String[] dataArray = clearanceData.split("/");
								dto.setStatus(dataArray[0]);
								if(dataArray[0].equalsIgnoreCase("VEHICLE_OUTSTANDING"))
								{
									dto.setVehicleNo(dataArray[1]);
									dto.setVehicleType(dataArray[2]);
								}
								else if(dataArray[0].equalsIgnoreCase("DRIVING_LICENSE_OUTSTANDING"))
								{
									dto.setLicenseNo(dataArray[1]);
								}
								if(!searchType.equalsIgnoreCase("LICENSE_HISTORY"))
									return dto;
							}
							
							
						}
						
						
						customerId = rs.getString("Customer_Id");
						Date dateOfBirth = null;
						if(null!= rs.getDate("Date_Of_Birth"))
						{
							dateOfBirth = rs.getDate("Date_Of_Birth");
						}
						Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date today = new Date(System.currentTimeMillis());
						String todayStr = formatter.format(today);
						String dob = formatter.format(dateOfBirth);
						int age = EralisCommonUtil.dateDifferenceAsInt(dob,todayStr, "Y");
						dto.setType("BHUTANESE");
						dto.setAge(Integer.toString(age));
						
						dto.setMinistry(rs.getString("Ministry_Id"));
						dto.setDepartment(rs.getString("Department_Id"));
						
						dto.setAmount(rs.getString("Amount_Paid"));
						dto.setReceiptDate(rs.getString("Receipt_Date"));
						dto.setReceiptNo(rs.getString("Receipt_No"));
						
						dto.setFirstname(rs.getString("First_Name"));
						dto.setMiddlename(rs.getString("Middle_Name"));
						dto.setLastName(rs.getString("Last_Name"));
						dto.setFirstname(rs.getString("First_Name"));
						dto.setName(rs.getString("First_Name") + " "+ rs.getString("Middle_Name") + " "+ rs.getString("Last_Name"));
						dto.setDzongkhag(rs.getString("dzongkhag_name"));
						dto.setGewog(rs.getString("Gewog_Name"));
						dto.setAddress(rs.getString("Present_Contact_Address"));
						dto.setCID(rs.getString("CID_Number"));
						dto.setPhone(rs.getString("Present_Phone_No"));
						dto.setCountry(rs.getString("Country_Name"));
						dto.setTitleOfcourtesy(rs.getString("Title_Of_Courtesy_Id"));
						dto.setOccupation(rs.getString("Occupation_Name"));
						dto.setCustomerID(rs.getString("Customer_Id"));
						dto.setBloodGroup(rs.getString("blood_group_type"));
						dto.setDOB(rs.getString("dob"));
						dto.setNationality(rs.getString("Nationality"));
						dto.setFathersName(rs.getString("Fathers_Name"));
						dto.setIdentificationMarks(rs.getString("Identification_Mark"));
						dto.setRemarks(rs.getString("Remarks"));
						dto.setBloodGroupId(rs.getString("Blood_Group_Id"));
						dto.setGender(rs.getString("Gender"));
						dto.setVillage(rs.getString("Permanant_Village_Id"));
						dto.setRegionId(rs.getString("Region_Id"));
						dto.setRegion(rs.getString("regionName"));
						//dto.setStatus(rs.getString("Status"));
						dto.setLicenseNo(rs.getString("Driving_License_No"));
						dto.setLLNo(rs.getString("Learner_License_No"));
						dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
						dto.setDateOfissue(rs.getString("IssueDate"));
						dto.setExpiryDate(rs.getString("ExpiryDate"));
						dto.setLicenseNo(rs.getString("Driving_License_No"));
						dto.setLicensetype(rs.getString("Driving_License_Type_Id"));
						dto.setIsInternational(rs.getString("Is_International"));
						dto.setLearnerLicenseId(rs.getString("Learner_License_Info_Id"));
						dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
						int rowCount =	0;
						pst = conn.prepareStatement(GET_APPLICANT_AGE);
						pst.setString(1, rs.getString("Personal_Info_Id"));
						rs = pst.executeQuery();
						rs.first();
						//dto.setAge(rs.getString("age"));
						dto.setCommercialApplicationAge(applicationAge);
						if(searchType.equalsIgnoreCase("LICENSE_COMMERCIAL"))
						{
							pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_HAS_OFFENCE);
							pst.setString(1, drivinglicenseId);
							rs = pst.executeQuery();
							rs.first();
							int rowCount3 = rs.getInt("rowCount");
							if (rowCount3 > 0) {
								dto.setLicenseStatus("HAS_OFFENCE");
							}
							pst = conn.prepareStatement(CHECK_IF_COMM_LICENSE_HAS_BEEN_ISSUED);
							pst.setString(1, dto.getCID());
							rs = pst.executeQuery();
							rs.first();
							int licenseCount = rs.getInt("licenseCount");
							int applicationCount = rs.getInt("applicationCount");
							if (licenseCount > 0) {
								dto.setLicenseStatus("ISSUED");
							}
							else if(applicationCount > 0) {
								dto.setLicenseStatus("ISSUED");
							}
						}
						else if(searchType.equalsIgnoreCase("LICENSE_RENEWAL")|| (searchType.equalsIgnoreCase("LICENSE_ENDORSEMENT")))
						{
							pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_HAS_OFFENCE);
							pst.setString(1, drivinglicenseId);
							rs = pst.executeQuery();
							rs.first();
							int rowCount3 = rs.getInt("rowCount");
							if (rowCount3 > 0) {
								dto.setLicenseStatus("HAS_OFFENCE");
							}
							pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_RENEWAL);
							pst.setString(1, drivinglicenseId);
							pst.setString(2, drivinglicenseId);
							pst.setString(3, drivinglicenseId);
							rs = pst.executeQuery();
							rs.first();
							String expiryDate	=	rs.getString("expiryDate");
							dto.setExpiryDate(expiryDate);
							
							int birthYear = Integer.parseInt(dob.substring(0, 4));
							int maxAgeYear = 0;
							int totalDuration = 0;
							int ageLimit = 0;
							String limitedDate = null;
							String dobMonth = dob.substring(5, 7);
							String dobDay = dob.substring(8, 10);
							
							if("N".equalsIgnoreCase(dto.getLicensetype()))
							{
								ageLimit	=	70;
							}	
							else if("C".equalsIgnoreCase(dto.getLicensetype()))
							{
								ageLimit	=	65;
							}
							maxAgeYear = birthYear+ageLimit;
							limitedDate	=	Integer.toString(maxAgeYear)+"-"+dobMonth+"-"+dobDay;
							totalDuration = EralisCommonUtil.dateDifferenceAsInt(expiryDate,limitedDate, "D");
							formatter = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date dateExpiry = ((DateFormat) formatter).parse(expiryDate);
							java.util.Date dateLimit = ((DateFormat) formatter).parse(limitedDate);
							
							String dateDifference	=	getDateDifferenceInDDMMYYYY(dateLimit,dateExpiry);
							
							dto.setTotalDuration(Integer.toString(totalDuration));
							dto.setDateDifference(dateDifference);
						}
						else if(searchType.equalsIgnoreCase("LICENSE_HISTORY") || (searchType.equalsIgnoreCase("LICENSE_DUPLICATE")))
						{
							pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_HAS_OFFENCE);
							pst.setString(1, drivinglicenseId);
							rs = pst.executeQuery();
							rs.first();
							int rowCount3 = rs.getInt("rowCount");
							if (rowCount3 > 0) {
								dto.setLicenseStatus("HAS_OFFENCE");
							} 
						}
						else
						{
							pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_HAS_BEEN_ISSUED);
							pst.setString(1, dto.getCID());
							rs = pst.executeQuery();
							rs.first();
							rowCount = rs.getInt("rowCount");
							if (rowCount > 0) {
								dto.setLicenseStatus("ISSUED");
							}
						}
						pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_IS_SUSPENDED);
						pst.setString(1, drivinglicenseId);
						rs = pst.executeQuery();
						rs.first();
						int rowCount2 = rs.getInt("rowCount");
						if (rowCount2 > 0) {
							dto.setLicenseStatus("SUSPENDED");
						}
						pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_IS_CANCELLED);
						pst.setString(1, drivinglicenseId);
						rs = pst.executeQuery();
						rs.first();
						rowCount1 = rs.getInt("rowCount");

						if (rowCount1 > 0) {
							dto.setLicenseStatus("CANCELLED");
							dto.setCancellationDate(rs.getString("Cancellation_Date"));
							dto.setReason(rs.getString("Cancellation_Reason"));
							
						}
						else
						{
							pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
							rowCount1 = rs.getInt("rowCount");
							if(rowCount1>0)
							{
								dto.setLicenseStatus("PENDING_APPLICATION");
								dto.setReason(rs.getString("remarks"));
							}
							
						}
					
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public EralisCommonDTO getaccidentPersonalDetails(String licenseNo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();

		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_PERSONAL_DTLS_FOR_ACCIDENT);
				pst.setString(1, licenseNo);
				rs = pst.executeQuery();
				rs.first();
				dto.setCustomerID(rs.getString("Customer_Id"));
				dto.setName(rs.getString("NAME"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setTitleOfcourtesy(rs.getString("Title_Of_Courtesy_Id"));
				dto.setOccupation(rs.getString("Occupation_Id"));
				dto.setNationality(rs.getString("Nationality_Id"));
				dto.setDOB(rs.getString("dob"));
				dto.setGender(rs.getString("Gender"));
				dto.setIsInternational(rs.getString("Is_International"));
				dto.setDzongkhag(rs.getString("Permanent_Dzongkhag_Id"));
				dto.setGewog(rs.getString("Permanent_Gewog_Id"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setCountry(rs.getString("Permanent_Country_Id"));
				dto.setAddress(rs.getString("Permanent_Address"));
				dto.setPresentDzongkhag(rs.getString("Present_Dzongkhag_Id"));
				dto.setContactAddress(rs.getString("Present_Contact_Address"));
				dto.setPhone(rs.getString("Present_Phone_No"));
				pst = conn.prepareStatement(GET_APPLICANT_AGE_ACCIDENT);
				pst.setString(1, licenseNo);
				rs = pst.executeQuery();
				rs.first();
				dto.setAge(rs.getString("age"));
				
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at CommonDAO[getaccidentPersonalDetails]:: " + e);
		} 
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	
	
	public VehicleDTO getaccidentDetails(String vehicleNo, String vehicleType)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		VehicleDTO dto = new VehicleDTO();
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_ACCIDENT_DETAILS);
				pst.setString(1, vehicleNo);
				pst.setString(2, vehicleType);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto.setVehicleId(rs.getString("Vehicle_Reg_Dtls_Id"));
					dto.setRegistrationType(rs.getString("Registration_Type"));
					dto.setVehicleRegistrationId(rs.getString("Vehicle_Registration_Id"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Id"));
					dto.setVanityNumber(rs.getString("Vanity_Number"));
					dto.setVehicleCompany(rs.getString("Vehicle_Company_Id"));
					dto.setRegistrationDate(rs.getString("registrationDate"));
					dto.setVehicleModel(rs.getString("Vehicle_Model_Id"));
					dto.setEngineType(rs.getString("Engine_Type_Id"));
					dto.setEngineNumber(rs.getString("Engine_Number"));
					dto.setChasisNumber(rs.getString("Chassis_Number"));
					dto.setStatus(rs.getString("Status"));
					dto.setColour(rs.getString("Colour"));
					dto.setPrice(rs.getString("Price"));
					dto.setLoadCapacity(rs.getString("Load_Capacity"));
					dto.setDealersName(rs.getString("Dealer_Id"));
					dto.setSeatCapacity(rs.getString("Seat_Capacity"));
					dto.setVehicleHorsePower(rs.getString("Vehicle_Horse_Power"));
					dto.setVehicleKiloWatt(rs.getString("Vehicle_Kilowatt"));
					dto.setUnladenWeight(rs.getString("Unladen_Weight"));
					dto.setManufactureYear(rs.getString("Manufacture_Year"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setManufactureCountry(rs.getString("Manufacture_Country_Id"));
					dto.setPurchaseType(rs.getString("Purchase_Type"));
					dto.setRegion(rs.getString("Region_Id"));
					dto.setBusType(rs.getString("Bus_Type_Id"));
					if(null != rs.getString("Remarks"))
						dto.setRemarks(rs.getString("Remarks"));
					else
						dto.setRemarks("");
					
					System.out.println(dto.getRemarks());
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public EralisCommonDTO getpermitDetails(String permitDetailsId , String searchType)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
			//	applicationAge = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-age-for-commercial-license-application"));

				//GET_PERMIT_DETAILS_FOR_RENEWAL
				pst = conn.prepareStatement(GET_PERMIT_DETAILS_FOR_RENEWAL);
				pst.setString(1, permitDetailsId);
				dto.setLicenseStatus("SUCCESS");
				rs = pst.executeQuery();
				rs.first();
				
				
				dto.setPermitDetailsId(rs.getString("Permit_Details_Id"));
				dto.setLicenseNo(rs.getString("Driver_License_No"));
				dto.setRegistrationNo(rs.getString("Registration_No"));
				dto.setPermitIDNo(rs.getString("Permit_No"));
				
				dto.setName(rs.getString("Driver_Name"));
				dto.setAddress(rs.getString("Driver_Address"));
				dto.setPhone(rs.getString("Driver_Phone_No"));
				dto.setRouteBetween(rs.getString("Route_Between"));
				dto.setTo(rs.getString("Route_To"));
				dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
				dto.setCarryingCapacity(rs.getString("Carrying_Capacity"));
				dto.setRegionId(rs.getString("Processed_Region_Id"));
				dto.setBaseoffice(rs.getString("Processed_Base_Id"));
				dto.setJourneyPurpose(rs.getString("Journey_Purpose"));
				
				dto.setEngineCC(rs.getString("Engine_CC"));
				dto.setSeatCapacity(rs.getString("Seat_Capacity"));
				dto.setDateOfissue(rs.getString("Issue_Date"));
				dto.setValidUpto(rs.getString("Validity"));
				dto.setRemarks(rs.getString("Remarks"));
				
				rs = pst.executeQuery();
				rs.first();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getPersonalInfoDtls]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	 public static String getDateDifferenceInDDMMYYYY(java.util.Date date1, java.util.Date date2) {
	    Calendar fromDate=Calendar.getInstance();
	    Calendar toDate=Calendar.getInstance();
	    fromDate.setTime(date2);
	    toDate.setTime(date1);
	    int increment = 0;
	    int year,month,day;
	    if (fromDate.get(Calendar.DAY_OF_MONTH) > toDate.get(Calendar.DAY_OF_MONTH)) {
	        increment =fromDate.getActualMaximum(Calendar.DAY_OF_MONTH);
	    }
	// DAY CALCULATION
	    if (increment != 0) {
	        day = (toDate.get(Calendar.DAY_OF_MONTH) + increment) - fromDate.get(Calendar.DAY_OF_MONTH);
	        increment = 1;
	    } else {
	        day = toDate.get(Calendar.DAY_OF_MONTH) - fromDate.get(Calendar.DAY_OF_MONTH);
	    }
	// MONTH CALCULATION
	    if ((fromDate.get(Calendar.MONTH) + increment) > toDate.get(Calendar.MONTH)) {
	        month = (toDate.get(Calendar.MONTH) + 12) - (fromDate.get(Calendar.MONTH) + increment);
	        increment = 1;
	    } else {
	        month = (toDate.get(Calendar.MONTH)) - (fromDate.get(Calendar.MONTH) + increment);
	        increment = 0;
	    }
	// YEAR CALCULATION
	    year = toDate.get(Calendar.YEAR) - (fromDate.get(Calendar.YEAR) + increment);
	    if(year==0 && month>0)
	    {
	    	return   month+"\tMonths\t\t"+day+"\tDays";
	    }
	    if(year==0 && month==0)
	    {
	    	return   month+"\tMonths\t\t"+day+"\tDays";
	    }
	    else
	    {
	    	return   year+"\tYears\t\t"+month+"\tMonths\t\t"+day+"\tDays";
	    }
	}
	

	public String drivetype_list(String learnerLicenseId)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String driveTypeList = "";
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_DRIVE_TYPE_FOR_LEARNER);
				pst.setString(1, learnerLicenseId);
				rs = pst.executeQuery();
				rs.first();
				driveTypeList = rs.getString("driveTypeName");
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlearnerlicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return driveTypeList;
	}
	private boolean getListOfDriveForLearnerLicense(String learnerLicenseId,String driveTypeId, Connection conn) throws ERALISException,ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean isChecked = false;
		try {
			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVE_TYPE_FOR_LEARNER);
				pst.setString(1, learnerLicenseId);
				rs = pst.executeQuery();
				while (rs.next()) {
					if (driveTypeId.equals(rs.getString("Drive_Type_Id")))
						isChecked = true;
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		return isChecked;
	}
	public String drivetype_licenselist(String customerId)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String driveTypeList = "";
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_DRIVE_TYPE_FOR_DRIVING_LICENSE);
				pst.setString(1, customerId);
				rs = pst.executeQuery();
				while(rs.next())
				{
					if(!driveTypeList.equals(""))
						driveTypeList = driveTypeList+", "+rs.getString("driveTypes");
					else 
						driveTypeList = rs.getString("driveTypes");
				}
			}
		} 
		catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivetype_licenselist]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return driveTypeList;
	}
	/*public LicenseDTO getDriveTypeTestMarks(String driveTypeId, String licenseNO,String testDate,String customerID)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		LicenseDTO dto = new LicenseDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				pst = conn.prepareStatement(CHECK_DRIVE_TYPE_EXISTED_IN_LICENSE);
				pst.setString(1, driveTypeId);
				pst.setString(2, customerID);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
				{
					dto.setStatus("0");
					dto.setFlag("0");
					dto.setReason("Drive Type already issued");
				}
				else
				{
				
					String endorsementVehicleType	=	"";
					int	criteriaAge	=	0; 
					pst = conn.prepareStatement(GET_DRIVE_TYPE_DESCRIPTION);
					pst.setString(1, driveTypeId);
					rs = pst.executeQuery();
					rs.first();
					
					String driveTypeName	=	rs.getString("Drive_Type_Name");
					String description	=	rs.getString("Description");
					String licenseType	=	rs.getString("Drive_Type_Category");
					if(description.equalsIgnoreCase("LIGHT_VEHICLE"))
					{
						endorsementVehicleType	=	"LIGHT_VEHICLE";
						criteriaAge	=	18;
					}
					else if(description.equalsIgnoreCase("TWO_WHEELER"))
					{
						endorsementVehicleType	=	"TWO_WHEELER";
						criteriaAge	=	18;
					}
					else if(description.equalsIgnoreCase("MEDIUM_VEHICLE"))
					{
						endorsementVehicleType	=	"MEDIUM_VEHICLE";
						criteriaAge	=	25;
					}
					else if(description.equalsIgnoreCase("HEAVY_VEHICLE"))
					{
						endorsementVehicleType	=	"HEAVY_VEHICLE";
						criteriaAge	=	25;
					} 
				 
					int endorsedDate_duration = 6;
					Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date today = new Date(System.currentTimeMillis());
					Date dateOfBirth = null;
					
					pst = conn.prepareStatement(GET_DOB_FROM_LICENSE_NO);
					pst.setString(1, licenseNO);
					rs = pst.executeQuery();
					rs.first();
					String customerId	=	rs.getString("Customer_Id");
					if(null!= rs.getDate("Date_Of_Birth"))
					{
						dateOfBirth = rs.getDate("Date_Of_Birth");
					}
					String todayStr = formatter.format(today);
					String dob = formatter.format(dateOfBirth);
					pst=conn.prepareStatement(GET_LATEST_ENDORSED_DATE);
					pst.setString(1, customerId);
					pst.setString(2, customerId);
					pst.setString(3, customerId);
					pst.setString(4, customerId);
					pst.setString(5, customerId);
					rs	=	pst.executeQuery();
					rs.first();
					if(rs.getInt("rowCount") > 0)
					{
						if(rs.getString("driveType")==null)
						{
							Date endorsed_date = rs.getDate("latest_issue_date");
							String endorsedDate = formatter.format(endorsed_date);
							if(null==testDate || "null".equalsIgnoreCase(testDate))
							{
								endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,todayStr, "M");
							}
							else
							{
 								endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,testDate, "M");
							}
							
							
						}
						else if(!rs.getString("driveType").equalsIgnoreCase("TOURIST_LIGHT_VEHICLE") && 
								!rs.getString("driveType").equalsIgnoreCase("TOURIST_HEAVY_BUS") &&
								!rs.getString("driveType").equalsIgnoreCase("TOURIST_MEDIUM_BUS"))
						{
							Date endorsed_date = rs.getDate("latest_issue_date");
							String endorsedDate = formatter.format(endorsed_date);
							endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,todayStr, "M");
						}
						else
						{
							endorsedDate_duration=6;
							
						}
					}
					if(!("O").equalsIgnoreCase(licenseType))
					{
						pst = conn.prepareStatement(GET_DRIVING_LICENSE_ID);
						pst.setString(1, licenseNO);
						rs = pst.executeQuery();
						rs.first();
						String Driving_License_Id	=	rs.getString("Driving_License_Id");
						
						dto	=	validateCommercialCriteria(driveTypeId,Driving_License_Id,customerID,testDate);
					}
					else
					{
						int age = EralisCommonUtil.dateDifferenceAsInt(dob,todayStr, "Y");
						if(age>=criteriaAge && endorsedDate_duration>=6)
						{
							dto.setFlag("1");
							dto.setStatus("1");
						}
						else
						{
							dto.setFlag("0");
						}
						if(age<criteriaAge)
							dto.setReason("Applicant is Under Age");
						if(endorsedDate_duration<6)
							dto.setReason("Applicant must maintain Six months gap");
						
					}
					
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getdrivetype_licenselist]:: " + e);
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}*/
	 
	public LicenseDTO getDriveTypeTestMarks(String driveTypeId, String licenseNO,String testDate,String customerID, String issueType)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		LicenseDTO dto = new LicenseDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
 
				pst = conn.prepareStatement(CHECK_DRIVE_TYPE_EXISTED_IN_LICENSE);
				pst.setString(1, driveTypeId);
				pst.setString(2, customerID);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
				{
					dto.setStatus("0");
					dto.setFlag("0");
					dto.setReason("Drive Type already issued");
				}
				else
				{
				
					String endorsementVehicleType	=	"";
					int	criteriaAge	=	0; 
					pst = conn.prepareStatement(GET_DRIVE_TYPE_DESCRIPTION);
					pst.setString(1, driveTypeId);
					rs = pst.executeQuery();
					rs.first();
					
					String driveTypeName	=	rs.getString("Drive_Type_Name");
					String description	=	rs.getString("Description");
					String licenseType	=	rs.getString("Drive_Type_Category");
					if(description.equalsIgnoreCase("LIGHT_VEHICLE"))
					{
						endorsementVehicleType	=	"LIGHT_VEHICLE";
						criteriaAge	=	18;
					}
					else if(description.equalsIgnoreCase("TWO_WHEELER"))
					{
						endorsementVehicleType	=	"TWO_WHEELER";
						criteriaAge	=	18;
					}
					else if(description.equalsIgnoreCase("MEDIUM_VEHICLE"))
					{
						endorsementVehicleType	=	"MEDIUM_VEHICLE";
						if(issueType.equalsIgnoreCase("VTI"))
							criteriaAge	=	21;
						else
							criteriaAge	=	25;
					}
					else if(description.equalsIgnoreCase("HEAVY_VEHICLE"))
					{
						endorsementVehicleType	=	"HEAVY_VEHICLE";
						if(issueType.equalsIgnoreCase("VTI"))
							criteriaAge	=	21;
						else
							criteriaAge	=	25;
					} 
				/*pst = conn.prepareStatement(GET_ENDORSE_TEST_MARKS);
				pst.setString(1, licenseNO);
				pst.setString(2, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				dto.setTestmarks(rs.getString("Overall_Marks_Obtained"));
				dto.setStatus(rs.getString("Practical_Test_Status"));*/
					int endorsedDate_duration = 6;
					Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date today = new Date(System.currentTimeMillis());
					Date dateOfBirth = null;
					
					//pst = conn.prepareStatement(GET_DOB_FROM_LICENSE_NO);
					pst = conn.prepareStatement(GET_PERSONAL_DETAILS1);
					pst.setString(1, customerID);
					rs = pst.executeQuery();
					rs.first();
					String customerId	= null;
					if(rs.getInt("rowCount")>0)
					{
						customerId = customerID;
						dateOfBirth = rs.getDate("Date_Of_Birth");
					}
					String todayStr = formatter.format(today);
					String dob = formatter.format(dateOfBirth);
					pst=conn.prepareStatement(GET_LATEST_ENDORSED_DATE);
					pst.setString(1, customerId);
					pst.setString(2, customerId);
					pst.setString(3, customerId);
					pst.setString(4, customerId);
					pst.setString(5, customerId);
					rs	=	pst.executeQuery();
					rs.first();
					if(rs.getInt("rowCount") > 0)
					{
						if(rs.getString("driveType")==null)
						{
							Date endorsed_date = rs.getDate("latest_issue_date");
							String endorsedDate = formatter.format(endorsed_date);
							if(null==testDate || "null".equalsIgnoreCase(testDate))
							{
								endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,todayStr, "M");
							}
							else
							{
 								endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,testDate, "M");
							}
						}
						else if(!rs.getString("driveType").equalsIgnoreCase("TOURIST_LIGHT_VEHICLE") && 
								!rs.getString("driveType").equalsIgnoreCase("TOURIST_HEAVY_BUS") &&
								!rs.getString("driveType").equalsIgnoreCase("TOURIST_MEDIUM_BUS"))
						{
							Date endorsed_date = rs.getDate("latest_issue_date");
							String endorsedDate = formatter.format(endorsed_date);
							endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,todayStr, "M");
						}
						else
						{
							endorsedDate_duration=6;
						}
					}
					if(!("O").equalsIgnoreCase(licenseType))
					{
						pst = conn.prepareStatement(GET_DRIVING_LICENSE_ID);
						pst.setString(1, licenseNO);
						rs = pst.executeQuery();
						rs.first();
						String Driving_License_Id	=	rs.getString("Driving_License_Id");
						
						dto	=	validateCommercialCriteria(driveTypeId,Driving_License_Id,customerID,testDate);
					}
					else
					{
						int age = EralisCommonUtil.dateDifferenceAsInt(dob,todayStr, "Y");
						if(issueType.equalsIgnoreCase("Driving_Institue"))
						{
							endorsedDate_duration = endorsedDate_duration + 3;
						}
						if(age>=criteriaAge && endorsedDate_duration>=6)
						{
							dto.setFlag("1");
							dto.setStatus("1");
						}
						else
						{
							dto.setFlag("0");
						}
						
						if(age<criteriaAge)
						{
							dto.setReason("Applicant is Under Age, minimum age shoud be "+criteriaAge+" years old");
						}
						if(endorsedDate_duration<6)
						{
							if(issueType.equalsIgnoreCase("Driving_Institue"))
							{
								dto.setReason("Applicant must maintain Three months gap");
							}
							else
							{
								dto.setReason("Applicant must maintain Six months gap");
							}
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getDriveTypeTestMarks]:: " + e);
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	 
	/*public LicenseDTO testValidation(String driveTypeId, String drivinglicenseId,String customerID,String testDate)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		LicenseDTO dto = new LicenseDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				int ordinary	=	0;
				String reason="";
				String criteriaVehicleType="";
				String commercialType="";
				
				pst = conn.prepareStatement(GET_DRIVE_TYPE_DESCRIPTION);
				pst.setString(1, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				
				String driveTypeName	=	rs.getString("Drive_Type_Name");
				String description	=	rs.getString("Description");
				String driveTypeCategory	=	rs.getString("Drive_Type_Category");
				
				pst = conn.prepareStatement(CHECK_DRIVE_TYPE_EXIST);
				pst.setString(1, customerID);
				pst.setString(2, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
				{
					dto.setReason("Sorry! Applicant has already issued "+driveTypeName+" drive type");
					dto.setStatus("0");
					
				}
				else
				{
					pst = conn.prepareStatement(GET_PARAM_FOR_COMMERCIAL_CRITERIA);
					pst.setString(1, description);
					pst.setString(2, drivinglicenseId);
					rs = pst.executeQuery();
					rs.first();
					String Is_License_Issued	=	rs.getString("Is_License_Issued");
					String practicalTestStatus	=	rs.getString("Practical_Test_Status");
					testDate	=	rs.getString("Test_Date");
					if(practicalTestStatus!=null)
					{	
						if("Y".equalsIgnoreCase(Is_License_Issued))
						{
							dto.setReason("Sorry! Applicant has already issued "+driveTypeName+" drive type");
							dto.setStatus("0");
						}
						else if("N".equalsIgnoreCase(practicalTestStatus))
						{
							dto.setReason("Sorry! Applicant either failed the driving test or test marks not yet entered");
							dto.setStatus("0");
							dto.setResult("FAILED");
						}
						else if("T".equalsIgnoreCase(driveTypeCategory) || "C".equalsIgnoreCase(driveTypeCategory))
						{
							dto	=	validateCommercialCriteria(driveTypeId,drivinglicenseId,customerID,testDate);
						}
						
					}
					else
					{
						dto.setReason("Sorry! Applicant did not attend the driving test");
						dto.setStatus("0");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getdrivetype_licenselist]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}*/
	
	
	public LicenseDTO testValidation(String driveTypeId, String drivinglicenseId,String customerID,String testDate)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		LicenseDTO dto = new LicenseDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				
				pst = conn.prepareStatement(GET_DRIVE_TYPE_DESCRIPTION);
				pst.setString(1, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				
				String driveTypeName	=	rs.getString("Drive_Type_Name");
				String description	=	rs.getString("Description");
				String driveTypeCategory	=	rs.getString("Drive_Type_Category");
				
				pst = conn.prepareStatement(CHECK_DRIVE_TYPE_EXIST);
				pst.setString(1, customerID);
				pst.setString(2, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>0)
				{
					dto.setReason("Sorry! Applicant has already issued "+driveTypeName+" drive type");
					dto.setStatus("0");
					
				}
				else
				{
					pst = conn.prepareStatement(GET_PARAM_FOR_COMMERCIAL_CRITERIA);
					pst.setString(1, description);
					pst.setString(2, drivinglicenseId);
					rs = pst.executeQuery();
					rs.first();
					String Is_License_Issued	=	rs.getString("Is_License_Issued");
					String practicalTestStatus	=	rs.getString("Practical_Test_Status");
					testDate	=	rs.getString("Test_Date");
					if(practicalTestStatus!=null)
					{	
						if("Y".equalsIgnoreCase(Is_License_Issued))
						{
							dto.setReason("Sorry! Applicant has already issued "+driveTypeName+" drive type");
							dto.setStatus("0");
						}
						else if("N".equalsIgnoreCase(practicalTestStatus) || "F".equalsIgnoreCase(practicalTestStatus))
						{
							dto.setReason("Sorry! Applicant either failed the driving test or test marks not yet entered");
							dto.setStatus("0");
							dto.setResult("FAILED");
						}
						else if("T".equalsIgnoreCase(driveTypeCategory) || "C".equalsIgnoreCase(driveTypeCategory))
						{
							dto	=	validateCommercialCriteria(driveTypeId,drivinglicenseId,customerID,testDate);
						}
						
					}
					else
					{
						dto.setReason("Sorry! Applicant did not attend the driving test");
						dto.setStatus("0");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[testValidation]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	
	
	
	
	public LicenseDTO validateCommercialCriteria(String driveTypeId,  String drivinglicenseId,String customerID,String testDate)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		LicenseDTO dto = new LicenseDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{
				int ordinary	=	0;
				String reason="";
				String criteriaVehicleType="";
				String commercialType="";
				pst = conn.prepareStatement(GET_DRIVE_TYPE_DESCRIPTION);
				pst.setString(1, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				String description	=	rs.getString("Description");
				String driveTypeName	=	rs.getString("Drive_Type_Name");
				int ageLimit	=	0;
				if(description.equalsIgnoreCase("TAXI"))
				{
					criteriaVehicleType	=	"LIGHT_VEHICLE";
					commercialType	=	"COMMERCIAL";
					ageLimit	=	65;
					
				}
				else if(description.equalsIgnoreCase("TOURIST_LIGHT_VEHICLE"))
				{
					criteriaVehicleType	=	"LIGHT_VEHICLE";
					commercialType	=	"TOURIST";
					ageLimit	=	65;
				}
				else if(description.equalsIgnoreCase("MEDIUM_BUS"))
				{
					criteriaVehicleType	=	"MEDIUM_VEHICLE";
					commercialType	=	"COMMERCIAL";
					ageLimit	=	63;
				}
				else if(description.equalsIgnoreCase("HEAVY_BUS"))
				{
					criteriaVehicleType	=	"HEAVY_VEHICLE";
					commercialType	=	"COMMERCIAL";
					ageLimit	=	63;
				}
				else if(description.equalsIgnoreCase("TOURIST_MEDIUM_BUS"))
				{
					criteriaVehicleType	=	"MEDIUM_BUS";
					commercialType	=	"TOURIST";
					ageLimit	=	65;
				}
				else if(description.equalsIgnoreCase("TOURIST_HEAVY_BUS"))
				{
					criteriaVehicleType	=	"HEAVY_BUS";
					commercialType	=	"TOURIST";
					ageLimit	=	65;
				}
				else
				{
					ordinary=1;
				}
				if(ordinary==0)
				{
					pst = conn.prepareStatement(GET_PARAM_FOR_COMMERCIAL_CRITERIA);
					pst.setString(1, description);
					pst.setString(2, drivinglicenseId);
					rs = pst.executeQuery();
					rs.first();
					if(rs.getInt("rowCount") > 0)
					{	
						/*String practicalTestStatus	=	rs.getString("Practical_Test_Status");
						if("P".equalsIgnoreCase(practicalTestStatus))
						{*/
							int endorsedDate_duration = 6;
							Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date dateOfBirth = rs.getDate("Date_Of_Birth");
							
							pst = conn.prepareStatement(CHECK_FOR_ISSUE_DATE);
							pst.setString(1, customerID);
							pst.setString(2, criteriaVehicleType);
							pst.setString(3, customerID);
							rs1 = pst.executeQuery();
							rs1.first();
							if(rs1.getInt("rowCount") > 0)
							{
								Date Issue_Date = rs1.getDate("issue_date");
								String dob = formatter.format(dateOfBirth);
								Date today = new Date(System.currentTimeMillis());
								String todayStr = formatter.format(today);
								String non_comm_issue_date = formatter.format(Issue_Date);
								pst=conn.prepareStatement(GET_LATEST_ENDORSED_DATE);
								pst.setString(1, rs.getString("Customer_Id"));
								pst.setString(2, rs.getString("Customer_Id"));
								pst.setString(3, rs.getString("Customer_Id"));
								pst.setString(4, rs.getString("Customer_Id"));
								pst.setString(5, rs.getString("Customer_Id"));
								rs	=	pst.executeQuery();
								rs.first();
								if(rs.getInt("rowCount") > 0)
								{
									/*if(!rs.getString("driveType").equalsIgnoreCase("TOURIST_LIGHT_VEHICLE") && 
											!rs.getString("driveType").equalsIgnoreCase("TOURIST_HEAVY_BUS") && 
											!rs.getString("driveType").equalsIgnoreCase("TOURIST_MEDIUM_BUS"))*/
									if(!description.equalsIgnoreCase("TOURIST_LIGHT_VEHICLE") && 
											!description.equalsIgnoreCase("TOURIST_HEAVY_BUS") && 
											!description.equalsIgnoreCase("TOURIST_MEDIUM_BUS"))
									{
										Date endorsed_date = rs.getDate("latest_issue_date");
										String endorsedDate = formatter.format(endorsed_date);
										
										if(testDate!=null)
										{
											endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,testDate, "M");
										}
										else
										{
											endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,todayStr, "M");
										}
										//endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,todayStr, "M");
									}
									else
									{
										endorsedDate_duration =6;
									}
								}
								int age =0;
								int issued_duration =0;
								if(testDate!=null)
								{
									 age = EralisCommonUtil.dateDifferenceAsInt(dob,testDate, "Y");
									 issued_duration = EralisCommonUtil.dateDifferenceAsInt(non_comm_issue_date,testDate, "Y");
								}
								else
								{
									 age = EralisCommonUtil.dateDifferenceAsInt(dob,todayStr, "Y");
									 issued_duration = EralisCommonUtil.dateDifferenceAsInt(non_comm_issue_date,todayStr, "Y");
								}
								
								if((age>=25 && age<=ageLimit && issued_duration>=3 && endorsedDate_duration>=6 && commercialType.equalsIgnoreCase("COMMERCIAL")) ||
									(age>=25 && age<=ageLimit && issued_duration>=3 && commercialType.equalsIgnoreCase("TOURIST")))
								{
									dto.setStatus("1");
								}
								else
								{
									if(age<25)
									{
										reason="Applicant is under age";
									}
									else if(age>ageLimit)
									{
										reason	=	"Applicant is over age";
									}
									else if(issued_duration<3)
									{
										reason	=	"Applicant is inexperienced for drive type";
									}
									else if(endorsedDate_duration<6)
									{
										reason	=	"License holder has not met 6 months gap";
									}
									dto.setReason(reason);
									dto.setFlag("0");
									dto.setStatus("0");
								}
							}
							else
							{
								String replaceString=criteriaVehicleType.replace("_"," ");
								dto.setReason("Applicant do not have <b>"+replaceString+"</b> drive type");
								dto.setStatus("0");
								dto.setFlag("0");
							}
							
						/*}
						else
						{
							dto.setReason("Sorry, Applicant did not attend or failed the drive test");
							dto.setStatus("0");
						}*/
					
					}
					else
					{
						reason	=	"License holder do not have "+criteriaVehicleType.replace("_"," ")+" drive type";
						dto.setReason(reason);
						dto.setStatus("0");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[validateCommercialCriteria]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public LicenseDTO validateNonCommercialCriteria(String driveTypeId, String learnerLicenseId, String issueType)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		LicenseDTO dto = new LicenseDTO();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				dto.setResult("");
				String reason="";
				pst = conn.prepareStatement(GET_DRIVE_TYPE_DESCRIPTION);
				pst.setString(1, driveTypeId);
				rs = pst.executeQuery();
				rs.first();
				String driveTypeName	=	rs.getString("Drive_Type_Name");
				String description	=	rs.getString("Description");
				pst = conn.prepareStatement(VALIDATE_LEARNER_LICENSE_TEST);
				pst.setString(1, description);
				pst.setString(2, learnerLicenseId);
				rs = pst.executeQuery();
				rs.first();
				String Is_License_Issued	=	rs.getString("Is_License_Issued");
				String practicalTestStatus	=	rs.getString("Practical_Test_Status");
				dto.setTestmarks(rs.getString("Overall_Marks_Obtained"));
				if(practicalTestStatus!=null)
				{	
					if("Y".equalsIgnoreCase(Is_License_Issued))
					{
						dto.setReason("Sorry! Applicant has already issued "+driveTypeName+" drive type");
						dto.setStatus("0");
					}
					else if("N".equalsIgnoreCase(practicalTestStatus))
					{
						dto.setReason("Sorry! Applicant either failed the driving test or test marks not yet entered");
						dto.setStatus("0");
						dto.setResult("FAILED");
					}
					else if("P".equalsIgnoreCase(practicalTestStatus))
					{
						pst=conn.prepareStatement(GET_PARAM_FOR_NON_COMMERCIAL_CRITERIA);
						pst.setString(1, learnerLicenseId);
						rs	=	pst.executeQuery();
						rs.first();
						
						if(rs.getInt("rowCount")==0)
						{	
							pst=conn.prepareStatement(GET_PERSONAL_DTLS_FROM_CID);
							pst.setString(1, learnerLicenseId);
							rs	=	pst.executeQuery();
							rs.first();
						}
						if(rs.getInt("rowCount")>0)
						{
							Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date dateOfBirth = rs.getDate("Date_Of_Birth");
							Date Issue_Date = null;
							String dob = formatter.format(dateOfBirth);
							Date today = new Date(System.currentTimeMillis());
							String todayStr = formatter.format(today);
							String non_comm_issue_date = null;
							int issued_duration	=	0;
							int duration	=	0;
							
							if(rs.getString("Issue_Date")!=null)
							{
								Issue_Date = rs.getDate("Issue_Date");
								non_comm_issue_date = formatter.format(Issue_Date);
								issued_duration = EralisCommonUtil.dateDifferenceAsInt(non_comm_issue_date,todayStr, "M");
								if(issueType.equalsIgnoreCase("Driving_Institue"))
								{
									duration=3;
								}
								else
								{
									duration=6;
								}
							}
							int age = EralisCommonUtil.dateDifferenceAsInt(dob,todayStr, "Y");
							
							if(age >= 18 && age <= 70 && issued_duration >= duration )
							{
								dto.setStatus("1");
								dto.setResult("PASSED");
							}
							else
							{
								if(age<18)
								{
									reason="Applicant is under age";
								}
								else if(age>70)
								{
									reason	=	"Applicant is over age";
								}
								else if(issued_duration<duration)
								{
									reason	=	"Applicant should maintain "+duration+" months gap after issuing leaner license";
								}
								
								dto.setReason(reason);
								dto.setStatus("0");
							}
						}
						else
						{
							reason	=	"Sorry applicant is not eligible";
							dto.setReason(reason);
							dto.setStatus("0");
						}
					}
					
				}
				else
				{
					dto.setReason("Sorry! Applicant did not attend the drive test");
					dto.setStatus("0");
				}
				
				
				
				
				
				
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[validateNonCommercialCriteria]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	private boolean getListOfDriveForDrivingLicense(String customerId,String driveTypeId, Connection conn) throws ERALISException,ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean isChecked = false;

		try {
			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVE_TYPE_FOR_DRIVING_LICENSE);
				pst.setString(1, customerId);
				pst.setString(2, customerId);
				rs = pst.executeQuery();

				while (rs.next()) {
					if (driveTypeId.equals(rs.getString("Drive_Type_Id")))
						isChecked = true;
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		return isChecked;
	}
	public List<EralisCommonDTO> getTestDate(String locationId, String jurisTypeId,String issueType)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> testDateList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				if(issueType!=null)
				{
					pst = conn.prepareStatement(GET_TEST_DATE);
					pst.setString(1, locationId);
					pst.setString(2, jurisTypeId);
					pst.setString(3, issueType);
					rs = pst.executeQuery();
					while(rs.next())
					{
						dto = new EralisCommonDTO();
						dto.setInstituteName(rs.getString("Institute_Name"));
						dto.setTestDate(rs.getString("Test_Date1"));
						dto.setTestDateId(rs.getString("testDate"));
						testDateList.add(dto);
					}
				}	
				else
				{
					pst = conn.prepareStatement(GET_TEST_DATE_FOR_ADMIN);
					pst.setString(1, locationId);
					pst.setString(2, jurisTypeId);
					rs = pst.executeQuery();
					while(rs.next())
					{
						dto = new EralisCommonDTO();
						dto.setInstituteName(rs.getString("Institute_Name"));
						dto.setTestDate(rs.getString("Test_Date1"));
						dto.setTestDateId(rs.getString("testDate"));
						testDateList.add(dto);
					}
					
				}
				
			}
		} 
		catch (Exception e)
		{
			throw new ERALISException("###Error at CommonDAO[getTestDate]:: "+ e);
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return testDateList;
	}
	public ApplicationDataVO getMaxApplicants(String testDate, String testLocation,String issueType,String learnerNo,String customerId,String testJurisTypeId,String searchBy) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ApplicationDataVO vo = new ApplicationDataVO();
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null)
			{
				Format formatter = new SimpleDateFormat("yyyy-MM-dd");
				if(issueType.equalsIgnoreCase("Learner_License") || issueType.equalsIgnoreCase("Driving_Institue"))
				{
					pst = conn.prepareStatement(GET_ISSUE_DATE_AND_DOB_FROM_LLNO);
					pst.setString(1, learnerNo);
					rs = pst.executeQuery();
					rs.first();
					if(rs.getInt("rowCount")>0)
					{
						Date dateOfBirth = rs.getDate("Date_Of_Birth");
						Date issue_date = rs.getDate("Issue_Date");

						String dob = formatter.format(dateOfBirth);
						
						String issue_date1 = formatter.format(issue_date);
						int issued_duration = EralisCommonUtil.dateDifferenceAsInt(issue_date1,testDate, "M");
						int age = EralisCommonUtil.dateDifferenceAsInt(dob,testDate, "Y");
						
						int maturity_duration	=	0;
						if(issueType.equalsIgnoreCase("Learner_License"))
						{
							maturity_duration	=	6;
						}
						else if(issueType.equalsIgnoreCase("Driving_Institue"))
						{
							maturity_duration	=	3;
						}
						
						if(age<18)
						{
							vo.setFlag("1");
						}
						else if(age>70)
						{
							vo.setFlag("2");
						}
						else if(issued_duration<maturity_duration)
						{
							vo.setFlag("3");
							vo.setMessage(Integer.toString(maturity_duration));
						}
					}
				}
				else if(issueType.equalsIgnoreCase("DL"))
				{
					int endorsedDate_duration = 0;	
					pst=conn.prepareStatement(GET_LATEST_ENDORSED_DATE);
					pst.setString(1, customerId);
					pst.setString(2, customerId);
					pst.setString(3, customerId);
					pst.setString(4, customerId);
					pst.setString(5, customerId);
					rs	=	pst.executeQuery();
					rs.first();
					if(rs.getInt("rowCount") > 0)
					{
						Date endorsed_date = rs.getDate("latest_issue_date");
						String endorsedDate = formatter.format(endorsed_date);
						endorsedDate_duration = EralisCommonUtil.dateDifferenceAsInt(endorsedDate,testDate, "M");
						if(endorsedDate_duration<6)
						{
							vo.setFlag("3");
							vo.setMessage("6");
							
						}
						
					}
					
				}
				if(searchBy.equalsIgnoreCase("CITIZEN"))
				{
					pst = conn.prepareStatement(GET_APPLICANT_COUNT);
					pst.setString(1, testDate);
					pst.setString(2, testLocation);
					pst.setString(3, testJurisTypeId);
					rs = pst.executeQuery();
					rs.first();
					vo.setApplicants(rs.getString("rowCount"));
					pst = conn.prepareStatement(GET_MAX_APPLICANT);
					pst.setString(1, testDate);
					pst.setString(2, testLocation);
					rs = pst.executeQuery();
					rs.first();
					vo.setMaxApplicants(rs.getString("Max_Applicants"));
				}
				else
				{
					vo.setApplicants("1");
					vo.setMaxApplicants("10");
				}
				
				
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getTestDate]:: "+ e);
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public EralisCommonDTO gettopDtls(String CID) throws ERALISException,ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try
		{
			conn = ConnectionManager.getConnection();

			if (conn != null)
			{
				String customerId;
				pst = conn.prepareStatement(CHECK_FOR_CID);
				pst.setString(1, CID);
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				customerId = rs.getString("a.Customer_Id");
				if (rowCount > 0) 
				{
					pst = conn.prepareStatement(CHECK_FOR_ALREADY_ISSUED);
					pst.setString(1, CID);
					rs = pst.executeQuery();
					rs.first();
					int rowCount3 = rs.getInt("rowCount");
					int taxiSold = rs.getInt("taxiSold");
					
					if(taxiSold>0)
					{
						dto.setStatus("TAXI_SOLD");
					}
					else if (rowCount3 > 0)
					{
						dto.setStatus("ISSUED");
					}
					else 
					{
						pst = conn.prepareStatement(CHECK_FOR_VEHICLE_REGISTERED);
						pst.setString(1, CID);
						rs = pst.executeQuery();
						rs.first();
						int rowCount1 = rs.getInt("rowCount");

						if (rowCount1 > 0) 
						{
							pst = conn.prepareStatement(CHECK_FOR_COMMERCIAL_LICENSE);
							pst.setString(1, CID);
							rs = pst.executeQuery();
							rs.first();
							int rowCount2 = rs.getInt("rowCount");
							if (rowCount2 > 0) 
							{
								pst = conn.prepareStatement(CHECK_IF_APPLICATION_ALREADY_EXISTS_FOR_TOP);
								pst.setString(1, CID);
								rs = pst.executeQuery();
								rs.first();
								int rowCount5 = rs.getInt("rowCount");
								if(rowCount5 == 0)
								{
									pst = conn.prepareStatement(GET_TOP_DTLS);
									pst.setString(1, CID);
									rs = pst.executeQuery();
									rs.first();
									int rowCount4 = rs.getInt("rowCount");
									if (rowCount4 > 0) 
									{
										dto.setName(rs.getString("NAME"));
										dto.setDOB(rs.getString("dob"));
										dto.setAddress(rs.getString("Present_Contact_Address"));
										dto.setPhone(rs.getString("Present_Phone_No"));
										dto.setGender(rs.getString("Gender"));
										dto.setFathersName(rs.getString("Fathers_Name"));
										dto.setBloodGroup(rs.getString("blood_group_type"));
										dto.setDzongkhag(rs.getString("dzongkhag_name"));
										dto.setGewog(rs.getString("gewog_name"));
										dto.setVillage(rs.getString("village_name"));
										dto.setCustomerID(rs.getString("Customer_Id"));
										dto.setLicenseNo(rs.getString("Driving_License_No"));
										dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
										dto.setStatus("SUCCESS");
									} 
									else
									{
										dto.setStatus("NO_RECORD");
									}
								}
								else
								{
									pst = conn.prepareStatement(CHECK_THE_APPLICATION_STATUS);
									pst.setString(1, rs.getString("Application_Number"));
									rs = pst.executeQuery();
									rs.first();
									
									String status = rs.getString("Status_Type_Short_Desc");
									if(status.equalsIgnoreCase("REJECTED"))
									{
										pst = conn.prepareStatement(GET_TOP_DTLS);
										pst.setString(1, CID);
										rs = pst.executeQuery();
										rs.first();
										int rowCount4 = rs.getInt("rowCount");
										if (rowCount4 > 0) 
										{
											dto.setName(rs.getString("NAME"));
											dto.setDOB(rs.getString("dob"));
											dto.setAddress(rs.getString("Present_Contact_Address"));
											dto.setPhone(rs.getString("Present_Phone_No"));
											dto.setGender(rs.getString("Gender"));
											dto.setFathersName(rs.getString("Fathers_Name"));
											dto.setBloodGroup(rs.getString("blood_group_type"));
											dto.setDzongkhag(rs.getString("dzongkhag_name"));
											dto.setGewog(rs.getString("gewog_name"));
											dto.setVillage(rs.getString("village_name"));
											dto.setCustomerID(rs.getString("Customer_Id"));
											dto.setLicenseNo(rs.getString("Driving_License_No"));
											dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
											dto.setStatus("SUCCESS");
										} 
										else
										{
											dto.setStatus("NO_RECORD");
										}
									}
									else
									{
										dto.setStatus("APPLICATION_UNDER_PROCESS");
									}
								}
							}
							else 
							{
								dto.setStatus("NO_COMMERCIAL");
							}
						} 
						else 
						{
							dto.setStatus("NO_TAXI");
						}
					}
				}
				else 
				{
					dto.setStatus("NO_CID");
				}
				
				pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
				pst.setString(1, customerId);
				rs = pst.executeQuery();
				rs.first();
				int rowCount1 = rs.getInt("rowCount");
				if(rowCount1>0)
				{
					dto.setStatus("PENDING_APPLICATION");
					dto.setReason(rs.getString("remarks"));
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at CommonDAO[getTOPDtls]:: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public List<EralisCommonDTO> getvehicleList(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> vehicleList = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_VEHICLE_LISTS);
				pst.setString(1, vo.getCidNumber());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new EralisCommonDTO();
					dto.setOwnerName(rs.getString("NAME"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setManufactureYear(rs.getString("Manufacture_year"));
					dto.setModel(rs.getString("Vehicle_Model_Name"));
					dto.setCompany(rs.getString("Vehicle_Company_Name"));
					dto.setVehicleId(rs.getString("Vehicle_Reg_Dtls_Id"));
					vehicleList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vehicleList;
	}
	public List<LicenseDTO> getdrivinglicenseRenewal(String licenseId)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseRenewalList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_RENEWAL_LISTS);
				pst.setString(1, licenseId);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setRenewaldate(rs.getString("renewalDate"));
					dto.setExpiryDate(rs.getString("expiryDate"));
					dto.setDeliveredon(rs.getString("deliveredOn"));
					licenseRenewalList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseRenewalList;
	}
	public List<LicenseDTO> getdrivingduplication(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> licenseDuplicationList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_DUPLICATE_LISTS);
				pst.setString(1, vo.getCidNumber());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setDuplicationdate(rs.getString("duplicate"));
					dto.setDeliveredon(rs.getString("delivered"));
					licenseDuplicationList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivinglicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseDuplicationList;
	}
	public ApplicationDataVO getLearnerInformation(ApplicationDataVO vo)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		vo.setStatus("FAILED");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_LICENSE_HAS_OFFENCE);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");

				if (rowCount > 0) {
					vo.setStatus("HAS_OFFENCE");
				} else {

					vo.setStatus("SUCCESS");
				}
			}
		} catch (Exception e) {
			vo.setStatus("FAILED");
			throw new ERALISException("###Error at CommonDAO[getLearnerInformation]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public ApplicationDataVO getLicenseInfo(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		vo.setStatus("FAILED");

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_LICENSE_HAS_OFFENCE);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0) {
					vo.setStatus("HAS_OFFENCE");
				} else {

					vo.setStatus("SUCCESS");
				}
				pst = conn.prepareStatement(CHECK_IF_LICENSE_IS_CANCELLED);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				int rowCount1 = rs.getInt("rowCount");

				if (rowCount1 > 0) {
					vo.setStatus("CANCELLED");
				} else {

					vo.setStatus("SUCCESS");
				}
				pst = conn.prepareStatement(CHECK_IF_LICENSE_IS_SUSPENDED);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				int rowCount2 = rs.getInt("rowCount");

				if (rowCount2 > 0) {
					vo.setStatus("SUSPENDED");
				} else {

					vo.setStatus("SUCCESS");
				}
			}
		} catch (Exception e) {
			vo.setStatus("FAILED");
			throw new ERALISException("###Error at CommonDAO[getLicenseInfo]: "+ e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public ApplicationDataVO getcheckCID(ApplicationDataVO vo)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		vo.setStatus("FAILED");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_LICENSE_HAS_BEEN_ISSUED);
				pst.setString(1, vo.getCidNumber());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0) {
					vo.setStatus("ISSUED");
				} else {
					vo.setStatus("SUCCESS");
				}
			}
		} catch (Exception e) {
			vo.setStatus("FAILED");
			throw new ERALISException("###Error at CommonDAO[getLearnerInformation]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public ApplicationDataVO getchecklicense(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		vo.setStatus("FAILED");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_DRIVING_LICENSE_HAS_BEEN_ISSUED);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0) {
					vo.setStatus("ISSUED");
				} else {
					vo.setStatus("SUCCESS");
				}
			}
		} catch (Exception e) {
			vo.setStatus("FAILED");
			throw new ERALISException("###Error at CommonDAO[getLearnerInformation]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public ApplicationDataVO getcheckcommerciallicense(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		vo.setStatus("FAILED");
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(CHECK_IF_NON_COMMERCIAL_DRIVING_LICENSE_HAS_BEEN_ISSUED);
				pst.setString(1, vo.getLicenseno());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				if (rowCount > 0) {
					vo.setStatus("ISSUED");
				} else {

					vo.setStatus("SUCCESS");
				}
			}
		} catch (Exception e) {
			vo.setStatus("FAILED");
			throw new ERALISException("###Error at CommonDAO[getLearnerInformation]: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return vo;
	}
	public String TotalDLIssued(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_DL_ISSUED_FOR_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_DL_ISSUED);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisTypeId);
				pst.setString(3, jurisId);
				pst.setString(4, jurisId);
			}
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	public String vehicleRgistered(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_VEHICLE_REGISTERED_FOR_HEAD_OFFICE);
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_VEHICLE_REGISTERED);
				pst.setString(1, jurisTypeId);
				pst.setString(2, jurisId);
				pst.setString(3, jurisId);
			}
			
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	public String vehicleRenewed(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_VEHICLE_RENEWED_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_VEHICLE_RENEWED);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisTypeId);
				pst.setString(3, jurisId);
				pst.setString(4, jurisId);
			}
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	
	public String licenseRenewed(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_LICENSE_RENEWED_FOR_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_LICENSE_RENEWED);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisTypeId);
				pst.setString(3, jurisId);
				pst.setString(4, jurisId);
			}
			
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	
	public String newlicensePrinted(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_NEW_LICENSE_PRINTED_FOR_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_NEW_LICENSE_PRINTED);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisId);
			}
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	public String newLearnerIssued(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_LEARNER_ISSUED_FOR_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_LEARNER_ISSUED);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisTypeId);
				pst.setString(3, jurisId);
				pst.setString(4, jurisId);
			}
			
			
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	public String getTotalPrintings(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_PRINTED_FOR_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_PRINTED);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisId);
			}
			
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	public String totalEarning(Role currentRole,String jurisId, String jurisTypeId) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Double totalAmount = 0.00;
		String result = "";
		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTOAL_EARNING_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTOAL_EARNING);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisId);
			}
			
			rs = pst.executeQuery();
			if (rs.first()) {
				if (!rs.getString("rowCount").equalsIgnoreCase("0"))
					totalAmount = rs.getDouble("total_amount")
							+ rs.getDouble("total_penalty");
				result = Double.toString(totalAmount);
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return result;
	}
	public String totalTaskCompletion(Role currentRole,String jurisId, String jurisTypeId) 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "";
		double total_submitted = 0;
		double total_verified = 0;
		double taskCompletion;

		try {
			conn = ConnectionManager.getConnection();
			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_SUBMITTED_TASK_FOR_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_SUBMITTED_TASK);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisId);
				pst.setString(3, targetSdf.format(date));
				pst.setString(4, jurisId);
				
			}
			
			rs = pst.executeQuery();

			while (rs.next()) {
				total_submitted = total_submitted + rs.getDouble("rowCount");
			}

			if(currentRole.getRoleCode().equalsIgnoreCase("HEAD_OFFICE"))
			{
				pst = conn.prepareStatement(GET_TOTAL_VERIFIED_TASK_HEAD_OFFICE);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, targetSdf.format(date));
			}
			else
			{
				pst = conn.prepareStatement(GET_TOTAL_VERIFIED_TASK);
				pst.setString(1, targetSdf.format(date));
				pst.setString(2, jurisId);
				pst.setString(3, targetSdf.format(date));
				pst.setString(4, jurisId);
				
			}
			
			rs = pst.executeQuery();

			while (rs.next()) {
				total_verified = total_verified + rs.getDouble("rowCount");
			}

			taskCompletion = (total_verified / total_submitted) * 100;
			taskCompletion = Math.round(taskCompletion * 100.0) / 100.0;
			
			DecimalFormat numberFormat = new DecimalFormat("#.00");
			result = numberFormat.format(taskCompletion);
		} 
		catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}

		return result;
	}
	
	public String getTotalEmission() {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_TOTAL_EMISSION);
			pst.setString(1, targetSdf.format(date));
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	
	public String getTotalPassedEmission() {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_TOTAL_EMISSION_PASSED_OR_FAILED);
			pst.setString(1, targetSdf.format(date));
			pst.setString(2, "PASS");
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	
	public String getTotalFailedEmission() {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_TOTAL_EMISSION_PASSED_OR_FAILED);
			pst.setString(1, targetSdf.format(date));
			pst.setString(2, "FAIL");
			rs = pst.executeQuery();
			if (rs.first()) {
				rowCount = rs.getString("rowCount");
			}
			ConnectionManager.close(conn, null, rs, pst);
		} catch (Exception exception) {
			Log.error("", exception.fillInStackTrace());
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return rowCount;
	}
	
	public String getTotalLicenseAlert(){
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		
		try {
			conn = ConnectionManager.getConnection();
			
			if(conn != null){
				pst = conn.prepareStatement(GET_LICENSE_ALERT_COUNT);
				rs = pst.executeQuery();
				rs.first();
				
				rowCount = rs.getString("rowCount");
			}
		} catch (Exception e) {
			Log.error("CommonDAO[getTotalLicenseAlert]::", e.fillInStackTrace());
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return rowCount;
	}
	
	public String getTotalApplicationAlert(){
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String rowCount = "";
		
		try {
			conn = ConnectionManager.getConnection();
			
			if(conn != null){
				pst = conn.prepareStatement(GET_APPLICATION_ALERT_COUNT);
				rs = pst.executeQuery();
				rs.first();
				
				rowCount = rs.getString("rowCount");
			}
		} catch (Exception e) {
			Log.error("CommonDAO[getTotalApplicationAlert]::", e.fillInStackTrace());
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return rowCount;
	}
	
	public List<AlertDTO> getAlertList(String identifier){
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<AlertDTO> alertList = new ArrayList<AlertDTO>();
		AlertDTO dto = null;
		
		try {
			conn = ConnectionManager.getConnection();
			
			if(conn != null){
				
				if(identifier.equalsIgnoreCase("LICENSE_PRINT_ALERT"))
				{
					pst = conn.prepareStatement(GET_LICENSE_ALERT_DETAILS);
					rs = pst.executeQuery();
					
					while(rs.next()){
						dto = new AlertDTO();
						dto.setApplicationNo(rs.getString("Application_Number"));
						dto.setServiceName(rs.getString("serviceName"));
						dto.setProcessedFrom(rs.getString("processedFrom"));
						dto.setLicenseNo(rs.getString("licenseNumber"));
						dto.setReceiptDate(rs.getString("receiptDate"));
						dto.setElapsedDuration(rs.getString("dayDifference"));
						
						alertList.add(dto);
					}
				} 
				else if(identifier.equalsIgnoreCase("APPLICATION_ALERT"))
				{
					pst = conn.prepareStatement(GET_APPLICATION_ALERT_LIST);
					rs = pst.executeQuery();
					
					while(rs.next()){
						dto = new AlertDTO();
						dto.setApplicationNo(rs.getString("Application_Number"));
						dto.setServiceName(rs.getString("service"));
						dto.setProcessedFrom(rs.getString("processedFrom"));
						dto.setReceiptDate(rs.getString("submittedOn"));
						dto.setElapsedDuration(rs.getString("dayDifference"));
						dto.setStatus(rs.getString("currentStatus"));
						
						alertList.add(dto);
					}
				}
			}
		} catch (Exception e) {
			Log.error("CommonDAO[getAlertList]::", e.fillInStackTrace());
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return alertList;
	}
	
	public String insertPaymentDetails(PaymentDTO dto, Connection conn)throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		try {
			pst = conn.prepareStatement(INSERT_INTO_T_PAYMENT_DTLS);
			pst.setString(1, dto.getApplicationNo());
			pst.setString(2, dto.getApplicationType());
			pst.setString(3, dto.getServiceId());
			pst.setString(4, dto.getAmount());
			pst.setString(5, dto.getPenalty());
			pst.setString(6, dto.getReceiptNo());
			pst.setString(7, dto.getReceiptDate());
			pst.setString(8, dto.getCreatedBy());
			int count = pst.executeUpdate();
			if (count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			throw new ERALISException();
		} 
		finally 
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		return result;
	}

	
	public String updatePayment(PaymentDTO dto, Connection conn)throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		try {
			
			pst = conn.prepareStatement(UPDATE_PAYMENT_DTLS);
			pst.setString(1, dto.getAmount());
			pst.setString(2, dto.getPenalty());
			pst.setString(3, dto.getApplicationNo());
			int count = pst.executeUpdate();
			if (count > 0)
				result = "SUCCESS";
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			throw new ERALISException();
		} 
		finally 
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		return result;
	}
	
	public synchronized String generateTOPNoFormat(String regionId) throws ERALISException,ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String topNo = null;
		try {
			conn = ConnectionManager.getConnection();
			topNo = EralisCommonUtil.generateTOPNoFormat(conn, regionId);
		} catch (Exception e) {
			throw new ERALISException(
					"###Error at CommonDAO[generateTOPNoFormat]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return topNo;
	}
	
	public NotificationDTO dispatch(String applicationNo, String requestType,String serviceType, UserDetailsVO userVo) throws ERALISException,ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Date date = new Date(System.currentTimeMillis());
		String result = "FAILURE", tableName = "";
		NotificationDTO notifyDTO = new NotificationDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				if(requestType.equalsIgnoreCase("ALL"))
				{
					String[] applicationArray = applicationNo.split("~");
					for (int i = 1; i < applicationArray.length; i++) {
						
						String serviceId = applicationArray[i].substring(2, 5);
						
						pst = conn.prepareStatement("SELECT a.`Table_Name`,a.Service_Identifier FROM t_service_master a WHERE a.`Service_Id`=?");
						pst.setString(1, serviceId);
						rs = pst.executeQuery();
						rs.first();
						tableName = rs.getString("Table_Name");
						serviceType = rs.getString("Service_Identifier");
						doDispatch(tableName,applicationArray[i],requestType,serviceType,userVo);
						notifyDTO.setStatus("SUCCESS");
					}
				}
				else
				{
					if (requestType.equalsIgnoreCase("VEHICLE")) {
						if (serviceType.equalsIgnoreCase("REGISTRATION"))
							tableName = "t_vehicle_registration_dtls";
						else if (serviceType.equalsIgnoreCase("RENEWAL"))
							tableName = "t_vehicle_renewal_dtls";
						else if (serviceType.equalsIgnoreCase("TRANSFER"))
							tableName = "t_vehicle_transfer_dtls";
						else if (serviceType.equalsIgnoreCase("DUPLICATION"))
							tableName = "t_vehicle_duplicate_dtls";
						else if (serviceType.equalsIgnoreCase("CONVERSION"))
							tableName = "t_vehicle_registration_dtls";
					} else if (requestType.equalsIgnoreCase("LICENSE")) {
						if (serviceType.equalsIgnoreCase("NEW"))
							tableName = "t_driving_license_dtls";
						else if (serviceType.equalsIgnoreCase("RENEWAL"))
							tableName = "t_driving_license_renewal_dtls";
						else if (serviceType.equalsIgnoreCase("DUPLICATION"))
							tableName = "t_driving_license_duplicate";
						else if (serviceType.equalsIgnoreCase("ENDORSEMENT"))
							tableName = "t_driving_license_endorsement";
					} else if (requestType.equalsIgnoreCase("LEARNER")) {
						if (serviceType.equalsIgnoreCase("NEW"))
							tableName = "t_learner_license_dtls";
						else if (serviceType.equalsIgnoreCase("RENEWAL"))
							tableName = "t_learner_license_renewal_dtls";
						else if (serviceType.equalsIgnoreCase("DUPLICATION"))
							tableName = "t_learner_license_duplicate_dtls";
					} else if (requestType.equalsIgnoreCase("TOP")) {
						if (serviceType.equalsIgnoreCase("NEW"))
							tableName = "t_top_dtls";
						else if (serviceType.equalsIgnoreCase("REPLACEMENT"))
							tableName = "t_top_replacement_dtls";
					}
					
					
					notifyDTO = doDispatch(tableName,applicationNo,requestType,serviceType,userVo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return notifyDTO;
	}
	
	public NotificationDTO doDispatch(String tableName,String applicationNo, String requestType,String serviceType, UserDetailsVO userVo) throws ERALISException,ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Date date = new Date(System.currentTimeMillis());
		String result = "FAILURE";
		NotificationDTO notifyDTO = new NotificationDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) {
					String query = "UPDATE "
						+ tableName
						+ " a SET a.`Dispatched_By`=?, a.`Dispatched_Date`=? WHERE a.`Application_Number`=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, userVo.getActorId());
					pst.setDate(2, date);
					pst.setString(3, applicationNo);
					int count = pst.executeUpdate();
					
					if (count > 0)
					{
						pst = conn.prepareStatement("SELECT Service_Id FROM `t_workflow_dtls` WHERE Application_Number=?");
						pst.setString(1, applicationNo);
						rs = pst.executeQuery();
						rs.first();
						String serviceId = rs.getString("Service_Id");
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNo);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceId);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						new WorkflowManager(conn).logCompletedWorkflow(vo);
						
						String query1 = null, customerId = null;
						if(serviceType.equalsIgnoreCase("TRANSFER")){
							query1 = "SELECT a.`Transferee_Customer_Id` FROM " + tableName + " a WHERE `Application_Number` = ?";
							pst = conn.prepareStatement(query1);
							pst.setString(1, applicationNo);
							rs = pst.executeQuery();
							rs.first();
							customerId = rs.getString("Transferee_Customer_Id");
						}
						else {
							query1 = "SELECT a.`Customer_Id` FROM " + tableName + " a WHERE `Application_Number` = ?";
							pst = conn.prepareStatement(query1);
							pst.setString(1, applicationNo);
							rs = pst.executeQuery();
							rs.first();
							customerId = rs.getString("Customer_Id");
						}
						
						if(requestType.equalsIgnoreCase("VEHICLE"))
						{
							String customerType = customerId.substring(0,1);
							if(customerType.equals("P"))
							{
								pst = conn.prepareStatement(GET_PERSONAL_CUSTOMER_INFORMATION);
								pst.setString(1, customerId);
								rs = pst.executeQuery();
								rs.first();
								notifyDTO.setCustomerName(rs.getString("pname"));
								notifyDTO.setApprovalDate(rs.getString("currentDate"));
								notifyDTO.setEmailAddress(rs.getString("Present_Email"));
								notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
								notifyDTO.setStatus("SUCCESS");
							}
							else
							{
								pst = conn.prepareStatement(GET_ORGANISATION_CUSTOMER_INFORMATION);
								pst.setString(1, customerId);
								rs = pst.executeQuery();
								rs.first();
								
								notifyDTO.setCustomerName(rs.getString("Organization_Name"));
								notifyDTO.setEmailAddress(rs.getString("Email_Id"));
								notifyDTO.setMobileNumber(rs.getString("Phone_Number"));
								notifyDTO.setApprovalDate(rs.getString("currentDate"));
								notifyDTO.setStatus("SUCCESS");
							}
						}
						else if(requestType.equalsIgnoreCase("LICENSE") ||
								requestType.equalsIgnoreCase("LEARNER") ||
								requestType.equalsIgnoreCase("TOP"))
						{
							pst = conn.prepareStatement(GET_PERSONAL_CUSTOMER_INFORMATION);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
						
							notifyDTO.setCustomerName(rs.getString("pname"));
							notifyDTO.setApprovalDate(rs.getString("currentDate"));
							notifyDTO.setEmailAddress(rs.getString("Present_Email"));
							notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
							notifyDTO.setStatus("SUCCESS");
						}
						
						notifyDTO.setRequestType(requestType);
						result = "SUCCESS";
					}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return notifyDTO;
	}
	
	
	public List<LicenseDTO> getTOPSearchList(LicenseDTO dto,String searchType) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> topSearchList = new ArrayList<LicenseDTO>();
		LicenseDTO licenseDTO;
		String query = "", where = "";
		int count = 1;
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				if (!dto.getTopNo().equalsIgnoreCase("NA")) {
					where = where + " AND a.`TOP_Number` = ? ";
				}
				if(!dto.getVehicleNo().equalsIgnoreCase("NA")) {
					where = where + " AND b.`Vehicle_Number`= ? ";
				}
				if(!dto.getCID().equalsIgnoreCase("NA")) {
					where = where + " AND c.`CID_Number`= ? ";
				}
				if(!dto.getLicenseNo().equalsIgnoreCase("NA")) {
					where = where + " AND d.`Driving_License_No`= ? ";
				}
				if(searchType.equalsIgnoreCase("TOP_DTLS"))
					query = GET_TOP_SEARCH_LIST;
				else if(searchType.equalsIgnoreCase("TOP_HISTORY"))
					query = GET_TOP_SEARCH_LIST_FOR_HISTORY;
				pst = conn.prepareStatement(query + where+" GROUP BY a.`Customer_Id`");
				if (!dto.getTopNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getTopNo());
					count = count + 1;
				}
				if(!dto.getVehicleNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getVehicleNo());
					count = count + 1;
				}
				if(!dto.getCID().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getCID());
					count = count + 1;
				}
				if(!dto.getLicenseNo().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getLicenseNo());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while(rs.next())
				{
					licenseDTO = new LicenseDTO();
					licenseDTO.setCustomerId(rs.getString("Customer_Id"));
					licenseDTO.setLicenseNo(rs.getString("Driving_License_No"));
					licenseDTO.setTopNo(rs.getString("TOP_Number"));
					licenseDTO.setName(rs.getString("pname"));
					licenseDTO.setRegion(rs.getString("region"));
					licenseDTO.setDzongkhag(rs.getString("dzongkhag"));
					licenseDTO.setExactLocation(rs.getString("Exact_Location"));
					topSearchList.add(licenseDTO);
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		return topSearchList;
	}
	
	public List<ServiceDTO> getTestLocationForAdmin() throws ERALISException,
			ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ServiceDTO> testLocationList = new ArrayList<ServiceDTO>();
		ServiceDTO dto;
		try {

			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_TEST_LOCATION_LIST_FOR_ADMIN);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new ServiceDTO();
					dto.setTestLocation(rs.getString("testLocation"));
					dto.setTestLocationId(rs.getString("Juris_Type_Id")+"#"+rs.getString("Test_Location_Id"));
					testLocationList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return testLocationList;
	}
	
	public List<ServiceDTO> getTestLocation() throws ERALISException,
			ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ServiceDTO> testLocationList = new ArrayList<ServiceDTO>();
		ServiceDTO dto;
		try {

			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_TEST_LOCATION_LIST);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new ServiceDTO();
					dto.setTestLocation(rs.getString("testLocation"));
					dto.setTestLocationId(rs.getString("Juris_Type_Id")+"#"+rs.getString("Test_Location_Id"));
					testLocationList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return testLocationList;
	}
	
	public List<DriveTypeDTO> get_learner_drive_type_for_resubmit(String applicationNo,String serviceCode)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DriveTypeDTO> drivetypeList = new ArrayList<DriveTypeDTO>();
		DriveTypeDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LEARNER_DRIVETYPE);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new DriveTypeDTO();
					dto.setDriveTypeId(rs.getString("Drive_Type_Id"));
					dto.setDriveType(rs.getString("Drive_Type_Name"));
					if(serviceCode.equalsIgnoreCase("ILL"))
					{
						boolean isChecked = get_drivetype_from_applicationNo(applicationNo, dto.getDriveTypeId(), conn);
						if (isChecked)
							dto.setIsChecked("Y");
						else
							dto.setIsChecked("N");
						drivetypeList.add(dto);
					}
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivetype_licenselist]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return drivetypeList;
	}
	private boolean get_drivetype_from_applicationNo(String applicationNo,String driveTypeId, Connection conn) throws ERALISException,ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean isChecked = false;
		try {
			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVE_TYPE_FROM_APPLICATION_NO);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				while (rs.next()) {
					if (driveTypeId.equals(rs.getString("Drive_Type_Id")))
						isChecked = true;
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		return isChecked;
	}
	public synchronized String do_resubmit(String serviceCode,ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		String serviceId="";
		try
		{
			if (conn != null)
			{	
			    if(serviceCode.equalsIgnoreCase("ILL"))
			    {
				    update_new_learner_license_application(dto, userVo,conn);
				    delete_by_application_no("driveType",DELETE_DRIVE_TYPE_FROM_LEARNER_LICENSE_APPLICATION,dto, userVo,conn);
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
				    insert_drive_type(INSERT_INTO_T_LEARNER_LICENSE_DRIVE_TYPE_APPLICATION,dto, userVo,conn);
				    
				    String SERVICE_NAME	=	"LEARNER";
				    String DOCUMENT_TYPE=	"LL";
				    String APPLICATION_NO	=	applicationNumber;
				    if(null!=dto.getFileMC())
				    {
					    byte[] FILE_DATA	=	dto.getFileMC().getFileData();
					    String FILE_NAME	=	dto.getFileMC().getFileName();
					    file_upload(SERVICE_NAME,FILE_DATA,FILE_NAME,DOCUMENT_TYPE,APPLICATION_NO,conn);
				    }
				    if(null!=dto.getFileApplForm())
				    {
				    	byte[] FILE_DATA	=	dto.getFileApplForm().getFileData();
					    String FILE_NAME	=	dto.getFileApplForm().getFileName();
					    file_upload(SERVICE_NAME,FILE_DATA,FILE_NAME,DOCUMENT_TYPE,APPLICATION_NO,conn);
				    }
			    }
			    else if(serviceCode.equalsIgnoreCase("REGV"))
			    {
			    	String SERVICE_NAME	=	"VEHICLE_REGISTRATION";
			    	String DOCUMENT_TYPE=	"RC";
			    	update_new_vehicle_registration(dto, userVo,conn);
			    	if(null!=dto.getInvoice())
			    	{
					    file_upload(SERVICE_NAME,dto.getInvoice().getFileData(),dto.getInvoice().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	}	
			    	if(null!=dto.getChallan())
			    	{
					    file_upload(SERVICE_NAME,dto.getChallan().getFileData(),dto.getChallan().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	}
			    	if(null!=dto.getLetterOfAuthenticity())
			    	{
					    file_upload(SERVICE_NAME,dto.getLetterOfAuthenticity().getFileData(),dto.getLetterOfAuthenticity().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	}
			    	if(null!=dto.getEmission())
			    	{
					    file_upload(SERVICE_NAME,dto.getEmission().getFileData(),dto.getEmission().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    	if(null!=dto.getExemptionCertificate())
			    	{
					    file_upload(SERVICE_NAME,dto.getExemptionCertificate().getFileData(),dto.getExemptionCertificate().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    	if(null!=dto.getExciseInvoice())
			    	{
					    file_upload(SERVICE_NAME,dto.getExemptionCertificate().getFileData(),dto.getExemptionCertificate().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	}  
			    	if(null!=dto.getCustomDeclaration())
			    	{
					    file_upload(SERVICE_NAME,dto.getCustomDeclaration().getFileData(),dto.getCustomDeclaration().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	}  
			    	if(null!=dto.getVehiclePicture())
			    	{
					    file_upload(SERVICE_NAME,dto.getVehiclePicture().getFileData(),dto.getVehiclePicture().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	}  
			    }
			    else if(serviceCode.equalsIgnoreCase("RV"))
			    {
			    	if(dto.getRenewalType().equalsIgnoreCase("E"))
					{
				    	String SERVICE_NAME	=	"VEHICLE_RENEWAL";
				    	String DOCUMENT_TYPE=	"RC";
				    	
				    	update_vehicle_renewal(dto, userVo,conn);
				    	if(null!=dto.getSupportingDocument())
				    	{
				    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
						    file_upload(SERVICE_NAME,dto.getSupportingDocument().getFileData(),dto.getSupportingDocument().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
				    	} 
					}
			    	else
			    	{
			    		 delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    	}
			    }
			    else if(serviceCode.equalsIgnoreCase("RLL"))
			    {
			    	String SERVICE_NAME	=	"LEARNER";
			    	String DOCUMENT_TYPE=	"LL";
			    	update_learner_renewal(dto, userVo,conn);
			    	if(null!=dto.getSupportDoc())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportDoc().getFileData(),dto.getSupportDoc().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    	
			    }
			    else if(serviceCode.equalsIgnoreCase("INDL"))
			    {
			    	String SERVICE_NAME	=	"LICENSE";
			    	String DOCUMENT_TYPE=	"DL";
			    	update_driving_license(dto, userVo,conn);
			    	if(null!=dto.getFileApplForm())
			    	{
					    file_upload(SERVICE_NAME,dto.getFileApplForm().getFileData(),dto.getFileApplForm().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    	if(null!=dto.getRcCertificate())
			    	{
					    file_upload(SERVICE_NAME,dto.getRcCertificate().getFileData(),dto.getRcCertificate().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    else if(serviceCode.equalsIgnoreCase("ICDL"))
			    {
			    	update_commercial_license_issuance(dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("RDL"))
			    {
			    	String SERVICE_NAME	=	"LICENSE";
			    	String DOCUMENT_TYPE=	"DL";
			    	update_license_renewal(dto, userVo,conn);
			    	if(null!=dto.getSupportDoc())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportDoc().getFileData(),dto.getSupportDoc().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    else if(serviceCode.equalsIgnoreCase("DDL"))
			    {
			    	String SERVICE_NAME	=	"LICENSE";
			    	String DOCUMENT_TYPE=	"DL";
			    	update_license_renewal(dto, userVo,conn);
			    	if(null!=dto.getSupportDoc())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportDoc().getFileData(),dto.getSupportDoc().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    else if(serviceCode.equalsIgnoreCase("EDL"))
			    {
			    	String SERVICE_NAME	=	"LICENSE";
			    	String DOCUMENT_TYPE=	"DL";
			    	update_license_endorse(dto, userVo,conn);
			    	if(null!=dto.getSupportDoc())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportDoc().getFileData(),dto.getSupportDoc().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    else if(serviceCode.equalsIgnoreCase("DLL"))
			    {
			    	String SERVICE_NAME	=	"LEARNER";
			    	String DOCUMENT_TYPE=	"LL";
			    	update_learner_replacement(dto, userVo,conn);
			    	if(null!=dto.getSupportDoc())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportDoc().getFileData(),dto.getSupportDoc().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    else if(serviceCode.equalsIgnoreCase("TV"))
			    {
			    	String SERVICE_NAME	=	"VEHICLE_TRANSFER";
			    	String DOCUMENT_TYPE=	"RC";
			    	
			    	update_vehicle_transfer(dto, userVo,conn);
			    	if(null!=dto.getSupportingDocument())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportingDocument().getFileData(),dto.getSupportingDocument().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    else if(serviceCode.equalsIgnoreCase("CV"))
			    {
			    	String SERVICE_NAME	=	"VEHICLE_CONVERSION";
			    	String DOCUMENT_TYPE=	"RC";
			    	
			    	//update_vehicle_conversion(dto, userVo,conn);
			    	if(null!=dto.getSupportingDocument())
			    	{
			    		delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
					    file_upload(SERVICE_NAME,dto.getSupportingDocument().getFileData(),dto.getSupportingDocument().getFileName(),DOCUMENT_TYPE,applicationNumber,conn);
			    	} 
			    }
			    pst = conn.prepareStatement(GET_SERVICE_CODE_FROM_SHORT_DESC);
				pst.setString(1, serviceCode);
				rs = pst.executeQuery();
				rs.first();
				serviceId	=	rs.getString("Service_Id");
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(applicationNumber);
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId(serviceId);
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				vo.setJurisTypeId(userVo.getJurisdictionTypeId());
				new WorkflowManager(conn).logResubmissionWorkflow(vo);
				result="SUCCESS"; 
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at commondao[do_resubmit]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result+"#"+applicationNumber;
	}
	public synchronized String deleteApplication(String serviceCode,ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		String serviceId="";
		try
		{
			if (conn != null)
			{	
			    if(serviceCode.equalsIgnoreCase("ILL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	

			    	pst = conn.prepareStatement("DELETE FROM t_learner_licn_drive_type_application WHERE Application_Number=?"); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_LEARNER_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("REGV"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_VEHICLE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);

			    }
			    else if(serviceCode.equalsIgnoreCase("RV"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_VEHICLE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
				    	
			    }
			    else if(serviceCode.equalsIgnoreCase("RLL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_LEARNER_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    	
			    }
			    else if(serviceCode.equalsIgnoreCase("INDL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_DRIVING_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("ICDL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_DRIVING_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("RDL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_DRIVING_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("DDL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_DRIVING_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("EDL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_DRIVING_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("DLL"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_LEARNER_LICENSE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("TV"))
			    {
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_VEHICLE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    }
			    else if(serviceCode.equalsIgnoreCase("CV"))
			    {
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_FROM_PAYMENT_TABLE); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_VEHICLE_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
			    	
			    }
			    else if(serviceCode.equalsIgnoreCase("TOP"))
			    {
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_TASK_DTLS); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW_AUDIT); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    	
			    	pst = conn.prepareStatement(DELETE_WORK_FLOW); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();

			    	pst = conn.prepareStatement(DELETE_TOP_APPLICATION); 
			    	pst.setString(1, applicationNumber);
			    	pst.executeUpdate();
			    }
				result="SUCCESS"; 
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result+"#"+applicationNumber;
	}
	public synchronized String update_new_learner_license_application(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_NEW_LEARNER_LICENSE_APPLICATION);
		    	pst.setString(count++, dto.getCertifyingDoctor());
				pst.setString(count++, dto.getRemarks());
		    	pst.setString(count++, userVo.getActorId());
				pst.setDate(count++, date);
		    	pst.setString(count++, applicationNumber);
		    	pst.executeUpdate();
		    	result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String clearPendingTask1(String pendingId) throws ERALISException, ERALISException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		Connection conn = null;
		try 
		{
			 
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
		
	}
	public synchronized String clearPendingTask(String pendingId,Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try {
			if(conn != null)
			{
				pst=conn.prepareStatement(CLEAR_PENDING_TASK);
				pst.setString(1, pendingId);
				int count	=	pst.executeUpdate();
				if(count>0)
				result = "SUCCESS";
			}
		}
		catch (Exception e) {
			result = "FAILURE";
			throw new ERALISException("###Error at licenseDAO[pay_offence]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;

	}
	public synchronized String update_new_vehicle_registration(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_NEW_VEHICLE_APPLICATION_DTLS);
				pst.setString(count++, dto.getRegistrationType());
				pst.setString(count++, dto.getVehicleRegistrationId());
				pst.setString(count++, dto.getVehicleType());
				pst.setString(count++, dto.getVehicleCompany());
				pst.setString(count++, dto.getVehicleModel());
				pst.setString(count++, dto.getEngineType());
				pst.setString(count++, dto.getEngineNumber());
				pst.setString(count++, dto.getChasisNumber());
				pst.setString(count++, dto.getColour());
				pst.setString(count++, dto.getPrice());
				pst.setString(count++, dto.getLoadCapacity());
				pst.setString(count++, dto.getDealersName());
				pst.setString(count++, dto.getSeatCapacity());
				String vehicleHorsePower;
				if(dto.getVehicleHorsePower().equalsIgnoreCase(""))
				{
					vehicleHorsePower="0.00";
				}
				else{vehicleHorsePower=dto.getVehicleHorsePower();}
				
				pst.setString(count++, vehicleHorsePower);
				String vehilceKiloWatt;
				if(dto.getVehicleKiloWatt().equalsIgnoreCase(""))
				{
					vehilceKiloWatt="0.00";
				}
				else{vehilceKiloWatt=dto.getVehicleKiloWatt();}
				pst.setString(count++, vehilceKiloWatt);
				pst.setString(count++, dto.getUnladenWeight());
				java.util.Date purchaseDate = sourceSdf.parse(dto.getPurchaseDate());
				String purchaseDate1 = targetSdf.format(purchaseDate);
				pst.setString(count++, purchaseDate1 );
				pst.setString(count++, dto.getManufactureYear());
				pst.setString(count++, dto.getEngineCC());
				pst.setString(count++, dto.getManufactureCountry());
				pst.setString(count++, dto.getPurchaseType());
				pst.setString(count++, dto.getBusType());
				pst.setString(count++, applicationNumber);
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{	
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_learner_renewal(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_LEARNER_RENEWAL);
				pst.setString(count++, dto.getRemarks()); 
				pst.setString(count++, userVo.getActorId()); 
				pst.setString(count++, dto.getRenewalDuration() ); 
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_vehicle_renewal(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_VEHICLE_RENEWAL);
				pst.setString(count++, dto.getRenewalDuration()); 
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_driving_license(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_NON_COMMERCIAL_ISSUE);
				pst.setString(count++, dto.getRemarks()); 
				pst.setString(count++, dto.getDrivetype()); 
				pst.setString(count++, userVo.getActorId()); 
				pst.setString(count++, dto.getIssueType());
				pst.setString(count++, dto.getRenewalDuration());
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{	
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String delete_by_application_no(String deleteType,String query,ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	
				if(deleteType.equalsIgnoreCase("file"))
		    	{
		    		pst = conn.prepareStatement(GET_FILE_PATH_FROM_DOC);
					pst.setString(1, dto.getApplicationNo());
					rs = pst.executeQuery();
					while (rs.next()) 
					{
						File filePath = new File(rs.getString("Document_Path"));
						if(filePath.exists())
							filePath.delete();
					}
		    	}
				pst = conn.prepareStatement(query); 
		    	pst.setString(1, applicationNumber);
		    	pst.executeUpdate();
		    	result	=	"SUCCESS";
			}
		} 
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String insert_drive_type(String query,ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	
				String[] driveTypeArray = dto.getDriveTypeList().split("#");
				for (int i = 0; i < driveTypeArray.length; i++) {
					if (!(driveTypeArray[i].equals("0"))) {
						pst = conn.prepareStatement(query);
						pst.setString(1, applicationNumber);
						pst.setString(2, driveTypeArray[i]);
						pst.executeUpdate();
						result="SUCCESS";
					}
				}
			}
		} 
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String file_upload(String SERVICE_NAME,byte[] FILE_DATA,String FILE_NAME,String DOCUMENT_TYPE,String APPLICATION_NO, Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	 
				DocumentVO documentVO = new DocumentVO();
				documentVO.setServiceName(SERVICE_NAME);
				documentVO.setFileContent(FILE_DATA);
				documentVO.setName(FILE_NAME);
				documentVO.setDocumentType(DOCUMENT_TYPE);
				CommonDAO.getInstance().fileUploader(documentVO, APPLICATION_NO);
			}
		} 
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String reject_application(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String serviceId="";
		ResultSet rs	=	null;
		try 
		{
			if (conn != null) 
			{	
				pst = conn.prepareStatement(GET_SERVICE_CODE_FROM_SHORT_DESC);
				pst.setString(1, dto.getServiceCode());
				rs = pst.executeQuery();
				rs.first();
				serviceId	=	rs.getString("Service_Id");
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNo());
				vo.setServiceId(serviceId);
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(userVo.getJurisdictionId());
				vo.setJurisTypeId(userVo.getJurisdictionTypeId());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				pst=conn.prepareStatement(UPDATE_REJECTION_REASON);
				pst.setString(1,dto.getReason());
				pst.setString(2,dto.getApplicationNo());
				int count1 = pst.executeUpdate();
				if (count1 > 0)
				{
					result = "SUCCESS";
				}
			}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[reject_application]:: "+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String savePendingPrint(PrintDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null) 
			{	
				pst=conn.prepareStatement(UPDATE_PRINT_PENDING);
				pst.setString(1,"P");
				pst.setString(2,dto.getRemarks());
				pst.setString(3,dto.getDocumentId());
				int count1 = pst.executeUpdate();
				if (count1 > 0)
				{
					result = "SUCCESS";
				}
			}
				
		} catch (Exception e) {
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[reject_application]:: "+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String publishReportQuery(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null) 
			{	
				int count=1;
				pst=conn.prepareStatement(INSERT_INTO_REPORT_CONFIGURATION);
				pst.setString(count++,dto.getTitle());
				pst.setString(count++,dto.getDescription());
				pst.setString(count++,dto.getSqlQuery());
				pst.setString(count++,dto.getWhereParam());
				pst.setString(count++,userVo.getActorId());
				int count1 = pst.executeUpdate();
				if (count1 > 0)
				{
					result = "SUCCESS";
				}
			}
				
		} catch (Exception e) {
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[reject_application]:: "+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public List<LicenseDTO>   getLearnerLicenseDtls(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> learnerLicense = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_LEANER_LICENSE_DTLS);
				pst.setString(1, vo.getLearnerLicenseId());

				rs = pst.executeQuery();

				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setApplicationNo(rs.getString("Application_Number"));
					dto.setIssuedate(rs.getString("issue_date"));
					dto.setLearnerlicenseNo(rs.getString("Learner_License_No"));
					dto.setReceiptDate(rs.getString("receipt_date"));
					dto.setCertifyingDoctor(rs.getString("Certifying_Doctor"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					learnerLicense.add(dto);
				}
			}
		} 
		catch (Exception e)
		{
			throw new ERALISException("###Error at CommonDAO[getlearnerlicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return learnerLicense;
	}
	public List<LicenseDTO> getLicenseOffense(ApplicationDataVO vo)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> learnerLicense = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_LEANER_LICENSE_DTLS);
				pst.setString(1, vo.getLearnerLicenseId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setIssuedate(rs.getString("issue_date"));
					dto.setLearnerlicenseNo(rs.getString("Learner_License_No"));
					dto.setReceiptDate(rs.getString("receipt_date"));
					dto.setCertifyingDoctor(rs.getString("Certifying_Doctor"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					learnerLicense.add(dto);
				}
			}
		} 
		catch (Exception e)
		{
			throw new ERALISException("###Error at CommonDAO[getlearnerlicense]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return learnerLicense;
	}
	public synchronized String update_commercial_license_issuance(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_COMMERCIAL_ISSUE);
				pst.setString(count++, dto.getRenewalDuration()); 
				pst.setString(count++, dto.getDrivetype()); 
				pst.setString(count++, dto.getRemarks()); 
				pst.setString(count++, userVo.getActorId()); 
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{	
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_license_renewal(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_DRIVING_LICENSE_RENEWAL); 
				pst.setString(count++, dto.getRenewalDuration()); 
				pst.setString(count++, dto.getRemarks()); 
				pst.setString(count++, userVo.getActorId()); 
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{	
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_license_endorse(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				String driveType = null;
				if(dto.getTcbEndorsement().equals("Y"))
				{
					for(int i=0; i<dto.getDrivetypeTCB().length; i++)
					{
						if(i == 0)
							driveType = dto.getDrivetypeTCB()[i];
						else
							driveType = driveType+"~"+dto.getDrivetypeTCB()[i];
					}
				}
				else
				{
					driveType = dto.getDrivetype();
				}
				pst = conn.prepareStatement(UPDATE_LICENSE_ENDORSE);
				pst.setString(count++, dto.getTcbEndorsement());  
				pst.setString(count++, driveType);  
				pst.setString(count++, dto.getRemarks()); 
				pst.setString(count++, userVo.getActorId()); 
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[update_license_endorse]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_learner_replacement(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_LEARNER_LICENSE_REPLACEMENT);
				pst.setString(count++, dto.getRemarks()); 
				pst.setString(count++, userVo.getActorId()); 
				pst.setString(count++, dto.getApplicationNo()); 
				int result_count	=	pst.executeUpdate();
				if(result_count>0)
					result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{	
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[update_license_endorse]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_vehicle_transfer(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
				if((null!=dto.getTransfereeCustomerId()) && (!"".equalsIgnoreCase(dto.getTransfereeCustomerId())))
				{
						pst = conn.prepareStatement(UPDATE_VEHICLE_TRANSFER);
						pst.setString(count++, dto.getVehicleRegistrationType()); 
						pst.setString(count++, dto.getSaleDeedAmount());
						java.util.Date saleDeedDate = sourceSdf.parse(dto.getSaleDeedDate());
						String saleDeedDate1 = targetSdf.format(saleDeedDate);
						  
						pst.setString(count++, saleDeedDate1);
						pst.setString(count++, dto.getTransfereeCustomerId());
						pst.setString(count++, userVo.getActorId()); 
						pst.setString(count++, dto.getApplicationNo()); 
						int result_count	=	pst.executeUpdate();
						if(result_count>0)
							result="SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[update_license_endorse]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public synchronized String update_vehicle_conversion(ServiceDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null)
			{	int count=1;
					pst = conn.prepareStatement(UPDATE_VEHICLE_CONVERSION);
					pst.setString(count++, dto.getConversionReason()); 
					pst.setString(count++, dto.getVehicleRegistrationId()); 
					pst.setString(count++, dto.getRegion()); 
					pst.setString(count++, dto.getVehicleType()); 
					pst.setString(count++, userVo.getActorId()); 
					pst.setString(count++, dto.getApplicationNo()); 
					int result_count	=	pst.executeUpdate();
					if(result_count>0)
						result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{	
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at CommonDAO[update_license_endorse]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	public List<DailyTaskUserDTO> getUserList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DailyTaskUserDTO> userList = new ArrayList<DailyTaskUserDTO>();
		DailyTaskUserDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_USER_LIST);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new DailyTaskUserDTO();
					dto.setEmployeeName(rs.getString("uname"));
					dto.setRegionName(rs.getString("region"));
					dto.setUserId(rs.getString("login_id"));
					dto.setJurisId(jurisId);
					dto.setJurisTypeId(jurisTypeId);
					userList.add(dto);
				}
			}
		}  
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return userList;
	}
	

	public String bookDrivingTest(ServiceDTO dto,String issueType,String cid,String learnerNo,Connection conn) throws ERALISException, ERALISSystemException
	{ 
		PreparedStatement pst = null;
		ResultSet rs 	= null;
		String result	=	null;
		int slNo = 0;
		String bsfOrderNo = null;
		int n = 0;
		
		try
		{
			if(conn != null)
			{
				String[] tempArray = dto.getTestLocation().split("#");
				
				pst	=	conn.prepareStatement(CHECK_DRIVING_TEST_SEAT);
				pst.setString(1, dto.getTestDate());
				pst.setString(2, tempArray[0]);
				pst.setString(3, tempArray[1]);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("totalApplicant")<rs.getInt("Max_Applicants"))
				{
					Random rand = new Random();
					n = rand.nextInt(99999999) + 1;
					
					pst	= conn.prepareStatement(BOOK_DRIVING_TEST, PreparedStatement.RETURN_GENERATED_KEYS);
					pst.setString(1, "A"+n);
					//if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType))
					if("Driving_Institue".equalsIgnoreCase(dto.getIssueType()) || "Learner_License".equalsIgnoreCase(dto.getIssueType()))
					{
						pst.setString(2, dto.getLearnerNo());
						pst.setString(3, dto.getDob());
					}
					else
					{
						//pst.setString(2, learnerNo);
						pst.setString(2, dto.getLearnerNo());
						pst.setString(3, dto.getDob());
					}
					
					pst.setString(4, dto.getDrivetype());
					pst.setString(5, tempArray[1]); 
					pst.setString(6, dto.getTestDate()); 
					pst.setString(7, "CITIZEN");
					
					pst.setString(8, dto.getTestType());
					if("Driving_Institue".equalsIgnoreCase(dto.getIssueType()) || "Learner_License".equalsIgnoreCase(dto.getIssueType()))
					{
						pst.setString(9, dto.getCustomerID());
					}
					else
					{
						//pst.setString(9,cid);
						pst.setString(9,dto.getCid());
					}
	
					pst.setString(10, tempArray[0]);
					pst.setString(11, dto.getIssueType());
					int count = pst.executeUpdate();
					if(count > 0)
					{	
						result 	= "SUCCESS";
					} 
				}
				else
				{
					result 	= "NO_SEAT_AVAILABLE";
				}
			}
		} 
		catch (Exception e) 
		{ 
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+"#"+bsfOrderNo+"#B"+n;
	}
	
	public String daily_task(List<DailyTaskUserDTO> userList, String startDate, String endDate, String requestType, String reportType) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer sbf = new StringBuffer();
		
		// Learner License Issuance 
		int illSubmittedCount = 0, illApprovedCount = 0, illRejectedCount = 0, illResubmittedCount = 0, illVerifiedCount = 0, illDispatchedCount = 0;
		// Learner License Renewal
		int rllSubmittedCount = 0, rllApprovedCount = 0, rllRejectedCount = 0, rllResubmittedCount = 0, rllVerifiedCount = 0, rllDispatchedCount = 0;
		// Learner License replacement
		int dllSubmittedCount = 0, dllApprovedCount = 0, dllRejectedCount = 0, dllResubmittedCount = 0, dllVerifiedCount = 0, dllDispatchedCount = 0;
		
		// Non-commercial driving License issuance
		int indlSubmittedCount = 0, indlApprovedCount = 0, indlRejectedCount = 0, indlResubmittedCount = 0, indlVerifiedCount = 0, indlDispatchedCount = 0;
		// commercial driving License issuance
		int icdlSubmittedCount = 0, icdlApprovedCount = 0, icdlRejectedCount = 0, icdlResubmittedCount = 0, icdlVerifiedCount = 0, icdlDispatchedCount = 0;
		// renewal driving License 
		int rdlSubmittedCount = 0, rdlApprovedCount = 0, rdlRejectedCount = 0, rdlResubmittedCount = 0, rdlVerifiedCount = 0, rdlDispatchedCount = 0;
		// replacement driving License 
		int ddlSubmittedCount = 0, ddlApprovedCount = 0, ddlRejectedCount = 0, ddlResubmittedCount = 0, ddlVerifiedCount = 0, ddlDispatchedCount = 0;
		// endorsement driving License 
		int edlSubmittedCount = 0, edlApprovedCount = 0, edlRejectedCount = 0, edlResubmittedCount = 0, edlVerifiedCount = 0, edlDispatchedCount = 0;
		
		// vehicle registration
		int regvSubmittedCount = 0, regvApprovedCount = 0, regvRejectedCount = 0, regvResubmittedCount = 0, regvVerifiedCount = 0, regvDispatchedCount = 0;
		// vehicle renewal
		int rvSubmittedCount = 0, rvApprovedCount = 0, rvRejectedCount = 0, rvResubmittedCount = 0, rvVerifiedCount = 0, rvDispatchedCount = 0;
		// vehicle transfer
		int tvSubmittedCount = 0, tvApprovedCount = 0, tvRejectedCount = 0, tvResubmittedCount = 0, tvVerifiedCount = 0, tvDispatchedCount = 0;
		// vehicle replacement
		int dvSubmittedCount = 0, dvApprovedCount = 0, dvRejectedCount = 0, dvResubmittedCount = 0, dvVerifiedCount = 0, dvDispatchedCount = 0;
		// vehicle conversion
		int cvSubmittedCount = 0, cvApprovedCount = 0, cvRejectedCount = 0, cvResubmittedCount = 0, cvVerifiedCount = 0, cvDispatchedCount = 0;
		
		// TOP Issuance
		int topSubmittedCount = 0, topApprovedCount = 0, topRejectedCount = 0, topResubmittedCount = 0, topVerifiedCount = 0, topDispatchedCount = 0;
		
		try  
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				for(int i=0;i<userList.size();i++)
				{
					if(reportType.equals("initial"))
					{
						pst = conn.prepareStatement(GET_DAILY_TASK_REPORT_INITIAL);
						
						pst.setString(1, userList.get(i).getUserId());
						Format formatter = new SimpleDateFormat("yyyy-MM-dd");
						Date today = new Date(System.currentTimeMillis());
						String todayStr = formatter.format(today);
						pst.setString(2, todayStr+"%");
						pst.setString(3, userList.get(i).getUserId());
						pst.setString(4, todayStr+"%");
						
					}
					else
					{
						pst = conn.prepareStatement(GET_DAILY_TASK_REPORT);
						
						pst.setString(1, userList.get(i).getUserId());
						java.util.Date from = sourceSdf.parse(startDate);
						String fromDate = targetSdf.format(from);
						pst.setString(2, fromDate);
						java.util.Date to = sourceSdf.parse(endDate);
						String tillDate = targetSdf.format(to);
						pst.setString(3, tillDate);
						pst.setString(4, userList.get(i).getUserId());
						pst.setString(5, fromDate);
						pst.setString(6, tillDate);
					}
					
					rs = pst.executeQuery();
					
					while(rs.next())
					{
						if(rs.getString("Service_Short_Desc").equalsIgnoreCase("ILL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								illSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								illApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								illRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								illResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								illVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								illDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("RLL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								rllSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								rllApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								rllRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								rllResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								rllVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								rllDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("DLL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								dllSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								dllApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								dllRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								dllResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								dllVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								dllDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("INDL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								indlSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								indlApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								indlRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								indlResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								indlVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								indlDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("ICDL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								icdlSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								icdlApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								icdlRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								icdlResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								icdlVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								icdlDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("RDL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								rdlSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								rdlApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								rdlRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								rdlResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								rdlVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								rdlDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("DDL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								ddlSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								ddlApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								ddlRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								ddlResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								ddlVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								ddlDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("EDL"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								edlSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								edlApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								edlRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								edlResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								edlVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								edlDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("REGV"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								regvSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								regvApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								regvRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								regvResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								regvVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								regvDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("RV"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								rvSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								rvApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								rvRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								rvResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								rvVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								rvDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("TV"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								tvSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								tvApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								tvRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								tvResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								tvVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								tvDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("DV"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								dvSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								dvApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								dvRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								dvResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								dvVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								dvDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("CV"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								cvSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								cvApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								cvRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								cvResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								cvVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								cvDispatchedCount = rs.getInt("appCount");
						}
						else if(rs.getString("Service_Short_Desc").equalsIgnoreCase("TOP"))
						{
							if(rs.getString("Status_Name").equalsIgnoreCase("SUBMITTED"))
								topSubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("APPROVED"))
								topApprovedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMIT"))
								topRejectedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("RESUBMITTED"))
								topResubmittedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("VERIFIED"))
								topVerifiedCount = rs.getInt("appCount");
							if(rs.getString("Status_Name").equalsIgnoreCase("DISPATCHED"))
								topDispatchedCount = rs.getInt("appCount");
						}
					}
					int rowCount = 0;
					int total = illSubmittedCount+illApprovedCount+illRejectedCount+illResubmittedCount+illVerifiedCount+illDispatchedCount+
								rllSubmittedCount+rllApprovedCount+rllRejectedCount+rllResubmittedCount+rllVerifiedCount+rllDispatchedCount+
								dllSubmittedCount+dllApprovedCount+dllRejectedCount+dllResubmittedCount+dllVerifiedCount+dllDispatchedCount+
								indlSubmittedCount+indlApprovedCount+indlRejectedCount+indlResubmittedCount+indlVerifiedCount+indlDispatchedCount+
								icdlSubmittedCount+icdlApprovedCount+icdlRejectedCount+icdlResubmittedCount+icdlVerifiedCount+icdlDispatchedCount+
								rdlSubmittedCount+rdlApprovedCount+rdlRejectedCount+rdlResubmittedCount+rdlVerifiedCount+rdlDispatchedCount+
								ddlSubmittedCount+ddlApprovedCount+ddlRejectedCount+ddlResubmittedCount+ddlVerifiedCount+ddlDispatchedCount+
								edlSubmittedCount+edlApprovedCount+edlRejectedCount+edlResubmittedCount+edlVerifiedCount+edlDispatchedCount+
								regvSubmittedCount+regvApprovedCount+regvRejectedCount+regvResubmittedCount+regvVerifiedCount+regvDispatchedCount+
								rvSubmittedCount+rvApprovedCount+rvRejectedCount+rvResubmittedCount+rvVerifiedCount+rvDispatchedCount+
								tvSubmittedCount+tvApprovedCount+tvRejectedCount+tvResubmittedCount+tvVerifiedCount+tvDispatchedCount+
								dvSubmittedCount+dvApprovedCount+dvRejectedCount+dvResubmittedCount+dvVerifiedCount+dvDispatchedCount+
								cvSubmittedCount+cvApprovedCount+cvRejectedCount+cvResubmittedCount+cvVerifiedCount+cvDispatchedCount+
								topSubmittedCount+topApprovedCount+topRejectedCount+topResubmittedCount+topVerifiedCount+topDispatchedCount;
					
					if(rowCount >= 1)
					{
						sbf.append("<tr>");
							sbf.append("<td align=\"left\">");
								sbf.append(userList.get(i).getEmployeeName());
							sbf.append("</td>");
							sbf.append("<td align=\"left\">");
								sbf.append(userList.get(i).getRegionName());
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illDispatchedCount);
							sbf.append("</td>");

							sbf.append("<td align=\"center\">");
								sbf.append(rllSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(dllSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(indlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(icdlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(rdlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(ddlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(edlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(regvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(rvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(tvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(dvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(cvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(topSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(total);
							sbf.append("</td>");
						sbf.append("</tr>");
						
						rowCount++;
						
						illSubmittedCount = 0;
						illApprovedCount = 0;
						illRejectedCount = 0;
						illResubmittedCount = 0;
						illVerifiedCount = 0;
						illDispatchedCount = 0;
						rllSubmittedCount = 0;
						rllApprovedCount = 0;
						rllRejectedCount = 0;
						rllResubmittedCount = 0;
						rllVerifiedCount = 0;
						rllDispatchedCount = 0;
						dllSubmittedCount = 0;
						dllApprovedCount = 0;
						dllRejectedCount = 0;
						dllResubmittedCount = 0;
						dllVerifiedCount = 0;
						dllDispatchedCount = 0;
						indlSubmittedCount = 0;
						indlApprovedCount = 0;
						indlRejectedCount = 0;
						indlResubmittedCount = 0;
						indlVerifiedCount = 0;
						indlDispatchedCount = 0;
						icdlSubmittedCount = 0;
						icdlApprovedCount = 0;
						icdlRejectedCount = 0;
						icdlResubmittedCount = 0;
						icdlVerifiedCount = 0;
						icdlDispatchedCount = 0;
						rdlSubmittedCount = 0;
						rdlApprovedCount = 0;
						rdlRejectedCount = 0;
						rdlResubmittedCount = 0;
						rdlVerifiedCount = 0;
						rdlDispatchedCount = 0;
						ddlSubmittedCount = 0;
						ddlApprovedCount = 0;
						ddlRejectedCount = 0;
						ddlResubmittedCount = 0;
						ddlVerifiedCount = 0;
						ddlDispatchedCount = 0;
						edlSubmittedCount = 0;
						edlApprovedCount = 0;
						edlRejectedCount = 0;
						edlResubmittedCount = 0;
						edlVerifiedCount = 0;
						edlDispatchedCount = 0;
						regvSubmittedCount = 0;
						regvApprovedCount = 0;
						regvRejectedCount = 0;
						regvResubmittedCount = 0;
						regvVerifiedCount = 0;
						regvDispatchedCount = 0;
						rvSubmittedCount = 0;
						rvApprovedCount = 0;
						rvRejectedCount = 0;
						rvResubmittedCount = 0;
						rvVerifiedCount = 0;
						rvDispatchedCount = 0;
						tvSubmittedCount = 0;
						tvApprovedCount = 0;
						tvRejectedCount = 0;
						tvResubmittedCount = 0;
						tvVerifiedCount = 0;
						tvDispatchedCount = 0;
						dvSubmittedCount = 0;
						dvApprovedCount = 0;
						dvRejectedCount = 0;
						dvResubmittedCount = 0;
						dvVerifiedCount = 0;
						dvDispatchedCount = 0;
						cvSubmittedCount = 0;
						cvApprovedCount = 0;
						cvRejectedCount = 0;
						cvResubmittedCount = 0;
						cvVerifiedCount = 0;
						cvDispatchedCount = 0;
						topSubmittedCount = 0;
						topApprovedCount = 0;
						topRejectedCount = 0;
						topResubmittedCount = 0;
						topVerifiedCount = 0;
						topDispatchedCount = 0;
					}
					else
					{
						sbf.append("<tr>");
							sbf.append("<td align=\"left\">");
								sbf.append(userList.get(i).getEmployeeName());
							sbf.append("</td>");
							sbf.append("<td align=\"left\">");
								sbf.append(userList.get(i).getRegionName());
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(illDispatchedCount);
							sbf.append("</td>");
	
							sbf.append("<td align=\"center\">");
								sbf.append(rllSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rllDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(dllSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dllDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(indlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(indlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(icdlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(icdlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(rdlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rdlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(ddlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(ddlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(edlSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(edlDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(regvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(regvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(rvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(rvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(tvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(tvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(dvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(dvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(cvSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(cvDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(topSubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topApprovedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topRejectedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topResubmittedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topVerifiedCount);
							sbf.append("</td>");
							sbf.append("<td align=\"center\">");
								sbf.append(topDispatchedCount);
							sbf.append("</td>");
							
							sbf.append("<td align=\"center\">");
								sbf.append(total);
							sbf.append("</td>");
						sbf.append("</tr>");
					
						rowCount++;
						
						illSubmittedCount = 0;
						illApprovedCount = 0;
						illRejectedCount = 0;
						illResubmittedCount = 0;
						illVerifiedCount = 0;
						illDispatchedCount = 0;
						rllSubmittedCount = 0;
						rllApprovedCount = 0;
						rllRejectedCount = 0;
						rllResubmittedCount = 0;
						rllVerifiedCount = 0;
						rllDispatchedCount = 0;
						dllSubmittedCount = 0;
						dllApprovedCount = 0;
						dllRejectedCount = 0;
						dllResubmittedCount = 0;
						dllVerifiedCount = 0;
						dllDispatchedCount = 0;
						indlSubmittedCount = 0;
						indlApprovedCount = 0;
						indlRejectedCount = 0;
						indlResubmittedCount = 0;
						indlVerifiedCount = 0;
						indlDispatchedCount = 0;
						icdlSubmittedCount = 0;
						icdlApprovedCount = 0;
						icdlRejectedCount = 0;
						icdlResubmittedCount = 0;
						icdlVerifiedCount = 0;
						icdlDispatchedCount = 0;
						rdlSubmittedCount = 0;
						rdlApprovedCount = 0;
						rdlRejectedCount = 0;
						rdlResubmittedCount = 0;
						rdlVerifiedCount = 0;
						rdlDispatchedCount = 0;
						ddlSubmittedCount = 0;
						ddlApprovedCount = 0;
						ddlRejectedCount = 0;
						ddlResubmittedCount = 0;
						ddlVerifiedCount = 0;
						ddlDispatchedCount = 0;
						edlSubmittedCount = 0;
						edlApprovedCount = 0;
						edlRejectedCount = 0;
						edlResubmittedCount = 0;
						edlVerifiedCount = 0;
						edlDispatchedCount = 0;
						regvSubmittedCount = 0;
						regvApprovedCount = 0;
						regvRejectedCount = 0;
						regvResubmittedCount = 0;
						regvVerifiedCount = 0;
						regvDispatchedCount = 0;
						rvSubmittedCount = 0;
						rvApprovedCount = 0;
						rvRejectedCount = 0;
						rvResubmittedCount = 0;
						rvVerifiedCount = 0;
						rvDispatchedCount = 0;
						tvSubmittedCount = 0;
						tvApprovedCount = 0;
						tvRejectedCount = 0;
						tvResubmittedCount = 0;
						tvVerifiedCount = 0;
						tvDispatchedCount = 0;
						dvSubmittedCount = 0;
						dvApprovedCount = 0;
						dvRejectedCount = 0;
						dvResubmittedCount = 0;
						dvVerifiedCount = 0;
						dvDispatchedCount = 0;
						cvSubmittedCount = 0;
						cvApprovedCount = 0;
						cvRejectedCount = 0;
						cvResubmittedCount = 0;
						cvVerifiedCount = 0;
						cvDispatchedCount = 0;
						topSubmittedCount = 0;
						topApprovedCount = 0;
						topRejectedCount = 0;
						topResubmittedCount = 0;
						topVerifiedCount = 0;
						topDispatchedCount = 0;
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return sbf.toString();
	}
	
	public List<DriveTypeDTO> get_customer_drive_type(String conditionValue,String driveTypeApplicationTable,String whereClause)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DriveTypeDTO> drivetypeList = new ArrayList<DriveTypeDTO>();
		DriveTypeDTO dto;
		try {
			conn = ConnectionManager.getConnection();
		
			if (conn != null) {
				int count	=	0;
				String query	=	SELECT_CLAUSE_FOR_DRIVE_TYPE+driveTypeApplicationTable+WHERE_CLAUSE_FOR_DRIVE_TYPE+whereClause;
				pst = conn.prepareStatement(query);
				pst.setString(1, conditionValue); 
				rs = pst.executeQuery();
				while (rs.next()) {
					String seprator	=	"";
					dto = new DriveTypeDTO();
					dto.setDriveTypeId(rs.getString("Drive_Type_Id"));
					if(count>0)
					{
						seprator=", ";
					}
					dto.setDriveType(seprator+rs.getString("Drive_Type_Name"));
					drivetypeList.add(dto);
					count++;
				}
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getdrivetype_licenselist]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return drivetypeList;
	}
	
	public List<DriveTypeDTO> get_customer_drive_type1(String conditionValue)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DriveTypeDTO> drivetypeList = new ArrayList<DriveTypeDTO>();
		DriveTypeDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
		
			if (conn != null)
			{
				String query = "SELECT a.Drive_Type_Id,a.Is_TCB_Endorsement FROM t_driving_license_application a WHERE a.`Application_Number`=?";
				pst = conn.prepareStatement(query);
				pst.setString(1, conditionValue); 
				rs = pst.executeQuery();
				rs.first();
				
				String driveType = rs.getString("Drive_Type_Id");
				
				if(rs.getString("Is_TCB_Endorsement").equals("Y"))
				{
					int count = 0;
					String separator = "";
					String[] driveTypeList = driveType.split("~");
					for(int i=0;i<driveTypeList.length;i++)
					{
						dto = new DriveTypeDTO();
						
						query = "SELECT `Drive_Type_Name` FROM t_drive_type_master a WHERE a.`Drive_Type_Id`=?";
						pst = conn.prepareStatement(query);
						pst.setString(1, driveTypeList[i]); 
						rs = pst.executeQuery();
						rs.first();
						
						if(count>0)
						{
							separator=", ";
						}
						
						dto.setDriveType(separator+rs.getString("Drive_Type_Name"));
						drivetypeList.add(dto);
						count++;
					}
				}
				else
				{
					query = "SELECT `Drive_Type_Name` FROM t_drive_type_master a WHERE a.`Drive_Type_Id`=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, driveType); 
					rs = pst.executeQuery();
					while(rs.next())
					{
						dto = new DriveTypeDTO();
						dto.setDriveType(rs.getString("Drive_Type_Name"));
						drivetypeList.add(dto);
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[get_customer_drive_type1]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return drivetypeList;
	}
	
	public List<LicenseDTO> get_offence_list(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		String offenceId	=	dto.getOffenceId();
		
		List<LicenseDTO> licenseList = new ArrayList<LicenseDTO>();
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				query = GET_OFFENCE_LIST_AGAINST_OFFENCE_ID;
				pst = conn.prepareStatement(query);
				pst.setString(1, dto.getOffenceId());
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					dto.setOffenceName(rs.getString("Offence_Name"));
					dto.setAmount(rs.getString("Amount"));
					dto.setOffenceId(offenceId);
					licenseList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException(
					"###Error at CommonDAO[getLearnerLicenseInfoList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseList;
	}


	public List<LicenseDTO> searchCustomerOffence(LicenseDTO dto,String offenceType)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String where = "";
		String query = "";
		List<LicenseDTO> licenseList = new ArrayList<LicenseDTO>();
		try {
			int count = 1;
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				// CHECKING NON EMPTY VARIABLE
				if(offenceType.equalsIgnoreCase("BHUTANESE"))
				{
					if (!dto.getLearnerLicenseId().equalsIgnoreCase("NA")) {
						where = where+" AND a.`Learner_License_Id`=?";
						//where = where+" AND a.`Learner_License_Id` IN (SELECT e.`Learner_License_Info_Id` FROM `t_learner_license_dtls` e WHERE e.`Learner_License_No`=?) ";
					}
					if (!dto.getDrivinglicenseId().equalsIgnoreCase("NA")) {
						//where = where+" AND a.`License_Id` IN (SELECT d.`Driving_License_Id` FROM `t_driving_license_dtls` d WHERE d.`Driving_License_No`=?) ";
						where = where+" AND a.`License_Id`=?";
					}
					if (!dto.getVehicleId().equalsIgnoreCase("NA")) {
						where = where+" AND a.`Vehicle_Id`=? ";
						//where = where+" AND a.`Vehicle_Id` IN (SELECT c.`Vehicle_Reg_Dtls_Id` FROM `t_vehicle_registration_dtls` c WHERE c.`Vehicle_Number`=?) ";
					}
				}
				else if(offenceType.equalsIgnoreCase("FOREIGN"))
				{
					
					if (!dto.getDrivinglicenseId().equalsIgnoreCase("NA")) {
						where = where+" AND a.`License_Number`=?";
					}
					if (!dto.getVehicleId().equalsIgnoreCase("NA")) {
						where = where+" AND a.`Vehicle_Number`=? ";
					}
				}
				else if(offenceType.equalsIgnoreCase("NON_DOCUMENT"))
				{
					
					if (!dto.getDrivinglicenseId().equalsIgnoreCase("NA")) {
						where = where+" AND a.`Customer_Id`=? AND a.`Is_Personal_New`='Y'";
					}
				}
				
				query = GET_OFFENCE_LIST+where+" GROUP BY a.`TIN_No`";
				pst = conn.prepareStatement(query);
				// CLAUSE VALUE
				if (!dto.getLearnerLicenseId().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getLearnerLicenseId());
					count = count + 1;
				}
				if (!dto.getDrivinglicenseId().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getDrivinglicenseId());
					count = count + 1;
				}
				if (!dto.getVehicleId().equalsIgnoreCase("NA")) {
					pst.setString(count, dto.getVehicleId());
					count = count + 1;
				}
				rs = pst.executeQuery();
				while (rs.next()) {
					dto = new LicenseDTO();
					if(null!=rs.getString("Is_paid"))
						if(rs.getString("Is_paid").equalsIgnoreCase("Y"))
							dto.setIsPaid("Paid");
						else
							dto.setIsPaid("Not Paid");
					dto.setOffenceId(rs.getString("Offence_Id"));
					dto.setTINno(rs.getString("TIN_No"));
					dto.setOffencedate(rs.getString("offenceDate"));
					
					if(null != rs.getString("foriegnVehicleNumber"))
						dto.setVehicleNo(rs.getString("foriegnVehicleNumber"));
					else
					  dto.setVehicleNo(rs.getString("Vehicle_Number"));
					if(null != rs.getString("Driving_License_No"))
						dto.setLicenseNo(rs.getString("Driving_License_No"));
					else
					   dto.setLicenseNo(rs.getString("License_Number"));
					dto.setLearnerlicenseno(rs.getString("Learner_License_No"));
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					licenseList.add(dto);
				}

			}
		} catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getLearnerLicenseInfoList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return licenseList;
	}
	
	public List<PrintDTO> print_task(String  jurisId, String  jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PrintDTO> printList = new ArrayList<PrintDTO>();
		PrintDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PRINT_TASK_LIST);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				pst.setString(3, "N");
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new PrintDTO();
					dto.setApplicationNo(rs.getString("Application_Number"));
					dto.setApprovedDate(rs.getString("approvedDate"));
					dto.setIdentityId(rs.getString("Identity_Id"));
					dto.setIdentityNo(rs.getString("Identity_Number"));
					dto.setServiceName(rs.getString("serviceName"));
					dto.setDocumentType(rs.getString("Document_Type"));
					dto.setDocumentId(rs.getString("Document_Id"));
					printList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return printList;
	}
	
	public String checkIfDuplicateApplication(DuplicateDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "N";
		
		try
		{
			if(dto.getRequestType().equalsIgnoreCase("VEHICLE"))
			{
				pst = conn.prepareStatement(CHECK_IF_DUPLICATE_APP_VEHICLE);
				pst.setString(1, dto.getVehicleId());
				pst.setString(2, dto.getServiceType());
				pst.setString(3, "DISPATCHED");
				rs = pst.executeQuery();
				rs.first();
				
				int rowCount = rs.getInt("rowCount");
				
				
				if(rowCount > 0)
					result = "Y"+"#"+rs.getString("Receipt_Number")+"#"+rs.getString("Application_Number");
			}
			else if(dto.getRequestType().equalsIgnoreCase("LICENSE"))
			{
				pst = conn.prepareStatement(CHECK_IF_DUPLICATE_APP_LICENSE);
				pst.setString(1, dto.getLicenseId());
				pst.setString(2, dto.getServiceType());
				pst.setString(3, "DISPATCHED");
				rs = pst.executeQuery();
				rs.first();
				
				int rowCount = rs.getInt("rowCount");
				
				if(rowCount > 0)
					result = "Y"+"#"+rs.getString("Receipt_Number")+"#"+rs.getString("Application_Number");
				
			}
			else if(dto.getRequestType().equalsIgnoreCase("LEARNER"))
			{
				pst = conn.prepareStatement(CHECK_IF_DUPLICATE_APP_LEARNER);
				pst.setString(1, dto.getLearnerId());
				pst.setString(2, dto.getServiceType());
				pst.setString(3, "DISPATCHED");
				rs = pst.executeQuery();
				rs.first();
				
				int rowCount = rs.getInt("rowCount");
				
				if(rowCount > 0)
					result = "Y"+"#"+rs.getString("Receipt_No")+"#"+rs.getString("Application_Number");
			}
			else if(dto.getRequestType().equalsIgnoreCase("TOP"))
			{
				
			}
		}
		catch (Exception e)
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	public String checkIfExists(String licenseNo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String status = "DOESNOT_EXISTS";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(CHECK_IF_EXISTS);
				pst.setString(1, licenseNo);
				rs = pst.executeQuery();
				rs.first();
				
				int rowCount = rs.getInt("rowCount");
				
				if(rowCount > 0)
					status = "EXISTS";
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return status;
	}
	
	public List<ApplicationStatusTrailDTO> getApplicationStatus(String requestType, String serviceType, String identityNo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ApplicationStatusTrailDTO> applStatusList = new ArrayList<ApplicationStatusTrailDTO>();
		ApplicationStatusTrailDTO dto;
		
		try 
		{
			
		} 
		catch (Exception e)
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return applStatusList;
	}
	public String getCustomerDtls(String cid)throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String customerId = null;
		Connection conn = null;

		try {
			
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
				pst = conn.prepareStatement("SELECT COUNT(*) rowCount,a.`Customer_Id` FROM `t_personal_dtls` a WHERE a.`CID_Number`=?");
				pst.setString(1, cid);
				rs = pst.executeQuery();
				rs.first();
				customerId = rs.getString("Customer_Id");
			}
			
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		return customerId;
	}
	
	public String validateOTP(String cid, String enteredOTP) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String status = "NO_MATCH";
		
		try 
		{
			String sql = "SELECT "
				+ "  COUNT(*) rowCount, "
				+ "  a.`Creation_Time`,NOW() as currentDate "
				+ "FROM "
				+ "  t_otp_master a "
				+ "WHERE a.`CID_Number` = ? "
				+ "  AND a.`OTP` = ?";
			
			String sql1 = "DELETE FROM t_otp_master WHERE CID_Number=? AND OTP=?";
			
			conn = ConnectionManager.getConnection();
			
			if(conn != null){
				pst = conn.prepareStatement(sql);
				pst.setString(1, cid);
				pst.setString(2, enteredOTP);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount") > 0){
					
					Timestamp d1 = rs.getTimestamp("Creation_Time");
					Timestamp d2 = rs.getTimestamp("currentDate");
					DateTime date1 = new DateTime(d1);
					DateTime date2 = new DateTime(d2);
					int diffMinutes = Minutes.minutesBetween(date2, date1).getMinutes();
					
					if(diffMinutes < 0)
						status = "OTP_EXPIRED";
					else
						status = "MATCH";
					
					pst = conn.prepareStatement(sql1);
					pst.setString(1, cid);
					pst.setString(2, enteredOTP);
					pst.executeUpdate();
				}
			}
		} 
		catch (Exception e) 
		{
			status = "NO_MATCH";
		}
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return status;
	}
	
	/*
	 * 
	 * Queries :: START
	 */
	
	private static final String GET_LICENSE_ALERT_COUNT = "SELECT "
		+ "  count(*) as rowCount "
		+ "FROM "
		+ "  t_print_dtls a "
		+ "  LEFT JOIN t_driving_license_application b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE a.`Document_Type` = 'DL' "
		+ "  AND IF( "
		+ "    DATEDIFF(CURRENT_DATE, b.`Receipt_Date`) > 3, "
		+ "    1 = 1, "
		+ "    0 "
		+ "  ) "
		+ "  AND a.`isPrinted` = 'N'";
	
	private static final String GET_APPLICATION_ALERT_COUNT = "SELECT COUNT(*) rowCount FROM t_workflow_dtls a WHERE a.`Status_Id` = 1";
	
	private static final String GET_LICENSE_ALERT_DETAILS = "SELECT "
		+ "  a.`Application_Number`, "
		+ "  (SELECT "
		+ "    Service_Name "
		+ "  FROM "
		+ "    t_service_master "
		+ "  WHERE `Service_Id` = a.`Service_Id`) serviceName, "
		+ "  IF( "
		+ "    a.`Jurisdiction_Type_Id` = 1, "
		+ "    (SELECT "
		+ "      `region_name` "
		+ "    FROM "
		+ "      `t_region_master` "
		+ "    WHERE `Region_Id` = a.`Jurisdiction_Id`), "
		+ "    (SELECT "
		+ "      `base_office_name` "
		+ "    FROM "
		+ "      `t_base_office_master` "
		+ "    WHERE `Base_Office_Id` = a.`Jurisdiction_Id`) "
		+ "  ) processedFrom, "
		+ "  a.`Identity_Number` AS licenseNumber, "
		+ "  DATE_FORMAT(b.`Receipt_Date`, '%d-%m-%Y') receiptDate, "
		+ "  CONCAT( "
		+ "    DATEDIFF(CURRENT_DATE, b.`Receipt_Date`), "
		+ "    ' ', "
		+ "    'days' "
		+ "  ) AS dayDifference "
		+ "FROM "
		+ "  t_print_dtls a "
		+ "  LEFT JOIN t_driving_license_application b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE a.`Document_Type` = 'DL' "
		+ "  AND IF( "
		+ "    DATEDIFF(CURRENT_DATE, b.`Receipt_Date`) > 3, "
		+ "    1 = 1, "
		+ "    0 "
		+ "  ) "
		+ "  AND a.`isPrinted` = 'N' "
		+ "ORDER BY a.`Jurisdiction_Id`,a.`Service_Id`";
	
	private static final String GET_APPLICATION_ALERT_LIST = "SELECT "
		+ "  a.`Application_Number`, "
		+ "  (SELECT "
		+ "    `Service_Name` "
		+ "  FROM "
		+ "    t_service_master "
		+ "  WHERE `Service_Id` = a.`Service_Id`) service, "
		+ "  DATE_FORMAT(a.`Action_Date`, '%d-%m-%Y') submittedOn, "
		+ "  (SELECT "
		+ "    `Status_Name` "
		+ "  FROM "
		+ "    `t_status_master` "
		+ "  WHERE `Status_Id` = a.`Status_Id`) currentStatus, "
		+ "  IF( "
		+ "    a.`Juris_Type_Id` = 1, "
		+ "    (SELECT "
		+ "      `region_name` "
		+ "    FROM "
		+ "      `t_region_master` "
		+ "    WHERE `region_id` = a.`Juris_Id`), "
		+ "    (SELECT "
		+ "      `base_office_name` "
		+ "    FROM "
		+ "      `t_base_office_master` "
		+ "    WHERE `base_office_id` = a.`Juris_Id`) "
		+ "  ) processedFrom, "
		+ "  CONCAT( "
		+ "    DATEDIFF(CURRENT_DATE, a.`Action_Date`), "
		+ "    ' ', "
		+ "    'days' "
		+ "  ) AS dayDifference "
		+ "FROM "
		+ "  t_workflow_dtls a "
		+ "WHERE a.`Status_Id` = 1 "
		+ "ORDER BY DATEDIFF(CURRENT_DATE, a.`Action_Date`) DESC";
	
	private static final String CHECK_IF_EXISTS = "SELECT COUNT(a.`Driving_License_Id`) rowCount FROM t_driving_license_dtls a WHERE a.`Driving_License_No`=?";
	
	private static final String CHECK_IF_DUPLICATE_APP_VEHICLE = "SELECT "
		+ "  COUNT(a.`Application_Number`) rowCount ,a.`Receipt_Number` ,a.`Application_Number` "
		+ "FROM "
		+ "  t_vehicle_application a "
		+ "  LEFT JOIN t_workflow_dtls b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE a.`Vehicle_Id` = ? "
		+ "  AND a.`Application_Type` = ? "
		+ "  AND b.`Status_Id` NOT IN "
		+ "  (SELECT "
		+ "    Status_Id "
		+ "  FROM "
		+ "    t_status_master "
		+ "  WHERE Status_Type_Short_Desc = ?)";
	
	private static final String CHECK_IF_DUPLICATE_APP_LEARNER = "SELECT "
		+ "  COUNT(a.`Application_Number`) rowCount ,a.`Receipt_No`,a.`Application_Number` "
		+ "FROM "
		+ "  `t_learner_license_application` a "
		+ "  LEFT JOIN t_workflow_dtls b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE a.`Learner_License_Id` = ? "
		+ "  AND a.`Application_Type` = ? "
		+ "  AND b.`Status_Id` NOT IN "
		+ "  (SELECT "
		+ "    Status_Id "
		+ "  FROM "
		+ "    t_status_master "
		+ "  WHERE Status_Type_Short_Desc = ?)";
	
	private static final String  CHECK_IF_DUPLICATE_APP_LICENSE = "SELECT "
		+ "  COUNT(a.`Application_Number`) rowCount ,a.`Receipt_Number`,a.`Application_Number` "
		+ "FROM "
		+ "  `t_driving_license_application` a "
		+ "  LEFT JOIN t_workflow_dtls b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE a.`License_Id` = ? "
		+ "  AND a.`Application_Type` = ? "
		+ "  AND b.`Status_Id` NOT IN "
		+ "  (SELECT "
		+ "    Status_Id "
		+ "  FROM "
		+ "    t_status_master "
		+ "  WHERE Status_Type_Short_Desc = ?)";
	
	private static final String GET_PRINT_TASK_LIST = "SELECT "
		+ "  a.`Application_Number`, "
		+ "  (SELECT "
		+ "    Service_Name "
		+ "  FROM "
		+ "    t_service_master "
		+ "  WHERE Service_Id = a.`Service_Id`) serviceName, "
		+ "  a.`Identity_Number`, "
		+ "  a.`Identity_Id`, "
		+ "  DATE_FORMAT(a.`Created_On`, '%d/%m/%Y') approvedDate, "
		+ "  a.`Document_Type`,a.`Document_Id` "
		+ "FROM "
		+ "  `t_print_dtls` a "
		+ "WHERE a.`Jurisdiction_Id` = ? "
		+ "  AND a.`Jurisdiction_Type_Id` = ? "
		+ "  AND a.`isPrinted` = ? " 
		+ "AND a.Document_Type='DL'  and a.`Service_Id` not in ('101','102','103') "
		+ " ORDER BY a.`Created_On` DESC";
	
	private static final String GET_DAILY_TASK_REPORT = "SELECT   actor_name, "
		+ "(SELECT     NAME   FROM     t_user_master   WHERE login_id = t_workflow_dtls.actor_name) userName, "
		+ "(SELECT     status_name   FROM     t_status_master   WHERE status_id = t_workflow_dtls.status_id) Status_Name, "
		+ "(SELECT     service_short_desc   FROM     t_service_master   WHERE service_id = t_workflow_dtls.service_id) Service_Short_Desc, "
		+ " COUNT(status_id) appCount "
		+ "FROM   t_workflow_dtls WHERE actor_name = ?   AND action_date BETWEEN ?   AND ? "
		+ " GROUP BY status_id,   service_id "
		+ " UNION "
		+ " SELECT   a.actor_name, "
		+ "(SELECT     NAME   FROM     t_user_master   WHERE login_id = a.actor_name) userName, "
		+ "(SELECT     status_name   FROM     t_status_master   WHERE status_id = a.status_id) Status_Name, "
		+ "(SELECT     service_short_desc   FROM     t_service_master   WHERE service_id = a.service_id) Service_Short_Desc, "
		+ " COUNT(a.status_id) appCount "
		+ "FROM   t_workflow_dtls_audit a WHERE actor_name = ?   AND action_date BETWEEN ? AND ? "
		+ " GROUP BY status_id,   service_id";
		
		
		/*"SELECT "
														+ "  actor_name, "
														+ "  (SELECT "
														+ "    NAME "
														+ "  FROM "
														+ "    t_user_master "
														+ "  WHERE login_id = t_workflow_dtls.actor_name) userName, "
														+ "  (SELECT "
														+ "    status_name "
														+ "  FROM "
														+ "    t_status_master "
														+ "  WHERE status_id = t_workflow_dtls.status_id) Status_Name, "
														+ "  (SELECT "
														+ "    service_short_desc "
														+ "  FROM "
														+ "    t_service_master "
														+ "  WHERE service_id = t_workflow_dtls.service_id) Service_Short_Desc, "
														+ "  COUNT(status_id) appCount "
														+ "FROM "
														+ "  t_workflow_dtls "
														+ "WHERE actor_name = ? "
														+ "  AND action_date BETWEEN ? "
														+ "  AND ? "
														+ "GROUP BY status_id, "
														+ "  service_id";*/
	
	private static final String GET_DAILY_TASK_REPORT_INITIAL =  "SELECT   actor_name, "
		+ "(SELECT     NAME   FROM     t_user_master   WHERE login_id = t_workflow_dtls.actor_name) userName, "
		+ "(SELECT     status_name   FROM     t_status_master   WHERE status_id = t_workflow_dtls.status_id) Status_Name, "
		+ "(SELECT     service_short_desc   FROM     t_service_master   WHERE service_id = t_workflow_dtls.service_id) Service_Short_Desc, "
		+ "  COUNT(status_id) appCount "
		+ "  FROM   t_workflow_dtls WHERE actor_name = ?   AND action_date LIKE ? GROUP BY status_id,   service_id "
		+ "  UNION "
		+ "  SELECT   a.actor_name, "
		+ "(SELECT     NAME   FROM     t_user_master   WHERE login_id = a.actor_name) userName, "
		+ "(SELECT     status_name   FROM     t_status_master   WHERE status_id = a.status_id) Status_Name, "
		+ "(SELECT     service_short_desc   FROM     t_service_master   WHERE service_id = a.service_id) Service_Short_Desc, "
		+ "  COUNT(a.status_id) appCount "
		+ "  FROM   t_workflow_dtls_audit a WHERE a.Actor_Name = ?   AND a.action_date LIKE ? GROUP BY a.status_id,   a.service_id;";

		
		
		
		/*"SELECT "
																+ "  actor_name, "
																+ "  (SELECT "
																+ "    NAME "
																+ "  FROM "
																+ "    t_user_master "
																+ "  WHERE login_id = t_workflow_dtls.actor_name) userName, "
																+ "  (SELECT "
																+ "    status_name "
																+ "  FROM "
																+ "    t_status_master "
																+ "  WHERE status_id = t_workflow_dtls.status_id) Status_Name, "
																+ "  (SELECT "
																+ "    service_short_desc "
																+ "  FROM "
																+ "    t_service_master "
																+ "  WHERE service_id = t_workflow_dtls.service_id) Service_Short_Desc, "
																+ "  COUNT(status_id) appCount "
																+ "FROM "
																+ "  t_workflow_dtls "
																+ "WHERE actor_name = ? "
																+ "  AND action_date LIKE ? "
																+ "GROUP BY status_id, "
																+ "  service_id";*/
	
	private static final String GET_USER_LIST = "SELECT "
		+ "  (SELECT "
		+ "    NAME "
		+ "  FROM "
		+ "    t_user_master "
		+ "  WHERE login_id = a.`login_id`) uname, "
		+ "  (SELECT "
		+ "    region_name "
		+ "  FROM "
		+ "    t_region_master "
		+ "  WHERE region_id = a.`region_id`) region, "
		+ "  a.`login_id` "
		+ "FROM "
		+ "  t_user_jurisdiction_mapping a "
		+ "WHERE a.`jurisdiction_id` = ? "
		+ "  AND a.`juris_type_id` = ? "
		+ "  AND a.`login_id` <> 'admin'";
	
	private static final String GET_PERSONAL_CUSTOMER_INFORMATION = "SELECT "
		+ "  COUNT(b.`First_Name`), "
		+ "  COUNT(b.`First_Name`), "
		+ "  CONCAT( "
		+ "    b.`First_Name`, "
		+ "    ' ', "
		+ "    b.`Middle_Name`, "
		+ "    ' ', "
		+ "    b.`Last_Name` "
		+ "  ) pname, "
		+ "  b.`Present_Email`, "
		+ "  b.`Present_Phone_No`, "
		+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
		+ "FROM "
		+ "  t_personal_dtls b "
		+ "WHERE b.`Customer_Id` = ?";


private static final String GET_ORGANISATION_CUSTOMER_INFORMATION = "SELECT "
	+ "  b.`Organization_Name`, "
	+ "  b.`Email_Id`, "
	+ "  b.`Phone_Number`, "
	+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
	+ "FROM "
	+ "  t_organization_info b "
	+ "WHERE b.`Customer_Id` = ?";

private static final String GET_TEST_LOCATION_LIST = "SELECT "
	+ "  IF( "
	+ "    a.`Juris_Type_Id` = '1', "
	+ "    (SELECT "
	+ "      region_name "
	+ "    FROM "
	+ "      t_region_master "
	+ "    WHERE region_id = a.`Test_Location_Id`), "
	+ "    (SELECT "
	+ "      base_office_name "
	+ "    FROM "
	+ "      t_base_office_master "
	+ "    WHERE Base_Office_Id = a.`Test_Location_Id`) "
	+ "  ) testLocation, "
	+ "  a.`Juris_Type_Id`, "
	+ "  a.`Test_Location_Id` "
	+ "FROM "
	+ "  t_etest_config_master a "
	+ "WHERE a.`Test_Date` > CURRENT_DATE "
	+ "GROUP BY a.`Juris_Type_Id`,a.`Test_Location_Id`";



private static final String GET_TEST_LOCATION_LIST_FOR_ADMIN = "SELECT "
	+ "  IF( "
	+ "    a.`Juris_Type_Id` = '1', "
	+ "    (SELECT "
	+ "      region_name "
	+ "    FROM "
	+ "      t_region_master "
	+ "    WHERE region_id = a.`Test_Location_Id`), "
	+ "    (SELECT "
	+ "      base_office_name "
	+ "    FROM "
	+ "      t_base_office_master "
	+ "    WHERE Base_Office_Id = a.`Test_Location_Id`) "
	+ "  ) testLocation, "
	+ "  a.`Juris_Type_Id`, "
	+ "  a.`Test_Location_Id` "
	+ "FROM "
	+ "  t_etest_config_master a "
	+ "GROUP BY a.`Juris_Type_Id`,a.`Test_Location_Id`";

	private static final String GET_TOP_SEARCH_LIST = "SELECT a.`Customer_Id`,"
						+ "  a.`TOP_Number`,d.`Driving_License_No`, "
						+ "  CONCAT( "
						+ "    c.`First_Name`, "
						+ "    ' ', "
						+ "    c.`Middle_Name`, "
						+ "    ' ', "
						+ "    c.`Last_Name` "
						+ "  ) pname, "
						+ "  (SELECT "
						+ "    `region_name` "
						+ "  FROM "
						+ "    `t_region_master` "
						+ "  WHERE `region_id` = a.`Region_Id`) region, "
						+ "  (SELECT "
						+ "    dzongkhag_name "
						+ "  FROM "
						+ "    t_dzongkhag_master "
						+ "  WHERE dzongkhag_id = a.`Dzongkhag_Id`) dzongkhag, "
						+ "  a.`Exact_Location` "
						+ "FROM "
						+ "  t_top_dtls a "
						+ "  LEFT JOIN t_vehicle_registration_dtls b "
						+ "    ON a.`Vehicle_Id` = b.`Vehicle_Reg_Dtls_Id` "
						+ "  LEFT JOIN t_personal_dtls c "
						+ "    ON a.`Customer_Id` = c.`Customer_Id` "
						+ "  LEFT JOIN `t_driving_license_dtls` d "
						+ "    ON a.`License_Id` = d.`Driving_License_Id` "
						+ " WHERE a.`Top_Issue_Id` > 0 AND "
						+ " (SELECT COUNT(*) FROM `t_top_dtls` WHERE `TOP_Number`=a.`TOP_Number`) " +
						" <> " +
						" (SELECT COUNT(*) FROM `t_top_cancellation_dtls` WHERE TOP_Number=a.`TOP_Number`) ";
	
	private static final String GET_TOP_SEARCH_LIST_FOR_HISTORY = "SELECT a.`Customer_Id`,"
				+ "  a.`TOP_Number`,d.`Driving_License_No`, "
				+ "  CONCAT( "
				+ "    c.`First_Name`, "
				+ "    ' ', "
				+ "    c.`Middle_Name`, "
				+ "    ' ', "
				+ "    c.`Last_Name` "
				+ "  ) pname, "
				+ "  (SELECT "
				+ "    `region_name` "
				+ "  FROM "
				+ "    `t_region_master` "
				+ "  WHERE `region_id` = a.`Region_Id`) region, "
				+ "  (SELECT "
				+ "    dzongkhag_name "
				+ "  FROM "
				+ "    t_dzongkhag_master "
				+ "  WHERE dzongkhag_id = a.`Dzongkhag_Id`) dzongkhag, "
				+ "  a.`Exact_Location` "
				+ "FROM "
				+ "  t_top_dtls a "
				+ "  LEFT JOIN t_vehicle_registration_dtls b "
				+ "    ON a.`Vehicle_Id` = b.`Vehicle_Reg_Dtls_Id` "
				+ "  LEFT JOIN t_personal_dtls c "
				+ "    ON a.`Customer_Id` = c.`Customer_Id` "
				+ "  LEFT JOIN `t_driving_license_dtls` d "
				+ "    ON a.`License_Id` = d.`Driving_License_Id` "
				+ " WHERE a.`Top_Issue_Id` > 0 ";

	private static final String INSERT_INTO_T_PAYMENT_DTLS = "INSERT INTO `t_payment_dtls` ( "
			+ "  `Application_Number`, "
			+ "  `Application_Type`, "
			+ "  `Service_Id`, "
			+ "  `Amount_Paid`, "
			+ "  `Penalty_Paid`, "
			+ "   Receipt_No, "
			+ "   Receipt_Date, "
			+ "  `Created_By` "
			+ ") " + "VALUES " + "  (?, ?, ?, ?, ?, ?, ?, ?)";
	
	
	private static final String UPDATE_PAYMENT_DTLS = "update t_payment_dtls set Amount_Paid=?,Penalty_Paid=? where Application_Number=?";

	private static final String CHECK_IF_CID_EXISTS = "SELECT COUNT(*) rowCount FROM t_personal_dtls a WHERE a.`CID_Number`=?";

	private static final String INSERT_INTO_T_ERROR_DETAILS = "INSERT INTO `t_error_log` "
			+ "            (`error_detail`, "
			+ "             `created_by`) "
			+ "VALUES (?, " + "        ?)";

	private static final String GET_FILE_PATH = "SELECT Document_Path FROM t_document_dtls WHERE UUID = ?";

	private static final String GET_LAST_LOGIN_DATE = "SELECT last_login_date FROM t_user_master WHERE login_id=?";

	private static String GET_STATUS_CODE = "SELECT STATUS_TYPE_SHORT_DESC FROM t_status_master WHERE STATUS_ID = (SELECT STATUS_ID FROM t_workflow_dtls WHERE APPLICATION_NUMBER = ?)";

	private static final String UPDATE_T_USER_MASTER_WITH_LOGIN_DATE = "UPDATE t_user_master a SET a.`last_login_date`= ? WHERE a.`login_id`=?";

	private static final String CHECK_IF_USER_EXISTS = "SELECT COUNT(*) rowCount FROM t_user_master WHERE login_id=? AND is_delete = 'N'";

	private static final String GET_ALL_ROLES_FOR_UID = "select `role_id`,`role_name`,`role_short_desc` from t_role_master where role_id in (select role_id from t_user_role_mapping where login_id = ?)";

	private static final String UPDATE_USER_EMAIL_ADDRESS = "UPDATE `t_user_master` a SET a.`email_id`=? WHERE a.`login_id`=?";

	private static final String UPDATE_USER_MOBILE_NUMBER = "UPDATE `t_user_master` a SET a.`mobile_number`=? WHERE a.`login_id`=?";

	private static final String GET_ALL_SERVICES_FOR_ROLE = "SELECT " + " `Service_Id`,`Service_Name`,`Service_Short_Desc` "
			+ "FROM " + "  t_service_master " + "WHERE Service_Id IN "
			+ "  (SELECT DISTINCT " + "    Service_Id " + "  FROM "
			+ "    t_privilege_master " + "  WHERE Priv_Id IN "
			+ "    (SELECT " + "      Priv_Id " + "    FROM "
			+ "      t_role_priv_mapping " + "    WHERE Role_Id = ?))";

	private static final String GET_ALL_PRIVILEGES_FOR_SERVICE = "SELECT `Priv_Id`,`Priv_Name`,`Priv_Short_Desc` FROM t_privilege_master WHERE Priv_Id IN (SELECT Priv_Id FROM t_role_priv_mapping WHERE Role_Id = ?) AND Service_Id = ?";

	public static final String GET_JURISDICTION_FOR_USERID = "SELECT "
		+ "  a.`region_id`, "
		+ "  (SELECT "
		+ "    region_name "
		+ "  FROM "
		+ "    t_region_master "
		+ "  WHERE region_id = a.`region_id`) regionName, "
		+ "  a.`jurisdiction_id`, "
		+ "  a.`juris_type_id`, "
		+ "  a.`base_office_id`, "
		+ "  (SELECT "
		+ "    `base_office_name` "
		+ "  FROM "
		+ "    `t_base_office_master` "
		+ "  WHERE `base_office_id` = a.`base_office_id`) baseName "
		+ "FROM "
		+ "  `t_user_jurisdiction_mapping` a "
		+ "WHERE a.`login_id` = ?";

	private static final String GET_USER_DETAILS = "SELECT " + "  `login_id`, "
			+ "  `Name`, " + "  `email_id`, " + "  `mobile_number`, "
			+ "  `is_active`, " + "  `last_login_date`, "
			+ "  `show_tasklist`, " + "   `agency_code` " + "FROM "
			+ "  `t_user_master` " + "WHERE `login_id` = ?";

	private static final String INSERT_INTO_T_PASSWORD_RETRIEVAL_DTLS = "INSERT INTO `t_password_retrieval_dtls` "
			+ "            (`login_id`, "
			+ "             `question_id`, "
			+ "             `answer`) "
			+ "VALUES (?, "
			+ "        ?, "
			+ "        ?)";

	// private static final String GET_MENU_LIST =
	// "SELECT menu_id,menu_name,menu_icon FROM t_menu_master";

	/*private static final String GET_MENU_LIST = "SELECT " + "  a.menu_id, "
			+ "  a.menu_name, " + "  a.menu_icon " + "FROM "
			+ "  t_menu_master a " + "  LEFT JOIN t_page_master b "
			+ "    ON a.`menu_id` = b.`menu_id` "
			+ "  LEFT JOIN t_role_page_mapping c "
			+ "    ON c.`page_id` = b.`page_id` "
			+ "    WHERE c.`is_disabled`<> ? " + "    AND c.`role_id`= ? "
			+ "    GROUP BY a.`menu_id`";*/
	
	private static final String GET_MENU_LIST = "SELECT "
		+ "  a.menu_id, "
		+ "  a.menu_name, "
		+ "  a.menu_icon "
		+ "FROM "
		+ "  t_menu_master a "
		+ "  LEFT JOIN t_page_master b "
		+ "    ON a.`menu_id` = b.`menu_id` "
		+ "  LEFT JOIN t_role_page_mapping c "
		+ "    ON c.`page_id` = b.`page_id` "
		+ "WHERE c.`role_id` = ? "
		+ "GROUP BY a.`menu_id`";

	private static final String GET_SUB_MENU_LIST = "SELECT page_id,page_name,page_link FROM t_page_master WHERE menu_id=? ORDER BY sequence ASC";

	/*
	 * private static final String GET_SUB_MENU_LIST = "SELECT " +
	 * "  a.page_id, " + "  a.page_name, " + "  a.page_link " + "FROM " +
	 * "  t_page_master a, " + "  t_role_page_mapping b " +
	 * "WHERE a.menu_id = ? " + "  AND b.`role_id` = ? " +
	 * "  AND a.`page_id` = b.`page_id` " + "  AND b.`is_disabled` <> ? " +
	 * "GROUP BY a.`page_id`";
	 */

	private static final String CHECK_IF_MENU_DISABLED = "SELECT COUNT(*) rowCount FROM t_role_page_mapping a WHERE a.`page_id`=? AND a.`role_id`=? AND a.`is_disabled`='N'";
	
	private static final String CHECK_IF_PAGE_DISABLED = "SELECT "
															+ "  COUNT(*) rowCount "
															+ "FROM "
															+ "  t_role_page_mapping a "
															+ "  LEFT JOIN t_page_master b "
															+ "    ON a.`page_id` = b.`page_id` "
															+ "WHERE b.`parent_page_id` = ? "
															+ "  AND a.`role_id` = ? "
															+ "  AND a.`is_disabled` = 'N'";

	private static final String GET_THIRD_LVL_MENU_LIST = "SELECT "
			+ "  b.`page_id`, " + "  b.`page_name`, " + "  b.`page_link` "
			+ "FROM " + "  t_role_page_mapping a "
			+ "  LEFT JOIN t_page_master b "
			+ "    ON a.`page_id` = b.`page_id` " + "WHERE a.`role_id` = ? "
			+ "  AND b.`parent_page_id` = ? " + "  AND a.`is_disabled`='N' ";

	private static final String GET_PAGE_PRIVILEDGE = "SELECT is_new,is_edit,is_delete FROM t_role_page_mapping WHERE role_id=? AND page_id=?";

	private static final String GET_FAQ_LIST = "SELECT faq_title, faq_description, faq_type FROM t_faq_dtls WHERE is_active='Y'";

	private static final String GET_PERSONAL_DETAILS = "SELECT " + "  a.`First_Name`,a.`Middle_Name`,a.`Last_Name`,a.`CID_Number`," +
	"a.`Title_Of_Courtesy_Id`,a.`Customer_Id`,a.`Fathers_Name`,a.`Identification_Mark`,a.`Remarks`,a.`Blood_Group_Id`,a.`Gender`," +
	"a.`Personal_Info_Id`,a.`Permanant_Village_Id`, "
	+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d-%m-%Y') dob, "
	+ "  b.dzongkhag_name, " + "  c.Gewog_Name, "
	+ "  a.Present_Contact_Address, " + "  d.`blood_group_type`, "
	+ "  e.`Country_Name`, " + "  a.`Permanant_Village_Id`, "
	+ "  e.`Country_Name`, " + "  e.`Nationality`, "
	+ "  f.`Village_Name`, " + "  g.`Occupation_Name`,"
	+ "	l.`Driving_License_No` ,l.`Driving_License_Id`,a.`Is_International`" + "FROM " + "  t_personal_dtls a "
	+ "  LEFT JOIN t_dzongkhag_master b "
	+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
	+ "  LEFT JOIN t_gewog_master c "
	+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
	+ "  LEFT JOIN t_blood_group_master d "
	+ "    ON a.`Blood_Group_Id` = d.`blood_group_id` "
	+ "  LEFT JOIN t_country_master e "
	+ "    ON a.`Permanent_Country_Id` = e.`Country_Id` "
	+ "  LEFT JOIN t_village_master v "
	+ "    ON v.`Village_Id` = a.`Permanant_Village_Id` "
	+ "  LEFT JOIN t_village_master f "
	+ "    ON a.`Permanant_Village_Id` = f.`Village_Id` "
	+ "  LEFT JOIN t_occupation_master g "
	+ "    ON a.`Occupation_Id` = g.`Occupation_Id` "
	+ "	 LEFT JOIN t_driving_license_dtls l "
	+ "	   ON l.`Customer_Id`=a.`Customer_Id`"
	+ "  WHERE a.Personal_Info_Id = ?";

	
	private static final String GET_PERSONAL_DETAILS_BY_CID = "SELECT   COUNT(*)rowCount, DATE_FORMAT(a.`Date_Of_Birth`,'%d/%m/%Y')dob, "
														+ " CONCAT(a.`First_Name`,' ',a.`Middle_Name`,' ',a.`Last_Name`) customerName,a.`CID_Number`,a.`Customer_Id`, "
														+ " a.`Gender`, a.`Permanent_Dzongkhag_Id`,a.`Permanent_Gewog_Id`, a.`Permanant_Village_Id`, "
														+ " a.`Present_Phone_No`,a.`Present_Email`, a.`Present_Contact_Address` "
														+ " FROM   t_personal_dtls a WHERE CID_Number = ?";

	private static final String GET_PERSONAL_DTLS_FOR_EDIT = "SELECT a.Mothers_Name,a.`Guardian_Contact_No`,"
															+ "  a.`Customer_Id`, "
															+ " CONCAT( "
															+ "    a.First_Name, "
															+ "    a.Middle_Name, "
															+ "    a.Last_Name "
															+ "  ) NAME, "
															+ "  a.`CID_Number`, "
															+ "  a.`Title_Of_Courtesy_Id`, "
															+ "  a.`First_Name`, "
															+ "  a.`Middle_Name`, "
															+ "  a.`Last_Name`, "
															+ "  a.`Occupation_Id`, "
															+ "  a.`Nationality_Id`, "
															+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d/%m/%Y') dob, "
															+ "  a.`Fathers_Name`, "
															+ "  a.`Blood_Group_Id`, "
															+ "  a.`Gender`, "
															+ "  a.`Remarks`, "
															+ "  a.`Is_International`, "
															+ "  a.`Permanent_Dzongkhag_Id`, "
															+ "  a.`Permanent_Gewog_Id`, "
															+ "  a.`Permanant_Village_Id`, "
															+ "  a.`Permanent_Country_Id`, "
															+ "  a.`Permanent_Address`, "
															+ "  a.`Present_Dzongkhag_Id`, "
															+ "  a.`Present_Contact_Address`, "
															+ "  a.`Present_Phone_No`, "
															+ "  a.`Present_Email`, "
															+ "  a.`Image_Path`,b.`blood_group_type`, c.`dzongkhag_name`, d.`Gewog_Name`,a.Contact_Type,a.Ministry_Id,a.Department_Id "
															+ "FROM "
															+ "  t_personal_dtls a " 
															+ " LEFT JOIN t_blood_group_master b ON a.`Blood_Group_Id`=b.`blood_group_id`"
															+ " LEFT JOIN t_dzongkhag_master c ON a.`Permanent_Dzongkhag_Id`=c.`dzongkhag_id` " 
															+ " LEFT JOIN t_gewog_master d ON a.`Permanent_Gewog_Id`=d.`Gewog_Id`"
															+ " WHERE a.`Personal_Info_Id` = ?";

	private static final String GET_APPLICANT_AGE = "SELECT TIMESTAMPDIFF(YEAR, a.`Date_Of_Birth`, CURDATE()) AS age FROM t_personal_dtls a WHERE a.`Personal_Info_Id` = ?";
	
	private static final String GET_APPLICANT_AGE_ACCIDENT = "SELECT TIMESTAMPDIFF(YEAR, a.`Date_Of_Birth`, CURDATE()) AS age FROM t_personal_dtls a WHERE a.`CID_Number` = ?";

	private static final String GET_ORGANISATION_DETAILS_LIST = "SELECT "
			+ "  a.`Organization_Info_Id`, " + "  IF( "
			+ "    a.`Organization_Name` <> '', "
			+ "    a.`Organization_Name`, " + "    IF( "
			+ "      b.`Private_Name` <> '', " + "      b.`Private_Name`, "
			+ "      IF( " + "        d.`Department_Name` <> '', "
			+ "        d.`Department_Name`, " + "    IF(h.`Name`<>'' ,h.`Name`, c.`Ministry_Name`  )   "
			+ "      ) " + "    ) " + "  ) AS Organization_Name, "
			+ "  e.`region_name`, " + "  f.`dzongkhag_name`, "
			+ "  g.`Owner_Name`,a.`Customer_Id` " + "FROM "
			+ "  t_organization_info a "
			+ "  LEFT JOIN `t_private_company_master` b "
			+ "    ON b.`Private_Id` = a.`Private_Id` "
			+ "  LEFT JOIN t_ministry_master c "
			+ "    ON c.`Ministry_Id` = a.`Ministry_Id` "
			+ "  LEFT JOIN t_department_master d "
			+ "    ON d.`Department_Id` = a.`Department_Id` "
			+ "  LEFT JOIN t_region_master e "
			+ "    ON e.`region_id` = a.`Region_Id` "
			+ "  LEFT JOIN t_dzongkhag_master f "
			+ "    ON f.`dzongkhag_id` = a.`Dzongkhag_Id` "
			+ "  LEFT JOIN t_owner_master g "
			+ "    ON g.`Owner_Type_Id` = a.`Organization_Type_Id`"
			+" LEFT JOIN t_diplomat_master h     ON a.`Diplomat_Id`=h.`Diplomat_Master_Id`  "
			+ "WHERE a.`Organization_Info_Id` > 0 ";
	/*
	 * "SELECT  " +
	 * "  a.`Organization_Name`,a.`Customer_Id`,b.`Owner_Name`,c.`region_name`,d.`dzongkhag_name`,a.Organization_Info_Id"
	 * + " FROM " + "  t_organization_info a " +
	 * " LEFT JOIN `t_owner_master` b ON b.`Owner_Type_Id`=a.`Organization_Type_Id`"
	 * + " LEFT JOIN `t_region_master` c ON c.`region_id`=a.`Region_Id`" +
	 * " LEFT JOIN `t_dzongkhag_master` d ON d.`dzongkhag_id`=a.`Dzongkhag_Id`"
	 * + " WHERE " + " a.`Organization_Info_Id`>0 ";
	 */

	private static final String GET_ORGANISATION_DETAILS = "SELECT "
		+ "  a.`Phone_Number`,a.`Email_Id`,a.`Customer_Id`,a.`Remarks`,a.`Organization_Type_Id`,a.`Region_Id`,a.`Dzongkhag_Id`,a.`Ministry_Id`," +
				"a.`Department_Id`,a.`Private_Id`,a.`Diplomat_Id`, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  a.Address, "
		+ "  d.region_name, "
		+ "  (SELECT "
		+ "    Description "
		+ "  FROM "
		+ "    `t_owner_master` "
		+ "  WHERE Owner_Type_Id = a.`Organization_Type_Id`) organizationType, "
		+" (SELECT "
		+ "    `Owner_Name` "
		+ "  FROM "
		+ "    `t_owner_master` "
		+ "  WHERE Owner_Type_Id = a.`Organization_Type_Id`) Owner_Name"
		+ " FROM "
		+ "  t_organization_info a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_region_master d "
		+ "    ON a.Region_Id = d.region_id "
		+ " WHERE a.Organization_Info_Id = ?";
	
	private static final String GET_MINISTRY_NAME = "SELECT Ministry_Name FROM t_ministry_master WHERE Ministry_Id=?";
	
	private static final String GET_DEPARTMENT_NAME = "SELECT Department_Name FROM t_department_master WHERE Department_Id=?";
	
	private static final String GET_PRIVATE_NAME = "SELECT Private_Name FROM `t_private_company_master` WHERE Private_Id=?";
		
	private static final String GET_DIPLOMAT_NAME = "SELECT a.`Name`,a.`Diplomat_Master_Id` FROM t_diplomat_master a WHERE a.`Diplomat_Master_Id`=?";
	
	private static final String GET_VEHICLE_DTLS = "SELECT f.Personal_Info_Id,a.`Application_Number`,DATE_FORMAT(a.`Receipt_Date`,'%d-%m-%Y') Receipt_Date,a.`Receipt_Number`,(SELECT `Name` FROM t_passenger_bus_category_master "
		+ "WHERE Passenger_Bus_Category_Id=a.Passenger_Bus_Category_Id)passengerBusType, "
		+ "DATE_ADD(a.Registration_Date,INTERVAL IF(  a.`Bus_Type_Id`>0, "
		+ " "
		+ "IF((SELECT COUNT(*) FROM t_life_span_master WHERE Category_Type='VEHICLE' "
		+ " AND Service_Type=(SELECT bus_type_desc FROM t_bus_type_master  WHERE bus_type_id=a.Bus_Type_Id) AND  Service_Model_Type=c.`Vehicle_Model_Name`)>0, "
		+ " (SELECT Life_Span FROM t_life_span_master WHERE Category_Type='VEHICLE' "
		+ " AND Service_Type=(SELECT bus_type_desc FROM t_bus_type_master  WHERE bus_type_id=a.Bus_Type_Id) AND  Service_Model_Type=c.`Vehicle_Model_Name`), "
		+ " (SELECT Life_Span FROM t_life_span_master WHERE Category_Type='VEHICLE' "
		+ " AND Service_Type=(SELECT bus_type_desc FROM t_bus_type_master  WHERE bus_type_id=a.Bus_Type_Id) AND  Service_Model_Type='ORDINARY')) "
		+ ", "
		+ "IF(e.`Description`='TAXI',(IF((SELECT COUNT(z.Life_Span)rowCount FROM t_life_span_master z WHERE "
		+ " z.`Service_Model_Type`=c.`Vehicle_Model_Name` AND z.`Service_Type`='TAXI' AND z.`Category_Type`='VEHICLE')>0, "
		+ "(SELECT u.`Life_Span` FROM t_life_span_master  u WHERE u.`Service_Model_Type`=c.`Vehicle_Model_Name` "
		+ "  AND u.`Service_Type`='TAXI' AND u.`Category_Type`='VEHICLE'), "
		+ "(SELECT v.`Life_Span` FROM t_life_span_master v WHERE v.`Service_Model_Type`='ORDINARY' "
		+ " AND v.`Service_Type`='TAXI' AND v.`Category_Type`='VEHICLE'))), "
		+ "IF((SELECT COUNT(Life_Span_Master_Id)  FROM t_life_span_master WHERE `Service_Type`=e.Description AND "
		+ "`Category_Type`='VEHICLE')>1, "
		+ "(SELECT Life_Span FROM t_life_span_master WHERE Service_Type=e.Description AND `Category_Type`='VEHICLE' "
		+ "AND c.Vehicle_Model_Name LIKE CONCAT('%',`Service_Model_Type`,'%')), "
		+ "(SELECT Life_Span FROM t_life_span_master WHERE Service_Type=e.Description AND "
		+ "`Category_Type`='VEHICLE'))) "
		+ ")      YEAR) lifeExpiryDate, "
		+ "CONCAT(f.`First_Name`,' ',f.`Middle_Name`,' ',f.`Last_Name`) ownerName, "
		+ "a.Customer_Id,(SELECT b.`Colour_Name`FROM t_colour_master b WHERE b.Colour_Id=a.`Colour`) colourName, "
		+ "a.Chassis_Number,   a.Engine_Number,   a.`Vehicle_Registration_Type`, "
		+ "a.`Vehicle_Registration_Id`,   a.`Region_Id`,   a.`Colour`,   a.`Status`, "
		+ "a.`Vehicle_Number`,   a.`Vehicle_Type_Id`,   a.`Vehicle_Horse_Power`,   a.`Vehicle_Kilowatt`, "
		+ "a.`Engine_CC`,   a.`Manufacture_Year`,   a.`Price`,   a.`Seat_Capacity`,   a.`Load_Capacity`, "
		+ "b.Vehicle_Company_Name,   c.Vehicle_Model_Name,   d.Engine_Name, "
		+ "e.`Vehicle_Type_Name`,   e.`Description` AS vehicleTypeDesc,a.`Registration_Date`, "
		+ "DATE_FORMAT(     a.`Registration_Date`,     '%d-%m-%Y'   ) rgndate, "
		+ "IF(MAX(r.`Expiry_Date`)IS NULL,DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y'), "
		+ "DATE_FORMAT(MAX(r.`Expiry_Date`),'%d-%m-%Y')) AS expdate, "
		+ "DATE_FORMAT(MAX(t.Receipt_Date),'%d-%m-%Y') AS tdate,f.`Personal_Info_Id` , "
		+ "(SELECT COUNT(*) FROM `t_vehicle_cancellation_dtls` WHERE Vehicle_Id=a.`Vehicle_Reg_Dtls_Id` ) AS rowCount, "
		+ "g.`bus_type_desc`,   a.`Bus_Type_Id`,(SELECT DATE_FORMAT(Validity,'%d/%m/%Y') FROM t_vehicle_fitness_test_dtls " +
				"WHERE Vehicle_Id=a.`Vehicle_Reg_Dtls_Id`  ORDER BY Validity DESC LIMIT 1)fitnessValidity "
		+ " FROM   t_vehicle_registration_dtls a "
		+ " LEFT JOIN t_vehicle_company_master b     ON b.Vehicle_Company_Id = a.Vehicle_Company_Id "
		+ " LEFT JOIN t_vehicle_model_master c     ON c.Vehicle_Model_Id = a.Vehicle_Model_Id "
		+ " LEFT JOIN t_engine_type_master d     ON d.Engine_Type_Id = a.Engine_Type_Id "
		+ " LEFT JOIN t_vehicle_type_master e     ON e.`Vehicle_Type_Id` = a.`Vehicle_Type_Id` "
		+ " LEFT JOIN `t_personal_dtls` f     ON f.`Customer_Id` = a.`Customer_Id` "
		+ " LEFT JOIN `t_bus_type_master` g ON a.`Bus_Type_Id`=g.`bus_type_id` "
		+ " LEFT JOIN t_vehicle_renewal_dtls r ON a.Vehicle_Reg_Dtls_Id =r.Vehicle_Id "
		+ " LEFT JOIN t_vehicle_transfer_dtls t ON a.Vehicle_Reg_Dtls_Id =t.Vehicle_Id " 
		+ "WHERE a.Vehicle_Reg_Dtls_Id = ? GROUP BY a.Vehicle_Number";
		
		
		
		
		
		/*
		
		
		
		"SELECT (select `Name` from t_passenger_bus_category_master where Passenger_Bus_Category_Id=a.Passenger_Bus_Category_Id)passengerBusType," 
		+" DATE_ADD(a.Registration_Date,INTERVAL "
		+ "IF( "
		+ " a.`Bus_Type_Id`>0,  IF( "
		+ " (SELECT COUNT(Life_Span_Master_Id) "
		+ " FROM t_life_span_master WHERE `Service_Type`='PASSENGER_BUS' AND `Category_Type`='VEHICLE')>1, "
		+ " (SELECT Life_Span FROM t_life_span_master WHERE      Service_Type=(SELECT bus_type_desc FROM t_bus_type_master "
		+ " WHERE bus_type_id=a.Bus_Type_Id)       AND Category_Type='VEHICLE' AND c.Vehicle_Model_Name LIKE CONCAT('%',`Service_Model_Type`,'%')), "
		+ "  (SELECT Life_Span FROM t_life_span_master WHERE Service_Type=(SELECT bus_type_desc FROM t_bus_type_master "
		+ "    WHERE bus_type_id=a.Bus_Type_Id) AND Category_Type='VEHICLE'  AND `Service_Model_Type`='ORDINARY'  )), "
		+ "   IF((SELECT COUNT(Life_Span_Master_Id) "
		+ " FROM t_life_span_master WHERE `Service_Type`=e.Description AND `Category_Type`='VEHICLE')>1, "
		+ "   (SELECT Life_Span FROM t_life_span_master WHERE Service_Type=e.Description AND `Category_Type`='VEHICLE' "
		+ "   AND c.Vehicle_Model_Name LIKE CONCAT('%',`Service_Model_Type`,'%')), "
		+ "    (SELECT Life_Span FROM t_life_span_master WHERE Service_Type=e.Description AND `Category_Type`='VEHICLE'))) "
		+ "     YEAR) lifeExpiryDate,"
		+" CONCAT(f.`First_Name`,' ',f.`Middle_Name`,' ',f.`Last_Name`) ownerName,"
		+ "  a.Customer_Id,(SELECT b.`Colour_Name`FROM t_colour_master b WHERE b.Colour_Id=a.`Colour`) colourName, "
		+ "  a.Chassis_Number, "
		+ "  a.Engine_Number, "
		+ "  a.`Vehicle_Registration_Type`, "
		+ "  a.`Vehicle_Registration_Id`, "
		+ "  a.`Region_Id`, "
		+ "  a.`Colour`, "
		+ "  a.`Status`, "
		+ "  a.`Vehicle_Number`, "
		+ "  a.`Vehicle_Type_Id`, "
		+ "  a.`Vehicle_Horse_Power`, "
		+ "  a.`Vehicle_Kilowatt`, "
		+ "  a.`Engine_CC`, "
		+ "  a.`Manufacture_Year`, "
		+ "  a.`Price`, "
		+ "  a.`Seat_Capacity`, "
		+ "  a.`Load_Capacity`, "
		+ "  b.Vehicle_Company_Name, "
		+ "  c.Vehicle_Model_Name, "
		+ "  d.Engine_Name, "
		+ "  e.`Vehicle_Type_Name`, "
		+ "  e.`Description` AS vehicleTypeDesc,a.`Registration_Date`, "
		+ "  DATE_FORMAT( "
		+ "    a.`Registration_Date`, "
		+ "    '%d-%m-%Y' "
		+ "  ) rgndate, "
		//+ "  DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y') expdate ," 
		+" IF(MAX(r.`Expiry_Date`)IS NULL,DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y'),DATE_FORMAT(MAX(r.`Expiry_Date`),'%d-%m-%Y')) AS expdate,"
		+ " DATE_FORMAT(MAX(t.Receipt_Date),'%d-%m-%Y') as tdate,"
		+ "f.`Personal_Info_Id` ,(SELECT COUNT(*) FROM `t_vehicle_cancellation_dtls` WHERE Vehicle_Id=a.`Vehicle_Reg_Dtls_Id` ) AS rowCount," 
		+ "g.`bus_type_desc`, "
		+ "  a.`Bus_Type_Id` "
		+ "FROM " 
		+ "  t_vehicle_registration_dtls a "
		+ "  LEFT JOIN t_vehicle_company_master b "
		+ "    ON b.Vehicle_Company_Id = a.Vehicle_Company_Id "
		+ "  LEFT JOIN t_vehicle_model_master c "
		+ "    ON c.Vehicle_Model_Id = a.Vehicle_Model_Id "
		+ "  LEFT JOIN t_engine_type_master d "
		+ "    ON d.Engine_Type_Id = a.Engine_Type_Id "
		+ "  LEFT JOIN t_vehicle_type_master e "
		+ "    ON e.`Vehicle_Type_Id` = a.`Vehicle_Type_Id` "
		+ "  LEFT JOIN `t_personal_dtls` f "
		+ "    ON f.`Customer_Id` = a.`Customer_Id` "
		+ " LEFT JOIN `t_bus_type_master` g ON a.`Bus_Type_Id`=g.`bus_type_id`"
		+ " LEFT JOIN t_vehicle_renewal_dtls r"
		+ " ON a.Vehicle_Reg_Dtls_Id =r.Vehicle_Id"
		+ " LEFT JOIN t_vehicle_transfer_dtls t"
		+ " ON a.Vehicle_Reg_Dtls_Id =t.Vehicle_Id "
		+ " WHERE a.Vehicle_Reg_Dtls_Id = ?"
		+ " GROUP BY a.Vehicle_Number";*/
//		+ " ORDER BY r.Renewal_Date ASC";
	
	private static final String GET_VEHICLE_FOREIGN_DTLS="SELECT "
														+ "  COUNT(Offence_Id) AS rowCount,"
														+ "  Is_Foreign, "
														+ "  Vehicle_Number, "
														+ "  License_Number, "
														+ "  Owner_Name, "
														+ "  Offence_Date, "
														+ "  TIN_No, "
														+ "  Receipt_Number, "
														+ "  Receipt_Date, "
														+ "  Received_From, "
														+ "  Received_Date, "
														+ "  Amount "
														+ "  FROM t_offence_information "
														+ "  WHERE Offence_Id = ? "
														+ "  AND Is_Foreign = 'Y'" ;
	
	private static final String GET_LICENSE_FOREIGN_DTLS = "SELECT "
															+ "  COUNT(Offence_Id) AS rowCount, "
															+ "  Is_Foreign, "
															+ "  Vehicle_Number, "
															+ "  License_Number, "
															+ "  Owner_Name, "
															+ "  Offence_Date, "
															+ "  TIN_No, "
															+ "  Receipt_Number, "
															+ "  Receipt_Date, "
															+ "  Received_From, "
															+ "  Received_Date, "
															+ "  Amount "
															+ "FROM "
															+ "  t_offence_information "
															+ "WHERE License_Id =? "
															+ "  AND Is_Foreign = 'Y'";
	
	private static final String GET_TRANSFEREE_DETAILS = "SELECT "
		+ "  a.`Transferee_Customer_Id` AS Customer_Id "
		+ "FROM "
		+ "  t_vehicle_transfer_dtls a "
		+ "WHERE a.`Vehicle_Id` = ? "
		+ "ORDER BY a.`Created_On` DESC "
		+ "LIMIT 1";

	private static final String GET_PERSONAL_DELAILS1 = "SELECT "
		+ "  a.`Present_Contact_Address`,"
		+ "  a.Customer_Id, "
		+ "  CONCAT( "
		+ "    a.First_Name,\" \", "
		+ "    a.Middle_Name,\" \", "
		+ "    a.Last_Name "
		+ "  ) NAME, "
		+ "  a.CID_Number, "
		+ "  a.Present_Phone_No, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  d.Country_Name, "
		+ "  a.Permanant_Village_Id AS Village_Name, "
		+ "  a.Permanent_Address, "
		+ "  a.Present_Email, "
		+ "  h.region_name,a.Is_International"
		+ " FROM "
		+ "  t_personal_dtls a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_village_master j "
		+ "    ON a.Permanant_Village_Id = j.Village_Id "
		+ "  LEFT JOIN t_country_master d "
		+ "    ON a.Permanent_Country_Id = d.Country_Id "
		+ "  LEFT JOIN t_vehicle_registration_dtls z "
		+ "    ON a.Customer_Id = z.Customer_Id "
		+ "  LEFT JOIN t_region_master h "
		+ "    ON z.Region_Id = h.region_id "
		+ " WHERE a.Customer_Id = ?";

	private static final String GET_ORGANIZATION_DETAILS1 = "SELECT "
		+ "  a.`Organization_Type_Id`,a.Organization_Info_Id, "
		+ "  a.`Address`, "
		+ "  a.Customer_Id, "
		+ "  a.Phone_Number, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  i.Village_Name, "
		+ "  a.Email_Id, "
		+ "  m.`Ministry_Name`, "
		+ "  d.`Department_Name`, "
		+ "  e.`Private_Name`, "
		+ "  o.`Owner_Name`, "
		+ "  o.`Description`,IF(a.Organization_Type_Id=2,e.`Private_Name`,d.`Department_Name`) Organization_Name "
		+ "FROM "
		+ "  t_organization_info a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_village_master i "
		+ "    ON a.Village_Id = i.Village_Id "
		+ "  LEFT JOIN t_ministry_master m "
		+ "    ON a.`Ministry_Id` = m.`Ministry_Id` "
		+ "  LEFT JOIN t_department_master d "
		+ "    ON a.`Department_Id` = d.`Department_Id` "
		+ "  LEFT JOIN t_private_company_master e "
		+ "    ON a.`Private_Id` = e.`Private_Id` "
		+ "  LEFT JOIN t_owner_master o "
		+ "    ON a.`Organization_Type_Id` = o.`Owner_Type_Id` " 
		+ "WHERE a.Customer_Id = ?";

	private static final String GET_RENEWAL_HISTORY_LIST = "SELECT Vehicle_Renewal_Id,Vehicle_Number,Renewal_Date,Expiry_Date,Receipt_No,Receipt_Date,Document_Name,UUID FROM ( "
								+ "SELECT   a.Vehicle_Renewal_Id,   b.Vehicle_Number,   DATE_FORMAT(a.Renewal_Date, '%d-%m-%Y') Renewal_Date, "
								+ " DATE_FORMAT(a.Expiry_Date, '%d-%m-%Y') Expiry_Date,   a.Receipt_No,   DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') Receipt_Date, "
								+ " z.Document_Name,z.UUID FROM   t_vehicle_renewal_dtls a "
								+ " LEFT JOIN `t_vehicle_registration_dtls` b     ON b.`Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id` "
								+ " LEFT JOIN t_payment_dtls c     ON c.Application_Number = a.Application_Number "
								+ " LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number    WHERE a.Vehicle_Id = ? "
								+ " UNION "
								+ " SELECT a.`Vehicle_Reg_Dtls_Id` Vehicle_Renewal_Id,a.`Vehicle_Number` Vehicle_Number,DATE_FORMAT(a.Registration_Date, '%d-%m-%Y') Renewal_Date, "
								+ " DATE_FORMAT(a.Expiry_Date, '%d-%m-%Y') Expiry_Date, "
								+ "a.`Receipt_Number` Receipt_No,a.`Receipt_Date` Receipt_Date,z.`Document_Name`,z.`UUID` "
								+ "FROM t_vehicle_registration_dtls a "
								+ "LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
								+ "WHERE a.`Vehicle_Reg_Dtls_Id`=?)table1 ORDER BY Renewal_Date DESC";
	
	private static final String GET_FITNESS_ID = "SELECT COUNT(*) rowCount, a.Vehicle_Fitness_Test_Id FROM t_vehicle_fitness_test_dtls a WHERE a.Receipt_Number='NA' AND a.Vehicle_Id=? LIMIT 1;";

	private static final String GET_DUPLICATION_HISTORY_LIST = "SELECT "
		+ "  DATE_FORMAT( "
		+ "    a.`Duplicate_Issue_Date`, "
		+ "    '%d-%m-%Y' "
		+ "  ) Duplication_Date, "
		+ "  a.Receipt_No AS duplicate_receipt_no, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') AS duplicate_receipt_date,z.Document_Name,z.UUID "
		+ "FROM "
		+ "  t_vehicle_duplicate_dtls a "
		+ "  LEFT JOIN `t_payment_dtls` b "
		+ "    ON b.`Application_Number` = a.`Application_Number` " +
		" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
		+ "WHERE a.`Vehicle_Id` = ? "
		+ "ORDER BY a.`Duplicate_Issue_Date` DESC";

	private static final String GET_TRANSFER_HISTORY_LIST = "SELECT "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') Transfer_Date, "
		+ "  a.`Receipt_Number`, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') Receipt_Date, "
		+ "  IF( "
		+ "    SUBSTR(a.`Transferor_Customer_Id`, 1, 1) = 'P', "
		+ "    (SELECT "
		+ "      CONCAT( "
		+ "        a.`First_Name`, "
		+ "        ' ', "
		+ "        a.`Middle_Name`, "
		+ "        ' ', "
		+ "        a.`Last_Name` "
		+ "      ) "
		+ "    FROM "
		+ "      t_personal_dtls a "
		+ "    WHERE `Customer_Id` = a.`Transferor_Customer_Id`), "
		+ "    (SELECT "
		+ "      `Organization_Name` "
		+ "    FROM "
		+ "      t_organization_info a "
		+ "    WHERE `Customer_Id` = a.`Transferor_Customer_Id`) "
		+ "  ) transferorName, "
		+ "  IF( "
		+ "    a.`Transferee_Type` = 'P', "
		+ "    (SELECT "
		+ "      CONCAT( "
		+ "        a.`First_Name`, "
		+ "        ' ', "
		+ "        a.`Middle_Name`, "
		+ "        ' ', "
		+ "        a.`Last_Name` "
		+ "      ) "
		+ "    FROM "
		+ "      t_personal_dtls a "
		+ "    WHERE `Customer_Id` = a.`Transferee_Customer_Id`), "
		+ "    (SELECT "
		+ "      `Organization_Name` "
		+ "    FROM "
		+ "      t_organization_info a "
		+ "    WHERE `Customer_Id` = a.`Transferee_Customer_Id`) "
		+ "  ) transfereeName, "
		+ "  b.`Amount_Paid`,z.Document_Name,z.UUID  "
		+ "FROM "
		+ "  t_vehicle_transfer_dtls a "
		+ "  LEFT JOIN t_payment_dtls b "
		+ "    ON a.`Application_Number` = b.`Application_Number` " +
		" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
		+ "WHERE a.`Vehicle_Id` = ? "
		+ "ORDER BY a.`Receipt_Date` DESC";
	
	private static final String GET_CONVERSION_HISTORY_LIST = "SELECT "
		+ "  DATE_FORMAT( "
		+ "    a.`Vehicle_Conversion_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) conversionDate, "
		+ "  a.`Receipt_Number`, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptDate, "
		+ "  (SELECT "
		+ "    Vehicle_Number "
		+ "  FROM "
		+ "    t_vehicle_registration_dtls "
		+ "  WHERE Vehicle_Reg_Dtls_Id = b.`Old_Vehicle_Id`) convertedFrom,z.Document_Name,z.UUID  "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "  LEFT JOIN t_vehicle_conversion b ON a.`Application_Number` = b.`New_Application_Number`" +
			" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number  "
		+ "WHERE a.`Vehicle_Reg_Dtls_Id` = ? "
		+ "  AND a.`Application_Number` IN "
		+ "  (SELECT "
		+ "    `New_Application_Number` "
		+ "  FROM "
		+ "    `t_vehicle_conversion`) ;";

	private static final String GET_FITNESS_HISTORY_LIST = "SELECT a.Remarks,"
		+ "  DATE_FORMAT(a.`Test_Date`, '%d-%m-%Y') test_date, "
		+ "  DATE_FORMAT(a.`Validity`, '%d-%m-%Y') validity, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receiptDate, "
		+ "  a.`Receipt_Number`, "
		+ "  (SELECT `name` FROM t_user_master b WHERE b.login_id=a.`Inspected_By`) Inspected_By, "
		+ "  (SELECT b.`region_name` FROM t_region_master b WHERE b.`region_id`=a.`Processed_Region_id`)regionName, "
		+ "  (SELECT b.`base_office_name` FROM t_base_office_master b WHERE b.`base_office_id`=a.`Processed_Base_id`)baseName "
		+ "FROM "
		+ "  t_vehicle_fitness_test_dtls a "
		+ "WHERE a.`Vehicle_Id` = ? "
		+ "ORDER BY a.Validity DESC";

	private static final String GET_EMISSION_HISTORY_LIST = "SELECT "
		+ "  `Emission_Test_Id`, "
		+ "  `Price`, "
		+ "  DATE_FORMAT(a.`Tested_On`, '%d-%m-%Y') testDate, "
		+ "  DATE_FORMAT(a.`Validity`, '%d-%m-%Y') validity, "
		+ "  (SELECT "
		+ "    `dzongkhag_name` "
		+ "  FROM "
		+ "    t_dzongkhag_master "
		+ "  WHERE `Dzongkhag_Id` = a.`Test_Location`) testLocation, "
		+ "  a.`CO`, "
		+ "  a.`HSU`, "
		+ "  a.`Test_Result` "
		+ "FROM "
		+ "  `t_vehicle_emission_test_dtls` a "
		+ "WHERE a.`Vehicle_Id` = ? "
		+ "ORDER BY a.`Created_On` DESC";

	private static final String GET_OFFENCE_HISTORY_LIST ="SELECT "
		+ " a.`Is_paid`, DATE_FORMAT(a.`Offence_Date`, '%d/%m/%Y') AS offense_date, "
		+ "  a.`Time_Of_Inspection`, "
		+ "  a.`Inspected_By`, "
		+ "  a.`Inspection_Type`, "
		+ "  a.`Traffic_Branch`, "
		+ "  a.`TIN_No`,a.`Place_Of_Inspection`,c.`Offence_Name`,c.`Amount`,z.Document_Name,z.UUID "
		+ "FROM "
		+ "  `t_offence_information` a "
		+ "  LEFT JOIN `t_offence_information_dtls` b "
		+ "    ON b.`Offence_Id` = a.`Offence_Id` "
		+ "  LEFT JOIN `t_offence_type_master` c "
		+ "    ON c.`Offence_Type_Id` = b.`Offence_Type_Id`  " +
				" LEFT JOIN t_document_dtls z ON a.Offence_Id=z.Application_Number AND Document_Type='OFFENCE' " +
				"WHERE ";

	private static final String GET_PERMIT_DETAILS_LIST = "SELECT " + " a.`Driver_Name`,a.`Permit_No`, a.`Permit_Details_Id` "
			+ "FROM " + "  t_permit_dtls a " + "WHERE a.Driver_Name = ? "
			+ "  OR a.Permit_No = ? " + "  OR a.Region_Id = ? ";

	private static final String GET_PERMIT_DETAILS = "SELECT "
			+ "	 a.Permit_No, " + "	 a.Driver_Name, "
			+ "	 a.Driver_Identification_Mark, " + "	 a.Driver_Address, "
			+ "	 a.Driver_Phone_No, " + "	 a.Registration_No, "
			+ "	 a.Region_Id, " + "	 a.Base_Office_Id, "
			+ "	 a.Route_Between, " + "	 a.Route_To, "
			+ "	 a.Vehicle_Type_Id, " + "	 a.Carrying_Capacity, "
			+ "	 a.Seat_Capacity, " + "	 a.`Receipt_No`, "
			+ "	 DATE_FORMAT(a.`Issue_Date`, '%d-%m-%Y') issuedate, "
			+ "	 DATE_FORMAT(a.`Validity`, '%d-%m-%Y') validity, "
			+ "	 a.Remarks " + "	  FROM " + "	  t_permit_dtls a "
			+ "	 WHERE a.Permit_Details_Id = ?";

	private static final String GET_LICENSE_DETAILS = "SELECT z.`Amount_Paid`,z.`Receipt_Date`,z.`Receipt_No`,"
		+ "  a.`Gender`, "
		+ "  a.`Present_Phone_No`, "
		+ "  a.`First_Name`, "
		+ "  a.`Middle_Name`, "
		+ "  a.`Last_Name`, "
		+ "  a.`CID_Number`, "
		+ "  a.`Title_Of_Courtesy_Id`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Fathers_Name`, "
		+ "  a.`Identification_Mark`, "
		+ "  a.`Remarks`, "
		+ "  a.`Blood_Group_Id`, "
		+ "  a.`Personal_Info_Id`, "
		+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d-%m-%Y') dob, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  a.Present_Contact_Address, "
		+ "  d.`blood_group_type`, "
		+ "  e.`Country_Name`, "
		+ "  v.`Village_Name`, "
		+ "  a.Is_International, "
		+ "  e.`Country_Name`, "
		+ "  e.`Nationality`, "
		+ "  f.`Village_Name`, "
		+ "  a.`Permanant_Village_Id`, "
		+ "  g.`Occupation_Name`, "
		+ "  l.`Learner_License_No`, "
		+ "  DATE_FORMAT(l.`Issue_Date`, '%d-%m-%Y') IssueDate, "
	/*	+ "  DATE_FORMAT(l.`Expiry_Date`, '%d-%m-%Y') ExpiryDate, "*/
		+ " if((SELECT count(*) FROM t_learner_license_renewal_dtls u where u.Learner_License_Id=l.Learner_License_Info_Id " +
				" order by u.Expiry_Date desc limit 1)>0," +
				" (SELECT u.Expiry_Date FROM t_learner_license_renewal_dtls u where u.Learner_License_Id=l.Learner_License_Info_Id " +
				" order by u.Expiry_Date desc limit 1),l.Expiry_Date) ExpiryDate,"
		+ "  (SELECT "
		+ "    region_name "
		+ "  FROM "
		+ "    t_region_master "
		+ "  WHERE region_id = l.`Region_Id`) regionName,l.Region_Id, "
		+ "  l.`Learner_License_Info_Id` "
		+ "FROM "
		+ "  t_personal_dtls a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_blood_group_master d "
		+ "    ON a.`Blood_Group_Id` = d.`blood_group_id` "
		+ "  LEFT JOIN t_country_master e "
		+ "    ON a.`Permanent_Country_Id` = e.`Country_Id` "
		+ "  LEFT JOIN t_village_master v "
		+ "    ON v.`Village_Id` = a.`Permanant_Village_Id` "
		+ "  LEFT JOIN t_village_master f "
		+ "    ON a.`Permanant_Village_Id` = f.`Village_Id` "
		+ "  LEFT JOIN t_occupation_master g "
		+ "    ON a.`Occupation_Id` = g.`Occupation_Id` "
		+ "  LEFT JOIN t_learner_license_dtls l "
		+ "    ON l.`Customer_Id` = a.`Customer_Id` "
		+ " LEFT JOIN t_payment_dtls z ON l.`Application_Number`=z.`Application_Number`"
		+ "WHERE a.Personal_Info_Id = ?";
	/*
	 * + "  a.*, " + "  DATE_FORMAT(a.`Date_Of_Birth`, '%d-%m-%Y') dob, " +
	 * "  b.dzongkhag_name, " + "  c.Gewog_Name, " +
	 * "  a.Present_Contact_Address, " + "  d.`blood_group_type`, " +
	 * "  e.`Country_Name`, " + "  v.`Village_Name`, " + "  e.`Country_Name`, "
	 * + "  e.`Nationality`, " + "  f.`Village_Name`, " +
	 * "  g.`Occupation_Name`, " + "  l.`Learner_License_No`, " +
	 * "  l.`Learner_License_Info_Id`, " +
	 * "  DATE_FORMAT(l.`Issue_Date`, '%d-%m-%Y') IssueDate, " +
	 * "  DATE_FORMAT(l.`Expiry_Date`, '%d-%m-%Y') ExpiryDate, " +
	 * "  l.`Region_Id` " + "FROM " + "  t_personal_dtls a " +
	 * "  LEFT JOIN t_dzongkhag_master b " +
	 * "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id " +
	 * "  LEFT JOIN t_gewog_master c " +
	 * "    ON a.Permanent_Gewog_Id = c.Gewog_Id " +
	 * "  LEFT JOIN t_blood_group_master d " +
	 * "    ON a.`Blood_Group_Id` = d.`blood_group_id` " +
	 * "  LEFT JOIN t_country_master e " +
	 * "    ON a.`Permanent_Country_Id` = e.`Country_Id` " +
	 * "  LEFT JOIN t_village_master v " +
	 * "    ON v.`Village_Id` = a.`Permanant_Village_Id` " +
	 * "  LEFT JOIN t_village_master f " +
	 * "    ON a.`Permanant_Village_Id` = f.`Village_Id` " +
	 * "  LEFT JOIN t_occupation_master g " +
	 * "    ON a.`Occupation_Id` = g.`Occupation_Id` " +
	 * "  LEFT JOIN t_learner_license_dtls l " +
	 * "    ON l.`Customer_Id` = a.`Customer_Id` " +
	 * "WHERE a.Personal_Info_Id = ?";
	 */

	private static final String GET_DRIVING_LICENSE_DETAILS_LIST = "SELECT "
			+ "b.`CID_Number`,b.`Customer_Id`,a.`Driving_License_Id`, "
			+ "CONCAT( " + "b.First_Name, "	+ " "+ "b.Middle_Name, " + " " + "b.Last_Name " + ") NAME "
			+  "FROM "
			+ " t_driving_license_dtls a " + "LEFT JOIN t_personal_dtls b "
			+ " ON a.Customer_Id = b.Customer_Id " + "WHERE a.Customer_Id = ?";

	private static final String GET_DRIVING_LICENSE_DETAILS = "SELECT "
			+ "  a.`Driving_License_No`,a.`Region_Id`,a.`Driving_License_Type_Id`,a.`Driving_License_Id`, "
			+ "  b.CID_Number, "
			+ "  CONCAT(b.First_Name,\" \",b.Middle_Name,\" \",b.Last_Name) name "
			+ "FROM " + "  t_driving_license_dtls a "
			+ "  LEFT JOIN t_personal_dtls b "
			+ "    ON a.Customer_Id = b.Customer_Id "
			+ "WHERE a.Region_Id = ? " + "  OR a.Learner_License_No = ? "
			+ "  OR a.Driving_License_Type_Id = ? "
			+ "  OR b.First_Name LIKE ? " + "  OR b.CID_Number = ?";
	// TOOLS
	private static final String GET_PERSONAL_LIST = "SELECT " + " p.Personal_Info_Id,p.`First_Name`,p.`Middle_Name`,p.`Last_Name`,p.`CID_Number`,p.`Customer_Id` ,"
			+ "d.`dzongkhag_name` dzongkhag_name,"
			+ "g.`Gewog_Name` Gewog_Name,"
			+ "b.`blood_group_type` blood_group_type," + "r.`region_name`  "
			+ " FROM " + "  t_personal_dtls p "
			+ " LEFT JOIN t_dzongkhag_master d ON "
			+ " d.`dzongkhag_id`=p.`Permanent_Dzongkhag_Id` "
			+ " LEFT JOIN t_gewog_master g "
			+ " ON g.`Gewog_Id`=p.`Permanent_Gewog_Id`"
			+ " LEFT JOIN t_blood_group_master b "
			+ " ON b.`blood_group_id`=p.`Blood_Group_Id` "
			+ " LEFT JOIN t_region_master r "
			+ " ON r.`region_id`=d.`region_id`" +
			" WHERE  p.`Personal_Info_Id`>0  ";

	private static final String GET_REGION_PERSONAL_DETAILS_LIST = "SELECT p.Personal_Info_Id,p.`First_Name`,p.`Middle_Name`,p.`Last_Name`,p.`CID_Number`,p.`Customer_Id`, "
			+ "d.`dzongkhag_name` dzongkhag_name,"
			+ "g.`Gewog_Name` Gewog_Name,"
			+ "b.`blood_group_type` blood_group_type,"
			+ "r.`region_name`  "
			+ "FROM t_region_master r "
			+ "LEFT JOIN t_dzongkhag_master d ON d.`region_id`=r.`region_id` "
			+ "LEFT JOIN t_personal_dtls p ON p.`Permanent_Dzongkhag_Id`=d.`dzongkhag_id` "
			+ " LEFT JOIN t_blood_group_master b "
			+ " ON b.`blood_group_id`=p.`Blood_Group_Id` "
			+ " LEFT JOIN t_gewog_master g "
			+ " ON g.`Gewog_Id`=p.`Permanent_Gewog_Id`" +
			"WHERE p.`Personal_Info_Id`>0 ";

	private static final String GET_DRIVING_LICENSE = "SELECT " 
			+ " a.`Customer_Id`,b.`CID_Number`,b.`Gender`,b.`Fathers_Name`,a.`Driving_License_No` "
			+ "a.Region_Id, "  
			+ "c.blood_group_type, "
			+ "d.dzongkhag_name, " + "e.Village_Name, " + "f.Gewog_Name, "
			+ "CONCAT( " + "b.First_Name, " + "b.Middle_Name, "
			+ "b.Last_Name " + ") NAME , "
			+ "DATE_FORMAT(b.`Date_Of_Birth`, '%d-%m-%Y') dob " + "FROM "
			+ "t_driving_license_dtls a " + "LEFT JOIN t_personal_dtls b "
			+ "ON a.Customer_Id = b.Customer_Id "
			+ "LEFT JOIN t_blood_group_master c "
			+ "ON b.`Blood_Group_Id` = c.`blood_group_id` "
			+ "LEFT JOIN t_dzongkhag_master d "
			+ "ON b.`Permanent_Dzongkhag_Id` = d.`dzongkhag_id` "
			+ "LEFT JOIN t_village_master e "
			+ "ON b.`Permanant_Village_Id` = e.`Village_Id` "
			+ "LEFT JOIN t_gewog_master f "
			+ "ON b.`Permanent_Gewog_Id` = f.`Gewog_Id` "
			+ "WHERE a.Driving_License_Id = ?";

	private static final String GET_LEARNER_LICENSE = "SELECT a.`Application_Number`,"
			+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt,"
			+ "   DATE_FORMAT(a.`Renewal_Date`, '%d-%m-%Y') Renewal_Date,"
			+ "  DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y') expiry,"
			+ "  a.`Remarks`,z.Document_Name,z.UUID " + "  FROM "
			+ "  t_learner_license_renewal_dtls a  LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number"
			+ "  WHERE a.Learner_License_Id = ? order by a.Renewal_Date desc";

	private static final String GET_LEARNER_LICENSE_DUPLICATION = "SELECT "
			+ " DATE_FORMAT(a.`Issue_Date`, '%d-%m-%Y') issue, "
			+ " DATE_FORMAT(a.`Delivered_On`, '%d-%m-%Y') delivered ,"
			+ " DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt,b.`Amount_Paid` AS amount_paid,b.`Receipt_No` AS learner_receipt_no,z.Document_Name,z.UUID "
			+ "  FROM "
			+ "  t_learner_license_duplicate_dtls a  LEFT JOIN `t_payment_dtls` b ON b.`Application_Number`=a.`Application_Number` " +
			" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
			+ "  WHERE a.Learner_License_Id = ?  order by a.Issue_Date desc";

	private static final String GET_LEARNER_LICENSE_RENEWAL_DETAILS = "SELECT "
			+ " DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt, "
			+ " DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y') expiry " + "  FROM "
			+ "  t_learner_license_renewal_dtls a "
			+ "  WHERE a.Learner_License_No = ?";

	private static final String GET_DRIVING_LICENSE_RENEWAL_LISTS = "SELECT "
			+ "  DATE_FORMAT(a.`Renewal_Date`, '%d-%m-%Y') renewal, "
			+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt, "
			+ "  DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y') expiry, "
			+ "  DATE_FORMAT(a.`Delivered_On`, '%d-%m-%Y') delivered, "
			+ "  a.Receipt_No,z.Document_Name,z.UUID  " + "FROM "
			+ "  t_driving_license_renewal_dtls a " +
			" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
			+ "WHERE a.`License_Id` = ?  order by a.`Renewal_Date` desc ";

	private static final String GET_DRIVING_LICENSE_DUPLICATION_LISTS = "SELECT "
			+ "DATE_FORMAT(a.`Duplication_Date`, '%d-%m-%Y') duplication, "
			+ "DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt, "
			+ "DATE_FORMAT(a.`Delivered_On`, '%d-%m-%Y') delivered , "
			+ "a.Receipt_No,z.Document_Name,z.UUID   "
			+ "FROM "
			+ "t_driving_license_duplicate a " +
					" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
			+ "WHERE License_Id = ? ORDER BY  a.`Duplication_Date` DESC";

	private static final String GET_LICENSE_SUSPENSION_LISTS = "SELECT "
			+ "DATE_FORMAT(a.`Start_Date`, '%d-%m-%Y') startDate, "
			+ "DATE_FORMAT(a.`End_Date`, '%d-%m-%Y') endDate, "
			+ "DATE_FORMAT(a.`Re_Issue_Date`, '%d-%m-%Y') reIssueDate, "
			+ "  a.`Reason_For_Suspension`, "
			+ "  IF(a.`Is_Reissued`='N','Suspended','Re-Issued') suspendStatus,z.Document_Name,z.UUID   "
			+ "FROM " + "t_driving_license_suspension a " +
					" LEFT JOIN t_document_dtls z ON concat('LIC_SUSPN_',a.Driving_License_Suspension_Id)=z.Application_Number "
			+ "WHERE a.License_Id = ? ORDER BY a.`Start_Date` DESC";
	
	private static final String GET_LEARNER_SUSPENSION_LIST =  "SELECT "
													+ "  DATE_FORMAT(a.`Start_Date`, '%d-%m-%Y') startDate, "
													+ "  DATE_FORMAT(a.`End_Date`, '%d-%m-%Y') endDate, "
													+ "  DATE_FORMAT(a.`Re_Issue_Date`, '%d-%m-%Y') reIssueDate, "
													+ "  a.`Reason_For_Suspension`, "
													+ "  IF(a.`Is_Reissued`='N','Suspended','Re-Issued') suspendStatus,z.Document_Name,z.UUID  "
													+ "FROM "
													+ "  `t_learner_license_suspension` a " +
															" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
													+ "WHERE a.`Learner_Id` = ?  ORDER BY a.`Start_Date` DESC;";

	
	/*private static final String GET_LICENSE_SUSPENSION_LISTS ="SELECT "
															+ "DATE_FORMAT(a.`Start_Date`, '%d-%m-%Y') startDate, "
															+ "DATE_FORMAT(a.`End_Date`, '%d-%m-%Y') endDate, "
															+ "DATE_FORMAT(a.`Re_Issue_Date`, '%d-%m-%Y') reIssueDate, "
															+ "a.Reason_For_Suspension "
															//+ "b.Document_Name,"
															//+ "b.Document_Path,"
															//+ "b.Document_Type,"
															//+ "b.UUID"
															//+ ""
															+ "FROM t_driving_license_suspension a "
															//+ "LEFT JOIN t_document_dtls b ON CONCAT('LIC_SUSPN_',a.Driving_License_Suspension_Id) = b.Application_Number "
															+ "WHERE a.License_Id = ?";*/

	private static final String GET_DRIVE_TYPE_SUSPENSION_LISTS = "SELECT "
			+ "DATE_FORMAT(a.`Start_Date`, '%d-%m-%Y') startDate, "
			+ "DATE_FORMAT(a.`End_Date`, '%d-%m-%Y') endDate, "
			+ "DATE_FORMAT(a.`Re_Issued_Date`, '%d-%m-%Y') reIssueDate ,"
			+ " b.Drive_Type_Name,z.Document_Name,z.UUID " + "FROM " + "t_drive_type_suspension a "
			+ "LEFT JOIN t_drive_type_master b "
			+ "ON a.`Drive_Type_Id` = b.`Drive_Type_Id` " +
					" LEFT JOIN t_document_dtls z ON concat('DRV_TYP_SUSPN_',a.Drive_Type_Suspension_Id)=z.Application_Number "
			+ "WHERE a.License_Id = ? ORDER BY a.`Start_Date` DESC";

	
	private static final String GET_DRIVING_LICENSE_ENDORSEMENT_LISTS = "SELECT "
		+ "  (SELECT "
		+ "    Driving_License_No "
		+ "  FROM "
		+ "    t_driving_license_dtls "
		+ "  WHERE Driving_License_Id = a.`License_Id`) drivingLicenseNo, "
		+ "  DATE_FORMAT(a.`Endorsed_Date`, '%d-%m-%Y') endorseddate, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt, "
		+ "  DATE_FORMAT(a.`Dispatched_Date`, '%d-%m-%Y') delivered, "
		+ "  a.Receipt_No, "
		+ "  a.IID, "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = b.`Drive_Type_Id`) Drive_Type_Name,z.Document_Name,z.UUID   "
		+ "FROM "
		+ "  t_driving_license_endorsement a "
		+ "  LEFT JOIN t_driving_license_drive_type b "
		+ "    ON a.`Driving_License_Endorsement_Id` = b.`Endorsement_Id` " +
				" LEFT JOIN t_document_dtls z ON a.Application_Number=z.Application_Number "
		+ "WHERE a.`Customer_Id` = ? "
		+ "ORDER BY b.`License_Id`";
	
	/*
	private static final String GET_DRIVING_LICENSE_ENDORSEMENT_LISTS = "SELECT "
		+ "  (SELECT "
		+ "    Driving_License_No "
		+ "  FROM "
		+ "    t_driving_license_dtls "
		+ "  WHERE Driving_License_Id = a.`License_Id`) drivingLicenseNo, "
		+ "  DATE_FORMAT(a.`Endorsed_Date`, '%d-%m-%Y') endorseddate, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receipt, "
		+ "  DATE_FORMAT(a.`Dispatched_Date`, '%d-%m-%Y') delivered, "
		+ "  a.Receipt_No, "
		+ "  a.IID, "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = b.`Drive_Type_Id`) Drive_Type_Name "
		+ "FROM "
		+ "  t_driving_license_endorsement a "
		+ "  LEFT JOIN t_driving_license_drive_type b "
		+ "    ON a.`Driving_License_Endorsement_Id` = b.`Endorsement_Id` "
		+ "WHERE a.`Customer_Id` = ? "
		+ "ORDER BY b.`License_Id` ORDER BY a.`Endorsed_Date` DESC";*/
	
//	private static final String GET_CUSTOMER_ID = "SELECT Customer_Id FROM t_driving_license_dtls a WHERE a.`Driving_License_Id`=?";

	private static final String GET_CUSTOMER_ID = "SELECT Customer_Id FROM t_driving_license_dtls a WHERE a.`Driving_License_Id`=?";

	/*
	 * private static final String GET_REGION_LICENSE_HOLDER_LIST =
	 * "SELECT  dl.`Driving_License_Type_Id`,dl.`Customer_Id`,p.`Personal_Info_Id`, "
	 * + "p.`CID_Number`," + " p.`First_Name`," + " p.`Middle_Name`, " +
	 * "p.`Last_Name`, " + "dl.`Driving_License_No` ," + " r.`region_name`," +
	 * " d.`dzongkhag_name`," + " dl.`Issue_Date`, " + "dl.`Expiry_Date`"+
	 * ",dl.Driving_License_Id" + " FROM " + "  t_driving_license_dtls dl " +
	 * "  LEFT JOIN t_personal_dtls p " +
	 * "    ON p.`Customer_Id` = dl.`Customer_Id` " +
	 * "  LEFT JOIN t_dzongkhag_master d " +
	 * "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` " +
	 * "  LEFT JOIN t_gewog_master g " +
	 * "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` " +
	 * "  LEFT JOIN t_blood_group_master b " +
	 * "    ON b.`blood_group_id` = p.`Blood_Group_Id` " +
	 * "  LEFT JOIN t_region_master r " +
	 * "    ON r.`region_id` = dl.`region_id` "
	 * +"WHERE dl.`Driving_License_Id`>0 AND ";
	 */
	private static final String GET_LICENSE_HOLDER_DETAILS = "SELECT  dl.`Issue_Date` AS non_issue_date,p.`Date_Of_Birth`,dl.`Driving_License_Type_Id`,dl.`Customer_Id`,p.`Personal_Info_Id`,  "
			+ "p.`CID_Number`,"
			+ " p.`First_Name`,"
			+ " p.`Middle_Name`, "
			+ "p.`Last_Name`, "
			+ "dl.`Driving_License_No` ,"
			+ " r.`region_name`,"
			+ " d.`dzongkhag_name`,"
			+ " DATE_FORMAT(dl.`Issue_Date`, '%d-%m-%Y') AS Issue_Date,"
			+ "DATE_FORMAT(dl.`Expiry_Date`, '%d-%m-%Y') AS Expiry_Date, "
			+ "v.`Village_Name`"
			+ ",dl.Driving_License_Id"
			+ " FROM "
			+ "  t_driving_license_dtls dl "
			+ "  LEFT JOIN t_personal_dtls p "
			+ "    ON p.`Customer_Id` = dl.`Customer_Id` "
			+ "  LEFT JOIN t_dzongkhag_master d "
			+ "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` "
			+ "  LEFT JOIN t_gewog_master g "
			+ "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` "
			+ "  LEFT JOIN t_blood_group_master b "
			+ "    ON b.`blood_group_id` = p.`Blood_Group_Id` "
			+ "  LEFT JOIN t_region_master r "
			+ "    ON r.`region_id` = dl.`region_id` "
			+ "LEFT JOIN t_village_master v "
			+ "ON v.`Village_Id`=p.`Permanant_Village_Id` "
			+ "WHERE dl.Driving_License_Id>0 ";
	
	private static final String GET_PERMIT_NO_DETAILS = "SELECT "
														+ "a.Issue_Date, "
														+ "a.Driver_License_No, "
														+ "a.Registration_No, "
														+ "a.Permit_No, "
														+ "a.Driver_Name, "
														+ "a.Driver_Phone_No, "
														+ "a.Driver_Address, "
														+ " Permit_Details_Id,"
														+ "DATE_FORMAT(a.Issue_Date, '%d-%m-%Y') AS Issue_Date, "
														+ "DATE_FORMAT(a.Validity, '%d-%m-%Y') AS Validity "
														+ "FROM "
														+ "t_permit_dtls a "
														+ "WHERE a.Driver_License_No IS NOT NULL ";
	
	private static final String SEARCH_ROUTE_PERMIT_VEHICLE = "SELECT  a.Permit_Details_Id,a.Registration_No,a.`Seat_Capacity`,b.`Vehicle_Type_Name`,COUNT(*) "
	+ "FROM t_permit_dtls a "
	+ "LEFT JOIN t_vehicle_type_master b ON a.`Vehicle_Type_Id`=b.`Vehicle_Type_Id` "
	+ " WHERE a.Driver_License_No IS NOT NULL";

	private static final String GET_SUSPENSION_DETAILS = "SELECT z.`Amount_Paid`,z.`Receipt_Date`,z.`Receipt_No`,"
		+ "  le.`Learner_License_Info_Id`, "
		+ "  a.`First_Name`, "
		+ "  a.`Middle_Name`, "
		+ "  a.`Last_Name`, "
		+ "  a.`CID_Number`, "
		+ "  a.`Present_Phone_No`, "
		+ "  a.`Title_Of_Courtesy_Id`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Fathers_Name`, "
		+ "  a.`Identification_Mark`, "
		+ "  le.`Remarks`, "
		+ "  a.`Blood_Group_Id`, "
		+ "  a.`Gender`, "
		+ "  a.`Personal_Info_Id`, "
		+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d/%m/%Y') dob, "
		+ "  a.`Date_Of_Birth`, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  a.Present_Contact_Address, "
		+ "  d.`blood_group_type`, "
		+ "  e.`Country_Name`, "
		+ "  v.`Village_Name`, "
		+ "  a.Permanant_Village_Id, "
		+ "  a.Is_International, "
		+ "  e.`Country_Name`, "
		+ "  e.`Nationality`, "
		+ "  f.`Village_Name`, "
		+ "  g.`Occupation_Name`, "
		+ "  l.`Driving_License_No`, "
		+ "  l.`Driving_License_Id`, "
		+ "  l.`Status`, "
		+ "  DATE_FORMAT(l.`Issue_Date`, '%d/%m/%Y') IssueDate, "
		+ "  DATE_FORMAT(l.`Expiry_Date`, '%d/%m/%Y') ExpiryDate, "
		+ "  l.`Region_Id`, "
		+ "  (SELECT "
		+ "    region_name "
		+ "  FROM "
		+ "    t_region_master "
		+ "  WHERE region_id = l.`Region_Id`) regionName, "
		+ "  l.`Driving_License_No`, "
		+ "  l.`Driving_License_Type_Id`, "
		+ "  le.`Learner_License_No`, "
		+ "  l.`Driving_License_Id`,a.`Ministry_Id`,a.`Department_Id` "
		+ "FROM "
		+ "  t_personal_dtls a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_blood_group_master d "
		+ "    ON a.`Blood_Group_Id` = d.`blood_group_id` "
		+ "  LEFT JOIN t_country_master e "
		+ "    ON a.`Permanent_Country_Id` = e.`Country_Id` "
		+ "  LEFT JOIN t_village_master v "
		+ "    ON v.`Village_Id` = a.`Permanant_Village_Id` "
		+ "  LEFT JOIN t_village_master f "
		+ "    ON a.`Permanant_Village_Id` = f.`Village_Id` "
		+ "  LEFT JOIN t_occupation_master g "
		+ "    ON a.`Occupation_Id` = g.`Occupation_Id` "
		+ "  LEFT JOIN t_driving_license_dtls l "
		+ "    ON l.`Customer_Id` = a.`Customer_Id` "
		+ "  LEFT JOIN t_learner_license_dtls le "
		+ "    ON le.`Customer_Id` = a.`Customer_Id` " 
		+ " LEFT JOIN t_payment_dtls z ON l.`Application_Number`=z.`Application_Number` "
		+ " WHERE l.Driving_License_Id = ? ";
	//deepak
	
	
	private static final String GET_PERMIT_DETAILS_FOR_RENEWAL ="SELECT "
																+ " a.Permit_Details_Id,a.`Region_Id` Processed_Region_Id," +
																	"(SELECT `base_office_name` FROM `t_base_office_master` WHERE `Base_Office_Id`=a.`Base_Office_Id`) Processed_Base_Id, "
																+ " a.Driver_License_No, "
																+ " a.Registration_No,b.`Vehicle_Type_Name`, "
																+ " a.Permit_No, "
																+ " a.Driver_Name, "
																+ " a.Driver_Phone_No, "
																+ " a.Driver_Address, "
																+ " a.Route_To, "
																+ " a.Route_Between, "
																+ " a.Vehicle_Type_Id, "
																+ " a.Carrying_Capacity, "
																+ " a.Engine_CC, "
																+ " a.Seat_Capacity, "
																+ " a.Journey_Purpose,"
																+ " a.Remarks,"
																+ " DATE_FORMAT(a.Issue_Date, '%d/%m/%Y') AS Issue_Date, "
																+ " DATE_FORMAT(a.Validity, '%d/%m/%Y') AS Validity "
																+ " FROM "
																+ " t_permit_dtls a "
																+" LEFT JOIN t_vehicle_type_master b ON a.`Vehicle_Type_Id`=b.`Vehicle_Type_Id` "
																+ " WHERE a.Driver_License_No IS NOT NULL " 
																+ " AND a.Permit_Details_Id =? ";
	
	
	/*private static final String CHECK_IF_LEARNER_NO_EXISTS = "SELECT COUNT(*) rowCount, Test_Type FROM t_etest_application a WHERE a.`Learner_License_No`=? AND a.`Is_License_Issued`='N'";
*/
	
/*	private static final String CHECK_IF_LEARNER_NO_EXISTS = "SELECT COUNT(*) rowCount, Test_Type FROM t_etest_application a " +
			" WHERE a.`Learner_License_No`=? AND a.`Is_License_Issued`='N' " +
			" and a.`Juris_Type_Id`=? and a.`Test_Location_Id`=?";*/
	
	private static final String CHECK_IF_LEARNER_NO_EXISTS = "SELECT COUNT(*) rowCount, Test_Type FROM t_etest_application a" +
	" WHERE a.`Learner_License_No`=? AND a.`Is_License_Issued`='N'" +
	"  and a.`Juris_Type_Id`=? and a.`Test_Location_Id`=?";

	
	private static final String INSERT_T_DOCUMENT_DTLS = "INSERT INTO `t_document_dtls` (`Application_Number`,`Document_Type`,`Document_Name`,`Document_Path`,`UUID`) VALUES (?,?,?,?,?)";

	private static final String FETCH_SELECTIVE_DOCUMENT_DATA = "SELECT Application_Number,Document_Type, Document_Name, Document_Path, UUID "
			+ "FROM t_document_dtls WHERE Application_Number =? and Document_Type=?";

	/*private static final String GET_LEARNER_INFORMATION = "SELECT "
			+ "  a.`Application_Number`, " + "  CONCAT( "
			+ "    c.`First_Name`, " + "    ' ', " + "    c.`Middle_Name`, "
			+ "    ' ', " + "    c.`Last_Name` " + "  ) pname, "
			+ "  c.`CID_Number`, " + "  (SELECT " + "    `Drive_Type_Name` "
			+ "  FROM " + "    `t_drive_type_master` "
			+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id`) driveType, "
			+ "  a.`Theory_Marks_Obtained`, " + "  a.`Theory_Test_Status` "
			+ "FROM " + "  t_etest_application a "
			+ "  LEFT JOIN t_learner_license_dtls b "
			+ "    ON a.`Learner_License_No` = b.`Learner_License_No` "
			+ "  LEFT JOIN t_personal_dtls c "
			+ "    ON b.`Customer_Id` = c.`Customer_Id` "
			+ "WHERE a.`Learner_License_No` = ? "
			+ "  AND a.`Is_License_Issued` = 'N'";*/
	
	

	private static final String GET_LEARNER_INFORMATION = "SELECT "
			+ "  a.`Application_Number`, " + "  CONCAT( "
			+ "    c.`First_Name`, " + "    ' ', " + "    c.`Middle_Name`, "
			+ "    ' ', " + "    c.`Last_Name` " + "  ) pname, "
			+ "  c.`CID_Number`, " + "  (SELECT " + "    `Drive_Type_Name` "
			+ "  FROM " + "    `t_drive_type_master` "
			+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id`) driveType, "
			+ "  a.`Theory_Marks_Obtained`, " + "  a.`Theory_Test_Status`,a.`Is_etest` "
			+ " FROM " + "  t_etest_application a "
			+ "  LEFT JOIN t_learner_license_dtls b "
			+ "    ON a.`Learner_License_No` = b.`Learner_License_No` "
			+ "  LEFT JOIN t_personal_dtls c "
			+ "    ON b.`Customer_Id` = c.`Customer_Id` "
			+ " WHERE a.`Learner_License_No` = ? "
			+ "  AND a.`Is_License_Issued` = 'N' ORDER BY a.`Test_Date` DESC LIMIT 1";
	
	
	
	
	/*private static final String GET_LEARNER_INFORMATION = "SELECT "
		+ "  CONCAT( "
		+ "    c.`First_Name`, "
		+ "    ' ', "
		+ "    c.`Middle_Name`, "
		+ "    ' ', "
		+ "    c.`Last_Name` "
		+ "  ) pname, "
		+ "  c.`CID_Number`, "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id` "
		+ "  LIMIT 1) driveType "
		+ "FROM "
		+ "  t_learner_license_dtls b "
		+ "  LEFT JOIN `t_learner_licn_drive_type` a "
		+ "    ON b.`Learner_License_Info_Id` = a.`Learner_License_Id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON b.`Customer_Id` = c.`Customer_Id` "
		+ "WHERE b.`Learner_License_No` = ?";*/
	
	/*private static final String GET_LICENSE_INFORMATION = "SELECT "
		+ "  a.`Application_Number`, "
		+ "  CONCAT( "
		+ "    c.`First_Name`, "
		+ "    ' ', "
		+ "    c.`Middle_Name`, "
		+ "    ' ', "
		+ "    c.`Last_Name` "
		+ "  ) pname, "
		+ "  c.`CID_Number`, "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id`) driveType, "
		+ "  a.`Theory_Marks_Obtained`, "
		+ "  a.`Theory_Test_Status` "
		+ "FROM "
		+ "  t_etest_application a "
		+ "  LEFT JOIN t_driving_license_dtls b "
		+ "    ON a.`Learner_License_No` = b.`Driving_License_No` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON b.`Customer_Id` = c.`Customer_Id` "
		+ "WHERE a.`Learner_License_No` = ? "
		+ "  AND a.`Is_License_Issued` = 'N'";*/
	
	private static final String GET_LICENSE_INFORMATION = "SELECT "
		+ "  a.`Application_Number`, "
		+ "  CONCAT( "
		+ "    c.`First_Name`, "
		+ "    ' ', "
		+ "    c.`Middle_Name`, "
		+ "    ' ', "
		+ "    c.`Last_Name` "
		+ "  ) pname, "
		+ "  c.`CID_Number`, "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id`) driveType, "
		+ "  a.`Theory_Marks_Obtained`, "
		+ "  a.`Theory_Test_Status` "
		+ "FROM "
		+ "  t_etest_application a "
		+ "  LEFT JOIN t_driving_license_dtls b "
		+ "    ON a.`Learner_License_No` = b.`Driving_License_No` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON b.`Customer_Id` = c.`Customer_Id` "
		+ "WHERE a.`Learner_License_No` = ? "
		+ "  AND a.`Is_License_Issued` = 'N'  ORDER BY a.`Test_Date` DESC LIMIT 1";

	public static final String CHECK_IF_VEHICLE_WAS_TRANSFERED = "SELECT "
			+ "  COUNT(*) rowCount " + "FROM " + "  t_vehicle_transfer_dtls a "
			+ "WHERE a.`Vehicle_Number` = ?";

	public static final String GET_TRANSFEREE_TYPE_AND_CUSTOMER_CODE = "SELECT "
			+ "  a.`Transferee_Type`, "
			+ "  a.`Transferee_Customer_Id` "
			+ "FROM "
			+ "  t_vehicle_transfer_dtls a "
			+ "WHERE a.`Vehicle_Number` = ? "
			+ "ORDER BY a.Transfer_Date DESC LIMIT 1";

	public static final String GET_PERSONAL_DETAILS1 = "SELECT " + "COUNT(*)rowCount,  CONCAT( "
			+ "    a.`First_Name`, " + "    \" \", " + "    a.`Middle_Name`, "
			+ "    \" \", " + "    a.`Last_Name` " + "  ) pname,a.`Date_Of_Birth`, "
			+ "  a.`CID_Number`, " + "  a.`Permanant_Village_Id`, "
			+ "  g.`Gewog_Name`, " + "  d.`dzongkhag_name`, "
			+ "  a.`Present_Contact_Address`, " + "  a.`Image_Path` " + "FROM "
			+ "  t_personal_dtls a " + "  LEFT JOIN t_gewog_master g "
			+ "    ON a.`Permanent_Gewog_Id` = g.`Gewog_Id` "
			+ "  LEFT JOIN t_dzongkhag_master d "
			+ "    ON a.`Permanent_Dzongkhag_Id` = d.`dzongkhag_id` "
			+ "WHERE a.`Customer_Id` = ?";

	public static final String GET_ORGANIZATION_DETAILS = "SELECT "
			+ "  a.`Organization_Name`, " + "  (SELECT "
			+ "    `Department_Name` " + "  FROM " + "    t_department_master "
			+ "  WHERE `Department_Id` = a.`Department_Id`) department, "
			+ "  (SELECT " + "    `Ministry_Name` " + "  FROM "
			+ "    t_ministry_master "
			+ "  WHERE `Ministry_Id` = a.`Ministry_Id`) ministry, "
			+ "  a.`Address` " + "FROM " + "  t_organization_info a "
			+ "  LEFT JOIN t_owner_master b "
			+ "    ON a.`Organization_Type_Id` = b.`Owner_Type_Id` "
			+ "  LEFT JOIN t_ministry_master m "
			+ "    ON a.`Ministry_Id` = m.`Ministry_Id` "
			+ "  LEFT JOIN t_department_master d "
			+ "    ON a.`Department_Id` = d.`Department_Id` "
			+ "WHERE a.`Customer_Id` = ?";

	public static final String GET_REG_TYPE_CUSTOMER_ID_FROM_VEHICLE_REGISTRATION = "SELECT Vehicle_Registration_Type, Customer_Id FROM t_vehicle_registration_dtls a WHERE a.`Vehicle_Number`=?";

	private static final String CHECK_IF_VEHICLE_IS_CANCELLED = "SELECT COUNT(*) rowCount FROM t_vehicle_cancellation_dtls a WHERE a.`Vehicle_Number`=?";
	
	private static final String GET_VEHICLE_ACCIDENT_DETAILS = "SELECT "
													+ "  a.`Vehicle_Reg_Dtls_Id`, "
													+ "  a.`Registration_Type`, "
													+ "  a.`Vehicle_Registration_Id`, "
													+ "  a.`Vehicle_Type_Id`, "
													+ "  a.`Vanity_Number`, "
													+ "  a.`Vehicle_Company_Id`, "
													+ "  DATE_FORMAT( "
													+ "    a.`Registration_Date`, "
													+ "    '%d/%m/%Y' "
													+ "  ) registrationDate, "
													+ "  a.`Vehicle_Model_Id`, "
													+ "  a.`Engine_Type_Id`, "
													+ "  a.`Engine_Number`, "
													+ "  a.`Chassis_Number`, "
													+ "  a.`Status`, "
													+ "  a.`Colour`, "
													+ "  a.`Price`, "
													+ "  a.`Load_Capacity`, "
													+ "  a.`Dealer_Id`, "
													+ "  a.`Seat_Capacity`, "
													+ "  a.`Vehicle_Horse_Power`, "
													+ "  a.`Vehicle_Kilowatt`, "
													+ "  a.`Unladen_Weight`, "
													+ "  a.`Manufacture_Year`, "
													+ "  a.`Engine_CC`, "
													+ "  a.`Manufacture_Country_Id`, "
													+ "  a.`Purchase_Type`, "
													+ "  `Region_Id`,a.Remarks,a.Bus_Type_Id "
													+ "FROM "
													+ "  t_vehicle_registration_dtls a "
													+ "WHERE a.`Vehicle_Number` = ? "
													+ "  AND a.`Vehicle_Type_Id` = ?";

	public static final String GET_VEHICLE_DETAILS = "SELECT "
			+ "  (SELECT "
			+ "    Vehicle_Model_Name "
			+ "  FROM "
			+ "    t_vehicle_model_master "
			+ "  WHERE Vehicle_Model_Id = a.`Vehicle_Model_Id`) vehicleModel, "
			+ "  DATE_FORMAT( "
			+ "    a.`Registration_Date`, "
			+ "    '%d/%m/%Y' "
			+ "  ) initialDateOfRegistration, "
			+ "  (SELECT "
			+ "    `Vehicle_Type_Name` "
			+ "  FROM "
			+ "    t_vehicle_type_master "
			+ "  WHERE `Vehicle_Type_Id` = a.`Vehicle_Type_Id`) typeOfVehicle, "
			+ "  (SELECT " + "    `Vehicle_Company_Name` " + "  FROM "
			+ "    `t_vehicle_company_master` "
			+ "  WHERE `Vehicle_Company_Id` = a.`Vehicle_Company_Id`) make, "
			+ "  a.`Chassis_Number`, " + "  a.`Engine_Number`, "
			+ "  a.`Manufacture_Year`, " + "  a.`Unladen_Weight`, "
			+ "  a.`Load_Capacity`, " + "  a.`Seat_Capacity`, "
			+ "  a.`Colour` " + "FROM " + "  t_vehicle_registration_dtls a "
			+ "WHERE a.`Vehicle_Number` = ?";
	private static final String GET_ORGANISATION_CUSTOMER_ID = "SELECT COUNT(*) rowCount,o.Customer_Id,o.`Ministry_Id`,o.`Department_Id`,"
			+ "o.`Region_Id`,o.`Dzongkhag_Id`,o.`Address`,"
			+ "o.`Phone_Number`,o.`Remarks`,o.`Email_Id`"
			+ " FROM `t_organization_info` o"
			+ " WHERE o.`Organization_Name`=? AND o.`Organization_Type_Id`=?";

	/*
	 * private static final String GET_REGION_LICENSE_HOLDER_LIST1 =
	 * "SELECT  dl.`Customer_Id`,p.`Personal_Info_Id`, " + "p.`CID_Number`," +
	 * " p.`First_Name`," + " p.`Middle_Name`, " + "p.`Last_Name`, " +
	 * "dl.`Learner_License_No` ," + " r.`region_name`," + " d.`dzongkhag_name`"
	 * + " FROM " + "  t_learner_license_dtls dl " +
	 * "  LEFT JOIN t_personal_dtls p " +
	 * "    ON p.`Customer_Id` = dl.`Customer_Id` " +
	 * "  LEFT JOIN t_dzongkhag_master d " +
	 * "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` " +
	 * "  LEFT JOIN t_gewog_master g " +
	 * "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` " +
	 * "  LEFT JOIN t_blood_group_master b " +
	 * "    ON b.`blood_group_id` = p.`Blood_Group_Id` " +
	 * "  LEFT JOIN t_region_master r " +
	 * "    ON r.`region_id` = dl.`Region_id` "
	 * +"WHERE dl.`Learner_License_Info_Id`>0 AND ";
	 */
	private static final String GET_LICENSE_HOLDER_DETAILS1 = "SELECT  dl.`Customer_Id`,p.`Personal_Info_Id`,  "
			+ "p.`CID_Number`,"
			+ " p.`First_Name`,"
			+ " p.`Middle_Name`, "
			+ "p.`Last_Name`, "
			+ "dl.`Learner_License_No` ,"
			+ " r.`region_name`,"
			+ " d.`dzongkhag_name`,"
			+ " dl.`Issue_Date`, "
			+ "dl.`Expiry_Date`,v.`Village_Name` "
			+ " FROM "
			+ "  t_learner_license_dtls dl "
			+ "  LEFT JOIN t_personal_dtls p "
			+ "    ON p.`Customer_Id` = dl.`Customer_Id` "
			+ "  LEFT JOIN t_dzongkhag_master d "
			+ "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` "
			+ "  LEFT JOIN t_gewog_master g "
			+ "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` "
			+ "  LEFT JOIN t_blood_group_master b "
			+ "    ON b.`blood_group_id` = p.`Blood_Group_Id` "
			+ "  LEFT JOIN t_region_master r "
			+ "    ON r.`region_id` = dl.`region_id` "
			+ "LEFT JOIN t_village_master v "
			+ "ON v.`Village_Id`=p.`Permanant_Village_Id` "
			+ "WHERE dl.`Learner_License_Info_Id`>0 ";
	/*
	 * private static final String GET_RENEWAL_PERSONAL_DTLS = "SELECT  " +
	 * "CONCAT( " + "p.First_Name, " + "p.Middle_Name, " + "p.Last_Name " +
	 * ") OwnerName, "
	 * +"p.Customer_Id,p.CID_Number AS CID  FROM t_personal_dtls p WHERE ";
	 */
	private static final String GET_TEST_DATE =  "SELECT "
		+ "  DATE_FORMAT(Test_Date, '%d-%m-%Y') Test_Date1, "
		+ "  DATE_FORMAT(Test_Date, '%Y-%m-%d') AS testDate,`Institute_Name` "
		+ "FROM "
		+ "  t_etest_config_master "
		+ "WHERE Test_Location_Id = ? "
		+ "  AND Juris_Type_Id = ? AND `Test_Type`=?"
		+ "  AND Test_Date > CURRENT_DATE "
		+ "ORDER BY Test_Date";
	

	private static final String GET_TEST_DATE_FOR_ADMIN =  "SELECT "
		+ "  DATE_FORMAT(Test_Date, '%d-%m-%Y') Test_Date1, "
		+ "  DATE_FORMAT(Test_Date, '%Y-%m-%d') AS testDate,`Institute_Name` "
		+ "FROM "
		+ "  t_etest_config_master "
		+ "WHERE Test_Location_Id = ? "
		+ "  AND Juris_Type_Id = ? "
		+ " "
		+ "ORDER BY Test_Date DESC";
	
	//private static final String GET_APPLICANT_COUNT = "SELECT COUNT(*) rowCount FROM `t_etest_application` a WHERE a.`Test_Date`=? AND a.`Test_Location_Id`=?";

	private static final String GET_APPLICANT_COUNT = "SELECT "
													+ "  COUNT(*) rowCount "
													+ "FROM "
													+ "  `t_etest_application_prebooking` a "
													+ "WHERE (( "
													+ "    a.`Status` = 'PENDING' "
													+ "    AND a.`Created_On` > ( "
													+ "      DATE_SUB( "
													+ "        CURRENT_TIMESTAMP, "
													+ "        INTERVAL '0:03' HOUR_MINUTE "
													+ "      ) "
													+ "    ) "
													+ "  ) "
													+ "  OR a.`Status` = 'BOOKED') "
													+ "  AND a.`Test_Date`=? "
													+ "  AND a.`Test_Location_Id`=? AND a.`Juris_Type_Id`=?";
	
	private static final String GET_MAX_APPLICANT = "SELECT Max_Applicants FROM `t_etest_config_master` WHERE Test_Date=? AND Test_Location_Id=?";
	
	/*
	 * private static final String GET_REGION_LEARNER_LICENSE_HOLDER_LIST =
	 * "SELECT  dl.`Customer_Id`,dl.`Learner_License_Info_Id`, " +
	 * "p.`CID_Number`," + " p.`First_Name`," + " p.`Middle_Name`, " +
	 * "p.`Last_Name`, " + "dl.`Learner_License_No` ," + " r.`region_name`," +
	 * " dd.`Driving_License_Id`," + " d.`dzongkhag_name`" + " FROM " +
	 * "  t_learner_license_dtls dl " + "  LEFT JOIN t_personal_dtls p " +
	 * "    ON p.`Customer_Id` = dl.`Customer_Id` " +
	 * "  LEFT JOIN t_dzongkhag_master d " +
	 * "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` " +
	 * "  LEFT JOIN t_gewog_master g " +
	 * "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` " +
	 * "  LEFT JOIN t_blood_group_master b " +
	 * "    ON b.`blood_group_id` = p.`Blood_Group_Id` " +
	 * "  LEFT JOIN t_region_master r " +
	 * "    ON r.`region_id` = dl.`Region_id` " +
	 * "  LEFT JOIN t_driving_license_dtls dd " +
	 * "    ON dd.`Customer_Id` = p.`Customer_Id` "
	 * +"WHERE dl.`Learner_License_Info_Id`>0 AND ";
	 */

	private static final String GET_LEARNER_LICENSE_HOLDER_DETAILS = "SELECT p.`Date_Of_Birth`,"
			+ "  dl.`Customer_Id`, "
			+ "  dl.`Learner_License_Info_Id`, "
			+ "  p.`CID_Number`, "
			+ "  p.`First_Name`, "
			+ "  p.`Middle_Name`, "
			+ "  p.`Last_Name`, "
			+ "  dl.`Learner_License_No`, "
			+ "  r.`region_name`, "
			+ "  d.`dzongkhag_name`, "
			+ "  DATE_FORMAT(dl.`Issue_Date`, '%d-%m-%Y') AS Issue_Date, "
			+ "  DATE_FORMAT(dl.`Expiry_Date`, '%d-%m-%Y') AS Expiry_Date, "
			+ "  dl.`Expiry_Date`, "
			+ "  v.`Village_Name` "
			+ "FROM "
			+ "  t_learner_license_dtls dl "
			+ "  LEFT JOIN t_personal_dtls p "
			+ "    ON p.`Customer_Id` = dl.`Customer_Id` "
			+ "  LEFT JOIN t_dzongkhag_master d "
			+ "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` "
			+ "  LEFT JOIN t_gewog_master g "
			+ "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` "
			+ "  LEFT JOIN t_blood_group_master b "
			+ "    ON b.`blood_group_id` = p.`Blood_Group_Id` "
			+ "  LEFT JOIN t_region_master r "
			+ "    ON r.`region_id` = dl.`region_id` "
			+ "  LEFT JOIN t_village_master v "
			+ "    ON v.`Village_Id` = p.`Permanant_Village_Id`"
			+ "WHERE dl.`Learner_License_Info_Id`>0 ";

	private static final String GET_LEARNER_LICENSE_DETAILS = "SELECT "
			+ "   a.`First_Name`,a.`Middle_Name`,a.`Last_Name`,a.`CID_Number`,a.`Title_Of_Courtesy_Id`,a.`Customer_Id`,"
			+"a.`Fathers_Name`,  a.`Identification_Mark`,le.`Remarks`,a.`Blood_Group_Id`,a.`Gender`,a.`Personal_Info_Id`," 
			+ "a.`Permanant_Village_Id`,a.`Is_International`," + "  DATE_FORMAT(a.`Date_Of_Birth`, '%d-%m-%Y') dob, "
			+ "  b.dzongkhag_name, " + "  c.Gewog_Name, a.Is_International,a.Permanant_Village_Id, "
			+ "  a.Present_Contact_Address, " + "  d.`blood_group_type`, "
			+ "  e.`Country_Name`, " + "  v.`Village_Name`, "
			+ "  e.`Country_Name`, " + "  e.`Nationality`, "
			+ "  f.`Village_Name`, " + "  g.`Occupation_Name`,"
			+ "	l.`Driving_License_No`, " + "	l.`Driving_License_Id`, "
			+ "	l.`Region_Id`, " + "	le.`Learner_License_No`,"
			+ "	le.`Learner_License_Info_Id`" + "	FROM "
			+ "  t_personal_dtls a " + "  LEFT JOIN t_dzongkhag_master b "
			+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
			+ "  LEFT JOIN t_gewog_master c "
			+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
			+ "  LEFT JOIN t_blood_group_master d "
			+ "    ON a.`Blood_Group_Id` = d.`blood_group_id` "
			+ "  LEFT JOIN t_country_master e "
			+ "    ON a.`Permanent_Country_Id` = e.`Country_Id` "
			+ "  LEFT JOIN t_village_master v "
			+ "    ON v.`Village_Id` = a.`Permanant_Village_Id` "
			+ "  LEFT JOIN t_village_master f "
			+ "    ON a.`Permanant_Village_Id` = f.`Village_Id` "
			+ "  LEFT JOIN t_occupation_master g "
			+ "    ON a.`Occupation_Id` = g.`Occupation_Id` "
			+ "	 LEFT JOIN t_driving_license_dtls l "
			+ "	   ON l.`Customer_Id`=a.`Customer_Id`"
			+ "	 LEFT JOIN t_learner_license_dtls le "
			+ "	   ON le.`Customer_Id`=a.`Customer_Id`"
			+ "  WHERE le.Learner_License_Info_Id = ?";


	private static final String GET_DRIVE_TYPE_FOR_LEARNER = "SELECT "
		+ "  GROUP_CONCAT(b.`Drive_Type_Name`) driveTypeName "
		+ "FROM "
		+ "  `t_learner_licn_drive_type` a "
		+ "  LEFT JOIN t_drive_type_master b "
		+ "    ON a.`Drive_Type_Id` = b.`Drive_Type_Id` "
		+ "WHERE a.Learner_License_Id = ?";
	
	/*private static final String GET_DRIVE_TYPE_FOR_DRIVING_LICENSE = "SELECT "
		+ "  c.`Drive_Type_Name` "
		+ "FROM "
		+ "  `t_driving_license_dtls` a "
		+ "  LEFT JOIN t_drive_type_master c "
		+ "    ON a.`Drive_Type_Id` = c.`Drive_Type_Id` "
		+ "WHERE a.`Customer_Id` = ? "
		+ "  UNION "
		+ "  SELECT "
		+ "    d.Drive_Type_Name "
		+ "  FROM "
		+ "    `t_driving_license_endorsement` b "
		+ "    LEFT JOIN t_drive_type_master d "
		+ "      ON b.`Drive_Type_Id` = d.`Drive_Type_Id` "
		+ "  WHERE b.`Customer_Id` = ?";*/
	
	private static final String GET_DRIVE_TYPE_FOR_DRIVING_LICENSE = "SELECT "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id`) driveTypes "
		+ "FROM "
		+ "  `t_driving_license_drive_type` a "
		+ "WHERE a.`Customer_Id` = ?";

	private static final String GET_VEHICLE_TRANSFER_RENEWAL = "(SELECT  r.Vehicle_Reg_Dtls_Id,  CONCAT(IFNULL(p.First_Name,''),' ', IFNULL(p.Middle_Name,''),' ', IFNULL(p.Last_Name,'')) personalOwnerName, "
			+ "  o.Organization_Name,p.CID_Number, t.Application_Number, r.Engine_Number, "
			+ "  r.Chassis_Number, t.Transferee_Type AS Vehicle_Registration_Type,  i.`Vehicle_Type_Name`, "
			+ "  r.Vehicle_Number, 'transfer' AS details "
			+ "FROM "
			+ "  t_vehicle_transfer_dtls t "
			+ "  LEFT JOIN t_vehicle_registration_dtls r "
			+ "    ON r.Vehicle_Reg_Dtls_Id = t.Vehicle_Id "
			+ "  LEFT JOIN t_personal_dtls p "
			+ "    ON p.Customer_Id = t.Transferee_Customer_Id "
			+ "  LEFT JOIN t_organization_info o "
			+ "    ON o.Customer_Id = t.Transferee_Customer_Id "
			+ "  LEFT JOIN `t_vehicle_type_master` i "
			+ "    ON i.`Vehicle_Type_Id` = r.`Vehicle_Type_Id` "
			+ "WHERE t.Vehicle_Id NOT IN "
			+ "  (SELECT "
			+ "    c.Vehicle_Id "
			+ "  FROM " + "    t_vehicle_cancellation_dtls c)";

	private static final String GET_VEHICLE_REGISTRATION_RENEWAL = "(SELECT "
		+ "  r.Vehicle_Reg_Dtls_Id, "
		+ "  CONCAT( "
		+ "    IFNULL(p.First_Name, ''), "
		+ "    ' ', "
		+ "    IFNULL(p.Middle_Name, ''), "
		+ "    ' ', "
		+ "    IFNULL(p.Last_Name, '') "
		+ "  ) personalOwnerName, "
		+ "  IF( "
		+ "    o.`Organization_Name` <> '', "
		+ "    o.`Organization_Name`, "
		+ "    IF( "
		+ "      d.`Private_Name` <> '', "
		+ "      d.`Private_Name`, "
		+ "      IF( "
		+ "        e.`Department_Name` <> '', "
		+ "        e.`Department_Name`, "
		+ "        c.`Ministry_Name` "
		+ "      ) "
		+ "    ) "
		+ "  ) AS Organization_Name, "
		+ "  p.CID_Number, "
		+ "  r.Application_Number, "
		+ "  r.Engine_Number, "
		+ "  r.Chassis_Number, "
		+ "  r.Vehicle_Registration_Type, "
		+ "  i.`Vehicle_Type_Name`, "
		+ "  r.Vehicle_Number, "
		+ "  'register' AS details "
		+ "FROM "
		+ "  t_vehicle_registration_dtls r "
		+ "  LEFT JOIN t_personal_dtls p "
		+ "    ON p.Customer_Id = r.Customer_Id "
		+ "  LEFT JOIN t_organization_info o "
		+ "    ON o.Customer_Id = r.Customer_Id "
		+ "  LEFT JOIN `t_vehicle_type_master` i "
		+ "    ON i.`Vehicle_Type_Id` = r.`Vehicle_Type_Id` "
		+ "  LEFT JOIN t_private_company_master d "
		+ "    ON o.`Private_Id` = d.`Private_Id` "
		+ "  LEFT JOIN t_ministry_master c "
		+ "    ON c.`Ministry_Id` = o.`Ministry_Id` "
		+ "  LEFT JOIN t_department_master e "
		+ "    ON e.`Department_Id` = o.`Department_Id` "
		+ "WHERE r.Vehicle_Reg_Dtls_Id > 0 ";

	private static final String GET_FORGOT_PASSWORD_DETAILS = "SELECT `login_id`,`question_id`,`answer` FROM `t_password_retrieval_dtls` WHERE `login_id`=?";

	private static final String CHECK_IF_LICENSE_HAS_OFFENCE = "SELECT COUNT(*) rowCount FROM t_offence_information a WHERE a.`License_Id`=? AND a.`Receipt_Number` IS NULL";

	private static final String CHECK_IF_LICENSE_IS_CANCELLED = "SELECT COUNT(*) rowCount FROM t_driving_license_cancelled a WHERE a.`License_No`=?";

	private static final String CHECK_IF_LICENSE_IS_SUSPENDED = "SELECT COUNT(*) rowCount FROM t_driving_license_suspension a WHERE a.`License_Number`=? AND a.`Is_Reissued` ='N'";

	private static final String CHECK_IF_LICENSE_HAS_BEEN_ISSUED = "SELECT "
		+ " (SELECT COUNT(*) FROM `t_driving_license_application` a WHERE a.`Customer_Id`=b.`Customer_Id`) AS applicationCount, "
		+ " (SELECT COUNT(*) FROM `t_driving_license_dtls` c WHERE c.`Customer_Id`=b.`Customer_Id`) AS licenseCount"
		+ " FROM"
		+ " `t_personal_dtls` b" 
		+" WHERE b.`CID_Number` = ?";

	private static final String CHECK_IF_LEARNER_LICENSE_HAS_BEEN_ISSUED = "SELECT "
		+ " (SELECT COUNT(*) FROM `t_learner_license_application` a WHERE a.`Customer_Id`=b.`Customer_Id`) AS applicationCount, "
		+ " (SELECT COUNT(*) FROM `t_learner_license_dtls` c WHERE c.`Customer_Id`=b.`Customer_Id`) AS licenseCount"
		+ " FROM"
		+ " `t_personal_dtls` b" 
		+" WHERE b.`CID_Number` = ?";

	private static final String CHECK_IF_DRIVING_LICENSE_HAS_BEEN_ISSUED = "SELECT COUNT(*) rowCount FROM t_personal_dtls b  LEFT JOIN t_driving_license_dtls a ON b.`Customer_Id`= a.`Customer_Id` WHERE b.Cid_Number=? AND `Driving_License_Type_Id`='N'";

	private static final String CHECK_IF_DRIVING_LICENSE_IS_SUSPENDED = "SELECT COUNT(*) rowCount FROM t_driving_license_suspension a WHERE a.`License_Id`=? AND a.`Is_Reissued` ='N'";

	private static final String CHECK_IF_DRIVING_LICENSE_IS_CANCELLED = "SELECT COUNT(*) rowCount ,a.`Cancellation_Reason`,DATE_FORMAT(a.`Cancellation_Date`,'%d/%m/%Y') AS Cancellation_Date FROM t_driving_license_cancelled a WHERE a.`License_Id`=?";

	private static final String CHECK_IF_DRIVING_LICENSE_HAS_OFFENCE = "SELECT COUNT(*) rowCount FROM t_offence_information a WHERE a.`License_Id`=? AND a.`Receipt_Number` IS NULL";

	private static final String CHECK_IF_NON_COMMERCIAL_DRIVING_LICENSE_HAS_BEEN_ISSUED = "SELECT COUNT(*) rowCount FROM t_personal_dtls b  LEFT JOIN t_driving_license_dtls a ON b.`Customer_Id`= a.`Customer_Id` WHERE b.Cid_Number=? AND `Driving_License_Type_Id`='C'";

	private static final String GET_TOP_DTLS = "SELECT "
			+ "  COUNT(*) rowCount, " + "  CONCAT( " + "    a.`First_Name`, "
			+ "    ' ', " + "    a.`Middle_Name`, " + "    ' ', "
			+ "    a.`Last_Name` " + "  ) NAME, "
			+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d-%m-%Y') dob, "
			+ "  a.`Present_Contact_Address`, " + "  a.`Present_Phone_No`, "
			+ "  a.`Gender`, " + "  a.`Fathers_Name`, "
			+ "  b.`blood_group_type`, " + "  c.dzongkhag_name, "
			+ "  d.gewog_name, " + "  e.village_name, " + "  f.Customer_Id, "
			+ "  g.`Driving_License_No`,g.Driving_License_Id " + "FROM " + "  t_personal_dtls a "
			+ "  LEFT JOIN t_blood_group_master b "
			+ "    ON a.`Blood_Group_Id` = b.`blood_group_id` "
			+ "  LEFT JOIN t_dzongkhag_master c "
			+ "    ON a.`Permanent_Dzongkhag_Id` = c.`dzongkhag_id` "
			+ "  LEFT JOIN t_gewog_master d "
			+ "    ON a.`Permanent_Gewog_Id` = d.`Gewog_Id` "
			+ "  LEFT JOIN t_village_master e "
			+ "    ON a.`Permanant_Village_Id` = e.`Village_Id` "
			+ "  LEFT JOIN t_vehicle_registration_dtls f "
			+ "    ON a.`Customer_Id` = f.`Customer_Id` "
			+ "  LEFT JOIN t_driving_license_dtls g "
			+ "    ON a.`Customer_Id` = g.`Customer_Id` "
			+ "WHERE a.Cid_Number = ? "
			+ "  AND g.`Driving_License_Type_Id` = 'C'";

	private static final String GET_VEHICLE_LISTS = "SELECT "
		+ "  CONCAT( "
		+ "    a.`First_Name`, "
		+ "    ' ', "
		+ "    a.`Middle_Name`, "
		+ "    ' ', "
		+ "    a.`Last_Name` "
		+ "  ) NAME, "
		+ "  f.Vehicle_Number, "
		+ "  f.Vehicle_Reg_Dtls_Id, "
		+ "  f.Manufacture_Year, "
		+ "  g.Vehicle_Model_Name, "
		+ "  h.Vehicle_Company_Name "
		+ "FROM "
		+ "  t_vehicle_registration_dtls f "
		+ "  LEFT JOIN t_vehicle_model_master g "
		+ "    ON f.`Vehicle_Model_Id` = g.`Vehicle_Model_Id` "
		+ "  LEFT JOIN t_vehicle_company_master h "
		+ "    ON f.`Vehicle_Company_Id` = h.`Vehicle_Company_Id` "
		+ "  LEFT JOIN t_personal_dtls a "
		+ "    ON a.`Customer_Id` = f.`Customer_Id` "
		+ "  LEFT JOIN `t_vehicle_type_master` i "
		+ "    ON i.`Vehicle_Type_Id` = f.`Vehicle_Type_Id` "
		+ "WHERE a.Cid_Number = ? "
		+ "  AND i.`Description` = 'TAXI' "
		+ "  AND f.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    `Vehicle_Id` "
		+ "  FROM "
		+ "    `t_vehicle_cancellation_dtls`)";

	private static final String GET_TOTAL_DL_ISSUED = "SELECT "
		+ "  COUNT(a.`Driving_License_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_driving_license_dtls` a "
		+ "WHERE a.`Created_On` = ? "
		+ "  AND IF( "
		+ "    '1' = ?, "
		+ "    a.`Processed_Region_Id` = ?, "
		+ "    a.`Processed_Base_Id` = ? "
		+ "  )";
	
	private static final String GET_TOTAL_DL_ISSUED_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Driving_License_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_driving_license_dtls` a "
		+ "WHERE a.`Created_On` = ? ";

	private static final String GET_TOTAL_LEARNER_ISSUED = "SELECT "
		+ "  COUNT(a.`Learner_License_Info_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_learner_license_dtls` a "
		+ "WHERE DATE_FORMAT(a.`Created_On`, '%Y-%m-%d') = ? "
		+ "  AND IF( "
		+ "    '1' = ?, "
		+ "    a.`Processed_Region_Id` = ?, "
		+ "    a.`Processed_Base_Id` = ? "
		+ "  )";

	private static final String GET_TOTAL_LEARNER_ISSUED_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Learner_License_Info_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_learner_license_dtls` a "
		+ "WHERE DATE_FORMAT(a.`Created_On`, '%Y-%m-%d') = ? ";

	private static final String GET_TOTAL_VEHICLE_REGISTERED = "SELECT "
		+ "  COUNT(a.`Vehicle_Registration_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_vehicle_registration_dtls` a "
		+ "WHERE  a.`Created_On` = CURDATE() "
		+ "  AND IF( "
		+ "    '1' = ?, "
		+ "    a.`Processed_Region_Id` = ?, "
		+ "    a.`Processed_Base_Id` = ? "
		+ "  )";
	
	private static final String GET_TOTAL_VEHICLE_REGISTERED_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Vehicle_Registration_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_vehicle_registration_dtls` a "
		+ "WHERE a.`Created_On` = CURDATE()";
	
	private static final String GET_TOTAL_VEHICLE_RENEWED = "SELECT "
		+ "  COUNT(a.`Vehicle_Renewal_Id`) AS rowCount "
		+ "FROM "
		+ "  t_vehicle_renewal_dtls a "
		+ "WHERE a.`Created_On` = ? "
		+ "  AND IF( "
		+ "    '1' = ?, "
		+ "    a.`Processed_Region_Id` = ?, "
		+ "    a.`Processed_Base_Id` = ? "
		+ "  )";

	private static final String GET_TOTAL_VEHICLE_RENEWED_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Vehicle_Renewal_Id`) AS rowCount "
		+ "FROM "
		+ "  t_vehicle_renewal_dtls a "
		+ "WHERE a.`Created_On` = ? ";
	
	private static final String GET_TOTAL_LICENSE_RENEWED = "SELECT "
		+ "  COUNT(a.`Driving_License_Renewal_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_driving_license_renewal_dtls` a "
		+ "WHERE a.`Created_On` = ? "
		+ "  AND IF( "
		+ "    '1' = ?, "
		+ "    a.`Processed_Region_Id` = ?, "
		+ "    a.`Processed_Base_Id` = ? "
		+ "  )";
	
	private static final String GET_TOTAL_LICENSE_RENEWED_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Driving_License_Renewal_Id`) AS rowCount "
		+ "FROM "
		+ "  `t_driving_license_renewal_dtls` a "
		+ "WHERE a.`Created_On` = ? ";

	private static final String GET_TOTAL_NEW_LICENSE_PRINTED = "SELECT "
		+ "  COUNT(a.`Application_Number`) AS rowCount "
		+ "FROM "
		+ "  `t_print_dtls` a "
		+ "WHERE a.`Document_Type` = 'DL' "
		+ "  AND a.`Printed_On` = ? "
		+ "  AND a.`Jurisdiction_Id`=?";

	private static final String GET_TOTAL_NEW_LICENSE_PRINTED_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Application_Number`) AS rowCount "
		+ "FROM "
		+ "  `t_print_dtls` a "
		+ "WHERE a.`Document_Type` = 'DL' "
		+ "  AND a.`Printed_On` = ? ";
		
	private static final String GET_TOTAL_PRINTED = "SELECT "
		+ "  COUNT(a.`Application_Number`) AS rowCount "
		+ "FROM "
		+ "  `t_print_dtls` a "
		+ "WHERE a.`Printed_On` = ? "
		+ "AND a.`Jurisdiction_Id`=?";

	private static final String GET_TOTAL_PRINTED_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Application_Number`) AS rowCount "
		+ "FROM "
		+ "  `t_print_dtls` a "
		+ "WHERE a.`Printed_On` = ?";
	
	private static final String GET_TOTOAL_EARNING = "SELECT "
		+ "  COUNT(a.`Payment_Id`) AS rowCount, "
		+ "  SUM(a.`Amount_Paid`) AS total_amount, "
		+ "  SUM(a.`Penalty_Paid`) AS total_penalty "
		+ "FROM "
		+ "  t_payment_dtls a "
		+ "  LEFT JOIN t_workflow_dtls b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE DATE_FORMAT(a.`Created_On`, '%Y-%m-%d') = ? "
		+ "  AND b.`Juris_Id` = ?";
	
	private static final String GET_TOTOAL_EARNING_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Payment_Id`) AS rowCount, "
		+ "  SUM(a.`Amount_Paid`) AS total_amount, "
		+ "  SUM(a.`Penalty_Paid`) AS total_penalty "
		+ "FROM "
		+ "  t_payment_dtls a "
		+ "  LEFT JOIN t_workflow_dtls b "
		+ "    ON a.`Application_Number` = b.`Application_Number` "
		+ "WHERE DATE_FORMAT(a.`Created_On`, '%Y-%m-%d') = ? ";
	
	private static final String GET_TOTAL_EMISSION = "SELECT "
		+ "  COUNT(a.`Emission_Test_Id`) rowCount "
		+ "FROM "
		+ "  `t_vehicle_emission_test_dtls` a "
		+ "WHERE a.`Created_On` = ?";
	
	private static final String GET_TOTAL_EMISSION_PASSED_OR_FAILED = "SELECT "
		+ "  COUNT(a.`Emission_Test_Id`) rowCount "
		+ "FROM "
		+ "  `t_vehicle_emission_test_dtls` a "
		+ "WHERE a.`Created_On` = ? "
		+ "AND a.`Test_Result`=?";

	private static final String GET_TOTAL_SUBMITTED_TASK = "SELECT "
		+ "  COUNT(a.`Status_Id`) AS rowCount, "
		+ "  'workFlow' AS col "
		+ "FROM "
		+ "  `t_workflow_dtls` a "
		+ "WHERE DATE_FORMAT(a.`Action_Date`, '%Y-%m-%d') = ? "
		+ "  AND a.`Status_Id` IN "
		+ "  (SELECT "
		+ "    c.`Status_Id` "
		+ "  FROM "
		+ "    `t_status_master` c "
		+ "  WHERE c.`Status_Name` = 'SUBMITTED') "
		+ "  AND a.`Juris_Id`=? "
		+ "  UNION "
		+ "  SELECT "
		+ "    COUNT(b.`Status_Id`) AS rowCount, "
		+ "    'workFlowAudit' AS col "
		+ "  FROM "
		+ "    `t_workflow_dtls_audit` b "
		+ "  WHERE DATE_FORMAT(b.Action_Date, '%Y-%m-%d') = ? "
		+ "    AND b.`Status_Id` IN "
		+ "    (SELECT "
		+ "      d.`Status_Id` "
		+ "    FROM "
		+ "      `t_status_master` d "
		+ "    WHERE d.`Status_Name` = 'SUBMITTED') "
		+ "    AND b.Juris_Id=?";

	private static final String GET_TOTAL_SUBMITTED_TASK_FOR_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Status_Id`) AS rowCount, "
		+ "  'workFlow' AS col "
		+ "FROM "
		+ "  `t_workflow_dtls` a "
		+ "WHERE DATE_FORMAT(a.`Action_Date`, '%Y-%m-%d') = ? "
		+ "  AND a.`Status_Id` IN "
		+ "  (SELECT "
		+ "    c.`Status_Id` "
		+ "  FROM "
		+ "    `t_status_master` c "
		+ "  WHERE c.`Status_Name` = 'SUBMITTED')"
		+ "  UNION "
		+ "  SELECT "
		+ "    COUNT(b.`Status_Id`) AS rowCount, "
		+ "    'workFlowAudit' AS col "
		+ "  FROM "
		+ "    `t_workflow_dtls_audit` b "
		+ "  WHERE DATE_FORMAT(b.Action_Date, '%Y-%m-%d') = ? "
		+ "    AND b.`Status_Id` IN "
		+ "    (SELECT "
		+ "      d.`Status_Id` "
		+ "    FROM "
		+ "      `t_status_master` d "
		+ "    WHERE d.`Status_Name` = 'SUBMITTED')";

	private static final String GET_TOTAL_VERIFIED_TASK = "SELECT "
		+ "  COUNT(a.`Status_Id`) AS rowCount, "
		+ "  'workFlow' AS col "
		+ "FROM "
		+ "  `t_workflow_dtls` a "
		+ "WHERE DATE_FORMAT(a.`Action_Date`, '%Y-%m-%d') = ? "
		+ "  AND a.`Status_Id` NOT IN "
		+ "  (SELECT "
		+ "    c.`Status_Id` "
		+ "  FROM "
		+ "    `t_status_master` c "
		+ "  WHERE c.`Status_Name` = 'SUBMITTED') "
		+ "  AND a.`Juris_Id`=? "
		+ "  UNION "
		+ "  SELECT "
		+ "    COUNT(b.`Status_Id`) AS rowCount, "
		+ "    'workFlowAudit' AS col "
		+ "  FROM "
		+ "    `t_workflow_dtls_audit` b "
		+ "  WHERE DATE_FORMAT(b.Action_Date, '%Y-%m-%d') = ? "
		+ "    AND b.`Status_Id` NOT IN "
		+ "    (SELECT "
		+ "      d.`Status_Id` "
		+ "    FROM "
		+ "      `t_status_master` d "
		+ "    WHERE d.`Status_Name` = 'SUBMITTED') "
		+ "    AND b.Juris_Id=?";


	private static final String GET_TOTAL_VERIFIED_TASK_HEAD_OFFICE = "SELECT "
		+ "  COUNT(a.`Status_Id`) AS rowCount, "
		+ "  'workFlow' AS col "
		+ " FROM "
		+ "  `t_workflow_dtls` a "
		+ " WHERE DATE_FORMAT(a.`Action_Date`, '%Y-%m-%d') = ? "
		+ "  AND a.`Status_Id` NOT IN "
		+ "  (SELECT "
		+ "    c.`Status_Id` "
		+ "  FROM "
		+ "    `t_status_master` c "
		+ "  WHERE c.`Status_Name` = 'SUBMITTED') "
		+ "  UNION "
		+ "  SELECT "
		+ "    COUNT(b.`Status_Id`) AS rowCount, "
		+ "    'workFlowAudit' AS col "
		+ "  FROM "
		+ "    `t_workflow_dtls_audit` b "
		+ "  WHERE DATE_FORMAT(b.Action_Date, '%Y-%m-%d') = ? "
		+ "    AND b.`Status_Id` NOT IN "
		+ "    (SELECT "
		+ "      d.`Status_Id` "
		+ "    FROM "
		+ "      `t_status_master` d "
		+ "    WHERE d.`Status_Name` = 'SUBMITTED')";

	private static final String GET_RENEWAL_LISTS = "SELECT "
		+ "  DATE_FORMAT(Renewal_Date, '%d-%m-%Y') renewalDate, "
		+ "  DATE_FORMAT(Expiry_Date, '%d-%m-%Y') expiryDate, "
		+ "  DATE_FORMAT(Dispatched_Date, '%d-%m-%Y') deliveredOn "
		+ "FROM "
		+ "  `t_driving_license_renewal_dtls` "
		+ "WHERE License_Id = ? ORDER BY Renewal_Date DESC";

	private static final String GET_DUPLICATE_LISTS = "SELECT "
			+ "  DATE_FORMAT(a.`Duplication_Date`, '%d-%m-%Y') duplicate, "
			+ "  DATE_FORMAT(a.`Delivered_On`, '%d-%m-%Y') delivered "
			+ "FROM " + "`t_driving_license_duplicate` a "
			+ "LEFT JOIN t_personal_dtls b "
			+ "ON b.`Customer_Id` = a.`Customer_Id` "
			+ "WHERE  b.Cid_Number = ? ORDER BY a.`Duplication_Date` DESC;";

	private static final String CHECK_FOR_VEHICLE_REGISTERED = "SELECT COUNT(*) rowCount FROM t_vehicle_registration_dtls a "
			+ "LEFT JOIN t_personal_dtls b "
			+ "ON b.`Customer_Id`= a.`Customer_Id` "
			+ "LEFT JOIN t_vehicle_type_master c "
			+ "ON a.`Vehicle_Type_Id`=c.`Vehicle_Type_Id` "
			+ "WHERE b.`CID_Number`=? AND c.`Description`='TAXI' " +
			"AND a.`Vehicle_Reg_Dtls_Id` NOT IN  " +
			"(SELECT b.`Vehicle_Id` FROM `t_vehicle_cancellation_dtls` b " +
			"WHERE b.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id`)";

	private static final String CHECK_FOR_COMMERCIAL_LICENSE = "SELECT COUNT(*) rowCount FROM t_driving_license_dtls a "
			+ "LEFT JOIN t_personal_dtls b "
			+ "ON b.`Customer_Id`= a.`Customer_Id` "
			+ "WHERE b.`CID_Number`=? AND a.`Driving_License_Type_Id`='C' " +
			"AND a.`Driving_License_Id` NOT IN " +
			"(SELECT c.`License_Id` FROM `t_driving_license_cancelled` c WHERE c.`License_Id`=a.`Driving_License_Id`)";

	private static final String CHECK_FOR_CID = "SELECT COUNT(*) rowCount,a.Customer_Id FROM t_personal_dtls a WHERE a.`CID_Number`= ?";

	private static final String CHECK_FOR_CID_FOR_ETEST = "SELECT  b.`Application_Number`," +
												"(SELECT COUNT(*) FROM t_etest_application c WHERE c.`Application_Number`=b.`Application_Number`)rowCount," +
												"b.`Drive_Type_Id`,a.Customer_Id " +
												" FROM t_personal_dtls a " +
												"LEFT JOIN t_etest_application b ON a.`CID_Number`=b.`Customer_Id` " +
												"OR a.`Customer_Id`=b.`Customer_Id` WHERE b.`Practical_Test_Status`= 'N' AND a.`CID_Number`= ?" +
												" and b.`Juris_Type_Id`=? and b.`Test_Location_Id`=? ORDER BY b.`Test_Date` DESC LIMIT 1";

/*	private static final String CHECK_FOR_ALREADY_ISSUED = "SELECT "
															+ "IF( "
															+ "(SELECT COUNT(*) FROM `t_top_cancellation_dtls` c WHERE c.`TOP_Number`=a.`TOP_Number` " 
															+" AND c.Cancellation_Reason_Id=10) "
															+ "= "
															+ "(COUNT(DISTINCT a.`Vehicle_Id`)),0,1) AS rowCount "
															+ " FROM `t_top_dtls` a "
															+ " LEFT JOIN t_personal_dtls b ON a.`Customer_Id`=b.`Customer_Id` "
															+ " WHERE b.`CID_Number`=?;";*/
	
	
	private static final String CHECK_FOR_ALREADY_ISSUED = "SELECT COUNT(*) rowCount,(SELECT COUNT(*) FROM `t_top_cancellation_dtls` z "
												+ " WHERE z.`Cancellation_Reason_Id`= (SELECT Reason_Id FROM `t_reason_master` r " +
														"WHERE r.`Reason_Description`='TAXI_SOLD')) AS taxiSold FROM `t_top_dtls` a "
												+ " LEFT JOIN `t_personal_dtls` b ON a.`Customer_Id`=b.`Customer_Id` "
												+ " WHERE b.`CID_Number`=? "
												+ " AND a.`TOP_Number` NOT IN (SELECT TOP_Number FROM `t_top_cancellation_dtls` WHERE `TOP_Number`=a.`TOP_Number`)";
	
	
	
	
	private static final String CEHCK_IF_TOP_IS_CANCELLED = "SELECT "
		+ " IF((SELECT COUNT(*) FROM t_top_dtls c WHERE c.`TOP_Number` = a.`TOP_Number` )" +
				" = (COUNT(DISTINCT a.`Cancellation_Date`)),1,0) rowCount  "
		+ "FROM "
		+ "  `t_top_cancellation_dtls` a "
		+ "  LEFT JOIN t_personal_dtls b "
		+ "    ON b.`Customer_Id` = a.`Customer_Id` "
		+ "WHERE b.`CID_Number` = ?";
	
	private static final String CHECK_IF_APPLICATION_ALREADY_EXISTS_FOR_TOP = "SELECT "
		+ "  COUNT(*) rowCount, "
		+ "  a.`Application_Number` "
		+ "FROM "
		+ "  t_top_application a "
		+ "  LEFT JOIN t_personal_dtls b "
		+ "    ON b.`Customer_Id` = a.`Customer_Id` "
		+ "  LEFT JOIN t_workflow_dtls c "
		+ "    ON a.`Application_Number` = c.`Application_Number` "
		+ "WHERE b.`CID_Number` = ? AND c.`Service_Id`=114 " 
		+ " AND a.`Application_Number` NOT IN (SELECT `Application_Number` FROM `t_workflow_dtls` WHERE `Application_Number`=a.`Application_Number` AND Status_Id IN (12,2))";
	
	private static final String CHECK_THE_APPLICATION_STATUS = "SELECT "
		+ "  b.`Status_Type_Short_Desc` "
		+ "FROM "
		+ "  t_workflow_dtls a "
		+ "  LEFT JOIN t_status_master b "
		+ "    ON a.`Status_Id` = b.`Status_Id` "
		+ "WHERE a.`Application_Number` = ?";

	private static final String CHECK_IF_LEARNER_LICENSE_HAS_OFFENCE = "SELECT COUNT(*) rowCount FROM t_offence_information a WHERE a.`Learner_License_Id`=? AND a.`Receipt_Number` IS NULL";

	private static final String GET_ETEST_OVERALL_MARKS = "SELECT "
		+ "  COUNT(a.`Application_Number`) AS rowCount, "
		+ "  a.`Overall_Marks_Obtained`, "
		+ "  a.`Practical_Test_Status`, "
		+ "  a.`Drive_Type_Id`, "
		+ "  b.`Drive_Type_Name`,a.`Theory_Marks_Obtained`,a.`Practical_Marks_Obtained`,a.`Issue_Type` "
		+ "FROM "
		+ "  `t_etest_application` a "
		+ "  LEFT JOIN `t_drive_type_master` b "
		+ "    ON b.`Drive_Type_Id` = a.`Drive_Type_Id` "
		+ "WHERE a.`Learner_License_No` = ? "
		+ "  AND a.`Is_License_Issued` = 'N' "
		+ "  AND a.`Practical_Test_Status`='P'";


	private static final String GET_ETEST_OVERALL_MARKS_BY_CID = "SELECT "
		+ "  COUNT(a.`Application_Number`) AS rowCount, "
		+ "  a.`Overall_Marks_Obtained`, "
		+ "  a.`Practical_Test_Status`, "
		+ "  a.`Drive_Type_Id`, "
		+ "  b.`Drive_Type_Name`,a.`Theory_Marks_Obtained`,a.`Practical_Marks_Obtained`,a.`Issue_Type` "
		+ "FROM "
		+ "  `t_etest_application` a "
		+ "  LEFT JOIN `t_drive_type_master` b "
		+ "    ON b.`Drive_Type_Id` = a.`Drive_Type_Id` "
		+ "WHERE a.`Customer_Id` = ? "
		+ "  AND a.`Is_License_Issued` = 'N' "
		+ "  AND a.`Practical_Test_Status`='P'";
	
	private static final String GET_ENDORSE_TEST_MARKS = "SELECT COUNT(a.`Application_Number`),a.`Overall_Marks_Obtained`,a.`Practical_Test_Status` FROM `t_etest_application` a WHERE a.`Learner_License_No`=? AND a.`Drive_Type_Id`=?";
	//private static final String GET_DRIVE_TYPE_DESCRIPTION	=	"SELECT a.Drive_Type_Name,a.`Description`,a.`Drive_Type_Category` FROM `t_drive_type_master` a WHERE a.`Drive_Type_Id`=?";
	
	
	
	
	private static final String VALIDATE_LEARNER_LICENSE_TEST	=	"SELECT "
		+ "  a.`Customer_Id`, "
		+ "  b.`Date_Of_Birth`, "
		/*+ "  COUNT(a.`Learner_License_Info_Id`) AS rowCount, "*/
		+ "  c.`Practical_Test_Status`, "
		+ "  c.`Overall_Marks_Obtained`, "
		+ "  c.`Is_License_Issued`, "
		+ "  c.`Test_Date` "
		+ "FROM "
		+ "  `t_learner_license_dtls` a "
		+ "  LEFT JOIN t_personal_dtls b "
		+ "    ON a.`Customer_Id` = b.`Customer_Id` "
		+ "  LEFT JOIN `t_etest_application` c "
		+ "    ON a.`Learner_License_No` = c.`Learner_License_No` "
		+ "    AND c.`Drive_Type_Id` IN "
		+ "    (SELECT "
		+ "      `Drive_Type_Id` "
		+ "    FROM "
		+ "      `t_drive_type_master` "
		+ "    WHERE `Description` = ?) "
		+ "WHERE a.`Learner_License_Info_Id` = ? "
		+ "ORDER BY c.`Test_Date` DESC "
		+ "LIMIT 1 ;";
	
	
	private static final String GET_DRIVE_TYPE_DESCRIPTION	=	"SELECT a.Drive_Type_Name,a.`Description`,a.`Drive_Type_Category` FROM `t_drive_type_master` a WHERE a.`Drive_Type_Id`=?";
	
	/*private static final String GET_PARAM_FOR_COMMERCIAL_CRITERIA	=	"SELECT a.`Customer_Id`,b.`Date_Of_Birth`,"
																+ "  COUNT(a.`Driving_License_Id`) AS rowCount, "
																+ "  a.`Issue_Date`, "
																+ "  MAX(c.`Endorsed_Date`) AS endorsed_date,d.`Practical_Test_Status`,d.`Overall_Marks_Obtained` "
																+ "FROM "
																+ "  `t_driving_license_dtls` a "
																+" LEFT JOIN `t_etest_application` d ON d.`Learner_License_No`=a.`Driving_License_No` "
																+" AND d.`Drive_Type_Id` IN (SELECT d.`Drive_Type_Id` FROM `t_drive_type_master` d WHERE d.`Description` = ?) "
																+ "  LEFT JOIN t_personal_dtls b "
																+ "    ON b.`Customer_Id` = a.`Customer_Id` "
																+ "  LEFT JOIN `t_driving_license_endorsement` c "
																+ "    ON  c.`Customer_Id`=a.`Customer_Id` AND c.`Drive_Type_Id` IN (SELECT d.`Drive_Type_Id` FROM `t_drive_type_master` d WHERE d.`Description` =? )  "
																+ "WHERE (a.`Drive_Type_Id` IN "
																+ "  (SELECT "
																+ "    d.`Drive_Type_Id` "
																+ "  FROM "
																+ "    `t_drive_type_master` d "
																+ "    WHERE d.`Description` = ?) OR c.`Drive_Type_Id` IN (SELECT "
																+ "    d.`Drive_Type_Id` "
																+ "  FROM "
																+ "    `t_drive_type_master` d "
																+ "    WHERE d.`Description` = ?)) "
																+ "  AND a.`Driving_License_Id` = ?";*/
	
	private static final String GET_PARAM_FOR_COMMERCIAL_CRITERIA = "SELECT "
		+ "  (SELECT COUNT(*) FROM t_driving_license_dtls z WHERE z.Driving_License_Id=a.Driving_License_Id) AS rowCount, "
		+ "  a.`Customer_Id`, "
		+ "  b.`Date_Of_Birth`, "
		+ "  c.`Practical_Test_Status`, "
		+ "  c.`Overall_Marks_Obtained`,c.`Is_License_Issued`,c.`Test_Date` "
		+ "FROM "
		+ "  t_driving_license_dtls a "
		+ "  LEFT JOIN t_personal_dtls b ON a.`Customer_Id` = b.`Customer_Id` "
		+ "  LEFT JOIN `t_etest_application` c ON a.`Driving_License_No` = c.`Learner_License_No`  AND c.`Drive_Type_Id` IN "
		+ "    (SELECT `Drive_Type_Id` FROM `t_drive_type_master` WHERE `Description` = ?) "
		+ "WHERE a.`Driving_License_Id` = ? ORDER BY c.`Test_Date` DESC LIMIT 1";
	
	/*private static final String GET_PARAM_FOR_NON_COMMERCIAL_CRITERIA="SELECT "
																	  +" a.`Customer_Id`,"
																	  +" b.`Date_Of_Birth`,"
																	  +" COUNT(a.`Learner_License_Info_Id`) AS rowCount,"
																	  +" a.`Issue_Date` "
																	  +" FROM"
																	  +" `t_learner_license_dtls` a" 
																	  +" LEFT JOIN t_personal_dtls b" 
																	  +" ON b.`Customer_Id` = a.`Customer_Id`" 
																	  +" WHERE  "
																	  +" a.`Learner_License_Info_Id` = ?";*/
	private static final String GET_PERSONAL_DTLS_FROM_CID	=	"SELECT p.`Customer_Id`,p.`Date_Of_Birth`,COUNT(*) AS rowCount,null as Issue_Date FROM t_personal_dtls p WHERE p.`CID_Number`=?";
	
	
	
	private static final String GET_DOB_FROM_LICENSE_NO	=	"SELECT count(*)rowCount,b.`Date_Of_Birth`,a.`Customer_Id` FROM `t_driving_license_dtls` a "
															+ "LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id` "
															+ "WHERE a.`Driving_License_No`=?";
	
	
	private static final String GET_LATEST_ENDORSED_DATE	="SELECT "
																+ "  (SELECT "
																+ "    c.`Description` "
																+ "  FROM "
																+ "    `t_driving_license_drive_type` a "
																+ "    LEFT JOIN `t_drive_type_master` c "
																+ "      ON a.`Drive_Type_Id` = c.`Drive_Type_Id` "
																+ "  WHERE a.`Customer_Id` = ? "
																+ "    AND a.`Endorsement_Id` IN "
																+ "    (SELECT "
																+ "      b.`Driving_License_Endorsement_Id` "
																+ "    FROM "
																+ "      t_driving_license_endorsement b "
																+ "    WHERE b.`Customer_Id` = ? "
																+ "      AND b.`Endorsed_Date` = latest_issue_date)) driveType,latest_issue_date,rowCount "
																+ "FROM "
																+ "  (SELECT "
																+ "    COUNT(b.`Issue_Date`) AS rowCount, "
																+ "    IF( "
																+ "      (SELECT "
																+ "        MAX(a.`Endorsed_Date`) "
																+ "      FROM "
																+ "        t_driving_license_endorsement a "
																+ "      WHERE a.Customer_Id = ?) > 0, "
																+ "      (SELECT "
																+ "        MAX(a.`Endorsed_Date`) "
																+ "      FROM "
																+ "        t_driving_license_endorsement a "
																+ "      WHERE a.Customer_Id = ?), "
																+ "      b.`Issue_Date` "
																+ "    ) AS latest_issue_date "
																+ "  FROM "
																+ "    `t_driving_license_dtls` b "
																+ "  WHERE b.`Customer_Id` = ?) table1";
	
	private static final String GET_DRIVE_TYPE_FROM_APPLICATION_NO	=	"SELECT a.`Drive_Type_Id` FROM `t_learner_licn_drive_type_application` a WHERE a.`Application_Number`=?";	
	
	private static final String GET_LEARNER_DRIVETYPE	=	"SELECT  a.`Drive_Type_Id`,a.`Drive_Type_Name`,a.`Description` FROM `t_drive_type_master` a WHERE a.`Drive_Type_Category`='O'";
	
	private static final String UPDATE_NEW_LEARNER_LICENSE_APPLICATION	=	"UPDATE `t_learner_license_application` SET  `Certifying_Doctor` = ?,`Remarks` = ?,`Updated_By` = ?, `Updated_On` = ? WHERE `Application_Number` = ?";
	
	private static final String GET_FILE_PATH_FROM_DOC	=	"SELECT a.`Document_Path` FROM t_document_dtls a WHERE a.`Application_Number`=?";
	
	private static final String DELETE_DRIVE_TYPE_FROM_LEARNER_LICENSE_APPLICATION	=	"DELETE FROM  `t_learner_licn_drive_type_application` WHERE `Application_Number` = ?";
	
	private static final String DELETE_FILE_BY_APPLICATION_NO	=	"DELETE FROM t_document_dtls WHERE `Application_Number`=?";
	
	private static final String GET_SERVICE_CODE_FROM_SHORT_DESC	=	"SELECT a.`Service_Id` FROM t_service_master a WHERE a.`Service_Short_Desc`=?";
	
	private static final String INSERT_INTO_T_LEARNER_LICENSE_DRIVE_TYPE_APPLICATION = "INSERT INTO `t_learner_licn_drive_type_application` ( "
		+ "  `Application_Number`, "
		+ "  `Drive_Type_Id` "
		+ ") "
		+ "VALUES " + "  ( " + "    ?, " + "    ? " + "  )";
	
	private static final String UPDATE_REJECTION_REASON	=	"UPDATE  `t_task_dtls` SET  `Task_Remark` = ? WHERE  `Application_Number`=?";
	
	private static final String UPDATE_NEW_VEHICLE_APPLICATION_DTLS	=	" UPDATE `t_vehicle_application`"
																+" SET  "
																+"  Registration_Type=?, "
																+"  Vehicle_Registration_Id=?,"
																+"  Vehicle_Type_Id=?," 
																+"  Vehicle_Company_Id=?," 
																+"  Vehicle_Model_Id=?," 
																+"  Engine_Type_Id=?," 
																+"  Engine_Number=?," 
																+"  Chasis_Number=?, " 
																+"  Color=?, "
																+"  Price=?, "
																+"  Load_Capacity=?, "
																+"  Dealer_Id=?, "
																+"  Seat_Capacity=?," 
																+"  Vehicle_Horse_Power=?," 
																+"  Vehicle_Kilowatt=?, "
																+"  Unladen_Weight=?, "
																+"  Purchase_Date=?, "
																+"  Manufacture_Year=?," 
																+"  Engine_CC=?," 
																+"  Manufactured_Country_Id=?," 
																+"  Purchase_Type=?,Bus_Type_Id=? "
																+" WHERE `Application_Number` = ?;";
	
	private static final String GET_LEANER_LICENSE_DTLS	=	"SELECT a.`Application_Number`,"
															+ "  a.`Receipt_No`,DATE_FORMAT(a.`Issue_Date`,'%d/%m/%Y') AS issue_date, "
															+ "a.`Learner_License_No`,DATE_FORMAT(a.`Receipt_Date`,('%d/%m/%Y')) AS receipt_date, "
															+ "a.`Certifying_Doctor` "
															+ "FROM "
															+ "  `t_learner_license_dtls` a "
															+ "WHERE a.`Learner_License_Info_Id` = ?";
	
	private static final String UPDATE_LEARNER_RENEWAL	=	" UPDATE `t_learner_license_application` "
															+ " SET "
															+ "  `Remarks` = ?, "
															+ "  `Updated_By` = ?,`Renewal_Duration`=? "
															+ " WHERE `Application_Number` = ?";
	
	private static final String UPDATE_NON_COMMERCIAL_ISSUE	=	"UPDATE `t_driving_license_application` "
																+ "SET "
																+ "  `Remarks`=?," 
																+ "`Drive_Type_Id` = ?, "
																+ "  `Updated_By` = ?,Issue_type=?,Renewal_Duration=? "
																+ "WHERE `Application_Number` = ?";
	
	private static final String	UPDATE_COMMERCIAL_ISSUE =	"UPDATE `t_driving_license_application` "
															+ " SET Renewal_Duration=?,"
															+ "  `Drive_Type_Id` = ?, "
															+ "  `Remarks` = ?, "
															+ "  `Created_By` = ? "
															+ " WHERE `Application_Number` = ?;";
	
	private static final String	UPDATE_DRIVING_LICENSE_RENEWAL ="UPDATE `t_driving_license_application` SET `Renewal_Duration`=?,`Remarks` = ?,`Updated_By` = ? WHERE `Application_Number` = ?";

	private static final String UPDATE_LICENSE_ENDORSE	=	"UPDATE `t_driving_license_application` "
															+ "SET "
															+ "  Is_TCB_Endorsement=?,`Drive_Type_Id` = ?, "
															+ "  `Remarks` =	?, "
															+ "  `Updated_By` = ? "
															+ "WHERE `Application_Number` = ?";
	
	private static final String UPDATE_LEARNER_LICENSE_REPLACEMENT	=	"UPDATE `t_learner_license_application` SET `Remarks` = ?,`Updated_By` = ? WHERE `Application_Number` = ?;";
	
	private static final String UPDATE_VEHICLE_TRANSFER	=	"UPDATE "
															+ "  `t_vehicle_application` a "
															+ "SET "
															+ "  a.`Vehicle_Registration_Type` = ?, "
															+ "  a.`Sale_Deed_Amount` = ?, "
															+ "  a.`Sale_Deed_Date` = ?, "
															+ "  a.`Transferee_Customer_Id` = ?, "
															+ "  a.`Updated_By` = ? "
															+ "WHERE a.`Application_Number` = ?";
	
	private static final String UPDATE_VEHICLE_WITHOUT_TRANSFEREE	=	"UPDATE `t_vehicle_application` SET  `Vehicle_Registration_Type` = ?," +
	"`Updated_By` = ? WHERE `Application_Number` = ?";

	private static final String UPDATE_VEHICLE_CONVERSION	=	"UPDATE "	
															+ "  `t_vehicle_application` "
															+ "SET "
															+ "  Conversion_Reason_Id = ?, "
															+ "  Vehicle_Registration_Id = ?, "
															+ "  Region_Id = ?, "
															+ "  Vehicle_Type_Id = ?, "
															+ "  `Updated_By` = ? "
															+ "WHERE `Application_Number` = ?";
	
	private static final String CHECK_IF_NON_COMM_LICENSE_ISSUED	=	"SELECT "
															+ " (SELECT COUNT(*) FROM `t_driving_license_application` b WHERE b.`Customer_Id`=a.`Customer_Id` ) applicationRow, "
															+ " (SELECT COUNT(*) FROM `t_driving_license_dtls` c WHERE c.Customer_Id=a.`Customer_Id` ) drivinggRow "
															+ "FROM "
															+ "  `t_learner_license_dtls` a "
															+ "WHERE a.`Learner_License_Info_Id` = ?";
	 
	private static final String CHECK_IF_COMM_LICENSE_HAS_BEEN_ISSUED = "SELECT (SELECT COUNT(*) FROM `t_driving_license_application` c" +
			" WHERE c.Customer_Id=b.`Customer_Id` AND Driving_license_type='C') AS applicationCount, " +
			" (SELECT COUNT(*) FROM `t_driving_license_dtls` d " +
			" WHERE d.Customer_Id=b.`Customer_Id` AND d.`Driving_License_Type_Id` = 'C') AS licenseCount " +
			" FROM  t_personal_dtls b  WHERE b.Cid_Number = ?  ";
	
	private static final String SELECT_CLAUSE_FOR_DRIVE_TYPE	=	"SELECT "
																+ " b.`Drive_Type_Id`,b.`Drive_Type_Name`"
																+ "FROM ";
	private static final String WHERE_CLAUSE_FOR_DRIVE_TYPE	=	 " a  LEFT JOIN `t_drive_type_master` b "
														+ "    ON b.`Drive_Type_Id` = a.`Drive_Type_Id` "
														+ "WHERE ";
	
	private static final String GET_OFFENCE_LIST="SELECT "
														+ "  a.`Offence_Id`,a.`Is_paid` ,a.`TIN_No`,date_format(a.`Offence_Date`,'%d/%m/%Y') AS offenceDate," 
														+ " f.`Vehicle_Number`,g.`Driving_License_No`,h.`Learner_License_No`,a.Vehicle_Number AS foriegnVehicleNumber,a.License_Number,"
														+ "DATE_FORMAT(a.`Receipt_Date`,'%d/%m/%Y') AS Receipt_Date,a.`Receipt_Number` "
														+ " FROM "
														+ "  `t_offence_information` a "
														+ "  LEFT JOIN `t_offence_information_dtls` b ON b.`Offence_Id`=a.`Offence_Id` "
														+ "  LEFT JOIN `t_vehicle_registration_dtls` f ON f.`Vehicle_Reg_Dtls_Id`=a.`Vehicle_Id` "
														+ "  LEFT JOIN `t_driving_license_dtls` g ON g.`Driving_License_Id`=a.`License_Id` "
														+ "  LEFT JOIN `t_learner_license_dtls` h ON h.`Learner_License_Info_Id`=a.`Learner_License_Id`" 
														+" WHERE a.`Offence_Id`>0 ";
	
	private static final String GET_OFFENCE_DTLS = "SELECT "
												+ "  a.`TIN_No`, "
												+ "  Offence_Id, "
												+ "  DATE_FORMAT(a.`Offence_Date`, '%d/%m/%Y') AS offenceDate, "
												+ "  Offence_Date, "
												+ "  a.Time_Of_Inspection, "
												+ "  a.`Inspected_By`, "
												+ "  a.`Inspection_Type`, "
												+ "  a.`Traffic_Branch`, "
												+ "  a.`Place_Of_Inspection`, "
												+ "  a.`Region_Id`, "
												+ "  IF( "
												+ "    a.`Is_Foreign` = 'Y', "
												+ "    a.`Vehicle_Number`, "
												+ "    (SELECT "
												+ "      `Vehicle_Number` "
												+ "    FROM "
												+ "      `t_vehicle_registration_dtls` "
												+ "    WHERE `Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id`) "
												+ "  ) Vehicle_Number, "
												+ "  IF( "
												+ "    a.`Is_Foreign` = 'Y', "
												+ "    a.`License_Number`, "
												+ "    (SELECT "
												+ "      `Driving_License_No` "
												+ "    FROM "
												+ "      `t_driving_license_dtls` "
												+ "    WHERE `Driving_License_Id` = a.`License_Id`) "
												+ "  ) Driving_License_No, "
												+ "  d.`Learner_License_No`, "
												+ "  a.`Is_Bluebook_Seized`, "
												+ "  a.`Is_License_Seized`, "
												+ "  a.`Remarks`, "
												+ "  (SELECT "
												+ "    SUM(g.`Amount`) "
												+ "  FROM "
												+ "    `t_offence_information_dtls` f "
												+ "    LEFT JOIN `t_offence_type_master` g "
												+ "      ON g.`Offence_Type_Id` = f.`Offence_Type_Id`) AS total_amount "
												+ "FROM "
												+ "  `t_offence_information` a "
												+ "  LEFT JOIN `t_learner_license_dtls` d "
												+ "    ON d.`Learner_License_Info_Id` = a.`Learner_License_Id` "
												+ "WHERE a.`Offence_Id` = ?";
	
	
	/*private static final String GET_OFFENCE_DTLS	=	"SELECT a.`TIN_No`,Offence_Id,"
													+ "  DATE_FORMAT(a.`Offence_Date`, '%d/%m/%Y') AS offenceDate,Offence_Date, "
													+ "  a.Time_Of_Inspection, "
													+ "  a.`Inspected_By`,a.`Inspection_Type`,a.`Traffic_Branch`, "
													+ "  a.`Place_Of_Inspection`,a.`Region_Id`, "
													+ "  b.`Vehicle_Number`, "
													+ "  c.`Driving_License_No`, "
													+ "  d.`Learner_License_No` ,a.`Is_Bluebook_Seized`,a.`Is_License_Seized` "
													+ "  ,a.`Remarks`,(SELECT SUM(g.`Amount`) FROM `t_offence_information_dtls` f LEFT JOIN `t_offence_type_master` g ON g.`Offence_Type_Id`=f.`Offence_Type_Id` ) AS total_amount "
													+ "FROM "
													+ "  `t_offence_information` a "
													+ "  LEFT JOIN `t_vehicle_registration_dtls` b "
													+ "    ON b.`Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id` "
													+ "  LEFT JOIN `t_driving_license_dtls` c "
													+ "    ON c.`Driving_License_Id` = a.`License_Id` "
													+ "  LEFT JOIN `t_learner_license_dtls` d "
													+ "    ON d.`Learner_License_Info_Id` = a.`Learner_License_Id` "
													+ "    WHERE a.`Offence_Id`=?";*/
	
	private static final String GET_OFFENCE_LIST_AGAINST_OFFENCE_ID="SELECT c.`Offence_Name` ,c.`Amount`  FROM `t_offence_information` a "
																+ "LEFT JOIN `t_offence_information_dtls` b ON b.`Offence_Id`=a.`Offence_Id` "
																+ "LEFT JOIN `t_offence_type_master` c  ON c.`Offence_Type_Id`=b.`Offence_Type_Id`"
																+ "WHERE a.`Offence_Id`=?";
	
	//private static final String GET_PERSONAL_DTLS_FROM_CID	=	"SELECT p.`Customer_Id`,p.`Date_Of_Birth`,COUNT(*) AS rowCount,null as Issue_Date FROM t_personal_dtls p WHERE p.`CID_Number`=?";
		
	private static final String GET_VEHICLE_CANCELLATION_DTLS	=	"SELECT Receipt_No,"
																+ "  DATE_FORMAT( "
																+ "    a.`Receipt_Date`, "
																+ "    '%d/%m/%Y' "
																+ "  ) AS Receipt_Date, "
																+ "  DATE_FORMAT( "
																+ "    a.`Cancellation_Date`, "
																+ "    '%d/%m/%Y' "
																+ "  ) AS cancellation_date, "
																+ "  b.`Reason` "
																+ "FROM "
																+ "  `t_vehicle_cancellation_dtls` a "
																+ "  LEFT JOIN `t_reason_master` b "
																+ "    ON b.`Reason_Id` = a.`Cancellation_Reason_Id` "
																+ "WHERE a.`Vehicle_Id` = ?";

	private static final String CHECK_IF_LICENSE_PUNCHED	=	"SELECT COUNT(*) AS rowCount FROM `t_license_punch_dtls` a " +
																"WHERE a.`Driving_License_Id` = ? AND a.`Is_Withdrawn`='N'";
	
	private static final String CHECK_IF_VEHICLE_HAS_OFFENCE	=	"SELECT COUNT(*) AS rowCount FROM `t_offence_information` a WHERE a.`Vehicle_Id`=? AND a.`Is_paid`=?";

	private static final String GET_EXPIRY_FOR_LICENSE_RENEWAL = "SELECT "
																+ " IF( " + "    a.`Driving_License_Id` IN " + "    (SELECT "
																+ "      `License_Id` " + "    FROM "
																+ "      t_driving_license_renewal_dtls "
																+ "    WHERE `License_Id` = ?), " + "    (SELECT "
																+ "      MAX(Expiry_Date) " + "    FROM "
																+ "      t_driving_license_renewal_dtls "
																+ "    WHERE `License_Id` = ?), " + "    a.`Expiry_Date` "
																+ "  ) expiryDate " + "FROM " + "  t_driving_license_dtls a "
																+ "WHERE a.`Driving_License_Id` = ?";
	
	private static final String UPDATE_VEHICLE_RENEWAL	=	"UPDATE "
														+ "  `t_vehicle_application` a "
														+ "SET "
														+ "  a.`Renewal_Duration` = ? "
														+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_AUTHORIZED_DRIVE_TYPE_LIST = "SELECT "
																+ "  b.`Drive_Type_Category`, "
																+ "  b.`Description` "
																+ "FROM "
																+ "  `t_driving_license_drive_type` a "
																+ "  LEFT JOIN t_drive_type_master b "
																+ "    ON a.`Drive_Type_Id` = b.`Drive_Type_Id` "
																+ "WHERE a.`License_Id` = ? GROUP BY a.Drive_Type_Id";
	
	private static final String GET_AUTHORIZED_DRIVE_TYPE_LIST_FOR_COMMERCIAL = "SELECT "
																				+ "  b.`Drive_Type_Category`, "
																				+ "  b.`Description` "
																				+ "FROM "
																				+ "  `t_driving_license_drive_type` a "
																				+ "  LEFT JOIN t_drive_type_master b "
																				+ "    ON a.`Drive_Type_Id` = b.`Drive_Type_Id` "
																				+ " WHERE a.`Customer_Id` = ? GROUP BY a.Drive_Type_Id " 
																				+ " ORDER BY b.`Drive_Type_Category` DESC";
	
	private static final String GET_CUSTOMER_ID_ENDORSEMENT = "SELECT Customer_Id,Driving_License_Type_Id FROM t_driving_license_dtls a WHERE a.`Driving_License_Id`=?";
	
	private static final String GET_TOURIST_DRIVE_TYPE = "SELECT Drive_Type_Id AS HEADER_ID, Drive_Type_Name AS HEADER_NAME FROM t_drive_type_master a WHERE a.`Description`=?";


	private static final String UPDATE_PRINT_PENDING	=	"UPDATE `t_print_dtls` a SET a.`isPrinted`=?,a.`Remarks`=? WHERE a.`Document_Id`=?";

	private static final String GET_PENDING_LIST	=	"SELECT "
												+ "  a.`Pending_Id`, "
												+ "  a.`Remarks`, "
												+ "  DATE_FORMAT(a.`Created_On`, '%d/%m/%Y') AS pendingDate, "
												+ "  (SELECT "
												+ "    b.`name` "
												+ "  FROM "
												+ "    `t_user_master` b "
												+ "  WHERE b.`login_id` = a.`Created_By`) AS userName "
												+ "FROM "
												+ "  `t_pending_task` a "
												+ "WHERE a.`Customer_Id` = ?";
	
	private static final String CLEAR_PENDING_TASK	=	"DELETE FROM `t_pending_task` WHERE `Pending_Id`=?";
	
	private static final String CHECK_EMISSION_TEST_RETESTED	=	"  SELECT COUNT(*) AS rowCount,a.`Test_Result` FROM t_vehicle_emission_test_dtls a WHERE a.`Vehicle_Id`=? ORDER BY a.`Emission_Test_Id` DESC LIMIT 1";
	
	private static final String CHECK_FOR_ISSUE_DATE	=	"SELECT "
														+ "  IF( "
														+ "    z.`Endorsement_Id` = 0, "
														+ "    (SELECT "
														+ "      c.Issue_Date "
														+ "    FROM "
														+ "      `t_driving_license_dtls` c "
														+ "    WHERE c.customer_id=? AND c.`Driving_License_Id`=z.`License_Id` order by c.Issue_Date desc limit 1), "
														+ "    (SELECT "
														+ "      d.`Endorsed_Date` "
														+ "    FROM "
														+ "      `t_driving_license_endorsement` d "
														+ "    WHERE d.`Driving_License_Endorsement_Id`=z.`Endorsement_Id` order by d.`Endorsed_Date` desc limit 1) "
														+ "  ) AS issue_date, "
														+ "  COUNT(*) AS rowCount "
														+ "FROM "
														+ "  `t_driving_license_drive_type` z "
														+ "WHERE z.`Drive_Type_Id` IN "
														+ "  (SELECT "
														+ "    b.`Drive_Type_Id` "
														+ "  FROM "
														+ "    `t_drive_type_master` b "
														+ "  WHERE b.`Description` = ?) "
														+ "  AND z.customer_id=?";
		
		
															/*"SELECT "
															+ "  IF( "
															+ "    z.`Endorsement_Id` = 0, "
															+ "    (SELECT "
															+ "      c.Issue_Date "
															+ "    FROM "
															+ "      `t_driving_license_dtls` c "
															+ "    WHERE c.Driving_License_Id = ?), "
															+ "    (SELECT "
															+ "       d.`Endorsed_Date` "
															+ "    FROM "
															+ "      `t_driving_license_endorsement` d "
															+ "    WHERE d.License_Id = ?) "
															+ "  ) AS issue_date,COUNT(*) AS rowCount "
															+ "FROM "
															+ "  `t_driving_license_drive_type` z "
															+ "WHERE z.`Drive_Type_Id` IN "
															+ "  (SELECT "
															+ "    b.`Drive_Type_Id` "
															+ "  FROM "
															+ "    `t_drive_type_master` b "
															+ "    WHERE b.`Description` = ?) AND z.`License_Id`=?";*/
	private static final String INSERT_INTO_REPORT_CONFIGURATION	=	"INSERT INTO `t_report_configuration` "
																+ "            (`Report_Title`, "
																+ "             `Report_Description`, "
																+ "             `Report_Query`,Report_where_Param, "
																+ "             `Created_By`, "
																+ "             `Created_On`) "
																+ "VALUES (?, "
																+ "        ?, "
																+ "        ?,?, "
																+ "        ?, "
																+ "        CURRENT_TIMESTAMP);";
	private static final String	CHECK_DRIVE_TYPE_EXISTED_IN_LICENSE	=	"SELECT COUNT(*)rowCount FROM `t_driving_license_drive_type` a WHERE a.`Drive_Type_Id`=? AND  a.`Customer_Id`=?";
		
	private static final String GET_DRIVING_LICENSE_ID	=	"SELECT a.`Driving_License_Id` FROM `t_driving_license_dtls` a WHERE a.`Driving_License_No`=?";

	private static final String DELETE_TASK_DTLS_AUDIT	=	"DELETE FROM `t_task_dtls_audit` WHERE `Application_Number`=?;";

	private static final String DELETE_TASK_DTLS	=	"DELETE FROM `t_task_dtls` WHERE `Application_Number`=?;";

	private static final String DELETE_WORK_FLOW_AUDIT	=	"DELETE FROM `t_workflow_dtls_audit` WHERE `Application_Number`=?;";

	private static final String DELETE_WORK_FLOW	=	"DELETE FROM `t_workflow_dtls` WHERE `Application_Number`=?;";

	private static final String DELETE_FROM_PAYMENT_TABLE	=	"DELETE FROM `t_payment_dtls` WHERE `Application_Number`=?;";

	private static final String DELETE_LEARNER_LICENSE_APPLICATION	=	"DELETE FROM `t_learner_license_application` WHERE `Application_Number`=?;";

	private static final String DELETE_DRIVING_LICENSE_APPLICATION	=	"DELETE FROM `t_driving_license_application` WHERE `Application_Number`=?;";

	private static final String DELETE_VEHICLE_APPLICATION	=	"DELETE FROM `t_vehicle_application` WHERE `Application_Number`=?;";
	
	private static final String DELETE_TOP_APPLICATION	=	"DELETE FROM `t_top_application` WHERE `Application_Number`=?";
	
	private static final String GET_LICENSE_CANCELLATION_HISTORY	=	"SELECT "
																		+ "  a.`Cancellation_Date`, "
																		+ "  a.`Cancellation_Reason`, "
																		+ "  a.`Withdrwan_Date`, "
																		+ "  a.`Withdrawn_Reason`, 'Withdrawn' AS STATUS, "
																		+ "   (SELECT b.`Document_Name` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LIC_CAN_', a.`Cancellation_Id`)) Can_Document_Name , "
																		+ "   (SELECT b.`Document_Path` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LIC_CAN_', a.`Cancellation_Id`)) Can_Document_Path, "
																		+ "   (SELECT b.`UUID` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LIC_CAN_', a.`Cancellation_Id`)) Can_UUID, "
																		+ "    "
																		+ "   (SELECT b.`Document_Name` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LICENSE_WITHDRAW_', a.`Driving_License_Cancelled_Audit_Id`)) With_Document_Name , "
																		+ "   (SELECT b.`Document_Path` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LICENSE_WITHDRAW_', a.`Driving_License_Cancelled_Audit_Id`)) With_Document_Path, "
																		+ "   (SELECT b.`UUID` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LICENSE_WITHDRAW_', a.`Driving_License_Cancelled_Audit_Id`)) With_UUID "
																		+ "FROM "
																		+ "  `t_driving_license_cancelled_audit` a "
																		+ "WHERE a.`License_Id` = ? "
																		+ "  UNION "
																		+ "  SELECT "
																		+ " a.`Cancellation_Date`, "
																		+ "  a.`Cancellation_Reason`, "
																		+ "  '' Withdrwan_Date, "
																		+ " '' Withdrawn_Reason, 'Cancelled' AS STATUS, "
																		+ "   "
																		+ "  (SELECT b.`Document_Name` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LIC_CAN_', a.`Driving_License_Cancelled_Id`)) Can_Document_Name, "
																		+ "  (SELECT b.`Document_Path` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LIC_CAN_', a.`Driving_License_Cancelled_Id`)) Can_Document_Path, "
																		+ "  (SELECT b.`UUID` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('LIC_CAN_', a.`Driving_License_Cancelled_Id`)) Can_UUID, "
																		+ "    "
																		+ "  ''  With_Document_Name , "
																		+ "   '' With_Document_Path, "
																		+ "   '' With_UUID "
																		+ "FROM "
																		+ "  `t_driving_license_cancelled` a "
																		+ "WHERE a.`License_Id` = ?";
	
	private static final String GET_VEHICLE_CANCELLATION_HISTORY	=	"SELECT "
																	+ "  a.`Cancellation_Date`, "
																	+ "  c.Reason, "
																	+ "  a.Withdrawn_Date, "
																	+ "  a.`Withdrawn_Reason`, "
																	+ "  'Withdrawn' AS STATUS, "
																	+ "   (SELECT b.`Document_Name` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_CAN_', a.`Vehicle_Cancellation_Id`)) Can_Document_Name , "
																	+ "   (SELECT b.`Document_Path` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_CAN_', a.`Vehicle_Cancellation_Id`)) Can_Document_Path, "
																	+ "   (SELECT b.`UUID` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_CAN_', a.`Vehicle_Cancellation_Id`)) Can_UUID, "
																	+ "    "
																	+ "   (SELECT b.`Document_Name` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_WITHDRAW_', a.`Vehicle_Cancellation_Audit_Id`)) With_Document_Name , "
																	+ "   (SELECT b.`Document_Path` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_WITHDRAW_', a.`Vehicle_Cancellation_Audit_Id`)) With_Document_Path, "
																	+ "   (SELECT b.`UUID` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_WITHDRAW_', a.`Vehicle_Cancellation_Audit_Id`)) With_UUID "
																	+ "FROM "
																	+ "  `t_vehicle_cancellation_audit` a "
																	+ "LEFT JOIN `t_reason_master` c ON a.`Cancellation_Reason_Id`=c.`Reason_Id` "
																	+ "WHERE a.`Vehicle_Id` = ? "
																	+ "  UNION "
																	+ "  SELECT "
																	+ " a.`Cancellation_Date`, "
																	+ "  c.`Reason` , "
																	+ "  '' Withdrwan_Date, "
																	+ " '' Withdrawn_Reason, "
																	+ "  'Cancelled' AS STATUS, "
																	+ "   "
																	+ "  (SELECT b.`Document_Name` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_CAN_', a.`Vehicle_Cancellation_Id`)) Can_Document_Name, "
																	+ "  (SELECT b.`Document_Path` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_CAN_', a.`Vehicle_Cancellation_Id`)) Can_Document_Path, "
																	+ "  (SELECT b.`UUID` FROM `t_document_dtls` b WHERE b.`Application_Number`=CONCAT('VEH_CAN_', a.`Vehicle_Cancellation_Id`)) Can_UUID, "
																	+ "    "
																	+ "  ''  With_Document_Name , "
																	+ "   '' With_Document_Path, "
																	+ "   '' With_UUID "
																	+ "FROM "
																	+ "  `t_vehicle_cancellation_dtls` a "
																	+ "LEFT JOIN `t_reason_master` c ON a.`Cancellation_Reason_Id`=c.`Reason_Id` "
																	+ "WHERE a.`Vehicle_Id` = ?";
	
	private static final String  GET_TOP_DETAILS_HISTORY = "SELECT "
														+ "a.TOP_Number, "
														+ "b.region_name, "
														+ "c.dzongkhag_name, "
														+ "a.Exact_Location, "
														+ "a.Issue_Date, "
														+ "a.Expiry_Date "
														+ "FROM "
														+ "t_top_dtls a "
														+ "LEFT JOIN  t_region_master b ON a.Region_Id = b.region_id "
														+ "LEFT JOIN  t_dzongkhag_master c ON a.Dzongkhag_Id = c.dzongkhag_id "
														+ "WHERE a.Vehicle_Id = ?";
	
	private static final String  GET_TOP_CANCELLATION_HISTORY = "SELECT "
																+ "d.TOP_Number, "
																+ "d.Cancellation_Date, "
																+ "r.Reason, "
																+ "b.region_name, "
																+ "c.base_office_name "
																+ "FROM "
																+ "t_top_dtls a "
																+ "LEFT JOIN t_top_cancellation_dtls d "
																+ "ON a.TOP_Number = d.TOP_Number "
																+ "LEFT JOIN t_region_master b "
																+ "ON b.Region_Id = d.Processed_Region_Id "
																+ "LEFT JOIN t_base_office_master c "
																+ "ON c.base_office_id = d.Processed_Base_Id "
																+ "LEFT JOIN t_reason_master r "
																+ "ON r.Reason_Id = d.Cancellation_Reason_Id "
																+ "WHERE a.Vehicle_Id = ?";
			
	
	private static final String GET_BOOKED_TEST_DTLS	=	"select "
														+ "  a.`Juris_Type_Id`,a.`Drive_Type_Id`, "
														+ "  a.`Test_Location_Id`, "
														+ "  date_format(a.`Test_Date`,'%d/%m/%Y' )testDate "
														+ "from "
														+ "  `t_etest_application` a "
														+ "where a.`Application_Number` = ?";
	private static final String GET_ISSUE_DATE_AND_DOB_FROM_LLNO	=	"SELECT COUNT(*)rowCount,b.`Date_Of_Birth`, a.`Issue_Date` FROM `t_learner_license_dtls` a LEFT JOIN `t_personal_dtls` b ON a.`Customer_Id`=b.`Customer_Id`  WHERE a.`Learner_License_No`=?";
	/*private static final String GET_LICENSE_CANCELLATION_DOC	=	"SELECT b.`Application_Number`,b.`Document_Path` FROM `t_driving_license_cancelled` a "
								+ "RIGHT JOIN `t_document_dtls` b ON b.`Application_Number`= CONCAT('LIC_CAN_', a.`Driving_License_Cancelled_Id`) "
								+ "WHERE a.`License_Id`=?";*/
	
	private static final String CHECK_IF_TOP_VEHICLE_CANCELLED	=	"SELECT "
																	+ "(SELECT COUNT(*) FROM `t_vehicle_cancellation_dtls` b WHERE b.Vehicle_Id=a.`Vehicle_Id`)rowCount "
																	+ "FROM "
																	+ "  `t_top_dtls` a "
																	+ "  LEFT JOIN t_personal_dtls b ON a.`Customer_Id`=b.`Customer_Id` "
																	+ "  WHERE b.`CID_Number`=?";
	
	
	/*private static final String VALIDATE_LEARNER_LICENSE_TEST	=	"SELECT "
																+ "  a.`Customer_Id`, "
																+ "  b.`Date_Of_Birth`, "
																+ "  COUNT(a.`Learner_License_Info_Id`) AS rowCount, "
																+ "  c.`Practical_Test_Status`, "
																+ "  c.`Overall_Marks_Obtained`, "
																+ "  c.`Is_License_Issued`, "
																+ "  c.`Test_Date` "
																+ "FROM "
																+ "  `t_learner_license_dtls` a "
																+ "  LEFT JOIN t_personal_dtls b "
																+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																+ "  LEFT JOIN `t_etest_application` c "
																+ "    ON a.`Learner_License_No` = c.`Learner_License_No` "
																+ "    AND c.`Drive_Type_Id` IN "
																+ "    (SELECT "
																+ "      `Drive_Type_Id` "
																+ "    FROM "
																+ "      `t_drive_type_master` "
																+ "    WHERE `Description` = ?) "
																+ "WHERE a.`Learner_License_Info_Id` = ? "
																+ "ORDER BY c.`Test_Date` DESC "
																+ "LIMIT 1 ;";*/
	
	private static final String GET_PARAM_FOR_NON_COMMERCIAL_CRITERIA="SELECT "
		  +" a.`Customer_Id`,"
		  +" b.`Date_Of_Birth`,"
		  +" COUNT(a.`Learner_License_Info_Id`) AS rowCount,"
		  +" a.`Issue_Date` "
		  +" FROM"
		  +" `t_learner_license_dtls` a" 
		  +" LEFT JOIN t_personal_dtls b" 
		  +" ON b.`Customer_Id` = a.`Customer_Id`" 
		  +" WHERE  "
		  +" a.`Learner_License_Info_Id` = ?";
	
	
	
	private static final String CHECK_DRIVE_TYPE_EXIST	=	"SELECT COUNT(*)rowCount FROM `t_driving_license_drive_type` a WHERE a.`Customer_Id`=? AND a.`Drive_Type_Id`=?";
	
	private static final String GET_OFFENCE_DTLS_FORIEGN="SELECT "
														+ "COUNT(a.Offence_Id) AS rowCount, "
														+ "a.Offence_Id, "
														+ "a.Is_Foreign, "
														+ "a.Vehicle_Number, "
														+ "a.License_Number, "
														+ "a.Mobile_Number, "
														+ "a.Owner_Name, "
														+ "b.Vehicle_Type_Name, "
														+ "a.TIN_No "
														+ "FROM t_offence_information a "
														+ "LEFT JOIN t_vehicle_type_master b ON a.Vehicle_Type=b.Vehicle_Type_Id "
														+ "WHERE a.Vehicle_Number = ? "
														+ "AND a.Is_Foreign = 'Y' "
														+ "AND a.Vehicle_Id IS NULL";
	
	private static final String GET_LICENSE_OFFENCE_DTLS_FORIEGN="SELECT "
														+ "COUNT(Offence_Id) AS rowCount, "
														+ "Offence_Id, "
														+ "Is_Foreign, "
														+ "Vehicle_Number, "
														+ "License_Number, "
														+ "Mobile_Number, "
														+ "Diver_Name, "
														+ "TIN_No "
														+ "FROM "
														+ "t_offence_information "
														+ "WHERE License_Number = ? "
														+ "AND Is_Foreign = 'Y' "
														+ "AND Vehicle_Id IS NULL";
													
	
	private static final String CHECK_LEARNER_SUSPENSE	= "SELECT  COUNT(a.`Learner_License_Suspension_Id`) rowCount " +
			"FROM `t_learner_license_suspension` a WHERE a.`Learner_Id`=? AND a.`Is_Reissued`='N'";
	
	private static final String CHECK_LEARNER_CANCELLATION = "select "
													+ "  count( "
													+ "    a.`Learner_License_Cancelled_Id` "
													+ "  ) rowCount, "
													+ "  date_format(a.`Cancellation_Date`,'%d/%m/%Y') canelledDate, "
													+ "  a.`Cancellation_Reason` reason "
													+ "from "
													+ "  `t_learner_license_cancelled` a "
													+ "where a.`Learner_Id` = ?";
	
	private static final String GET_PERSONAL_DTLS_FOR_ACCIDENT="SELECT "
																+ "  a.`Customer_Id`, "
																+ " CONCAT( "
																+ "    a.First_Name, "
																+ "    a.Middle_Name, "
																+ "    a.Last_Name "
																+ "  ) NAME, "
																+ "  a.`CID_Number`, "
																+ "  a.`Title_Of_Courtesy_Id`, "
															    + "  a.`Occupation_Id`, "
																+ "  a.`Nationality_Id`, "
																+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d/%m/%Y') dob, "
																+ "  a.Gender, "
																+ "  a.`Is_International`, "
																+ "  a.`Permanent_Dzongkhag_Id`, "
																+ "  a.`Permanent_Gewog_Id`, "
																+ "  a.`Permanant_Village_Id`, "
																+ "  a.`Permanent_Country_Id`, "
																+ "  a.`Permanent_Address`, "
																+ "  a.`Present_Dzongkhag_Id`, "
																+ "  a.`Present_Contact_Address`, "
																+ "  a.`Present_Phone_No` "
																+ " FROM "
																+ "  t_personal_dtls a "
																+ " LEFT JOIN t_blood_group_master b ON a.`Blood_Group_Id`=b.`blood_group_id` "
																+ " LEFT JOIN t_dzongkhag_master c ON a.`Permanent_Dzongkhag_Id`=c.`dzongkhag_id` "
																+ " LEFT JOIN t_gewog_master d ON a.`Permanent_Gewog_Id`=d.`Gewog_Id` "
																+ " WHERE a.CID_Number = ?";
	
	private static final String GET_LIST_OF_TOP =  "SELECT " 
			+"(SELECT region_name FROM t_region_master WHERE Region_Id=a.`Processed_Region_Id`)region, "
			+ "(SELECT base_office_name FROM t_base_office_master WHERE Base_Office_Id=a.`Processed_Base_Id`) base,"
			+"DATE_FORMAT(a.`Issue_Date`,'%d-%m-%Y') Issue_Date," 
													+"DATE_FORMAT(a.`Expiry_Date`,'%d-%m-%Y')Expiry_Date,a.`Amount`," +
													"DATE_FORMAT(a.`Receipt_Date`,'%d-%m-%Y')Receipt_Date,a.`Receipt_Number`,"
													+ "  a.`TOP_Number`, "
													+ "  b.`Vehicle_Number`, "
													+ "  (SELECT "
													+ "    `Vehicle_Company_Name` "
													+ "  FROM "
													+ "    `t_vehicle_company_master` "
													+ "  WHERE `Vehicle_Company_Id` = b.`Vehicle_Company_Id`) vehicleCompany, "
													+ "  (SELECT "
													+ "    `Vehicle_Model_Name` "
													+ "  FROM "
													+ "    `t_vehicle_model_master` "
													+ "  WHERE `Vehicle_Model_Id` = b.`Vehicle_Model_Id`) vehicleModel, "
													+ "  b.`Colour`, "
													+ "  b.`Engine_Number`, "
													+ "  b.`Engine_CC`, "
													+ "  b.`Chassis_Number`, "
													+ "  (SELECT "
													+ "    Engine_Name "
													+ "  FROM "
													+ "    `t_engine_type_master` "
													+ "  WHERE `Engine_Type_Id` = b.`Engine_Type_Id`) engineType, "
													+ "  b.`Seat_Capacity`, "
													+ "  d.`Driving_License_No`," 
													+ "(SELECT COUNT(*) FROM `t_top_cancellation_dtls` e WHERE e.top_number=a.`TOP_Number`)topCancelled" 
													+ " FROM "
													+ "  t_top_dtls a "
													+ "  LEFT JOIN t_vehicle_registration_dtls b "
													+ "    ON a.`Vehicle_Id` = b.`Vehicle_Reg_Dtls_Id` "
													+ "  LEFT JOIN t_personal_dtls c "
													+ "    ON a.`Customer_Id` = c.`Customer_Id` "
													+ "  LEFT JOIN `t_driving_license_dtls` d "
													+ "    ON a.`License_Id` = d.`Driving_License_Id` "
													+ " WHERE a.`Customer_Id`=? GROUP BY a.TOP_Number";
	
	private static final String CHECK_DOUBLE_OFFENCE = "SELECT COUNT(*) rowCount FROM t_offence_information a "
														+ "LEFT JOIN t_offence_information_dtls b ON a.`Offence_Id`=b.`Offence_Id` "
														+ " WHERE b.`Offence_Type_Id`=? AND ";
	
	
	private static final String GET_REPEATED_OFFENCE_ID = "SELECT count(*),Offence_Type_Id FROM t_offence_type_master WHERE Description ='REPEATED'";
	
	private static final String GET_FITNESS_VALIDITY = "SELECT IF(a.`Validity`<CURRENT_DATE(),'EXPIRED','VALID') validityStatus,a.`Validity`" +
														" FROM t_vehicle_fitness_test_dtls a  WHERE a.`Vehicle_Id`=? ORDER BY a.`Validity` DESC LIMIT 1";	
	
	private static final String CHECK_PENDING_APPLICATION = "SELECT COUNT(*)rowCount,GROUP_CONCAT(' Remarks : ',Remarks,', Updated By : ',(SELECT a.name FROM t_user_master a  WHERE a.`login_id`=Created_By),' From : ',(SELECT "
														+ "      IF( d.base_office_id IS NULL,(SELECT z.region_name FROM t_region_master z" +
																" WHERE z.region_id=d.region_id), "
														+ " (SELECT z.base_office_name FROM t_base_office_master z WHERE z.base_office_id=d.base_office_id))location "
														+ "    FROM "
														+ "       t_user_master a "
														+ "       LEFT JOIN t_user_role_mapping b "
														+ "         ON a.login_id = b.login_id "
														+ "       LEFT JOIN t_role_master c "
														+ "         ON c.role_id = b.role_id "
														+ "       LEFT JOIN t_user_jurisdiction_mapping d "
														+ "         ON a.login_id = d.login_id "
														+ "     WHERE  a.login_id = created_By LIMIT 1)) "
														+ " remarks FROM t_pending_task a WHERE a.Customer_Id=?";
	
	
	private static final String GET_ACCIDENT_RECORD = "SELECT "
								+ "e.`Vehicle_No`, "
								+ " "
								+ "(SELECT Vehicle_Type_Name FROM t_vehicle_type_master WHERE Vehicle_Type_Id=e.`Vehicle_Type`)Vehicle_Type_Name, "
								+ " "
								+ "b.`Police_Station_Name`, a.`Date_Of_Occurence`,a.`Place_Of_Occurence`,c.`Gewog_Name`,d.`dzongkhag_name`, "
								+ "f.`Country_Name` AS driverNationality, "
								+ "IF(f.`Country_Name`='Bhutan', "
								+ "(SELECT CONCAT(First_Name,' ',Middle_Name,' ',Last_Name) FROM t_personal_dtls WHERE CID_Number=e.`CID`), "
								+ "e.`Driver_Name` "
								+ ")Driver_Name, "
								+ "e.`CID`, "
								+ "e.`LL_DL_No`,a.`Type_Of_Accident`,a.`Nature_Of_Accident` "
								+ ",GROUP_CONCAT(' ',h.`Accident_Name`) accidentCause, "
								+ "(SELECT COUNT(*) FROM t_accident_injured_dtls WHERE Accident_Dtls_Id=a.`Accident_Dtls_Id`) totalInjured, "
								+ "(SELECT COUNT(*) FROM t_accident_killed_dtls WHERE Accident_Dtls_Id=a.`Accident_Dtls_Id`) totalKilled,a.`Remarks` "
								+ "FROM "
								+ "t_accident_dtls a "
								+ "LEFT JOIN t_police_station_master b ON a.`Police_Station_Id`=b.`Police_Station_Id` "
								+ "LEFT JOIN t_gewog_master c ON a.`Gewog_Id`=c.`Gewog_Id` "
								+ "LEFT JOIN t_dzongkhag_master d ON a.`Dzongkhag_Id`=d.`dzongkhag_id` "
								+ "LEFT JOIN t_accident_driver_vehicle_dtls e ON a.`Accident_Dtls_Id`=e.`Accident_Dtls_Id` "
								+ "LEFT JOIN t_country_master f ON e.`Driver_Nationality`=f.`Country_Id` "
								+ "LEFT JOIN t_accident_cause_dtls g ON a.`Accident_Dtls_Id`=g.`Accident_Dtls_Id` "
								+ "LEFT JOIN t_accident_cause_master h ON g.`Accident_Cause_Id`=h.`Accident_Cause_Id` "
								+ "WHERE e.`Vehicle_No`=?";
	
	
	
	
	private static final String GET_ROUTE_PERMIT_DTLS =  "SELECT a.`Permit_No`,a.`Issue_Date`,a.`Validity`,a.`Amount`,a.`Receipt_No`, "
													+ "a.`Receipt_Date`,a.`Journey_Purpose`,a.`Route_Between`,a.`Route_To`, "
													+ "a.`Driver_Name`,a.`Driver_License_No`,a.`Remarks`, "
													+ "(SELECT region_name FROM t_region_master WHERE Region_Id=a.`Processed_Region_Id`)region_name, "
													+ "(SELECT base_office_name FROM t_base_office_master WHERE Base_Office_Id=a.`Processed_Base_Id`) "
													+ "base_office_name "
													+ " FROM t_permit_dtls a "
													+ "WHERE "
													+ "a.`Registration_No`=?";
	
	
	
	private static final String GET_ACCIDENT_VEHICLE =  "SELECT count(*)rowCount,a.`Driver_Name`,a.`Vehicle_No`,a.`LL_DL_No`, "
		+ "(SELECT b.vehicle_type_name FROM t_vehicle_type_master b WHERE b.vehicle_type_id=a.`Vehicle_Type`)Vehicle_Type "
		+ " FROM t_accident_driver_vehicle_dtls a WHERE a.`Vehicle_No`=?";
	
	
	
	private static final String CHECK_DRIVING_TEST_SEAT	=	"SELECT "
													+ "  Max_Applicants, "
													+ "  (SELECT "
													+ "  COUNT(*) rowCount "
													+ "FROM "
													+ "  `t_etest_application_prebooking` b "
													+ "WHERE ( "
													+ "    ( "
													+ "      b.`Status` = 'PENDING' "
													+ "      AND b.`Created_On` > ( "
													+ "        DATE_SUB( "
													+ "          CURRENT_TIMESTAMP, "
													+ "          INTERVAL '0:03' HOUR_MINUTE "
													+ "        ) "
													+ "      ) "
													+ "    ) "
													+ "    OR b.`Status` = 'BOOKED' "
													+ "  ) AND b.`Test_Location_Id` = a.`Test_Location_Id` "
													+ "    AND b.`Test_Date` = a.`Test_Date` "
													+ "    AND b.`Juris_Type_Id` = a.`Juris_Type_Id`) AS totalApplicant "
													+ "FROM "
													+ "  `t_etest_config_master` a "
													+ "WHERE a.`Test_Date` = ? "
													+ "  AND a.`Juris_Type_Id` = ? "
													+ "  AND a.`Test_Location_Id` = ?";
	
	
	
	
	
	private static final String BOOK_DRIVING_TEST	=	"INSERT INTO `t_etest_application` ( "
														+ "  `Application_Number`, "
														+ "  `Learner_License_No`, "
														+ "  `DOB`, "
														+ "  `Drive_Type_Id`, "
														+ "  `Test_Location_Id`, "
														+ "  `Test_Date`, "
														+ "  `Created_By`, "
														+ "   `Test_Type`,Customer_Id,Juris_Type_Id,Issue_Type "
														+ ") "
														+ "VALUES "
														+ "  ( "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?,?,?,? "
														+ "  )";
	
	
	private static final String CHECK_CUSTOMER_VEHICLE_CANCELLED = "SELECT b.Vehicle_Number,d.Vehicle_Type_Name  "
															+ " FROM `t_vehicle_cancellation_dtls` a " 
															+ " left join t_vehicle_registration_dtls b on a.Vehicle_Id=b.Vehicle_Reg_Dtls_Id" 
															+ " left join t_personal_dtls c on a.Customer_Id = c.Customer_Id " 
															+" left join t_vehicle_type_master d on b.Vehicle_Type_Id=d.Vehicle_Type_Id "
															+ " where c.`Personal_Info_Id`=? and date_format(b.Registration_Date,'%Y') >2007 and  "
															+ " a.`Cancellation_Reason_Id` in (select Reason_Id from t_reason_master "
															+ " where Reason_Description= 'OUTSTANDING_VEHICLE')";
	
	private static final String CHECK_CUSTOMER_DRIVING_LICENSE_OUTSTANDING =  " select a.Driving_License_No, "
		+ " if((select count(*) from t_driving_license_renewal_dtls u where u.License_Id=a.Driving_License_Id)>0,(select if(z.Expiry_Date<CURRENT_DATE(),1,0) " 
		+ " from t_driving_license_renewal_dtls z WHERE z.License_Id=a.Driving_License_Id order by z.Expiry_Date desc limit 1)," 
		+ " if(a.Expiry_Date<CURRENT_DATE(),1,0))isExpired "
		+ " from t_driving_license_dtls a "
		+ " left join t_personal_dtls b on a.Customer_Id=b.Customer_Id "
		+ " where b.Personal_Info_Id = ? and a.Driving_License_Id not in " 
		+ " (select v.License_Id from t_driving_license_cancelled v where v.License_Id=a.Driving_License_Id)  order by a.Driving_License_Type_Id limit 1 ";
	
	private static final String CHECK_ORGANISATION_VEHICLE_CANCELLED = "SELECT b.Vehicle_Number,d.Vehicle_Type_Name  "
																	+ "  FROM `t_vehicle_cancellation_dtls` a "
																	+ "  LEFT JOIN t_vehicle_registration_dtls b ON a.Vehicle_Id=b.Vehicle_Reg_Dtls_Id "
																	+ "  LEFT JOIN t_organization_info c ON a.Customer_Id = c.Customer_Id "
																	+ " left join t_vehicle_type_master d on b.Vehicle_Type_Id=d.Vehicle_Type_Id "
																	+ "  WHERE c.Organization_Info_Id=? and date_format(b.Registration_Date,'%Y') >2007 AND "
																	+ "  a.`Cancellation_Reason_Id` IN (SELECT Reason_Id FROM t_reason_master "
																	+ "  WHERE Reason_Description= 'OUTSTANDING_VEHICLE')";
	
	
	private static final String CHECK_VEHICLE_OUTSTANDING_FROM_LICENSE_ID = "SELECT b.Vehicle_Number,e.Vehicle_Type_Name "
		+ "FROM `t_vehicle_cancellation_dtls` a "
		+ "LEFT JOIN t_vehicle_registration_dtls b ON a.Vehicle_Id=b.Vehicle_Reg_Dtls_Id "
		+ "LEFT JOIN t_personal_dtls c ON a.Customer_Id = c.Customer_Id "
		+ "LEFT JOIN t_driving_license_dtls d on c.Customer_Id=d.Customer_Id "
		+ " left join t_vehicle_type_master e on b.Vehicle_Type_Id=e.Vehicle_Type_Id "
		+ "WHERE d.Driving_License_Id=? and date_format(b.Registration_Date,'%Y') >2007 AND "
		+ "a.`Cancellation_Reason_Id` IN (SELECT Reason_Id FROM t_reason_master   WHERE Reason_Description= 'OUTSTANDING_VEHICLE')";
	
	
	
	private static final String CHECK_DOUBLE_TIN = "select count(*)rowCount from t_offence_information a where a.TIN_No=?";
	
	private static final String GET_LIST_COUNTERS_TOKEN = "SELECT a.id,a.`customer_id`,a.`identity_number`,a.`transaction_type`,a.`service_type`,a.token_no,a.`appointment_time_from`,a.`appointment_time_to`  FROM `t_generate_token` a"+
											" left join t_counter_master b on a.counter_id=b.id "+
											" left join t_master_token c on a.token_id=c.id "+
											" where "+
											" b.juris_type=?"+ 
											" and b.juris_id=?"+ 
											" and b.user_id=? "+
											" and c.token_date=CURRENT_DATE"+
											" and a.`appointment_time_from` <= CURRENT_TIME and a.`appointment_time_to`>=CURRENT_TIME"+
											" and a.`is_completed`='N'";
	
}



