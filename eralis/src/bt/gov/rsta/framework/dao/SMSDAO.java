package bt.gov.rsta.framework.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;

/**
 * The Class EmailDAO.
 */
public final class SMSDAO {


	public static void getMailTemplate(SMSModelVO smsObject, Connection conn)
			throws ERALISException

	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(GET_SMS_TEMPLATES_BY_ID);
			pstmt.setString(1, smsObject.getSmsType());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				smsObject.setSmsContent(rs.getString("TEMPLATE_SMS_BODY"));				
			}
		} catch (Exception ee) {
			Log.error("##### Error in SMSDAO[getMailTemplate]",ee);
			throw new ERALISException(ee);
		} finally {
			ConnectionManager.close(null, null, rs, pstmt);
		}

	}

	/** The Constant GET_SMS_TEMPLATES_BY_ID. */
	private static final String GET_SMS_TEMPLATES_BY_ID = new StringBuffer(
			"SELECT TEMPLATE_SMS_BODY,TEMPLATE_ID ")
			.append(" FROM t_sms_template ")
			.append(" WHERE TEMPLATE_MODULE =?").toString();

	/**
	 * Instantiates a new email dao.
	 */
	private SMSDAO() {

	}
}
