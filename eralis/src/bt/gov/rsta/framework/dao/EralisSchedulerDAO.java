package bt.gov.rsta.framework.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.WorkflowManager;
import bt.gov.rsta.framework.vo.WorkflowDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;


public class EralisSchedulerDAO
{
	private static EralisSchedulerDAO schedulerDAO = null;
	private static int duration = 0;
	
	public static EralisSchedulerDAO getInstance()
	{
		if(schedulerDAO == null)
			schedulerDAO = new EralisSchedulerDAO();
		
		duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-notify-duration"));
		
		return schedulerDAO;
	}
	
	public List<NotificationDTO> sendVehicleRenewalNotification() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<NotificationDTO> notificationList = new ArrayList<NotificationDTO>();
		NotificationDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null) 
			{
				pst = conn.prepareStatement(GET_VEHICLE_LIST_DUE_FOR_RENEWAL);
				pst.setInt(1, duration);
				pst.setInt(2, duration);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new NotificationDTO();
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setRenewalDueDate(rs.getString("Expiry_Date"));
					
					pst = conn.prepareStatement(CHECK_IF_VEHICLE_WAS_TRANSFERED);
					pst.setString(1, dto.getVehicleNo());
					rs = pst.executeQuery();
					rs.first();
					int transferCount = rs.getInt("rowCount");
					
					if(transferCount > 0)
					{
						pst = conn.prepareStatement(GET_TRANSFEREE_TYPE_AND_CUSTOMER_CODE);
						pst.setString(1, dto.getVehicleNo());
						rs = pst.executeQuery();
						rs.first();
						
						String transfereeType = rs.getString("Transferee_Type");
						String transfereeCustomerId = rs.getString("Transferee_Customer_Id");
						
						if(transfereeType.equalsIgnoreCase("P"))
						{
							pst = conn.prepareStatement(GET_PERSONAL_DETAILS);
							pst.setString(1, transfereeCustomerId);
							rs = pst.executeQuery();
							rs.first();
							
							dto.setCustomerName(rs.getString("pname"));
							dto.setEmailAddress(rs.getString("Present_Email"));
							dto.setMobileNumber(rs.getString("Present_Phone_No"));
						}
						else if(transfereeType.equalsIgnoreCase("O"))
						{
							String organizationName = null, department = null, ministry = null;
							
							pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS);
							pst.setString(1, transfereeCustomerId);
							rs = pst.executeQuery();
							rs.first();
							
							if(rs.getString("Organization_Name") != null)
								organizationName = rs.getString("Organization_Name");
							else
								organizationName = "";
							
							if(rs.getString("department") != null)
								department = rs.getString("department");
							else
								department = "";
							
							if(rs.getString("ministry") != null)
								ministry = rs.getString("ministry");
							else
								ministry = "";
							
							dto.setCustomerName(organizationName+" "+department+" "+ministry);
							dto.setEmailAddress(rs.getString("Email_Id"));
							dto.setMobileNumber(rs.getString("Phone_Number"));
						}
					}
					else
					{
						pst = conn.prepareStatement(GET_REG_TYPE_CUSTOMER_ID_FROM_VEHICLE_REGISTRATION);
						pst.setString(1, dto.getVehicleNo());
						rs = pst.executeQuery();
						rs.first();
						String customerId = rs.getString("Customer_Id");
						
						if(rs.getString("Vehicle_Registration_Type").equalsIgnoreCase("P"))
						{
							pst = conn.prepareStatement(GET_PERSONAL_DETAILS);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
							
							dto.setCustomerName(rs.getString("pname"));
							dto.setEmailAddress(rs.getString("Present_Email"));
							dto.setMobileNumber(rs.getString("Present_Phone_No"));
						}
						else if(rs.getString("Vehicle_Registration_Type").equalsIgnoreCase("O"))
						{
							String organizationName = null, department = null, ministry = null;
							
							pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS);
							pst.setString(1, customerId);
							rs = pst.executeQuery();
							rs.first();
							
							if(rs.getString("Organization_Name") != null)
								organizationName = rs.getString("Organization_Name");
							else
								organizationName = "";
							
							if(rs.getString("department") != null)
								department = rs.getString("department");
							else
								department = "";
							
							if(rs.getString("ministry") != null)
								ministry = rs.getString("ministry");
							else
								ministry = "";
							
							dto.setCustomerName(organizationName+" "+department+" "+ministry);
							dto.setEmailAddress(rs.getString("Email_Id"));
							dto.setMobileNumber(rs.getString("Phone_Number"));
						}
					}
					
					notificationList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendVehicleRenewalNotification]:"+e);
		}
		
		return notificationList;
	}
	
	public List<NotificationDTO> sendLicenseRenewalNotification() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<NotificationDTO> notificationList = new ArrayList<NotificationDTO>();
		NotificationDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_LICENSE_LIST_DUE_FOR_RENEWAL);
				pst.setInt(1, duration);
				pst.setInt(2, duration);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new NotificationDTO();
					dto.setLicenseNo(rs.getString("Driving_License_No"));
					dto.setRenewalDueDate(rs.getString("Expiry_Date"));
					
					String customerId = rs.getString("Customer_Id");
					
					pst = conn.prepareStatement(GET_PERSONAL_DETAILS);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					
					dto.setCustomerName(rs.getString("pname"));
					dto.setEmailAddress(rs.getString("Present_Email"));
					dto.setMobileNumber(rs.getString("Present_Phone_No"));
					
					notificationList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendLicenseRenewalNotification]:"+e);
		}
		
		return notificationList;
	}
	
	public List<NotificationDTO> sendTOPRenewalNotification() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<NotificationDTO> notificationList = new ArrayList<NotificationDTO>();
		NotificationDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_TOP_LIST_DUE_FOR_RENEWAL);
				pst.setInt(1, duration);
				pst.setInt(2, duration);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new NotificationDTO();
					dto.setTopNo(rs.getString("TOP_Number"));
					dto.setRenewalDueDate(rs.getString("Expiry_Date"));
					
					String customerId = rs.getString("Customer_Id");
					
					String customerIdentifier = customerId.substring(0,1);
					
					if(customerIdentifier.equalsIgnoreCase("P"))
					{
						pst = conn.prepareStatement(GET_PERSONAL_DETAILS);
						pst.setString(1, customerId);
						rs = pst.executeQuery();
						rs.first();
						
						dto.setCustomerName(rs.getString("pname"));
						dto.setEmailAddress(rs.getString("Present_Email"));
						dto.setMobileNumber(rs.getString("Present_Phone_No"));
					}
					else if(customerIdentifier.equalsIgnoreCase("O"))
					{
						String organizationName = null, department = null, ministry = null;
						
						pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS);
						pst.setString(1, customerId);
						rs = pst.executeQuery();
						rs.first();
						
						if(rs.getString("Organization_Name") != null)
							organizationName = rs.getString("Organization_Name");
						else
							organizationName = "";
						
						if(rs.getString("department") != null)
							department = rs.getString("department");
						else
							department = "";
						
						if(rs.getString("ministry") != null)
							ministry = rs.getString("ministry");
						else
							ministry = "";
						
						dto.setCustomerName(organizationName+" "+department+" "+ministry);
						dto.setEmailAddress(rs.getString("Email_Id"));
						dto.setMobileNumber(rs.getString("Phone_Number"));
					}
					
					notificationList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendLicenseRenewalNotification]:"+e);
		}
		
		return notificationList;
	}
	
	public List<NotificationDTO> sendVehicleOutstandingJob() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<NotificationDTO> notificationList = new ArrayList<NotificationDTO>();
		NotificationDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_RENEWAL_OUTSTANDING);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new NotificationDTO();

					/*Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date today = new Date(System.currentTimeMillis());
					
					String expiryDateStr = formatter.format(rs.getDate("mxdate"));
					String todayStr = formatter.format(today);
					
					int yearDiff = EralisCommonUtil.dateDifferenceAsInt(expiryDateStr, todayStr, "Y");
					
					if(yearDiff >= 1)
					{*/
						dto.setCustomerName(rs.getString("ownerName"));
						dto.setMobileNumber(rs.getString("contactNo"));
						dto.setEmailAddress(rs.getString("email"));
						dto.setVehicleNo(rs.getString("Vehicle_Number"));
						dto.setRenewalDueDate(rs.getString("mxdate"));
					//}
					
					notificationList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisSchedulerBusiness[sendLicenseRenewalNotification]:"+e);
		}
		
		return notificationList;
	}
	
	public void revokeSuspensionPeriod() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Date date = new Date(System.currentTimeMillis());
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(REVOKE_DRIVE_TYPE_SUSPENSION);
				pst.setString(1, "Y");
				pst.setDate(2, date);
				pst.setDate(3, date);
				pst.setString(4, "SYSTEM");
				pst.setDate(5, date);
				pst.setString(6, "N");
				pst.executeUpdate();
				
				pst = conn.prepareStatement(REVOKE_DRIVING_LICENSE_SUSPENSION);
				pst.setString(1, "Y");
				pst.setDate(2, date);
				pst.setDate(3, date);
				pst.setString(4, "SYSTEM");
				pst.setDate(5, date);
				pst.setString(6, "N");
				pst.executeUpdate();
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
	}
	
	public String dispatchLicenseApplication() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = null;
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_APPLICATION_PENDING_FOR_DISPATCH);
				rs = pst.executeQuery();
				while(rs.next())
				{
					String applicationNo = rs.getString("Application_Number");
					String serviceDesc = rs.getString("serviceDesc");
					String tableName = null;
					
					if (serviceDesc.equalsIgnoreCase("REGV"))
						tableName = "t_vehicle_registration_dtls";
					else if (serviceDesc.equalsIgnoreCase("RV"))
						tableName = "t_vehicle_renewal_dtls";
					else if (serviceDesc.equalsIgnoreCase("TV"))
						tableName = "t_vehicle_transfer_dtls";
					else if (serviceDesc.equalsIgnoreCase("DV"))
						tableName = "t_vehicle_duplicate_dtls";
					else if (serviceDesc.equalsIgnoreCase("CV"))
						tableName = "t_vehicle_registration_dtls";
					if (serviceDesc.equalsIgnoreCase("INDL") ||
							serviceDesc.equalsIgnoreCase("ICDL"))
						tableName = "t_driving_license_dtls";
					else if (serviceDesc.equalsIgnoreCase("RDL"))
						tableName = "t_driving_license_renewal_dtls";
					else if (serviceDesc.equalsIgnoreCase("DDL"))
						tableName = "t_driving_license_duplicate";
					else if (serviceDesc.equalsIgnoreCase("EDL"))
						tableName = "t_driving_license_endorsement";
					if (serviceDesc.equalsIgnoreCase("ILL"))
						tableName = "t_learner_license_dtls";
					else if (serviceDesc.equalsIgnoreCase("RLL"))
						tableName = "t_learner_license_renewal_dtls";
					else if (serviceDesc.equalsIgnoreCase("DLL"))
						tableName = "t_learner_license_duplicate_dtls";
					if (serviceDesc.equalsIgnoreCase("TOP"))
						tableName = "t_top_dtls";
					else if (serviceDesc.equalsIgnoreCase("TOPR"))
						tableName = "t_top_replacement_dtls";
					
					String query = "UPDATE "
						+ tableName
						+ " a SET a.`Dispatched_By`=?, a.`Dispatched_Date`=CURRENT_TIMESTAMP WHERE a.`Application_Number`=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, "SYSTEM");
					pst.setString(2, applicationNo);
					int count = pst.executeUpdate();
					
					if (count > 0)
					{
						pst = conn.prepareStatement("SELECT Service_Id FROM `t_workflow_dtls` WHERE Application_Number=?");
						pst.setString(1, applicationNo);
						rs = pst.executeQuery();
						rs.first();
						String serviceId = rs.getString("Service_Id");
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId("SYSTEM");
						vo.setActorName("SYSTEM");
						vo.setRoleId("1");
						vo.setRoleName("Administrator");
						vo.setApplicationNo(applicationNo);
						vo.setAssignedGroupId("1");
						vo.setAssignedUserId("SYSTEM");
						vo.setServiceId(serviceId);
						vo.setAssigned_Priv_Id("1");
						new WorkflowManager(conn).logCompletedWorkflow(vo);
						
						result = "SUCCESS";
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return result;
	}
	
	public void cancelOutstandingVehicleJob() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null, pst1 = null;
		ResultSet rs = null;
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_LIST_FOR_CANCELLATION);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					pst1 = conn.prepareStatement(INSERT_INTO_T_VEHICLE_CANCELLATION_DTLS);
					pst1.setString(1, rs.getString("Vehicle_Id"));
					pst1.setString(2, rs.getString("Customer_Id"));
					pst1.setString(3, "OUTSTANDING_VEHICLE");
					pst1.setString(4, "T");
					pst1.setString(5, "administrator");
					pst1.setString(6, "administrator");
					pst1.executeUpdate();
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
	}
	
	/*
	 * -------------------- Queries : Start ----------------------------------------------
	 */
	
	private static final String INSERT_INTO_T_VEHICLE_CANCELLATION_DTLS = "INSERT INTO `t_vehicle_cancellation_dtls` ( "
		+ "  `Vehicle_Id`, "
		+ "  `Customer_Id`, "
		+ "  `Cancellation_Date`, "
		+ "  `Cancellation_Reason_Id`, "
		+ "  `Processed_Region_Id`, "
		+ "  `Created_By`, "
		+ "  `Created_On`, "
		+ "  `Updated_By`, "
		+ "  `Updated_On` "
		+ ") "
		+ "VALUES "
		+ "  ( "
		+ "    ?, "
		+ "    ?, "
		+ "    CURRENT_TIMESTAMP, "
		+ "    (SELECT "
		+ "      `Reason_Id` "
		+ "    FROM "
		+ "      `t_reason_master` "
		+ "    WHERE `Reason_Description` = ?), "
		+ "    (SELECT "
		+ "      region_id "
		+ "    FROM "
		+ "      t_region_master "
		+ "    WHERE `region_desc` = ?), "
		+ "    ?, "
		+ "    CURRENT_TIMESTAMP, "
		+ "    ?, "
		+ "    CURRENT_TIMESTAMP "
		+ "  )";
	
	private static final String GET_APPLICATION_PENDING_FOR_DISPATCH = "SELECT "
																		+ "  Application_Number, "
																		+ "  (SELECT "
																		+ "    `Service_Short_Desc` "
																		+ "  FROM "
																		+ "    `t_service_master` "
																		+ "  WHERE `Service_Id` = a.`Service_Id`) serviceDesc "
																		+ "FROM "
																		+ "  t_workflow_dtls a "
																		+ "WHERE a.`Action_Date` < DATE_ADD(CURDATE(), INTERVAL 7 DAY) "
																		+ "  AND a.`Status_Id` = '2'";
	
	private static final String GET_VEHICLE_LIST_DUE_FOR_RENEWAL = "SELECT "
		+ "  a.`Vehicle_Number`, "
		+ "  DATE_FORMAT(a.`Expiry_Date`, '%d/%m/%Y') Expiry_Date "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE ( "
		+ "    DATEDIFF(a.`Expiry_Date`, CURDATE()) <= ? "
		+ "    AND DATEDIFF(a.`Expiry_Date`, CURDATE()) >= 1 "
		+ "  ) "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "  UNION "
		+ "  (SELECT "
		+ "    (SELECT "
		+ "      `Vehicle_Number` "
		+ "    FROM "
		+ "      t_vehicle_registration_dtls "
		+ "    WHERE Vehicle_Reg_Dtls_Id = b.`Vehicle_Id`) Vehicle_Number, "
		+ "    DATE_FORMAT(b.`Expiry_Date`, '%d/%m/%Y') Expiry_Date "
		+ "  FROM "
		+ "    t_vehicle_renewal_dtls b "
		+ "  WHERE ( "
		+ "      DATEDIFF(b.`Expiry_Date`, CURDATE()) <= ? "
		+ "      AND DATEDIFF(b.`Expiry_Date`, CURDATE()) >= 1 "
		+ "    ) "
		+ "    AND b.`Vehicle_Id` NOT IN "
		+ "    (SELECT "
		+ "      Vehicle_Id "
		+ "    FROM "
		+ "      t_vehicle_cancellation_dtls) "
		+ "  ORDER BY b.`Expiry_Date` DESC "
		+ "  LIMIT 1)";
	
	public static final String CHECK_IF_VEHICLE_WAS_TRANSFERED = "SELECT "
																+ "  COUNT(*) rowCount "
																+ "FROM "
																+ "  t_vehicle_transfer_dtls a "
																+ "WHERE a.`Vehicle_Number` = ?";
	
	public static final String GET_TRANSFEREE_TYPE_AND_CUSTOMER_CODE = "SELECT "
																		+ "  a.`Transferee_Type`, "
																		+ "  a.`Transferee_Customer_Id` "
																		+ "FROM "
																		+ "  t_vehicle_transfer_dtls a "
																		+ "WHERE a.`Vehicle_Number` = ? "
																		+ "ORDER BY a.Transfer_Date DESC LIMIT 1";
	
	public static final String GET_REG_TYPE_CUSTOMER_ID_FROM_VEHICLE_REGISTRATION = "SELECT Vehicle_Registration_Type, Customer_Id FROM t_vehicle_registration_dtls a WHERE a.`Vehicle_Number`=?";
	
	public static final String GET_PERSONAL_DETAILS = "SELECT "
													+ "  CONCAT( "
													+ "    a.`First_Name`, "
													+ "    ' ', "
													+ "    a.`Middle_Name`, "
													+ "    ' ', "
													+ "    a.`Last_Name` "
													+ "  ) pname, "
													+ "  a.`CID_Number`, "
													+ "  a.`Permanant_Village_Id`, "
													+ "  g.`Gewog_Name`, "
													+ "  d.`dzongkhag_name`, "
													+ "  a.`Present_Contact_Address`, "
													+ "  a.`Image_Path`, "
													+ "  a.`Present_Email`, "
													+ "  a.`Present_Phone_No` "
													+ "FROM "
													+ "  t_personal_dtls a "
													+ "  LEFT JOIN t_gewog_master g "
													+ "    ON a.`Permanent_Gewog_Id` = g.`Gewog_Id` "
													+ "  LEFT JOIN t_dzongkhag_master d "
													+ "    ON a.`Permanent_Dzongkhag_Id` = d.`dzongkhag_id` "
													+ "WHERE a.`Customer_Id` = ?";

	public static final String GET_ORGANIZATION_DETAILS = "SELECT "
														+ "  a.`Organization_Name`, "
														+ "  (SELECT "
														+ "    `Department_Name` "
														+ "  FROM "
														+ "    t_department_master "
														+ "  WHERE `Department_Id` = a.`Department_Id`) department, "
														+ "  (SELECT "
														+ "    `Ministry_Name` "
														+ "  FROM "
														+ "    t_ministry_master "
														+ "  WHERE `Ministry_Id` = a.`Ministry_Id`) ministry, "
														+ "  a.`Address`, "
														+ "  a.`Email_Id`, "
														+ "  a.`Phone_Number` "
														+ "FROM "
														+ "  t_organization_info a "
														+ "  LEFT JOIN t_owner_master b "
														+ "    ON a.`Organization_Type_Id` = b.`Owner_Type_Id` "
														+ "  LEFT JOIN t_ministry_master m "
														+ "    ON a.`Ministry_Id` = m.`Ministry_Id` "
														+ "  LEFT JOIN t_department_master d "
														+ "    ON a.`Department_Id` = d.`Department_Id` "
														+ "WHERE a.`Customer_Id` = ?";
	
	private static final String GET_LICENSE_LIST_DUE_FOR_RENEWAL = "SELECT "
																	+ "  a.`Driving_License_No`, "
																	+ "  DATE_FORMAT(a.`Expiry_Date`, '%d/%m/%Y') Expiry_Date, "
																	+ "  a.`Customer_Id` "
																	+ "FROM "
																	+ "  `t_driving_license_dtls` a "
																	+ "WHERE ( "
																	+ "    DATEDIFF(a.`Expiry_Date`, CURDATE()) <= ? "
																	+ "    AND DATEDIFF(a.`Expiry_Date`, CURDATE()) >= 1 "
																	+ "  ) "
																	+ "  AND a.`Driving_License_No` NOT IN "
																	+ "  (SELECT "
																	+ "    License_No "
																	+ "  FROM "
																	+ "    t_driving_license_cancelled) "
																	+ "  AND a.`Driving_License_No` NOT IN "
																	+ "  (SELECT "
																	+ "    License_Number "
																	+ "  FROM "
																	+ "    t_driving_license_suspension "
																	+ "  WHERE Is_Reissued = 'N') "
																	+ "  UNION "
																	+ "  (SELECT "
																	+ "    b.`License_No`, "
																	+ "    DATE_FORMAT(b.`Expiry_Date`, '%d/%m/%Y') Expiry_Date, "
																	+ "    b.`Customer_Id` "
																	+ "  FROM "
																	+ "    `t_driving_license_renewal_dtls` b "
																	+ "  WHERE ( "
																	+ "      DATEDIFF(b.`Expiry_Date`, CURDATE()) <= ? "
																	+ "      AND DATEDIFF(b.`Expiry_Date`, CURDATE()) >= 1 "
																	+ "    ) "
																	+ "    AND b.`License_No` NOT IN "
																	+ "    (SELECT "
																	+ "      License_No "
																	+ "    FROM "
																	+ "      t_driving_license_cancelled) "
																	+ "    AND b.`License_No` NOT IN "
																	+ "    (SELECT "
																	+ "      License_Number "
																	+ "    FROM "
																	+ "      t_driving_license_suspension "
																	+ "    WHERE Is_Reissued = 'N') "
																	+ "  ORDER BY b.`Expiry_Date` DESC "
																	+ "  LIMIT 1)";
	
	private static final String GET_TOP_LIST_DUE_FOR_RENEWAL = "SELECT "
																+ "  a.`TOP_Number`, "
																+ "  DATE_FORMAT(a.`Expiry_Date`, '%d/%m/%Y') Expiry_Date, "
																+ "  a.`Customer_Id` "
																+ "FROM "
																+ "  `t_top_dtls` a "
																+ "WHERE ( "
																+ "    DATEDIFF(a.`Expiry_Date`, CURDATE()) <= ? "
																+ "    AND DATEDIFF(a.`Expiry_Date`, CURDATE()) >= 1 "
																+ "  ) "
																+ "  UNION "
																+ "  (SELECT "
																+ "    b.`TOP_Number`, "
																+ "    DATE_FORMAT(b.`Expiry_Date`, '%d/%m/%Y') Expiry_Date, "
																+ "    b.`Customer_Id` "
																+ "  FROM "
																+ "    `t_top_renewal_dtls` b "
																+ "  WHERE ( "
																+ "      DATEDIFF(b.`Expiry_Date`, CURDATE()) <= ? "
																+ "      AND DATEDIFF(b.`Expiry_Date`, CURDATE()) >= 1 "
																+ "    ) "
																+ "  ORDER BY b.`Expiry_Date` DESC "
																+ "  LIMIT 1)";
	
	private static final String GET_DB_CURDATE = "SELECT CURDATE() AS currentDate;";
	
	private static final String REVOKE_DRIVE_TYPE_SUSPENSION = "UPDATE "
																+ "  t_drive_type_suspension a "
																+ "SET "
																+ "  a.`Is_Reissued` = ?, "
																+ "  a.`Re_Issued_Date` = ?, "
																+ "  Updated_On = ?, "
																+ "  Updated_By = ? "
																+ "WHERE a.`End_Date` = ? "
																+ "  AND a.`Is_Reissued` = ?";
	
	private static final String REVOKE_DRIVING_LICENSE_SUSPENSION = "UPDATE "
																	+ "  `t_driving_license_suspension` a "
																	+ "SET "
																	+ "  a.`Is_Reissued` = ?, "
																	+ "  a.`Re_Issue_Date` = ?, "
																	+ "  Updated_On = ?, "
																	+ "  Updated_By = ? "
																	+ "WHERE a.`End_Date` = ? "
																	+ "  AND a.`Is_Reissued` = ?";
	
	/*private static final String GET_VEHICLE_RENEWAL_OUTSTANDING = "SELECT "
		+ "  a.`Vehicle_Reg_Dtls_Id` AS vehicle_id, "
		+ "  a.`Customer_Id`, "
		+ "  a.Vehicle_Number, "
		+ "  IF( "
		+ "    a.Vehicle_Registration_Type = 'P', "
		+ "    (SELECT "
		+ "      CONCAT( "
		+ "        First_Name, "
		+ "        ' ', "
		+ "        Middle_Name, "
		+ "        ' ', "
		+ "        Last_Name "
		+ "      ) "
		+ "    FROM "
		+ "      t_personal_dtls "
		+ "    WHERE Customer_Id = a.Customer_Id), "
		+ "    (SELECT "
		+ "      Organization_Name "
		+ "    FROM "
		+ "      t_organization_info "
		+ "    WHERE Customer_Id = a.Customer_Id) "
		+ "  ) ownerName, "
		+ "  IF( "
		+ "    a.Vehicle_Registration_Type = 'P', "
		+ "    (SELECT "
		+ "      Present_Phone_No "
		+ "    FROM "
		+ "      t_personal_dtls "
		+ "    WHERE Customer_Id = a.Customer_Id), "
		+ "    (SELECT "
		+ "      Phone_Number "
		+ "    FROM "
		+ "      t_organization_info "
		+ "    WHERE Customer_Id = a.Customer_Id) "
		+ "  ) contactNo, "
		+ "  IF( "
		+ "    a.Vehicle_Registration_Type = 'P', "
		+ "    (SELECT "
		+ "      `Present_Email` "
		+ "    FROM "
		+ "      t_personal_dtls "
		+ "    WHERE Customer_Id = a.Customer_Id), "
		+ "    (SELECT "
		+ "      `Email_Id` "
		+ "    FROM "
		+ "      t_organization_info "
		+ "    WHERE Customer_Id = a.Customer_Id) "
		+ "  ) email, "
		+ "  DATE_FORMAT(b.mxdate, '%d/%m/%Y') mxdate, mxdate AS expiryDate "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "  INNER JOIN "
		+ "    (SELECT "
		+ "      Vehicle_Id, "
		+ "      MAX(Expiry_Date) mxdate "
		+ "    FROM "
		+ "      t_vehicle_renewal_dtls "
		+ "    GROUP BY Vehicle_Id "
		+ "    ORDER BY Expiry_Date) b "
		+ "    ON a.Vehicle_Reg_Dtls_Id = b.Vehicle_Id "
		+ "    AND b.mxdate <= CURRENT_DATE "
		+ "    AND a.Vehicle_Reg_Dtls_Id NOT IN "
		+ "    (SELECT "
		+ "      Vehicle_Id "
		+ "    FROM "
		+ "      t_vehicle_cancellation_dtls)";*/
	
	private static final String GET_VEHICLE_RENEWAL_OUTSTANDING = "SELECT "
		+ "  a.Vehicle_Id, "
		+ "  a.Customer_Id, "
		+ "  b.Vehicle_Number, "
		+ "  IF( "
		+ "    b.Vehicle_Registration_Type = 'P', "
		+ "    (SELECT "
		+ "      CONCAT( "
		+ "        First_Name, "
		+ "        ' ', "
		+ "        Middle_Name, "
		+ "        ' ', "
		+ "        Last_Name "
		+ "      ) "
		+ "    FROM "
		+ "      t_personal_dtls "
		+ "    WHERE Customer_Id = b.Customer_Id), "
		+ "    (SELECT "
		+ "      Organization_Name "
		+ "    FROM "
		+ "      t_organization_info "
		+ "    WHERE Customer_Id = b.Customer_Id) "
		+ "  ) ownerName, "
		+ "  IF( "
		+ "    b.Vehicle_Registration_Type = 'P', "
		+ "    (SELECT "
		+ "      Present_Phone_No "
		+ "    FROM "
		+ "      t_personal_dtls "
		+ "    WHERE Customer_Id = b.Customer_Id), "
		+ "    (SELECT "
		+ "      Phone_Number "
		+ "    FROM "
		+ "      t_organization_info "
		+ "    WHERE Customer_Id = b.Customer_Id) "
		+ "  ) contactNo, "
		+ "  IF( "
		+ "    b.Vehicle_Registration_Type = 'P', "
		+ "    (SELECT "
		+ "      `Present_Email` "
		+ "    FROM "
		+ "      t_personal_dtls "
		+ "    WHERE Customer_Id = b.Customer_Id), "
		+ "    (SELECT "
		+ "      `Email_Id` "
		+ "    FROM "
		+ "      t_organization_info "
		+ "    WHERE Customer_Id = b.Customer_Id) "
		+ "  ) email, "
		+ "  DATE_FORMAT(MAX(a.Expiry_Date), '%d/%m/%Y') mxdate "
		+ "FROM "
		+ "  t_vehicle_renewal_dtls a "
		+ "  LEFT JOIN t_vehicle_registration_dtls b "
		+ "    ON a.Vehicle_Id = b.Vehicle_Reg_Dtls_Id "
		+ "WHERE "
		+ "  (SELECT "
		+ "    MAX(Expiry_Date) mxdate "
		+ "  FROM "
		+ "    t_vehicle_renewal_dtls "
		+ "  WHERE Vehicle_Id = a.Vehicle_Id) < DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR) "
		+ "  AND a.Vehicle_Id NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls "
		+ "  WHERE Vehicle_Id = a.Vehicle_Id) "
		+ "  AND a.Vehicle_Id IN "
		+ "  (SELECT "
		+ "    Vehicle_Reg_Dtls_Id "
		+ "  FROM "
		+ "    t_vehicle_registration_dtls) "
		+ "GROUP BY a.Vehicle_Id "
		+ "ORDER BY a.Expiry_Date DESC";
	
	private static final String GET_VEHICLE_LIST_FOR_CANCELLATION = "SELECT "
		+ "  a.Vehicle_Id, "
		+ "  DATE_FORMAT(MAX(a.Expiry_Date), '%d/%m/%Y') expiryDate, "
		+ "  a.Customer_Id "
		+ "FROM "
		+ "  t_vehicle_renewal_dtls a "
		+ "WHERE "
		+ "  (SELECT "
		+ "    MAX(Expiry_Date) mxdate "
		+ "  FROM "
		+ "    t_vehicle_renewal_dtls "
		+ "  WHERE Vehicle_Id = a.Vehicle_Id) < DATE_SUB(CURRENT_DATE, INTERVAL 2 YEAR) "
		+ "  AND a.Vehicle_Id NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls "
		+ "  WHERE Vehicle_Id = a.Vehicle_Id) "
		+ "  AND a.Vehicle_Id IN "
		+ "  (SELECT "
		+ "    Vehicle_Reg_Dtls_Id "
		+ "  FROM "
		+ "    t_vehicle_registration_dtls) "
		+ "GROUP BY a.Vehicle_Id "
		+ "ORDER BY a.Expiry_Date DESC";
}
