package bt.gov.rsta.framework.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.dto.administration.AdministrationDTO;
import bt.gov.rsta.framework.dto.EmailConfigDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EmailUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class EmailDAO 
{
	/**
	 * Gets email template from DB for a given Template module name
	 * 
	 * @param emailObj
	 * @param conn
	 * @throws Exception
	 */
	public static void getMailTemplate(EmailModelVO emailObj, Connection conn) throws ERALISException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			
			pstmt = conn.prepareStatement(GET_EMAIL_TEMPLATES_BY_ID);
			pstmt.setString(1, emailObj.getMailType());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				emailObj.setMailBody(rs.getString("TEMPLATE_MAIL_BODY"));
				emailObj.setMailSubject(rs.getString("TEMPLATE_SUBJECT"));
				emailObj.setTemplateSerialNumber(rs.getString("TEMPLATE_ID"));
			}
		} catch (Exception ee) {
			Log.error("######## Error in EmailDAO[getMailTemplate] :", ee);
			throw new ERALISException(ee);
		} finally {
			ConnectionManager.close(null, null, rs, pstmt);
		}
	}
	
	/**
	 * This method inserts mail details into DB after sending the mail to
	 * recipients
	 * 
	 * @param emailObj
	 * @param conn
	 * @throws Exception
	 */
	public static String insertMail(EmailModelVO emailObj, Connection conn)
			throws ERALISException

	{
		Log.info("Inside EmailDAO::insertMail");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String emailSlNo = "0";
		try {
			pstmt = conn.prepareStatement(INSERT_EMAIL_INTO_DB,
					PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, emailObj.getTemplateSerialNumber());
			pstmt.setString(2, emailObj.getMailSubject());
			pstmt.setString(3, emailObj.getMailBody());
			String recipentsList = EmailUtil
					.prepareCommaSepStrFromList(emailObj.getRecipentList());
			pstmt.setString(4, recipentsList);
			pstmt.setString(5, "N");
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			while (rs.next()) {
				emailSlNo = rs.getString(1);
			}
		} catch (Exception ee) {
			Log.error(ee.getMessage());
			throw new ERALISException(ee);
		} finally {
			ConnectionManager.close(null, null, rs, pstmt);
		}
		return emailSlNo;

	}
	
	public static void updateMail(EmailModelVO emailObj, Connection conn) throws ERALISException
	{
		Log.info("Inside EmailDAO::updateMail");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(UPDATE_MAIL_TO_SENT);
			pstmt.setString(1, "Y");
			pstmt.setString(2, emailObj.getEmailSlNo());
			pstmt.executeUpdate();
		} catch (Exception ee) {
			Log.error(ee.getMessage());
			throw new ERALISException(ee);
		} finally {
			ConnectionManager.close(null, null, rs, pstmt);
		}
	}
	
	public static List<EmailModelVO> getToBeSentEmailList(Connection conn)
	throws ERALISException {
		List<EmailModelVO> toBeSentEmailList = new ArrayList<EmailModelVO>();
		
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = GET_TO_BE_EMAIL_SEND_LIST;
		Log.info("Executing query::GET_TO_BE_EMAIL_SEND_LIST::"
				+ GET_TO_BE_EMAIL_SEND_LIST);
		try {
			prepareStatement = conn.prepareStatement(query);
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				EmailModelVO emailVO = new EmailModelVO();
				emailVO.setEmailSlNo(resultSet.getString("MAIL_SL_NO"));
				emailVO.setMailSubject(resultSet.getString("MAIL_SUBJECT"));
				emailVO.setTemplateSerialNumber(resultSet
						.getString("TEMPLATE_ID"));
				emailVO.setMailBody(resultSet.getString("MAIL_BODY"));
				emailVO.setEmailId(resultSet.getString("MAIL_RECEPIENTS"));
		
				toBeSentEmailList.add(emailVO);
			}
		
		} catch (Exception e) {
			throw new ERALISException(
					"At EmailDAO.getToBeSentEmailList()::Exception : "
							+ e.getMessage());
		} finally {
			ConnectionManager.close(null, null, resultSet, prepareStatement);
		}
		return toBeSentEmailList;
		}
	
	
	public static EmailConfigDTO getEmailConfigDetails(Connection conn) throws ERALISException, ERALISSystemException
	{
		EmailConfigDTO dto = new EmailConfigDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = GET_EMAIL_CONFIG_DETAILS;
		
		try
		{
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				/*if(rs.getString("Config_Type").equalsIgnoreCase(HOST_ADDRESS))
					dto.setSmtpHost(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(SMTP_PORT))
					dto.setSmtpPort(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(DEBUG_FLAG))
					dto.setMailDebug(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(STARTTLS_FLAG))
					dto.setSmtpStartTLS(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(FROM_ADDRESS))
					dto.setFromAddress(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(USER_ADDRESS))
					dto.setUser(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(USER_PASSWORD))
					dto.setPassword(rs.getString("Configuration"));
				if(rs.getString("Config_Type").equalsIgnoreCase(SMTP_AUTH))
					dto.setSmtpAuth(rs.getString("Configuration"));*/
				
				dto.setSmtpHost(rs.getString(SMTP_HOST));
				dto.setSmtpPort(rs.getString(SMTP_PORT));
				dto.setMailDebug(rs.getString(DEBUG_FLAG));
				dto.setSmtpStartTLS(rs.getString(STARTTLS_ENABLE_FLAG));
				dto.setFromAddress(rs.getString(FROM_ADDRESS));
				dto.setUser(rs.getString(MAIL_USER_ADDRESS));
				dto.setPassword(rs.getString(MAIL_USER_PASSWORD));
				dto.setSmtpAuth(rs.getString(SMTP_AUTH_FLAG));
				dto.setConfigId(rs.getString(CONFIG_ID));
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException(
					"At EmailDAO.getEmailConfigDetails()::Exception : "
							+ e.getMessage());
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return dto;
	}
	
	public static String updateMailConfiguration(AdministrationDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "MAIL_UPDATE_FAILURE";
		
		try
		{
			pst = conn.prepareStatement(UPDATE_EMAIL_CONFIG_DETAILS);
			pst.setString(1, dto.getSmtpHost());
			pst.setString(2, dto.getSmtpPort());
			pst.setString(3, dto.getDebugFlag());
			pst.setString(4, dto.getTtlsFlag());
			pst.setString(5, dto.getSenderAddress());
			pst.setString(6, dto.getSenderAddress());
			pst.setString(7, dto.getSenderPassword());
			pst.setString(8, dto.getSmtpAuthFlag());
			pst.setString(9, dto.getMailConfigId());
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "MAIL_UPDATE_SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "MAIL_UPDATE_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"At EmailDAO.updateMailConfiguration()::Exception : "
							+ e.getMessage());
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	/*private static final String HOST_ADDRESS = "HOST_ADDRESS";
	private static final String SMTP_PORT = "SMTP_PORT";
	private static final String DEBUG_FLAG = "DEBUG_FLAG";
	private static final String STARTTLS_FLAG = "STARTTLS_ENABLE_FLAG";
	private static final String FROM_ADDRESS = "FROM_ADDRESS";
	private static final String USER_ADDRESS = "MAIL_USER_ADDRESS";
	private static final String USER_PASSWORD = "MAIL_USER_PASSWORD";
	private static final String SMTP_AUTH = "SMTP_AUTH_FLAG";*/
	
	private static final String CONFIG_ID = "Mail_Config_Id";
	private static final String SMTP_HOST = "SMTP_HOST";
	private static final String SMTP_PORT = "SMTP_PORT";
	private static final String DEBUG_FLAG = "DEBUG_FLAG";
	private static final String STARTTLS_ENABLE_FLAG = "STARTTLS_ENABLE_FLAG";
	private static final String FROM_ADDRESS = "FROM_ADDRESS";
	private static final String MAIL_USER_ADDRESS = "MAIL_USER_ADDRESS";
	private static final String MAIL_USER_PASSWORD = "MAIL_USER_PASSWORD";
	private static final String SMTP_AUTH_FLAG = "SMTP_AUTH_FLAG";
	
	
	private static final String INSERT_EMAIL_INTO_DB = new StringBuffer(
	" INSERT INTO t_mails ")
	.append("(")
	.append("TEMPLATE_ID, MAIL_SUBJECT, MAIL_BODY,MAIL_RECEPIENTS,MAIL_SENT_DATE,IS_MAIL_SENT ")
	.append(")").append("values ").append("(").append("?,?,?,?,SYSDATE(),? ")
	.append(")").toString();
	
	private static final String GET_EMAIL_TEMPLATES_BY_ID = new StringBuffer(
	"SELECT TEMPLATE_SUBJECT, TEMPLATE_MAIL_BODY,TEMPLATE_ID ")
	.append(" FROM t_mail_template ")
	.append(" WHERE TEMPLATE_MODULE =?").toString();
	
	private static final String UPDATE_MAIL_TO_SENT = "UPDATE `t_mails` SET IS_MAIL_SENT = ?, MAIL_SENT_DATE = SYSDATE() WHERE MAIL_SL_NO = ?";
	
	/** The Constant GET_TO_BE_EMAIL_SEND_LIST. */
	private static final String GET_TO_BE_EMAIL_SEND_LIST = new StringBuffer(
			"SELECT MAIL_SL_NO,MAIL_SUBJECT,TEMPLATE_ID,MAIL_BODY,MAIL_RECEPIENTS")
			.append(" FROM t_mails ").append(" WHERE IS_MAIL_SENT ='N'")
			.toString();
	
	//private static final String GET_EMAIL_CONFIG_DETAILS = "SELECT Config_Type, Configuration FROM t_email_config_master";
	private static final String GET_EMAIL_CONFIG_DETAILS = "SELECT * FROM `t_mail_config_master`";
	
	private static final String UPDATE_EMAIL_CONFIG_DETAILS = "UPDATE "
																+ "  `t_mail_config_master` "
																+ "SET "
																+ "  `SMTP_HOST` = ?, "
																+ "  `SMTP_PORT` = ?, "
																+ "  `DEBUG_FLAG` = ?, "
																+ "  `STARTTLS_ENABLE_FLAG` = ?, "
																+ "  `FROM_ADDRESS` = ?, "
																+ "  `MAIL_USER_ADDRESS` = ?, "
																+ "  `MAIL_USER_PASSWORD` = ?, "
																+ "  `SMTP_AUTH_FLAG` = ? "
																+ "WHERE `Mail_Config_Id` = ?";
}
