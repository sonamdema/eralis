package bt.gov.rsta.framework.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;

import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class PopulateDropDownDAO 
{
	private static PopulateDropDownDAO populateDropdownDAO;
	
	public static PopulateDropDownDAO getInstance() {
		if (populateDropdownDAO == null) {
			populateDropdownDAO = new PopulateDropDownDAO();
		}
		return populateDropdownDAO;
	}
	
	public List<DropDownDTO> getDropDownList(String fieldConstructor, String parentId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<DropDownDTO> dropDownList = new ArrayList<DropDownDTO>();
		
		if(Constants.SECURITY_QUESTION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_SECURITY_QUESTION_DROPDOWN_QUERY;
		else if(Constants.REGION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_REGION_DROPDOWN_QUERY;
		else if(Constants.ROLE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_ROLE_DROPDOWN_QUERY; 
		else if(Constants.DZONGKHAG_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_DZONGKHAG_DROPDOWN_QUERY;
		else if(Constants.DZONGKHAGALL_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_ALL_DZONGKHAG_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_TYPE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_TYPE_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_MODEL_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_MODEL_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_COMPANY_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_COMPANY_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_CANCELLATION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_CANCELLATION_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_CONVERSION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_CONVERSION_DROPDOWN_QUERY;
		else if(Constants.GEWOG_DROP_DOWN_ALL_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_ALL_GEWOG_DROPDOWN_QUERY;
		else if(Constants.ETEST_QUESTION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_ETEST_QUESTION_DROPDOWN_QUERY;
		else if(Constants.TITLE_OF_COURTESY_TYPE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_TITLE_OF_COURTESY_TYPE_DROPDOWN_QUERY;
		else if(Constants.OCCUPATION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = OCCUPATION_DROPDOWN_QUERY;
		else if(Constants.NATIONALITY_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_NATIONALITY_DROPDOWN_QUERY;
		else if(Constants.GEWOG_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_GEWOG_DROPDOWN_QUERY;
		else if(Constants.MINISTRY_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_MINISTRY_DROPDOWN_QUERY;
		else if(Constants.COUNTRY_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_COUNTRY_DROPDOWN_QUERY;
		else if(Constants.DZONGKHAG_DROP_DOWN_ALL_FIELD_CONSTRUCTOR.equals(fieldConstructor))
			query = GET_DZONGKHAG_ALL_DROPDOWN_QUERY;
		else if(Constants.BLOOD_GROUP_DROP_DOWN_FIELD_CONSTRUCTOR.equals(fieldConstructor))
			query = BLOOD_GROUP_DROPDOWN_QUERY;
		else if(Constants.ENGINE_TYPE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_ENGINE_TYPE_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_CODE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_CODE_DROPDOWN_QUERY;
		else if(Constants.DEALERS_NAME_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_DEALERS_NAME_DROPDOWN_QUERY;
		else if(Constants.BASE_OFFICE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_BASE_OFFICE_DROPDOWN_QUERY;
		else if(Constants.ACCIDENT_CAUSE_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_ACCIDENT_CAUSE_DROPDOWN_QUERY;
		else if(Constants.VEHICLE_TYPE_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VEHICLE_TYPE_DROPDOWN_QUERY;
		else if(Constants.OFFENCE_LIST_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_OFFENCE_LIST_DROPDOWN_QUERY;
		else if(Constants.MANUFACTURE_COUNTRY_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_MANUFACTURE_COUNTRY_DROPDOWN_QUERY;
		else if(Constants.OWNER_TYPE_LIST_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_OWNER_TYPE_LIST_DROPDOWN_QUERY;
		else if(Constants.DEPARTMENT_LIST_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_DEPARTMENT_DROPDOWN_QUERY;
		else if(Constants.DRIVE_TYPE_LIST_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_DRIVE_TYPE_LIST;
		else if(Constants.ROUTE_LIST.equalsIgnoreCase(fieldConstructor))
			query = ROUTE_LIST;
		else if(Constants.VILLAGE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = VILLAGE_LIST;
		else if(Constants.GET_BASE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_BASE_OFFICE;
		else if(Constants.VECHILE_PREFIX_LIST_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_VECHILE_PREFIX_LIST;
		else if(Constants.HYPOTHECATED_TO_LIST_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_HYPOTHECATED_TO_LIST;
		else if(Constants.GET_LEARNER_DRIVE_TYPE.equalsIgnoreCase(fieldConstructor))
			query	=	GET_LEARNER_DRIVE_TYPE;
		else if(Constants.GET_REGISTRATION_LIST.equalsIgnoreCase(fieldConstructor))
			query	=	GET_REGISTRATION_LIST;
		else if(Constants.POLICE_STATION_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
			query = GET_POLICE_STATION_LIST;
		else if(Constants.GET_BASE_OFFICE_ALL_LIST.equalsIgnoreCase(fieldConstructor))
			query = GET_BASE_OFFICE_ALL_DROPDOWN_QUERY;
		else if(Constants.GET_JURIS_TYPE_LIST.equalsIgnoreCase(fieldConstructor))
			query = GET_JURISDICTION_TYPE_LIST;
		else if(Constants.GET_PRIVATE_COMPANY_LIST.equalsIgnoreCase(fieldConstructor))
			query = GET_PRIVATE_COMPANY_LIST;
		else if(Constants.GET_ORDINARY_DRIVE_TYPE_LIST.equalsIgnoreCase(fieldConstructor))
			query	=	GET_ORDINARY_DRIVE_TYPE;
		else if(Constants.GET_COMMERCIAL_DRIVE_TYPE.equalsIgnoreCase(fieldConstructor))
			query	=	GET_COMMERCIAL_DRIVE_TYPE;
		else if(Constants.GET_ALL_VEHICLE_MODELS.equalsIgnoreCase(fieldConstructor))
			query = GET_ALL_VEHICLE_MODELS;
		else if(Constants.GET_REPORT_TITLE.equalsIgnoreCase(fieldConstructor))
			query = GET_REPORT_TITLE;
		else if(Constants.BUS_TYPE_LIST.equalsIgnoreCase(fieldConstructor))
			query = BUS_TYPE_LIST;
		else if(Constants.ONLINE_PAYMENT_BASE_OFFICE_LIST.equalsIgnoreCase(fieldConstructor))
			query = ONLINE_PAYMENT_BASE_OFFICE_LIST;
		else if(Constants.COLOUR_LIST.equalsIgnoreCase(fieldConstructor))
			query = COLOUR_LIST;
		else if(Constants.GET_REPORT_TITLE_FOR_HEADOFFICE.equalsIgnoreCase(fieldConstructor))
			query = GET_REPORT_TITLE_FOR_HEADOFFICE;
		else if(Constants.GET_DIPLOMAT_LIST.equalsIgnoreCase(fieldConstructor))
			query = DIPLOMAT_LIST;
		else if(Constants.GET_ONLINE_LOCATION_LIST.equalsIgnoreCase(fieldConstructor))
			query = GET_ONINE_LOCATION_LIST;
		else if(Constants.GET_NATIONALITY_LIST.equalsIgnoreCase(fieldConstructor))
			query = GET_NATIONALITY_LIST;
		else if(Constants.POLICE_DIVISION_LIST.equalsIgnoreCase(fieldConstructor))
			query = POLICE_DIVISION_LIST;
		else if(Constants.GET_ACCIDENT_INFORMER_LIST.equalsIgnoreCase(fieldConstructor))
			query = GET_ACCIDENT_INFORMER_LIST;
		else if(Constants.GET_POLICE_STATION_LIST_AGAINST_DIV.equalsIgnoreCase(fieldConstructor))
			query = GET_POLICE_STATION_LIST_AGAINST_DIV;
		else if(Constants.ACCIDENT_CAUSE_MASTER.equalsIgnoreCase(fieldConstructor))
			query	=	ACCIDENT_CAUSE_MASTER;
		else if(Constants.PASSANGER_BUS_CATEGORY_LIST.equalsIgnoreCase(fieldConstructor))
			query	=	PASSANGER_BUS_CATEGORY_LIST;
		else if(Constants.ORGANISATION_LIST.equalsIgnoreCase(fieldConstructor))
			query	=	ORGANISATION_LIST;
		else if(Constants.GET_REPORT_TITLE_FOR_TRAFFIC.equalsIgnoreCase(fieldConstructor))
			query	=	GET_REPORT_TITLE_FOR_TRAFFIC;
		else if(Constants.GET_REPORT_TITLE_FOR_EMISSION.equalsIgnoreCase(fieldConstructor))
			query	=	GET_REPORT_TITLE_FOR_EMISSION;
		else if(Constants.JURISDICTION_USER.equalsIgnoreCase(fieldConstructor))
			query	=	GET_JURISDICTION_USER_LIST;
		else if(Constants.GET_DIPLOMAT_LIST_FOR_ORGANISTAION.equalsIgnoreCase(fieldConstructor))
			query	=	GET_DIPLOMAT_LIST_FOR_ORGANISTAION;
		else if(Constants.GET_ALL_ROUTE_LIST.equalsIgnoreCase(fieldConstructor))
			query	=	GET_ALL_ROUTE_LIST;
		else if(Constants.GET_MVI_USER.equalsIgnoreCase(fieldConstructor))
			query	=	GET_MVI_USER;
		
		
		
		else if(Constants.REPORT_MASTER_TABLE.equalsIgnoreCase(fieldConstructor))
		{
			String[] duration	=	parentId.split("~");
			query = "SELECT "+duration[1]+" AS HEADER_ID,"+duration[2]+" AS HEADER_NAME FROM "+duration[0];
		}
		
			
		
		try 
		{
			Log.info("At PopulateDropDownDAO.getDropDownList: Query is: "+query);
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				pst = conn.prepareStatement(query);
				
				if(Constants.DZONGKHAG_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.VEHICLE_MODEL_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.GEWOG_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.BASE_OFFICE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.DEPARTMENT_LIST_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.VILLAGE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				//else if(Constants.GET_LEARNER_DRIVE_TYPE.equalsIgnoreCase(fieldConstructor))
				//	pst.setString(1, parentId);
				else if(Constants.ACCIDENT_CAUSE_MASTER.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.ONLINE_PAYMENT_BASE_OFFICE_LIST.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.GET_NATIONALITY_LIST.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.GET_POLICE_STATION_LIST_AGAINST_DIV.equalsIgnoreCase(fieldConstructor))
					pst.setString(1, parentId);
				else if(Constants.JURISDICTION_USER.equalsIgnoreCase(fieldConstructor))
				{
					String[] dataArray	=	parentId.split("~");
					pst.setString(1, dataArray[0]);
					pst.setString(2, dataArray[1]);
				}
				else if(Constants.GET_MVI_USER.equalsIgnoreCase(fieldConstructor))
				{
					String[] dataArray	=	parentId.split("~");
					pst.setString(1, dataArray[0]);
					pst.setString(2, dataArray[1]);
				}
				
				rs = pst.executeQuery();
				
				while(rs.next()){
					DropDownDTO dto = new DropDownDTO();
					dto.setHeaderId(rs.getString(Constants.HEADER_ID));
					if(Constants.VEHICLE_TYPE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
						dto.setHeaderName(WordUtils.capitalize(rs.getString(Constants.HEADER_NAME).replace("_", " ").toLowerCase()));
					else if(Constants.ENGINE_TYPE_DROP_DOWN_FIELD_CONSTRUCTOR.equalsIgnoreCase(fieldConstructor))
						dto.setHeaderName(WordUtils.capitalize(rs.getString(Constants.HEADER_NAME).toLowerCase()));
					else 
					dto.setHeaderName(rs.getString(Constants.HEADER_NAME));
					
					dropDownList.add(dto);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISSystemException("At PopulateDropDownDAO.getDropDownList(): connection can not be null;");
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dropDownList;
	}
	
	/*
	 * Queries Start
	 */
	private static final String GET_ETEST_QUESTION_DROPDOWN_QUERY = "SELECT Question_Id AS HEADER_ID, Question AS HEADER_NAME FROM t_etest_question_master";
	
	private static final String GET_SECURITY_QUESTION_DROPDOWN_QUERY = "SELECT question_id AS HEADER_ID, question AS HEADER_NAME FROM t_security_question_master";
	
	private static final String GET_REGION_DROPDOWN_QUERY = "SELECT region_id AS HEADER_ID, region_name AS HEADER_NAME FROM t_region_master ORDER BY region_name";
	
	private static final String GET_ROLE_DROPDOWN_QUERY = "SELECT `role_id` AS HEADER_ID, `role_name` AS HEADER_NAME FROM `t_role_master` WHERE `is_active`='Y' ORDER BY `role_name`";
	
	private static final String GET_DZONGKHAG_DROPDOWN_QUERY = "SELECT dzongkhag_id AS HEADER_ID, dzongkhag_name AS HEADER_NAME FROM t_dzongkhag_master a WHERE a.`region_id`=? ORDER BY dzongkhag_name";
	
	private static final String GET_ALL_DZONGKHAG_DROPDOWN_QUERY = "SELECT dzongkhag_id AS HEADER_ID, dzongkhag_name AS HEADER_NAME FROM t_dzongkhag_master ORDER BY dzongkhag_name";
	
	private static final String GET_VEHICLE_TYPE_DROPDOWN_QUERY = "SELECT Vehicle_Type_Id AS HEADER_ID, Description AS HEADER_NAME FROM t_vehicle_type_master ORDER BY Vehicle_Type_Name";
	
	private static final String GET_VEHICLE_MODEL_DROPDOWN_QUERY = "SELECT Vehicle_Model_Id AS HEADER_ID, Vehicle_Model_Name AS HEADER_NAME FROM t_vehicle_model_master WHERE Vehicle_Company_Id=? ORDER BY Vehicle_Model_Name";

	private static final String GET_VEHICLE_COMPANY_DROPDOWN_QUERY = "SELECT Vehicle_Company_Id AS HEADER_ID, Vehicle_Company_Name AS HEADER_NAME FROM t_vehicle_company_master ORDER BY Vehicle_Company_Name"; 
	
	private static final String GET_ENGINE_TYPE_DROPDOWN_QUERY = "SELECT Engine_type_Id AS HEADER_ID, Description AS HEADER_NAME FROM t_engine_type_master ORDER BY Engine_Type_Id";

	private static final String GET_VEHICLE_CODE_DROPDOWN_QUERY = "SELECT Vehicle_Registration_Id AS HEADER_ID, Vehicle_Reg_Code_Name AS HEADER_NAME FROM t_vehicle_registration_code_master ORDER BY Vehicle_Reg_Code_Name";

	private static final String GET_DEALERS_NAME_DROPDOWN_QUERY = "SELECT Dealer_Id AS HEADER_ID, Dealer_Name AS HEADER_NAME FROM t_dealer_master ORDER BY Dealer_Name";

	private static final String GET_MANUFACTURE_COUNTRY_DROPDOWN_QUERY = "SELECT Country_Id AS HEADER_ID, Country_Name AS HEADER_NAME FROM t_country_master ORDER BY Country_Name";

	private static final String GET_DEPARTMENT_DROPDOWN_QUERY = "SELECT Department_Id AS HEADER_ID, Department_Name AS HEADER_NAME FROM t_department_master a WHERE a.`Ministry_id`=? ORDER BY Department_Name";
	
	private static final String GET_DZONGKHAG_ALL_DROPDOWN_QUERY = "SELECT dzongkhag_id AS HEADER_ID, dzongkhag_name AS HEADER_NAME FROM t_dzongkhag_master ORDER BY dzongkhag_name";
	
	private static final String GET_TITLE_OF_COURTESY_TYPE_DROPDOWN_QUERY = "SELECT Courtesy_Title_Id AS HEADER_ID, Courtesy_Name AS HEADER_NAME FROM t_courtesy_title_master ORDER BY Courtesy_Name";
	
	private static final String OCCUPATION_DROPDOWN_QUERY = "SELECT Occupation_Id AS HEADER_ID, Occupation_Name AS HEADER_NAME FROM t_occupation_master ORDER BY Occupation_Name";
	
	private static final String GET_NATIONALITY_DROPDOWN_QUERY = "SELECT Country_Id AS HEADER_ID, Nationality AS HEADER_NAME FROM t_country_master ORDER BY Nationality";
	
	private static final String GET_GEWOG_DROPDOWN_QUERY = "SELECT Gewog_Id AS HEADER_ID, Gewog_Name AS HEADER_NAME FROM t_gewog_master a WHERE a.`Dzongkhag_Id`=? ORDER BY Gewog_Name";
	
	private static final String GET_MINISTRY_DROPDOWN_QUERY = "SELECT Ministry_Id AS HEADER_ID, Ministry_Name AS HEADER_NAME FROM t_ministry_master ORDER BY Ministry_Name";

	private static final String GET_COUNTRY_DROPDOWN_QUERY="SELECT Country_Id AS HEADER_ID, Country_Name AS HEADER_NAME FROM t_country_master ORDER BY Country_Name";
	
	private static final String BLOOD_GROUP_DROPDOWN_QUERY="SELECT blood_group_id AS HEADER_ID, blood_group_type AS HEADER_NAME FROM t_blood_group_master ORDER BY blood_group_type";

	private static final String GET_BASE_OFFICE_DROPDOWN_QUERY = "SELECT base_office_id AS HEADER_ID, base_office_name AS HEADER_NAME FROM t_base_office_master a WHERE a.`region_id`=? ORDER BY base_office_name";
	
	private static final String GET_BASE_OFFICE_ALL_DROPDOWN_QUERY = "SELECT base_office_id AS HEADER_ID, base_office_name AS HEADER_NAME FROM t_base_office_master a ORDER BY base_office_name";

	private static final String GET_ACCIDENT_CAUSE_DROPDOWN_QUERY="SELECT Accident_Cause_Id AS HEADER_ID, Accident_Name AS HEADER_NAME FROM t_accident_cause_master ORDER BY Accident_Name";

	//private static final String GET_VEHICLE_TYPE_DROPDOWN_QUERY="SELECT Vehicle_Type_Id AS HEADER_ID, Vehicle_Type_Name AS HEADER_NAME FROM t_vehicle_type_master ORDER BY Vehicle_Type_Name";

	private static final String GET_OFFENCE_LIST_DROPDOWN_QUERY="SELECT CONCAT(Offence_Type_Id,'_',`Is_Repeated_Offence_Payable`) AS HEADER_ID,Offence_Name AS HEADER_NAME FROM t_offence_type_master ORDER BY Offence_Name ";

	private static final String GET_OWNER_TYPE_LIST_DROPDOWN_QUERY="SELECT Owner_Type_Id AS HEADER_ID, Owner_Name AS HEADER_NAME FROM t_owner_master ORDER BY Owner_Name";
	
	private static final String GET_DRIVE_TYPE_LIST = "SELECT Drive_Type_Id AS HEADER_ID, Drive_Type_Name AS HEADER_NAME FROM `t_drive_type_master`";
	
	private static final String GET_VEHICLE_CANCELLATION_DROPDOWN_QUERY = "SELECT Reason_Id AS HEADER_ID, Reason AS HEADER_NAME FROM `t_reason_master`";
	
	private static final String GET_VEHICLE_CONVERSION_DROPDOWN_QUERY = "SELECT Reason_Id AS HEADER_ID, Reason AS HEADER_NAME FROM `t_reason_master`";
	
	private static final String ROUTE_LIST = "SELECT d.`dzongkhag_id` AS HEADER_ID,d.`dzongkhag_name` AS HEADER_NAME FROM t_dzongkhag_master d";
	
	private static final String VILLAGE_LIST = "SELECT  v.`Village_Serial_No` AS HEADER_ID,v.`Village_Name` AS HEADER_NAME FROM t_gewog_master g LEFT JOIN t_village_master v ON v.`Gewog_Serial_No`=g.`Gewog_Id` WHERE g.`Gewog_Id`=?";
	
	private static final String GET_BASE_OFFICE = "SELECT base_office_id AS HEADER_ID, base_office_name AS HEADER_NAME FROM t_base_office_master a  ORDER BY base_office_name";

	private static final String GET_ALL_GEWOG_DROPDOWN_QUERY = "SELECT `Gewog_Id` AS HEADER_ID, `Gewog_Name` AS HEADER_NAME FROM `t_gewog_master`";
	
	private static final String GET_VECHILE_PREFIX_LIST = "SELECT Sequence_Id AS HEADER_ID, Vehicle_Prefix AS HEADER_NAME FROM `t_vehicle_number_sequence`";
	
	private static final String GET_HYPOTHECATED_TO_LIST = "SELECT Hypothecated_Id AS HEADER_ID, Hypothecated_Name AS HEADER_NAME FROM `t_hypothecated_to_master`";
	
/*	FOLLOWING QUERY IS TO FETCH THE ASSIGNED DRIVE TYPE OF LEARNER LICENSE 
 * 
 * 	private static final String GET_LEARNER_DRIVE_TYPE	=	"SELECT "
		+ "  m.`Drive_Type_Name` AS HEADER_NAME, "
		+ "  m.`Drive_Type_Id` AS HEADER_ID "
		+ "FROM "
		+ "  `t_learner_licn_drive_type` d "
		+ "  LEFT JOIN `t_drive_type_master` m "
		+ "    ON m.`Drive_Type_Id` = d.`Drive_Type_Id` "
		+ "WHERE d.`Learner_License_Id` = "
		+ "  (SELECT "
		+ "    `Learner_License_Info_Id` "
		+ "  FROM "
		+ "    `t_learner_license_dtls` "
		+ "  WHERE `Learner_License_No` = ?)";*/
	
	private static final String GET_LEARNER_DRIVE_TYPE	="SELECT a.Drive_Type_Name AS HEADER_NAME,a.Drive_Type_Id AS HEADER_ID FROM `t_drive_type_master` a WHERE a.`Drive_Type_Category`='O'";
	
	private static final String GET_REGISTRATION_LIST = "SELECT `Registration_Type_Id` AS HEADER_ID, `Registration_Type_Name` AS HEADER_NAME FROM `t_registration_type_master`";
	
	private static final String GET_POLICE_STATION_LIST = "SELECT Police_Station_Id AS HEADER_ID, Police_Station_Name AS HEADER_NAME FROM t_police_station_master ORDER BY Police_Station_Name";
	
	private static final String GET_JURISDICTION_TYPE_LIST = "SELECT `Juris_Type_Id` AS HEADER_ID, `Jurisdiction_Type` AS HEADER_NAME FROM `t_jurisdiction_type_master`";

	private static final String GET_PRIVATE_COMPANY_LIST = "SELECT Private_Id AS HEADER_ID, Private_Name AS HEADER_NAME FROM t_private_company_master ORDER BY Private_Name";
	
	private static final String GET_ORDINARY_DRIVE_TYPE	=	"SELECT a.`Drive_Type_Id` AS HEADER_ID,a.`Drive_Type_Name` AS HEADER_NAME FROM `t_drive_type_master` a WHERE a.`Drive_Type_Category`='O'";
	
	private static final String GET_COMMERCIAL_DRIVE_TYPE	=	"SELECT a.`Drive_Type_Id` AS HEADER_ID,a.`Drive_Type_Name` AS HEADER_NAME FROM `t_drive_type_master` a WHERE a.`Drive_Type_Category`<>'O'";

	private static final String GET_ALL_VEHICLE_MODELS = "SELECT Vehicle_Model_Id AS HEADER_ID, Vehicle_Model_Name AS HEADER_NAME FROM t_vehicle_model_master ORDER BY Vehicle_Model_Name";

	private static final String GET_REPORT_TITLE	=	"SELECT a.`Report_Title` AS HEADER_NAME,a.`Report_Configuration_Id` AS HEADER_ID FROM `t_report_configuration` a WHERE a.`Is_Headoffice_Report`='N'";

	private static final String GET_REPORT_TITLE_FOR_HEADOFFICE	=	"SELECT a.`Report_Title` AS HEADER_NAME,a.`Report_Configuration_Id` AS HEADER_ID FROM `t_report_configuration` a WHERE a.`Is_Headoffice_Report`='Y'";

	private static final String GET_REPORT_TITLE_FOR_TRAFFIC	=	"SELECT a.`Report_Title` AS HEADER_NAME,a.`Report_Configuration_Id` AS HEADER_ID FROM `t_report_configuration` a WHERE a.`Is_Traffic_Report`='Y'";

	private static final String GET_REPORT_TITLE_FOR_EMISSION	=	"SELECT a.`Report_Title` AS HEADER_NAME,a.`Report_Configuration_Id` AS HEADER_ID FROM `t_report_configuration` a WHERE a.`Is_Emission_Report`='Y'";

	private static final String BUS_TYPE_LIST	=	"SELECT a.`bus_type_id`  AS HEADER_ID ,a.`bus_type_name` AS HEADER_NAME FROM `t_bus_type_master` a";
	
	private static final String COLOUR_LIST =	"SELECT a.`Colour_Id`  AS HEADER_ID ,a.`Colour_Name` AS HEADER_NAME FROM `t_colour_master` a";
	
	private static final String ACCIDENT_CAUSE_MASTER	=	"SELECT a.`Accident_Cause_Id` AS HEADER_ID , a.`Accident_Name` AS HEADER_NAME FROM `t_accident_cause_master` a WHERE	 a.`Accident_Type`=? ORDER BY a.`Accident_Name`";


	private static final String GET_ONINE_LOCATION_LIST = "SELECT CONCAT(1,'_',a.`region_id`) HEADER_ID,a.`region_name` HEADER_NAME FROM t_region_master a "
														+ "UNION "
														+ "SELECT CONCAT(2,'_', "
														+ "b.`base_office_id`) HEADER_ID,b.`base_office_name` HEADER_NAME "
														+ "FROM t_base_office_master b WHERE "
														+ "b.`base_office_name` NOT IN (SELECT CONCAT(region_name,' Base') FROM t_region_master)";
	
	
	private static final String ONLINE_PAYMENT_BASE_OFFICE_LIST	=		"SELECT base_office_id AS HEADER_ID,base_office_name AS HEADER_NAME " +
					"FROM t_base_office_master a LEFT JOIN `t_region_master` b ON a.`region_id`=b.`region_id` " +
					"WHERE a.`region_id` = ? AND a.`base_office_name` NOT LIKE (CONCAT(b.`region_name`,' Base')) ORDER BY base_office_name ";
	
	
	private static final String DIPLOMAT_LIST	=	"select a.Diplomat_Code HEADER_ID, " +
														"a.Name HEADER_NAME " +
														"from t_diplomat_master a";
	
	
	private static final String GET_NATIONALITY_LIST	=	"SELECT Country_Id HEADER_ID, Nationality HEADER_NAME FROM t_country_master WHERE Country_Id=?";
	
	private static final String  POLICE_DIVISION_LIST = "SELECT a.`Police_Division_Id` HEADER_ID,a.`Police_Division_Name` HEADER_NAME FROM t_police_division_master a";
	
	private static final String  GET_POLICE_STATION_LIST_AGAINST_DIV = "SELECT a.`Police_Station_Id` HEADER_ID,a.`Police_Station_Name` HEADER_NAME FROM t_police_station_master a WHERE a.`Division_Id`=?";
	
	
	private static final String  GET_ACCIDENT_INFORMER_LIST = "SELECT a.`Accident_Informer_Master_Id` HEADER_ID,a.`Name` HEADER_NAME FROM `t_accident_informer_master` a";
	
	private static final String  PASSANGER_BUS_CATEGORY_LIST = "SELECT a.`Passenger_Bus_Category_Id` HEADER_ID,a.`Name` HEADER_NAME FROM `t_passenger_bus_category_master` a";
	
	private static final String  ORGANISATION_LIST = "SELECT a.`Organization_Name` HEADER_NAME,a.`Customer_Id` HEADER_ID FROM t_organization_info a ";

	private static final String GET_JURISDICTION_USER_LIST = "SELECT b.`name` HEADER_NAME,b.`login_id` HEADER_ID FROM t_user_jurisdiction_mapping a "
															+ " LEFT JOIN t_user_master b ON a.`login_id`=b.`login_id` "
															+ " WHERE "
															+ " b.`login_id` IN "
															+ " (SELECT d.`login_id` FROM t_role_master c LEFT JOIN t_user_role_mapping d ON c.`role_id`=d.`role_id` "
															+ " WHERE c.`role_name` NOT IN ('Citizen','Traffic Police','Print','eTest Management','emission center','Report','Emission Admin','Head Office','Others','TOP','Region Admin','staff')) "
															+ " AND a.`juris_type_id`=? AND a.`jurisdiction_id`=? "
															+ " GROUP BY a.`login_id` ORDER BY b.name";
	

	private static final String GET_MVI_USER = "SELECT concat(b.`name`,' (',b.login_id,')') HEADER_NAME,b.`login_id` HEADER_ID FROM t_user_jurisdiction_mapping a "
												+ " LEFT JOIN t_user_master b ON a.`login_id`=b.`login_id` "
												+ " WHERE "
												+ " b.`login_id` IN "
												+ " (SELECT d.`login_id` FROM t_role_master c LEFT JOIN t_user_role_mapping d ON c.`role_id`=d.`role_id` "
												+ " WHERE c.`role_name` IN ('MVI')) AND b.is_active='Y' "
												+ " AND a.`juris_type_id`=? AND a.`jurisdiction_id`=? "
												+ " GROUP BY a.`login_id` ORDER BY b.name";
	
	
	
	private static final String GET_DIPLOMAT_LIST_FOR_ORGANISTAION = "SELECT a.`Diplomat_Master_Id` HEADER_ID,a.`Name` HEADER_NAME FROM t_diplomat_master a ";
	
	private static final String GET_ALL_ROUTE_LIST = "SELECT a.`Route_Id` HEADER_ID,a.`Route_Name` HEADER_NAME FROM t_route_master a ORDER BY a.`Route_Name` ASC";
	
	
	
	
	
}
