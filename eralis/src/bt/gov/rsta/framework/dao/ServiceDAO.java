package bt.gov.rsta.framework.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import bt.gov.g2c.aggregator.business.InvokePaymentWS;
import bt.gov.g2c.aggregator.dto.RequestDTO;
import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusTrailDTO;
import bt.gov.rsta.eralis.util.DateComparatorFromStringFormat;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.WorkflowManager;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.vo.WorkflowDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Months;
import org.joda.time.Years;

public class ServiceDAO 
{
	private static ServiceDAO serviceDao;
	
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date(System.currentTimeMillis());
	
	public static ServiceDAO getInstance()
	{
		if(serviceDao == null){
			serviceDao = new ServiceDAO();
		}
		return serviceDao;
	}
	public ServiceDTO getPersonalInfo(String cid,String dob) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				 
				pst = conn.prepareStatement(GET_PERSONAL_INFO); 
				pst.setString(1, cid);
				
			 	java.util.Date openDate = sourceSdf.parse(dob);
			 	String openDt = targetSdf.format(openDate)+" 00:00:00";
				
			 	pst.setString(2, openDt); 
				
				rs = pst.executeQuery();
				rs.first();
				
				
				dto.setMinistry(rs.getString("Ministry_Id"));
				dto.setDepartment(rs.getString("Department_Id"));
				dto.setGuardianNo(rs.getString("Guardian_Contact_No"));
				dto.setRowCount(rs.getString("rowCount"));
				dto.setTitleCourtesy(rs.getString("Title_Of_Courtesy_Id"));
				dto.setFirstName(rs.getString("First_Name"));
				dto.setMiddleName(rs.getString("Middle_Name"));
				dto.setLastName(rs.getString("Last_Name"));
				dto.setOccupation(rs.getString("Occupation_Id"));
				dto.setBloodGroup(rs.getString("Blood_Group_Id"));
				dto.setFatherName(rs.getString("Fathers_Name"));
				dto.setCid(rs.getString("CID_Number"));
				dto.setDob(rs.getString("dob"));
				dto.setGender(rs.getString("Gender"));
				dto.setPermanentDzongkhag(rs.getString("Permanent_Dzongkhag_Id"));
				dto.setPermanentGewog(rs.getString("Permanent_Gewog_Id"));
				dto.setPermanentVillage(rs.getString("Permanant_Village_Id"));

				dto.setPermanentAddress(rs.getString("Permanent_Address"));
				dto.setPresentDzongkhag(rs.getString("Present_Dzongkhag_Id"));
				dto.setPresentContactAddress(rs.getString("Present_Contact_Address"));
				dto.setPresentPhoneNo(rs.getString("Present_Phone_No"));
				dto.setPresentEmail(rs.getString("Present_Email"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setImage(rs.getString("Image_Path"));
				dto.setPermanentCountry(rs.getString("Country_Name"));
				dto.setNationality(rs.getString("Nationality_Id"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[getdrivinglicense]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public String saveOTP(String cidNumber, String otp) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try {
			conn = ConnectionManager.getConnection();
			
			if(conn != null){
				String sql = "INSERT INTO `t_otp_master` (`CID_Number`, `OTP`) VALUES (?, ?)";
				
				pst = conn.prepareStatement(sql);
				pst.setString(1, cidNumber);
				pst.setString(2, otp);
				int count = pst.executeUpdate();
				if(count > 0)
					result = "SUCCESS";
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
		} finally {
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return result;
	}
	
	
	public ServiceDTO getOtherIssueTypeBookDtls(String learnerNo,String cid, String testType,String issueType) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs 	= null,rs1 	= null; 
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
					pst = conn.prepareStatement(GET_PERSONAL_DTLS_FROM_CID);
					pst.setString(1, cid);
					rs = pst.executeQuery();
					rs.first();	
					if(rs.getInt("rowCount")>0)
					{
						dto.setStatus("VALID_CID");
						dto.setCustomerID(rs.getString("Customer_Id"));
						dto.setRowCount(rs.getString("rowCount"));
						dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
						dto.setTitleCourtesy(rs.getString("Title_Of_Courtesy_Id"));
						dto.setFirstName(rs.getString("First_Name"));
						dto.setMiddleName(rs.getString("Middle_Name"));
						dto.setLastName(rs.getString("Last_Name"));
						dto.setCid(rs.getString("CID_Number"));
						dto.setGender(rs.getString("Gender"));
						dto.setPresentPhoneNo(rs.getString("Present_Phone_No"));
						dto.setPresentEmail(rs.getString("Present_Email"));
						dto.setDob(rs.getString("Citizen_Date_Of_Birth"));
						pst1 = conn.prepareStatement("SELECT COUNT(*)rowCount FROM t_driving_license_dtls a WHERE a.`Customer_Id`=?");
						pst1.setString(1, rs.getString("Customer_Id"));
						rs1 = pst1.executeQuery();
						rs1.first();
						int rowCount = rs1.getInt("rowCount");
						if(rowCount>0)
						{
							dto.setStatus("DRIVING_LICENSE_ALREADY_EXISTED");
						}

						pst1 = conn.prepareStatement(GET_ETEST_ID);
						pst1.setString(1, learnerNo);
						pst1.setString(2, cid);
						rs1 = pst1.executeQuery();
						rs1.first();
						dto.setApplicationNumber(rs1.getString("Application_Number"));
					}
					else
					{
						dto.setStatus("INVALID_CID");
					}
					
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[getdrivinglicense]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
			ConnectionManager.close(null, null, rs1, pst1);
		}
		
		return dto;
	}
	public ServiceDTO getLearnerLicense(String learnerNo,String cid, String testType,String issueType) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
				if(testType.equals("ENDORSEMENT"))
				{
					pst = conn.prepareStatement(GET_DRIVING_LICENSE_DTLS);
					pst.setString(1, learnerNo);
					pst.setString(2, cid);
					rs = pst.executeQuery();
					rs.first();
					
					dto.setFirstName(rs.getString("First_Name"));
					dto.setMiddleName(rs.getString("Middle_Name"));
					dto.setLastName(rs.getString("Last_Name"));
					dto.setPresentPhoneNo(rs.getString("Present_Phone_No"));
					dto.setDob(rs.getString("Citizen_Date_Of_Birth"));
					dto.setApplicationNumber(rs.getString("test_applicationNo"));
					dto.setTestDate(rs.getString("Test_Date"));
					dto.setTestLocation(rs.getString("Test_Location_Id"));
					dto.setDrivetype(rs.getString("Drive_Type_Id"));
					dto.setRowCount(rs.getString("rowCount"));
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setCustomerID(rs.getString("Customer_Id"));
				}
				else
				{
					if(learnerNo.equalsIgnoreCase("0"))
					{
						pst = conn.prepareStatement(GET_LEARNER_LICENSE_BY_CID); 
						pst.setString(1, cid); 
					}
					else if (testType.equals("renew_learner_search") || testType.equals("learner_license_duplicate"))
					{
						pst = conn.prepareStatement(GET_LEARNER_LICENSE_DTLS_BY_CID_OR_LL); 
						pst.setString(1, cid); 
						pst.setString(2, cid); 
						pst.setString(3, learnerNo); 
						pst.setString(4, learnerNo); 
					}
					else
					{
						pst = conn.prepareStatement(GET_LEARNER_LICENSE); 
						pst.setString(1, learnerNo); 
						pst.setString(2, cid); 
					}
					
					 
					rs = pst.executeQuery();
					rs.first();
					if ((testType.equals("renew_learner_search") || testType.equals("learner_license_duplicate")) && rs.getInt("countDL")>0)
					{
						dto.setStatus("HAS_DRIVING_LICENSE");
						return dto;
					}
					
					dto.setRowCount(rs.getString("rowCount"));
					
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setTitleCourtesy(rs.getString("Title_Of_Courtesy_Id"));
					dto.setFirstName(rs.getString("First_Name"));
					dto.setMiddleName(rs.getString("Middle_Name"));
					dto.setLastName(rs.getString("Last_Name"));
					dto.setCid(rs.getString("CID_Number"));
					dto.setGender(rs.getString("Gender"));
					dto.setImage(rs.getString("Image_Path"));
					dto.setIssueDate(rs.getString("Issue_Date"));
					dto.setPresentPhoneNo(rs.getString("Present_Phone_No"));
					dto.setPresentEmail(rs.getString("Present_Email"));
					dto.setDob(rs.getString("Citizen_Date_Of_Birth"));
					dto.setApplicationNumber(rs.getString("test_applicationNo"));
					dto.setTestDate(rs.getString("Test_Date"));
					dto.setTestLocation(rs.getString("Test_Location_Id"));
					dto.setDrivetype(rs.getString("Drive_Type_Id"));
					dto.setCustomerID(rs.getString("Customer_Id"));
					dto.setLearnerLicenseId(rs.getString("Learner_License_Info_Id"));
					
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[getdrivinglicense]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public ServiceDTO organizationVehicleRenewaldtls(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst	=	conn.prepareStatement(GET_ORGANISATION_VEHICLE_DTLS);
				pst.setString(1, dto.getVehicleNo());
				pst.setString(2, dto.getOrganizationName()); 
				rs = pst.executeQuery();
				rs.first();
				 
				
				
				
				pst	=	conn.prepareStatement(GET_ORGANISATION_CANCELLATION_DTLS);
				pst.setString(1, dto.getVehicleNo());
				rs = pst.executeQuery();
				rs.first();
				if(rs.getString("rowCount").equalsIgnoreCase("0"))
				{
					pst	=	conn.prepareStatement(GET_ORGANISATION_VEHICLE_TRANSFER_DTLS);
					pst.setString(1, dto.getVehicleNo());
					pst.setString(2, dto.getOrganisationName()); 
					rs = pst.executeQuery();
					rs.first();
					if(rs.getString("rowCount").equalsIgnoreCase("0"))
					{
						pst	=	conn.prepareStatement(GET_ORGANISATION_VEHICLE_DTLS);
						pst.setString(1, dto.getVehicleNo());
						pst.setString(2, dto.getOrganizationName()); 
						rs = pst.executeQuery();
						rs.first();
						//if(rs.getString("rowCount").equalsIgnoreCase("0"))
						//{
						//	customerId	=	rs.getString("Customer_Id");
						//}
					}
				}
				if(!rs.getString("rowCount").equalsIgnoreCase("0"))
				{
					dto.setRowCount(rs.getString("rowCount")); 
					dto.setExpiryDate(rs.getString("vehExpiry_Date"));
					dto.setVehicleCompany(rs.getString("Vehicle_Company_Name"));
					dto.setVehicleModel(rs.getString("Vehicle_Model_Name"));
					dto.setEngineCC(rs.getString("Engine_CC"));  
					 
				 	String currentDate = sourceSdf.format(date);
					dto.setRenewalDate(currentDate);
					
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	
	/*public String editPersonalInfo(ServiceDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{*/
	
	//passengerBusPermit

	public String passengerBusPermit(ServiceDTO dto,Connection conn) throws ERALISException, ERALISSystemException
	{ 
		PreparedStatement pst = null;
		String result	=	null;
		String applicationNumber = null;
		try
		{
			if(conn != null)
			{
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_passenger_bus_route_permit_application", "120","", conn);
				
				String[] dataArr = dto.getLocation().split("_");
				pst = conn.prepareStatement(INSERT_INTO_PASSENGER_BUS_ROUTE_PERMIT);
				pst.setString(1, applicationNumber);
				pst.setString(2, dto.getName());
				
				java.util.Date dob = sourceSdf.parse(dto.getDob());
				String dobStr = targetSdf.format(dob);
				
				pst.setString(3, dobStr);
				pst.setString(4, dto.getGender());
				pst.setString(5, dto.getDzongkhag());
				pst.setString(6, dto.getGewog());
				pst.setString(7, dto.getVillage());
				pst.setString(8, dto.getPhone());
				pst.setString(9, dto.getEmail());
				pst.setString(10, dto.getAddress());
				pst.setString(11, dto.getType());
				pst.setString(12, dto.getBusCategory());
				pst.setString(13, dto.getRouteFrom());
				pst.setString(14, dto.getRouteTo());
				pst.setString(15, dto.getTiming());
				pst.setString(16, dto.getTransport());

				if(dataArr[0].equalsIgnoreCase("1"))
				{
					pst.setString(17, dataArr[0]);
					pst.setString(18, "0");
				}
				else if(dataArr[0].equalsIgnoreCase("2"))
				{
					pst.setString(17, "0");
					pst.setString(18, dataArr[0]);
				}
				
				pst.setString(19, "");
				pst.setString(20, "");
				pst.setString(21, "Citizen");
				pst.setString(22, "PBRP");
				pst.setString(23, dto.getCustomerId());
				pst.setString(24, dto.getCid());
				int count = pst.executeUpdate();
				
				if(count>0)
				{

					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId("Citizen");
					vo.setActorName("Citizen");
					vo.setRoleId("0");
					vo.setRoleName("Citizen");
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId("Citizen");
					vo.setAssignedUserId("Citizen");
					vo.setServiceId("120");
					vo.setAssigned_Priv_Id("");
					vo.setJurisId(dataArr[1]);
					vo.setJurisTypeId(dataArr[0]);
					new WorkflowManager(conn).logSubmissionWorkflow(vo);
					result = "SUCCESS";
				}
				
				
			}
		}
		catch (Exception e) 
		{ 
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+"#"+applicationNumber;
	}

	public String bookDrivingTest(ServiceDTO dto,String issueType,String cid,String learnerNo,Connection conn) throws ERALISException, ERALISSystemException
	{ 
		PreparedStatement pst = null;
		ResultSet rs 	= null;
		String result	=	null;
		int slNo = 0;
		String bsfOrderNo = null;
		int n = 0;
		
		try
		{
			if(conn != null)
			{
				String[] tempArray = dto.getTestLocation().split("#");
				
				pst	=	conn.prepareStatement(CHECK_DRIVING_TEST_SEAT);
				pst.setString(1, dto.getTestDate());
				pst.setString(2, tempArray[0]);
				pst.setString(3, tempArray[1]);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("totalApplicant")<rs.getInt("Max_Applicants"))
				{
					Random rand = new Random();
					n = rand.nextInt(99999999) + 1;
					
					pst	= conn.prepareStatement(BOOK_DRIVING_TEST, PreparedStatement.RETURN_GENERATED_KEYS);
					pst.setString(1, "B"+n);
					//if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType))
					if("Driving_Institue".equalsIgnoreCase(dto.getIssueType()) || "Learner_License".equalsIgnoreCase(dto.getIssueType()))
					{
						pst.setString(2, dto.getLearnerNo());
						pst.setString(3, dto.getDob());
					}
					else
					{
						//pst.setString(2, learnerNo);
						pst.setString(2, dto.getLearnerNo());
						pst.setString(3, dto.getDob());
					}
					
					pst.setString(4, dto.getDrivetype());
					pst.setString(5, tempArray[1]); 
					pst.setString(6, dto.getTestDate()); 
					pst.setString(7, "CITIZEN");
					
					pst.setString(8, dto.getTestType());
					if("Driving_Institue".equalsIgnoreCase(dto.getIssueType()) || "Learner_License".equalsIgnoreCase(dto.getIssueType()))
					{
						pst.setString(9, dto.getCustomerID());
					}
					else
					{
						//pst.setString(9,cid);
						pst.setString(9,dto.getCid());
					}
	
					pst.setString(10, tempArray[0]);
					pst.setString(11, dto.getIssueType());
					int count = pst.executeUpdate();
					if(count > 0)
					{	
						ResultSet resultSet = pst.getGeneratedKeys();
						while (resultSet.next()) {
							slNo = resultSet.getInt(1);
						}
						result 	= "SUCCESS";
						bsfOrderNo = invokePaymentAggregator(conn, "B"+n, dto.getDrivetype());
					} 
				}
				else
				{
					result 	= "NO_SEAT_AVAILABLE";
				}
			}
		} 
		catch (Exception e) 
		{ 
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+"#"+bsfOrderNo+"#B"+n;
	}
	
	

	public String adminBookingEtest(ServiceDTO dto,String issueType,String cid,String learnerNo,String userId,Connection conn) throws ERALISException, ERALISSystemException
	{ 
		PreparedStatement pst = null;
		String result	=	null;
		int slNo = 0;
		int n = 0;
		String bookingNo = null;
		try
		{
			if(conn != null)
			{
				String[] tempArray = dto.getTestLocation().split("#");
				Random rand = new Random();
				n = rand.nextInt(99999999) + 1;
				bookingNo = "RJ"+n;
				pst	= conn.prepareStatement(ADMIN_BOOK_DRIVING_TEST, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, bookingNo);
				if("Driving_Institue".equalsIgnoreCase(dto.getIssueType()) || "Learner_License".equalsIgnoreCase(dto.getIssueType()))
				{
					pst.setString(2, dto.getLearnerNo());
					pst.setString(3, dto.getDob());
				}
				else
				{
					pst.setString(2, dto.getLearnerNo());
					pst.setString(3, dto.getDob());
				}
				
				pst.setString(4, dto.getDrivetype());
				pst.setString(5, tempArray[1]); 
				pst.setString(6, dto.getTestDate()); 
				pst.setString(7, userId);
				
				pst.setString(8, dto.getTestType());
				if("Driving_Institue".equalsIgnoreCase(dto.getIssueType()) || "Learner_License".equalsIgnoreCase(dto.getIssueType()))
				{
					pst.setString(9, dto.getCustomerID());
				}
				else
				{
					pst.setString(9,dto.getCid());
				}

				pst.setString(10, tempArray[0]);
				pst.setString(11, dto.getIssueType());
				int count = pst.executeUpdate();
				if(count > 0)
				{	
					ResultSet resultSet = pst.getGeneratedKeys();
					while (resultSet.next()) {
						slNo = resultSet.getInt(1);
					}
					result 	= "SUCCESS";
					pst = conn.prepareStatement(INSERT_INTO_T_PAYMENT_DTLS);
					pst.setInt(1, slNo);
					pst.setString(2, dto.getAmount());
					pst.setString(3, dto.getReceiptNo());
					java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr1 = targetSdf.format(receiptDate1);
					pst.setString(4, receiptDateStr1);
					pst.setString(5, userId);
					count = pst.executeUpdate();
				} 
			}
		} 
		catch (Exception e) 
		{ 
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+"#"+bookingNo;
	}
	
	private String invokePaymentAggregator(Connection conn, String applicationNo, String driveTypeId) throws Exception{
		PreparedStatement pst = null, pst1 = null;
		ResultSet rs = null, rs1 = null;
		String result = null;
		String accountHeadDesc = null;
		String testFees = null;
		
		try {
			pst1 = conn.prepareStatement("SELECT Description FROM t_drive_type_master WHERE Drive_Type_Id=?");
			pst1.setString(1, driveTypeId);
			rs1 = pst1.executeQuery();
			rs1.first(); 
			String description = rs1.getString("Description");
			
			if(description.equalsIgnoreCase("TWO_WHEELER")){
				accountHeadDesc = "TWO_WHEELER_DRIVING_TEST_FEES";
				testFees = "50";
			}
			else if(description.equalsIgnoreCase("MEDIUM_VEHICLE") || description.equalsIgnoreCase("MEDIUM_BUS") 
					|| description.equalsIgnoreCase("TOURIST_MEDIUM_BUS")){
				accountHeadDesc = "MEDIUM_VEHICLE_DRIVING_TEST_FEES";
				testFees = "100";
			}
			else if(description.equalsIgnoreCase("HEAVY_VEHICLE") || description.equalsIgnoreCase("HEAVY_BUS") 
					|| description.equalsIgnoreCase("TOURIST_HEAVY_BUS") || description.equalsIgnoreCase("TRACTOR")){
				accountHeadDesc = "HEAVY_VEHICLE_DRIVING_TEST_FEES";
				testFees = "100";
			}
			else if(description.equalsIgnoreCase("TAXI")){
				accountHeadDesc = "TAXI_DRIVING_TEST_FEES";
				testFees = "100";
			} 
			else if(description.equalsIgnoreCase("EXCAVATOR") || description.equalsIgnoreCase("BULL_DOZER")
					|| description.equalsIgnoreCase("PAY_LOADER")
					|| description.equalsIgnoreCase("ROAD_ROLLER") || description.equalsIgnoreCase("ROAD_PAVER"))
			{
				accountHeadDesc = "HEAVY_VEHICLE_DRIVING_TEST_FEES";
				testFees = "500";
			}
			else
			{
				accountHeadDesc = "LIGHT_VEHICLE_DRIVING_TEST_FEES";
				testFees = "75";
			}
			
			
			RequestDTO dto = new RequestDTO();
			dto.setApplicationNo(applicationNo);
			dto.setAgencyCode("RSTA");
			dto.setServiceName("Driving Test Booking");
			dto.setExpiryDate(null);//only for renewal services or services which involve penalty or late fees
			ArrayList<bt.gov.g2c.aggregator.dto.PaymentDTO> paymentList = new ArrayList<bt.gov.g2c.aggregator.dto.PaymentDTO>();
			
			/***offence amount****/
			bt.gov.g2c.aggregator.dto.PaymentDTO dto1 = new bt.gov.g2c.aggregator.dto.PaymentDTO();
			dto1.setServiceFee(testFees);
			
			pst = conn.prepareStatement(GET_ACCOUNT_HEAD_ID);
			pst.setString(1, accountHeadDesc);
			rs = pst.executeQuery();
			rs.first();
			if(rs.getInt("rowCount")>0)
			{
				dto1.setAccountHeadId(rs.getString("account_head_code"));
			} 
			paymentList.add(dto1);
			
			dto.setPaymentList(paymentList.toArray(new bt.gov.g2c.aggregator.dto.PaymentDTO[paymentList.size()]));
			ResourceBundle bundle = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.wsEndPointURL_en_US");
			InvokePaymentWS invokews = new InvokePaymentWS(bundle.getString("getPaymentDetails.endPointURL"));
			String bfsOrderNo = invokews.insertPaymentDetailsOnSubmission(dto);
			result	=	bfsOrderNo;
			
		} catch (Exception e) {
			throw new Exception();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	public String cancelDrivingTest(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		//ResultSet rs 	= null; 
		String result	=	null;
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst	=	conn.prepareStatement(CANCEL_DRIVING_TEST);
				pst.setString(1,dto.getApplicationNumber()); 
				int count = pst.executeUpdate();
				if(count > 0)
				{	
					result 	= "SUCCESS";
				} 
			}
		} 
		catch (Exception e) 
		{ 
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return result;
	}
	public ServiceDTO getVehicleId(String vehicleNo, String vehicleType) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null;
		ServiceDTO dto = new ServiceDTO();
		//String customerId	=	null;
		//String query	=	null;
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst	=	conn.prepareStatement(GET_VEHICLE_ID);
				pst.setString(1, vehicleNo);
				//pst.setString(2, cid);
				pst.setString(2, vehicleType);
				rs = pst.executeQuery();
				rs.first();
				dto.setVehicleRegDtlsId(rs.getString("vehicle_id"));
				dto.setRowCount(rs.getString("rowCount"));
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	//generateAcknowledgementReceipt
	

	public ServiceDTO vehicleDetails(String vehicleNo,String cid) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_VEHILCE_DETIALS); 
				pst.setString(1, vehicleNo); 
				pst.setString(2, cid); 
				rs = pst.executeQuery();
				rs.first();
				dto.setRowCount(rs.getString("rowCount")); 
				dto.setVehicleNo(rs.getString("Vehicle_Number")); 
				dto.setVehicleRegistrationType(rs.getString("Vehicle_Registration_Type")); 
				dto.setRegionId(rs.getString("vehicleRegion")); 
				dto.setVehicleCompany(rs.getString("Vehicle_Company_Name")); 
				dto.setRegistrationDate(rs.getString("Registration_Date")); 
				dto.setVehicleModelId(rs.getString("Vehicle_Model_Id")); 
				dto.setVehicleModelId(rs.getString("Vehicle_Model_Name")); 
				dto.setEngineNumber(rs.getString("Engine_Number")); 
				dto.setEngineCC(rs.getString("Engine_CC")); 
				dto.setPurchaseType(rs.getString("Purchase_Type")); 

				dto.setFirstName(rs.getString("First_Name")); 
				dto.setLastName(rs.getString("Last_Name")); 
				dto.setMiddleName(rs.getString("Middle_Name")); 
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public ServiceDTO generateAcknowledgementReceipt(String applicationNo) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				if(applicationNo.contains("B"))
				{
					pst = conn.prepareStatement(GET_ONLINEPAYEMENT_BOOKING_DTLS); 
					pst.setString(1, applicationNo); 
					rs = pst.executeQuery();
					rs.first();
					
					dto.setAmount(rs.getString("Amount_Paid")); 
					dto.setReceiptDate(rs.getString("Receipt_Date")); 
					dto.setReceiptNo(rs.getString("Receipt_No")); 
					dto.setApplicationNo(applicationNo);
				}
				else if(applicationNo.contains("PN"))
				{
					String serviceId = "000";
					if(applicationNo.length() > 10)
						serviceId = applicationNo.substring(2, 5);
					
					pst = conn.prepareStatement(GET_SERVICE_SHORT_DESC);
					pst.setString(1, serviceId);
					rs = pst.executeQuery();
					rs.first();
					String serviceShortCode = rs.getString("Service_Short_Desc");
					
					if(serviceShortCode.equalsIgnoreCase("FITNESS"))
					{
						applicationNo = applicationNo.substring(5, applicationNo.length());
					}
						pst = conn.prepareStatement(GET_ONLINEPAYMENT_DTLS); 
						pst.setString(1, applicationNo); 
						rs = pst.executeQuery();
						rs.first();
						double totalAmount = rs.getDouble("Amount_Paid") + rs.getDouble("Penalty_Paid");
						dto.setAmount(Double.toString(totalAmount)); 
						dto.setReceiptDate(rs.getString("Receipt_Date")); 
						dto.setReceiptNo(rs.getString("Receipt_No")); 
						dto.setApplicationNo(applicationNo);
				}
				else
				{
					pst = conn.prepareStatement(GET_ONLINEPAYMENT_OFFENCE_DTLS); 
					pst.setString(1, applicationNo); 
					rs = pst.executeQuery();
					rs.first();
					dto.setAmount(rs.getString("Amount")); 
					dto.setReceiptDate(rs.getString("Receipt_Date")); 
					dto.setReceiptNo(rs.getString("Receipt_Number")); 
					dto.setApplicationNo(applicationNo);
				}
				
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[vehicleDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public ServiceDTO getLicenseDetails(String tinNo,String licenseNo) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				 
				pst = conn.prepareStatement(GET_FINE_INFO); 
				pst.setString(1, tinNo);  
				pst.setString(2, licenseNo); 
				
				rs = pst.executeQuery();
				rs.first();
				dto.setRowCount(rs.getString("rowCount"));
				dto.setFirstName(rs.getString("First_Name"));
				dto.setMiddleName(rs.getString("Middle_Name"));
				dto.setLastName(rs.getString("Last_Name"));
				 
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[getdrivinglicense]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	

	public ServiceDTO checkApplicationSubmitted(String identityId,String formType) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null;
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getOnlineDbConnection();
			
			if(conn != null)
			{
				dto.setRowCount("0");
				String query = null;
				String applicationType = null;
				if(formType.equalsIgnoreCase("renew_driving_license"))
				{
					query = CHECK_APPLICATION_SUBMITTED;
					applicationType = "LICENSERENEWAL";
				}
				else if(formType.equalsIgnoreCase("driving_license_duplication")) 
				{
					query = CHECK_APPLICATION_SUBMITTED;
					applicationType = "LICENSEDUPLICATION";
				}
				else if(formType.equalsIgnoreCase("VEH_RENEWAL")) 
				{
					query = CHECK_APPLICATION_SUBMITTED_FOR_VEHICLE;
					applicationType = "RENEWAL";
				}
				else if(formType.equalsIgnoreCase("vehicle_duplication")) 
				{
					query = CHECK_APPLICATION_SUBMITTED_FOR_VEHICLE;
					applicationType = "DUPLICATION";
				}
				
				
				
				pst = conn.prepareStatement(query);
				pst.setString(1, identityId);
				pst.setString(2, applicationType);
				rs = pst.executeQuery();
				while(rs.next())
				{	
					dto.setRowCount(rs.getString("rowCount"));
					dto.setApplicationNo(rs.getString("Application_Number"));
					dto.setRenewalDuration(rs.getString("Renewal_Duration"));
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[checkApplicationSubmitted]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}

	public ServiceDTO getPersonalInfoId(String cid,String dlNo) throws ERALISException, ERALISSystemException
	{ 
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null;
		ServiceDTO dto = new ServiceDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_LICENSE_PERSONAL_INFO_ID);
				pst.setString(1, cid);
				pst.setString(2, cid);
				pst.setString(3, dlNo);
				pst.setString(4, dlNo);
				rs = pst.executeQuery();
				String  count	=	"0";
				while(rs.next())
				{	
					count	=	"1";
					dto.setDrivinglicenseId(rs.getString("Driving_License_Id"));
					dto.setDrivingLicenseType(rs.getString("Driving_License_Type_Id"));
				}
				dto.setRowCount(count);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at ServiceDAO[getdrivinglicense]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	 
	public String editPersonalInfo(ServiceDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		try 
		{	
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_CUSTOMER_ID_FROM_INFO_ID);
				pst.setString(1, dto.getPersonalInfoId());
				rs = pst.executeQuery();
				rs.first();
				String customerId = rs.getString("Customer_Id");
				
				DocumentVO vo = new DocumentVO();
				if(!dto.getNewImage().getFileName().equalsIgnoreCase(""))
				{
					vo.setFileContent(dto.getNewImage().getFileData());
					vo.setName(dto.getNewImage().getFileName());
					vo.setDocumentType("IMAGE");
					vo = CommonDAO.getInstance().imageUploader(vo, customerId);
				}
				
				pst = conn.prepareStatement(UPDATE_PERSONAL_INFO);
				pst.setString(1, dto.getTitleCourtesy());
				pst.setString(2, dto.getFirstName());
				pst.setString(3, dto.getMiddleName());
				pst.setString(4, dto.getLastName());
				pst.setString(5, dto.getOccupation());
				pst.setString(6, dto.getBloodGroup());
				pst.setString(7, dto.getFatherName());
				pst.setString(8, dto.getGender());
				pst.setString(9, dto.getPermanentDzongkhag());
				pst.setString(10, dto.getPermanentGewog());
				pst.setString(11, dto.getPermanentVillage());
				pst.setString(12, dto.getPermanentAddress());
				pst.setString(13, dto.getPresentDzongkhag());
				pst.setString(14, dto.getPresentContactAddress());
				pst.setString(15, dto.getPresentPhoneNo());
				pst.setString(16, dto.getPresentEmail());
				
				if(dto.getNewImage().getFileName().equalsIgnoreCase(""))
				pst.setString(17, dto.getOldImage());
				else
				pst.setString(17, vo.getUploadURL());
				pst.setString(18, vo.getUuid());
				pst.setString(19, dto.getNationality());
				pst.setString(20, dto.getPersonalInfoId());
				
				int count = pst.executeUpdate();
				if(count > 0)
					result 	= "SUCCESS";
			}
		}
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ServiceDAO[editPersonalInfo]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String getCustomerId(String cid) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs 	= null; 
		String customerId	=	null;
		try 
		{	conn = ConnectionManager.getConnection();
		
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_CUSTOMER_ID);
				pst.setString(1, cid);
				rs = pst.executeQuery();
				rs.first();
				customerId	=	rs.getString("Customer_Id");
 				 
				 
				
				 
			}
		}
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ServiceDAO[editPersonalInfo]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return customerId;
	}
	
	public List<EralisCommonDTO> getRenewalHistoryList(ServiceDTO vo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> renewalHistory = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		
		try
		{
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				pst = conn.prepareStatement(GET_RENEWAL_HISTORY_LIST);
				pst.setString(1, vo.getVehicleRegDtlsId());
				
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new EralisCommonDTO();
					//dto.setRenewalHistoryId(rs.getString("Vehicle_Renewal_Id"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setRenewalDate(rs.getString("renewalDate"));
					dto.setExpiryDate(rs.getString("expDate"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setReceiptDate(rs.getString("receiptDate"));
					//add more if required
					
					renewalHistory.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at ServiceDAO[getRenewalHistoryDtls]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return renewalHistory;
	}
	
	public String getServiceCode(String appNo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String serviceCode = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_SERVICE_CODE);
			pst.setString(1, appNo);
			rs = pst.executeQuery();
			
			if(rs!=null)
				if (rs.first()) 
				{
					serviceCode = rs.getString(1);
				}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return serviceCode;
	}
	
	public ApplicationStatusDTO retrieveApplicationStatus(String applicationNo,String serviceType,String cid,String LLNO,String dlNo,String VNo,String vehicleType) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String W_STATUS_NAME = "Status_Name";
		String W_STATUS_CODE = "Status_Type_Short_Desc";
		String W_ACTION_DATE = "Action_Date";
		String W_ACTOR_NAME = "Actor_Name";
		String W_ROLE_NAME = "Role_Name";
		
	    ApplicationStatusDTO appStatusDto = new ApplicationStatusDTO();
	    List<ApplicationStatusTrailDTO> appStatusTrailList = new ArrayList<ApplicationStatusTrailDTO>();
	    
	    try
	    {
	    	conn = ConnectionManager.getConnection();
	    	if("VL".equalsIgnoreCase(serviceType) && applicationNo.equalsIgnoreCase(""))
	    	{
	    		pst = conn.prepareStatement(GET_APPLICATION_STATUS_OF_VEHICLE_SERVICE);
		    	pst.setString(1, cid);
		    	pst.setString(2, cid);
		    	pst.setString(3, vehicleType);
		    	pst.setString(4, vehicleType);
		    	pst.setString(5, VNo);
		    	rs = pst.executeQuery() ;
		    	while(rs.next())
		    	{
		    		ApplicationStatusTrailDTO appStatusTrailDto = new ApplicationStatusTrailDTO();
		    		appStatusTrailDto.setAction(rs.getString(W_STATUS_NAME));
		    		appStatusTrailDto.setActor(rs.getString(W_ACTOR_NAME));
		    		appStatusTrailDto.setDate(rs.getString(W_ACTION_DATE));
		    		appStatusTrailDto.setRole(rs.getString(W_ROLE_NAME));
		    		appStatusTrailDto.setActionCode(rs.getString(W_STATUS_CODE));
		    		appStatusTrailDto.setApplicationNo(rs.getString("Application_Number"));
		    		appStatusTrailDto.setClaimedBy(rs.getString("claimed_by"));
		    		appStatusTrailDto.setServiceName(rs.getString("service_name"));
		    		appStatusTrailDto.setServiceTypeNo(rs.getString("serviceTypeNo"));
		    		appStatusTrailDto.setSubmittedTo(rs.getString("submittedTo"));
		    		appStatusTrailDto.setCid(rs.getString("cid"));
		    		appStatusTrailList.add(appStatusTrailDto);
		    	}
	    	}
	    	else if("DL".equalsIgnoreCase(serviceType) && applicationNo.equalsIgnoreCase(""))
	    	{
	    		pst = conn.prepareStatement(GET_APPLICATION_STATUS_OF_DRIVING_LICENSE_SERVICE);
		    	pst.setString(1, cid);
		    	pst.setString(2, cid);
		    	pst.setString(3, dlNo);
		    	pst.setString(4, dlNo);
		    	rs = pst.executeQuery() ;
		    	while(rs.next())
		    	{
		    		ApplicationStatusTrailDTO appStatusTrailDto = new ApplicationStatusTrailDTO();
		    		appStatusTrailDto.setAction(rs.getString(W_STATUS_NAME));
		    		appStatusTrailDto.setActor(rs.getString(W_ACTOR_NAME));
		    		appStatusTrailDto.setDate(rs.getString(W_ACTION_DATE));
		    		appStatusTrailDto.setRole(rs.getString(W_ROLE_NAME));
		    		appStatusTrailDto.setActionCode(rs.getString(W_STATUS_CODE));
		    		appStatusTrailDto.setApplicationNo(rs.getString("Application_Number"));
		    		appStatusTrailDto.setClaimedBy(rs.getString("claimed_by"));
		    		appStatusTrailDto.setServiceName(rs.getString("service_name"));
		    		appStatusTrailDto.setServiceTypeNo(rs.getString("serviceTypeNo"));
		    		appStatusTrailDto.setSubmittedTo(rs.getString("submittedTo"));
		    		appStatusTrailDto.setCid(rs.getString("cid"));
		    		appStatusTrailList.add(appStatusTrailDto);
		    	}
	    	}
	    	else if("LL".equalsIgnoreCase(serviceType) && applicationNo.equalsIgnoreCase(""))
	    	{
	    		pst = conn.prepareStatement(GET_APPLICATION_STATUS_OF_LEARNER_LICENSE_SERVICE);
		    	pst.setString(1, cid);
		    	pst.setString(2, cid);
		    	pst.setString(3, LLNO);
		    	pst.setString(4, LLNO);
		    	rs = pst.executeQuery() ;
		    	while(rs.next())
		    	{
		    		ApplicationStatusTrailDTO appStatusTrailDto = new ApplicationStatusTrailDTO();
		    		appStatusTrailDto.setAction(rs.getString(W_STATUS_NAME));
		    		appStatusTrailDto.setActor(rs.getString(W_ACTOR_NAME));
		    		appStatusTrailDto.setDate(rs.getString(W_ACTION_DATE));
		    		appStatusTrailDto.setRole(rs.getString(W_ROLE_NAME));
		    		appStatusTrailDto.setActionCode(rs.getString(W_STATUS_CODE));
		    		appStatusTrailDto.setApplicationNo(rs.getString("Application_Number"));
		    		appStatusTrailDto.setClaimedBy(rs.getString("claimed_by"));
		    		appStatusTrailDto.setServiceName(rs.getString("service_name"));
		    		appStatusTrailDto.setServiceTypeNo(rs.getString("serviceTypeNo"));
		    		appStatusTrailDto.setSubmittedTo(rs.getString("submittedTo"));
		    		appStatusTrailDto.setCid(rs.getString("cid"));
		    		appStatusTrailList.add(appStatusTrailDto);
		    	}
	    	}
	    	else
	    	{
		    	pst = conn.prepareStatement(GET_ALL_WORKFLOW_STATUS_LIST);
		    	pst.setString(1, applicationNo);
		    	pst.setString(2, applicationNo);
		    	rs = pst.executeQuery() ;
		    	while(rs.next())
		    	{
		    		ApplicationStatusTrailDTO appStatusTrailDto = new ApplicationStatusTrailDTO();
		    		appStatusTrailDto.setAction(rs.getString(W_STATUS_NAME));
		    		appStatusTrailDto.setActor(rs.getString(W_ACTOR_NAME));
		    		appStatusTrailDto.setDate(rs.getString(W_ACTION_DATE));
		    		appStatusTrailDto.setRole(rs.getString(W_ROLE_NAME));
		    		appStatusTrailDto.setActionCode(rs.getString(W_STATUS_CODE));
		    		appStatusTrailDto.setApplicationNo(rs.getString("Application_Number"));
		    		appStatusTrailDto.setClaimedBy(rs.getString("claimed_by"));
		    		appStatusTrailDto.setServiceName(rs.getString("service_name"));
		    		appStatusTrailDto.setServiceTypeNo(rs.getString("serviceTypeNo"));
		    		appStatusTrailDto.setSubmittedTo(rs.getString("submittedTo"));
		    		appStatusTrailDto.setCid(rs.getString("cid"));
		    		appStatusTrailList.add(appStatusTrailDto);
		    	}
	    	}
	    	if(appStatusTrailList.size() > 0)
	    	{
	    		Collections.sort(appStatusTrailList, new DateComparatorFromStringFormat());
	    		appStatusDto.setAppStatusTrailList(appStatusTrailList);
	    		appStatusDto.setLatestStatus(appStatusTrailList.get(0).getAction());
	    		appStatusDto.setLatestStatusCode(appStatusTrailList.get(0).getActionCode());
	    		appStatusDto.setRole(appStatusTrailList.get(0).getRole());
	    	}
	    	else
	    	{
	    		appStatusDto.setLatestStatusCode("invalid");
	    	}
	    	
	    }
	    catch (SQLException e) 
	    {e.printStackTrace();
		  appStatusDto.setLatestStatusCode("invalid");
		  throw new ERALISException(e);
		}
	    catch (Exception e) 
	    {
		  appStatusDto.setLatestStatusCode("invalid");
		  e.printStackTrace();
		  throw new ERALISException(e);
		}
	    finally
	    {
			ConnectionManager.close(conn, null, rs, pst);
		}
	    
	    return appStatusDto;
	}
	
	public List<LicenseDTO> searchCustomerOffence(ServiceDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		//String where = "";
		String query = "";
		List<LicenseDTO> offenceList = new ArrayList<LicenseDTO>();
		try {
			int count = 1;
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				
				if(dto.getType().equalsIgnoreCase("searchUnpaidOffence"))
				{
					query = GET_VEHICLE_UNPAID_OFFENCE_LIST;
					pst = conn.prepareStatement(query);
					// CLAUSE VALUE
					pst.setString(count++, dto.getVehicleNo());
					pst.setString(count++, dto.getVehicleType());
					
					rs = pst.executeQuery();
					while(rs.next())
					{
						LicenseDTO liscenseDTO	=	new LicenseDTO();
						liscenseDTO.setOffenceId(rs.getString("Offence_Id"));
						liscenseDTO.setTINno(rs.getString("TIN_No"));
						liscenseDTO.setOffencedate(rs.getString("offenceDate"));
						liscenseDTO.setStatus(rs.getString("paidStatus"));
						liscenseDTO.setPlaceofInspection(rs.getString("placeOfInspection"));
						//offenceList	=	CommonDAO.getInstance().get_offence_list(liscenseDTO);
						offenceList.add(liscenseDTO);
					}
					
				}
				else
				{
					/*query = GET_OFFENCE_LIST;
					pst = conn.prepareStatement(query);
					pst.setString(count++, dto.getVehicleNo());
					pst.setString(count++, dto.getVehicleType());
					
					rs = pst.executeQuery();
					rs.first();
					LicenseDTO liscenseDTO	=	new LicenseDTO();
					liscenseDTO.setOffenceId(rs.getString("Offence_Id"));
					liscenseDTO.setRowCount(rs.getString("rowCount"));*/
					LicenseDTO liscenseDTO	=	new LicenseDTO();
					liscenseDTO.setOffenceId(dto.getOffenceId());
					offenceList	=	CommonDAO.getInstance().get_offence_list(liscenseDTO);
					
					
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at CommonDAO[getLearnerLicenseInfoList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return offenceList;
	}
	
	
	public synchronized String paymentResponse(String applicationNo,String paymentDate,String txnId,String txnAmount,String paymentStatus, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		String ServiceName	=	"";
		String validity	=	null;
		String customerName	=	null;
		String moduleNo	=	null;
		String refrenceNo	=	null; 
		String vehicleType	=	null; 
		String renewalDuration = null;
		String receiptNoSubString	=	null;
		String serviceType	=	null;
		String receiptSequence = null;
		String regionId = null;
		String etestSlNo = null;
		String locationName = null;
		String testDate = null;
		String serviceShortCode = null;
		try 
		{
			if(conn != null)
			{
				if(applicationNo.contains("B")){
					
					if(paymentStatus.equalsIgnoreCase("PAID")){
						
						pst = conn.prepareStatement("SELECT COUNT(*)rowCount FROM `t_etest_application` a WHERE a.`Prebooking_Application_No`=?");
					    pst.setString(1,applicationNo);
					    rs = pst.executeQuery();
					    rs.first();
					    if(rs.getInt("rowCount")==0)
					    {
					    	pst = conn.prepareStatement(INSERT_T_ETEST_APPLICATION, PreparedStatement.RETURN_GENERATED_KEYS);
							pst.setString(1, applicationNo);
							int count = pst.executeUpdate();
							
							if(count > 0){
								rs = pst.getGeneratedKeys();
								while (rs.next()) {
									etestSlNo = rs.getString(1);
								}
								pst = conn.prepareStatement("UPDATE `t_etest_application_prebooking` a SET a.`Status`='BOOKED' WHERE a.`Application_Number`=?");
								pst.setString(1, applicationNo);
								count = pst.executeUpdate();
								if(count > 0){
									pst = conn.prepareStatement(INSERT_INTO_T_PAYMENT_DTLS);
									pst.setString(1, etestSlNo);
									pst.setString(2, txnAmount);
									pst.setString(3, txnId);
									pst.setString(4, paymentDate);
									pst.setString(5, "CITIZEN");
									count = pst.executeUpdate();
									
									if(count > 0){
										result = "SUCCESS";
										serviceType = "BOOKING";
										
										pst = conn.prepareStatement(GET_TEST_DETAILS);
									    pst.setString(1,applicationNo);
									    rs = pst.executeQuery();
									    rs.first();
									    moduleNo	=	rs.getString("Learner_License_No");
									    customerName = rs.getString("customerName");
									    testDate	=	rs.getString("testDate");
									    refrenceNo = rs.getString("driveType");
									    locationName = rs.getString("locationName");
									    
									  /*  pst = conn.prepareStatement(GET_LICENSE_OWNER_NUMBER);
										pst.setString(1, rs.getString("Learner_License_No"));
										rs = pst.executeQuery();
										rs.first();
										
										NotificationDTO notifyDTO = new NotificationDTO();
										notifyDTO.setTestDate(customerName);
										notifyDTO.setDriveType(refrenceNo);
										notifyDTO.setTestLocation(locationName);
										notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
										SMSModelVO smsVO = new SMSModelVO();
										smsVO.setSmsType(NotificationConstants.SMS_TEMPLATE_TEST_BOOKING_NOTIFICATION);
										LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);*/
									}
								}
							}
					    }
					    else{
					    	result 	= "ALREADY_APPLIED";
							//result = "SUCCESS";
							serviceType = "BOOKING";
							
							pst = conn.prepareStatement(GET_TEST_DETAILS);
						    pst.setString(1,applicationNo);
						    rs = pst.executeQuery();
						    rs.first();
						    moduleNo	=	rs.getString("Learner_License_No");
						    customerName = rs.getString("customerName");
						    testDate	=	rs.getString("testDate");
						    refrenceNo = rs.getString("driveType");
						    locationName = rs.getString("locationName");
						}
					}
				}
				else
				{
					String serviceId = "000";
					if(applicationNo.length() > 10)
					serviceId = applicationNo.substring(2, 5);
					
					pst = conn.prepareStatement(GET_SERVICE_SHORT_DESC);
					pst.setString(1, serviceId);
					rs = pst.executeQuery();
					rs.first();
					serviceShortCode = rs.getString("Service_Short_Desc");
					
					if(serviceShortCode!=null)
					{
						/*if(!paymentStatus.equalsIgnoreCase("PAID"))
						{
							pst	=	conn.prepareStatement(DELETE_FROM_TASK_DTLS_AUDIT);
							pst.setString(1, applicationNo); 
							pst.executeUpdate();
							pst	=	conn.prepareStatement(DELETE_FROM_TASK_DTLS);
							pst.setString(1, applicationNo); 
							pst.executeUpdate();
							pst	=	conn.prepareStatement(DELETE_FROM_WORKFLOW_DTLS_AUDIT);
							pst.setString(1, applicationNo); 
							pst.executeUpdate();
							pst	=	conn.prepareStatement(DELETE_FROM_WORKFLOW_DTLS);
							pst.setString(1, applicationNo); 
							pst.executeUpdate();
							pst	=	conn.prepareStatement(DELETE_FROM_PAYMENT_DTLS);
							pst.setString(1, applicationNo); 
							pst.executeUpdate();
						}*/
						if(serviceShortCode.equalsIgnoreCase("ILL"))
						{
							int checkApplicationExist = checkApplicationExist(applicationNo,conn);
							if(checkApplicationExist==0 && paymentStatus.equalsIgnoreCase("PAID"))
							{
								serviceType	=	"New Learner";
								pst	=	conn.prepareStatement(UPDATE_LEARNER_LICENSE_APPLICATION_PAYMENT);
								pst.setString(1, paymentDate); 
								pst.setString(2, txnId); 
								pst.setString(3, applicationNo); 
								int count = pst.executeUpdate();
								if(count > 0)
								{	
									pst	=	conn.prepareStatement(UPDATE_T_PAYMENT_DTLS);
									pst.setString(1, paymentDate); 
									pst.setString(2, txnId); 
									pst.setString(3, applicationNo); 
									count = pst.executeUpdate();
									if(count > 0)
									{
										pst = conn.prepareStatement(GET_LL_APPLICATION_DTLS);
									    pst.setString(1,applicationNo);
									    rs = pst.executeQuery();
									    rs.first();
									    customerName	=	rs.getString("customerName");
									    moduleNo	=	rs.getString("Learner_License_No");
										
										result 	= "SUCCESS";
										if(serviceShortCode.equalsIgnoreCase("ILL"))
										{
											ServiceName	=	"Learner License Registration";
										}
									}
								}
							}
							else
							{
								pst = conn.prepareStatement(GET_LL_APPLICATION_DTLS);
							    pst.setString(1,applicationNo);
							    rs = pst.executeQuery();
							    rs.first();
							    customerName	=	rs.getString("customerName");
							    moduleNo	=	rs.getString("Learner_License_No");
							    locationName	=	rs.getString("submittedTo");
							    
								
								result 	= "SUCCESS";
								if(serviceShortCode.equalsIgnoreCase("ILL"))
								{
									ServiceName	=	"Learner License Registration";
								}
							}
						}
						else if(serviceShortCode.equalsIgnoreCase("RLL") || serviceShortCode.equalsIgnoreCase("DLL"))
						{
							serviceType	=	"Learner";
							int checkApplicationExist = checkApplicationExist(applicationNo,conn);
							if(checkApplicationExist==0 && paymentStatus.equalsIgnoreCase("PAID"))
							{
								pst	= conn.prepareStatement(INSERT_INTO_LL_APPLICATION_FROM_ONLINE_PAYMENT);
								//pst	=	conn.prepareStatement(UPDATE_LEARNER_LICENSE_APPLICATION_PAYMENT);
								pst.setString(1, paymentDate); 
								pst.setString(2, txnId); 
								pst.setString(3, applicationNo); 
								int count = pst.executeUpdate();
								if(count > 0)
								{	
									paymentWorkflowAndTasklistSubmission(applicationNo,conn);
									pst	= conn.prepareStatement(INSERT_INTO_T_PAYMENT_DTLS_FROM_ONLINE_PAYMENT);
									//pst	=	conn.prepareStatement(UPDATE_T_PAYMENT_DTLS);
									pst.setString(1, paymentDate); 
									pst.setString(2, txnId); 
									pst.setString(3, applicationNo); 
									count = pst.executeUpdate();
									if(count > 0)
									{
										pst = conn.prepareStatement(GET_LL_APPLICATION_DTLS);
									    pst.setString(1,applicationNo);
									    rs = pst.executeQuery();
									    rs.first();
									    customerName	=	rs.getString("customerName");
									    moduleNo	=	rs.getString("Learner_License_No");
									    renewalDuration = rs.getString("Renewal_Duration");
									    locationName	=	rs.getString("submittedTo");
									    
										if(serviceShortCode.equalsIgnoreCase("ILL"))
										{
											ServiceName	=	"Learner License Registration";
										}
										else if(serviceShortCode.equalsIgnoreCase("RLL"))
										{
											pst = conn.prepareStatement(GET_EXPIRY_FOR_LL_RENEWAL);
											pst.setString(1, rs.getString("Learner_License_Id"));
											rs = pst.executeQuery();
											rs.first();
											//java.util.Date nextExpiry = targetSdf.parse(rs.getString("expiryDate"));
											//validity = sourceSdf.format(nextExpiry);
											validity = rs.getString("expiryDate");
											LicenseDTO dto = new LicenseDTO();
											UserDetailsVO userVo = new UserDetailsVO();
											dto.setApplicationNo(applicationNo);
											dto.setNextExpiry(validity);
											
											DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
											DateTime lastExpiryDate = format.parseDateTime(validity);
											String expiryDate = format.print(lastExpiryDate.plusMonths(Integer.parseInt(renewalDuration)));
											
											java.util.Date expiry = targetSdf.parse(expiryDate);
											validity = sourceSdf.format(expiry);
											ServiceName	=	"Learner License Renewal";
											
											String approvalResult = LicenseBusiness.getInstance().license_renewal_application_approval(dto, userVo);

										}
										else if(serviceShortCode.equalsIgnoreCase("DLL"))
										{
											ServiceName	=	"Learner License Replacement";
											pst = conn.prepareStatement(GET_EXPIRY_FOR_LL_RENEWAL);
											pst.setString(1, rs.getString("Learner_License_Id"));
											rs = pst.executeQuery();
											rs.first();
											//java.util.Date nextExpiry = targetSdf.parse(rs.getString("expiryDate"));
											//validity = sourceSdf.format(nextExpiry);
											validity = rs.getString("expiryDate");
										}
										result 	= "SUCCESS";
									}
								}
							}
							else
							{
								/*if(serviceShortCode.equalsIgnoreCase("ILL"))
								{
									pst	=	conn.prepareStatement(DELETE_FROM_LL_DRIVE_TYPE_APPLICATION);
									pst.setString(1, applicationNo); 
									pst.executeUpdate();
								}
								pst	=	conn.prepareStatement(DELETE_FROM_LL_APPLICATION);
								pst.setString(1, applicationNo); 
								pst.executeUpdate();*/
								pst = conn.prepareStatement(GET_LL_APPLICATION_DTLS);
							    pst.setString(1,applicationNo);
							    rs = pst.executeQuery();
							    rs.first();
							    customerName	=	rs.getString("customerName");
							    moduleNo	=	rs.getString("Learner_License_No");
							    renewalDuration = rs.getString("Renewal_Duration");
							    locationName	=	rs.getString("submittedTo");
								
							    pst = conn.prepareStatement(GET_EXPIRY_FOR_LL_RENEWAL);
								pst.setString(1, rs.getString("Learner_License_Id"));
								rs = pst.executeQuery();
								rs.first();
								//java.util.Date nextExpiry = targetSdf.parse(rs.getString("expiryDate"));
								//validity = sourceSdf.format(nextExpiry);
								validity = rs.getString("expiryDate");
								
								if(serviceShortCode.equalsIgnoreCase("ILL"))
								{
									ServiceName	=	"Learner License Registration";
								}
								else if(serviceShortCode.equalsIgnoreCase("RLL"))
								{
									DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
									DateTime lastExpiryDate = format.parseDateTime(validity);
									String expiryDate = format.print(lastExpiryDate.plusMonths(Integer.parseInt(renewalDuration)));
									
									java.util.Date expiry = targetSdf.parse(validity);
									validity = sourceSdf.format(expiry);
									
									ServiceName	=	"Learner License Renewal";
								}
								else if(serviceShortCode.equalsIgnoreCase("DLL"))
								{
									ServiceName	=	"Learner License Replacement";
								}
								result 	= "ALREADY_APPLIED";
							}
						}
						else if(serviceShortCode.equalsIgnoreCase("INDL") || serviceShortCode.equalsIgnoreCase("ICDL") || serviceShortCode.equalsIgnoreCase("RDL") || serviceShortCode.equalsIgnoreCase("DDL")|| serviceShortCode.equalsIgnoreCase("EDL"))
						{
							serviceType	=	"License";
							int checkApplicationExist = checkApplicationExist(applicationNo,conn);
							if(checkApplicationExist==0 && paymentStatus.equalsIgnoreCase("PAID"))
							{
								
								//pst	=	conn.prepareStatement(UPDATE_DRIVING_LICENSE_APPLICATION_PAYMENT);
								pst	=	conn.prepareStatement(INSERT_INTO_DL_APPLICATION_FROM_ONLINE_PAYMENT);
								pst.setString(1, paymentDate); 
								pst.setString(2, txnId); 
								pst.setString(3, applicationNo); 
								int count = pst.executeUpdate();
								if(count > 0)
								{
									paymentWorkflowAndTasklistSubmission(applicationNo,conn);
									pst	=	conn.prepareStatement(INSERT_INTO_T_PAYMENT_DTLS_FROM_ONLINE_PAYMENT);
									//pst	=	conn.prepareStatement(UPDATE_T_PAYMENT_DTLS);
									pst.setString(1, paymentDate); 
									pst.setString(2, txnId); 
									pst.setString(3, applicationNo); 
									count = pst.executeUpdate();
									if(count > 0)
									{
										pst = conn.prepareStatement(GET_DL_APPLICATION_DTLS);
									    pst.setString(1,applicationNo);
									    rs = pst.executeQuery();
									    rs.first(); 
									    
									    locationName	=	rs.getString("submittedTo");
									    customerName	=	rs.getString("customerName");
									    moduleNo	=	rs.getString("Driving_License_No");
									    renewalDuration = 	rs.getString("Renewal_Duration");
									    receiptNoSubString	=	txnId.substring(txnId.length() - 4);
									    System.out.println(rs.getString("Driving_License_Id"));
									    pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_RENEWAL);
									    pst.setString(1,rs.getString("Driving_License_Id"));
									    pst.setString(2,rs.getString("Driving_License_Id"));
									    pst.setString(3,rs.getString("Driving_License_Id"));
									    rs = pst.executeQuery();
									    rs.first();  
									    validity	=	rs.getString("expiryDate");
									    
										if(serviceShortCode.equalsIgnoreCase("RDL"))
										{
											
											DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
											DateTime lastExpiryDate = format.parseDateTime(validity);
											
											String[] duration	=	renewalDuration.split("_");
											String p = null;
											if(duration[1].equalsIgnoreCase("month"))
											{
												DateTime a = lastExpiryDate.plusMonths(Integer.parseInt(duration[0]));
												p = a.toString();
											}
											else if(duration[1].equalsIgnoreCase("days"))
											{
												DateTime a = lastExpiryDate.plusDays(Integer.parseInt(duration[0]));
												p = a.toString();
											}
											java.util.Date nextExpiry = targetSdf.parse(p);
											//java.util.Date nextExpiry = targetSdf.parse(validity);
											validity = sourceSdf.format(nextExpiry);
											 
											ServiceName	=	"Driving License Renewal";
										}
										else if(serviceShortCode.equalsIgnoreCase("DDL"))
										{
											java.util.Date nextExpiry = targetSdf.parse(validity);
											validity = sourceSdf.format(nextExpiry);
											ServiceName	=	"Driving License Replacement";
										}
										result 	= "SUCCESS";
									}
								}
							}
							else
							{
								pst = conn.prepareStatement(GET_DL_APPLICATION_DTLS);
							    pst.setString(1,applicationNo); 
							    rs = pst.executeQuery(); 
							    rs.first(); 
							    
							    customerName	=	rs.getString("customerName");  
							    moduleNo	=	rs.getString("Driving_License_No");
							    renewalDuration = 	rs.getString("Renewal_Duration");
							    locationName	=	rs.getString("submittedTo");
							    receiptNoSubString	=	txnId.substring(txnId.length() - 4);
							    
							    pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_RENEWAL);
							    pst.setString(1,rs.getString("Driving_License_Id"));
							    pst.setString(2,rs.getString("Driving_License_Id"));
							    pst.setString(3,rs.getString("Driving_License_Id"));
							    rs = pst.executeQuery();
							    rs.first();  
							    validity	=	rs.getString("expiryDate");
							    
								if(serviceShortCode.equalsIgnoreCase("RDL"))
								{
									
									/*DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
									DateTime lastExpiryDate = format.parseDateTime(validity);
									
									String[] duration	=	renewalDuration.split("_");
									String p = null;
									if(duration[1].equalsIgnoreCase("month"))
									{
										DateTime a = lastExpiryDate.plusMonths(Integer.parseInt(duration[0]));
										p = a.toString();
									}
									else if(duration[1].equalsIgnoreCase("days"))
									{
										DateTime a = lastExpiryDate.plusDays(Integer.parseInt(duration[0]));
										p = a.toString();
									}*/
									//java.util.Date nextExpiry = targetSdf.parse(p);
									java.util.Date nextExpiry = targetSdf.parse(validity);
									validity = sourceSdf.format(nextExpiry);
									
									ServiceName	=	"Driving License Renewal";
								}
								else if(serviceShortCode.equalsIgnoreCase("DDL"))
								{
									java.util.Date nextExpiry = targetSdf.parse(validity);
									validity = sourceSdf.format(nextExpiry);
									ServiceName	=	"Driving License Replacement";
								}
								result 	= "ALREADY_APPLIED";
							}
						}
						else if(serviceShortCode.equalsIgnoreCase("REGV") || serviceShortCode.equalsIgnoreCase("RV") 
								|| serviceShortCode.equalsIgnoreCase("TV") || serviceShortCode.equalsIgnoreCase("DV")
								|| serviceShortCode.equalsIgnoreCase("CV"))
						{
							serviceType	=	"Vehicle";
							int checkApplicationExist = checkApplicationExist(applicationNo,conn);
							if(checkApplicationExist==0 && paymentStatus.equalsIgnoreCase("PAID"))
							{
								pst	=	conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_FROM_PAYMENT_DB);
								//pst	=	conn.prepareStatement(UPDATE_VEHICLE_APPLICATION_PAYMENT);
								pst.setString(1, paymentDate); 
								pst.setString(2, txnId); 
								pst.setString(3, applicationNo); 
								int count = pst.executeUpdate();
								if(count > 0)
								{	
									System.out.println(INSERT_INTO_T_PAYMENT_DTLS_FROM_ONLINE_PAYMENT);
									paymentWorkflowAndTasklistSubmission(applicationNo,conn);
									pst	=	conn.prepareStatement(INSERT_INTO_T_PAYMENT_DTLS_FROM_ONLINE_PAYMENT);
								//	pst	=	conn.prepareStatement(UPDATE_T_PAYMENT_DTLS);
									pst.setString(1, paymentDate); 
									pst.setString(2, txnId); 
									pst.setString(3, applicationNo); 
									count = pst.executeUpdate();
									if(count > 0)
									{	
										pst = conn.prepareStatement(GET_VEHICLE_APPLICATION_DTLS);
									    pst.setString(1,applicationNo);
									    rs = pst.executeQuery();
									    rs.first(); 
									   // customerName	=	rs.getString("customerName");
									    moduleNo	=	rs.getString("Vehicle_Number");
									    renewalDuration = 	rs.getString("Renewal_Duration");
									    vehicleType 	= 	rs.getString("vehicleType");
									    receiptNoSubString	=	txnId.substring(0, 4);
									    String ownerType	=	rs.getString("Vehicle_Registration_Type");
									    String customerId	=	rs.getString("Customer_Id");
									    locationName	=	rs.getString("submittedTo");
									    
									    pst = conn.prepareStatement(GET_VEHICLE_EXPIRY_DATE);
									    pst.setString(1,rs.getString("Vehicle_Id"));
									    pst.setString(2,rs.getString("Vehicle_Id"));
									    pst.setString(3,rs.getString("Vehicle_Id"));
									    rs = pst.executeQuery();
									    rs.first();  
									    validity	=	rs.getString("expiryDate");
									    
									    if(serviceShortCode.equalsIgnoreCase("RV"))
										{
											DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
											DateTime lastExpiryDate = format.parseDateTime(rs.getString("expiryDate"));
											
											lastExpiryDate.plusMonths(Integer.parseInt(renewalDuration));
											String expiryDate = format.print(lastExpiryDate.plusMonths(Integer.parseInt(renewalDuration)));
											java.util.Date expiry = targetSdf.parse(expiryDate);
											validity = sourceSdf.format(expiry);
											ServiceName	=	"Vehicle Renewal";
											
										}
										if(serviceShortCode.equalsIgnoreCase("DV"))
										{
											java.util.Date expiry = targetSdf.parse(validity);
											validity = sourceSdf.format(expiry);
											ServiceName	=	"Vehicle Replacement";
										}
									    
									    
									    if(ownerType.equals("P")){
											pst = conn.prepareStatement(GET_INDIVIDUAL_CUSTOMER_NAME);
											pst.setString(1, customerId);
											rs = pst.executeQuery();
											rs.first();
											customerName = rs.getString("customerName");
										} else if(ownerType.equals("O")){
											pst = conn.prepareStatement(GET_ORGANIZATION_TYPE);
											pst.setString(1, customerId);
											rs = pst.executeQuery();
											rs.first();
											
											if(rs.getString("organizationType").equals("GOVERNMENT")){
												String ministryId = rs.getString("Ministry_Id");
												String departmentId = rs.getString("Department_Id");
												pst = conn.prepareStatement(GET_MINISTRY_NAME);
												pst.setString(1, ministryId);
												rs = pst.executeQuery();
												rs.first();
												customerName = rs.getString("Ministry_Name");
												if(Integer.parseInt(departmentId) != 0)
												{
													pst = conn.prepareStatement(GET_DEPARTMENT_NAME);
													pst.setString(1, departmentId);
													rs = pst.executeQuery();
													rs.first();
													customerName = customerName + "," + rs.getString("Department_Name");
												}
											} else if(rs.getString("organizationType").equals("PRIVATE")){
												pst = conn.prepareStatement(GET_PRIVATE_NAME);
												pst.setString(1, rs.getString("Private_Id"));
												rs = pst.executeQuery();
												rs.first();
												customerName = rs.getString("Private_Name");
											} else {
												customerName = rs.getString("Organization_Name");
											}
										}
									    result 	= "SUCCESS";
									}
								}
							}
							else
							{
								
								pst = conn.prepareStatement(GET_VEHICLE_APPLICATION_DTLS);
							    pst.setString(1,applicationNo);
							    rs = pst.executeQuery();
							    rs.first(); 
							   // customerName	=	rs.getString("customerName");
							    moduleNo	=	rs.getString("Vehicle_Number");
							    renewalDuration = 	rs.getString("Renewal_Duration");
							    vehicleType 	= 	rs.getString("vehicleType");
							    receiptNoSubString	=	txnId.substring(0, 4);
							    String ownerType	=	rs.getString("Vehicle_Registration_Type");
							    String customerId	=	rs.getString("Customer_Id");
							    locationName	=	rs.getString("submittedTo");
							    
							    
							    
							    if(serviceShortCode.equalsIgnoreCase("RV"))
								{
							    	validity = rs.getString("Expiry_Date");
									
							    	DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
									DateTime lastExpiryDate = format.parseDateTime(rs.getString("Expiry_Date"));
									
									lastExpiryDate.plusMonths(Integer.parseInt(renewalDuration));
									String expiryDate = format.print(lastExpiryDate.plusMonths(Integer.parseInt(renewalDuration)));
									
									
									java.util.Date expiry = targetSdf.parse(expiryDate);
									validity = sourceSdf.format(expiry);
									ServiceName	=	"Vehicle Renewal";
									
								}
							    
							    
							    
								if(serviceShortCode.equalsIgnoreCase("DV"))
								{
									pst = conn.prepareStatement(GET_VEHICLE_EXPIRY_DATE);
								    pst.setString(1,rs.getString("Vehicle_Id"));
								    pst.setString(2,rs.getString("Vehicle_Id"));
								    pst.setString(3,rs.getString("Vehicle_Id"));
								    rs = pst.executeQuery();
								    rs.first();  
								    validity	=	rs.getString("expiryDate");
								    
									java.util.Date expiry = targetSdf.parse(validity);
									validity = sourceSdf.format(expiry);
									ServiceName	=	"Vehicle Replacement";
								}
							    
							    
							    if(ownerType.equals("P")){
									pst = conn.prepareStatement(GET_INDIVIDUAL_CUSTOMER_NAME);
									pst.setString(1, customerId);
									rs = pst.executeQuery();
									rs.first();
									customerName = rs.getString("customerName");
								} else if(ownerType.equals("O")){
									pst = conn.prepareStatement(GET_ORGANIZATION_TYPE);
									pst.setString(1, customerId);
									rs = pst.executeQuery();
									rs.first();
									
									if(rs.getString("organizationType").equals("GOVERNMENT")){
										String ministryId = rs.getString("Ministry_Id");
										String departmentId = rs.getString("Department_Id");
										pst = conn.prepareStatement(GET_MINISTRY_NAME);
										pst.setString(1, ministryId);
										rs = pst.executeQuery();
										rs.first();
										customerName = rs.getString("Ministry_Name");
										if(Integer.parseInt(departmentId) != 0)
										{
											pst = conn.prepareStatement(GET_DEPARTMENT_NAME);
											pst.setString(1, departmentId);
											rs = pst.executeQuery();
											rs.first();
											customerName = customerName + "," + rs.getString("Department_Name");
										}
									} else if(rs.getString("organizationType").equals("PRIVATE")){
										pst = conn.prepareStatement(GET_PRIVATE_NAME);
										pst.setString(1, rs.getString("Private_Id"));
										rs = pst.executeQuery();
										rs.first();
										customerName = rs.getString("Private_Name");
									} else {
										customerName = rs.getString("Organization_Name");
									}
								}
							    result 	= "ALREADY_APPLIED";
							}
						}
						else if(serviceShortCode.equalsIgnoreCase("FITNESS"))
						{
							serviceType	=	"Vehicle";
							String fitnessId = applicationNo.substring(5, applicationNo.length());

							pst = conn.prepareStatement(UPDATE_PAYMENT_TABLE);
							pst.setString(1, txnId);
							pst.setString(2, paymentDate);
							pst.setString(3, fitnessId);
							pst.executeUpdate();
							
							pst = conn.prepareStatement(UPDATE_FITNESS_PAYMENT_DETAILS);
							pst.setString(1, txnId);
							pst.setString(2, paymentDate);
							pst.setString(3, "Citizen");
							pst.setString(4, paymentDate);
							pst.setString(5, fitnessId);
							pst.executeUpdate();
							
							pst = conn.prepareStatement(GET_VEHICLE_DETAILS_FOR_FITNESS);
							pst.setString(1, fitnessId);
							rs = pst.executeQuery();
							rs.first();
							
							moduleNo	=	rs.getString("Vehicle_Number");
							String ownerType = rs.getString("Vehicle_Registration_Type");
							String customerId = rs.getString("Customer_Id");
							vehicleType 	= 	rs.getString("vehicleType");
						    receiptNoSubString	=	txnId.substring(0, 4);
						    validity = rs.getString("validity");
							
							if(ownerType.equals("P")){
								pst = conn.prepareStatement(GET_INDIVIDUAL_CUSTOMER_NAME);
								pst.setString(1, customerId);
								rs = pst.executeQuery();
								rs.first();
								customerName = rs.getString("customerName");
							} else if(ownerType.equals("O")){
								pst = conn.prepareStatement(GET_ORGANIZATION_TYPE);
								pst.setString(1, customerId);
								rs = pst.executeQuery();
								rs.first();
								
								if(rs.getString("organizationType").equals("GOVERNMENT")){
									String ministryId = rs.getString("Ministry_Id");
									String departmentId = rs.getString("Department_Id");
									pst = conn.prepareStatement(GET_MINISTRY_NAME);
									pst.setString(1, ministryId);
									rs = pst.executeQuery();
									rs.first();
									customerName = rs.getString("Ministry_Name");
									if(Integer.parseInt(departmentId) != 0)
									{
										pst = conn.prepareStatement(GET_DEPARTMENT_NAME);
										pst.setString(1, departmentId);
										rs = pst.executeQuery();
										rs.first();
										customerName = customerName + "," + rs.getString("Department_Name");
									}
								} else if(rs.getString("organizationType").equals("PRIVATE")){
									pst = conn.prepareStatement(GET_PRIVATE_NAME);
									pst.setString(1, rs.getString("Private_Id"));
									rs = pst.executeQuery();
									rs.first();
									customerName = rs.getString("Private_Name");
								} else {
									customerName = rs.getString("Organization_Name");
								}
							}
							
							result 	= "SUCCESS";
							ServiceName	=	"Vehicle Fitness";
						}
						else if(serviceShortCode.equalsIgnoreCase("TOP") || serviceShortCode.equalsIgnoreCase("TOPR")){
							
						}
					}
					else
					{
						if(paymentStatus.equalsIgnoreCase("PAID"))
						{
							serviceType	=	"Offence";
							pst	=	conn.prepareStatement(UPDATE_OFFENCE_PAYMENT);
							pst.setString(1, paymentDate);
							pst.setString(2, txnId); 
							pst.setString(3, txnAmount); 
							pst.setString(4, applicationNo); 
							int count = pst.executeUpdate();
							if(count > 0)
							{
								pst = conn.prepareStatement(GET_OFFENCE_DTLS);
							    pst.setString(1,applicationNo);
							    rs = pst.executeQuery();
							    rs.first(); 
							    customerName=	rs.getString("customerName");
							    moduleNo	=	rs.getString("Vehicle_Number")+"~"+rs.getString("TIN_No");
							    vehicleType = 	rs.getString("Vehicle_Type_Name");
							    regionId = rs.getString("Region_Id");
							    
								result 	= "SUCCESS";
								ServiceName	=	"Offence";
								
								NotificationDTO notifyDTO = new NotificationDTO();
								notifyDTO.setTinNo(rs.getString("TIN_No"));
								notifyDTO.setAmount(txnAmount);
								notifyDTO.setPaymentDate(paymentDate);
								
								pst = conn.prepareStatement(GET_OWNER_PHONE_NUMBER);
								pst.setString(1, applicationNo);
								rs = pst.executeQuery();
								rs.first();
								notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
								
								SMSModelVO smsVO = new SMSModelVO();
								smsVO.setSmsType(NotificationConstants.SMS_TEMPLATE_OFFENCE_PAYMENT_UPDATE_NOTIFICATION);
								LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
							}
						}
					}
					if("Vehicle".equalsIgnoreCase(serviceType) || "License".equalsIgnoreCase(serviceType))
					{
						String dateArr[] = paymentDate.split("-");
						String month = dateArr[1];
						String day = dateArr[2];
						char customerChar	=	Character.toUpperCase(customerName.charAt(1));
						String randomNumber	=	getFourDigitRandomNumber();
						String moduleNoLastTwoChar	=	moduleNo.substring(moduleNo.length() - 2);
						
						refrenceNo	=	String.valueOf(customerChar)+randomNumber+moduleNoLastTwoChar+day+month+receiptNoSubString;
					}
					
					if(paymentStatus.equals("PAID"))
					{
						java.util.Date receiptDate = targetSdf.parse(paymentDate);
						paymentDate = sourceSdf.format(receiptDate);
						
						if(!serviceId.equals("000") && !serviceShortCode.equals("FITNESS")){
							pst = conn.prepareStatement(GET_APPLICATION_REGION_ID);
							pst.setString(1, applicationNo);
							rs = pst.executeQuery();
							rs.first();
							regionId = rs.getString("regionId");
						}
						
						//receiptSequence = generateReceiptSequence(serviceId, regionId, conn);
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ServiceDAO[editPersonalInfo]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		return result+"#"+ServiceName+"#"+refrenceNo+"#"+moduleNo+"#"+vehicleType+"#"+customerName+"#"+validity+"#"+serviceType+"#"+paymentDate+"#"+receiptSequence+"#"+locationName+"#"+testDate+"#"+serviceShortCode;
	}
	
	
	public synchronized String updatePaymentDB(String applicationNo,String paymentDate,String txnId,String txnAmount, String isPaymentSuccessful, Connection conn1) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILED";
		ResultSet rs = null;
		try {
			int count = 0;
			if(applicationNo.contains("B"))
			{
				pst = conn1.prepareStatement(CHECK_APPLICATION_EXIST_IN_PAYMENT_DB);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")==0)
				{
					pst	=	conn1.prepareStatement(INSERT_INTO_PAYMENT_DTLS_ON_UNSUCCESSFUL_PAYMENT);
					pst.setString(1, applicationNo); 
					pst.setString(2, txnAmount);
					pst.setString(3, txnId);
					pst.setString(4, paymentDate);
					pst.setString(5, "BOOKING");
					pst.executeUpdate();
					
				}
				else
				{
					pst	=	conn1.prepareStatement(UPDATE_T_PAYMENT_DTLS_OF_PAYMENT_DB);
					pst.setString(1, paymentDate); 
					pst.setString(2, txnId);
					pst.setString(3, isPaymentSuccessful);
					pst.setString(4, applicationNo);
					pst.executeUpdate();
				}
			}
			else if(applicationNo.contains("PN"))
			{
				pst	=	conn1.prepareStatement(UPDATE_T_PAYMENT_DTLS_OF_PAYMENT_DB);
				pst.setString(1, paymentDate); 
				pst.setString(2, txnId);
				pst.setString(3, isPaymentSuccessful);
				pst.setString(4, applicationNo);
				pst.executeUpdate();
			}
			else
			{
				pst = conn1.prepareStatement(CHECK_APPLICATION_EXIST_IN_PAYMENT_DB);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")==0)
				{
					pst	=	conn1.prepareStatement(INSERT_INTO_PAYMENT_DTLS_ON_UNSUCCESSFUL_PAYMENT);
					pst.setString(1, applicationNo); 
					pst.setString(2, txnAmount);
					pst.setString(3, txnId);
					pst.setString(4, paymentDate);
					pst.setString(5, "OFFENCE");
					pst.executeUpdate();
					
				}
				else
				{
					pst	=	conn1.prepareStatement(UPDATE_T_PAYMENT_DTLS_OF_PAYMENT_DB);
					pst.setString(1, paymentDate); 
					pst.setString(2, txnId);
					pst.setString(3, isPaymentSuccessful);
					pst.setString(4, applicationNo);
					pst.executeUpdate();
				}
				
			}
			
			if(count>0)
			{
				result = "SUCCESS";
			}
		}
		catch (Exception e) {
			ConnectionManager.rollbackConnection(conn1);
			throw new ERALISException("###Error at ServiceDAO[updatePaymentDB]:: "+e);
		}
		return result;
		
	}
	
	
	private synchronized String paymentWorkflowAndTasklistSubmission(String applicationNo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		String result = "FAILED";
		int count1 = 0;
		try {
			if(conn != null){
				String instanceId = null;
				pst	=	conn.prepareStatement(INSERT_INTO_T_WORKFLOW_DTLS_FROM_PAYMENT_DB,PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, applicationNo); 
				int count = pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					instanceId = rs.getString(1);
				}
				if(count > 0)
				{
					pst1 = conn.prepareStatement(INSERT_INTO_TASKDTLS_FROM_PAYMENT_DB);
					pst1.setString(1, instanceId);
					pst1.setString(2, applicationNo);  
					count1 = pst1.executeUpdate();
					if(count1 > 0)
					{
						result = "SUCCESS";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ServiceDAO[paymentWorkflowAndTasklistSubmission]:: "+e);
		} finally {
			ConnectionManager.close(null, null, rs, pst);
			ConnectionManager.close(null, null, null, pst1);
		}
		
		return result;
	}
	
	private synchronized String generateReceiptSequence(String serviceId, String regionId, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		final int ERALIS_SL_NO_PORTION_LENGTH = 8;
		String finalStrSlNo = null;
		StringBuffer receiptNo = new StringBuffer();
		String newSlNo = null;
		String slNo = null;
		
		try {
			if(serviceId.equals("000"))
				serviceId = "1";
			
			if(conn != null){
				pst = conn.prepareStatement("SELECT a.Service_Receipt_Sequence FROM t_online_payment_sequence a WHERE a.Service_Id=?");
				pst.setString(1, serviceId);
				rs = pst.executeQuery();
				rs.first();
				
				String lastSequenceNo = rs.getString("Service_Receipt_Sequence");
				
				if(null != lastSequenceNo){
					slNo = lastSequenceNo;
					if(null != slNo && !"".equalsIgnoreCase(slNo) && !EralisCommonUtil.isStringPresent(slNo))
					{
						newSlNo = String.valueOf(((new Integer(slNo)) + 1));
						int dgtCount = String.valueOf(newSlNo).length();
						StringBuffer sbf = new StringBuffer();
						for (int i = dgtCount; i < ERALIS_SL_NO_PORTION_LENGTH; i++)
							sbf.append("0");
						sbf.append(newSlNo);
						finalStrSlNo = sbf.toString();
					}
					
					receiptNo.append(EralisCommonUtil.getRegionCode(conn, regionId));
					receiptNo.append("/");
					receiptNo.append(finalStrSlNo);
					
					pst = conn.prepareStatement("UPDATE t_online_payment_sequence a SET a.Service_Receipt_Sequence=? WHERE a.Service_Id=?");
					pst.setString(1, Integer.toString(Integer.parseInt(slNo)+1));
					pst.setString(2, serviceId);
					pst.executeUpdate();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ServiceDAO[generateReceiptSequence]:: "+e);
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return receiptNo.toString();
	}
	
	
	public String getFourDigitRandomNumber() 
	{
		int minimum = 1000;
		int maximum = 9999;
		int randomNum = 0;
		randomNum = minimum + (int)(Math.random() * ((maximum - minimum) + 1));
		
	    return Integer.toString(randomNum);
	}
	
	
	public String fetLastTwoChar(String str) 
	{ 
		str = str.substring(str.length() - 2);
		return str;
	} 

	public String getLatestExpiryDate(String id, String requestType) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String latestExpiryDate = "", sql = null;
		
		try {
			conn = ConnectionManager.getConnection();
			if(conn != null){
				if(requestType.equals("VEHICLE"))
					sql = LASTEST_EXPIRY_DATE_VEHICLE;
				else if(requestType.equals("LICENSE"))
					sql = LASTEST_EXPIRY_DATE_LICENSE;
				else if(requestType.equals("LEARNER"))
					sql = LASTEST_EXPIRY_DATE_LEARNER;
				
				pst = conn.prepareStatement(sql);
				pst.setString(1, id);
				pst.setString(2, id);
				pst.setString(3, id);
				rs = pst.executeQuery();
				rs.first();
				
				java.util.Date expiryDate = targetSdf.parse(rs.getString("expiryDate"));
				latestExpiryDate = sourceSdf.format(expiryDate);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return latestExpiryDate;
	}

	public int checkApplicationExist(String applicationNo,Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = ConnectionManager.getConnection();
			if(conn != null){
				pst = conn.prepareStatement(CHECK_APPLICATION_EXIST);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				result = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return result;
	}
	/*
	 * Queries :: START
	 */
	private static final String CHECK_APPLICATION_EXIST = "SELECT COUNT(a.`Application_Number`) rowCount FROM `t_task_dtls` a WHERE a.`Application_Number`=?";
	
	private static final String LASTEST_EXPIRY_DATE_VEHICLE =  "SELECT "
		 + "  IF( "
		 + "    a.`Vehicle_Reg_Dtls_Id` IN "
		 + "    (SELECT "
		 + "      `Vehicle_Id` "
		 + "    FROM "
		 + "      t_vehicle_renewal_dtls "
		 + "    WHERE `Vehicle_Id` = ?), "
		 + "    (SELECT "
		 + "      MAX(Expiry_Date) "
		 + "    FROM "
		 + "      t_vehicle_renewal_dtls "
		 + "    WHERE `Vehicle_Id` = ?), "
		 + "    a.`Expiry_Date` "
		 + "  ) expiryDate "
		 + "FROM "
		 + "  t_vehicle_registration_dtls a "
		 + "WHERE a.`Vehicle_Reg_Dtls_Id` = ?";

	private static final String LASTEST_EXPIRY_DATE_LICENSE = "SELECT "
			+ "  IF( "
			+ "    a.`Driving_License_Id` IN "
			+ "    (SELECT "
			+ "      `License_Id` "
			+ "    FROM "
			+ "      `t_driving_license_renewal_dtls` "
			+ "    WHERE `License_Id` = ?), "
			+ "    (SELECT "
			+ "      MAX(`Expiry_Date`) "
			+ "    FROM "
			+ "      `t_driving_license_renewal_dtls` "
			+ "    WHERE `License_Id` = ?), "
			+ "    a.`Expiry_Date` "
			+ "  ) expiryDate "
			+ "FROM "
			+ "  t_driving_license_dtls a "
			+ "WHERE a.`Driving_License_Id` = ?";

	private static final String LASTEST_EXPIRY_DATE_LEARNER = "SELECT "
		+ "  IF( "
		+ "    a.`Learner_License_Info_Id` IN "
		+ "    (SELECT "
		+ "      b.`Learner_License_Id` "
		+ "    FROM "
		+ "      t_learner_license_renewal_dtls b "
		+ "    WHERE b.`Learner_License_Id` = ?), "
		+ "    (SELECT "
		+ "      MAX(c.Expiry_Date) "
		+ "    FROM "
		+ "      t_learner_license_renewal_dtls c "
		+ "    WHERE c.`Learner_License_Id` = ?), "
		+ "    a.`Expiry_Date` "
		+ "  ) expiryDate "
		+ "FROM "
		+ "  t_learner_license_dtls a "
		+ "WHERE a.`Learner_License_Info_Id` = ?";
	
	private static String GET_APPLICATION_STATUS_OF_VEHICLE_SERVICE = "SELECT "
												+ "(SELECT u.`Service_Name` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) AS service_name, "
												+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), "
												+ "(SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number), "
												+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) " +
														"IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap "
												+ "WHERE ap.Application_Number=a.Application_Number), "
												+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`)" +
														" IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number FROM t_vehicle_application ap "
												+ "WHERE ap.Application_Number=a.Application_Number),IF((SELECT u.`Service_Short_Desc` FROM t_service_master u" +
														" WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), "
												+ "(SELECT ap.`Learner_License_No` FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number),'') "
												+ "))) serviceTypeNo, "
												+ "a.`Application_Number`, "
												+ "c.Status_Name, "
												+ "c.Status_Type_Short_Desc, "
												+ "a.Action_Date, "
												+ "a.Role_Name, "
												+ "a.Actor_Name, "
												+ "(SELECT u.`name` FROM `t_task_dtls` z LEFT JOIN `t_user_master` u ON u.`login_id` = z.`Assigned_User_Id` "
												+ "WHERE z.`Application_Number` = a.`Application_Number` AND z.`Task_State_Id` = 8 )AS claimed_by , "
												+ "IF(a.`Juris_Type_Id`=2,(SELECT l.`base_office_name` FROM t_base_office_master l WHERE l.`base_office_id`=a.`Juris_Id`), "
												+ "(SELECT l.`region_name` FROM t_region_master l WHERE l.`region_id`=a.`Juris_Id`) "
												+ ") submittedTo, "
												+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), "
												+ "(SELECT p.CID_Number FROM t_learner_license_application ap "
												+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number), "
												+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap "
												+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number), "
												+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap "
												+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number),'' "
												+ ") ) ) cid "
												+ "FROM   t_workflow_dtls a,t_status_master c "
												+ "WHERE a.Application_Number IN (SELECT z.`Application_Number` FROM t_vehicle_application z" +
														" LEFT JOIN t_vehicle_registration_dtls m ON z.vehicle_id = m.`Vehicle_Reg_Dtls_Id` "
												+ " WHERE IF(''=?,1=1,z.`Customer_Id` IN (SELECT u.`Customer_Id` FROM t_personal_dtls u WHERE u.`CID_Number`=?))  "
												+ "  AND IF(''=?,1=1, (m.`Vehicle_Type_Id`=? AND z.`Vehicle_Number`=?))) "
												+ "AND a.Status_Id = c.Status_Id order by a.Action_Date desc";
											
											
											
	private static String GET_ALL_WORKFLOW_STATUS_LIST ="SELECT "
		+ " (SELECT u.`Service_Name` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) AS service_name, "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), "
		+ "(SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap "
		+ "WHERE ap.Application_Number=a.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number FROM t_vehicle_application ap "
		+ "WHERE ap.Application_Number=a.Application_Number),IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), "
		+ "(SELECT ap.`Learner_License_No` FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number),'') "
		+ "))) serviceTypeNo, "
		+ " a.`Application_Number`, "
		+ " c.Status_Name, "
		+ " c.Status_Type_Short_Desc, "
		+ " a.Action_Date, "
		+ " a.Role_Name, "
		+ " a.Actor_Name, "
		+ " (SELECT u.`name` FROM `t_task_dtls` z LEFT JOIN `t_user_master` u ON u.`login_id` = z.`Assigned_User_Id` "
		+ "	WHERE z.`Application_Number` = a.`Application_Number` AND z.`Task_State_Id` = 8 )AS claimed_by , "
		+ "	IF(a.`Juris_Type_Id`=2,(SELECT l.`base_office_name` FROM t_base_office_master l WHERE l.`base_office_id`=a.`Juris_Id`), "
		+ "		(SELECT l.`region_name` FROM t_region_master l WHERE l.`region_id`=a.`Juris_Id`) "
		+ ") submittedTo, "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), "
		+ "(SELECT p.CID_Number FROM t_learner_license_application ap "
		+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap "
		+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap "
		+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number),'' "
		+ ") ) ) cid "
		+ "FROM   t_workflow_dtls a,t_status_master c "
		+ "WHERE a.Application_Number = ? "
		+ "AND a.Status_Id = c.Status_Id "
		+ "UNION "
		+ "SELECT "
		+ " (SELECT u.`Service_Name` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) AS service_name, "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('ILL','RLL','DLL'), "
		+ "(SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=b.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap "
		+ "WHERE ap.Application_Number=b.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number FROM t_vehicle_application ap "
		+ "WHERE ap.Application_Number=b.Application_Number),IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('ILL','RLL','DLL'), "
		+ "(SELECT ap.`Learner_License_No` FROM t_learner_license_application ap WHERE ap.Application_Number=b.Application_Number),'') "
		+ "))) serviceTypeNo, "
		+ " b.`Application_Number`, "
		+ " d.Status_Name, "
		+ " d.Status_Type_Short_Desc, "
		+ " b.Action_Date, "
		+ " b.Role_Name, "
		+ " b.Actor_Name, "
		+ " (SELECT u.`name` FROM `t_task_dtls` z LEFT JOIN `t_user_master` u ON u.`login_id` = z.`Assigned_User_Id` "
		+ "	WHERE z.`Application_Number` = b.`Application_Number` AND z.`Task_State_Id` = 8 )AS claimed_by , "
		+ "	IF(b.`Juris_Type_Id`=2,(SELECT l.`base_office_name` FROM t_base_office_master l WHERE l.`base_office_id`=b.`Juris_Id`), "
		+ "		(SELECT l.`region_name` FROM t_region_master l WHERE l.`region_id`=b.`Juris_Id`) "
		+ ") submittedTo, "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('ILL','RLL','DLL'), "
		+ "(SELECT p.CID_Number FROM t_learner_license_application ap "
		+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=b.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap "
		+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=b.Application_Number), "
		+ "IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=b.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap "
		+ "LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=b.Application_Number),'' "
		+ ") ) ) cid "
		+ " FROM "
		+ " t_workflow_dtls_audit b, "
		+ " t_status_master d "
		+ " WHERE b.Application_Number =  ? "
		+ " AND b.Status_Id = d.Status_Id "
		+ " ORDER BY Action_Date DESC";
	
	
	private static final String GET_SERVICE_CODE = "SELECT s.`service_short_desc` "
												+ "FROM   `t_workflow_dtls` w, "
												+ "       `t_service_master` s "
												+ "WHERE  s.`service_id` = w.`service_id` "
												+ "       AND w.`application_number` = ? ";
	
		private static final String GET_PERSONAL_INFO	=	"SELECT "
															+ "  COUNT(*) rowCount,a.`Guardian_Contact_No`,a.`Ministry_Id`,a.`Department_Id`, "
															+ "  a.*, "
															+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d/%m/%Y') dob, "
															+ "  b.dzongkhag_name AS permanentDzongkhag, "
															+ "  c.Gewog_Name AS permanentGewog, "
															+ "  a.Present_Contact_Address, "
															+ "  d.`blood_group_type`, "
															+ "  e.`Country_Name`, "
															+ "  v.`Village_Name` AS permanentVillage, "
															+ "  e.`Country_Name`, "
															+ "  e.`Nationality`, "
															+ "  f.`Village_Name`, "
															+ "  g.`Occupation_Name`,"
															+"	l.`Driving_License_No` " 
															+ " FROM "
															+ "  t_personal_dtls a "
															+ "  LEFT JOIN t_dzongkhag_master b "
															+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
															+ "  LEFT JOIN t_gewog_master c "
															+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
															+ "  LEFT JOIN t_blood_group_master d "
															+ "    ON a.`Blood_Group_Id` = d.`blood_group_id` "
															+ "  LEFT JOIN t_country_master e "
															+ "    ON a.`Permanent_Country_Id` = e.`Country_Id` "
															+ "  LEFT JOIN t_village_master v "
															+ "    ON v.`Village_Id` = a.`Permanant_Village_Id` "
															+ "  LEFT JOIN t_village_master f "
															+ "    ON a.`Permanant_Village_Id` = f.`Village_Id` "
															+ "  LEFT JOIN t_occupation_master g "
															+ "    ON a.`Occupation_Id` = g.`Occupation_Id` "
															+"	LEFT JOIN t_driving_license_dtls l " 
															+"		ON l.`Customer_Id`=a.`Customer_Id`"
															 +" WHERE "
															 +" a.`CID_Number`=?  AND a.`Date_Of_Birth`=?";
	   
		 public static final String UPDATE_PERSONAL_INFO	=  "UPDATE  `t_personal_dtls` "
															 + "SET "
															 + "  `Title_Of_Courtesy_Id` = ?, "
															 + "  `First_Name` = ?, "
															 + "  `Middle_Name` = ?, "
															 + "  `Last_Name` = ?, "
															 + "  `Occupation_Id` =?, "
															 + "  `Blood_Group_Id` = ?, "
															 + "  `Fathers_Name` = ?, "
															 + "  `Gender` = ?, "
															 + "  `Permanent_Dzongkhag_Id` = ?, "
															 + "  `Permanent_Gewog_Id` = ?, "
															 + "  `Permanant_Village_Id` = ?, "
															 + "  `Permanent_Address` = ?, "
															 + "  `Present_Dzongkhag_Id` = ?, "
															 + "  `Present_Contact_Address` = ?, "
															 + "  `Present_Phone_No` = ?, "
															 + "  `Present_Email` = ?, "
															 + "  `Image_Path` = ?, "
															 + "  `UUID` = ?, "
															 + "  `Nationality_Id`=?"
															 + "WHERE `Personal_Info_Id` = ?";
		 
		 private static final String GET_CUSTOMER_ID_FROM_INFO_ID = "SELECT Customer_Id FROM t_personal_dtls WHERE Personal_Info_Id=?";

		 public static final String GET_LEARNER_LICENSE	= "SELECT p.`Present_Phone_No`,p.`Title_Of_Courtesy_Id`,p.`Personal_Info_Id`"
			 + "  ,l.`Learner_License_Info_Id`,"
			 + "l.`Issue_Date`,"
			 + " p.`First_Name`,"
			 + "p.`Middle_Name`,"
			 + "p.`Last_Name`,"
			 + "p.`CID_Number`,"
			 + "p.`Gender`,"
			 + "p.`Customer_Id`,"
			 + "p.`Image_Path`,"
			 + "p.`Present_Email`,"
			 + "  DATE_FORMAT(p.`Date_Of_Birth`, '%Y-%m-%d') AS Citizen_Date_Of_Birth, "
			 + "  COUNT(*) rowCount, "
			 + "  e.`Application_Number` AS test_applicationNo, "
			 + "  e.`Test_Date`, "
			 + "  CONCAT(f.`Juris_Type_Id`,'#',f.`Test_Location_Id`) Test_Location_Id, "
			 + "  e.`Drive_Type_Id` "
			 + " FROM "
			 + "  `t_learner_license_dtls` l "
			 + "  LEFT JOIN `t_personal_dtls` p "
			 + "    ON p.`Customer_Id` = l.`Customer_Id` "
			 + "  LEFT JOIN `t_etest_application` e "
			 + "    ON e.`Learner_License_No` = l.`Learner_License_No`   and e.`Test_Date`>=curdate()"
			 + "  LEFT JOIN t_etest_config_master f "
			 + "    ON e.`Test_Location_Id` = f.`Sl_No` "
			 + " WHERE l.`Learner_License_No` = ? "
			 + "  AND p.`CID_Number` = ?";

		 public static final String GET_LEARNER_LICENSE_BY_CID	= "SELECT p.`Present_Phone_No`,p.`Title_Of_Courtesy_Id`,p.`Personal_Info_Id`"
			 + "  ,l.`Learner_License_Info_Id`,"
			 + "l.`Issue_Date`,"
			 + " p.`First_Name`,"
			 + "p.`Middle_Name`,"
			 + "p.`Last_Name`,"
			 + "p.`CID_Number`,"
			 + "p.`Gender`,"
			 + "p.`Customer_Id`,"
			 + "p.`Image_Path`,"
			 + "p.`Present_Email`,"
			 + "  DATE_FORMAT(p.`Date_Of_Birth`, '%Y-%m-%d') AS Citizen_Date_Of_Birth, "
			 + "  COUNT(*) rowCount, "
			 + "  e.`Application_Number` AS test_applicationNo, "
			 + "  e.`Test_Date`, "
			 + "  CONCAT(f.`Juris_Type_Id`,'#',f.`Test_Location_Id`) Test_Location_Id, "
			 + "  e.`Drive_Type_Id` "
			 + " FROM "
			 + "  `t_learner_license_dtls` l "
			 + "  LEFT JOIN `t_personal_dtls` p "
			 + "    ON p.`Customer_Id` = l.`Customer_Id` "
			 + "  LEFT JOIN `t_etest_application` e "
			 + "    ON e.`Learner_License_No` = l.`Learner_License_No`   and e.`Test_Date`>=curdate()"
			 + "  LEFT JOIN t_etest_config_master f "
			 + "    ON e.`Test_Location_Id` = f.`Sl_No` "
			 + " WHERE  p.`CID_Number` = ?";
		 
		 private static final String GET_DRIVING_LICENSE_DTLS =  "SELECT "
			 + "  b.`Personal_Info_Id`, "
			 + "  b.`Customer_Id`, "
			 + "  b.`First_Name`, "
			 + "  b.`Middle_Name`, "
			 + "  b.`Last_Name`, "
			 + "  DATE_FORMAT(b.`Date_Of_Birth`, '%Y-%m-%d') AS Citizen_Date_Of_Birth, "
			 + "  b.`Present_Phone_No`, "
			 + "  COUNT(*) rowCount, "
			 + "  c.`Application_Number` AS test_applicationNo, "
			 + "  c.`Test_Date`, "
			 + "  c.`Drive_Type_Id`, "
			 + "  CONCAT(d.`Juris_Type_Id`,'#',d.`Test_Location_Id`) Test_Location_Id,d.Sl_No "
			 + " FROM "
			 + "  t_driving_license_dtls a "
			 + "  LEFT JOIN t_personal_dtls b "
			 + "    ON a.`Customer_Id` = b.`Customer_Id` "
			 + "  LEFT JOIN t_etest_application c "
			 + "    ON a.`Driving_License_No` = c.`Learner_License_No`  AND c.`Test_Date`>=CURDATE() "
			 + "  LEFT JOIN t_etest_config_master d "
			 + "    ON c.`Test_Location_Id` = d.`Sl_No` "
			 + " WHERE a.`Driving_License_No` = ? "
			 + "  AND b.`CID_Number` = ?";
		 
		 public static final String GET_LICENSE_DETAILS	=	" SELECT COUNT(*) rowCount,l.*,p.*,b.`blood_group_type`,DATE_FORMAT(l.`Issue_Date`, '%d/%m/%Y') issueDate" +
		 													" FROM `t_driving_license_dtls` l " +
		 													" LEFT JOIN `t_personal_dtls` p ON p.`Customer_Id`=l.`Customer_Id` " +
		 													" LEFT JOIN t_blood_group_master b ON b.`blood_group_id`=p.`Blood_Group_Id` " +
		 													" WHERE l.`Driving_License_No`=? AND p.`CID_Number`=?";
		
		public static final String GET_VEHILCE_DETIALS	=	"SELECT COUNT(*) rowCount,v.`Vehicle_Number`, " 
															+ " p.`First_Name`,p.`Last_Name`,p.`Middle_Name`,"
															+ " v.`Vehicle_Registration_Type`, "
															+ " r.`region_name` vehicleRegion,c.`Vehicle_Company_Name`, "
															+ " v.`Registration_Date`, "
															+ " v.`Vehicle_Model_Id`,m.`Vehicle_Model_Name`, "
															+ " v.`Engine_Number`,v.`Engine_CC`,v.`Purchase_Type` "
															+ " FROM `t_vehicle_registration_dtls` v "
															+ " LEFT JOIN `t_personal_dtls` p ON p.`Customer_Id`=v.`Customer_Id` "
															+ "LEFT JOIN `t_region_master` r ON r.`region_id` =v.`Region_Id` "
															+ "LEFT JOIN `t_vehicle_model_master` m ON m.`Vehicle_Model_Id`=v.`Vehicle_Model_Id` "
															+ "LEFT JOIN `t_vehicle_company_master` c ON c.`Vehicle_Company_Id`=v.`Vehicle_Company_Id` "
															+ " WHERE v.`Vehicle_Number`=? AND p.`CID_Number`=?";
		public static final String GET_FINE_INFO		=	"SELECT COUNT(*) rowCount,m1.`Offence_Name`,m2.`Offence_Name`,m3.`Offence_Name`,i.* " +
															"FROM `t_offence_information` i " +
															"LEFT JOIN `t_offence_type_master` m1  ON m1.`Offence_Type_Id`=i.`Offence_Type_Id1` " +
															"LEFT JOIN `t_offence_type_master` m2  ON m2.`Offence_Type_Id`=i.`Offence_Type_Id2` " +
															"LEFT JOIN `t_offence_type_master` m3  ON m3.`Offence_Type_Id`=i.`Offence_Type_Id3` " +
															"WHERE i.`TIN_No`=? AND i.`License_Number`=?";
		public static final String GET_CUSTOMER_ID		=	"SELECT p.`Customer_Id` FROM `t_personal_dtls` p WHERE p.`CID_Number`=?";
		public static final String GET_PERSONAL_VEHICLE_RENEWAL	=	"SELECT p.`First_Name`,p.`Middle_Name`,p.`Last_Name`,m.`Village_Name`," +
																	"d.`dzongkhag_name`,g.`Gewog_Name` " +
																	" FROM `t_vehicle_registration_dtls` v " +
																	" LEFT JOIN t_constant c ON c.`Constant_Type`='VEH_RENEWAL' AND c.`Vehicle_Type_Id`=v.`Vehicle_Type_Id`" +
																	" AND c.`CC_From`<v.`Engine_CC` AND c.`CC_To`<v.`Engine_CC` " +
																	" LEFT JOIN `t_personal_dtls` p ON p.`Customer_Id`=v.`Customer_Id` " +
																	" LEFT JOIN t_village_master m ON m.`Village_Id`=p.`Permanant_Village_Id`" +
																	" LEFT JOIN `t_dzongkhag_master` d ON d.`dzongkhag_id`=p.`Permanent_Dzongkhag_Id`" +
																	" LEFT JOIN t_gewog_master g ON g.`Gewog_Id`=p.`Permanent_Gewog_Id`" +
																	" WHERE p.`CID_Number`=? AND v.`Vehicle_Number`=?";

		public static final String GET_CANCELLATION_DTLS	=			"SELECT COUNT(c.`Vehicle_Cancellation_Id`) AS rowCount " +
																	" FROM `t_vehicle_cancellation_dtls` c " +
																	 " WHERE c.`Vehicle_Number`=? ";

		private static final String GET_RENEWAL_HISTORY_LIST =   "SELECT COUNT(a.Receipt_No) AS rowCount,"
																+ "  a.Receipt_No, "
																+ "  DATE_FORMAT(a.`Renewal_Date`, '%d-%m-%Y') renewalDate, "
																+ "  DATE_FORMAT(a.`Expiry_Date`, '%d-%m-%Y') expDate, "
																+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receiptDate, b.`Vehicle_Number` "
																+ " FROM "
																+ "  t_vehicle_renewal_dtls a " 
																+ " LEFT JOIN `t_vehicle_registration_dtls` b ON a.`Vehicle_Id`=b.`Vehicle_Reg_Dtls_Id`"
																+ " WHERE a.`Vehicle_Id` = ?";
		
	private static final String GET_ORGANISATION_CANCELLATION_DTLS	=	"SELECT COUNT(c.`Vehicle_Cancellation_Id`) AS rowCount " +
																		"FROM  `t_vehicle_cancellation_dtls` c WHERE c.`Vehicle_Number` = ? ";
	
	
	private static final String GET_ORGANISATION_VEHICLE_TRANSFER_DTLS	=	" SELECT  COUNT(t.`Vehicle_Transfer_Id`) AS rowCount," +
																			"r.`Engine_CC`,a.`Vehicle_Company_Name`,r.`Vehicle_Model_Id`,c.`Amount`," +
																			"c.`Fine_Per_Day`, DATE_FORMAT(d.`Expiry_Date`, '%d/%m/%Y') AS vehExpiry_Date,o.`Address`," +
																			"e.`Ministry_Name`,f.`Department_Name`,m.`Vehicle_Model_Name` " +
																			"FROM `t_vehicle_transfer_dtls` t " +
																			" LEFT JOIN `t_vehicle_registration_dtls` r ON r.`Vehicle_Number` = t.`Vehicle_Number`" +
																			" LEFT JOIN `t_organization_info` o ON o.`Customer_Id`=t.`Transferee_Customer_Id`" +
																			" LEFT JOIN `t_vehicle_company_master`a ON a.`Vehicle_Company_Id`=r.`Vehicle_Company_Id`" +
																			"LEFT JOIN `t_vehicle_renewal_dtls` d ON d.`Vehicle_Number` =t.`Vehicle_Number`" +
																			"LEFT JOIN `t_constant` c ON c.`CC_From`<r.`Engine_CC` " +
																			"LEFT JOIN `t_ministry_master` e ON e.`Ministry_Id`=o.`Ministry_Id` " +
																			"LEFT JOIN `t_department_master` f ON f.`Department_Id`=o.`Department_Id` " +
																			"LEFT JOIN `t_vehicle_model_master` m ON m.`Vehicle_Model_Id`=r.`Vehicle_Model_Id` " +
																			" AND c.`CC_To`>r.`Engine_CC` AND c.`Vehicle_Type_Id`=r.`Vehicle_Type_Id`" +
																			" WHERE t.`Vehicle_Number`=? AND o.`Organization_Name`=? ";
	private static final String GET_ORGANISATION_VEHICLE_DTLS	=	"SELECT " +
																	" COUNT(r.`Vehicle_Reg_Dtls_Id`) AS rowCount," +
																	"r.`Engine_CC`,a.`Vehicle_Company_Name`,r.`Vehicle_Model_Id`,c.`Amount`," +
																	"c.`Fine_Per_Day`, DATE_FORMAT(d.`Expiry_Date`, '%d/%m/%Y') AS vehExpiry_Date,o.`Address`," +
																	"e.`Ministry_Name`,f.`Department_Name`,m.`Vehicle_Model_Name` " +
																	" FROM  `t_vehicle_registration_dtls`  r " +
																	"LEFT JOIN `t_vehicle_company_master`a ON a.`Vehicle_Company_Id`=r.`Vehicle_Company_Id`" +
																	"LEFT JOIN `t_organization_info` o ON o.`Customer_Id`=r.`Customer_Id` " +
																	"LEFT JOIN `t_constant` c ON c.`CC_From`<r.`Engine_CC` AND c.`CC_To`>r.`Engine_CC`" +
																	"LEFT JOIN `t_ministry_master` e ON e.`Ministry_Id`=o.`Ministry_Id`" +
																	"LEFT JOIN `t_vehicle_renewal_dtls` d ON d.`Vehicle_Number` =r.`Vehicle_Number`" +
																	" LEFT JOIN `t_department_master` f ON f.`Department_Id`=o.`Department_Id`" +
																	" LEFT JOIN `t_vehicle_model_master` m ON m.`Vehicle_Model_Id`=r.`Vehicle_Model_Id` " +
																	" AND c.`Vehicle_Type_Id`=r.`Vehicle_Type_Id` " +
																	"WHERE r.`Vehicle_Number`=? AND o.`Organization_Name`=?";
	
	private static final String BOOK_DRIVING_TEST	=	"INSERT INTO `t_etest_application_prebooking` ( "
														+ "  `Application_Number`, "
														+ "  `Learner_License_No`, "
														+ "  `DOB`, "
														+ "  `Drive_Type_Id`, "
														+ "  `Test_Location_Id`, "
														+ "  `Test_Date`, "
														+ "  `Created_By`, "
														+ "   `Test_Type`,Customer_Id,Juris_Type_Id,Issue_Type "
														+ ") "
														+ "VALUES "
														+ "  ( "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?, "
														+ "    ?,?,?,? "
														+ "  )";
	
	private static final String ADMIN_BOOK_DRIVING_TEST	=	"INSERT INTO `t_etest_application` ( "
		+ "  `Prebooking_Application_No`, "
		+ "  `Learner_License_No`, "
		+ "  `DOB`, "
		+ "  `Drive_Type_Id`, "
		+ "  `Test_Location_Id`, "
		+ "  `Test_Date`, "
		+ "  `Created_By`, "
		+ "   `Test_Type`,Customer_Id,Juris_Type_Id,Issue_Type,Created_On "
		+ ") "
		+ "VALUES "
		+ "  ( "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?,?,?,?,CURRENT_TIMESTAMP "
		+ "  )";


	private static final String CANCEL_DRIVING_TEST	=	"DELETE FROM `t_etest_application` WHERE `Application_Number` = ?";
	
	private static final String GET_LICENSE_PERSONAL_INFO_ID	=	"SELECT l.`Driving_License_Id`,l.Driving_License_Type_Id " 
																	+ "FROM `t_driving_license_dtls` l " 
																	+ "LEFT JOIN `t_personal_dtls` p ON p.`Customer_Id`=l.`Customer_Id` " 
																	+ "WHERE " 
																	+ "if(?='',1=1,p.`CID_Number`=?) "
																	+ " AND "
																	+ " if(?='',1=1, l.Driving_License_No=?)"
																	+"AND l.`Driving_License_Id` NOT IN "
																	+ "  (SELECT "
																	+ "    `License_Id` "
																	+ "  FROM "
																	+ "    `t_driving_license_cancelled`) "
																	+ "  AND IF( l.`Driving_License_Type_Id` = 'C', "
																	+ "    (TIMESTAMPDIFF(YEAR, p.`Date_Of_Birth`, CURDATE()))<=65, "
																	+ "   (TIMESTAMPDIFF(YEAR, p.`Date_Of_Birth`, CURDATE()))<=70 "
																	+ "  ) ORDER BY l.`Driving_License_Type_Id` ASC  LIMIT 1";
	
	
	private static final String GET_PARAM_FOR_NON_COMMERCIAL_CRITERIA="SELECT "
																	  +" a.`Customer_Id`,"
																	  +" b.`Date_Of_Birth`,"
																	  +" COUNT(a.`Learner_License_Info_Id`) AS rowCount,"
																	  +" a.`Issue_Date` "
																	  +" FROM"
																	  +" `t_learner_license_dtls` a" 
																	  +" LEFT JOIN t_personal_dtls b" 
																	  +" ON b.`Customer_Id` = a.`Customer_Id`" 
																	  +" WHERE  "
																	  +" a.`Learner_License_Info_Id` = ?";
	
	private static final String GET_DRIVING_LICENSE_APPLICATION_FROM_CID	=	"SELECT b.`Application_Number` FROM `t_personal_dtls` a "
																				+ " LEFT JOIN `t_driving_license_application` b "
																				+ " ON b.`Customer_Id`=a.`Customer_Id` "
																				+ " WHERE a.`CID_Number`=?";
	
	private static final String GET_LEARNER_LICENSE_APPLICATION_FROM_CID	=	"SELECT b.`Application_Number` FROM `t_personal_dtls` a "
																				+ " LEFT JOIN `t_learner_license_application` b "
																				+ " ON b.`Customer_Id`=a.`Customer_Id` "
																				+ " WHERE a.`CID_Number`=?";
	
	private static final String GET_VEHICLE_APPLICATION_FROM_CID	=	"SELECT b.`Application_Number` FROM `t_personal_dtls` a "
																				+ " LEFT JOIN `t_vehicle_application` b "
																				+ " ON b.`Customer_Id`=a.`Customer_Id` "
																				+ " WHERE a.`CID_Number`=?";
	
	private static final String GET_OFFENCE_LIST	=	"SELECT count(*) rowCount,"
														+ "  a.`Offence_Id`"
														+ " FROM "
														+ "  `t_offence_information` a "
														+ "  LEFT JOIN `t_offence_information_dtls` b "
														+ "    ON b.`Offence_Id` = a.`Offence_Id` "
														+ "  LEFT JOIN `t_vehicle_registration_dtls` f "
														+ "    ON f.`Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id` "
														+ "  LEFT JOIN `t_driving_license_dtls` g "
														+ "    ON g.`Driving_License_Id` = a.`License_Id` "
														+ "  LEFT JOIN `t_learner_license_dtls` h "
														+ "    ON h.`Learner_License_Info_Id` = a.`Learner_License_Id` "
														+ " WHERE" +
													//	" a.`TIN_No`=? AND" +
														" f.`Vehicle_Number`=? AND f.vehicle_type_id=?";
	
	
	private static final String GET_SERVICE_SHORT_DESC	=	"SELECT COUNT(a.`Service_Short_Desc`)rowCount,a.`Service_Short_Desc` FROM `t_service_master` a WHERE a.`Service_Id`=?";
	
	private static final String UPDATE_OFFENCE_PAYMENT	=	"UPDATE `t_offence_information` a" 
															+" SET a.`Receipt_Date`=?,a.`Receipt_Number`=?,a.`Amount`=?,a.`Is_paid`='Y',a.Is_Online_Payment='Y' " 
															+" WHERE a.`Offence_Id`=?";
	
	private static final String GET_VEHICLE_ID	=	"SELECT COUNT(a.`Vehicle_Reg_Dtls_Id`) AS rowCount,a.`Vehicle_Reg_Dtls_Id` AS vehicle_id FROM `t_vehicle_registration_dtls` a "
													+ " LEFT JOIN `t_personal_dtls` b ON a.`Customer_Id`=b.`Customer_Id` "
													+ " WHERE a.`Vehicle_Number`=? AND a.`Vehicle_Type_Id`=?";
	
	
	private static final String UPDATE_VEHICLE_APPLICATION_PAYMENT	=	"UPDATE `t_vehicle_application` a "
																		+" SET a.`Receipt_Date`=?,a.`Receipt_Number`=?"
																		+" WHERE a.`Application_Number`=?";
	
	private static final String INSERT_INTO_DL_APPLICATION_FROM_ONLINE_PAYMENT = "INSERT INTO  `t_driving_license_application` "
																		+ "            (`Application_Number`, "
																		+ "             `Application_Type`, "
																		+ "             `Driving_license_type`, "
																		+ "             `Issued_To_Id`, "
																		+ "             `Learner_License_No`, "
																		+ "             `Customer_Id`, "
																		+ "             `Driving_License_No`, "
																		+ "             `License_Id`, "
																		+ "             `Region_Id`, "
																		+ "             `Issue_Date`, "
																		+ "             `Expiry_Date`, "
																		+ "             `Status`, "
																		+ "             `IID`, "
																		+ "             `Test_Marks`, "
																		+ "             `Register_Number`, "
																		+ "             `Drive_Type_Id`, "
																		+ "             `Is_Renewal_Upon_MC`, "
																		+ "             `Renewal_Duration`, "
																		+ "             `Issue_type`, "
																		+ "             `Remarks`, "
																		+ "             `Receipt_Date`, "
																		+ "             `Receipt_Number`, "
																		+ "             `Endorsed_Dated`, "
																		+ "             `Is_TCB_Endorsement`, "
																		+ "             `App_Submission_Date`, "
																		+ "             `App_Verification_Date`, "
																		+ "             `App_Approval_Date`, "
																		+ "             `Created_By`, "
																		+ "             `Created_On`, "
																		+ "             `Updated_By`, "
																		+ "             `Updated_On`) "
																		+ "             ( "
																		+ "             SELECT "
																		+ "  `Application_Number`, "
																		+ "  `Application_Type`, "
																		+ "  `Driving_license_type`, "
																		+ "  `Issued_To_Id`, "
																		+ "  `Learner_License_No`, "
																		+ "  `Customer_Id`, "
																		+ "  `Driving_License_No`, "
																		+ "  `License_Id`, "
																		+ "  `Region_Id`, "
																		+ "  `Issue_Date`, "
																		+ "  `Expiry_Date`, "
																		+ "  `Status`, "
																		+ "  `IID`, "
																		+ "  `Test_Marks`, "
																		+ "  `Register_Number`, "
																		+ "  `Drive_Type_Id`, "
																		+ "  `Is_Renewal_Upon_MC`, "
																		+ "  `Renewal_Duration`, "
																		+ "  `Issue_type`, "
																		+ "  `Remarks`, "
																		+ "  ?, "
																		+ "  ?, "
																		+ "  `Endorsed_Dated`, "
																		+ "  `Is_TCB_Endorsement`, "
																		+ "  `App_Submission_Date`, "
																		+ "  `App_Verification_Date`, "
																		+ "  `App_Approval_Date`, "
																		+ "  `Created_By`, "
																		+ "  `Created_On`, "
																		+ "  `Updated_By`, "
																		+ "  `Updated_On` "
																		+ "FROM `eralis_payment_db`.`t_driving_license_application` WHERE Application_Number=? "
																		+ "             )";
	
	
	private static final String UPDATE_DRIVING_LICENSE_APPLICATION_PAYMENT	=	"UPDATE `t_driving_license_application` a "
																		+" SET a.`Receipt_Date`=?,a.`Receipt_Number`=? "
																		+" WHERE a.`Application_Number`=?";
	
	private static final String UPDATE_LEARNER_LICENSE_APPLICATION_PAYMENT	=	"UPDATE `t_learner_license_application` a "
																		+" SET a.`Receipt_Date`=?,a.`Receipt_No`=? "
																		+" WHERE a.`Application_Number`=?";
	
	private static final String INSERT_INTO_LL_APPLICATION_FROM_ONLINE_PAYMENT =  "INSERT INTO `t_learner_license_application` "
																		+ "            (`Application_Number`,  `Application_Type`, `Customer_Id`, "
																		+ "             `Learner_License_No`,  `Learner_License_Id`, `Region_Id`, "
																		+ "             `Receipt_Date`,`Receipt_No`, `Certifying_Doctor`, "
																		+ "             `Renewal_Duration`, `Remarks`,`App_Submission_Date`, "
																		+ "             `App_Verification_Date`, `App_Approval_Date`, `Created_By`, "
																		+ "             `Create_On`, `Updated_By`,`Updated_On`) "
																		+ "             (SELECT  `Application_Number`,`Application_Type`,`Customer_Id`, "
																		+ "  `Learner_License_No`,`Learner_License_Id`,`Region_Id`, "
																		+ "  ?,?,`Certifying_Doctor`,`Renewal_Duration`, `Remarks`,`App_Submission_Date`, "
																		+ "  `App_Verification_Date`,`App_Approval_Date`,`Created_By`, "
																		+ "  `Create_On`,`Updated_By`,`Updated_On` "
																		+ "FROM `eralis_payment_db`.`t_learner_license_application` WHERE `Application_Number`=?)";
	
	
	private static final String UPDATE_T_PAYMENT_DTLS	=	"UPDATE `t_payment_dtls` a SET a.`Receipt_Date`=?,a.`Receipt_No`=? WHERE a.`Application_Number`=?";

	private static final String UPDATE_T_PAYMENT_DTLS_OF_PAYMENT_DB	=	"UPDATE `t_payment_dtls` a SET a.`Receipt_Date`=?,a.`Receipt_No`=?,Is_Payment_Successful=? WHERE a.`Application_Number`=?";

	private static final String INSERT_INTO_T_PAYMENT_DTLS_FROM_ONLINE_PAYMENT =  "INSERT INTO `t_payment_dtls` "
																+ "( `Application_Number`,`Application_Type`, "
																+ "`Service_Id`,`Amount_Paid`,`Penalty_Paid`,`Receipt_Date`, "
																+ "`Receipt_No`,`Created_By`,`Created_On`) "
																+ "(SELECT "
																+ " `Application_Number`,`Application_Type`, "
																+ "`Service_Id`,`Amount_Paid`,`Penalty_Paid`,?,?, "
																+ "`Created_By`, `Created_On` "
																+ " FROM `eralis_payment_db`.`t_payment_dtls` WHERE `Application_Number`=?)";
	
	private static final String GET_ETEST_ID	=	"SELECT COUNT(*)rowCount,a.`Application_Number` FROM `t_etest_application` a WHERE a.`Learner_License_No`=? AND a.`Customer_Id`=?";
	
	
	private static final String GET_VEHICLE_EXPIRY_DATE =  "SELECT "
													 + "  IF( "
													 + "    a.`Vehicle_Reg_Dtls_Id` IN "
													 + "    (SELECT "
													 + "      `Vehicle_Id` "
													 + "    FROM "
													 + "      t_vehicle_renewal_dtls "
													 + "    WHERE `Vehicle_Id` = ?), "
													 + "    (SELECT "
													 + "      MAX(Expiry_Date) "
													 + "    FROM "
													 + "      t_vehicle_renewal_dtls "
													 + "    WHERE `Vehicle_Id` = ?), "
													 + "    a.`Expiry_Date` "
													 + "  ) expiryDate "
													 +" "
													 + " FROM "
													 + "  t_vehicle_registration_dtls a "
													 + " WHERE a.`Vehicle_Reg_Dtls_Id` = ?";
	
	private static final String GET_VEHICLE_APPLICATION_DTLS	=	"SELECT c.`Customer_Id`,c.`Vehicle_Registration_Type`,a.`Expiry_Date`,"
																	+ "  a.`Vehicle_Id`, "
																	+ "  a.`Renewal_Duration`, "
																	+ "  c.`Vehicle_Number`, "
																	+ "  CONCAT( "
																	+ "    b.`First_Name`, "
																	+ "    ' ', "
																	+ "    b.`Middle_Name`, "
																	+ "    ' ', "
																	+ "    b.`Last_Name` "
																	+ "  ) customerName, "
																	+ "  d.`Vehicle_Type_Name` AS vehicleType, "
																	+ "  a.Region_Id, "
																	+ "IF(e.`Juris_Type_Id`=1,(SELECT region_name FROM t_region_master WHERE Region_Id=e.`Juris_Id`), "
																	+ "     (SELECT base_office_name FROM t_base_office_master WHERE Base_Office_Id=e.`Juris_Id`)) submittedTo "
																	+ " FROM "
																	+ "  `t_vehicle_application` a "
																	+ "  LEFT JOIN t_personal_dtls b "
																	+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																	+ "  LEFT JOIN `t_vehicle_registration_dtls` c "
																	+ "    ON a.`Vehicle_Id` = c.`Vehicle_Reg_Dtls_Id` "
																	+ "  LEFT JOIN `t_vehicle_type_master` d "
																	+ "    ON c.`Vehicle_Type_Id` = d.`Vehicle_Type_Id` "
																	+ " LEFT JOIN t_workflow_dtls e ON a.`Application_Number`=e.`Application_Number`"
																	+ " WHERE a.`Application_Number` = ?";
	
	private static final String GET_DL_APPLICATION_DTLS	=	"SELECT "
															+ "  a.`Renewal_Duration`, "
															+ "  CONCAT( "
															+ "    c.`First_Name`, "
															+ "    ' ', "
															+ "    c.`Middle_Name`, "
															+ "    ' ', "
															+ "    c.`Last_Name` "
															+ "  ) AS customerName, "
															+ "  b.`Driving_License_No`, "
															+ "  b.`Driving_License_Id`, "
															+ "  b.`Customer_Id`, "
															+ "  c.`Customer_Id`, "
															+ "  a.Region_Id, "
															+ "IF(e.`Juris_Type_Id`=1,(SELECT region_name FROM t_region_master WHERE Region_Id=e.`Juris_Id`), "
															+ "     (SELECT base_office_name FROM t_base_office_master WHERE Base_Office_Id=e.`Juris_Id`)) submittedTo "
															+ " FROM "
															+ "  `t_driving_license_application` a "
															+ "  LEFT JOIN t_driving_license_dtls b "
															+ "    ON a.`License_Id` = b.`Driving_License_Id` "
															+ "  LEFT JOIN `t_personal_dtls` c "
															+ "    ON b.`Customer_Id` = c.`Customer_Id` LEFT JOIN t_workflow_dtls e ON a.`Application_Number`=e.`Application_Number`"
															+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_EXPIRY_FOR_LICENSE_RENEWAL = "SELECT "
														+ "  IF( " + "    a.`Driving_License_Id` IN " + "    (SELECT "
														+ "      `License_Id` " + "    FROM "
														+ "      t_driving_license_renewal_dtls "
														+ "    WHERE `License_Id` = ?), " + "    (SELECT "
														+ "      MAX(Expiry_Date) " + "    FROM "
														+ "      t_driving_license_renewal_dtls "
														+ "    WHERE `License_Id` = ?), " + "    a.`Expiry_Date` "
														+ "  ) expiryDate " + "FROM " + "  t_driving_license_dtls a "
														+ " WHERE a.`Driving_License_Id` = ?";
	
	private static final String GET_LL_APPLICATION_DTLS	=	"SELECT "
															+ "  a.`Learner_License_Id`, "
															+ "  CONCAT( "
															+ "    b.`First_Name`, "
															+ "    ' ', "
															+ "    b.`Middle_Name`, "
															+ "    ' ', "
															+ "    b.`Last_Name` "
															+ "  ) customerName, "
															+ "  a.`Learner_License_No`, "
															+ "  a.Region_Id,a.Renewal_Duration, "
															+ "IF(e.`Juris_Type_Id`=1,(SELECT region_name FROM t_region_master WHERE Region_Id=e.`Juris_Id`), "
															+ "     (SELECT base_office_name FROM t_base_office_master WHERE Base_Office_Id=e.`Juris_Id`)) submittedTo "
															+ " FROM "
															+ "  `t_learner_license_application` a "
															+ "  LEFT JOIN `t_personal_dtls` b "
															+ "    ON a.`Customer_Id` = b.`Customer_Id` LEFT JOIN t_workflow_dtls e ON a.`Application_Number`=e.`Application_Number` "
															+ " WHERE a.`Application_Number` = ?";
	
	private static final String GET_EXPIRY_FOR_LL_RENEWAL = "SELECT "
															+ "  IF( "
															+ "    a.`Learner_License_Info_Id` IN "
															+ "    (SELECT "
															+ "      b.`Learner_License_Id` "
															+ "    FROM "
															+ "      t_learner_license_renewal_dtls b WHERE b.`Learner_License_Id`=a.`Learner_License_Info_Id`), "
															+ "    (SELECT "
															+ "      MAX(c.Expiry_Date) "
															+ "    FROM "
															+ "      t_learner_license_renewal_dtls  c  WHERE c.`Learner_License_Id`=a.`Learner_License_Info_Id`), "
															+ "    a.`Expiry_Date` " + "  ) expiryDate " + "FROM "
															+ "  t_learner_license_dtls a "
															+ "WHERE a.`Learner_License_Info_Id` = ?";
	
	private static final String GET_OFFENCE_DTLS	=	"SELECT "
														+ "  a.`TIN_No`, "
														+ "  b.`Vehicle_Number`, "
														+ "  CONCAT( "
														+ "    c.`First_Name`, "
														+ "    ' ', "
														+ "    c.`Middle_Name`, "
														+ "    ' ', "
														+ "    c.`Last_Name` "
														+ "  ) AS customerName, "
														+ "  d.`Vehicle_Type_Name`, "
														+ "  a.Region_Id "
														+ "FROM "
														+ "  t_offence_information a "
														+ "  LEFT JOIN `t_vehicle_registration_dtls` b "
														+ "    ON a.`Vehicle_Id` = b.`Vehicle_Reg_Dtls_Id` "
														+ "  LEFT JOIN `t_personal_dtls` c "
														+ "    ON b.`Customer_Id` = c.`Customer_Id` "
														+ "  LEFT JOIN t_vehicle_type_master d "
														+ "    ON b.`Vehicle_Type_Id` = d.`Vehicle_Type_Id` "
														+ "WHERE a.`Offence_Id` = ?";
	
	private static final String GET_OWNER_PHONE_NUMBER = "SELECT "
		+ "  c.Present_Phone_No "
		+ "FROM "
		+ "  t_offence_information a "
		+ "  LEFT JOIN t_vehicle_registration_dtls b "
		+ "    ON a.Vehicle_Id = b.Vehicle_Reg_Dtls_Id "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON b.Customer_Id = c.Customer_Id "
		+ "WHERE a.Offence_Id = ? "
		+ "  AND b.Vehicle_Registration_Type = 'P'";
	
	private static final String GET_LICENSE_OWNER_NUMBER = "SELECT "
		+ "  b.Present_Phone_No "
		+ "FROM "
		+ "  t_driving_license_dtls a "
		+ "  LEFT JOIN t_personal_dtls b "
		+ "    ON a.Customer_Id = b.Customer_Id "
		+ "WHERE a.Driving_License_No = ?";
	
	private static final String GET_APPLICATION_REGION_ID = "SELECT "
		+ "  IF( "
		+ "    a.Juris_Type_Id = 1, "
		+ "    a.Juris_Id, "
		+ "    (SELECT "
		+ "      region_id "
		+ "    FROM "
		+ "      t_base_office_master "
		+ "    WHERE base_office_id = a.Juris_Id) "
		+ "  ) regionId "
		+ "FROM "
		+ "  t_workflow_dtls a "
		+ "WHERE a.Application_Number = ?";

	
	private static final String DELETE_FROM_DL_APPLICATION	=	"DELETE FROM `t_driving_license_application` WHERE Application_Number=?;";
	
	private static final String DELETE_FROM_VEH_APPLICATION	=	"DELETE FROM `t_vehicle_application` WHERE Application_Number=?;";

	private static final String DELETE_FROM_LL_APPLICATION	=	"DELETE FROM `t_learner_license_application` WHERE Application_Number=?;";

	private static final String DELETE_FROM_TASK_DTLS	=	"DELETE FROM `t_task_dtls` WHERE Application_Number=?;";

	private static final String DELETE_FROM_TASK_DTLS_AUDIT	=	"DELETE FROM `t_task_dtls_audit` WHERE Application_Number=?;";

	private static final String DELETE_FROM_WORKFLOW_DTLS	=	"DELETE FROM `t_workflow_dtls` WHERE Application_Number=?;";

	private static final String DELETE_FROM_WORKFLOW_DTLS_AUDIT	=	"DELETE FROM `t_workflow_dtls_audit` WHERE Application_Number=?;";

	private static final String DELETE_FROM_LL_DRIVE_TYPE_APPLICATION	=	"DELETE FROM `t_learner_licn_drive_type_application` WHERE Application_Number=?;";

	private static final String DELETE_FROM_PAYMENT_DTLS	=	"DELETE FROM `t_payment_dtls` WHERE Application_Number=?;";
	
	private static final String UPDATE_FITNESS_PAYMENT_DETAILS = "UPDATE t_vehicle_fitness_test_dtls a SET a.Receipt_Number=?, a.Receipt_Date=?,Updated_By=?,Updated_On=? WHERE a.Vehicle_Fitness_Test_Id=?";

	private static final String UPDATE_PAYMENT_TABLE = "UPDATE t_payment_dtls a SET a.Receipt_No=?, a.Receipt_Date=? WHERE a.Application_Number=?;";
	
	
	private static final String GET_VEHICLE_DETAILS_FOR_FITNESS = "SELECT "
		+ "  b.Vehicle_Number, "
		+ "  b.Vehicle_Registration_Type, "
		+ "  a.Customer_Id, "
		+ "  (SELECT "
		+ "    Vehicle_Type_Name "
		+ "  FROM "
		+ "    t_vehicle_type_master "
		+ "  WHERE Vehicle_Type_Id = b.Vehicle_Type_Id) vehicleType, "
		+ "  DATE_FORMAT(a.Validity,'%d/%m/%Y') validity "
		+ "FROM "
		+ "  t_vehicle_fitness_test_dtls a "
		+ "  LEFT JOIN t_vehicle_registration_dtls b "
		+ "    ON a.Vehicle_Id = b.Vehicle_Reg_Dtls_Id "
		+ "WHERE a.Vehicle_Fitness_Test_Id = ?";
	
	private static final String GET_INDIVIDUAL_CUSTOMER_NAME = "SELECT CONCAT(a.First_Name,' ',a.Middle_Name,' ',a.Last_Name) customerName FROM t_personal_dtls a WHERE a.Customer_Id=?";

	private static final String GET_ORGANIZATION_TYPE = "SELECT "
		+ "  (SELECT "
		+ "    Description "
		+ "  FROM "
		+ "    t_owner_master "
		+ "  WHERE Owner_Type_Id = a.Organization_Type_Id) organizationType, "
		+ "  a.Ministry_Id, "
		+ "  a.Department_Id, "
		+ "  a.Private_Id, "
		+ "  a.Organization_Name "
		+ "FROM "
		+ "  t_organization_info a "
		+ "WHERE a.Customer_Id = ?";
	
	private static final String GET_MINISTRY_NAME = "SELECT Ministry_Name FROM t_ministry_master WHERE Ministry_Id=?";
	
	private static final String GET_DEPARTMENT_NAME = "SELECT Department_Name FROM t_department_master WHERE Department_Id=?";
	
	private static final String GET_PRIVATE_NAME = "SELECT Private_Name FROM `t_private_company_master` WHERE Private_Id=?";
	
	private static final String CHECK_DRIVING_TEST_SEAT	=	"SELECT "
													+ "  Max_Applicants, "
													+ "  (SELECT "
													+ "  COUNT(*) rowCount "
													+ "FROM "
													+ "  `t_etest_application_prebooking` b "
													+ "WHERE ( "
													+ "    ( "
													+ "      b.`Status` = 'PENDING' "
													+ "      AND b.`Created_On` > ( "
													+ "        DATE_SUB( "
													+ "          CURRENT_TIMESTAMP, "
													+ "          INTERVAL '0:03' HOUR_MINUTE "
													+ "        ) "
													+ "      ) "
													+ "    ) "
													+ "    OR b.`Status` = 'BOOKED' "
													+ "  ) AND b.`Test_Location_Id` = a.`Test_Location_Id` "
													+ "    AND b.`Test_Date` = a.`Test_Date` "
													+ "    AND b.`Juris_Type_Id` = a.`Juris_Type_Id`) AS totalApplicant "
													+ "FROM "
													+ "  `t_etest_config_master` a "
													+ "WHERE a.`Test_Date` = ? "
													+ "  AND a.`Juris_Type_Id` = ? "
													+ "  AND a.`Test_Location_Id` = ?";
	
	private static final String GET_ACCOUNT_HEAD_ID	=	"SELECT a.`account_head_code`,COUNT(a.`account_head_code`) rowCount FROM `t_account_head_master` a WHERE a.`account_head_desc`=?";
	
	private static final String INSERT_T_ETEST_APPLICATION = "INSERT INTO t_etest_application ( Prebooking_Application_No,"
		+ "  Learner_License_No, "
		+ "  Customer_Id, "
		+ "  DOB, "
		+ "  Drive_Type_Id, "
		+ "  Test_Location_Id, "
		+ "  Juris_Type_Id, "
		+ "  Test_Date, "
		+ "  Test_Type, "
		+ "  Theory_Marks_Obtained, "
		+ "  Theory_Test_Status, "
		+ "  Practical_Marks_Obtained, "
		+ "  Practical_Test_Status, "
		+ "  Overall_Marks_Obtained, "
		+ "  Is_License_Issued, "
		+ "  Created_By, "
		+ "  Created_On, "
		+ "  Updated_By, "
		+ "  Updated_On, "
		+ "  Issue_Type "
		+ ") "
		+ "SELECT Application_Number,"
		+ "  Learner_License_No, "
		+ "  Customer_Id, "
		+ "  DOB, "
		+ "  Drive_Type_Id, "
		+ "  Test_Location_Id, "
		+ "  Juris_Type_Id, "
		+ "  Test_Date, "
		+ "  Test_Type, "
		+ "  Theory_Marks_Obtained, "
		+ "  Theory_Test_Status, "
		+ "  Practical_Marks_Obtained, "
		+ "  Practical_Test_Status, "
		+ "  Overall_Marks_Obtained, "
		+ "  Is_License_Issued, "
		+ "  Created_By, "
		+ "  CURRENT_TIMESTAMP, "
		+ "  Updated_By, "
		+ "  Updated_On, "
		+ "  Issue_Type "
		+ "FROM "
		+ "  t_etest_application_prebooking "
		+ "WHERE Application_Number = ?";
	
	private String INSERT_INTO_T_PAYMENT_DTLS = "INSERT INTO t_payment_dtls ( "
		+ "  Application_Number, "
		+ "  Application_Type, "
		+ "  Service_Id, "
		+ "  Amount_Paid, "
		+ "  Penalty_Paid, "
		+ "  Receipt_No, "
		+ "  Receipt_Date, "
		+ "  Created_By "
		+ ") "
		+ "VALUES "
		+ "  (?, 'BOOKING', 0, ?, 0, ?, ?, ?)";
	
	private String GET_TEST_DETAILS = "SELECT "
		+ "  DATE_FORMAT(a.Test_Date, '%d-%m-%Y') testDate, "
		+ "  a.Learner_License_No, "
		+ "  (SELECT "
		+ "    Drive_Type_Name "
		+ "  FROM "
		+ "    t_drive_type_master "
		+ "  WHERE Drive_Type_Id = a.Drive_Type_Id) driveType, "
		+ "  IF( "
		+ "    a.Juris_Type_Id = '1', "
		+ "    (SELECT "
		+ "      region_name AS locationName "
		+ "    FROM "
		+ "      t_region_master "
		+ "    WHERE region_id = a.Test_Location_Id), "
		+ "    (SELECT "
		+ "      base_office_name AS locationName "
		+ "    FROM "
		+ "      t_base_office_master "
		+ "    WHERE base_office_id = a.Test_Location_Id) "
		+ "  ) locationName, "
		+ " (SELECT CONCAT(First_Name,' ',Middle_Name,' ',Last_Name)customerName FROM t_personal_dtls "
		+ " WHERE Customer_Id=a.`Customer_Id` OR CID_Number=a.`Customer_Id`) customerName"
		+ " FROM "
		+ "  t_etest_application a "
		+ "WHERE a.Prebooking_Application_No = ?";
	
	private static final String INSERT_INTO_T_WORKFLOW_DTLS_FROM_PAYMENT_DB = "INSERT INTO  `t_workflow_dtls`" +
			"( `Application_Number`, `Status_Id`,`Service_Id`,`Action_Date`,`Actor_Id`,`Actor_Name`, `Role_Id`,`Role_Name`," +
			"`Juris_Id`, `Juris_Type_Id`) " +
			" (SELECT  `Application_Number`,`Status_Id`, `Service_Id`,`Action_Date`," +
			"`Actor_Id`,`Actor_Name`,`Role_Id`,`Role_Name`,`Juris_Id`,`Juris_Type_Id`" +
			" FROM `eralis_payment_db`.`t_workflow_dtls` WHERE `Application_Number`=?)";
	
	private static final String INSERT_INTO_TASKDTLS_FROM_PAYMENT_DB = "INSERT INTO `t_task_dtls`  (`Instance_Id`,  `Seq_Details_Id`, `Application_Number`, " +
			"`Assigned_User_Id`, `Assigned_Priv_Id`,`Task_State_Id`, `Task_Remark`,`Action_Date`)" +
			"(SELECT ?,`Seq_Details_Id`,`Application_Number`,`Assigned_User_Id`," +
			"`Assigned_Priv_Id`,`Task_State_Id`,  `Task_Remark`,`Action_Date`" +
			" FROM `eralis_payment_db`.`t_task_dtls` WHERE " +
			" `Application_Number`=?)";
	
	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_FROM_PAYMENT_DB = "INSERT INTO  `t_vehicle_application` (`Application_Number`,`Application_Type`, "
										+ "             `Vehicle_Registration_Type`,`Customer_Id`,`Registration_Type`, "
										+ "             `Amount`,`Vehicle_Registration_Id`,`Region_Id`, `Vehicle_Prefix`, "
										+ "             `Vehicle_Number`,`Vehicle_Id`,`Vehicle_Type_Id`,`Vanity_Number`, "
										+ "             `Vehicle_Company_Id`,`Vehicle_Model_Id`, `Engine_Type_Id`, "
										+ "             `Engine_Number`, `Chasis_Number`,`Status`,`Color`,`Price`, "
										+ "             `Load_Capacity`, `Dealer_Id`,`Seat_Capacity`,`Vehicle_Horse_Power`, "
										+ "             `Vehicle_Kilowatt`,`Hypothecated_To_Id`, `Unladen_Weight`, "
										+ "             `Letter_Number`, `Letter_Date`,`Purchase_Date`, `Manufacture_Year`, "
										+ "             `Engine_CC`,`Manufactured_Country_Id`,`Purchase_Type`,`Renewal_Amount`, "
										+ "             `Renewal_Duration`,`Conversion_Reason_Id`,`Cancellation_Reason_Id`,`Transferor_Customer_Id`, "
										+ "             `Transfer_Date`, `Transferee_Type`, `Transferee_Customer_Id`, `Duplicate_Issue_Date`, "
										+ "             `Expiry_Date`, `Receipt_Date`,  `Receipt_Number`, `Sale_Deed_Amount`, "
										+ "             `Sale_Deed_Date`, `App_Submission_Date`,`App_Verification_Date`, `App_Approval_Date`, "
										+ "             `Bus_Type_Id`,`Remarks`,`Created_By`,`Created_On`,`Updated_By`,`Updated_On`,`Traffic_Remarks`,  `Conversion_Type`) "
										+ "             (SELECT "
										+ "  `Application_Number`, "
										+ "  `Application_Type`, "
										+ "  `Vehicle_Registration_Type`, "
										+ "  `Customer_Id`, "
										+ "  `Registration_Type`, "
										+ "  `Amount`, "
										+ "  `Vehicle_Registration_Id`, "
										+ "  `Region_Id`, "
										+ "  `Vehicle_Prefix`, "
										+ "  `Vehicle_Number`, "
										+ "  `Vehicle_Id`, "
										+ "  `Vehicle_Type_Id`, "
										+ "  `Vanity_Number`, "
										+ "  `Vehicle_Company_Id`, "
										+ "  `Vehicle_Model_Id`, "
										+ "  `Engine_Type_Id`, "
										+ "  `Engine_Number`, "
										+ "  `Chasis_Number`, "
										+ "  `Status`, "
										+ "  `Color`, "
										+ "  `Price`, "
										+ "  `Load_Capacity`, "
										+ "  `Dealer_Id`, "
										+ "  `Seat_Capacity`, "
										+ "  `Vehicle_Horse_Power`, "
										+ "  `Vehicle_Kilowatt`, "
										+ "  `Hypothecated_To_Id`, "
										+ "  `Unladen_Weight`, "
										+ "  `Letter_Number`, "
										+ "  `Letter_Date`, "
										+ "  `Purchase_Date`, "
										+ "  `Manufacture_Year`, "
										+ "  `Engine_CC`, "
										+ "  `Manufactured_Country_Id`, "
										+ "  `Purchase_Type`, "
										+ "  `Renewal_Amount`, "
										+ "  `Renewal_Duration`, "
										+ "  `Conversion_Reason_Id`, "
										+ "  `Cancellation_Reason_Id`, "
										+ "  `Transferor_Customer_Id`, "
										+ "  `Transfer_Date`, "
										+ "  `Transferee_Type`, "
										+ "  `Transferee_Customer_Id`, "
										+ "  `Duplicate_Issue_Date`, "
										+ "  `Expiry_Date`, "
										+ "  ?, "
										+ "  ?, "
										+ "  `Sale_Deed_Amount`, "
										+ "  `Sale_Deed_Date`, "
										+ "  `App_Submission_Date`, "
										+ "  `App_Verification_Date`, "
										+ "  `App_Approval_Date`, "
										+ "  `Bus_Type_Id`, "
										+ "  `Remarks`, "
										+ "  `Created_By`, "
										+ "  `Created_On`, "
										+ "  `Updated_By`, "
										+ "  `Updated_On`, "
										+ "  `Traffic_Remarks`, "
										+ "  `Conversion_Type` "
										+ "FROM  `eralis_payment_db`.`t_vehicle_application` WHERE `Application_Number`=?)";
	
	private static final String GET_PERSONAL_DTLS_FROM_CID = "SELECT p.Customer_Id,p.`Present_Phone_No`,p.`Title_Of_Courtesy_Id`,p.`Personal_Info_Id`, "
															+ "			   p.`First_Name`, "
															+ "			  p.`Middle_Name`, "
															+ "			  p.`Last_Name`, "
															+ "			  p.`CID_Number`, "
															+ "			  p.`Gender`, "
															+ "			  p.`Customer_Id`, "
															+ "			  p.`Image_Path`, "
															+ "			  p.`Present_Email`, "
															+ "			    DATE_FORMAT(p.`Date_Of_Birth`, '%Y-%m-%d') AS Citizen_Date_Of_Birth, "
															+ "			    COUNT(*) rowCount "
															+ "			   FROM "
															+ "			     `t_personal_dtls` p "
															+ "			   WHERE  p.`CID_Number` = ?;";
	
	
	private static final String GET_VEHICLE_UNPAID_OFFENCE_LIST = "SELECT a.`Offence_Id`,a.`TIN_No`,DATE_FORMAT(a.`Offence_Date`,'%d-%m-%Y')offenceDate,'Not Paid' paidStatus, "
																+ "IF(a.`Place_Of_Inspection`='null','',a.`Place_Of_Inspection`) placeOfInspection "
																+ "FROM t_offence_information a "
																+ "LEFT JOIN t_vehicle_registration_dtls b ON a.`Vehicle_Id`=b.`Vehicle_Reg_Dtls_Id` "
																+ "WHERE a.`Is_paid`='N' AND b.`Vehicle_Number` = ? AND b.`Vehicle_Type_Id`=?";
	
	
	
	private static final String INSERT_INTO_PASSENGER_BUS_ROUTE_PERMIT = "INSERT INTO `t_passenger_bus_route_permit_application` "
															+ "(`Application_Number`, "
															+ "`Applicant_Name`, "
															+ "`DOB`, "
															+ "`Gender`, "
															+ "`Permanent_Dzongkhag`, "
															+ "`Permanenet_Gewog`, "
															+ "`Permanent_Village`, "
															+ "`Phone_No`, "
															+ "`Email_Id`, "
															+ "`Present_Address`, "
															+ "`Route_Type`, "
															+ "`Bus_Category_Id`, "
															+ "`Route_From`, "
															+ "`Route_To`, "
															+ "`Timing`, "
															+ "`Transport_Name`, "
															+ "`Process_Region_Id`, "
															+ "`Process_Base_Id`, "
															+ "`File_Path`, "
															+ "`File_Name`, "
															+ "`Created_By`, "
															+ "`Application_Type`,App_Submission_Date,Customer_Id,CID) "
															+ "VALUES "
															+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?);";
	
	
	
	private static final String GET_APPLICATION_STATUS_OF_DRIVING_LICENSE_SERVICE = "SELECT (SELECT u.`Service_Name` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) AS service_name, "
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'),"
															+" (SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number),"
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`)"
															+" IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap"
															+" WHERE ap.Application_Number=a.Application_Number), IF((SELECT u.`Service_Short_Desc` FROM t_service_master u"
															+" WHERE u.Service_Id=a.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number "
															+" FROM t_vehicle_application ap WHERE ap.Application_Number=a.Application_Number),IF((SELECT u.`Service_Short_Desc`" 
															+" FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), (SELECT ap.`Learner_License_No`"
															+" FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number),'') ))) serviceTypeNo,"
															+" a.`Application_Number`, c.Status_Name, c.Status_Type_Short_Desc, a.Action_Date, a.Role_Name, a.Actor_Name,"
															+" (SELECT u.`name` FROM `t_task_dtls` z LEFT JOIN `t_user_master` u ON u.`login_id` = z.`Assigned_User_Id`"
															+" WHERE z.`Application_Number` = a.`Application_Number` AND z.`Task_State_Id` = 8 )AS claimed_by , "
															+" IF(a.`Juris_Type_Id`=2,(SELECT l.`base_office_name` FROM t_base_office_master l WHERE l.`base_office_id`=a.`Juris_Id`),"
															+" (SELECT l.`region_name` FROM t_region_master l WHERE l.`region_id`=a.`Juris_Id`) ) submittedTo,"
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL')," 
															+" (SELECT p.CID_Number FROM t_learner_license_application ap LEFT JOIN t_personal_dtls p ON "
															+"  ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number),"
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`)"
															+" IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap" 
															+" LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number),"
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) "
															+" IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap"
															+" LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id "
															+" WHERE ap.Application_Number=a.Application_Number),'' ) ) ) cid "
															+" FROM   t_workflow_dtls a,t_status_master c"
															+"  WHERE a.Application_Number IN (SELECT z.`Application_Number` FROM t_driving_license_application z" 
															+" LEFT JOIN t_driving_license_dtls m ON z.`License_Id` = m.`Driving_License_Id`  "
															+" WHERE IF(''=?,1=1,z.`Customer_Id` IN (SELECT u.`Customer_Id` FROM t_personal_dtls u WHERE u.`CID_Number`=?))" 
															+"  AND IF(''=?,1=1, (m.`Driving_License_No`=? ))) AND a.Status_Id = c.Status_Id  order by a.Action_Date desc";
	
	
	private static final String GET_APPLICATION_STATUS_OF_LEARNER_LICENSE_SERVICE = "SELECT (SELECT u.`Service_Name` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) AS service_name, "
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'),"
															+"  (SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number),"
															+"  IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`)"
															+"  IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap"
															+"  WHERE ap.Application_Number=a.Application_Number), IF((SELECT u.`Service_Short_Desc` FROM t_service_master u"
															+"  WHERE u.Service_Id=a.`Service_Id`) IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number "
															+"  FROM t_vehicle_application ap WHERE ap.Application_Number=a.Application_Number),IF((SELECT u.`Service_Short_Desc`" 
															+"  FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL'), (SELECT ap.`Learner_License_No`"
															+"  FROM t_learner_license_application ap WHERE ap.Application_Number=a.Application_Number),'') ))) serviceTypeNo,"
															+"  a.`Application_Number`, c.Status_Name, c.Status_Type_Short_Desc, a.Action_Date, a.Role_Name, a.Actor_Name,"
															+"  (SELECT u.`name` FROM `t_task_dtls` z LEFT JOIN `t_user_master` u ON u.`login_id` = z.`Assigned_User_Id`"
															+"  WHERE z.`Application_Number` = a.`Application_Number` AND z.`Task_State_Id` = 8 )AS claimed_by , "
															+"  IF(a.`Juris_Type_Id`=2,(SELECT l.`base_office_name` FROM t_base_office_master l WHERE l.`base_office_id`=a.`Juris_Id`),"
															+"  (SELECT l.`region_name` FROM t_region_master l WHERE l.`region_id`=a.`Juris_Id`) ) submittedTo,"
															+"  IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) IN('ILL','RLL','DLL')," 
															+"  (SELECT p.CID_Number FROM t_learner_license_application ap LEFT JOIN t_personal_dtls p ON "
															+"  ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number),"
															+"  IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`)"
															+"  IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap" 
															+"  LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=a.Application_Number),"
															+" IF((SELECT u.`Service_Short_Desc` FROM t_service_master u WHERE u.Service_Id=a.`Service_Id`) "
															+"  IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap"
															+"  LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id "
															+"  WHERE ap.Application_Number=a.Application_Number),'' ) ) ) cid "
															+"  FROM   t_workflow_dtls a,t_status_master c"
															+"   WHERE a.Application_Number IN (SELECT z.`Application_Number` FROM t_learner_license_application z" 
															+"  LEFT JOIN t_learner_license_dtls m ON z.`Learner_License_Id` = m.`Learner_License_Info_Id`  "
															 +"  WHERE IF(''=?,1=1,z.`Customer_Id` IN (SELECT u.`Customer_Id` FROM t_personal_dtls u WHERE u.`CID_Number`=?))" 
															 +"    AND IF(''=?,1=1, (m.`Learner_License_No`=? ))) AND a.Status_Id = c.Status_Id  order by a.Action_Date desc";
	
	
	private static final String INSERT_INTO_PAYMENT_DTLS_ON_UNSUCCESSFUL_PAYMENT = "INSERT INTO `eralis_payment_db`.`t_payment_dtls` "
																			+ "            ( "
																			+ "             `Application_Number`, "
																			+ "             `Service_Id`, "
																			+ "             `Amount_Paid`, "
																			+ "             `Receipt_No`, "
																			+ "             `Receipt_Date`, "
																			+ "             `Created_By` , "
																			+ "             `Is_Payment_Successful`,Application_Type) "
																			+ "VALUES (?,'00',?,?,?,'SYSTEM','N',?);";
	
	
	private static final String CHECK_APPLICATION_EXIST_IN_PAYMENT_DB = "SELECT COUNT(*) rowCount FROM `eralis_payment_db`.`t_payment_dtls` WHERE Application_Number=?";

	
	private static final String GET_ONLINEPAYMENT_DTLS = "SELECT a.`Amount_Paid`,a.`Amount_Paid`,a.`Penalty_Paid`,a.`Receipt_Date`,a.`Receipt_No` FROM t_payment_dtls a WHERE a.`Application_Number`=?";

	
	private static final String GET_ONLINEPAYEMENT_BOOKING_DTLS = "SELECT b.`Amount_Paid`,b.`Receipt_Date`,b.`Receipt_No` FROM t_etest_application a"
										+" LEFT JOIN t_payment_dtls b ON b.`Application_Number`=a.`Application_Number`"
										 +" WHERE a.`Prebooking_Application_No`=?";
	
	
	private static final String GET_ONLINEPAYMENT_OFFENCE_DTLS = "SELECT a.`Receipt_Date`,a.`Receipt_Number`,a.`Amount` FROM t_offence_information a WHERE a.`Offence_Id`=?";

	private static final String GET_LEARNER_LICENSE_DTLS_BY_CID_OR_LL = ""
		+ "SELECT count(*)rowCount,(select count(*) from t_driving_license_dtls a where a.customer_id=l.customer_id)countDL, p.`Present_Phone_No`,p.`Title_Of_Courtesy_Id`,p.`Personal_Info_Id`  ,l.`Learner_License_Info_Id`,l.`Issue_Date`, p.`First_Name`,p.`Middle_Name`,p.`Last_Name`,p.`CID_Number`,p.`Gender`,p.`Customer_Id`,p.`Image_Path`,p.`Present_Email`,  DATE_FORMAT(p.`Date_Of_Birth`, '%Y-%m-%d') AS Citizen_Date_Of_Birth,   COUNT(*) rowCount,   e.`Application_Number` AS test_applicationNo,   e.`Test_Date`,   CONCAT(f.`Juris_Type_Id`,'#',f.`Test_Location_Id`) Test_Location_Id,   e.`Drive_Type_Id` "
		+ " FROM   `t_learner_license_dtls` l "
		+ " LEFT JOIN `t_personal_dtls` p     ON p.`Customer_Id` = l.`Customer_Id` "
		+ " LEFT JOIN `t_etest_application` e     ON e.`Learner_License_No` = l.`Learner_License_No`   and e.`Test_Date`>=curdate() "
		+ " LEFT JOIN t_etest_config_master f     ON e.`Test_Location_Id` = f.`Sl_No` "
		+ " WHERE"
		+ " if(?='',1=1,p.`CID_Number` = ?) "
		+ " AND "
		+ " if(?='',1=1,l.Learner_License_No=?)";
	
	
	private static final String CHECK_APPLICATION_SUBMITTED ="select "+
			" (select count(*) from t_payment_dtls b where b.Application_Number=a.Application_Number and b.Is_Payment_Successful='N')rowCount,"+ 
			" a.Application_Number, "+
			" a.Renewal_Duration "+
			" from t_driving_license_application a"+ 
			" where a.License_Id=? and a.Application_Type = ?"+ 
			" order by a.Created_On desc limit 1";
	
	private static final String CHECK_APPLICATION_SUBMITTED_FOR_VEHICLE = "SELECT (select count(*) from t_payment_dtls a " +
			" where  a.Application_Number=b.`Application_Number` and a.Is_Payment_Successful='N')rowCount,b.Application_Number," +
			"b.Renewal_Duration   FROM `t_vehicle_application` b WHERE b.`Vehicle_Id`=? " +
			" and b.`Application_Type`=? order by b.Created_On desc limit 1";
	
	
	
}