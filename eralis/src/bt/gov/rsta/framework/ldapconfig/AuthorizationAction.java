package bt.gov.rsta.framework.ldapconfig;


import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

/**
 * 
 * @author Poojan Sharma
 * AuthorizationAction.java
 */
public class AuthorizationAction {
	
	public DirContext getLDAPInitialDirContext() throws ERALISException, ERALISSystemException
	{	
		String ldapHostName = LdapConstants.BLANK_VALUE;
		String ldapHostRMIPort = LdapConstants.BLANK_VALUE;
		String principal = LdapConstants.BLANK_VALUE;
		String credent = LdapConstants.BLANK_VALUE;
		DirContext returnDirContext = null;

		try
		{
			ResourceBundle ecsLdapConf =ResourceBundle.getBundle(LdapConstants.LDAP_CONFIGURATION_FILE);
			ldapHostName = ecsLdapConf.getString(LdapConstants.LDAP_HOST);
			ldapHostRMIPort = ecsLdapConf.getString(LdapConstants.LDAP_RMIPORT);
			principal = ecsLdapConf.getString(LdapConstants.LDAP_PRINCIPAL);
			credent = ecsLdapConf.getString(LdapConstants.LDAP_CREDENT);
			Hashtable<String, String> env = new Hashtable<String, String>();
			String providerURL = "ldap://" + ldapHostName + ":" + ldapHostRMIPort;
			env.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(javax.naming.Context.PROVIDER_URL, providerURL);
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, principal);
			env.put(Context.SECURITY_CREDENTIALS, credent);
			returnDirContext = new InitialDirContext(env);
		} 
		catch (Exception e) 
		{
			throw new ERALISException("### Error at Authorization[getLDAPInitialDirContext]: exception:: "+e);
		}
		
		return returnDirContext;
	}


}
