package bt.gov.rsta.framework.ldapconfig;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * 
 * @author Poojan D Sharma
 * @date Apr 02, 2012
 * LdapDeleteUserDetails.java
 */

public class LdapDeleteUserDetails 
{
	/*
	 * This method deletes user details from LDAP
	 */
	public static boolean deleteUserDetailsFromLdap(String userId)
	{
		boolean flag = false;
		AuthorizationAction authAction = new AuthorizationAction();
		try
		{
			DirContext dirContext = authAction.getLDAPInitialDirContext();
			String baseDn = "ou=dahe,dc=bhutan,dc=gov";
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = {"uid","userPassword"};
			constraints.setReturningAttributes(attrIDs);
			
			if(dirContext != null)
			{
				NamingEnumeration result = dirContext.search(baseDn, "uid=" + userId, constraints);
				Attribute objclass = new BasicAttribute("objectclass");
				objclass.add("inetOrgPerson");
				
				if(result.hasMore())
				{
					SearchResult r = (SearchResult)result.next();
					String name = r.getName();
					String dn = name +','+ baseDn;
					
					dirContext.destroySubcontext(dn);
					
					flag = true;
				}
			}
			dirContext.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			flag = false;
		}
		
		return flag;
	}
}
