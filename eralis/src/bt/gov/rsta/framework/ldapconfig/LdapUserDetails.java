package bt.gov.rsta.framework.ldapconfig;

/**
 * 
 * @author Poojan Sharma
 * @date Apr 02, 2012
 * LdapUserDetails.java
 */
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import bt.gov.rsta.framework.util.CryptoUtils;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class LdapUserDetails 
{
	/*
	 * This method return true if the userId and 
	 * password matches the LDAP entry or else returns false.
	 * @param String - userId
	 * @param String - password
	 * @return Boolean
	 */
	
	public static Boolean authenticateUserFromLDAP(String userId, String password) throws ERALISException, ERALISSystemException
	{
		Boolean flag = false;
		String uId = userId;
		Log.debug("### Entering LdapUserDetails[authenticateUserFromLDAP] for uId : " + uId);	
		try 
		{
			AuthorizationAction authAction = new AuthorizationAction();
			DirContext dirContext = authAction.getLDAPInitialDirContext();
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			
			
			NamingEnumeration<SearchResult> result = dirContext.search("OU=eralis,DC=rsta,DC=gov", "uid=" + uId, constraints);
			if (result.hasMore()) 
			{
				Attributes attrs = ((SearchResult) result.next()).getAttributes();
					Attribute userPassword = attrs.get("userPassword");
					
					if (userPassword != null) 
					{
						String ldapPassword = new String((byte[])userPassword.get());
						
						CryptoUtils passwordEncryption = new CryptoUtils();
						String encryptedPassword = passwordEncryption.encrypt(password, "SHA-1", "UTF-8");
						String enteredPassword = Integer.toString(encryptedPassword.hashCode());
						String ldapPassword1 = Integer.toString(ldapPassword.hashCode());
						if (ldapPassword1.equals(enteredPassword)) 
						{
							flag = true;
						} 
					}
			}
		} 
		catch (NamingException e) 
		{
			e.printStackTrace();
			throw new ERALISException("### Error at LdapUserDetails[autheticateUserFromLDAP]: exception:: "+e);
		}
		return flag;
	}
}
