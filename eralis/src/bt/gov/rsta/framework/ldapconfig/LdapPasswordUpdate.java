package bt.gov.rsta.framework.ldapconfig;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import bt.gov.rsta.framework.util.CryptoUtils;
import bt.gov.rsta.framework.util.Log;

/**
 * 
 * @author Poojan Sharma
 * @date Apr 05, 2012
 * LdapPasswordUpdate.java
 */
public class LdapPasswordUpdate 
{
	/*
	 * This method is for updating the users password in LDAP
	 * @param userId
	 * @param password
	 * @return Boolean flag
	 */
	public static String updateUserPassword(String userId, String password, String oldPassword) throws Exception
	{
		String flag = "NOT_UPDATED";
		AuthorizationAction authAction = new AuthorizationAction();
		try
		{
			DirContext dirContext = authAction.getLDAPInitialDirContext();
			String baseDn = "ou=eralis,dc=rsta,dc=gov";
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = {"uid","userPassword"};
			constraints.setReturningAttributes(attrIDs);
			
			if(dirContext != null)
			{
				NamingEnumeration result = dirContext.search(baseDn, "uid=" + userId, constraints);
				
				if(result.hasMore())
				{
					String tag = checkMatch(userId,oldPassword);
					
					if("TRUE".equalsIgnoreCase(tag))
					{
						SearchResult r = (SearchResult)result.next();
						String name = r.getName();
						String dn = name +','+ baseDn;
						
						//convert password string to bytes
						byte[] bPwd = password.getBytes();
						Attribute mod = new BasicAttribute("userPassword",bPwd);
						ModificationItem[] mods = new ModificationItem[1];
						mods[0] = new ModificationItem(dirContext.REPLACE_ATTRIBUTE,mod);
						
						//modify the attribute value in ldap
						dirContext.modifyAttributes(dn,mods);
						flag = "UPDATED";
					}
					else if("NO_MATCH".equalsIgnoreCase(tag))
					{
						flag = tag;
					}
				}
			}
			dirContext.close();
		}
		catch (Exception e) 
		{
			Log.error("##### Error in LdapPasswordUpdate[updateUserPassword]: "+e);
			flag = "ERROR";
		}
		
		return flag;
	}
	
	
	/*
	 * This method is for checking whether the entered old password
	 * matches the previous password stored in LDAP or not
	 * @param userId
	 * @param oldPassword
	 * @return String flag
	 */
	public static String checkMatch(String userId, String oldPassword)
	{
		String flag = "FALSE";
		AuthorizationAction authAction = new AuthorizationAction();
		DirContext dirContext;
		try 
		{
			dirContext = authAction.getLDAPInitialDirContext();
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration<SearchResult> result = dirContext.search(
					"OU=eralis,DC=rsta,DC=gov", "uid=" + userId, constraints);
			if (result.hasMore()) 
			{
				Attributes attrs = ((SearchResult) result.next()).getAttributes();
				Attribute userPassword = attrs.get("userPassword");
				Log.debug("userPassword attribute: "+userPassword.toString());
				
				if (userPassword != null) 
				{
					String ldapPassword = new String((byte[])userPassword.get());
					
					CryptoUtils passwordEncryption = new CryptoUtils();
					String encryptedPassword = passwordEncryption.encrypt(oldPassword, "SHA-1", "UTF-8");
					String enteredOldPassword = Integer.toString(encryptedPassword.hashCode());
					String ldapPassword1 = Integer.toString(ldapPassword.hashCode());
					if (ldapPassword1.equalsIgnoreCase(enteredOldPassword)) 
					{
						flag = "TRUE";
					} 
					else
					{
						flag = "NO_MATCH";
					}
				}
			}
		} 
		catch (Exception e) 
		{
			Log.error("##### Error in LdapPasswordUpdate[checkMatch]: "+e);
			flag = "ERROR";
		}
		return flag;
	}
	
	public static Boolean resetUserPassword(String userId, String password) throws Exception
	{
		Boolean flag = false;
		AuthorizationAction authAction = new AuthorizationAction();
		try
		{
			DirContext dirContext = authAction.getLDAPInitialDirContext();
			String baseDn = "ou=eralis,dc=rsta,dc=gov";
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = {"uid","userPassword"};
			constraints.setReturningAttributes(attrIDs);
			
			if(dirContext != null)
			{
				NamingEnumeration result = dirContext.search(baseDn, "uid=" + userId, constraints);
				
				if(result.hasMore())
				{
						SearchResult r = (SearchResult)result.next();
						String name = r.getName();
						String dn = name +','+ baseDn;
						
						//convert password string to bytes
						byte[] bPwd = password.getBytes();
						Attribute mod = new BasicAttribute("userPassword",bPwd);
						ModificationItem[] mods = new ModificationItem[1];
						mods[0] = new ModificationItem(dirContext.REPLACE_ATTRIBUTE,mod);
						
						//modify the attribute value in ldap
						dirContext.modifyAttributes(dn,mods);
						flag = true;
				}
			}
			dirContext.close();
		}
		catch (Exception e) 
		{
			Log.error("##### Error in LdapPasswordUpdate[resetUserPassword]: "+e);
			flag = false;
		}
		
		return flag;
	}
}
