package bt.gov.rsta.framework.ldapconfig;

/**
 * 
 * @author Poojan Sharma
 * @date Sept 17, 2012
 * LdapConstants.java
 */
public class LdapConstants 
{
	//LDAP Configuration
    public static final String LDAP_CONFIGURATION_FILE = "bt.gov.rsta.framework.properties.ldap_context";
    public static final String LDAP_HOST = "ldap_host";
    public static final String LDAP_RMIPORT = "ldap_rmiPort";
    public static final String LDAP_PRINCIPAL = "ldap_principal";
    public static final String LDAP_CREDENT = "ldap_credent";
    public static final String BLANK_VALUE = " ";
    public static final String LDAP_PATH = "ou=eralis,dc=rsta,dc=gov";
}
