package bt.gov.rsta.framework.ldapconfig;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import bt.gov.rsta.eralis.dto.administration.AdministrationDTO;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;

/**
 * 
 * @author Poojan Sharma
 */
public class InsertUsersIntoLDAP 
{
	public static boolean insertUserIntoLdap(AdministrationDTO dto)
	{
		boolean isInsert = false;
		 String usersContainer = "";
		
		 try 
		 {
			 boolean isPresent = checkIfExists(dto.getLoginId());
			 
			 if(!isPresent)
			 {
			 	AuthorizationAction authAction = new AuthorizationAction();
				DirContext dirContext = authAction.getLDAPInitialDirContext();
				
				usersContainer = LdapConstants.LDAP_PATH;
				
				 Attributes attrs = new BasicAttributes(true);
				 Attribute objclass = new BasicAttribute("objectclass");
				 objclass.add("inetOrgPerson");
				 
				 Attribute surName = new BasicAttribute("sn");
				 surName.add(dto.getUserName().trim().equalsIgnoreCase("")?" ":dto.getUserName());
				 Attribute uid = new BasicAttribute("uid");
				 uid.add(dto.getLoginId().trim().equalsIgnoreCase("")?" ":dto.getLoginId());
				 Attribute pwd = new BasicAttribute("userPassword");
				 pwd.add(Constants.ERALIS_PASSWORD_BUILDER+"@"+dto.getLoginId());
				 
				 attrs.put(objclass);
				 attrs.put(surName);
				 attrs.put(uid);
				 attrs.put(pwd);
				 
				 Log.debug("Before creating the sub context");
				 DirContext result = dirContext.createSubcontext("cn="+dto.getLoginId()+","+usersContainer, attrs);
				 
				 if(result != null)
				 {
					 isInsert = true;
				 }
			 }
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
			Log.error("Error occurred in insertUserIntoLdap method of LdapInsertUserDetails class"+e);
		 }
		 
		 return isInsert;
	}
	
	
	public static boolean checkIfExists(String userId) throws Exception
	{
		boolean isPresent = false;
		String uId = userId;
		try 
		{
			AuthorizationAction authAction = new AuthorizationAction();
			DirContext dirContext = authAction.getLDAPInitialDirContext();
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration<SearchResult> result = dirContext.search("OU=eralis,DC=rsta,DC=gov", "uid=" + uId, constraints);
			
			if (result.hasMore()) 
			{
				isPresent = true;
			}
		} 
		catch (NamingException e) 
		{
			e.printStackTrace();
		}
		return isPresent;
	}
}
