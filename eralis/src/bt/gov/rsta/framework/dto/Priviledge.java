package bt.gov.rsta.framework.dto;

import java.io.Serializable;

public class Priviledge implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String priviledgeId;
	private String priviledgeName;
	private String priviledgeCode;
	
	public String getPriviledgeId() {
		return priviledgeId;
	}
	public void setPriviledgeId(String priviledgeId) {
		this.priviledgeId = priviledgeId;
	}
	public String getPriviledgeName() {
		return priviledgeName;
	}
	public void setPriviledgeName(String priviledgeName) {
		this.priviledgeName = priviledgeName;
	}
	public String getPriviledgeCode() {
		return priviledgeCode;
	}
	public void setPriviledgeCode(String priviledgeCode) {
		this.priviledgeCode = priviledgeCode;
	}
}
