package bt.gov.rsta.framework.dto;

import java.io.Serializable;

import org.apache.struts.upload.FormFile;

public class ServiceDTO implements Serializable
 {
	private static final long serialVersionUID = 1L;
	private String cid;
	private String titleCourtesy;
	private String dob;
	private String personalInfoId;
	private String customerId;
	private String name;
	private String occupation;
	private String bloodGroup;
	private String fatherName;
	private String gender;
	private String permanentDzongkhag;
	private String permanentGewog;
	private String permanentVillage;
	private String permanentCountry;
	private String permanentAddress;
	private String presentDzongkhag;
	private String presentContactAddress;
	private String presentPhoneNo;
	private String presentEmail;
	private String image;
	private FormFile newImage;
	private String rowCount;
	private String firstName;
	private String middleName;
	private String lastName;
	private String titleOfcourtesyList;
	private String learnerNo;
	private String issueDate;
	private String licenseNo;
	private String drivingLicenseType;
	private String vehicleNo;
	private String vehicleRegistrationType;
	private String regionId;
	private String vehicleCompany;
	private String registrationDate;
	private String vehicleModelId;
	private String engineNumber;
	private String chassisNumber;
	private String loadCapacity;
	private String seatCapacity;
	private String engineCC;
	private String purchaseType;
	private String tinNo;
	private String testType;
	///
	private String ownerID;
	private String amount;
	private String vehicleCode;
	private String vehiclePrefix;
	private String vehicleType;
	private String vanityNumber;
	private String expiryDate;
	private String receiptDate;
	private String receiptNo;
	private String status;
	private String colour;
	private String price;
	private String dealersName;
	private String letterDate;
	private String hypothecatedTo;
	private String unladenWeight;
	private String letterNo;
	private String manufactureYear;
	private String manufactureCountry;
	private String remarks;
	private String lastRegistrationDate;
	private String penality;
	private String renewalDate;
    private String transferDate;
    private String duplicateIssueDate;
    private String cancellationDate;
    private String cancellationReason;
    private String conversionDate;
    private String conversionReason;
    private String petrolRadio;
    private String dieselRadio;
    private String testedOn;
    private String validUntil;
    private String testLocation;
    private String cO;
    private String hSU;
    private String testResult;
    private String testFrequency;
    private String postedDate;
    private String printDate;
    private String applicationNumber;
    private String renewalAmount;
/////
    private String customerID;
	private String nationality;
	private String national;
	private String international;
	private FormFile upload;
	private String code;
	private String type;
	private String ministry;
	private String department;
	private String region;
	private String LLNo;
	private String formType;
	private String titleOfcourtesy;
	private String firstname;
	private String middlename;
	private String CID;
	private String DOB;
	private String bloodGroupId;
	private String fathersName;
	private String identificationMarks;
	private String drivetype;
	private String searchpermitID;
	private String dzongkhag;
	private String gewog;
	private String village;
	private String country;
	private String address;
	private String contactAddress;
	private String phone;
	private String email;
	private String conductorName;
	private String accidentDate;
	private String accidentCause;
	private String accidentSite;
	private String driverName;
	private String permitID;
	private String registrationNo;
	private String baseoffice;
	private String routeBetween;
	private String to;
	private String carryingCapacity;
	private String dateOfissue;
	private String validUpto;
	private String licensetype;
	private String numberOfDeath;
	private String numberofInjured;
	private String numberofunInjured;
	private String registrationcertificate;
	private String issurance;
	private String certificateOfroadworthiness;
	private String emmissioncertificate;
	private String manualFlag;
	private String registrationType;
	private String vehicleNumber;	
	private String vehicleModel;
//////
	private String personalRadio;
	private String organisationRadio;
	private String engineType;
	private String chasisNumber;
	private String citizenID;
    private String vehicleRegistrationId;
    private String vehicleRegDtlsId;
   
    private String testDtlsType;
    private String emissionTestType;
	private String pageId;
    private String appsubmissiondate;
    private String organizationName;
    private String transferorCustomerId;
    private String transfereeCustomerId;
	private String transfereeName;
	private String transfereeDzongkhag;
    private String transfereeGewog;
    private String transfereeAddress;
    private String transfereeRegion;
    private String customerId1;

	private String organisationName;
    private String organisationRegion;
	private String organisationDzongkhag;
    private String organisationAddress;
    private String organisationPhone;
    private String organisationEmail;
    private String organisationRemarks;
	private String testDate;
	private String certifyingDoctor;
	private String driveTypeList;
	private String issuedTo;
    private String drivinglicenseId;
    private String endorseddate;
    private String testmarks;
    private String iid;
    private String oldImage;
    private String testLocationId;
    private String jurisTypeId;
    private String applicationNo;
    private String serviceId;
    private String reason;
    private String serviceCode;
    private String purchaseDate;
    private FormFile fileMC;
	private FormFile fileApplForm;
	private FormFile supportDoc;
	private FormFile rcCertificate;
	private String vehicleHorsePower;
	private String vehicleKiloWatt;
	private FormFile invoice;
    private FormFile challan;
    private FormFile letterOfAuthenticity;
    private FormFile emission;
    private FormFile exemptionCertificate;
    private FormFile exciseInvoice;
    private FormFile customDeclaration;
    private FormFile vehiclePicture;
    private FormFile supportingDocument;
	private String renewalType;
	private String driveTypeId;
	private String conversionReasonId;
	private String penalty;
	private String renewalDuration;
	private String issueType;
    private String saleDeedAmount;
    private String saleDeedDate;
    private String tcbEndorsement;
    private String[] drivetypeTCB;
    private String sqlQuery;
    private String title;
    private String description;
    private String whereParam;
    private String dzongkhagName;
	private String gewogName;
	private String bloodTypeName;
	private String busType;
	private String inspectedby;
	private String isPaid;
	private String offenceId;
	private String offenceDate;
	private String learnerLicenseNo;
	private String age;   
	private String totalDuration;
	private String dateDifference;
	private String learnerLicenseId;
	private String otpNumber;
	private String jurisId;
    private String busCategory;
    private String routeFrom;
    private String routeTo;
    private String timing;
    private String transport;
    private String location;
    private String guardianNo;
	private String isInternational;
    
    
	
	public String getIsInternational() {
		return isInternational;
	}
	public void setIsInternational(String isInternational) {
		this.isInternational = isInternational;
	}
	public String getGuardianNo() {
		return guardianNo;
	}
	public void setGuardianNo(String guardianNo) {
		this.guardianNo = guardianNo;
	}
    
    
	public String getBusCategory() {
		return busCategory;
	}
	public void setBusCategory(String busCategory) {
		this.busCategory = busCategory;
	}
	public String getRouteFrom() {
		return routeFrom;
	}
	public void setRouteFrom(String routeFrom) {
		this.routeFrom = routeFrom;
	}
	public String getRouteTo() {
		return routeTo;
	}
	public void setRouteTo(String routeTo) {
		this.routeTo = routeTo;
	}
	public String getTiming() {
		return timing;
	}
	public void setTiming(String timing) {
		this.timing = timing;
	}
	public String getTransport() {
		return transport;
	}
	public void setTransport(String transport) {
		this.transport = transport;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getJurisId() {
		return jurisId;
	}
	public void setJurisId(String jurisId) {
		this.jurisId = jurisId;
	}
	public String getOtpNumber() {
		return otpNumber;
	}
	public void setOtpNumber(String otpNumber) {
		this.otpNumber = otpNumber;
	}
	public String getLearnerLicenseId() {
		return learnerLicenseId;
	}
	public void setLearnerLicenseId(String learnerLicenseId) {
		this.learnerLicenseId = learnerLicenseId;
	}
	public String getTotalDuration() {
		return totalDuration;
	}
	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}
	public String getDateDifference() {
		return dateDifference;
	}
	public void setDateDifference(String dateDifference) {
		this.dateDifference = dateDifference;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(String isPaid) {
		this.isPaid = isPaid;
	}
	public String getOffenceId() {
		return offenceId;
	}
	public void setOffenceId(String offenceId) {
		this.offenceId = offenceId;
	}
	public String getOffenceDate() {
		return offenceDate;
	}
	public void setOffenceDate(String offenceDate) {
		this.offenceDate = offenceDate;
	}
	public String getLearnerLicenseNo() {
		return learnerLicenseNo;
	}
	public void setLearnerLicenseNo(String learnerLicenseNo) {
		this.learnerLicenseNo = learnerLicenseNo;
	}
	public String getInspectedby() {
		return inspectedby;
	}
	public void setInspectedby(String inspectedby) {
		this.inspectedby = inspectedby;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getDzongkhagName() {
		return dzongkhagName;
	}
	public void setDzongkhagName(String dzongkhagName) {
		this.dzongkhagName = dzongkhagName;
	}
	public String getGewogName() {
		return gewogName;
	}
	public void setGewogName(String gewogName) {
		this.gewogName = gewogName;
	}
	public String getBloodTypeName() {
		return bloodTypeName;
	}
	public void setBloodTypeName(String bloodTypeName) {
		this.bloodTypeName = bloodTypeName;
	}
	public String getWhereParam() {
		return whereParam;
	}
	public void setWhereParam(String whereParam) {
		this.whereParam = whereParam;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSqlQuery() {
		return sqlQuery;
	}
	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}
	public String getTcbEndorsement() {
		return tcbEndorsement;
	}
	public String[] getDrivetypeTCB() {
		return drivetypeTCB;
	}
	public void setDrivetypeTCB(String[] drivetypeTCB) {
		this.drivetypeTCB = drivetypeTCB;
	}
	public void setTcbEndorsement(String tcbEndorsement) {
		this.tcbEndorsement = tcbEndorsement;
	}
	public String getSaleDeedAmount() {
		return saleDeedAmount;
	}
	public void setSaleDeedAmount(String saleDeedAmount) {
		this.saleDeedAmount = saleDeedAmount;
	}
	public String getSaleDeedDate() {
		return saleDeedDate;
	}
	public void setSaleDeedDate(String saleDeedDate) {
		this.saleDeedDate = saleDeedDate;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public String getRenewalDuration() {
		return renewalDuration;
	}
	public void setRenewalDuration(String renewalDuration) {
		this.renewalDuration = renewalDuration;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getConversionReasonId() {
		return conversionReasonId;
	}
	public void setConversionReasonId(String conversionReasonId) {
		this.conversionReasonId = conversionReasonId;
	}
	public String getDriveTypeId() {
		return driveTypeId;
	}
	public void setDriveTypeId(String driveTypeId) {
		this.driveTypeId = driveTypeId;
	}
	public String getRenewalType() {
		return renewalType;
	}
	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}
	public FormFile getInvoice() {
		return invoice;
	}
	public void setInvoice(FormFile invoice) {
		this.invoice = invoice;
	}
	public FormFile getChallan() {
		return challan;
	}
	public void setChallan(FormFile challan) {
		this.challan = challan;
	}
	public FormFile getLetterOfAuthenticity() {
		return letterOfAuthenticity;
	}
	public void setLetterOfAuthenticity(FormFile letterOfAuthenticity) {
		this.letterOfAuthenticity = letterOfAuthenticity;
	}
	public FormFile getEmission() {
		return emission;
	}
	public void setEmission(FormFile emission) {
		this.emission = emission;
	}
	public FormFile getExemptionCertificate() {
		return exemptionCertificate;
	}
	public void setExemptionCertificate(FormFile exemptionCertificate) {
		this.exemptionCertificate = exemptionCertificate;
	}
	public FormFile getExciseInvoice() {
		return exciseInvoice;
	}
	public void setExciseInvoice(FormFile exciseInvoice) {
		this.exciseInvoice = exciseInvoice;
	}
	public FormFile getCustomDeclaration() {
		return customDeclaration;
	}
	public void setCustomDeclaration(FormFile customDeclaration) {
		this.customDeclaration = customDeclaration;
	}
	public FormFile getVehiclePicture() {
		return vehiclePicture;
	}
	public void setVehiclePicture(FormFile vehiclePicture) {
		this.vehiclePicture = vehiclePicture;
	}
	public FormFile getSupportingDocument() {
		return supportingDocument;
	}
	public void setSupportingDocument(FormFile supportingDocument) {
		this.supportingDocument = supportingDocument;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	public String getVehicleKiloWatt() {
		return vehicleKiloWatt;
	}
	public void setVehicleKiloWatt(String vehicleKiloWatt) {
		this.vehicleKiloWatt = vehicleKiloWatt;
	}
	public String getVehicleHorsePower() {
		return vehicleHorsePower;
	}
	public void setVehicleHorsePower(String vehicleHorsePower) {
		this.vehicleHorsePower = vehicleHorsePower;
	}
	public FormFile getFileMC() {
		return fileMC;
	}
	public void setFileMC(FormFile fileMC) {
		this.fileMC = fileMC;
	}
	public FormFile getFileApplForm() {
		return fileApplForm;
	}
	public void setFileApplForm(FormFile fileApplForm) {
		this.fileApplForm = fileApplForm;
	}
	public FormFile getSupportDoc() {
		return supportDoc;
	}
	public void setSupportDoc(FormFile supportDoc) {
		this.supportDoc = supportDoc;
	}
	public FormFile getRcCertificate() {
		return rcCertificate;
	}
	public void setRcCertificate(FormFile rcCertificate) {
		this.rcCertificate = rcCertificate;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getTestLocationId() {
		return testLocationId;
	}
	public void setTestLocationId(String testLocationId) {
		this.testLocationId = testLocationId;
	}
	public String getJurisTypeId() {
		return jurisTypeId;
	}
	public void setJurisTypeId(String jurisTypeId) {
		this.jurisTypeId = jurisTypeId;
	}
	public String getOldImage() {
		return oldImage;
	}
	public void setOldImage(String oldImage) {
		this.oldImage = oldImage;
	}
    
	public FormFile getNewImage() {
		return newImage;
	}
	public void setNewImage(FormFile newImage) {
		this.newImage = newImage;
	}
	public String getEndorseddate() {
		return endorseddate;
	}
	public void setEndorseddate(String endorseddate) {
		this.endorseddate = endorseddate;
	}
	public String getTestmarks() {
		return testmarks;
	}
	public void setTestmarks(String testmarks) {
		this.testmarks = testmarks;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	public String getDrivinglicenseId() {
		return drivinglicenseId;
	}
	public void setDrivinglicenseId(String drivinglicenseId) {
		this.drivinglicenseId = drivinglicenseId;
	}
	public String getIssuedTo() {
		return issuedTo;
	}
	public void setIssuedTo(String issuedTo) {
		this.issuedTo = issuedTo;
	}
	public String getDriveTypeList() {
		return driveTypeList;
	}
	public void setDriveTypeList(String driveTypeList) {
		this.driveTypeList = driveTypeList;
	}
	public String getCertifyingDoctor() {
		return certifyingDoctor;
	}
	public void setCertifyingDoctor(String certifyingDoctor) {
		this.certifyingDoctor = certifyingDoctor;
	}
	public String getTestDate() {
		return testDate;
	}
	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}
	public String getOrganisationAddress() {
		return organisationAddress;
	}
	public void setOrganisationAddress(String organisationAddress) {
		this.organisationAddress = organisationAddress;
	}
	public String getOrganisationPhone() {
		return organisationPhone;
	}
	public void setOrganisationPhone(String organisationPhone) {
		this.organisationPhone = organisationPhone;
	}
	public String getOrganisationEmail() {
		return organisationEmail;
	}
	public void setOrganisationEmail(String organisationEmail) {
		this.organisationEmail = organisationEmail;
	}
	public String getOrganisationRemarks() {
		return organisationRemarks;
	}
	public void setOrganisationRemarks(String organisationRemarks) {
		this.organisationRemarks = organisationRemarks;
	}
	public String getOrganisationDzongkhag() {
		return organisationDzongkhag;
	}
	public void setOrganisationDzongkhag(String organisationDzongkhag) {
		this.organisationDzongkhag = organisationDzongkhag;
	}
	public String getOrganisationRegion() {
		return organisationRegion;
	}
	public void setOrganisationRegion(String organisationRegion) {
		this.organisationRegion = organisationRegion;
	}
	public String getOrganisationName() {
		return organisationName;
	}
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}
	public String getPersonalRadio() {
		return personalRadio;
	}
	public void setPersonalRadio(String personalRadio) {
		this.personalRadio = personalRadio;
	}
	public String getOrganisationRadio() {
		return organisationRadio;
	}
	public void setOrganisationRadio(String organisationRadio) {
		this.organisationRadio = organisationRadio;
	}
	public String getEngineType() {
		return engineType;
	}
	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
	public String getChasisNumber() {
		return chasisNumber;
	}
	public void setChasisNumber(String chasisNumber) {
		this.chasisNumber = chasisNumber;
	}
	public String getCitizenID() {
		return citizenID;
	}
	public void setCitizenID(String citizenID) {
		this.citizenID = citizenID;
	}
	public String getVehicleRegistrationId() {
		return vehicleRegistrationId;
	}
	public void setVehicleRegistrationId(String vehicleRegistrationId) {
		this.vehicleRegistrationId = vehicleRegistrationId;
	}
	public String getVehicleRegDtlsId() {
		return vehicleRegDtlsId;
	}
	public void setVehicleRegDtlsId(String vehicleRegDtlsId) {
		this.vehicleRegDtlsId = vehicleRegDtlsId;
	}
	public String getTestDtlsType() {
		return testDtlsType;
	}
	public void setTestDtlsType(String testDtlsType) {
		this.testDtlsType = testDtlsType;
	}
	public String getEmissionTestType() {
		return emissionTestType;
	}
	public void setEmissionTestType(String emissionTestType) {
		this.emissionTestType = emissionTestType;
	}
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getAppsubmissiondate() {
		return appsubmissiondate;
	}
	public void setAppsubmissiondate(String appsubmissiondate) {
		this.appsubmissiondate = appsubmissiondate;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getTransferorCustomerId() {
		return transferorCustomerId;
	}
	public void setTransferorCustomerId(String transferorCustomerId) {
		this.transferorCustomerId = transferorCustomerId;
	}
	public String getTransfereeCustomerId() {
		return transfereeCustomerId;
	}
	public void setTransfereeCustomerId(String transfereeCustomerId) {
		this.transfereeCustomerId = transfereeCustomerId;
	}
	public String getTransfereeName() {
		return transfereeName;
	}
	public void setTransfereeName(String transfereeName) {
		this.transfereeName = transfereeName;
	}
	public String getTransfereeDzongkhag() {
		return transfereeDzongkhag;
	}
	public void setTransfereeDzongkhag(String transfereeDzongkhag) {
		this.transfereeDzongkhag = transfereeDzongkhag;
	}
	public String getTransfereeGewog() {
		return transfereeGewog;
	}
	public void setTransfereeGewog(String transfereeGewog) {
		this.transfereeGewog = transfereeGewog;
	}
	public String getTransfereeAddress() {
		return transfereeAddress;
	}
	public void setTransfereeAddress(String transfereeAddress) {
		this.transfereeAddress = transfereeAddress;
	}
	public String getTransfereeRegion() {
		return transfereeRegion;
	}
	public void setTransfereeRegion(String transfereeRegion) {
		this.transfereeRegion = transfereeRegion;
	}
	public String getCustomerId1() {
		return customerId1;
	}
	public void setCustomerId1(String customerId1) {
		this.customerId1 = customerId1;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	
	
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getTitleOfcourtesy() {
		return titleOfcourtesy;
	}
	public void setTitleOfcourtesy(String titleOfcourtesy) {
		this.titleOfcourtesy = titleOfcourtesy;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getCID() {
		return CID;
	}
	public void setCID(String cID) {
		CID = cID;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getBloodGroupId() {
		return bloodGroupId;
	}
	public void setBloodGroupId(String bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getIdentificationMarks() {
		return identificationMarks;
	}
	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}
	public String getDrivetype() {
		return drivetype;
	}
	public void setDrivetype(String drivetype) {
		this.drivetype = drivetype;
	}
	public String getSearchpermitID() {
		return searchpermitID;
	}
	public void setSearchpermitID(String searchpermitID) {
		this.searchpermitID = searchpermitID;
	}
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getGewog() {
		return gewog;
	}
	public void setGewog(String gewog) {
		this.gewog = gewog;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getConductorName() {
		return conductorName;
	}
	public void setConductorName(String conductorName) {
		this.conductorName = conductorName;
	}
	public String getAccidentDate() {
		return accidentDate;
	}
	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}
	public String getAccidentCause() {
		return accidentCause;
	}
	public void setAccidentCause(String accidentCause) {
		this.accidentCause = accidentCause;
	}
	public String getAccidentSite() {
		return accidentSite;
	}
	public void setAccidentSite(String accidentSite) {
		this.accidentSite = accidentSite;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getPermitID() {
		return permitID;
	}
	public void setPermitID(String permitID) {
		this.permitID = permitID;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public String getBaseoffice() {
		return baseoffice;
	}
	public void setBaseoffice(String baseoffice) {
		this.baseoffice = baseoffice;
	}
	public String getRouteBetween() {
		return routeBetween;
	}
	public void setRouteBetween(String routeBetween) {
		this.routeBetween = routeBetween;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getCarryingCapacity() {
		return carryingCapacity;
	}
	public void setCarryingCapacity(String carryingCapacity) {
		this.carryingCapacity = carryingCapacity;
	}
	public String getDateOfissue() {
		return dateOfissue;
	}
	public void setDateOfissue(String dateOfissue) {
		this.dateOfissue = dateOfissue;
	}
	public String getValidUpto() {
		return validUpto;
	}
	public void setValidUpto(String validUpto) {
		this.validUpto = validUpto;
	}
	public String getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(String licensetype) {
		this.licensetype = licensetype;
	}
	public String getNumberOfDeath() {
		return numberOfDeath;
	}
	public void setNumberOfDeath(String numberOfDeath) {
		this.numberOfDeath = numberOfDeath;
	}
	public String getNumberofInjured() {
		return numberofInjured;
	}
	public void setNumberofInjured(String numberofInjured) {
		this.numberofInjured = numberofInjured;
	}
	public String getNumberofunInjured() {
		return numberofunInjured;
	}
	public void setNumberofunInjured(String numberofunInjured) {
		this.numberofunInjured = numberofunInjured;
	}
	public String getRegistrationcertificate() {
		return registrationcertificate;
	}
	public void setRegistrationcertificate(String registrationcertificate) {
		this.registrationcertificate = registrationcertificate;
	}
	public String getIssurance() {
		return issurance;
	}
	public void setIssurance(String issurance) {
		this.issurance = issurance;
	}
	public String getCertificateOfroadworthiness() {
		return certificateOfroadworthiness;
	}
	public void setCertificateOfroadworthiness(String certificateOfroadworthiness) {
		this.certificateOfroadworthiness = certificateOfroadworthiness;
	}
	public String getEmmissioncertificate() {
		return emmissioncertificate;
	}
	public void setEmmissioncertificate(String emmissioncertificate) {
		this.emmissioncertificate = emmissioncertificate;
	}
	public String getManualFlag() {
		return manualFlag;
	}
	public void setManualFlag(String manualFlag) {
		this.manualFlag = manualFlag;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getNational() {
		return national;
	}
	public void setNational(String national) {
		this.national = national;
	}
	public String getInternational() {
		return international;
	}
	public void setInternational(String international) {
		this.international = international;
	}
	public FormFile getUpload() {
		return upload;
	}
	public void setUpload(FormFile upload) {
		this.upload = upload;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getLLNo() {
		return LLNo;
	}
	public void setLLNo(String lLNo) {
		LLNo = lLNo;
	}
	public String getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getVehicleCode() {
		return vehicleCode;
	}
	public void setVehicleCode(String vehicleCode) {
		this.vehicleCode = vehicleCode;
	}
	public String getVehiclePrefix() {
		return vehiclePrefix;
	}
	public void setVehiclePrefix(String vehiclePrefix) {
		this.vehiclePrefix = vehiclePrefix;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVanityNumber() {
		return vanityNumber;
	}
	public void setVanityNumber(String vanityNumber) {
		this.vanityNumber = vanityNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDealersName() {
		return dealersName;
	}
	public void setDealersName(String dealersName) {
		this.dealersName = dealersName;
	}
	public String getLetterDate() {
		return letterDate;
	}
	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}
	public String getHypothecatedTo() {
		return hypothecatedTo;
	}
	public void setHypothecatedTo(String hypothecatedTo) {
		this.hypothecatedTo = hypothecatedTo;
	}
	public String getUnladenWeight() {
		return unladenWeight;
	}
	public void setUnladenWeight(String unladenWeight) {
		this.unladenWeight = unladenWeight;
	}
	public String getLetterNo() {
		return letterNo;
	}
	public void setLetterNo(String letterNo) {
		this.letterNo = letterNo;
	}
	public String getManufactureYear() {
		return manufactureYear;
	}
	public void setManufactureYear(String manufactureYear) {
		this.manufactureYear = manufactureYear;
	}
	public String getManufactureCountry() {
		return manufactureCountry;
	}
	public void setManufactureCountry(String manufactureCountry) {
		this.manufactureCountry = manufactureCountry;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getLastRegistrationDate() {
		return lastRegistrationDate;
	}
	public void setLastRegistrationDate(String lastRegistrationDate) {
		this.lastRegistrationDate = lastRegistrationDate;
	}
	public String getPenality() {
		return penality;
	}
	public void setPenality(String penality) {
		this.penality = penality;
	}
	public String getRenewalDate() {
		return renewalDate;
	}
	public void setRenewalDate(String renewalDate) {
		this.renewalDate = renewalDate;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	public String getDuplicateIssueDate() {
		return duplicateIssueDate;
	}
	public void setDuplicateIssueDate(String duplicateIssueDate) {
		this.duplicateIssueDate = duplicateIssueDate;
	}
	public String getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public String getCancellationReason() {
		return cancellationReason;
	}
	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	public String getConversionDate() {
		return conversionDate;
	}
	public void setConversionDate(String conversionDate) {
		this.conversionDate = conversionDate;
	}
	public String getConversionReason() {
		return conversionReason;
	}
	public void setConversionReason(String conversionReason) {
		this.conversionReason = conversionReason;
	}
	public String getPetrolRadio() {
		return petrolRadio;
	}
	public void setPetrolRadio(String petrolRadio) {
		this.petrolRadio = petrolRadio;
	}
	public String getDieselRadio() {
		return dieselRadio;
	}
	public void setDieselRadio(String dieselRadio) {
		this.dieselRadio = dieselRadio;
	}
	public String getTestedOn() {
		return testedOn;
	}
	public void setTestedOn(String testedOn) {
		this.testedOn = testedOn;
	}
	public String getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}
	public String getTestLocation() {
		return testLocation;
	}
	public void setTestLocation(String testLocation) {
		this.testLocation = testLocation;
	}
	public String getcO() {
		return cO;
	}
	public void setcO(String cO) {
		this.cO = cO;
	}
	public String gethSU() {
		return hSU;
	}
	public void sethSU(String hSU) {
		this.hSU = hSU;
	}
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	public String getTestFrequency() {
		return testFrequency;
	}
	public void setTestFrequency(String testFrequency) {
		this.testFrequency = testFrequency;
	}
	public String getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public String getRenewalAmount() {
		return renewalAmount;
	}
	public void setRenewalAmount(String renewalAmount) {
		this.renewalAmount = renewalAmount;
	}
	public String getTinNo() {
		return tinNo;
	}
	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}
	public String getVehicleRegistrationType() {
		return vehicleRegistrationType;
	}
	public void setVehicleRegistrationType(String vehicleRegistrationType) {
		this.vehicleRegistrationType = vehicleRegistrationType;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getVehicleCompany() {
		return vehicleCompany;
	}
	public void setVehicleCompany(String vehicleCompany) {
		this.vehicleCompany = vehicleCompany;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getVehicleModelId() {
		return vehicleModelId;
	}
	public void setVehicleModelId(String vehicleModelId) {
		this.vehicleModelId = vehicleModelId;
	}
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	public String getChassisNumber() {
		return chassisNumber;
	}
	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}
	public String getLoadCapacity() {
		return loadCapacity;
	}
	public void setLoadCapacity(String loadCapacity) {
		this.loadCapacity = loadCapacity;
	}
	public String getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(String seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getPurchaseType() {
		return purchaseType;
	}
	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getDrivingLicenseType() {
		return drivingLicenseType;
	}
	public void setDrivingLicenseType(String drivingLicenseType) {
		this.drivingLicenseType = drivingLicenseType;
	}
	 
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getLearnerNo() {
		return learnerNo;
	}
	public void setLearnerNo(String learnerNo) {
		this.learnerNo = learnerNo;
	}
	public String getTitleOfcourtesyList() {
		return titleOfcourtesyList;
	}
	public void setTitleOfcourtesyList(String titleOfcourtesyList) {
		this.titleOfcourtesyList = titleOfcourtesyList;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTitleCourtesy() {
		return titleCourtesy;
	}
	public void setTitleCourtesy(String titleCourtesy) {
		this.titleCourtesy = titleCourtesy;
	}
	public String getRowCount() {
		return rowCount;
	}
	public void setRowCount(String rowCount) {
		this.rowCount = rowCount;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPersonalInfoId() {
		return personalInfoId;
	}
	public void setPersonalInfoId(String personalInfoId) {
		this.personalInfoId = personalInfoId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPermanentDzongkhag() {
		return permanentDzongkhag;
	}
	public void setPermanentDzongkhag(String permanentDzongkhag) {
		this.permanentDzongkhag = permanentDzongkhag;
	}
	public String getPermanentGewog() {
		return permanentGewog;
	}
	public void setPermanentGewog(String permanentGewog) {
		this.permanentGewog = permanentGewog;
	}
	public String getPermanentVillage() {
		return permanentVillage;
	}
	public void setPermanentVillage(String permanentVillage) {
		this.permanentVillage = permanentVillage;
	}
	public String getPermanentCountry() {
		return permanentCountry;
	}
	public void setPermanentCountry(String permanentCountry) {
		this.permanentCountry = permanentCountry;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getPresentDzongkhag() {
		return presentDzongkhag;
	}
	public void setPresentDzongkhag(String presentDzongkhag) {
		this.presentDzongkhag = presentDzongkhag;
	}
	public String getPresentContactAddress() {
		return presentContactAddress;
	}
	public void setPresentContactAddress(String presentContactAddress) {
		this.presentContactAddress = presentContactAddress;
	}
	public String getPresentPhoneNo() {
		return presentPhoneNo;
	}
	public void setPresentPhoneNo(String presentPhoneNo) {
		this.presentPhoneNo = presentPhoneNo;
	}
	public String getPresentEmail() {
		return presentEmail;
	}
	public void setPresentEmail(String presentEmail) {
		this.presentEmail = presentEmail;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
 }
