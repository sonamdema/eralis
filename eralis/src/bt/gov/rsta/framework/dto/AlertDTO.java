package bt.gov.rsta.framework.dto;

import java.io.Serializable;

public class AlertDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String applicationNo;
	private String serviceName;
	private String processedFrom;
	private String licenseNo;
	private String receiptDate;
	private String elapsedDuration;
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getProcessedFrom() {
		return processedFrom;
	}
	public void setProcessedFrom(String processedFrom) {
		this.processedFrom = processedFrom;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getElapsedDuration() {
		return elapsedDuration;
	}
	public void setElapsedDuration(String elapsedDuration) {
		this.elapsedDuration = elapsedDuration;
	}
	
	
}
