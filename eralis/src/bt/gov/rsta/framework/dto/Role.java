package bt.gov.rsta.framework.dto;

import java.io.Serializable;

public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	private String roleId;
	private String roleName;
	private String roleCode;
	private Service[] services;
	
	
	public Service[] getServices() {
		return services;
	}
	public void setServices(Service[] services) {
		this.services = services;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
}
