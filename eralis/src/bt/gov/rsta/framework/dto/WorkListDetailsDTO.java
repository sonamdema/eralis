package bt.gov.rsta.framework.dto;

import java.io.Serializable;

public class WorkListDetailsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String appNo = null;
	private String serviceCode = null;
	private String serviceName = null;
	private String statusCode = null;
	private String statusName = null;
	private String lastUpdatedDate = null;
	private String assignedUserId = null;
	private String taskStatus = null;
	private String appSubmissionDate = null;
	private String jurisdiction;
	private String location;
	private String dzongkhagSerialNo;
	private String presentVillageSerialNo;
	private String permanentVillageSerialNo;
	private String actorName;

	private String serviceTypeNo;
	private String customerId;
	private String customerName;
	
	
	
	
	public String getServiceTypeNo() {
		return serviceTypeNo;
	}
	public void setServiceTypeNo(String serviceTypeNo) {
		this.serviceTypeNo = serviceTypeNo;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getActorName() {
		return actorName;
	}
	public void setActorName(String actorName) {
		this.actorName = actorName;
	}
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getAssignedUserId() {
		return assignedUserId;
	}
	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	public String getAppSubmissionDate() {
		return appSubmissionDate;
	}
	public void setAppSubmissionDate(String appSubmissionDate) {
		this.appSubmissionDate = appSubmissionDate;
	}
	public String getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDzongkhagSerialNo() {
		return dzongkhagSerialNo;
	}
	public void setDzongkhagSerialNo(String dzongkhagSerialNo) {
		this.dzongkhagSerialNo = dzongkhagSerialNo;
	}
	public String getPresentVillageSerialNo() {
		return presentVillageSerialNo;
	}
	public void setPresentVillageSerialNo(String presentVillageSerialNo) {
		this.presentVillageSerialNo = presentVillageSerialNo;
	}
	public String getPermanentVillageSerialNo() {
		return permanentVillageSerialNo;
	}
	public void setPermanentVillageSerialNo(String permanentVillageSerialNo) {
		this.permanentVillageSerialNo = permanentVillageSerialNo;
	}
}
