package bt.gov.rsta.framework.dto;

import java.io.Serializable;

public class DropDownDTO implements Serializable{
private static final long serialVersionUID = 1L;
	
	private String headerId;
	private String headerName;
	
	
	public DropDownDTO() {
		super();
	}
	public DropDownDTO(String headerId, String headerName) {
		super();
		this.headerId = headerId;
		this.headerName = headerName;
	}
	
	
	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}
	public String getHeaderId() {
		return headerId;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	public String getHeaderName() {
		return headerName;
	}
}
