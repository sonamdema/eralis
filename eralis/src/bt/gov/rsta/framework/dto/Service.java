package bt.gov.rsta.framework.dto;

import java.io.Serializable;

public class Service implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String serviceId;
	private String serviceName;
	private String serviceCode;
	private Priviledge[] priviledges;
	
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public Priviledge[] getPriviledges() {
		return priviledges;
	}
	public void setPriviledges(Priviledge[] priviledges) {
		this.priviledges = priviledges;
	}
}
