package bt.gov.rsta.framework.web.exception;

public class ERALISException extends Exception{

	private static final long serialVersionUID = 6548634662667555087L;
	    
	    public ERALISException()
	    {
	        super();
	    }

	    public ERALISException(String message)
	    {
	        super(message);
	    }

	    public ERALISException(String message, Throwable cause)
	    {
	        super(message, cause);
	    }

	    public ERALISException(Throwable cause)
	    {
	        super(cause);
	    }
		
}
