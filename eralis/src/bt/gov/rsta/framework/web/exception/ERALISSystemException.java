package bt.gov.rsta.framework.web.exception;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ERALISSystemException extends Exception {

	private static final long serialVersionUID = 1L;
	private int sqlErrorCode = 0;
	private String timestamp = null;
	private String errorKey = null;
	
	public int getSqlErrorCode() {
		return sqlErrorCode;
	}
	public void setSqlErrorCode(int sqlErrorCode) {
		this.sqlErrorCode = sqlErrorCode;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getErrorKey() {
		return errorKey;
	}
	public void setErrorKey(String errorKey) {
		this.errorKey = errorKey;
	}
	
	public ERALISSystemException(int sqlErrorCode, String errorKey){
		this.sqlErrorCode = sqlErrorCode; 
		this.errorKey = errorKey;
		
		SimpleDateFormat opFormatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateString=opFormatter.format(new Date());
		this.timestamp = dateString;
	}
	public ERALISSystemException( String errorKey) {
	    this.errorKey = errorKey;
	}
}
