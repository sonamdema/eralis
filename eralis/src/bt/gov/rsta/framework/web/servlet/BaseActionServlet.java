package bt.gov.rsta.framework.web.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionServlet;

import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;

/**
 * Servlet implementation class BaseActionServlet
 */
public class BaseActionServlet extends ActionServlet {
	private static final long serialVersionUID = 1L;
       
	protected void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		//our custom code : START
		Log.info("BaseActionServlet :: START");
		HttpSession session = request.getSession();
		String uid = null;
		String password = null;
	     
		if(request.getUserPrincipal()!=null && request.getUserPrincipal().getName()!=null)
		{
		    uid = request.getUserPrincipal().getName();
		    Log.debug("****Trying to get uid = "+uid );
		}
		else if(request.getAttribute("UID")!=null && request.getAttribute("UNIQUE")!=null)
		{
			uid =(String) request.getAttribute("UID");
			password = (String) request.getAttribute("UNIQUE");
			Log.debug("UID :: "+uid);
		}
		
		try {
			
			if(uid != null){
				EralisUserRolePriviledge userRolePriviledge = new EralisUserRolePriviledge();
				
				if(userRolePriviledge == null || userRolePriviledge.getRoles() == null || userRolePriviledge.getRoles().length == 0){
					userRolePriviledge.setUserId(uid);
					userRolePriviledge.setPassword(password);
					
					userRolePriviledge = CommonBusiness.getInstance().populateUserRolePriviledgeHierarchy(userRolePriviledge);
					
					session.setAttribute(Constants.USER_DETAILS, userRolePriviledge);
					
					Role[] roles = userRolePriviledge.getRoles();
					Role currentRole = userRolePriviledge.getCurrentRole();
					
					if(roles != null && roles.length > 1){
						if(currentRole == null){
							request.setAttribute("roleList", roles);
							RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/pages/common/select_roles.jsp");
							requestDispatcher.forward(request, response);
							return;
						}
					}
				}
			}
			
		} catch (Exception e) {
			Log.error("Error in BaseActionServlet[Process] ---> ", e);
		}
		
		super.process(request, response);
	}
}
