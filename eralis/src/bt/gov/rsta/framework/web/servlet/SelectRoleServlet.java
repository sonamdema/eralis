package bt.gov.rsta.framework.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;

/**
 * Servlet implementation class SelectRoleServlet
 */
public class SelectRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectRoleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		List<Role> roles = new ArrayList<Role>();
		Role currentRole  = null;
		EralisUserRolePriviledge userRolePriv = null;
		try {
			HttpSession session = request.getSession();
		    
		    userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		    roles = Arrays.asList(userRolePriv.getRoles());
		    request.setAttribute("roleList", roles);		
		    currentRole = userRolePriv.getCurrentRole();
		    String selectedRoleCode = request.getParameter("roleCode");
		    if(currentRole == null && StringUtils.isEmpty(selectedRoleCode))
		    {
		    	RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/pages/common/select_roles.jsp");
				requestDispatcher.forward(request, response);
				return;
		    }
		    else
		    {
		    	Iterator<Role>iterator = roles.iterator();
		    	while(iterator.hasNext())
		    	{
		    		Role role = iterator.next();
		    		String roleCode = role.getRoleCode();
		    		if(roleCode.equalsIgnoreCase(selectedRoleCode))
		    		{
		    			userRolePriv.setCurrentRole(role);
		    			break;
		    		}
		    	}
		    	
		    	RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/login.html");
				requestDispatcher.forward(request, response);
				return;
		    }

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
