package bt.gov.rsta.framework.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;

/**
 * Servlet implementation class ContextLoaderServlet
 */
public class ContextLoaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContextLoaderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException 
    {
    	try 
    	{
			Class.forName("bt.gov.rsta.framework.util.StringToFormFileConverter");
		} 
    	catch (ClassNotFoundException e) 
    	{
			Log.error("Error to load StringToFormFileConverter");
		}
    	initAppContextObject();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}
	
	/**
	 * Do action.
	 * 
	 * @param req
	 *            the req
	 * @param resp
	 *            the resp
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected void doAction(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		initAppContextObject();
	}
	
	/**
	 * Inits the app context object.
	 */
	private void initAppContextObject() 
	{
		String parentId = null;
		try 
		{
			Log.info("### Entering ContextLoaderServlet[initAppContextObject]");
			
			List<DropDownDTO> securityQuestionList = CommonBusiness.getInstance().getDropDownList(Constants.SECURITY_QUESTION_DROP_DOWN_FIELD_CONSTRUCTOR, null);
			getServletContext().setAttribute("SECURITY_QUESTION_LIST", securityQuestionList);
			 
			Log.info("### Exiting ContextLoaderServlet[initAppContextObject]");
		}
		catch(Exception e)
		{
			
		}
	}
	
	

}
