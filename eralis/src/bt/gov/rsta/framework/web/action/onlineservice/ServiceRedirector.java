package bt.gov.rsta.framework.web.action.onlineservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;

public class ServiceRedirector extends Action
{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		
		try 
		{
			String q = request.getParameter("q");
			String form_type = request.getParameter("form_type");
			actionForward = q;
			request.setAttribute("form_type", form_type);
			if(q.equalsIgnoreCase("vehicle_registration") || q.equalsIgnoreCase("vehicle_renewal_service") 
					|| q.equalsIgnoreCase("issue_new_learner_license") || q.equalsIgnoreCase("issue_new_learner_license") 
					|| q.equalsIgnoreCase("issue_new_learner_license") || q.equalsIgnoreCase("renew_driving_license")
					|| q.equalsIgnoreCase("driving_license_duplication"))
			{
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);
				
				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
				request.setAttribute("engineTypeList", engineTypeList);
				
				List<DropDownDTO> dealersNameList = CommonBusiness.getInstance().getDropDownList("DEALERS_NAME_LIST", null);
				request.setAttribute("dealersNameList", dealersNameList);
				
				List<DropDownDTO> manufactureCountryList = CommonBusiness.getInstance().getDropDownList("MANUFACTURE_COUNTRY_LIST", null);
				request.setAttribute("manufactureCountryList", manufactureCountryList);
				
				List<DropDownDTO> ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				List<DropDownDTO> vehicleCancellationList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CANCELLATION_LIST", null);
				request.setAttribute("vehicleCancellationList", vehicleCancellationList);
			
				List<DropDownDTO> vehicleConversionList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CONVERSION_LIST", null);
				request.setAttribute("vehicleConversionList", vehicleConversionList); 
			
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> titleOfcourtesyList = CommonBusiness.getInstance().getDropDownList("TITLE_OF_COURTESY_LIST", null);
				request.setAttribute("titleOfcourtesyList", titleOfcourtesyList);
				
				List<DropDownDTO> occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				request.setAttribute("occupationList", occupationList);
				
				List<DropDownDTO> nationalityList = CommonBusiness.getInstance().getDropDownList("NATIONALITY_LIST", null);
				request.setAttribute("nationalityList", nationalityList);
				
				List<DropDownDTO> countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				request.setAttribute("countryList", countryList);
				
				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				List<DropDownDTO> bloodgroupList = CommonBusiness.getInstance().getDropDownList("BLOOD_GROUP_LIST", null);
				request.setAttribute("bloodgroupList", bloodgroupList);
				
				List<DropDownDTO> departmentList = CommonBusiness.getInstance().getDropDownList("DEPARTMENT_LIST", null);
				request.setAttribute("departmentList", departmentList);
				
				List<DropDownDTO> hypothecatedToList = CommonBusiness.getInstance().getDropDownList("HYPOTHECATED_TO_LIST", null);
				request.setAttribute("hypothecatedToList", hypothecatedToList);

				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
				request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
				
				request.setAttribute("DRIVE_TYPE_LIST_SIZE", driveTypeList.size());
			}
			else if(q.equalsIgnoreCase("payfine") || q.equalsIgnoreCase("vehicle_search_form"))
			{
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
			}
			else if(q.equalsIgnoreCase("passenger_bus_route_permit"))
			{

				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> passengerBusCategoryList = CommonBusiness.getInstance().getDropDownList("PASSANGER_BUS_CATEGORY_LIST", null);
				request.setAttribute("passengerBusCategoryList", passengerBusCategoryList);

			  	List<DropDownDTO> locationList = CommonBusiness.getInstance().getDropDownList("GET_ONLINE_LOCATION_LIST", null);
				request.setAttribute("locationList", locationList);
			}
			else if(q.equalsIgnoreCase("view_application_status"))
			{
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
			}
		} 
		catch (Exception e) 
		{
			Log.error("Error RedirectionAction[execute]: "+e);
			String owner = "CITIZEN";
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		//if(!actionForward.equalsIgnoreCase("search_learner_dtls"))
		//{
			//actionForward ="maintenance_message";
			//}
		return mapping.findForward(actionForward);
	}
}
