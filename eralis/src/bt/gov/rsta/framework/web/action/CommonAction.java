package bt.gov.rsta.framework.web.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.google.gson.Gson;

import antlr.CommonToken;
import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.common.PrintDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.faq.FaqDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusTrailDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.eralis.web.actionform.license.LicenseActionForm;
import bt.gov.rsta.eralis.web.actionform.vehicle.VehicleActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.AlertDTO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.PagePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.actionform.service.serviceActionForm;

public class CommonAction extends DispatchAction
{
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null,jurisId = null, jurisTypeId = null;
	
	public ActionForward switch_roles(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			Role[] roles = userRolePriv.getRoles();
			request.setAttribute("roleList", roles);
			
			actionForward = "switch_roles";
			
		} 
		catch (Exception e)
		{
			Log.error("Error CommonAction[switch_roles]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	


	public ActionForward getTokenDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String tokenId = request.getParameter("tokenId");

			EralisCommonDTO dto = (EralisCommonDTO) CommonBusiness.getInstance().getTokenDetails(tokenId);
			  
			request.setAttribute("COMMON_DTO", dto);
			actionForward = "token_details";
		}
		catch (Exception e) 
		{
			Log.error("Error commonaction[getTokenDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	 
	public ActionForward getTCBDriveTypeList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			response.setContentType("application/json;charset=utf-8");
			PrintWriter out = response.getWriter();
			
			String licenseId = request.getParameter("drivingLicenseId");
			
			String jsonText = "";
			Gson gson = new Gson();
			List<DropDownDTO> dropDownList = new ArrayList<DropDownDTO>();
			dropDownList = CommonBusiness.getInstance().getTCBDriveTypeList(licenseId);
			jsonText = gson.toJson(dropDownList);
			jsonText = new String(jsonText.getBytes("UTF-8"), "UTF-8");
			out.println(jsonText);
			out.flush();
		} 
		catch (Exception e)
		{
			Log.error("Error CommonAction[getTCBDriveTypeList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward get_faq_list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			List<FaqDTO> faqList = CommonBusiness.getInstance().get_faq_list();
			request.setAttribute("FAQ_LIST", faqList);
			
			actionForward = "faq";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[get_faq_list]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward updateFirstLoginDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String uid = request.getParameter("uid");
			String cpassword = request.getParameter("cpassword");
			String npassword = request.getParameter("newpassword");
			String securityQuestionId = request.getParameter("securityQuestionId");
			String securityAnswer = request.getParameter("answer");
			
			appVO.setUid(uid);
			appVO.setCpassword(cpassword);
			appVO.setNpassword(npassword);
			appVO.setSecurityQuestionId(securityQuestionId);
			appVO.setSecurityAnswer(securityAnswer);
			
			String result = CommonBusiness.getInstance().updateFirstLoginDtls(appVO);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error CommonAction[updateFirstLoginDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward resetPassword(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String loginId = request.getParameter("loginId");
			String questionId = request.getParameter("questionId");
			String answer = request.getParameter("answer");
			String result = CommonBusiness.getInstance().resetPassword(loginId, questionId, answer);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error CommonAction[resetPassword]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getPersonalInfoList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String cidNumber 	= request.getParameter("cid");
			String customerId 	= request.getParameter("customerId");
			String searchType = request.getParameter("searchType");
			
			appVO.setCidNumber(cidNumber);
			appVO.setCustomerId(customerId);
			appVO.setFormType(searchType);
			List<EralisCommonDTO> personalList = CommonBusiness.getInstance().getPersonalInfoList(appVO);
			request.setAttribute("PERSONAL_LIST", personalList);
			request.setAttribute("SEARCH_TYPE", searchType);
			
			actionForward = "personal_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getPersonalInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getPersonalInfoForEdit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
		
			String cidNumber 	= request.getParameter("cid");
			String customerId 	= request.getParameter("customerId");
			String searchType = request.getParameter("searchType");
			appVO.setCidNumber(cidNumber);
			appVO.setCustomerId(customerId);
			appVO.setFormType(searchType);
			List<EralisCommonDTO> personalList = CommonBusiness.getInstance().getPersonalInfoList(appVO);
			request.setAttribute("PERSONAL_LIST", personalList);
			request.setAttribute("SEARCH_TYPE", searchType);
			
			actionForward = "personal_details_edit";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getPersonalInfoListForEdit]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward getPendingList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
		
			String customerId 	= request.getParameter("customerId");
			appVO.setCustomerId(customerId);
			List<EralisCommonDTO> pendingList = CommonBusiness.getInstance().getPendingList(appVO);
			request.setAttribute("PENDING_LIST", pendingList);
			
			actionForward = "pending_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getPersonalInfoListForEdit]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward clearPendingTask(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		ApplicationDataVO appVO = new ApplicationDataVO();
		try 
		{
			String pendingId 	= request.getParameter("pendingId");
			String customerId 	= request.getParameter("customerID");
			appVO.setCustomerId(customerId);
			
			String result = CommonBusiness.getInstance().clearPendingTask(pendingId);
			List<EralisCommonDTO> pendingList = CommonBusiness.getInstance().getPendingList(appVO);
			request.setAttribute("PENDING_LIST", pendingList);
			
			actionForward = "pending_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getPersonalInfoListForEdit]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getPersonalInfoList1(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{

		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String name 		= request.getParameter("name");
			String cidNumber 	= request.getParameter("cid");
			String customerId 	= request.getParameter("customerId");
			String regionId 	= request.getParameter("region");
			String dzongkhagId 	= request.getParameter("dzongkhag");
			
			appVO.setName(name);
			appVO.setCidNumber(cidNumber);
			appVO.setCustomerId(customerId);
			appVO.setRegion(regionId);
			appVO.setDzongkhag(dzongkhagId );
			appVO.setFormType("transfer");
			
			List<EralisCommonDTO> personalList1 = CommonBusiness.getInstance().getPersonalInfoList1(appVO);
			request.setAttribute("PERSONAL_LIST1", personalList1);
			
			actionForward = "personal_details1";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getPersonalInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward getOrganisationInfoList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
	
		try 
		{
			String code 		= request.getParameter("code");
			String name 		= request.getParameter("name");
			String types 		= request.getParameter("type");
			String regionId		= request.getParameter("regionId");
			String dzongkhagId 	= request.getParameter("dzongkhagId");
			 
			appVO.setCustomerId(code);
			appVO.setName(name);
			appVO.setType(types);
			appVO.setRegion(regionId);
			appVO.setDzongkhag(dzongkhagId);
			
			List<EralisCommonDTO> organisationList = CommonBusiness.getInstance().getOrganisationInfoList(appVO);
			request.setAttribute("ORGANISATION_LIST", organisationList);
			
			actionForward = "organisation_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getOrganisationInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getOrganisationInfoForEdit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
	
		try 
		{
			String code 		= request.getParameter("code");
			String name 		= request.getParameter("name");
			/*String types 		= request.getParameter("type");
			String regionId		= request.getParameter("regionId");
			String dzongkhagId 	= request.getParameter("dzongkhagId");*/
			 
			appVO.setCustomerId(code);
			appVO.setName(name);
			/*appVO.setType(types);
			appVO.setRegion(regionId);
			appVO.setDzongkhag(dzongkhagId);*/
			
			List<EralisCommonDTO> organisationList = CommonBusiness.getInstance().getOrganisationInfoList(appVO);
			request.setAttribute("ORGANISATION_LIST", organisationList);
			
			actionForward = "organisation_details_edit";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getOrganisationInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
    
	public ActionForward getOrganisationInfoList1(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
	
		try 
		{
			String code 		= request.getParameter("code");
			String name 		= request.getParameter("name");
			String types 		= request.getParameter("type");
			appVO.setCustomerId(code);
			appVO.setName(name);
			appVO.setType(types);
			appVO.setFormType("transfer");
			List<EralisCommonDTO> organisationList1 = CommonBusiness.getInstance().getOrganisationInfoList(appVO);
			request.setAttribute("ORGANISATION_LIST1", organisationList1);
			
			actionForward = "organisation_details1";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getOrganisationInfoList1]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	
	public ActionForward getRenewalInfoList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String vehicleNumber 	= request.getParameter("vehicleNumber");
			String engineNumber 	= request.getParameter("engineNumber");
			String chasisNumber    = request.getParameter("chasisNumber");
			String cidNumber = request.getParameter("cidNumber");
			String searchType	=	request.getParameter("searchType");
			String type	=	request.getParameter("type");
			String organizationName	=	request.getParameter("organizationName");
			if(organizationName==null)
			{
				organizationName="NA";
			}
			appVO.setVehicleNumber(vehicleNumber);
			appVO.setEngineNumber(engineNumber);
			appVO.setChasisNumber(chasisNumber);
			appVO.setCidNumber(cidNumber);
			appVO.setOrganizationName(organizationName);
			
			
			//BUS_INSPECTION
			
			
			
			List<EralisCommonDTO> renewalList = CommonBusiness.getInstance().getRenewalInfoList(appVO);
			request.setAttribute("RENEWAL_LIST", renewalList);

			request.setAttribute("SEARCH_TYPE", searchType);
			request.setAttribute("TYPE", type);
			
			actionForward = "renewal_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRenewalInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward getAccidentRecord(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String vehicleNumber 	= request.getParameter("vehicleNumber");
			appVO.setVehicleNumber(vehicleNumber);
			
			List<EralisCommonDTO> accidentRecord = CommonBusiness.getInstance().getAccidentRecord(appVO);
			request.setAttribute("ACCIDENT_RECORD", accidentRecord);
			
			actionForward = "accident_record";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getAccidentRecord]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}


	public ActionForward getRoutePermitDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			appVO.setVehicleNumber(request.getParameter("vehicleNo"));
			List<EralisCommonDTO> routepermitDtls = CommonBusiness.getInstance().getRoutePermitDtls(appVO);
			request.setAttribute("ROUTE_PERMIT_DTLS", routepermitDtls);
			actionForward = "route_permit_dtls";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRoutePermitDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}

	public ActionForward searchAccidentVehicle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String vehicleNumber 	= request.getParameter("vehicleNumber");
			String engineNumber 	= request.getParameter("engineNumber");
			String chasisNumber    = request.getParameter("chasisNumber");
			String cidNumber = request.getParameter("cidNumber");
			String searchType	=	request.getParameter("searchType");
			String type	=	request.getParameter("type");
			String organizationName	=	request.getParameter("organizationName");
			if(organizationName==null)
			{
				organizationName="NA";
			}
			appVO.setVehicleNumber(vehicleNumber);
			appVO.setEngineNumber(engineNumber);
			appVO.setChasisNumber(chasisNumber);
			appVO.setCidNumber(cidNumber);
			appVO.setOrganizationName(organizationName);
			
			
			List<EralisCommonDTO> accidentVehicleList = CommonBusiness.getInstance().searchAccidentVehicle(appVO);
			request.setAttribute("ACCIDENT_VEHICLE_LIST", accidentVehicleList);

			actionForward = "accident_vehicle_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRenewalInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward searchVehicle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String vehicleNumber 	= request.getParameter("vehicleNumber");
			String engineNumber 	= request.getParameter("engineNumber");
			String chasisNumber    = request.getParameter("chasisNumber");
			String cidNumber = request.getParameter("cidNumber");
			String searchType	=	request.getParameter("searchType");
			String type	=	request.getParameter("type");
			String organizationName	=	request.getParameter("organizationName");
			if(organizationName==null)
			{
				organizationName="NA";
			}
			appVO.setVehicleNumber(vehicleNumber);
			appVO.setEngineNumber(engineNumber);
			appVO.setChasisNumber(chasisNumber);
			appVO.setCidNumber(cidNumber);
			appVO.setOrganizationName(organizationName);
			
			
			List<EralisCommonDTO> renewalList = CommonBusiness.getInstance().getRenewalInfoList(appVO);
			request.setAttribute("RENEWAL_LIST", renewalList);

			request.setAttribute("SEARCH_TYPE", searchType);
			request.setAttribute("TYPE", type);
			
			actionForward = "search_vehicle_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRenewalInfoDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	 
	public ActionForward getRenewalHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId 	= request.getParameter("vehicleId");
			
			appVO.setVehicleId(vehicleId);
			List<EralisCommonDTO> renewalHistory = CommonBusiness.getInstance().getRenewalHistoryList(appVO);
			request.setAttribute("RENEWAL_HISTORY", renewalHistory);
			
			actionForward = "renewal_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRenewalHistoryDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward fetchVehCancellationHistroy(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String vehicleId 	= request.getParameter("vehicleId");
			
			appVO.setVehicleId(vehicleId);
			List<EralisCommonDTO> cancellationHistory = CommonBusiness.getInstance().fetchVehCancellationHistroy(appVO);
			request.setAttribute("VEHICLE_CANCELLATION_HISTORY", cancellationHistory);
			
			actionForward = "cancellationHistory";
		}
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRenewalHistoryDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	//fetchTopDetailsHistory
	
	public ActionForward fetchTopDetailsHistory(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String vehicleId 	= request.getParameter("vehicleId");
			
			appVO.setVehicleId(vehicleId);
			List<EralisCommonDTO> topDetailsHistory = CommonBusiness.getInstance().fetchTopDetailsHistory(appVO);
			request.setAttribute("TOP_DETAILS_HISTORY", topDetailsHistory);
			
			actionForward = "topDetailsHistory";
		}
		catch (Exception e) 
		{
			Log.error("Error CommonAction[fetchTopDetailsHistory]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	//fetchTopCancellationHistory
	
	public ActionForward fetchTopCancellationHistory(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String vehicleId 	= request.getParameter("vehicleId");
			
			appVO.setVehicleId(vehicleId);
			List<EralisCommonDTO> topCancellationHistory = CommonBusiness.getInstance().fetchTopCancellationHistory(appVO);
			request.setAttribute("TOP_CANCELLATION_HISTORY", topCancellationHistory);
			
			actionForward = "topCancellationHistory";
		}
		catch (Exception e) 
		{
			Log.error("Error CommonAction[fetchTopCancellationHistory]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getVehicleRenewalHistory(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleNumber = request.getParameter("vehicleNo");
			
			appVO.setVehicleNumber(vehicleNumber);
			
			
			List<EralisCommonDTO> renewalHistory = CommonBusiness.getInstance().getRenewalHistoryList(appVO);
			request.setAttribute("RENEWAL_HISTORY", renewalHistory);
			
			actionForward = "vehicle_renewal_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getVehicleRenewalHistory]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward getDuplicationHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId 	= request.getParameter("vehicleId");
			
			appVO.setVehicleId(vehicleId);
			
			
			List<EralisCommonDTO> duplicationHistory = CommonBusiness.getInstance().getDuplicationHistoryList(appVO);
			request.setAttribute("DUPLICATION_HISTORY", duplicationHistory);
			
			actionForward = "duplication_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getDuplicationHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getTransferHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId 	= request.getParameter("vehicleId");
			
			appVO.setVehicleId(vehicleId);
			
			
			List<EralisCommonDTO> transferHistory = CommonBusiness.getInstance().getTransferHistoryList(appVO);
			request.setAttribute("TRANSFER_HISTORY", transferHistory);
			
			actionForward = "transfer_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getTransferHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getConversionHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId = request.getParameter("vehicleId");
			appVO.setVehicleId(vehicleId);
			
			List<EralisCommonDTO> conversionHistory = CommonBusiness.getInstance().getConversionHistoryList(appVO);
			request.setAttribute("CONVERSION_HISTORY", conversionHistory);
			
			actionForward = "conversion_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getConversionHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getFitnessHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId 	= request.getParameter("vehicleId");
			appVO.setVehicleId(vehicleId);
			
			List<EralisCommonDTO> fitnessHistory = CommonBusiness.getInstance().getFitnessHistoryList(appVO);
			request.setAttribute("FITNESS_HISTORY", fitnessHistory);
			
			actionForward = "fitness_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getFitnessHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getEmissionHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId 	= request.getParameter("vehicleId");
			appVO.setVehicleId(vehicleId);
			
			List<EralisCommonDTO> emissionHistory = CommonBusiness.getInstance().getEmissionHistoryList(appVO);
			request.setAttribute("EMISSION_HISTORY", emissionHistory);
			
			actionForward = "emission_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getEmissionHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	
	public ActionForward getVehicleRegSupportingDoc(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String applicationNo 	= request.getParameter("applicationNo");

			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,"RC");
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			actionForward = "uploadedFiles";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getVehicleRegSupportingDoc]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getOffenceHistoryList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String id 	= request.getParameter("id");
			String type = request.getParameter("type");
			
			List<EralisCommonDTO> offenceHistory = CommonBusiness.getInstance().getOffenceHistoryList(id, type);
			request.setAttribute("OFFENCE_HISTORY", offenceHistory);
			
			String serviceName = "OFFENCE";
			
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(id,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			actionForward = "offence_history";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getOffenceHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward getOffencePicture(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String id 	= request.getParameter("id");
			String serviceName = "OFFENCE";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(id,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			actionForward = "uploadedFiles";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getOffenceHistoryList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getStatusList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleNumber 	= request.getParameter("vehicleNumber");
			
			appVO.setVehicleNumber(vehicleNumber);
			
			
			List<EralisCommonDTO> status = CommonBusiness.getInstance().getStatusList(appVO);
			request.setAttribute("STATUS", status);
			
			actionForward = "status";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getRenewalHistoryDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getPermitInfoList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String driverName		= request.getParameter("driverName");
			String permitNo 	= request.getParameter("permitNo");
			String regionId		= request.getParameter("region");
			
			
			appVO.setDrivername(driverName);
			appVO.setPermitno(permitNo);
			appVO.setRegion(regionId);
			
			
			
			List<EralisCommonDTO> permitList = CommonBusiness.getInstance().getPermitInfoList(appVO);
			request.setAttribute("PERMIT_LIST", permitList);
			
			actionForward = "permit_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getPermitInfoList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getLearnerLicenseInfoList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		LicenseDTO dto=new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String cidNumber = request.getParameter("cid");
			String learnerno = request.getParameter("learnerno");
			String searchType = request.getParameter("searchType");
			
			if(searchType==null)
			{
				searchType= "NA";
			}
			dto.setFormType(searchType);
			dto.setCID(cidNumber);
			dto.setLearnerlicenseno(learnerno);
			
			List<LicenseDTO> licenseList = CommonBusiness.getInstance().getLearnerLicenseInfoList(dto);
			request.setAttribute("RENEWAL_LEARNER_LIST", licenseList);
			request.setAttribute("SEARCH_TYPE", searchType);
			
			actionForward = "learnerRenewal_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getLearnerLicenseInfoList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getlicenserenewalInfoList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String name		= request.getParameter("name");
			String cidNumber 	= request.getParameter("cidNumber");
			String licenseno		= request.getParameter("licenseno");
			String regionId=request.getParameter("region");
			String licensetype=request.getParameter("licensetype");
			
			appVO.setName(name);
			appVO.setCidNumber(cidNumber);
			appVO.setLicenseno(licenseno);
			appVO.setRegion(regionId);
			appVO.setLicensetype(licensetype);
			
			List<EralisCommonDTO> licenseRenewalList = CommonBusiness.getInstance().getlicenserenewalList(appVO);
			request.setAttribute("RENEWAL_LICENSE_LIST", licenseRenewalList);
			
			actionForward = "licenseRenewalDetails";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenserenewalInfoList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	//TOOLS REPLACE CID
	public ActionForward replaceCID(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String name		= request.getParameter("name");
			String cidNumber 	= request.getParameter("cidNumber");
			String licenseno		= request.getParameter("licenseno");
			String regionId = request.getParameter("region");
			String licensetype = request.getParameter("licensetype");
			
			appVO.setName(name);
			appVO.setCidNumber(cidNumber);
			appVO.setLicenseno(licenseno);
			appVO.setRegion(regionId);
			appVO.setLicensetype(licensetype);
			
			List<EralisCommonDTO> licenseRenewalList = CommonBusiness.getInstance().getlicenserenewalList(appVO);
			request.setAttribute("RENEWAL_LICENSE_LIST", licenseRenewalList);
			
			actionForward = "licenseRenewalDetails";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[replaceCID]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward getlearnerrenewallist(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String learnerLicenseId = request.getParameter("learnerLicenseId");
			
			appVO.setLearnerLicenseId(learnerLicenseId);
			
			List<LicenseDTO> learnerLicenseRenewalList = CommonBusiness.getInstance().getlearnerlicense(appVO);
			request.setAttribute("RENEWAL_LEARNER_LICENSE_LIST", learnerLicenseRenewalList);
			actionForward = "learnerlicenserenewalDetails";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlearnerrenewallist]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward getlearnerduplicationlist(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String learnerLicenseId = request.getParameter("learnerLicenseId");
			appVO.setLearnerLicenseId(learnerLicenseId);
			
			List<LicenseDTO> learnerDuplicationList = CommonBusiness.getInstance().getlearnerduplicationlist(appVO);
			request.setAttribute("DUPLICATE_LEARNER_LIST", learnerDuplicationList);
			
			actionForward = "learnerlicenseduplicationList";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlearnerduplicationlist]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getlicenserenewal(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String drivingLicenseId = request.getParameter("drivingLicenseId");
			appVO.setDrivingLicenseId(drivingLicenseId);
			
			List<LicenseDTO> licenseRenewalList = CommonBusiness.getInstance().getdrivinglicense(appVO);
			request.setAttribute("RENEWAL_LICENSE_LIST", licenseRenewalList);
			
			actionForward = "drivinglicenseRenewalList";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenserenewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getListOfTop(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String customerId = request.getParameter("customerId");
			
			List<LicenseDTO> topList = CommonBusiness.getInstance().getListOfTop(customerId);
			request.setAttribute("TOP_LIST", topList);
			
			actionForward = "top_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenserenewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getTopReplacementHistory(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String searchBy = request.getParameter("searchBy");
			String indentifer = request.getParameter("indentifer");
			
			List<LicenseDTO> topList = CommonBusiness.getInstance().getTopReplacementHistory(searchBy,indentifer);
			request.setAttribute("TOP_LIST", topList);
			
			actionForward = "top_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenserenewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getlicenseduplication(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			
			String drivinglicenseId = request.getParameter("drivinglicenseId");
			
			appVO.setDrivingLicenseId(drivinglicenseId);
			
			List<LicenseDTO> licenseDuplicationList = CommonBusiness.getInstance().getdrivinglicenseduplication(appVO);
			request.setAttribute("DUPLICATE_LICENSE_LIST", licenseDuplicationList);
			
			actionForward = "drivinglicenseduplicationList";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenseduplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getlicenseendorsementlists(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String drivinglicenseId = request.getParameter("drivinglicenseId");
			
			appVO.setDrivingLicenseId(drivinglicenseId);
			
			List<LicenseDTO> licenseEndorsementList = CommonBusiness.getInstance().getlicenseEndorsementList(appVO);
			request.setAttribute("LICENSE_ENDORSEMENT_LIST", licenseEndorsementList);
			
			actionForward = "endorsementList";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenseendorsementlists]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	//License Suspension	
	public ActionForward getLicesneInfo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		LicenseDTO dto = new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String cidNumber 	= request.getParameter("cid");
			String licenseNo 	= request.getParameter("licenseNo");
			String searchType = request.getParameter("searchType");
			dto.setCID(cidNumber);
			dto.setLicenseNo(licenseNo);
			dto.setFormType(searchType);
		  	List<LicenseDTO> licenseList = CommonBusiness.getInstance().license_suspension_list(dto);
			request.setAttribute("LICENSE_LIST", licenseList);
			request.setAttribute("SEARCH_TYPE", searchType);
			actionForward = "license_suspension";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getLicesneInfo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward searchVehicleRoutePermitDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		LicenseDTO dto = new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String permitNo 	= request.getParameter("permitNo");
			String licenseNo 	= request.getParameter("licenseNo");
			String vehicleNo 	= request.getParameter("vehicleNo");
			String searchType = request.getParameter("searchType");
			dto.setPermitNo(permitNo);
			dto.setLicenseNo(licenseNo);
			dto.setVehicleNo(vehicleNo);
			dto.setFormType(searchType);
		  	List<LicenseDTO> licenseList = CommonBusiness.getInstance().searchVehicleRoutePermitDtls(dto);
			request.setAttribute("PERMIT_LIST", licenseList);
			request.setAttribute("SEARCH_TYPE", searchType);
			actionForward = "permit_vehicle_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getLicesneInfo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getLicesneFORInfo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		LicenseDTO dto = new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String permitNo 	= request.getParameter("permitNo");
			String licenseNo 	= request.getParameter("licenseNo");
			String vehicleNo 	= request.getParameter("vehicleNo");
			String searchType = request.getParameter("searchType");
			dto.setPermitNo(permitNo);
			dto.setLicenseNo(licenseNo);
			dto.setVehicleNo(vehicleNo);
			dto.setFormType(searchType);
		  	List<LicenseDTO> licenseList = CommonBusiness.getInstance().license_suspensionFOR_list(dto);
			request.setAttribute("PERMIT_LIST", licenseList);
			request.setAttribute("SEARCH_TYPE", searchType);
			actionForward = "permit_renewal";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getLicesneInfo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward getsuspensionlist(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String formType = request.getParameter("FORM_TYPE");
			String drivinglicenseId = request.getParameter("drivinglicenseId");
			appVO.setDrivingLicenseId(drivinglicenseId);
			appVO.setFormType(formType);
			List<LicenseDTO> licenseSuspensionList = CommonBusiness.getInstance().getsuspensionlist(appVO);
			request.setAttribute("LICENSE_SUSPENSION_LIST", licenseSuspensionList);
			
			actionForward = "licensesuspensionList";
		} 
		
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenseduplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward getsearchdrivetypesuspensionlist(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			
			String drivinglicenseId = request.getParameter("drivinglicenseId");
			
			appVO.setDrivingLicenseId(drivinglicenseId);
			
			List<LicenseDTO> drivetypeSuspensionList = CommonBusiness.getInstance().getdrivetypeSuspensionList(appVO);
			request.setAttribute("DRIVE_TYPE_SUSPENSION_LIST", drivetypeSuspensionList);
			
			actionForward = "drivetypesuspensionList";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenseduplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward getLicenseCancellationHistory(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			
			String drivinglicenseId = request.getParameter("drivinglicenseId");
			appVO.setDrivingLicenseId(drivinglicenseId);
			
			
			List<LicenseDTO> licenseCancellationList = CommonBusiness.getInstance().getLicenseCancellationHistory(appVO);

			request.setAttribute("LICENSE_CANCELLATION_LIST", licenseCancellationList);
			
			actionForward = "licenseCancellationList";			

		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlicenseduplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getLearnerLicesneInfo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		LicenseDTO dto = new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String cidNumber 	= request.getParameter("cid");
			String licenseNo 	= request.getParameter("learnerlicenseNo");
			String formType		= request.getParameter("formType");
			dto.setFormType(formType);
			dto.setCID(cidNumber);
			dto.setLLNo(licenseNo);

		  	List<LicenseDTO> learnerlicenseList = CommonBusiness.getInstance().learner_license_list(dto);
			request.setAttribute("LICENSE_LIST", learnerlicenseList);
		 
			request.setAttribute("SEARCH_TYPE", formType);
			
			actionForward = "learner_license_search";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getLearnerLicesneInfo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getdrivetype(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		PrintWriter out = response.getWriter();
		//String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String learnerLicenseId = request.getParameter("learnerLicenseId");
			
			String driveTypeList = CommonBusiness.getInstance().drivetype_list(learnerLicenseId);
			
		  	//List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().drivetype_list(learnerLicenseId);
			//request.setAttribute("DRIVETYPE_LIST", drivetypeList);
			//actionForward = "drivetypeList";
			out.println(driveTypeList);
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getdrivetype]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			//actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return null;
		//return mapping.findForward(actionForward);
	}
	public ActionForward getdrivinglicensedrivetype(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		PrintWriter out = response.getWriter();
		//String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			
			String customerId 	= request.getParameter("customerId");
			String driveTypeList = CommonBusiness.getInstance().drivetype_licenselist(customerId);
			out.println(driveTypeList);
		  	/*List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().drivetype_licenselist(customerId);
			request.setAttribute("DRIVETYPE_LIST", drivetypeList);
			actionForward = "drivetypeListLicense";*/
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getdrivinglicensedrivetype]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			//actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return null;
		//return mapping.findForward(actionForward);
	}
	public ActionForward getDriveTypeTestMarks(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{

			String driveTypeId 	= request.getParameter("driveTypeId");
			String licenseNO 	= request.getParameter("licenseNO");
			LicenseDTO drivetypeList = CommonBusiness.getInstance().getDriveTypeTestMarks(driveTypeId,licenseNO,null,null,"");
			
			actionForward = "drivetypeListLicense";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getdrivinglicensedrivetype]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward gettopDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			Role currentRole = userRolePriv.getCurrentRole();
			String roleId = currentRole.getRoleId();
			String page_id = request.getParameter("page_id");
			String pageIdentifier = request.getParameter("pageIdentifier");
			
			PagePriviledge pagePriv = CommonBusiness.getInstance().getPagePriviledge(roleId, page_id);
			request.setAttribute("priviledge", pagePriv);
			request.setAttribute("pageId",page_id);
			request.setAttribute("pageIdentifier", pageIdentifier);
			
			String CID 	= request.getParameter("cid");
			EralisCommonDTO eralisCommonDTO = CommonBusiness.getInstance().gettopDtls(CID);
			
			String regionId = userRolePriv.getRegionId();
			String regionName = userRolePriv.getRegionName();
			request.setAttribute("REGION_ID", regionId);
			request.setAttribute("REGION_NAME", regionName);
			
			ApplicationDataVO appVO = new ApplicationDataVO();
			appVO.setCidNumber(CID);
			
			List<EralisCommonDTO> vehicleList = CommonBusiness.getInstance().getvehicleList(appVO);
			request.setAttribute("VEHICLE_LIST", vehicleList);
			
			List<LicenseDTO> licenseRenewalList = CommonBusiness.getInstance().getdrivinglicenseRenewal(eralisCommonDTO.getDrivinglicenseId());
			request.setAttribute("RENEWAL_LICENSE_LIST", licenseRenewalList);
			
			List<LicenseDTO> licenseDuplicationList = CommonBusiness.getInstance().getdrivingduplication(appVO);
			request.setAttribute("DUPLICATE_LICENSE_LIST", licenseDuplicationList);
			
			
			request.setAttribute("EralisCommonDTO", eralisCommonDTO);
			actionForward = "top_information";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getTOPDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward dispatch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String applicationNo = request.getParameter("applicationNo");
			String requestType = request.getParameter("requestType");
			String serviceType = request.getParameter("serviceType");
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
		  	String result = CommonBusiness.getInstance().dispatch(applicationNo, requestType, serviceType, userVo);
		  	
		  	request.setAttribute("MESSAGE", result);
		  	actionForward = "message";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[dispatch]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getTOPSearchList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		LicenseDTO dto = new LicenseDTO();
		
		try 
		{
			String topNumber = request.getParameter("topNumber");
			String vehicleNumber = request.getParameter("vehicleNumber");
			String citizenId = request.getParameter("citizenId");
			String licenseNumber = request.getParameter("licenseNumber");
			String searchType = request.getParameter("searchType");
			
			dto.setTopNo(topNumber);
			dto.setVehicleNo(vehicleNumber);
			dto.setCID(citizenId);
			dto.setLicenseNo(licenseNumber);
			
			List<LicenseDTO> topList = CommonBusiness.getInstance().getTOPSearchList(dto,"TOP_DTLS");
			request.setAttribute("TOP_LIST", topList);
			request.setAttribute("SEARCH_TYPE", searchType);
			actionForward = "top_search";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getTOPSearchList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getTopList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		LicenseDTO dto = new LicenseDTO();
		
		try 
		{
			String topNumber = request.getParameter("topNumber");
			String vehicleNumber = request.getParameter("vehicleNumber");
			String citizenId = request.getParameter("citizenId");
			String licenseNumber = request.getParameter("licenseNumber");
			
			dto.setTopNo(topNumber);
			dto.setVehicleNo(vehicleNumber);
			dto.setCID(citizenId);
			dto.setLicenseNo(licenseNumber);
			
			List<LicenseDTO> topList = CommonBusiness.getInstance().getTOPSearchList(dto,"TOP_HISTORY");
			request.setAttribute("TOP_LIST", topList);
			request.setAttribute("SEARCH_TYPE", "TOP_HISTORY");
			
			actionForward = "top_search";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getTOPSearchList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward formResubmit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result ="";
		try 
		{
			String serviceCode = request.getParameter("serviceCode");
			String driveTypeIds = request.getParameter("driveTypeIds");
			serviceActionForm serviceActionForm = (serviceActionForm) form;
			ServiceDTO dto=new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceActionForm);
			//dto.setDrivetype(serviceActionForm.getDrivetype());
			dto.setDriveTypeList(driveTypeIds);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			result = CommonBusiness.getInstance().do_resubmit(serviceCode,dto, userVo);
			
			String[] resultArray = result.split("#");
			if(resultArray[0].equalsIgnoreCase("SUCCESS"))
			{
				request.setAttribute("MESSAGE", "RESUBMIT-COMPLETE");
			}
			else
			{
				request.setAttribute("MESSAGE", "RESUBMIT-INCOMPLETE");
			}
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error LicenseAction[new_issue]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward deleteApplication(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result ="";
		try 
		{
			String serviceCode = request.getParameter("serviceCode");
			serviceActionForm serviceActionForm = (serviceActionForm) form;
			ServiceDTO dto=new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceActionForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			result = CommonBusiness.getInstance().deleteApplication(serviceCode,dto, userVo);
			String[] resultArray = result.split("#");
			
			request.setAttribute("MESSAGE", resultArray[0]);
			actionForward = "message";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error LicenseAction[new_issue]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward reject_application(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			serviceActionForm serviceAF = (serviceActionForm) form;
			ServiceDTO dto=new ServiceDTO();
			String serviceCode = request.getParameter("serviceCode");
			dto.setServiceCode(serviceCode);
			BeanUtils.copyProperties(dto, serviceAF);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = CommonBusiness.getInstance().reject_application(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error commonAction[application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward getLearnerLicenseDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ApplicationDataVO appVO = new ApplicationDataVO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String learnerLicenseId = request.getParameter("learnerLicenseId");
			
			appVO.setLearnerLicenseId(learnerLicenseId);
			
			List<LicenseDTO> learnerLicenseDtls = CommonBusiness.getInstance().getLearnerLicenseDtls(appVO);
			request.setAttribute("LEARNER_LICENSE_DTLS", learnerLicenseDtls);
			
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(learnerLicenseDtls.get(0).getApplicationNo(),"LL");
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			actionForward = "learnerLicenseDtls";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getlearnerrenewallist]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward daily_task_redirect(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			actionForward = "daily_task";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction daily_task_redirect]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward searchCustomerOffence(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		LicenseDTO dto=new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String learnerId = request.getParameter("learnerId");
			String licenseId = request.getParameter("licenseId"); 
			String vehicleId = request.getParameter("vehicleId"); 
			String offenceType = request.getParameter("offenceType"); 
			
			
			dto.setLearnerLicenseId(learnerId);
			dto.setDrivinglicenseId(licenseId);
			dto.setVehicleId(vehicleId);

			List<LicenseDTO> offenceList = CommonBusiness.getInstance().searchCustomerOffence(dto,offenceType);
			request.setAttribute("OFFENCE_LIST", offenceList);
			
			actionForward = "search_offence_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[searchCustomerOffence]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward get_offence_list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		LicenseDTO dto=new LicenseDTO();
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			
			String offenceId 	= request.getParameter("offenceId"); 
			
			dto.setOffenceId(offenceId);

			List<LicenseDTO> offenceList = CommonBusiness.getInstance().get_offence_list(dto);
			request.setAttribute("OFFENCE_LIST", offenceList);
			
			actionForward = "offence_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[learnerlicenseNo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward daily_task(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String requestType = request.getParameter("requestType");
			String reportType = request.getParameter("reportType");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			
			String reportStr = CommonBusiness.getInstance().daily_task(startDate, endDate, requestType, reportType, userRolePriv);
			
			request.setAttribute("DAILY_TASK", reportStr);
			
			actionForward = "daily_task_content";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[daily_task]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward print_task(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			List<PrintDTO> printTaskList = CommonBusiness.getInstance().print_task(userRolePriv.getJurisdictionId(), 
																							userRolePriv.getJurisdictionTypeId());
			request.setAttribute("PRINT_LIST", printTaskList);
			actionForward = "print_task";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[print_task]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward savePendingPrint(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String printId	=	request.getParameter("printId");
			serviceActionForm serviceForm = (serviceActionForm) form;
			PrintDTO dto = new PrintDTO();
			dto.setDocumentId(printId);
			BeanUtils.copyProperties(dto, serviceForm);

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = CommonBusiness.getInstance().savePendingPrint(dto, userVo);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[license_Punch]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	public ActionForward getApplicationStatus(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String requestType = request.getParameter("requestType");
			String serviceType = request.getParameter("serviceType");
			String identityNo = request.getParameter("identityNo");
			
			List<ApplicationStatusTrailDTO> applStatusList = CommonBusiness.getInstance().getApplicationStatus(requestType, serviceType, identityNo);
			request.setAttribute("APPLICATION_STATUS_LIST", applStatusList);
			actionForward = "appl_status";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getApplicationStatus]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getAlertList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession(); 
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String identifier = request.getParameter("identifier");
			
			List<AlertDTO> alertList = CommonBusiness.getInstance().getAlertList(identifier);
			request.setAttribute("ALERT_LIST", alertList);
			request.setAttribute("IDENTIFIER", identifier);
			actionForward = "alert_page";
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[getLicenseAlertList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward publishReportQuery(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception  
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			BeanUtils.copyProperties(dto, serviceForm);

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			String result = CommonBusiness.getInstance().publishReportQuery(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
			
			
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[new_issue]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	/**
	 *  Set values of user related information
	 */
	private void setUserValues(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge)session.getAttribute(Constants.USER_DETAILS);
		this.userId = userRolePriv.getUserId();
		this.userName = userRolePriv.getUserName();
		Role currentRole = userRolePriv.getCurrentRole();
		this.roleId = currentRole.getRoleId();
		this.roleName = currentRole.getRoleName();
		this.roleCode = currentRole.getRoleCode();
	}
	
	/*
	 * Copy user values to data transfer object
	 */
	private void copyUserValues(UserDetailsVO userVo)
	{
		userVo.setActorId(userId);
		userVo.setActorName(userName);
		userVo.setAssignedGroupId(assignedGroupId);
		userVo.setAssignedUserId(userId);
		userVo.setRoleId(roleId);
		userVo.setRoleName(roleName);
		userVo.setAssignedPrivId(assignedPrivId);
		userVo.setRoleCode(roleCode);
		userVo.setJurisdictionId(jurisId);
		userVo.setJurisdictionTypeId(jurisTypeId);
	}
}