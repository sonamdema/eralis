package bt.gov.rsta.framework.web.action;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import bt.gov.rsta.eralis.business.eralis_common.EralisCommonBusiness;
import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.payment.PaymentBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dao.eralis_common.EralisCommonDAO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.online_service.ApplicationStatusDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.business.ServiceBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.actionform.service.serviceActionForm;

public class ServiceAction extends DispatchAction
{
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null;
	public ActionForward getPersonalInfo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String cid	=	request.getParameter("cid");
			String dob	=	request.getParameter("dob");
			
			request.setAttribute("CID", cid);
			
		  	ServiceDTO dto = ServiceBusiness.getInstance().getPersonalInfo(cid,dob);
		  	if(dto.getRowCount().equalsIgnoreCase("0")){
		  		
	 			request.setAttribute("parameters", "CID or DOB");
	 			request.setAttribute("messageType", "UNVALID");
	 			actionForward = "serviceMessage";
		  	}
		  	else
		  	{	request.setAttribute("serviceDTO", dto);

		  		List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
			  	List<DropDownDTO> countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				request.setAttribute("countryList", countryList);
			
				List<DropDownDTO> titleOfcourtesyList = CommonBusiness.getInstance().getDropDownList("TITLE_OF_COURTESY_LIST", null);
				request.setAttribute("titleOfcourtesyList", titleOfcourtesyList);
				
				List<DropDownDTO> occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				request.setAttribute("occupationList", occupationList);
				
				List<DropDownDTO> bloodgroup = CommonBusiness.getInstance().getDropDownList("BLOOD_GROUP_LIST", null);
				request.setAttribute("bloodgroupList", bloodgroup);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList); 
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> nationalityList = CommonBusiness.getInstance().getDropDownList("NATIONALITY_LIST", null);
				request.setAttribute("nationalityList", nationalityList);
				
				String pageType	=	request.getParameter("pageType");
				if(pageType.equals("registration")){
					actionForward = "personalRegistration";
				}
				else{
					actionForward = "personal_details";
				}
		  	}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[getPersonalInfo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward learnerLicenseDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String applicationNo	=	null;
			String learnerNo	=	request.getParameter("learnerNo");
			String cid	=	request.getParameter("cid");
			String testType = request.getParameter("testType");
			String issueType = request.getParameter("issueType");
			String form_type	=	request.getParameter("form_type");
			String issueBasedOn	=	request.getParameter("issueBasedOn");
			actionForward = form_type;
			String messageType=null;
			if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
			{
				if("Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
				{
					issueType = issueBasedOn;
					if("DL".equalsIgnoreCase(issueBasedOn))
					{
						testType = "ENDORSEMENT";	
					}
					else if("Learner_License".equalsIgnoreCase(issueBasedOn))
					{
						testType = "NEW";	
					}
					
				}
				  
				ServiceDTO dto = ServiceBusiness.getInstance().getLearnerLicense(learnerNo,cid, testType,issueType);
				if(dto.getStatus()!=null && dto.getStatus().equalsIgnoreCase("HAS_DRIVING_LICENSE"))
				{
					request.setAttribute("parameters", "You already have driving license.");  
		 			actionForward = "serviceMessage";
		 			request.setAttribute("messageType", "validation");
		 			return mapping.findForward(actionForward);
				}
				applicationNo	=	dto.getApplicationNumber();
				if(dto.getRowCount().equalsIgnoreCase("0"))
			  	{
		 			request.setAttribute("parameters", "Invalid selection, please select correct issue type and test type OR Learner No. or CID");
		 			actionForward = "serviceMessage";
		 			messageType="checkExist";
		 			request.setAttribute("messageType", messageType);
			  	}
			  	else
			  	{
			  		if("0".equalsIgnoreCase(dto.getStatus()))
			  		{
			  			request.setAttribute("parameters", dto.getReason());
			 			actionForward = "serviceMessage";
			 			messageType	=	"validation";
			  			request.setAttribute("messageType", messageType);
			  		}
			  		else
			  		{

						actionForward = "book_driving_test";
				  		EralisCommonDTO commonDTO = CommonBusiness.getInstance().getlearnerRenewalInfoDtls(dto.getPersonalInfoId(),testType);
				  		
				  		/*String latestExpiryDate = ServiceBusiness.getInstance().getLatestExpiryDate(commonDTO.getLearnerLicenseId(), "LEARNER");
				  		request.setAttribute("LATEST_EXPIRY_DATE", latestExpiryDate);*/
				  		
				  		if("HAS_OFFENCE".equalsIgnoreCase(commonDTO.getStatus()))
				  		{
				  			request.setAttribute("parameters", "Learner License Has Offence");
				 			actionForward = "serviceMessage";
				 			request.setAttribute("messageType", "validation");
				  		}
				  		else if("LICENSE_EXPIRED".equalsIgnoreCase(commonDTO.getStatus()))
				  		{
				  			request.setAttribute("parameters", "License Has Expired, please renew it and proceed again");
				 			actionForward = "serviceMessage";
				 			request.setAttribute("messageType", "validation");
				  		}
				  		else
				  		{
				  			request.setAttribute("customerId", commonDTO.getCustomerID());
				  			request.setAttribute("name", commonDTO.getName());
				  			request.setAttribute("bloodgroup", commonDTO.getBloodGroup());
				  			request.setAttribute("dob", commonDTO.getDOB());
				  			request.setAttribute("country", commonDTO.getCountry());
				  			request.setAttribute("village", commonDTO.getVillage());
				  			request.setAttribute("dzongkhag", commonDTO.getDzongkhag());
				  			request.setAttribute("gewog", commonDTO.getGewog());
				  			request.setAttribute("address", commonDTO.getAddress());
				  			request.setAttribute("learnerlicenseNo", commonDTO.getLLNo());
				  			request.setAttribute("region", commonDTO.getRegion());
				  			request.setAttribute("expirydate", commonDTO.getExpiryDate());
				  			request.setAttribute("issuedate", commonDTO.getDateOfissue());
				  			request.setAttribute("learnerLicenseId", dto.getLearnerLicenseId());
				  			
					  		if(form_type.equalsIgnoreCase("renew_learner_search"))
					  		{
					  			ApplicationDataVO appVO = new ApplicationDataVO();
					  			appVO.setLicenseno(learnerNo);
					  			
					  			//List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().drivetype_list(learnerNo);
								//request.setAttribute("DRIVETYPE_LIST", drivetypeList);
								
					  			List<LicenseDTO> learnerLicenseRenewalList = CommonBusiness.getInstance().getlearnerlicense(appVO);
								request.setAttribute("RENEWAL_LEARNER_LICENSE_LIST", learnerLicenseRenewalList);
							  
								actionForward = "renew_learner_dtls";
					  		}
					  		else if(form_type.equalsIgnoreCase("learner_license_duplicate"))
					  		{
					  			ApplicationDataVO appVO = new ApplicationDataVO();
					  			appVO.setLicenseno(learnerNo);
					  			
								List<LicenseDTO> learnerDuplicationList = CommonBusiness.getInstance().getlearnerduplicationlist(appVO);
								request.setAttribute("DUPLICATE_LEARNER_LIST", learnerDuplicationList);
								
					  			actionForward = form_type;
					  		}
					  		else if(form_type.equalsIgnoreCase("issuance_non_commercial_license"))
					  		{
					  			List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
								request.setAttribute("regionList", regionList);
								
					  			actionForward = form_type;
					  		}
					  		else
					  		{
					  			request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
					 			request.setAttribute("firstName", dto.getFirstName());
					 			request.setAttribute("middleName", dto.getMiddleName());
					 			request.setAttribute("lastName", dto.getLastName());
								request.setAttribute("cid", dto.getCid());
								request.setAttribute("gender", dto.getGender());
								request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
								request.setAttribute("presentEmail", dto.getPresentEmail());
								request.setAttribute("image", dto.getImage());
					  			request.setAttribute("rowCount", dto.getRowCount());
								request.setAttribute("learnerNo", learnerNo);
								request.setAttribute("cid", cid);
								request.setAttribute("dob", dto.getDob());
								
								request.setAttribute("Test_Date", dto.getTestDate());
								request.setAttribute("Test_Location_Id", dto.getTestLocation());
								request.setAttribute("Drive_Type_Id", dto.getDrivetype());
								request.setAttribute("TEST_TYPE", testType);
								request.setAttribute("CUSTOMER_ID", dto.getCustomerID());
								
								
								
					  		}
					  		List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
							request.setAttribute("regionList", regionList);
							
							List<DropDownDTO> locationList = CommonBusiness.getInstance().getDropDownList("GET_ONLINE_LOCATION_LIST", null);
							request.setAttribute("locationList", locationList);
				  			
				  		}
			  			
			  		}
			  	}
			}
			else
			{
				testType = "NEW";
				ServiceDTO dto = ServiceBusiness.getInstance().getOtherIssueTypeBookDtls(learnerNo,cid, testType,issueType);
				applicationNo	=	dto.getApplicationNumber();
				
				if(dto.getStatus().equalsIgnoreCase("VALID_CID"))
				{
					if(dto.getStatus()!=null && dto.getStatus().equalsIgnoreCase("DRIVING_LICENSE_ALREADY_EXISTED") )
				  	{
			 			request.setAttribute("parameters", "Sorry! Driving License is already issued in this CID No., Please book with your Driving License No.");
			 			actionForward = "serviceMessage";
			 			messageType="checkExist";
			 			request.setAttribute("messageType", "validation");
				  	}
					else
					{
						request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
			 			request.setAttribute("firstName", dto.getFirstName());
			 			request.setAttribute("middleName", dto.getMiddleName());
			 			request.setAttribute("lastName", dto.getLastName());
						request.setAttribute("cid", dto.getCid());
						request.setAttribute("gender", dto.getGender());
						request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
						request.setAttribute("presentEmail", dto.getPresentEmail());
						request.setAttribute("image", dto.getImage());
			  			request.setAttribute("rowCount", dto.getRowCount());
						request.setAttribute("learnerNo", learnerNo);
						request.setAttribute("cid", cid);
						request.setAttribute("dob", dto.getDob());
						
						request.setAttribute("Test_Date", dto.getTestDate());
						request.setAttribute("Test_Location_Id", dto.getTestLocation());
						request.setAttribute("Drive_Type_Id", dto.getDrivetype());
						request.setAttribute("TEST_TYPE", testType);
						request.setAttribute("CUSTOMER_ID", dto.getCustomerID());
					}
					
				}
				else
				{
					request.setAttribute("parameters", "Prior to booking, please visit the nearest RSTA office to update your <b>Personal Information</>");
		 			actionForward = "serviceMessage";
		 			messageType="validation";
		 			request.setAttribute("messageType", messageType);
				}
				
			}
			if("book_driving_test".equalsIgnoreCase(form_type))
			{
				request.setAttribute("test_applicationNo", applicationNo);
				List<ServiceDTO> testLocationList = CommonBusiness.getInstance().getTestLocation();
				request.setAttribute("baseofficeList", testLocationList);
				String bookedTestDtls = "0";
				if(applicationNo!=null)
				{
					bookedTestDtls = CommonBusiness.getInstance().bookedTestDtls(applicationNo);
				}
				request.setAttribute("BOOKED_TEST_DTLS", bookedTestDtls);
				
				if(testType.equals("NEW"))
				{
					List<DropDownDTO> driveType = CommonBusiness.getInstance().getDropDownList("GET_LEARNER_DRIVE_TYPE", learnerNo);
					request.setAttribute("DRIVE_TYPE", driveType);
				}
				else if(testType.equals("ENDORSEMENT"))
				{
					List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
					request.setAttribute("DRIVE_TYPE", driveTypeList);
				}
				else
				{
					actionForward = "book_driving_test";
				}
			}
		}
		catch (Exception e)
		{e.printStackTrace();
			Log.error("Error ServiceAction[learnerLicenseDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward getLicenseDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			//String licenseNo=	request.getParameter("licenseNo");
			String cid		=	request.getParameter("cid");
			String dlNo		=	request.getParameter("dlNo");
			String form_type=	request.getParameter("form_type");
	  		
			ServiceDTO dto = ServiceBusiness.getInstance().getPersonalInfoId(cid,dlNo);
		  	ServiceDTO applicationDto = ServiceBusiness.getInstance().checkApplicationSubmitted(dto.getDrivinglicenseId(),form_type);
		  	if(!applicationDto.getRowCount().equalsIgnoreCase("0"))
		  	{
		  		request.setAttribute("APPLICATION_NO", applicationDto.getApplicationNo());
		  		request.setAttribute("ONLINE_PAYMENT_TYPE", "ALREADY_APPLIED");
				String result = PaymentBusiness.getInstance().processServicePayment(null,applicationDto.getApplicationNo(),null);

				
				String message		  =	"SUCCESS";
				String applicationNo =	applicationDto.getApplicationNo();
				String bfsOrderNo	  =	result;
				
				if("SUCCESS".equalsIgnoreCase(message))
				{
					request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
					request.setAttribute("APPLICATION_NO", applicationNo);
					actionForward = "online_payment_receipt";
				}
				else if ("BFS_ORDER_NO_FAILURE".equalsIgnoreCase(message) || "FAILURE".equalsIgnoreCase(message))
				{
					request.setAttribute("messageType", "validation");
					request.setAttribute("parameters", "Sorry! Transaction could not complete due to technical failure. Try again or contact nearest RSTA");
		 			actionForward = "serviceMessage";
				}
				else if ("LICENSE_DUPLICATE_TRANSACTION".equalsIgnoreCase(message))
				{
					if(null==bfsOrderNo || "00".equalsIgnoreCase(bfsOrderNo))
					{
						request.setAttribute("messageType", "warning");
						request.setAttribute("parameters", "You have already applied and your application number is "+applicationNo+" but you failed to make payment. To make payment <a target='_blank' href='https://www.citizenservices.gov.bt/G2CPaymentAggregator/redirect.html?param=makepayment&applicationNo="+applicationNo+"'>[Click Here]</a>");
						actionForward = "serviceMessage";
					}
					else
					{
						request.setAttribute("messageType", "validation");
						request.setAttribute("parameters", "Sorry! Your application has already submitted and waiting for approval. Thank you");
						actionForward = "serviceMessage";
					}
				}
				
	 			return mapping.findForward(actionForward);
		  	}
		  	
		  	if(dto.getRowCount().equalsIgnoreCase("0")){

	 			request.setAttribute("parameters", "CID");
	 			request.setAttribute("messageType", "license");
	 			actionForward = "serviceMessage";
		  	}
		  	else
		  	{
		  		if(form_type.equalsIgnoreCase("issuance_commercial_license") || form_type.equalsIgnoreCase("renew_driving_license")
		  				|| form_type.equalsIgnoreCase("driving_license_duplication") || form_type.equalsIgnoreCase("driving_license_endorsement"))
		  		{
		  			String drivinglicenseId	=	dto.getDrivinglicenseId();
		  			String searchType	=	null;
		  			if(form_type.equalsIgnoreCase("renew_driving_license"))
		  			{
		  				searchType	=	"LICENSE_RENEWAL";
		  			}
		  			else if(form_type.equalsIgnoreCase("driving_license_duplication"))
		  			{
		  				searchType	=	"LICENSE_DUPLICATE";
		  			}
		  			
		  			EralisCommonDTO Edto = CommonBusiness.getInstance().getSuspensionDtls(drivinglicenseId,searchType);
		  			String latestExpiryDate = ServiceBusiness.getInstance().getLatestExpiryDate(drivinglicenseId, "LICENSE");
		  			request.setAttribute("LATEST_EXPIRY_DATE", latestExpiryDate);
		  			
		  			if("HAS_OFFENCE".equalsIgnoreCase(Edto.getLicenseStatus()) || "VEHICLE_OUTSTANDING".equalsIgnoreCase(Edto.getStatus())){

		  				if("HAS_OFFENCE".equalsIgnoreCase(Edto.getLicenseStatus()))
		  				{
		  					request.setAttribute("parameters", "License Has Offence");
		  				}	
		  				else
		  				{
		  					request.setAttribute("vehicleNo", Edto.getVehicleNo());
		  					request.setAttribute("vehicleType", Edto.getVehicleType());
		  					request.setAttribute("parameters", Edto.getStatus());
		  				}
		  				request.setAttribute("messageType", "validation");
			 			form_type = "serviceMessage";
				  	}
		  			else
		  			{
		  				BeanUtils.copyProperties(dto, Edto);
		  				request.setAttribute("serviceDTO", dto);
			  			
			  			List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
						request.setAttribute("regionList", regionList);

					  	List<DropDownDTO> locationList = CommonBusiness.getInstance().getDropDownList("GET_ONLINE_LOCATION_LIST", null);
						request.setAttribute("locationList", locationList);
						
						List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
						request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
						
						ApplicationDataVO appVO = new ApplicationDataVO();
						appVO.setName(dto.getName());
						appVO.setCidNumber(dto.getCid());
						appVO.setLicenseno(dto.getLicenseNo());
						appVO.setRegion(dto.getRegion());
						appVO.setLicensetype(dto.getLicensetype());
						
						List<EralisCommonDTO> licenseRenewalList = CommonBusiness.getInstance().getlicenserenewalList(appVO);
						request.setAttribute("RENEWAL_LICENSE_LIST", licenseRenewalList);
		  				
		  			}
		  			
					
					
					actionForward =	form_type;
		  		}
		  		else if(form_type.equalsIgnoreCase("license_information"))
		  		{
		  			String drivinglicenseId	=	dto.getDrivinglicenseId();
		  			LicenseDTO ldto = new LicenseDTO();
		  			String searchType="";
		  			EralisCommonDTO Edto = CommonBusiness.getInstance().getSuspensionDtls(drivinglicenseId,searchType);
		  			BeanUtils.copyProperties(dto, Edto);
		  			request.setAttribute("serviceDTO", dto);

					ApplicationDataVO appVO = new ApplicationDataVO();
					appVO.setName(dto.getName());
					appVO.setCidNumber(dto.getCid());
					appVO.setLicenseno(dto.getLicenseNo());
					appVO.setRegion(dto.getRegion());
					appVO.setLicensetype(dto.getLicensetype());
					
					
					List<LicenseDTO> licenseRenewalList = CommonBusiness.getInstance().getdrivinglicense(appVO);
					request.setAttribute("RENEWAL_LICENSE_LIST", licenseRenewalList);
					
					List<LicenseDTO> licenseDuplicationList = CommonBusiness.getInstance().getdrivinglicenseduplication(appVO);
					request.setAttribute("DUPLICATE_LICENSE_LIST", licenseDuplicationList);
					
					List<LicenseDTO> licenseEndorsementList = CommonBusiness.getInstance().getlicenseEndorsementList(appVO);
					request.setAttribute("LICENSE_ENDORSEMENT_LIST", licenseEndorsementList);
					
					ldto.setName("NA");
					ldto.setCID("NA");
					ldto.setLicenseNo("NA");
					ldto.setRegion("NA");
					ldto.setLicenseTypeId("NA");
					BeanUtils.copyProperties(ldto, dto);
					
					List<LicenseDTO> licenseList = CommonBusiness.getInstance().license_suspension_list(ldto);
					request.setAttribute("LICENSE_LIST", licenseList);
					
		  			actionForward =	form_type;
		  		}
		  		else
		  		{
					actionForward = "licenseDetails";
		  		}
		  	}
		}
		catch (Exception e)
		{e.printStackTrace();
			Log.error("Error ServiceAction[getLicenseDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward getVehilceDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			ApplicationDataVO appVO = new ApplicationDataVO();
			String searchFormType	=	request.getParameter("searchFormType");
			String form_type		=	request.getParameter("form_type");
			String vehicle_no		=	"NA";
			String cid	=	"NA";
			String name	=	"NA";
			String vehicleRegType	=	"";
			
			if(searchFormType.equalsIgnoreCase("personal"))
			{
				vehicle_no	=	request.getParameter("vehicle_no");
				cid	=	request.getParameter("cid");
				vehicleRegType="P";
			}
			else if(searchFormType.equalsIgnoreCase("organization"))
			{
				vehicle_no	=	request.getParameter("vehicle_no");
				name	=	request.getParameter("name");
				vehicleRegType="O";
			}
			appVO.setOwnerName(name);
			appVO.setCidNumber(cid);
			appVO.setOwnerType(vehicleRegType);
			appVO.setVehicleNumber(vehicle_no);
			appVO.setVehicleType("NA");
			appVO.setEngineNumber("NA");
			appVO.setChasisNumber("NA");
			
			
			List<EralisCommonDTO> renewalList = CommonBusiness.getInstance().getRenewalInfoList(appVO);
			String vehicleRegId = "";
			for (EralisCommonDTO eralisCommonDTO : renewalList) 
			{
				vehicleRegId = eralisCommonDTO.getRenewalInfoId();
			}
		  	if(!vehicleRegId.equalsIgnoreCase(""))
		  	{
		  		ServiceDTO dto = new ServiceDTO();
		  		EralisCommonDTO Edto = CommonBusiness.getInstance().getRenewalInfoDtls(vehicleRegId,"");
		  		BeanUtils.copyProperties(dto, Edto);
	  			request.setAttribute("serviceDTO", dto);
	  			
 
	  			appVO.setVehicleNumber(dto.getVehicleNo());
				List<EralisCommonDTO> renewalHistory = CommonBusiness.getInstance().getRenewalHistoryList(appVO);
				request.setAttribute("RENEWAL_HISTORY", renewalHistory);
				
				List<EralisCommonDTO> duplicationHistory = CommonBusiness.getInstance().getDuplicationHistoryList(appVO);
				request.setAttribute("DUPLICATION_HISTORY", duplicationHistory);
				
	  			actionForward = form_type;
		  	}
		  	else
		  	{
		  		request.setAttribute("parameters", "License No. or CID");
		  		actionForward = "serviceMessage";
		  	}
		}
		catch (Exception e)
		{e.printStackTrace();
			Log.error("Error ServiceAction[getLicenseDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward editPersonalInfo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			request.setAttribute("DOB", dto.getDOB());
			request.setAttribute("CID", dto.getCID());
			String result = ServiceBusiness.getInstance().editPersonalInfo(dto);
			actionForward = "change_personal_details";
			request.setAttribute("MESSAGE", result);
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[editPersonalInfo]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "change_personal_details";
			request.setAttribute("MESSAGE", "FAILURE");
		}
		 
		return mapping.findForward(actionForward);
	}
	public ActionForward vehicleDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String vehicleNo	=	request.getParameter("vehicleNo");
			String cid	=	request.getParameter("cid");
		  	ServiceDTO dto = ServiceBusiness.getInstance().vehicleDetails(vehicleNo,cid);
		  	if(dto.getRowCount().equalsIgnoreCase("0")){
		  		
	 			request.setAttribute("parameters", "Vehicle No. or CID");
	 			actionForward = "serviceMessage";
		  	}
		  	else
		  	{
	 			request.setAttribute("firstName", dto.getFirstName());
	 			request.setAttribute("middleName", dto.getMiddleName());
	 			request.setAttribute("lastName", dto.getLastName());
				request.setAttribute("Vehicle_Number", dto.getVehicleNo());
				request.setAttribute("Vehicle_Registration_Type", dto.getVehicleRegistrationType());
				request.setAttribute("vehicleRegion", dto.getRegionId());
				request.setAttribute("Vehicle_Company_Name", dto.getVehicleCompany());
				request.setAttribute("Registration_Date", dto.getRegistrationDate());
				request.setAttribute("Vehicle_Model_Name", dto.getVehicleModelId());
				request.setAttribute("Engine_Number", dto.getEngineNumber());
				request.setAttribute("Engine_CC",  dto.getEngineCC());
				request.setAttribute("Purchase_Type",  dto.getPurchaseType());
 
				
				actionForward = "vehicle_details";
		  	}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[vehicleDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	

	public ActionForward generateAcknowledgementReceipt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String applicationNo	=	request.getParameter("applicationNo");
		  	ServiceDTO dto = ServiceBusiness.getInstance().generateAcknowledgementReceipt(applicationNo);
		  	
				String paymentDate = dto.getReceiptDate();
				String txnId = dto.getReceiptNo();
				String txnAmount = dto.getAmount();
				String paymentStatus = "PAID";
				
				
				String result	=	ServiceBusiness.getInstance().paymentResponse(applicationNo, paymentDate, txnId, txnAmount, paymentStatus);
				String[] tempArray = result.split("#");
				String responseStr	= tempArray[0];
				String service 		= tempArray[1];
				String refrenceNo 	= tempArray[2];
				String moduleNo 	= tempArray[3];
				String vehicleType 	= tempArray[4];
				String customerName = tempArray[5];
				String validity 	= tempArray[6];
				String serviceType	= tempArray[7];
				String moduleNo1	= null;
				
				if(serviceType.equalsIgnoreCase("Offence"))
				{
					String[] moduleArray = moduleNo.split("~");
					moduleNo = moduleArray[0];
					moduleNo1= moduleArray[1];
				}
				
				paymentDate	= tempArray[8];
				String receiptNumber = tempArray[9];
				String location = tempArray[10];
				String testDate = tempArray[11];
				if(serviceType.equalsIgnoreCase("BOOKING"))
				{
					service = "Booking";
				}
				session = request.getSession();
				session.setAttribute("RESPONSE_STR", responseStr);
				session.setAttribute("Service", service);
				session.setAttribute("APPLICATION_NO", applicationNo);
				session.setAttribute("PAYMENT_DATE", paymentDate);
				session.setAttribute("RECEIPT_NO", txnId);
				session.setAttribute("TXN_AMOUNT", txnAmount);
				session.setAttribute("REFRENCE_NO", refrenceNo);
				session.setAttribute("MODULE_NO", moduleNo);
				session.setAttribute("VEHICLE_TYPE", vehicleType);
				session.setAttribute("CUSTOMER_NAME", customerName);
				session.setAttribute("VALIDITY", validity);
				session.setAttribute("SERVICE_TYPE", serviceType);
				session.setAttribute("MODULE_NO1", moduleNo1);
				session.setAttribute("RECEIPT_SEQUENCE", receiptNumber);
				session.setAttribute("LOCATION", location);
				session.setAttribute("TEST_DATE", testDate);
				
				RequestDispatcher rd = request.getRequestDispatcher("/pages/payment/acknowledgement-receipt.jsp");
				rd.forward(request, response);
				//actionForward = "generate_";
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[vehicleDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward organizationVehicleRenewaldtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
		 
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);	
		  	dto = ServiceBusiness.getInstance().organizationVehicleRenewaldtls(dto);
		  	if(dto.getRowCount().equalsIgnoreCase("0")){
		  		
	 			request.setAttribute("parameters", "Vehicle No. or CID");
	 			actionForward = "serviceMessage";
		  	}
		  	else
		  	{
		  		request.setAttribute("firstName", dto.getFirstName());
	 			request.setAttribute("middleName", dto.getMiddleName());
	 			request.setAttribute("lastName", dto.getLastName());
				request.setAttribute("dzongkhagId", dto.getPermanentDzongkhag());
				request.setAttribute("address", dto.getPermanentAddress());
				request.setAttribute("village", dto.getPermanentVillage());
				request.setAttribute("Expiry_Date", dto.getExpiryDate());
				request.setAttribute("Vehicle_Company_Name", dto.getVehicleCompany());
				request.setAttribute("Vehicle_Model_Id", dto.getVehicleModel());
				request.setAttribute("Engine_CC",  dto.getEngineCC());
				request.setAttribute("Renewal_Date",  dto.getRenewalDate());
				
				
				dto.setVehicleNumber( dto.getVehicleNo());
				
				List<EralisCommonDTO> renewalHistory = ServiceBusiness.getInstance().getRenewalHistoryList(dto);
				request.setAttribute("ORGANISATION_RENEWAL_HISTORY", renewalHistory);
				
				actionForward = "organisation_vehicle_renew";
		  	}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[organizationVehicleRenewaldtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward passengerBusPermit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	null;
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO sdto = new ServiceDTO();
			BeanUtils.copyProperties(sdto, serviceForm);
			result = ServiceBusiness.getInstance().passengerBusPermit(sdto);
			String[] dataArray = result.split("#");
			request.setAttribute("messageType", dataArray[0]);
			request.setAttribute("parameters", "<i class='fa fa-check'></i>Thank you, Your application has submitted successfully to RSTA," +
					" and your application no. is <b>"+ dataArray[1]+"</b>");
			
			actionForward = "serviceMessage";
			
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[organizationVehicleRenewaldtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward bookDrivingTest(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	null;
		try
		{
			String issueType	=	request.getParameter("issueType");
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO sdto = new ServiceDTO();
			BeanUtils.copyProperties(sdto, serviceForm);
			String learnerNo	=	sdto.getLearnerNo();
			String cid			=	null;
			String issueBasedOn = request.getParameter("issueBasedOn");
			String testType =  request.getParameter("testType");
			if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
			{
				if("Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
				{
					issueType = issueBasedOn;
					if("DL".equalsIgnoreCase(issueBasedOn))
					{
						testType = "ENDORSEMENT";	
					}
					else if("Learner_License".equalsIgnoreCase(issueBasedOn))
					{
						testType = "NEW";	
					}
				}
				sdto.setIssueType(issueType);
				sdto.setTestType(testType);
			}
			
			result = ServiceBusiness.getInstance().bookDrivingTest(sdto,issueType,cid,learnerNo);
			String[] temp = result.split("#");
			if(temp[0].equalsIgnoreCase("SUCCESS"))
			{
				if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "DL".equalsIgnoreCase(issueType))
				{
					ServiceDTO dto = ServiceBusiness.getInstance().getLearnerLicense(learnerNo,cid,testType,issueType);
		 			request.setAttribute("testMessage", "1");
		 			request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
		 			request.setAttribute("firstName", dto.getFirstName());
		 			request.setAttribute("middleName", dto.getMiddleName());
		 			request.setAttribute("lastName", dto.getLastName());
					request.setAttribute("cid", dto.getCid());
					request.setAttribute("gender", dto.getGender());
					request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
					request.setAttribute("presentEmail", dto.getPresentEmail());
					request.setAttribute("image", dto.getImage());
					request.setAttribute("rowCount", dto.getRowCount());
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("dob", dto.getDob());
					request.setAttribute("test_applicationNo", dto.getApplicationNumber());
					request.setAttribute("Test_Date", dto.getTestDate());
					request.setAttribute("Test_Location_Id", dto.getTestLocation());
					request.setAttribute("Drive_Type_Id", dto.getDrivetype());
					request.setAttribute("TEST_TYPE", sdto.getTestType());
					
				}
				else
				{
					request.setAttribute("testMessage", "1");
		 			request.setAttribute("titleCourtesy", "");
		 			request.setAttribute("firstName",  "");
		 			request.setAttribute("middleName",  "");
		 			request.setAttribute("lastName",  "");
					request.setAttribute("cid",  "");
					request.setAttribute("gender",  "");
					request.setAttribute("presentPhoneNo",  "");
					request.setAttribute("presentEmail",  "");
					request.setAttribute("image",  "");
					request.setAttribute("rowCount",  "");
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("dob",  "");
					request.setAttribute("test_applicationNo",  "");
					request.setAttribute("Test_Date",  "");
					request.setAttribute("Test_Location_Id",  "");
					request.setAttribute("Drive_Type_Id",  "");
					request.setAttribute("TEST_TYPE",  "");
				}
				
				List<ServiceDTO> testLocationList = CommonBusiness.getInstance().getTestLocation();
				request.setAttribute("baseofficeList", testLocationList);
				
				if(sdto.getTestType().equals("NEW"))
				{
					List<DropDownDTO> driveType = CommonBusiness.getInstance().getDropDownList("GET_LEARNER_DRIVE_TYPE", learnerNo);
					request.setAttribute("DRIVE_TYPE", driveType);
				}
				else if(sdto.getTestType().equals("ENDORSEMENT"))
				{
					List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
					request.setAttribute("DRIVE_TYPE", driveTypeList);
				}

				String bookedTestDtls = "0";
				request.setAttribute("BOOKED_TEST_DTLS", bookedTestDtls);
				
				request.setAttribute("BFS_ORDER_NO", temp[1]);
				request.setAttribute("APPLICATION_NO", "BOOKING");
				
				//actionForward = "book_driving_test"; 
				actionForward = "online_payment_receipt";
			}
			else if(temp[0].equalsIgnoreCase("NO_SEAT_AVAILABLE"))
			{
				actionForward = "serviceMessage"; 
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! All seats are booked. Either choose another test date or contact nearest RSTA office for help.");
				
			}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[organizationVehicleRenewaldtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward adminBookingEtest(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	null;
		try
		{
			String issueType	=	request.getParameter("issueType");
			String reciptNo	=	request.getParameter("reciptNo");
			String reciptDate	=	request.getParameter("reciptDate");
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO sdto = new ServiceDTO();
			BeanUtils.copyProperties(sdto, serviceForm);
			String learnerNo	=	sdto.getLearnerNo();
			String cid			=	null;
			String issueBasedOn = request.getParameter("issueBasedOn");
			String testType =  request.getParameter("testType");
			if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType)
				|| "Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
			{
				if("Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
				{
					issueType = issueBasedOn;
					if("DL".equalsIgnoreCase(issueBasedOn))
					{
						testType = "ENDORSEMENT";	
					}
					else if("Learner_License".equalsIgnoreCase(issueBasedOn))
					{
						testType = "NEW";	
					}
				}
				sdto.setIssueType(issueType);
				sdto.setTestType(testType);
			}
			
			result = ServiceBusiness.getInstance().adminBookingEtest(sdto,issueType,cid,learnerNo,userRolePriv.getUserId());
			String[] temp = result.split("#");
			if(temp[0].equalsIgnoreCase("SUCCESS"))
			{
				if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "DL".equalsIgnoreCase(issueType))
				{
					ServiceDTO dto = ServiceBusiness.getInstance().getLearnerLicense(learnerNo,cid,testType,issueType);
		 			request.setAttribute("testMessage", "1");
		 			request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
		 			request.setAttribute("firstName", dto.getFirstName());
		 			request.setAttribute("middleName", dto.getMiddleName());
		 			request.setAttribute("lastName", dto.getLastName());
					request.setAttribute("cid", dto.getCid());
					request.setAttribute("gender", dto.getGender());
					request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
					request.setAttribute("presentEmail", dto.getPresentEmail());
					request.setAttribute("image", dto.getImage());
					request.setAttribute("rowCount", dto.getRowCount());
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("dob", dto.getDob());
					request.setAttribute("test_applicationNo", dto.getApplicationNumber());
					request.setAttribute("Test_Date", dto.getTestDate());
					request.setAttribute("Test_Location_Id", dto.getTestLocation());
					request.setAttribute("Drive_Type_Id", dto.getDrivetype());
					request.setAttribute("TEST_TYPE", sdto.getTestType());
					
				}
				else
				{
					request.setAttribute("testMessage", "1");
		 			request.setAttribute("titleCourtesy", "");
		 			request.setAttribute("firstName",  "");
		 			request.setAttribute("middleName",  "");
		 			request.setAttribute("lastName",  "");
					request.setAttribute("cid",  "");
					request.setAttribute("gender",  "");
					request.setAttribute("presentPhoneNo",  "");
					request.setAttribute("presentEmail",  "");
					request.setAttribute("image",  "");
					request.setAttribute("rowCount",  "");
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("dob",  "");
					request.setAttribute("test_applicationNo",  "");
					request.setAttribute("Test_Date",  "");
					request.setAttribute("Test_Location_Id",  "");
					request.setAttribute("Drive_Type_Id",  "");
					request.setAttribute("TEST_TYPE",  "");
				}
				
				List<ServiceDTO> testLocationList = CommonBusiness.getInstance().getTestLocation();
				request.setAttribute("baseofficeList", testLocationList);
				
				if(sdto.getTestType().equals("NEW"))
				{
					List<DropDownDTO> driveType = CommonBusiness.getInstance().getDropDownList("GET_LEARNER_DRIVE_TYPE", learnerNo);
					request.setAttribute("DRIVE_TYPE", driveType);
				}
				else if(sdto.getTestType().equals("ENDORSEMENT"))
				{
					List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
					request.setAttribute("DRIVE_TYPE", driveTypeList);
				}
				if(temp[0].equalsIgnoreCase("SUCCESS"))
				{
					actionForward = "serviceMessage";
					request.setAttribute("messageType", "success");
					request.setAttribute("parameters", "eTest booked successfully. And Booking no is <b>"+temp[1]+"</b>");
				}
				else
				{
					actionForward = "serviceMessage";
					request.setAttribute("messageType", "validation");
					request.setAttribute("parameters", "Sorry! Some technical failure");
				}
			}
			 
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[organizationVehicleRenewaldtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward cancelDrivingTest(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	null;
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO sdto = new ServiceDTO();
			
			BeanUtils.copyProperties(sdto, serviceForm);	
			String testType	=	request.getParameter("testType");
			String applicationNo	=	request.getParameter("applicationNo");
			sdto.setApplicationNumber(applicationNo);
			String learnerNo	=	sdto.getLearnerNo();
			String cid			=	sdto.getCid();
			result = ServiceBusiness.getInstance().cancelDrivingTest(sdto);
			String issueType	=	null;
			if(result=="SUCCESS")
			{
				ServiceDTO dto = ServiceBusiness.getInstance().getLearnerLicense(learnerNo,cid,testType,issueType);
	 			request.setAttribute("testMessage", "0");
	 			request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
	 			request.setAttribute("firstName", dto.getFirstName());
	 			request.setAttribute("middleName", dto.getMiddleName());
	 			request.setAttribute("lastName", dto.getLastName());
				request.setAttribute("cid", dto.getCid());
				request.setAttribute("gender", dto.getGender());
				request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
				request.setAttribute("presentEmail", dto.getPresentEmail());
				request.setAttribute("image", dto.getImage());
				request.setAttribute("rowCount", dto.getRowCount());
				request.setAttribute("learnerNo", learnerNo);
				request.setAttribute("dob", dto.getDob());
				request.setAttribute("test_applicationNo", dto.getApplicationNumber());
				request.setAttribute("Test_Date", dto.getTestDate());
				request.setAttribute("Test_Location_Id", dto.getTestLocation());
				request.setAttribute("Drive_Type_Id", dto.getDrivetype());
				request.setAttribute("TEST_TYPE", sdto.getTestType());
				
				List<ServiceDTO> testLocationList = CommonBusiness.getInstance().getTestLocation();
				request.setAttribute("baseofficeList", testLocationList);
				
				if(testType.equals("NEW"))
				{
					List<DropDownDTO> driveType = CommonBusiness.getInstance().getDropDownList("GET_LEARNER_DRIVE_TYPE", learnerNo);
					request.setAttribute("DRIVE_TYPE", driveType);
				}
				else if(testType.equals("ENDORSEMENT"))
				{
					List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
					request.setAttribute("DRIVE_TYPE", driveTypeList);
				}
				
				actionForward = "book_driving_test"; 
			}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[organizationVehicleRenewaldtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getVehicleDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null; 
		String fitnessId = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{ 
			String formType	=	request.getParameter("formType");
			String vehicleNo	=	request.getParameter("vehicleNo"); 
			//String cid	=	request.getParameter("cid");
			String vehicleType	=	request.getParameter("vehicleType");
		  	ServiceDTO dto = ServiceBusiness.getInstance().getVehicleId(vehicleNo,vehicleType);
		  	/**IF VEHICLE ID IS NOT 0**/
		  	
		  /*	if(!formType.equalsIgnoreCase("vehicle_fitness"))
		  	{
			  	ServiceDTO applicationDto = ServiceBusiness.getInstance().checkApplicationSubmitted(dto.getVehicleRegDtlsId(),formType);
	
			  	if(!applicationDto.getRowCount().equalsIgnoreCase("0"))
			  	{
			  		request.setAttribute("APPLICATION_NO", applicationDto.getApplicationNo());
			  		request.setAttribute("ONLINE_PAYMENT_TYPE", "ALREADY_APPLIED"); 
					String result = PaymentBusiness.getInstance().processServicePayment(null,applicationDto.getApplicationNo(),null);
					
					if(result.equalsIgnoreCase("ALREADY_PAID") || result==null) 
					{
						request.setAttribute("messageType", "warning");
						request.setAttribute("parameters", "Payment for the same transaction was already done. Verify and contact RSTA");
			 			actionForward = "serviceMessage";
			 			return mapping.findForward(actionForward);
					}
					
		 			//return mapping.findForward(actionForward);
			  	} 
		  	}*/
		  	
		  	
		  	
		  	if(!dto.getRowCount().equalsIgnoreCase("0"))
		  	{
		  		EralisCommonDTO Edto = CommonBusiness.getInstance().getRenewalInfoDtls(dto.getVehicleRegDtlsId(),"");
		  		if(Edto.getStatus().equalsIgnoreCase("DRIVING_LICENSE_OUTSTANDING"))
		  		{
		  			request.setAttribute("messageType","validation");
		  			request.setAttribute("parameters","Your driving license has outstanding against the driving License no. : <b>"+Edto.getLicenseNo()+"</b> , please clear the outstanding.");
		  			return mapping.findForward("serviceMessage");
		  		}
		  		else if(Edto.getVehicleType().equalsIgnoreCase("3") && Edto.getLoadCapacity()!=null && (Double.parseDouble(Edto.getLoadCapacity())>=10000 || Double.parseDouble(Edto.getLoadCapacity())<3000 ))
		  		{
		  			request.setAttribute("messageType","validation");
		  			request.setAttribute("parameters","This vehicle loading capacity is incorrect, contact nearest RSTA.");
		  			return mapping.findForward("serviceMessage");
		  		}
		  		else if(Edto.getVehicleType().equalsIgnoreCase("3") && Edto.getLoadCapacity()==null)
		  		{
		  			request.setAttribute("messageType","validation");
		  			request.setAttribute("parameters","This vehicle loading capacity is incorrect, contact nearest RSTA.");
		  			return mapping.findForward("serviceMessage");
		  		}
		  		else if(Edto.getVehicleType().equalsIgnoreCase("2") && Edto.getLoadCapacity()!=null && Double.parseDouble(Edto.getLoadCapacity())<10000)
		  		{
		  			request.setAttribute("messageType","validation");
		  			request.setAttribute("parameters","This vehicle loading capacity is incorrect, contact nearest RSTA.");
		  			return mapping.findForward("serviceMessage");
		  		}
		  		else if(Edto.getVehicleType().equalsIgnoreCase("2") && Edto.getLoadCapacity()==null)
		  		{
		  			request.setAttribute("messageType","validation");
		  			request.setAttribute("parameters","This vehicle loading capacity is incorrect, contact nearest RSTA.");
		  			return mapping.findForward("serviceMessage");
		  		}
		  		request.setAttribute("LIFE_SPAN_EXPIRY_DATE", Edto.getLifeSpanExpiryDate());
		  		
		  		String latestExpiryDate = ServiceBusiness.getInstance().getLatestExpiryDate(dto.getVehicleRegDtlsId(), "VEHICLE");
		  		request.setAttribute("LATEST_EXPIRY_DATE", latestExpiryDate);
		  		
		  		fitnessId = CommonBusiness.getInstance().getFitnessId(dto.getVehicleRegDtlsId());
		  		
			  	List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);

			  	List<DropDownDTO> locationList = CommonBusiness.getInstance().getDropDownList("GET_ONLINE_LOCATION_LIST", null);
				request.setAttribute("locationList", locationList);
			  	
				if(dto.getRowCount().equalsIgnoreCase("0"))
			  	{
		 			request.setAttribute("parameters", "Vehicle No. or CID");
		 			actionForward = "serviceMessage";
			  	}
			  	else if(("HAS_OFFENCE".equalsIgnoreCase(Edto.getStatus()) || "VEHICLE_OUTSTANDING".equalsIgnoreCase(Edto.getStatus())) && null!=Edto.getStatus())
			  	{
			  		
			  		if("HAS_OFFENCE".equalsIgnoreCase(Edto.getLicenseStatus()))
	  				{
				  		request.setAttribute("parameters","Vehicle Has Offence");
	  				}	
	  				else
	  				{
	  					request.setAttribute("vehicleNo", Edto.getOutstandingVehicleNo());
	  					request.setAttribute("vehicleType", Edto.getOutstandingVehicleType());
	  					request.setAttribute("parameters", Edto.getStatus());
	  				}
			  		
			  		
			  		request.setAttribute("messageType","validation");
		 			actionForward = "serviceMessage";
			  	}
			  	else if("CANCELLED".equalsIgnoreCase(Edto.getStatus()) && null!=Edto.getStatus())
			  	{
			  		request.setAttribute("messageType","validation");
			  		request.setAttribute("parameters","This Vehicle is cancelled, please contact nearest RSTA.");
		 			actionForward = "serviceMessage";
			  	}
			  	else if("PENDING_APPLICATION".equalsIgnoreCase(Edto.getStatus()) && null!=Edto.getStatus())
			  	{
			  		request.setAttribute("messageType","validation");
			  		request.setAttribute("parameters","This Vehicle has pending application, please contact nearest RSTA.");
		 			actionForward = "serviceMessage";
			  	}
			  	else if(Edto.getPhone()== null || Edto.getPhone().equalsIgnoreCase("")|| 
			  			Edto.getAddress()== null || Edto.getAddress().equalsIgnoreCase(""))
				{
					request.setAttribute("messageType","validation");
			  		request.setAttribute("parameters","This vehicle doesnot have complete owner information, Please visit nearest RSTA Office");
		 			actionForward = "serviceMessage";
				}
				else if(Edto.getRegistrationDate()== null || Edto.getRegistrationDate().equalsIgnoreCase("")|| 
			  			Edto.getCompany()== null || Edto.getCompany().equalsIgnoreCase("")|| 
			  			Edto.getModel()== null || Edto.getModel().equalsIgnoreCase("")|| 
			  			Edto.getChasisNumber()== null || Edto.getChasisNumber().equalsIgnoreCase("")|| 
			  			Edto.getEngineType()== null || Edto.getEngineType().equalsIgnoreCase("")|| 
			  			Edto.getVehicleType()== null || Edto.getVehicleType().equalsIgnoreCase("")|| 
			  			Edto.getManufactureYear()== null || Edto.getManufactureYear().equalsIgnoreCase("")|| 
			  			Edto.getAmount()== null || Edto.getAmount().equalsIgnoreCase("")|| 
			  			Edto.getVehicleRegistrationType()== null || Edto.getVehicleRegistrationType().equalsIgnoreCase(""))
				{
					request.setAttribute("messageType","validation");
			  		request.setAttribute("parameters","This vehicle doesnot have complete vehicle information, Please visit nearest RSTA Office to update your information");
		 			actionForward = "serviceMessage";
				}
				else if(formType.equals("vehicle_fitness") && null == fitnessId){
	  				request.setAttribute("messageType","validation");
			  		request.setAttribute("parameters","This vehicle has not come for physical fitness verification by RSTA");
		 			actionForward = "serviceMessage";
	  			}
			  	else
			  	{
					dto.setVehicleNumber(vehicleNo);
					if(formType.equalsIgnoreCase("VEH_RENEWAL"))
					{
						actionForward = "vehicle_renewal_dtls";
					}
					else if(formType.equalsIgnoreCase("vehicle_duplication"))
					{
						actionForward = "veh_duplicate";
					}
					else if(formType.equalsIgnoreCase("vehicle_fitness"))
					{
						request.setAttribute("FITNESS_ID", fitnessId);
						actionForward = "vehicle_fitness";
					}
					
					request.setAttribute("EDTO", Edto);
			  	}
		  	}
		  	else
		  	{
		  		request.setAttribute("messageType", "FINE_INCORRECT_PARAM");
				request.setAttribute("parameters", "Vehicle Number or Owner CID");
	 			actionForward = "serviceMessage";
		  	}
		  	
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.error("Error ServiceAction[vehicleDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getFineInfo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String tinNo	=	request.getParameter("tinNo");
			String licenseNo	=	request.getParameter("licenseNo");
			
		  	ServiceDTO dto = ServiceBusiness.getInstance().getLicenseDetails(tinNo,licenseNo);
		  	if(dto.getRowCount().equalsIgnoreCase("0")){
		  		
	 			request.setAttribute("parameters", "License No. or CID");
	 			actionForward = "serviceMessage";
		  	}
		  	else
		  	{
	 			request.setAttribute("firstName", dto.getFirstName());
	 			request.setAttribute("middleName", dto.getMiddleName());
	 			request.setAttribute("lastName", dto.getLastName());
				request.setAttribute("cid", dto.getCid());
				request.setAttribute("gender", dto.getGender());
				request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
				request.setAttribute("presentEmail", dto.getPresentEmail());
				request.setAttribute("presentAddress", dto.getPresentContactAddress());
				request.setAttribute("image", dto.getImage());
				request.setAttribute("rowCount", dto.getRowCount());
				request.setAttribute("issueDate",  dto.getIssueDate());
				request.setAttribute("drivingLicenseType",  dto.getDrivingLicenseType());
				request.setAttribute("blood_group_type",  dto.getBloodGroup());
				
				actionForward = "licenseDetails";
		  	}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[getLicenseDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward issue_new_learner_license(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String result	=	null;
			String customerId =	null;
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			String driveTypeIds = request.getParameter("driveTypeIds");
			dto.setDriveTypeList(driveTypeIds);
			
			BeanUtils.copyProperties(dto, serviceForm);
			dto.setManualFlag(request.getParameter("manualFlag"));
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO(); 
			BeanUtils.copyProperties(ldto, dto);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			if(request.getParameter("newCustomer").equalsIgnoreCase("Y"))
			{
				result = EralisCommonBusiness.getInstance().personal_info(commonDto, userVo);
				String[] tempArray = result.split("#");
				customerId = tempArray[1];
				
			}else if(request.getParameter("newCustomer").equalsIgnoreCase("N"))
			{
				String cid	=	request.getParameter("cid");
				customerId = ServiceBusiness.getInstance().getCustomerId(cid);
			}
			
			ldto.setCustomerId(customerId);
			String result1 = LicenseBusiness.getInstance().new_issue(ldto, userVo);
			
			
			request.setAttribute("MESSAGE", result1);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error EralisCommonAction[personal_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward personal_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String result	=	null;
			String customerId =	null;
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			dto.setManualFlag(request.getParameter("manualFlag"));
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			
			VehicleDTO vdto = new VehicleDTO();
			BeanUtils.copyProperties(vdto, dto);
 			vdto.setRegion(request.getParameter("regionId"));
			
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			if(request.getParameter("newCustomer").equalsIgnoreCase("Y"))
			{
				result = EralisCommonBusiness.getInstance().personal_info(commonDto, userVo);
				String[] tempArray = result.split("#");
				customerId = tempArray[1];
				
			}else if(request.getParameter("newCustomer").equalsIgnoreCase("N"))
			{
				String cid	=	request.getParameter("cid");
				customerId = ServiceBusiness.getInstance().getCustomerId(cid);
			}
			vdto.setCustomerId(customerId);
			String result1 = VehicleBusiness.getInstance().new_registration(vdto, userVo);
			
			request.setAttribute("MESSAGE", result1);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error EralisCommonAction[personal_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward organisational_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String customerId =	null;
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			
			VehicleDTO vdto = new VehicleDTO();
			BeanUtils.copyProperties(vdto, dto);
			
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			commonDto.setName(dto.getOrganisationName());
			commonDto.setRegion(dto.getOrganisationRegion());
			commonDto.setDzongkhag(dto.getOrganisationDzongkhag());
			commonDto.setAddress(dto.getOrganisationAddress());
			commonDto.setPhone(dto.getOrganisationPhone());
			commonDto.setEmail(dto.getOrganisationEmail());
			commonDto.setRemarks(dto.getOrganisationRemarks());
			
			
			
			if(request.getParameter("newCustomer").equalsIgnoreCase("Y"))
			{
				String result = EralisCommonBusiness.getInstance().organization_info(commonDto,userVo);
				String[] tempArray = result.split("#");
				customerId = tempArray[1];
			}
			else if(request.getParameter("newCustomer").equalsIgnoreCase("N"))
			{
				customerId = request.getParameter("customerId");
			}
			vdto.setCustomerId(customerId);
			String result1 = VehicleBusiness.getInstance().new_registration(vdto, userVo);
			request.setAttribute("MESSAGE", result1);
			actionForward = "message";
		}
		catch (Exception e) 
		{ 
			Log.error("Error EralisCommonAction[organisational_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	//////
	public ActionForward renew_learner_license(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		 
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_renewal_learner(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward duplicate_learner_license(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		 
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_duplication_learner(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
			 
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward issue_non_commercial_driving_license(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		try 
		{
			 
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			
			 
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);

			String result = LicenseBusiness.getInstance().license_non_commerical(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
			
			request.setAttribute("MESSAGE", result);
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward issue_commercial_driving_license(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		try 
		{
			 
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			
			 
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);

			String result = LicenseBusiness.getInstance().license_commerical(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
			
			request.setAttribute("MESSAGE", result);
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward renew_driving_license(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		try 
		{
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			
			 
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_renewal(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
			
			request.setAttribute("MESSAGE", result);
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward driving_license_duplication(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		try 
		{
			 
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			
			 
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_renewal(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
			
			request.setAttribute("MESSAGE", result);
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward driving_license_endorsement(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		try 
		{
			 
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO dto = new ServiceDTO();
			
			BeanUtils.copyProperties(dto, serviceForm);
			
			EralisCommonDTO commonDto = new EralisCommonDTO();
			BeanUtils.copyProperties(commonDto, dto);
			
			LicenseDTO ldto=new LicenseDTO();
			BeanUtils.copyProperties(ldto, dto);
			
			 
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			String result = LicenseBusiness.getInstance().license_endorsement(ldto, userVo,"N");
			//String result = LicenseBusiness.getInstance().license_endorsement(ldto, userVo);
			//String result = LicenseBusiness.getInstance().license_renewal(ldto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
			
			request.setAttribute("MESSAGE", result);
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[renew_learner_license]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getApplicationStatus(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		try
		{
			 String serviceType = request.getParameter("serviceType");
			 String cid = request.getParameter("cid");
			 String LLNO = request.getParameter("LLNO");
			 String dlNo = request.getParameter("dlNo");
			 String VNo = request.getParameter("VNo");
			 String vehicleType = request.getParameter("vehicleType");
			 String applicationNo = request.getParameter("applicationNo");
			 String serviceCode = ServiceBusiness.getInstance().getServiceCode(applicationNo);
			 ApplicationStatusDTO appStatusDto = ServiceBusiness.getInstance().retrieveApplicationStatus(applicationNo,serviceType,cid,LLNO,dlNo,VNo,vehicleType);
			 if(appStatusDto.getLatestStatusCode().equalsIgnoreCase("invalid"))
			 {
				 appStatusDto.setLatestStatus("invalid");
			 }
			 
			 appStatusDto.setApplicationNo(applicationNo);
			 appStatusDto.setServiceCode(serviceCode);				 
			 
			 request.setAttribute("status_dto", appStatusDto);
			 
			 actionForward = "application_status";
		}
		catch (Exception e) 
		{ 
			Log.error("Error ServiceAction[getApplicationStatus]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward offenceSearchedByCitizen(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		ServiceDTO dto=new ServiceDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String vehicle_no = request.getParameter("vehicle_no");
			String vehicle_type = request.getParameter("vehicle_type"); 
			String offenceId = request.getParameter("offenceId");
			String searchType = request.getParameter("searchType");
			
			
			dto.setType(searchType);
			dto.setVehicleNo(vehicle_no);
			dto.setVehicleType(vehicle_type);
			dto.setOffenceId(offenceId);

			List<LicenseDTO> offenceList  = ServiceBusiness.getInstance().searchCustomerOffence(dto);
			if(offenceList.size()==0)
			{
				request.setAttribute("messageType", "FINE_INCORRECT_PARAM");
				request.setAttribute("parameters", " Vehicle Number or Vehicle Type ");
	 			actionForward = "serviceMessage";
			}
			else
			{
				if(searchType.equalsIgnoreCase("searchUnpaidOffence"))
				{
					actionForward = "vehicle_unpaid_offence";
				}
				else if(searchType.equalsIgnoreCase("getOffenceDtls"))
				{
					actionForward = "offence_list";
				}
				request.setAttribute("OFFENCE_LIST", offenceList);
			}
		} 
		catch (Exception e) 
		{
			Log.error("Error CommonAction[searchCustomerOffence]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward vehicleRenewal(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String renewalType = request.getParameter("renewalType");
			String amount = request.getParameter("amount");
			String totalAmount = request.getParameter("totalAmount");
			String penalty = request.getParameter("penalty");
			String serviceName = "RSTA Vehicle Renewal";
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			BeanUtils.copyProperties(dto, serviceForm);
			dto.setPageId("66");
			dto.setReceiptDate("00/00/0000");
			dto.setReceiptNo("00");
			dto.setAmount(amount);
			dto.setPenalty(penalty);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			
			userVo.setJurisdictionId(dto.getJurisId());
			userVo.setJurisdictionTypeId(dto.getJurisTypeId());
			
			
			/*if(dto.getBaseoffice().equals("0"))
			{
				userVo.setJurisdictionId(dto.getRegion());
				userVo.setJurisdictionTypeId("1");
			}
			else
			{
				userVo.setJurisdictionId(dto.getBaseoffice());
				userVo.setJurisdictionTypeId("2");
			}*/
			
			dto.setRenewalType(renewalType);
			dto.setBfsNo("BFS_NO");
			
			
			String[ ][ ] services = new String[6][6];
			services[0][0]	=	"MOTOR_VEHICLE_ENDORSEMENT_FEES";
			services[1][0]	=	amount;
			services[0][1]	=	"FINES_AND_PENALTIES_ROAD";
			services[1][1]	=	penalty;

			dto.setService(services);
			dto.setServiceName(serviceName);
			String result = VehicleBusiness.getInstance().new_renewal(dto,userVo);
			String[] resultArray = result.split("#");
			String message			=	resultArray[0];
			String applicationNo	=	resultArray[1];
			String bfsOrderNo		=	resultArray[2];
			
			
			if("SUCCESS".equalsIgnoreCase(message))
			{
				request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
				request.setAttribute("APPLICATION_NO", applicationNo);
				actionForward = "online_payment_receipt";
			}
			else if ("BFS_ORDER_NO_FAILURE".equalsIgnoreCase(message))
			{
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! Transaction could not complete due to technical failure. Try again or contact nearest RSTA");
	 			actionForward = "serviceMessage";
			}
			else if ("VEHICLE_DUPLICATE_TRANSACTION".equalsIgnoreCase(message))
			{
				if(null==bfsOrderNo || "00".equalsIgnoreCase(bfsOrderNo))
				{
					request.setAttribute("messageType", "warning");
					request.setAttribute("parameters", "You have already applied and your application number is "+applicationNo+" but you failed to make payment. To make payment <a target='_blank' href='https://www.citizenservices.gov.bt/G2CPaymentAggregator/redirect.html?param=makepayment&applicationNo="+applicationNo+"'>[Click Here]</a>");
					actionForward = "serviceMessage";
				}
				else
				{
					request.setAttribute("messageType", "validation");
					request.setAttribute("parameters", "Sorry! Your application has already submitted and waiting for approval. Thank you");
					actionForward = "serviceMessage";
					
				}
			}
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward drivingLicenseOnlinePayment(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String pageId	=	request.getParameter("pageId");
			String formType	=	request.getParameter("formType");
			String licenseType = request.getParameter("licenseType");
			String amount = request.getParameter("amount");
			String penalty = request.getParameter("penalty");
			String cardCost = request.getParameter("cardCost");
			String ODLRenewalCost = request.getParameter("ODLRenewalCost");
			String PDRenewalCost = request.getParameter("PDRenewalCost");
			String serviceName = null;

		/*
			PaymentDTO dto1 = new PaymentDTO();
			dto1.setRequestType("LICENSE");
			dto1.setServiceType("RENEWAL");
			dto1.setIdentityNo("83926");
			dto1.setIdentityTypeId(licenseType);
			dto1.setLoadingCapacity("");
			dto1.setSeatingCapacity("");
			dto1.setVehicleHP("");
			dto1.setVehicleKilowatt("");
			dto1.setVehicleEngineCC("");
			dto1.setPurchaseDate("");
			dto1.setSaleDeedAmount("");
			dto1.setSaleDeedDate("");
			dto1.setRenewalDuration("");
			dto1.setRegistrationCode("");
			dto1 = CommonBusiness.getInstance().calculatePayableAmount(dto1);
		*/
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			BeanUtils.copyProperties(dto, serviceForm);
			dto.setPageId(pageId);
			dto.setReceiptDate("00/00/0000");
			dto.setReceiptNo("00");
			dto.setAmount(amount);
			dto.setPenalty(penalty);
			
			/**USER DTLS**/
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			/**USER JURISDICTION**/
			
		/*	if(dto.getBaseoffice().equals("0"))
			{
				userVo.setJurisdictionId(dto.getRegion());
				userVo.setJurisdictionTypeId("1");
			}
			else
			{
				userVo.setJurisdictionId(dto.getBaseoffice());
				userVo.setJurisdictionTypeId("2");
			}*/
			
			userVo.setJurisdictionId(dto.getJurisId());
			userVo.setJurisdictionTypeId(dto.getJurisTypeId());
			
			/**SERVICES**/
			String[][] services	= null;
			String applicationNo	= null;
			dto.setBfsNo("BFS_NO");
			String message	=	null;
			String bfsOrderNo	=	null;
			String result 	=	null;
				
			if(formType.equalsIgnoreCase("DL_RENEWAL"))
			{
				serviceName	=	"RSTA Driving License Renewal";
				services = new String[4][4];
				if(licenseType.equalsIgnoreCase("C"))
				{
					services[0][0]	=	"PROFESSIONAL_DRIVING_LICENSE_RENEWAL_FEES";
					services[1][0]	=	PDRenewalCost;
					services[0][1]	=	"ORDINARY_DRIVING_LICENSE_RENEWAL_FEES";
					services[1][1]	=	ODLRenewalCost;
					services[0][2]	=	"PVC_CARD_COST";
					services[1][2]	=	cardCost;
					services[0][3]	=	"FINES_AND_PENALTIES_ROAD";
					services[1][3]	=	penalty;
					
				}
				else if(licenseType.equalsIgnoreCase("N"))
				{
					services[0][0]	=	"ORDINARY_DRIVING_LICENSE_RENEWAL_FEES";
					services[1][0]	=	ODLRenewalCost;
					services[0][1]	=	"PVC_CARD_COST";
					services[1][1]	=	cardCost;
					services[0][2]	=	"FINES_AND_PENALTIES_ROAD";
					services[1][2]	=	penalty;
				}

				dto.setService(services);
				dto.setServiceName(serviceName);
				result = LicenseBusiness.getInstance().license_renewal(dto, userVo);
				
			}
			else if(formType.equalsIgnoreCase("DL_DUPLICATE"))
			{
				services = new String[2][2];
				services[0][0]	=	"DUPLICATE_DRIVING_LICENSE";
				services[1][0]	=	amount;
				serviceName	=	"RSTA License Replacement";
				
				dto.setService(services);
				dto.setServiceName(serviceName);
				
				result = LicenseBusiness.getInstance().license_duplication(dto, userVo);
			}
			
			String[] resultArray = result.split("#");
			message		  =	resultArray[0];
			applicationNo =	resultArray[1];
			bfsOrderNo	  =	resultArray[2];
			
			if("SUCCESS".equalsIgnoreCase(message))
			{
				request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
				request.setAttribute("APPLICATION_NO", applicationNo);
				actionForward = "online_payment_receipt";
			}
			else if ("BFS_ORDER_NO_FAILURE".equalsIgnoreCase(message) || "FAILURE".equalsIgnoreCase(message))
			{
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! Transaction could not complete due to technical failure. Try again or contact nearest RSTA");
	 			actionForward = "serviceMessage";
			}
			else if ("LICENSE_DUPLICATE_TRANSACTION".equalsIgnoreCase(message))
			{
				if(null==bfsOrderNo || "00".equalsIgnoreCase(bfsOrderNo))
				{
					request.setAttribute("messageType", "warning");
					request.setAttribute("parameters", "You have already applied and your application number is "+applicationNo+" but you failed to make payment. To make payment <a target='_blank' href='https://www.citizenservices.gov.bt/G2CPaymentAggregator/redirect.html?param=makepayment&applicationNo="+applicationNo+"'>[Click Here]</a>");
					actionForward = "serviceMessage";
				}
				else
				{
					request.setAttribute("messageType", "validation");
					request.setAttribute("parameters", "Sorry! Your application has already submitted and waiting for approval. Thank you");
					actionForward = "serviceMessage";
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward learnerLicenseOnlinePayment(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String pageId	=	request.getParameter("pageId");
			String formType	=	request.getParameter("formType");
			String amount = request.getParameter("amount");
			String serviceName = null;
			
			
			
			
			
			
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			BeanUtils.copyProperties(dto, serviceForm);
			dto.setPageId(pageId);
			dto.setReceiptDate("00/00/0000");
			dto.setReceiptNo("00");
			dto.setAmount(amount);
			
			/**USER DTLS**/
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			/**USER JURISDICTION**/
			

			userVo.setJurisdictionId(dto.getJurisId());
			userVo.setJurisdictionTypeId(dto.getJurisTypeId());
			
			/**SERVICES**/
			String[ ][ ] services	= null;
			String applicationNo	= null;
			String result	=	null;
			String message	=	null;
			String bfsOrderNo	=	null;
			dto.setBfsNo("BFS_NO");
			if(formType.equalsIgnoreCase("LL_NEW"))
			{
				String cid	=	request.getParameter("cid");
				String customerId	=	CommonBusiness.getInstance().getCustomerDtls(cid);
				dto.setCustomerId(customerId);
				
				serviceName	=	"RSTA New Learner License";
				services = new String[4][4];
				services[0][0]	=	"LEARNER_LICENSE_FEE";
				services[1][0]	=	amount;
				dto.setService(services);
				dto.setServiceName(serviceName);
				result = LicenseBusiness.getInstance().new_issue(dto, userVo);
				
				bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(services,applicationNo,serviceName);
				
				if(bfsOrderNo != null){
					result = result+"#"+bfsOrderNo;
				} else {
					String[] resultArray = result.split("#");
					message		  =	"BFS_ORDER_NO_FAILURE";
					applicationNo =	resultArray[1];
					bfsOrderNo	  =	"NA";
				}
			}
			else if(formType.equalsIgnoreCase("LL_RENEWAL"))
			{
				serviceName	=	"RSTA Learner Renewal";
				services = new String[4][4];
				services[0][0]	=	"LEARNER_LICENSE_RENEWAL";
				services[1][0]	=	amount;
				dto.setService(services);
				dto.setServiceName(serviceName);
				
				result = LicenseBusiness.getInstance().license_renewal_learner(dto, userVo);
			}
			else if(formType.equalsIgnoreCase("LL_DUPLICATE"))
			{
				serviceName	=	"RSTA Learner Replacement";
				services = new String[2][2];
				services[0][0]	=	"LEARNER_LICENSE_REPLACEMENT";
				services[1][0]	=	amount;
				dto.setService(services);
				dto.setServiceName(serviceName);
				
				result = LicenseBusiness.getInstance().license_duplication_learner(dto, userVo);
			}

			String[] resultArray = result.split("#");
			message		  =	resultArray[0];
			applicationNo =	resultArray[1];
			bfsOrderNo	  =	resultArray[2];
			
			if("SUCCESS".equalsIgnoreCase(message))
			{
				request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
				request.setAttribute("APPLICATION_NO", applicationNo);
				actionForward = "online_payment_receipt";
			}
			else if ("BFS_ORDER_NO_FAILURE".equalsIgnoreCase(message))
			{
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! Transaction could not complete due to technical failure. Try again or contact nearest RSTA");
	 			actionForward = "serviceMessage";
			}
			else if ("LEARNER_DUPLICATE_TRANSACTION".equalsIgnoreCase(message))
			{
				if(null==bfsOrderNo || "00".equalsIgnoreCase(bfsOrderNo))
				{
					request.setAttribute("messageType", "warning");
					request.setAttribute("parameters", "You have already applied and your application number is "+applicationNo+" but you failed to make payment. To make payment <a target='_blank' href='https://www.citizenservices.gov.bt/G2CPaymentAggregator/redirect.html?param=makepayment&applicationNo="+applicationNo+"'>[Click Here]</a>");
					actionForward = "serviceMessage";
				}
				else
				{
					request.setAttribute("messageType", "validation");
					request.setAttribute("parameters", "Sorry! Your application has already submitted and waiting for approval. Thank you");
					actionForward = "serviceMessage";
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward vehicleOnlinePayment(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		String fitnessId = null;
		String serviceName = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleNo = request.getParameter("vehicleNo");
			String formType = request.getParameter("formType");
			String amount = request.getParameter("amount");
			String penalty = request.getParameter("penalty");
			String pageId = request.getParameter("pageId");
			
			if(formType.equals("VEH_FITNESS")){
				fitnessId = request.getParameter("fitnessId");
			}
			
			serviceActionForm serviceForm = (serviceActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			BeanUtils.copyProperties(dto, serviceForm);
			dto.setVehicleNumber(vehicleNo);
			dto.setPageId(pageId);
			dto.setReceiptDate(null);
			dto.setReceiptNo("00");
			dto.setAmount(amount);
			dto.setPenalty(penalty);
			
			/**USER DTLS**/
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			if(!formType.equalsIgnoreCase("VEH_FITNESS")){
				/**JURISDICTION**/

				userVo.setJurisdictionId(dto.getJurisId());
				userVo.setJurisdictionTypeId(dto.getJurisTypeId());
			}
			
			/**SERVICES**/
			dto.setBfsNo("BFS_NO");
			String applicationNo	= null;
			String[ ][ ] services = null;
			String result =	null;
			String message	=	null;
			String bfsOrderNo	=	null;
			
			if(formType.equalsIgnoreCase("VEH_DUP"))
			{
				serviceName	=	"Replacement of RC";
				services = new String[2][2];
				services[0][0]	=	"DUPLICATE_REGISTRATION_CERTIFICATE_FEES";
				services[1][0]	=	amount;

				dto.setService(services);
				dto.setServiceName(serviceName);
				
				result = VehicleBusiness.getInstance().new_duplication(dto,userVo);
			}
			else if(formType.equalsIgnoreCase("VEH_FITNESS"))
			{
				serviceName = "RSTA Vehicle Fitness";
				String appNo = "PN116"+fitnessId;
				services = new String[2][2];
				applicationNo	=	"FITNESS";
				
				String vehicleType = request.getParameter("vehicleTypeDesc");
				
				if(vehicleType.equalsIgnoreCase("TWO_WHEELER"))
					services[0][0]	=	"TWO_WHEELER_ROAD_WORTHINESS_CERTIFICATE_FEES";
				else if(vehicleType.equalsIgnoreCase("LIGHT_VEHICLE"))
					services[0][0] = "LIGHT_VEHICLE_ROAD_WORTHINESS_CERTIFICATE_FEES";
				else if(vehicleType.equalsIgnoreCase("MEDIUM_VEHICLE"))
					services[0][0] = "MEDIUM_VEHICLE_ROAD_WORTHINESS_CERTIFICATE_FEES";
				else if(vehicleType.equalsIgnoreCase("HEAVY_VEHICLE"))
					services[0][0] = "HEAVY_VEHICLE_ROAD_WORTHINESS_CERTIFICATE_FEES";
				else if(vehicleType.equalsIgnoreCase("TAXI"))
					services[0][0] = "TAXI_ROAD_WORTHINESS_CERTIFICATE_FEES";
				else
					services[0][0] = "LIGHT_VEHICLE_ROAD_WORTHINESS_CERTIFICATE_FEES";
				
				double totalAmount = Double.parseDouble(amount) + Double.parseDouble(penalty);
				
				services[1][0]	=	Double.toString(totalAmount);
				 
				String payment_status = VehicleBusiness.getInstance().online_fitness_payment(fitnessId, amount, penalty);

				bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(services,appNo,serviceName);
				
				if(bfsOrderNo != null){
					message = "SUCCESS";
				} else {
					message = "BFS_ORDER_NO_FAILURE";
				}
				applicationNo = appNo; 
			}
			
			if(formType.equalsIgnoreCase("VEH_DUP"))
			{
				String[] resultArray = result.split("#");
				message		  =	resultArray[0];
				applicationNo =	resultArray[1];
				bfsOrderNo	  =	resultArray[2];
			}
			
			if("SUCCESS".equalsIgnoreCase(message))
			{
				request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
				request.setAttribute("APPLICATION_NO", applicationNo);
				actionForward = "online_payment_receipt";
			}
			else if ("BFS_ORDER_NO_FAILURE".equalsIgnoreCase(message))
			{
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! Transaction could not complete due to technical failure. Try again or contact nearest RSTA");
	 			actionForward = "serviceMessage";
			}
			else if ("VEHICLE_DUPLICATE_TRANSACTION".equalsIgnoreCase(message))
			{
				if(null==bfsOrderNo || "00".equalsIgnoreCase(bfsOrderNo))
				{
					request.setAttribute("messageType", "warning");
					request.setAttribute("parameters", "You have already applied and your application number is "+applicationNo+" but you failed to make payment. To make payment <a target='_blank' href='https://www.citizenservices.gov.bt/G2CPaymentAggregator/redirect.html?param=makepayment&applicationNo="+applicationNo+"'>[Click Here]</a>");
					actionForward = "serviceMessage";
				}
				else
				{
					request.setAttribute("messageType", "validation");
					request.setAttribute("parameters", "Sorry! Your application has already submitted and waiting for approval. Thank you");
					actionForward = "serviceMessage";
					
				}
			}
			
			/*if(!applicationNo.equalsIgnoreCase("null"))
			{
				String bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(services,applicationNo,serviceName);
				request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
				actionForward = "online_payment_receipt";
			}
			else
			{
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! Your application has already submitted and waiting for approval. Thank you");
	 			actionForward = "serviceMessage";
			}*/
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_personal_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			serviceActionForm serviceForm = (serviceActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, serviceForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			dto.setManualFlag("N");
			String result = EralisCommonBusiness.getInstance().edit_personal_info(dto, userVo);

			request.setAttribute("messageType", "success");
			if(result.equalsIgnoreCase("SUCCESS"))
				request.setAttribute("parameters", "Your details has been updated successfully");
			else
				request.setAttribute("parameters", "Your details couldnot be updated, please try again later");
			
 			actionForward = "serviceMessage";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getDataFromAggregator(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		
		try 
		{
			PrintWriter out = response.getWriter();
			StringBuffer buffer = new StringBuffer();
			
			response.setContentType("text/xml");
			String orderNo = request.getParameter("orderNo");
			String result = EralisCommonDAO.getInstance().getDataFromAggregator(orderNo);
			String[] tempArray = result.split("#");
			
			buffer.append("<xml-response>");
			buffer.append("<application_no>"+tempArray[0]+"</application_no>");
			buffer.append("<receipt_date>"+tempArray[1]+"</receipt_date>");
			buffer.append("<receipt_no>"+tempArray[2]+"</receipt_no>");
			buffer.append("<amount>"+tempArray[3]+"</amount>");
			buffer.append("<status>"+tempArray[4]+"</status>");
			buffer.append("</xml-response>");
			
			out.println(buffer);
			out.flush();
			out.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return mapping.findForward(actionForward);
	}
	
	
	private void copyUserValues(UserDetailsVO userVo)
	{
		userVo.setActorId(userId);
		userVo.setActorName(userName);
		userVo.setAssignedGroupId(assignedGroupId);
		userVo.setAssignedUserId(userId);
		userVo.setRoleId(roleId);
		userVo.setRoleName(roleName);
		userVo.setAssignedPrivId(assignedPrivId);
		userVo.setRoleCode(roleCode);
	}
	
	private void setUserValues(HttpServletRequest request)
	{ 
		this.userId = "Citizen";
		this.userName = "Citizen";
		this.roleId = "0";
		this.roleName = "Citizen";
		this.roleCode = "Citizen"; 
	}
}