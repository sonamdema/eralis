package bt.gov.rsta.framework.web.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.MenuVO;

public class LoginAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Log.info("### Entering LoginAction[execute]");

		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);

		if (userRolePriv != null) {
			if (userRolePriv.getRoles() == null
					|| userRolePriv.getRoles().length == 0) {
				actionForward = "GLOBAL_REDIRECT_LOGIN";
				request.setAttribute("message", Constants.FAILURE);
			} else {
				String lastLoginDate = CommonBusiness.getInstance()
						.getLastLoginDate(userRolePriv.getUserId());

				if (null == lastLoginDate) {
					actionForward = "CHANGE_PASSWORD";
					List<DropDownDTO> securityQuestionList = CommonBusiness
							.getInstance()
							.getDropDownList(
									Constants.SECURITY_QUESTION_DROP_DOWN_FIELD_CONSTRUCTOR,
									null);
					request.setAttribute("uid", userRolePriv.getUserId());
					request.setAttribute("securityQuestionList",
							securityQuestionList);
				} else if (userRolePriv.getIsActive().equals("Y")) {
					// CommonBusiness.getInstance().updateLoginDate(userRolePriv.getUserId());

					String totalLicenseAlert = CommonBusiness.getInstance()
							.getTotalLicenseAlert();
					request.setAttribute("TOTAL_LICENSE_ALERT",
							totalLicenseAlert);

					String totalApplicationAlert = CommonBusiness.getInstance()
							.getTotalApplicationAlert();
					request.setAttribute("TOTAL_APPLICATION_ALERT",
							totalApplicationAlert);

					Role currentRole = userRolePriv.getCurrentRole();
					String roleId = currentRole.getRoleId();

					List<MenuVO> menuList = CommonBusiness.getInstance().getMenuList(roleId);
					request.setAttribute("menuList", menuList);

					if (currentRole.getRoleCode().equalsIgnoreCase("PRINT"))
						actionForward = "PRINTER_TASK";
					else
						actionForward = Constants.LOGIN_SUCCESS;

					if (actionForward.equalsIgnoreCase("LOGIN_SUCCESS")
							&& !currentRole.getRoleCode().equalsIgnoreCase(
									"EMISSION_CENTER")) {
						String dlIssued = CommonBusiness.getInstance().TotalDLIssued(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("DL_ISSUED", dlIssued);

						String vehicleRgistered = CommonBusiness.getInstance().vehicleRgistered(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("VEHICLE_REGISTERED",vehicleRgistered);

						String vehicleRenewed = CommonBusiness.getInstance().vehicleRenewed(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("VEHICLE_RENEWAL", vehicleRenewed);

						String licenseRenewed = CommonBusiness.getInstance().licenseRenewed(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("LICENSE_RENEWAL", licenseRenewed);

						String newlicensePrinted = CommonBusiness.getInstance().newlicensePrinted(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("NEW_LICENSE_PRINTED",newlicensePrinted);

						String newLearnerIssued = CommonBusiness.getInstance().newLearnerIssued(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("NEW_LEARNER_ISSUED",newLearnerIssued);

						String getTotalPrintings = CommonBusiness.getInstance().getTotalPrintings(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("GET_TOTAL_PRINTINGS",getTotalPrintings);

						String totalEarning = CommonBusiness.getInstance().totalEarning(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("TOTAL_EARNING", totalEarning);

						String totalTaskCompletion = CommonBusiness.getInstance().totalTaskCompletion(userRolePriv.getCurrentRole(),userRolePriv.getJurisdictionId(),userRolePriv.getJurisdictionTypeId());
						request.setAttribute("TOTAL_TASK_COMPLETION",totalTaskCompletion);
					}
					if (actionForward.equalsIgnoreCase("LOGIN_SUCCESS")
							&& currentRole.getRoleCode().equalsIgnoreCase(
									"EMISSION_CENTER")) {
						String totalEmission = CommonBusiness.getInstance()
								.getTotalEmission();
						request.setAttribute("TOTAL_EMISSION", totalEmission);

						String totalEmissionPassed = CommonBusiness
								.getInstance().getTotalPassedEmission();
						request.setAttribute("TOTAL_EMISSION_PASSED",
								totalEmissionPassed);

						String totalEmissionFailed = CommonBusiness
								.getInstance().getTotalFailedEmission();
						request.setAttribute("TOTAL_EMISSION_FAILED",
								totalEmissionFailed);
					}
				} else {
					actionForward = "GLOBAL_REDIRECT_LOGIN";
					request.setAttribute("message", "LOGIN_NOT_ACTIVE");
				}
			}
		} else {
			actionForward = "GLOBAL_REDIRECT_LOGIN";
			request.setAttribute("message", Constants.FAILURE);
		}

		Log.info("### Exiting LoginAction[execute]");
		return mapping.findForward(actionForward);
	}
}
