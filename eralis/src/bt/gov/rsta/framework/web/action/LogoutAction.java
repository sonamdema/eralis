package bt.gov.rsta.framework.web.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;

public class LogoutAction extends Action
{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		Log.debug("### Entering LogoutAction[execute]");
		
		//extracting session attributes for the user
		HttpSession userSession = request.getSession(false);
		
		if(userSession != null)
		{
			Log.debug("session id: "+userSession.getId());
			synchronized(userSession)
			{
				userSession.removeAttribute(Constants.USER_DETAILS);
				userSession.invalidate();
				Log.debug("session destroyed.");
			}
		}
		
		return mapping.findForward("GLOBAL_REDIRECT_LOGIN");
	}
}
