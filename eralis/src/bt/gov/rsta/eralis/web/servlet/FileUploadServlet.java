package bt.gov.rsta.eralis.web.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUploadServlet
 */
public class FileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// sets temporary location to store files
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		ArrayList<String> lineList = new ArrayList<String>();
		StringBuffer buffer = new StringBuffer();
		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");
		
		try {
			List<FileItem> formItems = upload.parseRequest(request);
			 if (formItems != null && formItems.size() > 0) 
	            {
	                // iterates over form's fields
	                for (FileItem item : formItems) 
	                {
	                    // processes only fields that are not form fields
	                    if (!item.isFormField()) 
	                    {
	                    	InputStream fis = item.getInputStream();
	                    	
	                    	BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	                    	String next, line = br.readLine();
	                    	
	                    	for(boolean first = true, last = (line == null); !last; first = false, line = next){
	                    		last = ((next = br.readLine()) == null);
	                            lineList.add(line);
	                    	}
	                    }
	                }
	                
	                String[] values = new String[lineList.size()];
	        		lineList.toArray(values);
	        		
	        		String testDate = values[2];
	        		String coValue = values[5];
	        		String hcValue = values[4];
	        		
	        		String[] coArray = coValue.split(":");
	        		String[] hcArray = hcValue.split(":");
	        		String[] testDateArray = testDate.split(":");
	        		
	        		System.out.println("CO Value: "+coArray[1].trim());
	        		System.out.println("HC Value: "+hcArray[1].trim());
	        		System.out.println("Test Date: "+testDateArray[1].trim());
	        		
	        		buffer.append("<xml-response>");
	        			buffer.append("<co-value>" +coArray[1].trim()+ "</co-value>");
	        			buffer.append("<hc-value>" +hcArray[1].trim()+ "</hc-value>");
	        			buffer.append("<test-date>" +testDateArray[1].trim()+ "</test-date>");
	        		buffer.append("</xml-response>");
	        		
	        		out.println(buffer);
	        		out.flush();
	        		out.close();
	            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> readTxtFile(File fileName) 
	{
		ArrayList<String> lineList = new ArrayList<String>();
		
        try  {
        	Scanner scanner = new Scanner(fileName);
        	
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lineList.add(line);
            }
            
            scanner.close();
        } 
        catch (Exception e){
                e.printStackTrace();
        }
        
        return lineList;
	}
}
