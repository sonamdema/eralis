package bt.gov.rsta.eralis.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.util.Log;

/**
 * Servlet implementation class FileDownloadServlet
 */
public class FileDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileDownloadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}
	
	/**
	 * @see HttpServlet#doAction(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
	    {
		    String uuid = request.getParameter("uuid");
		    String fileName = request.getParameter("fileName");
		    
		    byte[] fileContent = CommonBusiness.getInstance().downloadFile(uuid, fileName, response);
		    response.getOutputStream().write(fileContent);
		    response.getOutputStream().flush();
		    response.getOutputStream().close();
	    } 
	    catch (Exception exception) 
	    {
	    	exception.printStackTrace();
		    Log.debug("" + exception.fillInStackTrace());
	    }
	}

}
