package bt.gov.rsta.eralis.web.servlet;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

/**
 * Servlet implementation class TaskListRedirectionServlet
 */
public class TaskListRedirectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.RedirectionMappings");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskListRedirectionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request,response);
		} catch (ERALISException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request,response);
		} catch (ERALISException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException 
	{
		Log.debug("Inside TaskListRedirectionServlet");
	    String serviceCode = request.getParameter("serviceCode");
	    String applicationNo = request.getParameter("applicationNo");
	    String assignedUserId = request.getParameter("assignedUserId");
	    
	    Log.debug("serviceCode = "+serviceCode+"; applicationNo = "+applicationNo+"; assignedUserId = "+assignedUserId);
	   
	    request.setAttribute("serviceCode", serviceCode);
	    request.setAttribute("applicationNo", applicationNo);
	    request.setAttribute("assignedUserId", assignedUserId);
	    
	    String redirectionPath = ""; 
	    String statusCode = CommonBusiness.getInstance().getStatusCode(applicationNo);
	    request.setAttribute("statusCode", statusCode);
	    if(statusCode!=null && !statusCode.equals(""))
	    {
			if(statusCode.equalsIgnoreCase("SUBMITTED") && (serviceCode.equalsIgnoreCase("ILL") || 
					serviceCode.equalsIgnoreCase("RLL") ||
					serviceCode.equalsIgnoreCase("INDL") ||
					serviceCode.equalsIgnoreCase("ICDL") ||
					serviceCode.equalsIgnoreCase("RDL") ||
					serviceCode.equalsIgnoreCase("EDL") ||
					serviceCode.equalsIgnoreCase("REGV") ||
					serviceCode.equalsIgnoreCase("RV") ||
					serviceCode.equalsIgnoreCase("TV") ||
				    serviceCode.equalsIgnoreCase("CV")||
				    serviceCode.equalsIgnoreCase("TOP") ||
				    serviceCode.equalsIgnoreCase("TOPR")||
				    serviceCode.equalsIgnoreCase("PBRP")||
				    serviceCode.equalsIgnoreCase("RRWC")))
			{
			    String key = serviceCode+"_APPROVE";
			    redirectionPath = resourceBundle.getString(key);
			    
			    request.setAttribute("PARAM", "APPROVE");
			}
			if(statusCode.equalsIgnoreCase("RESUBMITTED") && (serviceCode.equalsIgnoreCase("ILL") || 
					serviceCode.equalsIgnoreCase("RLL") ||
					serviceCode.equalsIgnoreCase("INDL") ||
					serviceCode.equalsIgnoreCase("ICDL") ||
					serviceCode.equalsIgnoreCase("RDL") ||
					serviceCode.equalsIgnoreCase("EDL") ||
					serviceCode.equalsIgnoreCase("REGV") ||
					serviceCode.equalsIgnoreCase("RV") ||
					serviceCode.equalsIgnoreCase("TV") ||
				    serviceCode.equalsIgnoreCase("CV") ||
				    serviceCode.equalsIgnoreCase("TOP") ||
					serviceCode.equalsIgnoreCase("DV")))
			{
			    String key = serviceCode+"_APPROVE";
			    redirectionPath = resourceBundle.getString(key);
			    request.setAttribute("PARAM", "APPROVE");
			}
			else if(statusCode.equalsIgnoreCase("RESUBMITTED") && (serviceCode.equalsIgnoreCase("DDL") || serviceCode.equalsIgnoreCase("DLL")))
			{
			    String key = serviceCode+"_VERIFY";
			    redirectionPath = resourceBundle.getString(key);
			    request.setAttribute("PARAM", "APPROVE");
			}
			if(statusCode.equalsIgnoreCase("SUBMITTED") && (serviceCode.equalsIgnoreCase("DLL") || 
					serviceCode.equalsIgnoreCase("DDL") ||
					serviceCode.equalsIgnoreCase("DV")))
			{
			    String key = serviceCode+"_VERIFY";
			    redirectionPath = resourceBundle.getString(key);
			    
			    request.setAttribute("PARAM", "APPROVE");
			}
			if(statusCode.equalsIgnoreCase("VERIFIED") && (serviceCode.equalsIgnoreCase("DLL") || 
					serviceCode.equalsIgnoreCase("DDL") ||
					serviceCode.equalsIgnoreCase("DV")))
			{
			    String key = serviceCode+"_APPROVE";
			    redirectionPath = resourceBundle.getString(key);
			    
			    request.setAttribute("PARAM", "APPROVE");
			}
			if(statusCode.equalsIgnoreCase("APPROVED") && 
					(serviceCode.equalsIgnoreCase("ILL") || 
					serviceCode.equalsIgnoreCase("RLL") ||
					serviceCode.equalsIgnoreCase("INDL") ||
					serviceCode.equalsIgnoreCase("ICDL") ||
					serviceCode.equalsIgnoreCase("RDL") ||
					serviceCode.equalsIgnoreCase("EDL") ||
					serviceCode.equalsIgnoreCase("REGV") ||
					serviceCode.equalsIgnoreCase("RV") ||
					serviceCode.equalsIgnoreCase("TV") ||
				    serviceCode.equalsIgnoreCase("CV")||
				    serviceCode.equalsIgnoreCase("TOP") ||
				    serviceCode.equalsIgnoreCase("TOPR") ||
				    serviceCode.equalsIgnoreCase("DLL") || 
					serviceCode.equalsIgnoreCase("DDL") ||
					serviceCode.equalsIgnoreCase("DV")))
			{
				String key = serviceCode+"_DISPATCH";
			    redirectionPath = resourceBundle.getString(key);
			    
			    request.setAttribute("PARAM", "DISPATCH");
			}
			if(statusCode.equalsIgnoreCase("PRINTED") && 
					(serviceCode.equalsIgnoreCase("INDL") ||
					serviceCode.equalsIgnoreCase("ICDL") ||
					serviceCode.equalsIgnoreCase("RDL") ||
					serviceCode.equalsIgnoreCase("EDL") ||
				    serviceCode.equalsIgnoreCase("TOP") ||
				    serviceCode.equalsIgnoreCase("TOPR") ||
					serviceCode.equalsIgnoreCase("DDL")))
			{
				String key = serviceCode+"_DISPATCH";
			    redirectionPath = resourceBundle.getString(key);
			    request.setAttribute("PARAM", "DISPATCH");
			}
			if(statusCode.equalsIgnoreCase("RESUBMIT"))
			{
			    String key = "RESUBMIT";
			    redirectionPath = resourceBundle.getString(key);
			    request.setAttribute("PARAM", "RESUBMIT");
			}	
			
	    }
	    Log.debug("####### Redirection Mapping =======>"+redirectionPath);
	    
	    RequestDispatcher dispatcher = request.getRequestDispatcher(redirectionPath);
	    dispatcher.forward(request, response);
	}

}
