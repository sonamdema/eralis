package bt.gov.rsta.eralis.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.Log;

/**
 * Servlet implementation class JsonDataLoader
 */
public class JsonDataLoader extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public JsonDataLoader() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doAction(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		String parentId = request.getParameter("parentId");
		String fieldCons = request.getParameter("fieldCons");
		
		out.println(populateDropDown(fieldCons, parentId));
		out.flush();
	}
	
	public String populateDropDown(String fieldCons, String parentId) throws UnsupportedEncodingException 
	{
		String jsonText = "";
		Gson gson = new Gson();
		List<DropDownDTO> dropDownList = new ArrayList<DropDownDTO>();
		try {
			dropDownList = CommonBusiness.getInstance().getDropDownList(fieldCons, parentId);
			jsonText = gson.toJson(dropDownList);

		} catch (Exception e) {
			Log.error("JSonDataLoader:populateDropDown() -> exception ->" + e);
		}
		jsonText = new String(jsonText.getBytes("UTF-8"), "UTF-8");
		return jsonText;
	}
}
