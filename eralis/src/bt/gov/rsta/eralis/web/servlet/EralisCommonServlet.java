
package bt.gov.rsta.eralis.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.ldapconfig.LdapPasswordUpdate;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

/**
 * Servlet implementation class EralisCommonServlet
 */
public class EralisCommonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EralisCommonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletRespongetPersonalDtlsForEditse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doAction(request, response);
		} catch (ERALISException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doAction(request, response);
		} catch (ERALISException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @throws ERALISSystemException 
	 * @throws ERALISException 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException {
		String q = request.getParameter("q");
		
		if(q == null){
			return;
		}
		
		if(q.equals("getVehicleAccidentDtls"))
		{
			getVehicleAccidentDtls(request, response);
		}
		else if(q.equals("getCidDetails"))
		{
			getCidDetails(request, response);
		}else if(q.equals("organisationCustomerId"))
		{
			organisationCustomerId(request, response);
		}
		else if(q.equals("hasApplied"))
		{
			getPrevApplyDetails(request, response);
		}
		else if(q.equals("getPersonalDtls"))
		{
			getPersonalDetails(request, response);
		}
		else if(q.equals("getPersonalDtlsByCid"))
		{
			getPersonalDtlsByCid(request, response);
		}
		else if(q.equals("getPersonalDtlsForEdit"))
		{
			getPersonalDetailsForEdit(request, response);
		}
		else if(q.equals("getOrganisationDtls"))
		{
			getOrganizationDetails(request, response);
		}
		else if(q.equals("getRenewalDtls"))
		{
			getRenewalDetails(request, response);
		}
		else if(q.equals("getOffenceDtls"))
		{
			getOffenceDtls(request, response);
		}
		else if(q.equals("getPermitInfoList"))
		{
			getPermitInfoList(request, response);
		}
		else if(q.equals("getLearnerLicenseInfoList"))
		{
			getLearnerLicenseInfoList(request, response);
		}
		else if(q.equals("getlicenserenewalInfoList"))
		{
			getlicenserenewalInfoList(request, response);
		}
		else if(q.equals("getlearnerlicenserenewalList"))
		{
			getlearnerlicenserenewalList(request, response);
		}
		else if(q.equals("getLearnerTestDetails"))
		{
			getLearnerTestDetails(request, response);
		}
		else if(q.equals("getSuspensionDetails"))
		{
			getSuspensionDetails(request, response);
		}
		
		else if(q.equals("getpermitDetails"))
		{
			getpermitDetails(request, response);
		}
		else if(q.equals("getOwnerInformation"))
		{
			getOwnerInformation(request, response);
		}
		else if(q.equals("getlearnerDetails"))
		{
			getlearnerDetails(request, response);
		}
		else if(q.equals("getTestDate"))
		{
			getTestDate(request,response);
		}
		else if(q.equals("getMaxApplicants"))
		{
			getMaxApplicants(request, response);
		}
		else if(q.equals("getLearnerInformation"))
		{
			getLearnerInformation(request,response);
		}
		else if(q.equals("getLicenseInfo"))
		{
			getLicenseInfo(request,response);
		}
		else if(q.equals("getcheckCID"))
		{
			getcheckCID(request,response);
		}
		else if(q.equals("testValidation"))
		{
			testValidation(request, response);
		}
		else if(q.equals("changePassword"))
		{
			changePassword(request, response);
		}
		else if(q.equals("changeEmailAddress"))
		{
			changeEmailAddress(request, response);
		}
		else if(q.equals("changeMobileNumber"))
		{
			changeMobileNumber(request, response);
		}
		else if(q.equalsIgnoreCase("getPaymentDetails"))
		{
			calculatePayableAmount(request, response);
		}
		else if(q.equalsIgnoreCase("generateTOPNoFormat"))
		{
			generateTOPNoFormat(request, response);
		}
		else if(q.equals("getFitnessValidity"))
		{
			getFitnessValidity(request, response);
		}
		else if(q.equals("getDriveTypeTestMarks"))
		{
			getDriveTypeTestMarks(request, response);
		}
		else if(q.equals("validateCommercialCriteria"))
		{
			validateCommercialCriteria(request, response);
		}
		else if(q.equals("validateNonCommercialCriteria"))
		{
			validateNonCommercialCriteria(request, response);
		}
		else if(q.equals("getTopDtls"))
		{
			getTopDtls(request, response);
		}
		else if(q.equals("checkIfExists"))
		{
			checkIfExists(request, response);
		}
		else if(q.equals("validateOTP"))
		{
			validateOTP(request, response);
		}
		else if(q.equals("getVehicleModelDtls"))
		{
			getVehicleModelDtls(request, response);
		}
		else if(q.equals("getaccidentDetails"))
		{
			getaccidentDetails(request, response);
		}
		else if(q.equals("getaccidentPersonalDetails"))
		{
			getaccidentPersonalDetails(request, response);
		}
		else if(q.equals("checkDoubleOffence"))
		{
			checkDoubleOffence(request, response);
		}
		else if(q.equals("getLicensAndLearnerDtls"))
		{
			getLicensAndLearnerDtls(request, response);
		}
		else if(q.equals("getVehicleDtls"))
		{
			getVehicleDtls(request, response);
		}
		else if(q.equals("checkDoubleTin"))
		{
			checkDoubleTin(request, response);
		}
		else if(q.equals("validateChasisEngineNo"))
		{
			validateChasisEngineNo(request, response);
		}
		else if(q.equals("getVehiclePreviousPayment"))
		{
			getVehiclePreviousPayment(request, response);
		}
		
		
		
	}
	
	protected void getTopDtls(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		String result = "";
		try 
		{
			response.setContentType("text/xml");
			String q = request.getParameter("q");
			if(q == null){
				return;
			}
			String searchType = request.getParameter("searchType");
			String topNumber = request.getParameter("topNumber");
			LicenseDTO dto = LicenseBusiness.getInstance().getTopDtls(topNumber,searchType);
			buffer.append("<xml-response>");
				buffer.append("<topno>" + dto.getTopNo() + "</topno>");
				buffer.append("<name>" + dto.getName() + "</name>");
				buffer.append("<region>" + dto.getRegion() + "</region>");
				buffer.append("<dzongkhag>" + dto.getDzongkhag() + "</dzongkhag>");
				buffer.append("<exactLocation>" + dto.getExactLocation() + "</exactLocation>");
				buffer.append("<customerId>" + dto.getCustomerId() + "</customerId>");
				buffer.append("<phoneNo>" + dto.getPhoneNo() + "</phoneNo>");
				buffer.append("<cid>" + dto.getCID() + "</cid>");
				buffer.append("<permDzongkhag>" + dto.getPermDzongkhag() + "</permDzongkhag>");
				buffer.append("<permGewog>" + dto.getPermGewog() + "</permGewog>");
				buffer.append("<permVillage>" + dto.getPermVillage() + "</permVillage>");
				buffer.append("<vehicleNo>" + dto.getVehicleNo() + "</vehicleNo>");
				buffer.append("<vehicleCompany>" + dto.getVehicleCompany() + "</vehicleCompany>");
				buffer.append("<vehicleModel>" + dto.getVehicleModel() + "</vehicleModel>");
				buffer.append("<vehicleColor>" + dto.getVehicleColor() + "</vehicleColor>");
				buffer.append("<engineNo>" + dto.getEngineNumber() + "</engineNo>");
				buffer.append("<engineCC>" + dto.getEngineCC() + "</engineCC>");
				buffer.append("<chassisNo>" + dto.getChassisNumber() + "</chassisNo>");
				buffer.append("<engineType>" + dto.getEngineType() + "</engineType>");
				buffer.append("<seatingCapacity>" + dto.getSeatingCapacity() + "</seatingCapacity>");
				buffer.append("<regionId>" + dto.getRegionId() + "</regionId>");
				buffer.append("<licenseNo>" + dto.getLicenseNo() + "</licenseNo>");
				buffer.append("<status>" + dto.getStatus() + "</status>");
				buffer.append("<reason>" + dto.getReason() + "</reason>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}
	
	protected void getFitnessValidity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		String result = "";
		try 
		{
			response.setContentType("text/xml");
			String q = request.getParameter("q");
			if(q == null){
				return;
			}
			String testDate = request.getParameter("testDate");
			String vehicleId = request.getParameter("vehicleId");
			String vehicleTypeDesc = request.getParameter("vehicleTypeDesc");
			result = VehicleBusiness.getInstance().getFitnessValidity(testDate, vehicleId, vehicleTypeDesc);
			buffer.append("<xml-response>");
				buffer.append("<validity>" + result + "</validity>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}
	
	protected void calculatePayableAmount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		PaymentDTO dto = new PaymentDTO();
		
		try 
		{
			response.setContentType("text/xml");
			
			String q = request.getParameter("q");
			
			if(q == null){
				return;
			}
			
			String requestType = request.getParameter("requestType");
			String serviceType = request.getParameter("serviceType");
			String identityNo = request.getParameter("identityNo");
			String identityTypeId = request.getParameter("identityTypeId");
			String loadingCapacity = request.getParameter("loadingCapacity");
			String seatingCapacity = request.getParameter("seatingCapacity");
			String vehicleHP = request.getParameter("vehicleHP");
			String kilowatt = request.getParameter("kilowatt");
			String engineCC = request.getParameter("engineCC");
			String purchaseDate = request.getParameter("purchaseDate");
			String saleDeedAmount = request.getParameter("saleDeedAmount");
			String saleDeedDate = request.getParameter("saleDeedDate");
			String renewalDuration = request.getParameter("renewalDuration");
			String registrationCode = request.getParameter("registrationCode");
			
			dto.setRequestType(requestType);
			dto.setServiceType(serviceType);
			dto.setIdentityNo(identityNo);
			dto.setIdentityTypeId(identityTypeId);
			dto.setLoadingCapacity(loadingCapacity);
			dto.setSeatingCapacity(seatingCapacity);
			dto.setVehicleHP(vehicleHP);
			dto.setVehicleKilowatt(kilowatt);
			dto.setVehicleEngineCC(engineCC);
			dto.setPurchaseDate(purchaseDate);
			dto.setSaleDeedAmount(saleDeedAmount);
			dto.setSaleDeedDate(saleDeedDate);
			dto.setRenewalDuration(renewalDuration);
			dto.setRegistrationCode(registrationCode);
			
			dto = CommonBusiness.getInstance().calculatePayableAmount(dto);
			
			buffer.append("<xml-response>");
				buffer.append("<status>" + dto.getStatus() + "</status>");
				buffer.append("<penalty-flag>" + dto.getPenaltyFlag() + "</penalty-flag>");
				buffer.append("<amount>" + dto.getAmount() + "</amount>");
				buffer.append("<penalty>" + dto.getPenalty() + "</penalty>");
				buffer.append("<total>" + dto.getTotalAmount() + "</total>");
				buffer.append("<marketValue>" + dto.getMarketValue() + "</marketValue>");
				buffer.append("<saleDeedValue>" + dto.getSaleDeedAmount() + "</saleDeedValue>");
				buffer.append("<valueForTT>" + dto.getValueForTT() + "</valueForTT>");
				buffer.append("<taxedAmount>" + dto.getTaxedAmount() + "</taxedAmount>");
				buffer.append("<transferFees>" + dto.getTransferFees() + "</transferFees>");
				buffer.append("<initial-price>" + dto.getInitialPurchasePrice() + "</initial-price>");
				buffer.append("<registration-date>" + dto.getRegistrationDate() + "</registration-date>");
				buffer.append("<cardCost>" + dto.getCardCost() + "</cardCost>");
				buffer.append("<ODLRenewalCost>" + dto.getODLRenewalCost() + "</ODLRenewalCost>");
				buffer.append("<PDRenewalCost>" + dto.getPDRenewalCost() + "</PDRenewalCost>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		out.println(buffer);
	}
	
	protected void changePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		String result = "";
		
		try 
		{
			response.setContentType("text/xml");
			
			String q = request.getParameter("q");
			
			if(q == null){
				return;
			}
			
			String uid = request.getParameter("uid");
			String cpassword = request.getParameter("cpassword");
			String newpassword = request.getParameter("newpassword");
			
			result = LdapPasswordUpdate.updateUserPassword(uid, newpassword, cpassword);
			
			buffer.append("<xml-response>");
				buffer.append("<status>" + result + "</status>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		out.println(buffer);
	}
	
	protected void changeEmailAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		try 
		{
			response.setContentType("text/xml");
			
			String q = request.getParameter("q");
			
			if(q == null){
				return;
			}
			
			String uid = request.getParameter("uid");
			String nemail = request.getParameter("nemail");
			
			ApplicationDataVO appVO = new ApplicationDataVO();
			appVO.setUid(uid);
			appVO.setEmail(nemail);
			
			String result = CommonBusiness.getInstance().changeUserAccountInfo("CHANGE_EMAIL", appVO);
			
			buffer.append("<xml-response>");
				buffer.append("<status>" + result + "</status>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		out.println(buffer);
	}
	
	protected void changeMobileNumber(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		try 
		{
			response.setContentType("text/xml");
			
			String q = request.getParameter("q");
			
			if(q == null){
				return;
			}
			
			String uid = request.getParameter("uid");
			String nmobileNumber = request.getParameter("nmobilenumber");
			
			ApplicationDataVO appVO = new ApplicationDataVO();
			appVO.setUid(uid);
			appVO.setMobileNumber(nmobileNumber);
			
			String result = CommonBusiness.getInstance().changeUserAccountInfo("CHANGE_MOBILE_NUMBER", appVO);
			
			buffer.append("<xml-response>");
				buffer.append("<status>" + result + "</status>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		out.println(buffer);
	}
	
	protected void getOwnerInformation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String vehicleNo = request.getParameter("vehicleNo");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setVehicleNumber(vehicleNo);
		appVO = CommonBusiness.getInstance().getOwnerInformation(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +appVO.getOwnerName()+ "</name>");
			buffer.append("<customer-id>" +appVO.getCustomerId()+ "</customer-id>");
			buffer.append("<owner-type>" +appVO.getOwnerType()+ "</owner-type>");
			buffer.append("<model>" +appVO.getModel()+ "</model>");
			buffer.append("<make>" +appVO.getMake()+ "</make>");
			buffer.append("<engine-number>" +appVO.getEngineNumber()+ "</engine-number>");
			buffer.append("<chassis-number>" +appVO.getChasisNumber()+ "</chassis-number>");
			buffer.append("<manufacture-year>" +appVO.getManufacturerYear()+ "</manufacture-year>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	/*protected void getLearnerTestDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String learnerNo = request.getParameter("learnerNo");
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setLicenseno(learnerNo);
		appVO = CommonBusiness.getInstance().getLearnerTestDetails(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +appVO.getName()+ "</name>");
			buffer.append("<cid>" +appVO.getCidNumber()+ "</cid>");
			buffer.append("<drive-type>" +appVO.getDriveType()+ "</drive-type>");
			buffer.append("<marks-obtained>" +appVO.getTheoryMarksObtained()+ "</marks-obtained>");
			buffer.append("<app-no>" +appVO.getApplicationNumber()+ "</app-no>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
			buffer.append("<type>"+appVO.getType()+"</type>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}*/
	
	protected void getLearnerTestDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		response.setContentType("text/xml");
		
		String learnerNo = request.getParameter("learnerNo");
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setLicenseno(learnerNo);
		appVO = CommonBusiness.getInstance().getLearnerTestDetails(appVO,userRolePriv);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +appVO.getName()+ "</name>");
			buffer.append("<isTest>" +appVO.getIsTest()+ "</isTest>");
			buffer.append("<cid>" +appVO.getCidNumber()+ "</cid>");
			buffer.append("<drive-type>" +appVO.getDriveType()+ "</drive-type>");
			buffer.append("<marks-obtained>" +appVO.getTheoryMarksObtained()+ "</marks-obtained>");
			buffer.append("<app-no>" +appVO.getApplicationNumber()+ "</app-no>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
			buffer.append("<type>"+appVO.getType()+"</type>");
			buffer.append("<testStatus>"+appVO.getTestStatus()+"</testStatus>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getVehicleAccidentDtls(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String vehicleNumber = request.getParameter("vehicleNumber");
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO = CommonBusiness.getInstance().getVehicleAccidentDtls(vehicleNumber);
		
		buffer.append("<xml-response>");

			buffer.append("<Vehicle_Type_Name>" +appVO.getVehicleType()+ "</Vehicle_Type_Name>");
			buffer.append("<Owner_Type>" +appVO.getOwnerType()+ "</Owner_Type>");
			buffer.append("<Vehicle_Model_Name>" +appVO.getVehicleModel()+ "</Vehicle_Model_Name>");
			buffer.append("<Vehicle_Company_Name>" +appVO.getVehicleCompany()+ "</Vehicle_Company_Name>");
			buffer.append("<Country_Name>" +appVO.getCountry()+ "</Country_Name>");
			
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}

	
	protected void getCidDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String cid = request.getParameter("cid");
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setCidNumber(cid);
		appVO = CommonBusiness.getInstance().getCidDetails(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<first-name>" +appVO.getFirstName()+ "</first-name>");
			buffer.append("<middle-name>" +appVO.getMiddleName()+ "</middle-name>");
			buffer.append("<last-name>" +appVO.getLastName()+ "</last-name>");
			buffer.append("<dob>" +appVO.getDob()+ "</dob>");
			buffer.append("<fatherName>" +appVO.getFatherName()+ "</fatherName>");
			buffer.append("<gender>" +appVO.getGender()+ "</gender>");
			buffer.append("<dzongkhagId>" +appVO.getPermDzongkhagId()+ "</dzongkhagId>");
			buffer.append("<gewogId>" +appVO.getPermGewogId()+ "</gewogId>");
			buffer.append("<village>" +appVO.getPermVillage()+ "</village>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void organisationCustomerId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		String organisationName = request.getParameter("organisationName");
		String organisationType = request.getParameter("organisationType");

		ApplicationDataVO appVO = CommonBusiness.getInstance().organisationCustomerId(organisationName,organisationType);
		request.setAttribute("customerId", appVO.getCustomerId());
		buffer.append("<xml-response>");
			buffer.append("<customerId>" +appVO.getCustomerId()+ "</customerId>");
			buffer.append("<Ministry_Id>" +appVO.getMinistryId()+ "</Ministry_Id>");
			buffer.append("<Department_Id>" +appVO.getDepartmentId()+ "</Department_Id>");
			buffer.append("<Region_Id>" +appVO.getRegion()+ "</Region_Id>");
			buffer.append("<Dzongkhag_Id>" +appVO.getDzongkhag()+ "</Dzongkhag_Id>");
			buffer.append("<Address>" +appVO.getAddress()+ "</Address>");
			buffer.append("<Phone_Number>" +appVO.getPhone()+ "</Phone_Number>");
			buffer.append("<Remarks>" +appVO.getRemarks()+ "</Remarks>");
			buffer.append("<Email_Id>" +appVO.getEmail()+ "</Email_Id>");
			 
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getPrevApplyDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		try 
		{
			response.setContentType("text/xml");
			String q = request.getParameter("q");
			if(q == null){
				return;
			}
			String cidNumber = request.getParameter("cid");
			ApplicationDataVO appVO = new ApplicationDataVO();
			appVO.setCidNumber(cidNumber);
			appVO = CommonBusiness.getInstance().getPrevApplyDetails(appVO);
			buffer.append("<xml-response>");
				buffer.append("<flag>" + appVO.getPrevApplyFlag() + "</flag>");
			buffer.append("</xml-response>");
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		out.println(buffer);
	}
	
	protected void getPersonalDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String searchType = request.getParameter("searchType");
		String personalInfoId = request.getParameter("personalInfoId");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getPersonalInfoDtls(personalInfoId,searchType);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +dto.getName()+ "</name>");
			buffer.append("<age>" +dto.getAge()+ "</age>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<citizenID>" +dto.getCID()+ "</citizenID>");	
			buffer.append("<courtesy>" +dto.getTitleOfcourtesy()+ "</courtesy>");
			buffer.append("<occupation>" +dto.getOccupation()+ "</occupation>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<bloodgroup>" +dto.getBloodGroup()+ "</bloodgroup>");
			buffer.append("<bloodgroupId>" +dto.getBloodGroupId()+ "</bloodgroupId>");
			buffer.append("<dob>" +dto.getDOB()+ "</dob>");
			buffer.append("<nationality>" +dto.getNationality()+ "</nationality>");
			buffer.append("<fathersname>"+dto.getFathersName()+"</fathersname>");
			buffer.append("<identificationMarks>"+dto.getIdentificationMarks()+"</identificationMarks>");
			buffer.append("<remarks>"+dto.getRemarks()+"</remarks>");
			buffer.append("<gender>"+dto.getGender()+"</gender>");
			buffer.append("<country>"+dto.getCountry()+"</country>");
			buffer.append("<transfereeCustomerId>" +dto.getTransfereeCustomerId()+ "</transfereeCustomerId>");
			buffer.append("<duration>" +dto.getLearnerapplicationage()+ "</duration>");
			buffer.append("<drivingLicenseId>" +dto.getDrivinglicenseId()+ "</drivingLicenseId>");
			buffer.append("<CID>"+dto.getCID()+"</CID>");
			buffer.append("<personalInfoId>"+dto.getPersonalInfoId()+"</personalInfoId>");
			buffer.append("<village>"+dto.getVillage()+"</village>");
			buffer.append("<licenseNO>"+dto.getLicenseNo()+"</licenseNO>");
			buffer.append("<vehicleNo>"+dto.getVehicleNo()+"</vehicleNo>");
			buffer.append("<status>" +dto.getStatus()+ "</status>");
			buffer.append("<reason>" +dto.getReason()+ "</reason>");

			buffer.append("<testmarks>"+dto.getOverallObtainedMark()+"</testmarks>");
			buffer.append("<driveTypeId>"+dto.getDrivetype()+"</driveTypeId>");
			buffer.append("<Theory_Marks_Obtained>" +dto.getTheoryMarks()+ "</Theory_Marks_Obtained>");
			buffer.append("<Practical_Marks_Obtained>" +dto.getPracticalMarks()+ "</Practical_Marks_Obtained>");
			buffer.append("<Issue_Type>" +dto.getIssueType()+ "</Issue_Type>");
			buffer.append("<is_international>" +dto.getIsInternational()+ "</is_international>");
			buffer.append("<vehicle_type>" +dto.getVehicleType()+ "</vehicle_type>");
			
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	
	protected void getPersonalDtlsByCid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String cid = request.getParameter("cid");
		String dob = request.getParameter("dob");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getPersonalDtlsByCid(cid,dob);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +dto.getName()+ "</name>");
			buffer.append("<cid>" +dto.getCID()+ "</cid>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<gender>" +dto.getGender()+ "</gender>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");	
			buffer.append("<village>" +dto.getVillage()+ "</village>");
			buffer.append("<phone>" +dto.getPhone()+ "</phone>");
			buffer.append("<email>" +dto.getEmail()+ "</email>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<status>" +dto.getStatus()+ "</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getPersonalDetailsForEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String personalInfoId = request.getParameter("personalInfoId");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getPersonalInfoDtlsForEdit(personalInfoId);
		
		buffer.append("<xml-response>");
			buffer.append("<guardianContactNo>" +dto.getGuardianNo()+ "</guardianContactNo>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<cid>" +dto.getCID()+ "</cid>");
			buffer.append("<title-courtesy>" +dto.getTitleOfcourtesy()+ "</title-courtesy>");
			buffer.append("<firstname>" +dto.getFirstname()+ "</firstname>");
			buffer.append("<middlename>" +dto.getMiddlename()+ "</middlename>");
			buffer.append("<lastname>" +dto.getLastName()+ "</lastname>");
			buffer.append("<occupation>" +dto.getOccupation()+ "</occupation>");
			buffer.append("<nationality>" +dto.getNationality()+ "</nationality>");
			buffer.append("<dob>" +dto.getDOB()+ "</dob>");
			buffer.append("<father-name>" +dto.getFathersName()+ "</father-name>");
			buffer.append("<blood-group>" +dto.getBloodGroupId()+ "</blood-group>");
			buffer.append("<gender>" +dto.getGender()+ "</gender>");
			buffer.append("<remarks>" +dto.getRemarks()+ "</remarks>");
			buffer.append("<is-international>" +dto.getIsInternational()+ "</is-international>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
			buffer.append("<village>" +dto.getVillage()+ "</village>");
			buffer.append("<country>" +dto.getCountry()+ "</country>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<present-dzongkhag>" +dto.getPresentDzongkhag()+ "</present-dzongkhag>");
			buffer.append("<present-address>" +dto.getContactAddress()+ "</present-address>");
			buffer.append("<phone>" +dto.getPhone()+ "</phone>");
			buffer.append("<email>" +dto.getEmail()+ "</email>");
			buffer.append("<personal-info-id>" +dto.getPersonalInfoId()+ "</personal-info-id>");
			buffer.append("<image-path>" +dto.getImagePath()+ "</image-path>");
			buffer.append("<contactRadio>" +dto.getContactRadio()+ "</contactRadio>");
			buffer.append("<ministry>" +dto.getMinistry()+ "</ministry>");
			buffer.append("<department>" +dto.getDepartment()+ "</department>");
			buffer.append("<mother-name>" +dto.getMothersName()+ "</mother-name>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}

	protected void getOrganizationDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String organisationInfoId = request.getParameter("organisationInfoId");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getOrganisationInfoDtls(organisationInfoId);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +dto.getName()+ "</name>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<region>" +dto.getRegion()+ "</region>");
			buffer.append("<phone>" +dto.getPhone()+ "</phone>");
			buffer.append("<email>" +dto.getEmail()+ "</email>");
			buffer.append("<remarks>" +dto.getRemarks()+ "</remarks>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<organization-type>" +dto.getType()+ "</organization-type>");
			buffer.append("<organization-type-id>" +dto.getOrganizationTypeId()+ "</organization-type-id>");
			buffer.append("<region-id>" +dto.getRegionId()+ "</region-id>");
			buffer.append("<dzongkhag-id>" +dto.getDzongkhagId()+ "</dzongkhag-id>");
			buffer.append("<ministry-id>" +dto.getMinistry()+ "</ministry-id>");
			buffer.append("<department-id>" +dto.getDepartment()+ "</department-id>");
			buffer.append("<private-id>" +dto.getPrivateCompany()+ "</private-id>");
			buffer.append("<diplomat-id>" +dto.getDiplomatId()+ "</diplomat-id>");
			buffer.append("<organisationInfoId>"+dto.getOrganisationInfoId()+"</organisationInfoId>");
			buffer.append("<vehicle_type>"+dto.getVehicleType()+"</vehicle_type>");
			buffer.append("<vehicleNo>"+dto.getVehicleNo()+"</vehicleNo>");
			buffer.append("<status>" +dto.getStatus()+ "</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getRenewalDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		EralisCommonDTO dto = new EralisCommonDTO();
		
		response.setContentType("text/xml");
		String renewalInfoId = request.getParameter("renewalInfoId");
		String type	=	request.getParameter("type");
		dto = CommonBusiness.getInstance().getRenewalInfoDtls(renewalInfoId,type);
		
		buffer.append("<xml-response>");
		
			buffer.append("<vehicle_type>" +dto.getVehicleType()+ "</vehicle_type>");
			buffer.append("<vehicleNo>" +dto.getVehicleNo()+ "</vehicleNo>");
			buffer.append("<passangerBusType>" +dto.getPassengerBusType()+ "</passangerBusType>");
			buffer.append("<lifeExpiryDate>" +dto.getLifeSpanExpiryDate()+ "</lifeExpiryDate>");
			buffer.append("<nationalType>" +dto.getType()+ "</nationalType>");
			buffer.append("<vehicleAge>" +dto.getVehicleAge()+ "</vehicleAge>");
			buffer.append("<OwnerType>" +dto.getOwnerType()+ "</OwnerType>");
			buffer.append("<OwnerName>" +dto.getName() + "</OwnerName>");
			buffer.append("<CID>" +dto.getCID()+ "</CID>");
			buffer.append("<ownerid>" +dto.getCustomerID() + "</ownerid>");
			buffer.append("<phoneno>" +dto.getPhone() + "</phoneno>");
			buffer.append("<Dzongkhag>" +dto.getDzongkhag() + "</Dzongkhag>");
			buffer.append("<Gewog>" +dto.getGewog() + "</Gewog>");
			buffer.append("<Village>" +dto.getVillage() + "</Village>");
			buffer.append("<Countryname>" +dto.getCountry() + "</Countryname>");
			buffer.append("<Address>" +dto.getAddress() + "</Address>");
			buffer.append("<IsInternational>" +dto.getNational() + "</IsInternational>");
			buffer.append("<RegistrationDate>" +dto.getRegistrationDate() + "</RegistrationDate>");
			buffer.append("<ExpiryDate>" +dto.getExpiryDate() + "</ExpiryDate>");
			buffer.append("<Company>" +dto.getCompany()+ "</Company>");
			buffer.append("<Model>" +dto.getModel() + "</Model>");
			buffer.append("<chasisNumber>" +dto.getChasisNumber()+ "</chasisNumber>");
			buffer.append("<engineNumber>" +dto.getEngineNumber() + "</engineNumber>");
			buffer.append("<engineType>" +dto.getEngineType() + "</engineType>");
			buffer.append("<seatCapacity>" +dto.getSeatCapacity() + "</seatCapacity>");
			buffer.append("<loadCapacity>" +dto.getLoadCapacity() + "</loadCapacity>");
			buffer.append("<Region>" +dto.getRegion() + "</Region>");
			buffer.append("<Region-Id>" +dto.getRegionId() + "</Region-Id>");
			buffer.append("<lastIssueDate>" +dto.getRegistrationDate() + "</lastIssueDate>");
			buffer.append("<initial-price>" +dto.getInitialAmount()+ "</initial-price>");
			buffer.append("<personalInfoId>" +dto.getPersonalInfoId()+ "</personalInfoId>");
			buffer.append("<vehicleType>" +dto.getVehicleType()+ "</vehicleType>");
			buffer.append("<vehicle-type-name>" +dto.getVehicleTypeName()+ "</vehicle-type-name>");
			buffer.append("<Color>" +dto.getColor() + "</Color>");
			buffer.append("<colorName>" +dto.getColorName() + "</colorName>");
			buffer.append("<Status>" +dto.getStatus() + "</Status>");
			buffer.append("<Email>" +dto.getEmail() + "</Email>");
			buffer.append("<vehicleNumber>" +dto.getVehicleNo() + "</vehicleNumber>");
			buffer.append("<customerId>" +dto.getCustomerID() + "</customerId>");
			buffer.append("<vehicleHorsePower>" +dto.getVehicleHorsePower() + "</vehicleHorsePower>");
			buffer.append("<vehicleKiloWatt>" +dto.getVehicleKiloWatt() + "</vehicleKiloWatt>");
			buffer.append("<engineCC>" +dto.getEngineCC() + "</engineCC>");
			buffer.append("<manufacture-year>" +dto.getManufactureYear()+ "</manufacture-year>");
			buffer.append("<transferorCustomerId>" +dto.getTransferorCustomerId()+ "</transferorCustomerId>");
			buffer.append("<vehicleId>" +dto.getVehicleId()+ "</vehicleId>");
			buffer.append("<vehicleTypeDesc>" +dto.getVehicleTypeDesc()+ "</vehicleTypeDesc>");
			buffer.append("<vehicle-registration-code>" +dto.getVehicleRegistrationType()+ "</vehicle-registration-code>");
			buffer.append("<region>" +dto.getRegion()+ "</region>");
			buffer.append("<cancelDate>" +dto.getCancellationDate()+ "</cancelDate>");
			buffer.append("<reason>" +dto.getReason()+ "</reason>");
			buffer.append("<ministry>" +dto.getMinistry()+ "</ministry>");
			buffer.append("<department>" +dto.getDepartment()+ "</department>");
			buffer.append("<private>" +dto.getPrivateCompany()+ "</private>");
			buffer.append("<owner>" +dto.getOwner()+ "</owner>");
			buffer.append("<owner-type-desc>" +dto.getOwnerTypeDesc()+ "</owner-type-desc>");
			buffer.append("<testResult>" +dto.getTestResult()+ "</testResult>");
			buffer.append("<receiptNo>" +dto.getReceiptNo()+ "</receiptNo>");
			buffer.append("<receiptDate>" +dto.getReceiptDate()+ "</receiptDate>");
			buffer.append("<bus-type-id>" +dto.getBusTypeId()+ "</bus-type-id>");
			buffer.append("<bus-type-name>" +dto.getBusTypeDesc()+ "</bus-type-name>");
			buffer.append("<validity>" +dto.getValidUpto() + "</validity>");
			
			buffer.append("<initialReceiptDate>" +dto.getInitialReceiptDate() + "</initialReceiptDate>");
			buffer.append("<initialReceiptNo>" +dto.getInitialReceiptNo() + "</initialReceiptNo>");
			buffer.append("<penalty>" +dto.getPenalty() + "</penalty>");
			buffer.append("<transferExpiryDate>" +dto.getTransferExpiryDate() + "</transferExpiryDate>");
			buffer.append("<licenseNo>" +dto.getLicenseNo() + "</licenseNo>");
			buffer.append("<outsatndingVehicleNo>" +dto.getOutstandingVehicleNo() + "</outsatndingVehicleNo>");
			buffer.append("<outsatndingVehicleType>" +dto.getOutstandingVehicleType() + "</outsatndingVehicleType>");
			
		buffer.append("</xml-response>");
		
		
		out.println(buffer);
		out.flush();
		out.close();
	}

	protected void getOffenceDtls(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		EralisCommonDTO dto = new EralisCommonDTO();
		response.setContentType("text/xml");
		String offenceId = request.getParameter("offenceId");
		dto = CommonBusiness.getInstance().getOffenceDtls(offenceId);
		
		buffer.append("<xml-response>");
		buffer.append("<offenceDate>" +dto.getOffenceDate()+ "</offenceDate>");
		buffer.append("<Inspected_By>" +dto.getInspectedBy()+ "</Inspected_By>");
		buffer.append("<Inspection_Type>" +dto.getInspectionType()+ "</Inspection_Type>");
		buffer.append("<Traffic_Branch>" +dto.getTrafficBranch()+ "</Traffic_Branch>");
		buffer.append("<Place_Of_Inspection>" +dto.getPlaceOfInspection()+ "</Place_Of_Inspection>");
		buffer.append("<Region_Id>" +dto.getRegionId()+ "</Region_Id>");
		buffer.append("<Vehicle_Number>" +dto.getVehicleNo()+ "</Vehicle_Number>");
		buffer.append("<Driving_License_No>" +dto.getLicenseNo()+ "</Driving_License_No>");
		buffer.append("<Learner_License_No>" +dto.getLearnerLicenseId()+ "</Learner_License_No>");
		buffer.append("<Is_Bluebook_Seized>" +dto.getIsBluebookSeized()+ "</Is_Bluebook_Seized>");
		buffer.append("<Is_License_Seized>" +dto.getIsLicenseSeized()+ "</Is_License_Seized>");
		buffer.append("<Remarks>" +dto.getRemarks()+ "</Remarks>");
		buffer.append("<Time_Of_Inspection>" +dto.getTimeOfInspection()+ "</Time_Of_Inspection>");
		buffer.append("<amount>" +dto.getAmount()+ "</amount>");
		buffer.append("<offenceId>" +dto.getOffenceId()+ "</offenceId>");
		buffer.append("<tinNo>" +dto.getTIN()+ "</tinNo>");
		buffer.append("<totalOffenceDuration>" +dto.getTotalDuration()+ "</totalOffenceDuration>");
		
		
		buffer.append("</xml-response>");
		
		
		out.println(buffer);
		out.flush();
 
		out.close();
	}


	protected void checkDoubleTin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		//EralisCommonDTO dto = new EralisCommonDTO();
		response.setContentType("text/xml");
		String tinNo = request.getParameter("tinNo");
		
		String result = CommonBusiness.getInstance().checkDoubleTin(tinNo);
		
		buffer.append("<xml-response>");
			buffer.append("<result>" +result+ "</result>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
 
		out.close();
	}


	protected void checkDoubleOffence(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		//EralisCommonDTO dto = new EralisCommonDTO();
		response.setContentType("text/xml");

		String offenceTypeId = request.getParameter("offenceTypeId");
		String identificationNo = request.getParameter("identificationNo");
		String identificationType = request.getParameter("identificationType");
		
		
		String result = CommonBusiness.getInstance().checkDoubleOffence(offenceTypeId,identificationNo,identificationType);
		
		buffer.append("<xml-response>");
			buffer.append("<result>" +result+ "</result>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
 
		out.close();
	}
	
	
	
	protected void getPermitInfoList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String permitDetailsId = request.getParameter("permitDetailsId");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getPermitInfoDtls(permitDetailsId);
		
		
		buffer.append("<xml-response>");
			buffer.append("<permitID>" +dto.getPermitID()+ "</permitID>");
			buffer.append("<driverName>" +dto.getDriverName()+ "</driverName>");
			buffer.append("<identificationMarks>" +dto.getIdentificationMarks()+ "</identificationMarks>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<phone>" +dto.getPhone()+ "</phone>");
			buffer.append("<registrationNo>" +dto.getRegistrationNo()+ "</registrationNo>");
			buffer.append("<region>" +dto.getRegion()+ "</region>");
			buffer.append("<baseoffice>" +dto.getBaseoffice()+ "</baseoffice>");
			buffer.append("<routeBetween>" +dto.getRouteBetween()+ "</routeBetween>");
			buffer.append("<to>" +dto.getTo()+ "</to>");
			buffer.append("<vehicleType>" +dto.getVehicleType()+ "</vehicleType>");
			buffer.append("<carryingCapacity>" +dto.getCarryingCapacity()+ "</carryingCapacity>");
			buffer.append("<seatCapacity>" +dto.getSeatCapacity()+ "</seatCapacity>");
			buffer.append("<receiptNo>" +dto.getReceiptNo()+ "</receiptNo>");
			buffer.append("<dateOfissue>" +dto.getDateOfissue()+ "</dateOfissue>");
			buffer.append("<validity>" +dto.getValidUpto()+ "</validity>");
			buffer.append("<remarks>" +dto.getRemarks()+ "</remarks>");
		buffer.append("</xml-response>");
		
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getLearnerLicenseInfoList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		response.setContentType("text/xml");
		String searchType = request.getParameter("searchType");
		String learnerlicenseInfoId = request.getParameter("learnerlicenseInfoId");
		EralisCommonDTO dto = CommonBusiness.getInstance().getlearnerRenewalInfoDtls(learnerlicenseInfoId,searchType);
		buffer.append("<xml-response>");
				buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
				buffer.append("<name>" +dto.getName()+ "</name>");
				buffer.append("<bloodgroup>" +dto.getBloodGroup()+ "</bloodgroup>");
				buffer.append("<dob>" +dto.getDOB()+ "</dob>");
				buffer.append("<country>"+dto.getCountry()+"</country>");
				buffer.append("<village>"+dto.getVillage()+"</village>");
				buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
				buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
				buffer.append("<address>" +dto.getAddress()+ "</address>");
				buffer.append("<learnerlicenseNo>" +dto.getLLNo() + "</learnerlicenseNo>");
				buffer.append("<region>" +dto.getRegionId()+ "</region>");
				buffer.append("<region-name>" +dto.getRegion()+ "</region-name>");
				buffer.append("<expirydate>" +dto.getExpiryDate()+ "</expirydate>");
				buffer.append("<issuedate>" +dto.getDateOfissue()+ "</issuedate>");
				buffer.append("<status>" +dto.getStatus()+ "</status>");
				buffer.append("<learnerLicenseId>" +dto.getLearnerLicenseId()+ "</learnerLicenseId>");
				buffer.append("<isInternational>" +dto.getIsInternational()+ "</isInternational>");
				buffer.append("<personalInfoId>" +dto.getPersonalInfoId()+ "</personalInfoId>");
				buffer.append("<CID>" +dto.getCID()+ "</CID>");
				buffer.append("<phoneNo>" +dto.getPhone()+ "</phoneNo>");
				buffer.append("<gender>" +dto.getGender()+ "</gender>");
				buffer.append("<fatherName>" +dto.getFathersName()+ "</fatherName>");
				buffer.append("<cancellationDate>" +dto.getCancellationDate()+ "</cancellationDate>");
				buffer.append("<cancellationReason>" +dto.getCancellationReason()+ "</cancellationReason>");

				buffer.append("<amount>" +dto.getAmount()+ "</amount>");
				buffer.append("<receiptDate>" +dto.getReceiptDate()+ "</receiptDate>");
				buffer.append("<receiptno>" +dto.getReceiptNo()+ "</receiptno>");
				
		buffer.append("</xml-response>");
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getlicenserenewalInfoList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String drivinglicenseId = request.getParameter("drivinglicenseId");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getlicenserenewalDtls(drivinglicenseId);
		
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +dto.getName()+ "</name>");
			buffer.append("<bloodgroup>" +dto.getBloodGroup()+ "</bloodgroup>");
			buffer.append("<dob>" +dto.getDOB()+ "</dob>");
			buffer.append("<cid>" +dto.getCID()+ "</cid>");
			buffer.append("<gender>" +dto.getGender()+ "</gender>");
			buffer.append("<fathersname>" +dto.getFathersName()+ "</fathersname>");
			buffer.append("<village>"+dto.getVillage()+"</village>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<licenseno>" +dto.getLicenseNo()+ "</licenseno>");
			buffer.append("<region>" +dto.getRegion()+ "</region>");
				
		buffer.append("</xml-response>");
		
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getlearnerlicenserenewalList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String learnerlicneseno = request.getParameter("learnerlicneseno");
		
		LicenseDTO dto = CommonBusiness.getInstance().getlearnerlicenserenewalList(learnerlicneseno);
		
		
		buffer.append("<xml-response>");
		buffer.append("<receiptdate>" +dto.getReceiptDate() + "</receiptdate>");
		buffer.append("<expirydate>" +dto.getExpiryDate() + "</expirydate>");
		
		buffer.append("</xml-response>");
		
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getSuspensionDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
 		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String drivinglicenseId = request.getParameter("drivinglicenseId");
		String searchType = request.getParameter("searchType");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getSuspensionDtls(drivinglicenseId,searchType);
		
		buffer.append("<xml-response>");
			buffer.append("<name>" +dto.getName()+ "</name>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<type>" +dto.getType()+ "</type>");
			buffer.append("<region>" +dto.getRegionId()+ "</region>");
			buffer.append("<region-name>" +dto.getRegion()+ "</region-name>");
			buffer.append("<citizenID>" +dto.getCID()+ "</citizenID>");
			buffer.append("<phoneNo>" +dto.getPhone()+ "</phoneNo>");
			buffer.append("<status>" +dto.getStatus()+ "</status>");
			buffer.append("<courtesy>" +dto.getTitleOfcourtesy()+ "</courtesy>");
			buffer.append("<occupation>" +dto.getOccupation()+ "</occupation>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<bloodgroup>" +dto.getBloodGroup()+ "</bloodgroup>");
			buffer.append("<bloodgroupId>" +dto.getBloodGroupId()+ "</bloodgroupId>");
			buffer.append("<dob>" +dto.getDOB()+ "</dob>");
			buffer.append("<nationality>" +dto.getNationality()+ "</nationality>");
			buffer.append("<fathersname>"+dto.getFathersName()+"</fathersname>");
			buffer.append("<identificationMarks>"+dto.getIdentificationMarks()+"</identificationMarks>");
			buffer.append("<remarks>"+dto.getRemarks()+"</remarks>");
			buffer.append("<gender>"+dto.getGender()+"</gender>");
			buffer.append("<country>"+dto.getCountry()+"</country>");
			buffer.append("<village>"+dto.getVillage()+"</village>");
			buffer.append("<learnerlicenseno>"+dto.getLLNo()+"</learnerlicenseno>");
			buffer.append("<expirydate>" +dto.getExpiryDate()+ "</expirydate>");
			buffer.append("<issuedate>" +dto.getDateOfissue()+ "</issuedate>");
			buffer.append("<licenseNO>"+dto.getLicenseNo()+"</licenseNO>");
			buffer.append("<applicationAge>"+dto.getCommercialApplicationAge()+"</applicationAge>");
			buffer.append("<licensestatus>" +dto.getLicenseStatus()+ "</licensestatus>");
			buffer.append("<licensetype>"+dto.getLicensetype()+"</licensetype>");
			buffer.append("<drivingLicenseId>"+dto.getDrivinglicenseId()+"</drivingLicenseId>");
			buffer.append("<isInternational>"+dto.getIsInternational()+"</isInternational>");
			buffer.append("<learnerLicenseId>"+dto.getLearnerLicenseId()+"</learnerLicenseId>");
			
			buffer.append("<firstName>"+dto.getFirstname()+"</firstName>");
			buffer.append("<middleName>"+dto.getMiddlename()+"</middleName>");
			buffer.append("<lastName>"+dto.getLastName()+"</lastName>");
			buffer.append("<lastName>"+dto.getPersonalInfoId()+"</lastName>");
			buffer.append("<age>"+dto.getAge()+"</age>");
			

			buffer.append("<totalDuration>"+dto.getTotalDuration()+"</totalDuration>");
			buffer.append("<dateDifference>"+dto.getDateDifference()+"</dateDifference>");

			buffer.append("<amount>"+dto.getAmount()+"</amount>");
			buffer.append("<receiptDate>"+dto.getReceiptDate()+"</receiptDate>");
			buffer.append("<receiptNo>"+dto.getReceiptNo()+"</receiptNo>");
			
			/*TOOLS START*/

			buffer.append("<CID>"+dto.getCID()+"</CID>");
			buffer.append("<personalInfoId>"+dto.getPersonalInfoId()+"</personalInfoId>");

			buffer.append("<village>"+dto.getVillage()+"</village>");
			buffer.append("<licensePunchNo>"+dto.getLicensePunchNo()+"</licensePunchNo>");
			buffer.append("<cancelledDate>"+dto.getCancellationDate()+"</cancelledDate>");
			buffer.append("<cancelReason>"+dto.getReason()+"</cancelReason>");
			buffer.append("<ministryId>"+dto.getMinistry()+"</ministryId>");
			buffer.append("<departmentId>"+dto.getDepartment()+"</departmentId>");
			buffer.append("<vehicleNo>"+dto.getVehicleNo()+"</vehicleNo>");
			buffer.append("<vehicle_type>"+dto.getVehicleType()+"</vehicle_type>");
			
			
			/*TOOLS END*/
			
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	//deepak
	//getpermitDetails
	
	//getaccidentDetails
	
	protected void getaccidentDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
 		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String vehicleNo = request.getParameter("vehicleNo");
		String vehicleType = request.getParameter("vehicleType");
		
		VehicleDTO dto = CommonBusiness.getInstance().getaccidentDetails(vehicleNo,vehicleType);
		//getpermitDetails
		
		
		buffer.append("<xml-response>");
		buffer.append("<Vehicle_Model_Id>" +dto.getVehicleModel()+ "</Vehicle_Model_Id>");
		buffer.append("<Manufacture_Year>" +dto.getManufactureYear()+ "</Manufacture_Year>");
		buffer.append("<Vehicle_Registration_Id>" +dto.getVehicleRegistrationId()+ "</Vehicle_Registration_Id>");

		
	    buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	//getaccidentPersonalDetails
	
	protected void getaccidentPersonalDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
 		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String licenseNo = request.getParameter("licenseNo");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getaccidentPersonalDetails(licenseNo);
		//getpermitDetails
		
		
		buffer.append("<xml-response>");
		buffer.append("<NAME>" +dto.getName()+ "</NAME>");
		//buffer.append("<Manufacture_Year>" +dto.getAge()+ "</Manufacture_Year>");
		buffer.append("<dob>" +dto.getDOB()+ "</dob>");
		buffer.append("<age>" +dto.getAge()+ "</age>");
		buffer.append("<Gender>" +dto.getGender()+ "</Gender>");
		buffer.append("<Present_Phone_No>" +dto.getPhone()+ "</Present_Phone_No>");
		buffer.append("<Nationality_Id>" +dto.getNationality()+ "</Nationality_Id>");
		
		
	    buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getpermitDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
 		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		response.setContentType("text/xml");
		String permitDetailsId = request.getParameter("permitDetailsId");
		String searchType = request.getParameter("searchType");
		EralisCommonDTO dto = CommonBusiness.getInstance().getpermitDetails(permitDetailsId,searchType);
		
		buffer.append("<xml-response>");
		buffer.append("<permitDetailsId>" +dto.getPermitDetailsId()+ "</permitDetailsId>");
		buffer.append("<licenseNo>" +dto.getLicenseNo()+ "</licenseNo>");
		buffer.append("<registrationNo>" +dto.getRegistrationNo()+ "</registrationNo>");
		buffer.append("<permitNo>" +dto.getPermitIDNo()+ "</permitNo>");
		buffer.append("<name>" +dto.getName()+ "</name>");
		buffer.append("<address>" +dto.getAddress()+ "</address>");
		buffer.append("<phoneNo>" +dto.getPhone()+ "</phoneNo>");
		buffer.append("<routeBetween>" +dto.getRouteBetween()+ "</routeBetween>");
		buffer.append("<routeTo>"+dto.getTo()+"</routeTo>");
		buffer.append("<vehicleType>" +dto.getVehicleType()+ "</vehicleType>");
		buffer.append("<carryingCapacity>"+dto.getCarryingCapacity()+"</carryingCapacity>");
		buffer.append("<engineCC>"+dto.getEngineCC()+"</engineCC>");
		buffer.append("<seatCapacity>" +dto.getSeatCapacity()+ "</seatCapacity>");
		buffer.append("<issuedate>" +dto.getDateOfissue()+ "</issuedate>");
		buffer.append("<validity>" +dto.getValidUpto()+ "</validity>");
		
		buffer.append("<regionId>" +dto.getRegionId()+ "</regionId>");
		buffer.append("<baseoffice>" +dto.getBaseoffice()+ "</baseoffice>");
		buffer.append("<journeyPurpose>" +dto.getJourneyPurpose()+ "</journeyPurpose>");
		buffer.append("<remarks>" +dto.getRemarks()+ "</remarks>");
		
	    buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getlearnerDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String learnerlicenseinfoid = request.getParameter("learnerlicenseinfoid");
		String searchType = request.getParameter("searchType");
		
		EralisCommonDTO dto = CommonBusiness.getInstance().getlearnerDtls(learnerlicenseinfoid,searchType);
		
		buffer.append("<xml-response>");
			buffer.append("<reason>" +dto.getReason()+ "</reason>");
			buffer.append("<name>" +dto.getName()+ "</name>");
			buffer.append("<age>" +dto.getAge()+ "</age>");
			buffer.append("<dzongkhag>" +dto.getDzongkhag()+ "</dzongkhag>");
			buffer.append("<gewog>" +dto.getGewog()+ "</gewog>");
			buffer.append("<address>" +dto.getAddress()+ "</address>");
			buffer.append("<region>" +dto.getRegion()+ "</region>");
			buffer.append("<citizenID>" +dto.getCID()+ "</citizenID>");
			buffer.append("<courtesy>" +dto.getTitleOfcourtesy()+ "</courtesy>");
			buffer.append("<occupation>" +dto.getOccupation()+ "</occupation>");
			buffer.append("<customerId>" +dto.getCustomerID()+ "</customerId>");
			buffer.append("<bloodgroup>" +dto.getBloodGroup()+ "</bloodgroup>");
			buffer.append("<bloodgroupId>" +dto.getBloodGroupId()+ "</bloodgroupId>");
			buffer.append("<dob>" +dto.getDOB()+ "</dob>");
			buffer.append("<nationality>" +dto.getNationality()+ "</nationality>");
			buffer.append("<fathersname>"+dto.getFathersName()+"</fathersname>");
			buffer.append("<identificationMarks>"+dto.getIdentificationMarks()+"</identificationMarks>");
			buffer.append("<remarks>"+dto.getRemarks()+"</remarks>");
			buffer.append("<gender>"+dto.getGender()+"</gender>");
			buffer.append("<country>"+dto.getCountry()+"</country>");
			buffer.append("<village>"+dto.getVillage()+"</village>");
			buffer.append("<learnerlicenseno>"+dto.getLLNo()+"</learnerlicenseno>");
			buffer.append("<CID>"+dto.getCID()+"</CID>");
			buffer.append("<personalInfoId>"+dto.getPersonalInfoId()+"</personalInfoId>");
			buffer.append("<licenseNO>"+dto.getLicenseNo()+"</licenseNO>");
			buffer.append("<validity>"+dto.getLicenseApplicationAge()+"</validity>");
			buffer.append("<testmarks>"+dto.getOverallObtainedMark()+"</testmarks>");
			buffer.append("<learnerLicenseId>"+dto.getLearnerLicenseId()+"</learnerLicenseId>");
			buffer.append("<isInternational>"+dto.getIsInternational()+"</isInternational>");
			buffer.append("<driveTypeName>"+dto.getDriverName()+"</driveTypeName>");
			buffer.append("<status>" +dto.getStatus()+ "</status>");
			

			buffer.append("<driveTypeId>"+dto.getDrivetype()+"</driveTypeId>");
			buffer.append("<Theory_Marks_Obtained>" +dto.getTheoryMarks()+ "</Theory_Marks_Obtained>");
			buffer.append("<Practical_Marks_Obtained>" +dto.getPracticalMarks()+ "</Practical_Marks_Obtained>");
			buffer.append("<Issue_Type>" +dto.getIssueType()+ "</Issue_Type>");
			
			
			
			

		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getTestDate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		
		String jsonText = "";
		Gson gson = new Gson();
		List<EralisCommonDTO> dropDownList = new ArrayList<EralisCommonDTO>();
		
		String locationId = request.getParameter("locationId");
		String jurisTypeId = request.getParameter("jurisTypeId");
		String issueType = request.getParameter("issueType");
		
		try 
		{
			dropDownList = CommonBusiness.getInstance().getTestDate(locationId,jurisTypeId,issueType);
			jsonText = gson.toJson(dropDownList);
		} 
		catch (Exception e)
		{
			Log.error("JSonDataLoader:populateDropDown() -> exception ->" + e);
		}
		
		jsonText = new String(jsonText.getBytes("UTF-8"), "UTF-8");
		out.println(jsonText);
		out.flush();
	}
	
	protected void getMaxApplicants(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		ApplicationDataVO appVO = new ApplicationDataVO();
		response.setContentType("text/xml");
		
		String testJurisTypeId = request.getParameter("testJurisTypeId");
		String testDate = request.getParameter("testDate");
		String testLocation = request.getParameter("testLocation");
		String issueType = request.getParameter("issueType");
		String learnerNo = request.getParameter("learnerNo");
		String customerId = request.getParameter("customerId");
		String searchBy = request.getParameter("searchBy");
		
		appVO = CommonBusiness.getInstance().getMaxApplicants(testDate, testLocation,issueType,learnerNo,customerId,testJurisTypeId,searchBy);
		
		buffer.append("<xml-response>");
			buffer.append("<maturity>"+appVO.getMessage()+"</maturity>");
			buffer.append("<flag>"+appVO.getFlag()+"</flag>");
			buffer.append("<applicantNo>"+appVO.getApplicants()+"</applicantNo>");
			buffer.append("<max-applicant>"+appVO.getMaxApplicants()+"</max-applicant>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getLicenseInformation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String licenseNo = request.getParameter("licenseNo");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setVehicleNumber(licenseNo);
		appVO = CommonBusiness.getInstance().getLicenseInformation(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getLearnerInformation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String licenseNo = request.getParameter("licenseNo");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setLicenseno(licenseNo);
		appVO = CommonBusiness.getInstance().getLearnerInformation(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void getLicenseInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String licenseNo = request.getParameter("licenseNo");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setLicenseno(licenseNo);
		appVO = CommonBusiness.getInstance().getLicenseInfo(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<licensestatus>"+appVO.getStatus()+"</licensestatus>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getcheckCID(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String cidno = request.getParameter("cid");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setCidNumber(cidno);
		appVO = CommonBusiness.getInstance().getcheckCID(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getchecklicense(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String learnerlicenseno = request.getParameter("cid");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setLicenseno(learnerlicenseno);
		appVO = CommonBusiness.getInstance().getchecklicense(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void getcheckcommerciallicense(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String learnerlicenseno = request.getParameter("cid");
		
		ApplicationDataVO appVO = new ApplicationDataVO();
		appVO.setLicenseno(learnerlicenseno);
		appVO = CommonBusiness.getInstance().getcheckcommerciallicense(appVO);
		
		buffer.append("<xml-response>");
			buffer.append("<status>"+appVO.getStatus()+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void generateTOPNoFormat(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String regionId = request.getParameter("regionId");
		
		String topNo = CommonBusiness.getInstance().generateTOPNoFormat(regionId);
		
		buffer.append("<xml-response>");
		buffer.append("<top-no>"+topNo+"</top-no>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	/*protected void getDriveTypeTestMarks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String testDate = request.getParameter("testDate");
		String driveTypeId = request.getParameter("driveTypeId");
		String licenseNO = request.getParameter("licenseNO");
		String customerID = request.getParameter("customerID");
		LicenseDTO dto = CommonBusiness.getInstance().getDriveTypeTestMarks(driveTypeId,licenseNO,testDate,customerID);
		
		buffer.append("<xml-response>");
		buffer.append("<flag>"+dto.getFlag()+"</flag>");
		buffer.append("<testMark>"+dto.getTestmarks()+"</testMark>");
		buffer.append("<testStatus>"+dto.getStatus()+"</testStatus>");
		buffer.append("<reason>"+dto.getReason()+"</reason>");
		buffer.append("<testReason>"+dto.getReason()+"</testReason>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}*/
	
	protected void getDriveTypeTestMarks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		
		String issueType = request.getParameter("issueType");
		String testDate = request.getParameter("testDate");
		String driveTypeId = request.getParameter("driveTypeId");
		String licenseNO = request.getParameter("licenseNO");
		String customerID = request.getParameter("customerID");
		LicenseDTO dto = CommonBusiness.getInstance().getDriveTypeTestMarks(driveTypeId,licenseNO,testDate,customerID,issueType);
		
		buffer.append("<xml-response>");
		buffer.append("<flag>"+dto.getFlag()+"</flag>");
		buffer.append("<testMark>"+dto.getTestmarks()+"</testMark>");
		buffer.append("<testStatus>"+dto.getStatus()+"</testStatus>");
		buffer.append("<reason>"+dto.getReason()+"</reason>");
		buffer.append("<testReason>"+dto.getReason()+"</testReason>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void testValidation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String driveTypeId = request.getParameter("driveTypeId");
		String drivinglicenseId = request.getParameter("drivinglicenseId");
		String customerID = request.getParameter("customerID");
		String testDate = request.getParameter("testDate");
		
		LicenseDTO dto = CommonBusiness.getInstance().testValidation(driveTypeId,drivinglicenseId,customerID,testDate);
		
		buffer.append("<xml-response>");
		buffer.append("<testStatus>"+dto.getStatus()+"</testStatus>");
		buffer.append("<testReason>"+dto.getReason()+"</testReason>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void validateCommercialCriteria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");

		String driveTypeId = request.getParameter("driveTypeId");
		String drivinglicenseId = request.getParameter("drivinglicenseId");
		String customerID = request.getParameter("customerID");
		String testDate = request.getParameter("testDate");
		
		LicenseDTO dto = CommonBusiness.getInstance().validateCommercialCriteria(driveTypeId,drivinglicenseId,customerID,testDate);
		
		buffer.append("<xml-response>");
		buffer.append("<testStatus>"+dto.getStatus()+"</testStatus>");
		buffer.append("<testReason>"+dto.getReason()+"</testReason>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	protected void validateNonCommercialCriteria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		String issueType	=	 request.getParameter("issueType");
		String driveTypeId = request.getParameter("driveTypeId");
		String learnerLicenseId = request.getParameter("learnerLicenseId");
		LicenseDTO dto = CommonBusiness.getInstance().validateNonCommercialCriteria(driveTypeId,learnerLicenseId,issueType);
		
		buffer.append("<xml-response>");
		buffer.append("<testMark>"+dto.getTestmarks()+"</testMark>");
		buffer.append("<testStatus>"+dto.getStatus()+"</testStatus>");
		buffer.append("<testReason>"+dto.getReason()+"</testReason>");
		buffer.append("<testResult>"+dto.getResult()+"</testResult>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void checkIfExists(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		String licenseNo	=	 request.getParameter("licenseNo");
		String status = CommonBusiness.getInstance().checkIfExists(licenseNo);
		
		buffer.append("<xml-response>");
		buffer.append("<status>"+status+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	protected void validateOTP(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		
		response.setContentType("text/xml");
		String cid = request.getParameter("cid");
		String enteredOTP	=	 request.getParameter("otp");
		String status = CommonBusiness.getInstance().validateOTP(cid, enteredOTP);
		
		buffer.append("<xml-response>");
		buffer.append("<status>"+status+"</status>");
		buffer.append("</xml-response>");
		
		out.println(buffer);
		out.flush();
		out.close();
	}


	protected void getVehicleModelDtls(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		try 
		{
			response.setContentType("text/xml");
			String q = request.getParameter("q");
			if(q == null){
				return;
			}
			EralisCommonDTO dto = new EralisCommonDTO();
			String vehicleModelId = request.getParameter("vehicleModelId");
			dto = VehicleBusiness.getInstance().getVehicleModelDtls(vehicleModelId);

			buffer.append("<xml-response>");
			buffer.append("<engineCC>" + dto.getEngineCC() + "</engineCC>");
			buffer.append("<seatCapacity>" + dto.getSeatCapacity() + "</seatCapacity>");
			buffer.append("<unladenWeight>" + dto.getUnladenWeight() + "</unladenWeight>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}

	protected void getLicensAndLearnerDtls(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		try 
		{
			response.setContentType("text/xml");
			EralisCommonDTO dto = new EralisCommonDTO();
			String licenseNo = request.getParameter("licenseNo");
			dto = VehicleBusiness.getInstance().getLicensAndLearnerDtls(licenseNo);

			buffer.append("<xml-response>");
			
			buffer.append("<name>" + dto.getName() + "</name>");
			buffer.append("<cid>" + dto.getCID() + "</cid>");
			buffer.append("<dob>" + dto.getDOB() + "</dob>");
			buffer.append("<mobileNo>" + dto.getMobileNo() + "</mobileNo>");
			buffer.append("<gender>" + dto.getGender() + "</gender>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}

	protected void getVehicleDtls(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		try 
		{
			response.setContentType("text/xml");
			EralisCommonDTO dto = new EralisCommonDTO();
			String vehicleNo = request.getParameter("vehicleNo");
			String vehicleType = request.getParameter("vehicleType");
			dto = VehicleBusiness.getInstance().getVehicleDtls(vehicleNo,vehicleType);

			buffer.append("<xml-response>");
				buffer.append("<model>" + dto.getModel() + "</model>");
				buffer.append("<company>" + dto.getCompany() + "</company>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}
	
	protected void getVehiclePreviousPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		try 
		{
			EralisCommonDTO dto = new EralisCommonDTO();

			response.setContentType("text/xml");
			
			String viewRenewalDurations = request.getParameter("viewRenewalDurations");
			String vehicleId = request.getParameter("vehicleId");
			
			dto = VehicleBusiness.getInstance().getVehiclePreviousPayment(vehicleId,viewRenewalDurations);

			buffer.append("<xml-response>");
			buffer.append("<amount>" +dto.getAmount() + "</amount>");
			buffer.append("<duration>" +dto.getRenewalDate() + "</duration>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}

	protected void validateChasisEngineNo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ERALISException, ERALISSystemException
	{
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		try 
		{
			response.setContentType("text/xml");
			EralisCommonDTO dto = new EralisCommonDTO();
			String engineNumber = request.getParameter("engineNumber");
			String chasisNumber = request.getParameter("chasisNumber");
			dto = VehicleBusiness.getInstance().validateChasisEngineNo(engineNumber,chasisNumber);

			buffer.append("<xml-response>");
				buffer.append("<engineNoStatus>" + dto.getEngineNoStatus() + "</engineNoStatus>");
				buffer.append("<chasisStatus>" + dto.getChasisStatus() + "</chasisStatus>");
			buffer.append("</xml-response>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		out.println(buffer);
	}
	
}
