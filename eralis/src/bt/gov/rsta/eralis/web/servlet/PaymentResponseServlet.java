package bt.gov.rsta.eralis.web.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.eralis.web.action.LoadApplicationBusiness;
import bt.gov.rsta.framework.business.ServiceBusiness;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

/**
 * Servlet implementation class PaymentResponseServlet
 */
public class PaymentResponseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;  
         
    /**  
     * @see HttpServlet#HttpServlet()
     */
    public PaymentResponseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doAction(request, response);
		} catch (ERALISException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}*/
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doAction(request, response);
		} catch (ERALISException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException, ERALISException, ERALISSystemException {
		
		String applicationNo = request.getParameter("applicationNo"); 
		String paymentDate = request.getParameter("paymentDate");
		String txnId = request.getParameter("txnId");
		String txnAmount = request.getParameter("txnAmount");
		String paymentStatus = request.getParameter("paymentStatus");
		System.out.println(applicationNo+"?"+paymentDate+"?"+paymentDate+"?"+txnAmount+"?"+paymentStatus);
 
		try {
			String result	=	ServiceBusiness.getInstance().paymentResponse(applicationNo, paymentDate, txnId, txnAmount, paymentStatus);
			String[] tempArray = result.split("#");
			String responseStr	= tempArray[0];
			String service 		= tempArray[1];
			String refrenceNo 	= tempArray[2];
			String moduleNo 	= tempArray[3];
			String vehicleType 	= tempArray[4];
			String customerName = tempArray[5];
			String validity 	= tempArray[6];
			String serviceType	= tempArray[7]; 
			String serviceShortCode	= tempArray[8];
			String moduleNo1	= null;
			  
			if(!responseStr.equalsIgnoreCase("ALREADY_APPLIED") && !responseStr.equalsIgnoreCase("FAILURE")) 
			{
				UserDetailsVO userVo = new UserDetailsVO();
				userVo.setActorId("Citizen");
				userVo.setActorName("Citizen");
				userVo.setRoleId("0");
				userVo.setRoleName("Citizen");
				userVo.setRoleCode("Citizen");
				
				if(service.contains("Learner License Renewal"))
				{
					LicenseDTO dto=new LicenseDTO();
					dto.setApplicationNo(applicationNo);
					dto = LoadApplicationBusiness.getInstance().getlicenserenewalList(dto);
					String approvalStatus = LicenseBusiness.getInstance().license_renewal_application_approval(dto, userVo);
				}
				else if(service.contains("Driving License Renewal"))
				{
					LicenseDTO dto=new LicenseDTO();
					dto.setApplicationNo(applicationNo);
					dto = LoadApplicationBusiness.getInstance().getlicenseRenewalList(dto);
					String approvalStatus = LicenseBusiness.getInstance().driving_license_renewal_application_approval(dto, userVo);
				}
				else if(service.contains("Vehicle Renewal"))
				{
					VehicleDTO dto = new VehicleDTO();
					dto.setApplicationNumber(applicationNo);
					dto = LoadApplicationBusiness.getInstance().getvehicleRenewalList(dto, "APPROVE");
					String approvalStatus = VehicleBusiness.getInstance().renewal_application_approval(dto, userVo);
				}
			}
			
			
			if(serviceType.equalsIgnoreCase("Offence"))
			{
				String[] moduleArray = moduleNo.split("~");
				moduleNo = moduleArray[0];
				moduleNo1= moduleArray[1];
			}
			
			paymentDate	= tempArray[8];
			String receiptNumber = tempArray[9];
			String location = tempArray[10];
			String testDate = tempArray[11];
			
			HttpSession session = request.getSession();
			session.setAttribute("RESPONSE_STR", responseStr);
			session.setAttribute("Service", service);
			session.setAttribute("APPLICATION_NO", applicationNo);
			session.setAttribute("PAYMENT_DATE", paymentDate);
			session.setAttribute("RECEIPT_NO", txnId);
			session.setAttribute("TXN_AMOUNT", txnAmount);
			session.setAttribute("REFRENCE_NO", refrenceNo);
			session.setAttribute("MODULE_NO", moduleNo);
			session.setAttribute("VEHICLE_TYPE", vehicleType);
			session.setAttribute("CUSTOMER_NAME", customerName);
			session.setAttribute("VALIDITY", validity); 
			session.setAttribute("SERVICE_TYPE", serviceType);
			session.setAttribute("MODULE_NO1", moduleNo1);
			session.setAttribute("RECEIPT_SEQUENCE", receiptNumber);
			session.setAttribute("LOCATION", location);
			session.setAttribute("TEST_DATE", testDate);
			
			RequestDispatcher rd = request.getRequestDispatcher("/pages/payment/acknowledgement-receipt.jsp");
			rd.forward(request, response);
			
		} catch (ERALISException e) {
			e.printStackTrace();
		} catch (ERALISSystemException e) {
			e.printStackTrace();
		}
		
	}

		
		

}
