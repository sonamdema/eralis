package bt.gov.rsta.eralis.web.servlet;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.pdf.Barcode39;

/**
 * Servlet implementation class Barcode
 */
public class Barcode extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Barcode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String strBarCode = request.getParameter("data");

		byte[] pngImageData = null;

		try {
			Barcode39 code39ext = new Barcode39();
			code39ext.setCode(strBarCode);
			code39ext.setStartStopText(false);
			code39ext.setExtended(true);
			java.awt.Image rawImage = code39ext.createAwtImage(Color.BLACK, Color.WHITE);
			BufferedImage outImage = new BufferedImage(rawImage.getWidth(null), rawImage.getHeight(null), BufferedImage.TYPE_INT_RGB);
			outImage.getGraphics().drawImage(rawImage, 0, 0, null);
			ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
			ImageIO.write(outImage, "png", bytesOut);
			bytesOut.flush();
			pngImageData = bytesOut.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (pngImageData != null) {
			response.setContentLength(pngImageData.length);
			response.setContentType("image/png");
			OutputStream out = response.getOutputStream();
			out.write(pngImageData);
			out.flush();
			out.close();
		} else {
			response.sendRedirect("/images/nophoto.jpg");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
