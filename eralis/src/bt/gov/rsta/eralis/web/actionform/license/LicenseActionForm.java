package bt.gov.rsta.eralis.web.actionform.license;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import bt.gov.rsta.eralis.dto.license.OffenceDTO;

/**
 * @author
 *
 */
public class LicenseActionForm extends ActionForm
{
	private static final long serialVersionUID = 1L;
	
	public LicenseActionForm()
	{
		offenceList = new ArrayList<OffenceDTO>();
		offenceList.add(new OffenceDTO());
		offenceListB = new ArrayList<OffenceDTO>();
		offenceListB.add(new OffenceDTO());
		offenceListF = new ArrayList<OffenceDTO>();
		offenceListF.add(new OffenceDTO());
		
		
	}
	
	
	
	private String CID;
	private String LLNo;
	private String region;
	private String issuedate;
	private String receiptNo;
	private String expiryDate;
	private String receiptDate;
	private String certifyingDoctor;
	private String remarks;
	private String driveType;
	private String roadroller;
	private String powertiller;
	private String crane;
	private String TMB;
	private String TLV;
	private String THB;
	private String roadPaver;
	private String LV;
	private String HV;
	private String TW;
	private String MV;
	private String excavator;
	private String bullDozer;
	private String payLoader;
	private String tractor;
	private String renewaldate;
	private String deliveryon;
	private String licenseNo;

	private FormFile offence1;
	private FormFile offence2;
	private FormFile offence3;
	private FormFile offence4;
	private FormFile offence5;
	private FormFile offence6;
	
	private List<OffenceDTO> offenceList;
	private List<OffenceDTO> offenceListB;
	private List<OffenceDTO> offenceListF;
	private String offencedate;
	private String timeofInspection;
	private String inspectedby;
	private String inspectedType;
	private String trafficbranch;
	private String placeofInspection;
	private String learnerlicense;
	private String bluebook;
	private String TINno;
	private String receivedfrom;
	private String receivedDate;
	
	private String learnerlicenseNo;
	private String name;
	private String customerId;
	private String occupation;
	private String DOB;
	private String identificationmarks;
	private String bloodgroup;
	private String fathersname;
	private String dzongkhag;
	private String gewog;
	private String village;
	private String country;
	private String address;
	private String dlNo;
	private String iid;
	private String testmarks;
	private String registerNo;
	private String drivetype;
	private String deliveredon;
	private String gender;
	private String nationality;
	private String autogenerate;
	private String deliveredOn;
	private String status;
	private String taxi;
	private String MB;
	private String HB;
	private String mc;
	private String penalty;
	private String duplicationdate;
	private String cancellationdate;
	private String cancellationReason;
	private String reissuedate;
	private String isReissue;
	private String startDate;
	private String endDate;
	private String reason;
	private String vehicleNo;
	private String issuedTo;
	private String endorseddate;
	private String pageId;
	private String learnerlicenseno;
	private String applicationNo;
	private String appsubmissiondate;
	private String regionId;
	private String receiptDate1;
	private String licenseTypeId;
	private String offenceLicesneType;
	private String learnerlicenseinfoid;
	private String nextExpiry;
	private FormFile fileMC;
	private FormFile fileApplForm;
	private FormFile supportDoc;
	private FormFile rcCertificate;
	private String exactLocation;
	private String licensetype;
	private String topNo;
	private String amount;
	private String learnerLicenseId;
	private String drivinglicenseId;
	private String vehicleId;
	private String serviceId;
	private String renewalDuration;
	private String issueType;
	private String[] drivetypeTCB;
	private String tcbEndorsement;
	private String withdrawnDate;
	private String withdrawnReason;
	private FormFile imgPath;

	private String TINnoFor;
	private String offencedateFor;
	private String timeofInspectionFor;
	private String inspectedbyFor;
	private String inspectedTypeFor;
	private String placeofInspectionFor;
	private String vehicleNoFor;
	private String licenseNoFor;
	private String ownerNameFor;
	private String diverNameFor;
	private String mobileNoFor;
	private String remarksFor;
	private String trafficbranchFor;
	private String regionfor;

	private String TINnoP;
	private String offencedateP;
	private String timeofInspectionP;
	private String inspectedbyP;
	private String inspectedTypeP;
	private String placeofInspectionP;
	private String vehicleNoP;
	private String licenseNoP;
	private String ownerNameP;
	private String diverNameP;
	private String mobileNoP;
	private String remarksP;
	private String trafficbranchP;
	private String regionP;
	private String vehicleType;
	private String mobile;
	private String tinPrefix;
	
	
	
	public String getTinPrefix() {
		return tinPrefix;
	}
	public void setTinPrefix(String tinPrefix) {
		this.tinPrefix = tinPrefix;
	}
	public FormFile getOffence3() {
		return offence3;
	}
	public void setOffence3(FormFile offence3) {
		this.offence3 = offence3;
	}
	public FormFile getOffence4() {
		return offence4;
	}
	public void setOffence4(FormFile offence4) {
		this.offence4 = offence4;
	}
	public FormFile getOffence5() {
		return offence5;
	}
	public void setOffence5(FormFile offence5) {
		this.offence5 = offence5;
	}
	public FormFile getOffence6() {
		return offence6;
	}
	public void setOffence6(FormFile offence6) {
		this.offence6 = offence6;
	}
	public List<OffenceDTO> getOffenceListF() {
		return offenceListF;
	}
	public void setOffenceListF(List<OffenceDTO> offenceListF) {
		this.offenceListF = offenceListF;
	}
	public List<OffenceDTO> getOffenceListB() {
		return offenceListB;
	}
	public void setOffenceListB(List<OffenceDTO> offenceListB) {
		this.offenceListB = offenceListB;
	}
	public String getRegionP() {
		return regionP;
	}
	public void setRegionP(String regionP) {
		this.regionP = regionP;
	}
	public String getTINnoP() {
		return TINnoP;
	}
	public void setTINnoP(String tINnoP) {
		TINnoP = tINnoP;
	}
	public String getOffencedateP() {
		return offencedateP;
	}
	public void setOffencedateP(String offencedateP) {
		this.offencedateP = offencedateP;
	}
	public String getTimeofInspectionP() {
		return timeofInspectionP;
	}
	public void setTimeofInspectionP(String timeofInspectionP) {
		this.timeofInspectionP = timeofInspectionP;
	}
	public String getInspectedbyP() {
		return inspectedbyP;
	}
	public void setInspectedbyP(String inspectedbyP) {
		this.inspectedbyP = inspectedbyP;
	}
	public String getInspectedTypeP() {
		return inspectedTypeP;
	}
	public void setInspectedTypeP(String inspectedTypeP) {
		this.inspectedTypeP = inspectedTypeP;
	}
	public String getPlaceofInspectionP() {
		return placeofInspectionP;
	}
	public void setPlaceofInspectionP(String placeofInspectionP) {
		this.placeofInspectionP = placeofInspectionP;
	}
	public String getVehicleNoP() {
		return vehicleNoP;
	}
	public void setVehicleNoP(String vehicleNoP) {
		this.vehicleNoP = vehicleNoP;
	}
	public String getLicenseNoP() {
		return licenseNoP;
	}
	public void setLicenseNoP(String licenseNoP) {
		this.licenseNoP = licenseNoP;
	}
	public String getOwnerNameP() {
		return ownerNameP;
	}
	public void setOwnerNameP(String ownerNameP) {
		this.ownerNameP = ownerNameP;
	}
	public String getDiverNameP() {
		return diverNameP;
	}
	public void setDiverNameP(String diverNameP) {
		this.diverNameP = diverNameP;
	}
	public String getMobileNoP() {
		return mobileNoP;
	}
	public void setMobileNoP(String mobileNoP) {
		this.mobileNoP = mobileNoP;
	}
	public String getRemarksP() {
		return remarksP;
	}
	public void setRemarksP(String remarksP) {
		this.remarksP = remarksP;
	}
	public String getTrafficbranchP() {
		return trafficbranchP;
	}
	public void setTrafficbranchP(String trafficbranchP) {
		this.trafficbranchP = trafficbranchP;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getRegionfor() {
		return regionfor;
	}
	public void setRegionfor(String regionfor) {
		this.regionfor = regionfor;
	}
	public String getTrafficbranchFor() {
		return trafficbranchFor;
	}
	public void setTrafficbranchFor(String trafficbranchFor) {
		this.trafficbranchFor = trafficbranchFor;
	}
	public String getRemarksFor() {
		return remarksFor;
	}
	public void setRemarksFor(String remarksFor) {
		this.remarksFor = remarksFor;
	}
	public String getOwnerNameFor() {
		return ownerNameFor;
	}
	public void setOwnerNameFor(String ownerNameFor) {
		this.ownerNameFor = ownerNameFor;
	}
	public String getDiverNameFor() {
		return diverNameFor;
	}
	public void setDiverNameFor(String diverNameFor) {
		this.diverNameFor = diverNameFor;
	}
	public String getMobileNoFor() {
		return mobileNoFor;
	}
	public void setMobileNoFor(String mobileNoFor) {
		this.mobileNoFor = mobileNoFor;
	}
	public String getTINnoFor() {
		return TINnoFor;
	}
	public void setTINnoFor(String tINnoFor) {
		TINnoFor = tINnoFor;
	}
	public String getOffencedateFor() {
		return offencedateFor;
	}
	public void setOffencedateFor(String offencedateFor) {
		this.offencedateFor = offencedateFor;
	}
	public String getTimeofInspectionFor() {
		return timeofInspectionFor;
	}
	public void setTimeofInspectionFor(String timeofInspectionFor) {
		this.timeofInspectionFor = timeofInspectionFor;
	}
	public String getInspectedbyFor() {
		return inspectedbyFor;
	}
	public void setInspectedbyFor(String inspectedbyFor) {
		this.inspectedbyFor = inspectedbyFor;
	}
	public String getInspectedTypeFor() {
		return inspectedTypeFor;
	}
	public void setInspectedTypeFor(String inspectedTypeFor) {
		this.inspectedTypeFor = inspectedTypeFor;
	}
	public String getPlaceofInspectionFor() {
		return placeofInspectionFor;
	}
	public void setPlaceofInspectionFor(String placeofInspectionFor) {
		this.placeofInspectionFor = placeofInspectionFor;
	}
	public String getVehicleNoFor() {
		return vehicleNoFor;
	}
	public void setVehicleNoFor(String vehicleNoFor) {
		this.vehicleNoFor = vehicleNoFor;
	}
	public String getLicenseNoFor() {
		return licenseNoFor;
	}
	public void setLicenseNoFor(String licenseNoFor) {
		this.licenseNoFor = licenseNoFor;
	}
	public FormFile getImgPath() {
		return imgPath;
	}
	public void setImgPath(FormFile imgPath) {
		this.imgPath = imgPath;
	}
	public String getWithdrawnDate() {
		return withdrawnDate;
	}
	public void setWithdrawnDate(String withdrawnDate) {
		this.withdrawnDate = withdrawnDate;
	}
	public String getWithdrawnReason() {
		return withdrawnReason;
	}
	public void setWithdrawnReason(String withdrawnReason) {
		this.withdrawnReason = withdrawnReason;
	}
	public String getTcbEndorsement() {
		return tcbEndorsement;
	}
	public void setTcbEndorsement(String tcbEndorsement) {
		this.tcbEndorsement = tcbEndorsement;
	}
	public String[] getDrivetypeTCB() {
		return drivetypeTCB;
	}
	public void setDrivetypeTCB(String[] drivetypeTCB) {
		this.drivetypeTCB = drivetypeTCB;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public String getRenewalDuration() {
		return renewalDuration;
	}
	public void setRenewalDuration(String renewalDuration) {
		this.renewalDuration = renewalDuration;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public List<OffenceDTO> getOffenceList() {
		return offenceList;
	}
	public void setOffenceList(List<OffenceDTO> offenceList) {
		this.offenceList = offenceList;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getDrivinglicenseId() {
		return drivinglicenseId;
	}
	public void setDrivinglicenseId(String drivinglicenseId) {
		this.drivinglicenseId = drivinglicenseId;
	}
	public String getLearnerLicenseId() {
		return learnerLicenseId;
	}
	public void setLearnerLicenseId(String learnerLicenseId) {
		this.learnerLicenseId = learnerLicenseId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTopNo() {
		return topNo;
	}
	public void setTopNo(String topNo) {
		this.topNo = topNo;
	}
	public String getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(String licensetype) {
		this.licensetype = licensetype;
	}
	public String getExactLocation() {
		return exactLocation;
	}
	public void setExactLocation(String exactLocation) {
		this.exactLocation = exactLocation;
	}
	public FormFile getRcCertificate() {
		return rcCertificate;
	}
	public void setRcCertificate(FormFile rcCertificate) {
		this.rcCertificate = rcCertificate;
	}
	
	
	public FormFile getSupportDoc() {
		return supportDoc;
	}
	public void setSupportDoc(FormFile supportDoc) {
		this.supportDoc = supportDoc;
	}
	public FormFile getFileMC() {
		return fileMC;
	}
	public void setFileMC(FormFile fileMC) {
		this.fileMC = fileMC;
	}
	public FormFile getFileApplForm() {
		return fileApplForm;
	}
	public void setFileApplForm(FormFile fileApplForm) {
		this.fileApplForm = fileApplForm;
	}
	public String getNextExpiry() {
		return nextExpiry;
	}
	public void setNextExpiry(String nextExpiry) {
		this.nextExpiry = nextExpiry;
	}
	public String getLearnerlicenseinfoid() {
		return learnerlicenseinfoid;
	}
	public void setLearnerlicenseinfoid(String learnerlicenseinfoid) {
		this.learnerlicenseinfoid = learnerlicenseinfoid;
	}
	public String getOffenceLicesneType() {
		return offenceLicesneType;
	}
	public void setOffenceLicesneType(String offenceLicesneType) {
		this.offenceLicesneType = offenceLicesneType;
	}
	public String getLicenseTypeId() {
		return licenseTypeId;
	}
	public void setLicenseTypeId(String licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}
	public String getLearnerlicenseno() {
		return learnerlicenseno;
	}
	public void setLearnerlicenseno(String learnerlicenseno) {
		this.learnerlicenseno = learnerlicenseno;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getAppsubmissiondate() {
		return appsubmissiondate;
	}
	public void setAppsubmissiondate(String appsubmissiondate) {
		this.appsubmissiondate = appsubmissiondate;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getReceiptDate1() {
		return receiptDate1;
	}
	public void setReceiptDate1(String receiptDate1) {
		this.receiptDate1 = receiptDate1;
	}
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getEndorseddate() {
		return endorseddate;
	}
	public void setEndorseddate(String endorseddate) {
		this.endorseddate = endorseddate;
	}
	public String getDriveType() {
		return driveType;
	}
	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}
	public String getIssuedTo() {
		return issuedTo;
	}
	public void setIssuedTo(String issuedTo) {
		this.issuedTo = issuedTo;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getReissuedate() {
		return reissuedate;
	}
	public void setReissuedate(String reissuedate) {
		this.reissuedate = reissuedate;
	}
	public String getIsReissue() {
		return isReissue;
	}
	public void setIsReissue(String isReissue) {
		this.isReissue = isReissue;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getCancellationdate() {
		return cancellationdate;
	}
	public void setCancellationdate(String cancellationdate) {
		this.cancellationdate = cancellationdate;
	}
	public String getCancellationReason() {
		return cancellationReason;
	}
	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	
	public String getDuplicationdate() {
		return duplicationdate;
	}
	public void setDuplicationdate(String duplicationdate) {
		this.duplicationdate = duplicationdate;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getMc() {
		return mc;
	}
	public void setMc(String mc) {
		this.mc = mc;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTaxi() {
		return taxi;
	}
	public void setTaxi(String taxi) {
		this.taxi = taxi;
	}
	public String getMB() {
		return MB;
	}
	public void setMB(String mB) {
		MB = mB;
	}
	public String getHB() {
		return HB;
	}
	public void setHB(String hB) {
		HB = hB;
	}
	public String getDeliveredOn() {
		return deliveredOn;
	}
	public void setDeliveredOn(String deliveredOn) {
		this.deliveredOn = deliveredOn;
	}
	public String getAutogenerate() {
		return autogenerate;
	}
	public void setAutogenerate(String autogenerate) {
		this.autogenerate = autogenerate;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBloodgroup() {
		return bloodgroup;
	}
	public void setBloodgroup(String bloodgroup) {
		this.bloodgroup = bloodgroup;
	}
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getGewog() {
		return gewog;
	}
	public void setGewog(String gewog) {
		this.gewog = gewog;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDlNo() {
		return dlNo;
	}
	public void setDlNo(String dlNo) {
		this.dlNo = dlNo;
	}
	public String getLearnerlicenseNo() {
		return learnerlicenseNo;
	}
	public void setLearnerlicenseNo(String learnerlicenseNo) {
		this.learnerlicenseNo = learnerlicenseNo;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getIdentificationmarks() {
		return identificationmarks;
	}
	public void setIdentificationmarks(String identificationmarks) {
		this.identificationmarks = identificationmarks;
	}
	
	public String getFathersname() {
		return fathersname;
	}
	public void setFathersname(String fathersname) {
		this.fathersname = fathersname;
	}
	
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	public String getTestmarks() {
		return testmarks;
	}
	public void setTestmarks(String testmarks) {
		this.testmarks = testmarks;
	}
	public String getRegisterNo() {
		return registerNo;
	}
	public void setRegisterNo(String registerNo) {
		this.registerNo = registerNo;
	}
	public String getDrivetype() {
		return drivetype;
	}
	public void setDrivetype(String drivetype) {
		this.drivetype = drivetype;
	}
	public String getDeliveredon() {
		return deliveredon;
	}
	public void setDeliveredon(String deliveredon) {
		this.deliveredon = deliveredon;
	}
	
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public FormFile getOffence1() {
		return offence1;
	}
	public void setOffence1(FormFile offence1) {
		this.offence1 = offence1;
	}
	public FormFile getOffence2() {
		return offence2;
	}
	public void setOffence2(FormFile offence2) {
		this.offence2 = offence2;
	}
	public String getOffencedate() {
		return offencedate;
	}
	public void setOffencedate(String offencedate) {
		this.offencedate = offencedate;
	}
	public String getTimeofInspection() {
		return timeofInspection;
	}
	public void setTimeofInspection(String timeofInspection) {
		this.timeofInspection = timeofInspection;
	}
	public String getInspectedby() {
		return inspectedby;
	}
	public void setInspectedby(String inspectedby) {
		this.inspectedby = inspectedby;
	}
	public String getInspectedType() {
		return inspectedType;
	}
	public void setInspectedType(String inspectedType) {
		this.inspectedType = inspectedType;
	}
	public String getTrafficbranch() {
		return trafficbranch;
	}
	public void setTrafficbranch(String trafficbranch) {
		this.trafficbranch = trafficbranch;
	}
	public String getPlaceofInspection() {
		return placeofInspection;
	}
	public void setPlaceofInspection(String placeofInspection) {
		this.placeofInspection = placeofInspection;
	}
	public String getLearnerlicense() {
		return learnerlicense;
	}
	public void setLearnerlicense(String learnerlicense) {
		this.learnerlicense = learnerlicense;
	}
	public String getBluebook() {
		return bluebook;
	}
	public void setBluebook(String bluebook) {
		this.bluebook = bluebook;
	}
	public String getTINno() {
		return TINno;
	}
	public void setTINno(String tINno) {
		TINno = tINno;
	}
	public String getReceivedfrom() {
		return receivedfrom;
	}
	public void setReceivedfrom(String receivedfrom) {
		this.receivedfrom = receivedfrom;
	}
	public String getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getRenewaldate() {
		return renewaldate;
	}
	public void setRenewaldate(String renewaldate) {
		this.renewaldate = renewaldate;
	}
	public String getDeliveryon() {
		return deliveryon;
	}
	public void setDeliveryon(String deliveryon) {
		this.deliveryon = deliveryon;
	}
	public String getRoadPaver() {
		return roadPaver;
	}
	public void setRoadPaver(String roadPaver) {
		this.roadPaver = roadPaver;
	}
	public String getExcavator() {
		return excavator;
	}
	public void setExcavator(String excavator) {
		this.excavator = excavator;
	}
	public String getBullDozer() {
		return bullDozer;
	}
	public void setBullDozer(String bullDozer) {
		this.bullDozer = bullDozer;
	}
	public String getPayLoader() {
		return payLoader;
	}
	public void setPayLoader(String payLoader) {
		this.payLoader = payLoader;
	}
	public String getTractor() {
		return tractor;
	}
	public void setTractor(String tractor) {
		this.tractor = tractor;
	}
	public String getCID() {
		return CID;
	}
	public void setCID(String cID) {
		CID = cID;
	}
	public String getLLNo() {
		return LLNo;
	}
	public void setLLNo(String lLNo) {
		LLNo = lLNo;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getIssuedate() {
		return issuedate;
	}
	public void setIssuedate(String issuedate) {
		this.issuedate = issuedate;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getCertifyingDoctor() {
		return certifyingDoctor;
	}
	public void setCertifyingDoctor(String certifyingDoctor) {
		this.certifyingDoctor = certifyingDoctor;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRoadroller() {
		return roadroller;
	}
	public void setRoadroller(String roadroller) {
		this.roadroller = roadroller;
	}
	public String getPowertiller() {
		return powertiller;
	}
	public void setPowertiller(String powertiller) {
		this.powertiller = powertiller;
	}
	public String getCrane() {
		return crane;
	}
	public void setCrane(String crane) {
		this.crane = crane;
	}
	public String getTMB() {
		return TMB;
	}
	public void setTMB(String tMB) {
		TMB = tMB;
	}
	public String getTLV() {
		return TLV;
	}
	public void setTLV(String tLV) {
		TLV = tLV;
	}
	public String getTHB() {
		return THB;
	}
	public void setTHB(String tHB) {
		THB = tHB;
	}
	public String getLV() {
		return LV;
	}
	public void setLV(String lV) {
		LV = lV;
	}
	public String getHV() {
		return HV;
	}
	public void setHV(String hV) {
		HV = hV;
	}
	public String getTW() {
		return TW;
	}
	public void setTW(String tW) {
		TW = tW;
	}
	public String getMV() {
		return MV;
	}
	public void setMV(String mV) {
		MV = mV;
	}

	public OffenceDTO getOffence(int index) 
	{
		while (this.offenceList.size() <= index)
		{
			offenceList.add(new OffenceDTO());
		}
		return ((OffenceDTO) offenceList.get(index));
		
	}
	public OffenceDTO getOffenceB(int index) 
	{
		while (this.offenceListB.size() <= index)
		{
			offenceListB.add(new OffenceDTO());
		}
		return ((OffenceDTO) offenceListB.get(index));
	}
	public OffenceDTO getOffenceF(int index) 
	{
		while (this.offenceListF.size() <= index)
		{
			offenceListF.add(new OffenceDTO());
		}
		return ((OffenceDTO) offenceListF.get(index));
	}
	
	
}
