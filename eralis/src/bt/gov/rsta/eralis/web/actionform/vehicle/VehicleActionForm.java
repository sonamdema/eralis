package bt.gov.rsta.eralis.web.actionform.vehicle;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class VehicleActionForm extends ActionForm
{
	private static final long serialVersionUID = 1L;
	private String personalRadio;
	private String organisationRadio;
	private String ownerID;
	private String registrationType;
	private String amount;
	private String penalty;
	private String vehicleCode;
	private String region;
	private String vehiclePrefix;
	private String vehicleNumber;
	private String vehicleType;
	private String vanityNumber;
	private String vehicleCompany;
	private String registrationDate;
	private String vehicleModel;
	private String expiryDate;
	private String engineType;
	private String receiptDate;
	private String engineNumber;
	private String receiptNo;
	private String chasisNumber;
	private String status;
	private String colour;
	private String price;
	private String loadCapacity;
	private String dealersName;
	private String seatCapacity;
	private String letterDate;
	private String hypothecatedTo;
	private String unladenWeight;
	private String letterNo;
	private String manufactureYear;
	private String engineCC;
	private String manufactureCountry;
	private String remarks;
	private String purchaseType;
	private String lastRegistrationDate;
	private String penality;
	private String renewalDate;
	private String citizenID;
    private String transferDate;
    private String duplicateIssueDate;
    private String cancellationDate;
    private String cancellationReason;
    private String conversionDate;
    private String conversionReason;
    private String petrolRadio;
    private String dieselRadio;
    private String testedOn;
    private String validUntil;
    private String testLocation;
    private String cO;
    private String hSU;
    private String testResult;
    private String testFrequency;
    private String postedDate;
    private String printDate;
    private String applicationNumber;
    private String customerId;
    private String renewalAmount;
    private String pageId;
    private String type;
    private String vehicleRegistrationType;
    private String vehicleRegistrationId;
    private String transferorCustomerId;
    private String transfereeCustomerId;
    private String transfereeName;
	private String transfereeDzongkhag;
    private String transfereeGewog;
    private String customerId1;
    private String transfereeLastRegistrationDate;
    private String transfereeAddress;
    private String transfereeRegion;
    private String transfereeExpiryDate;
    private String transfereePhone;
    private String transfereeVehicleRegistrationType;
    private String description;
    private String lastExpiryDate;
    private String vehicleHorsePower;
    private String vehicleKiloWatt;
    private String purchaseDate;
    private String saleDeedAmount;
    private String saleDeedDate;
    private FormFile invoice;
    private FormFile challan;
    private FormFile letterOfAuthenticity;
    private FormFile emission;
    private FormFile exemptionCertificate;
    private FormFile exciseInvoice;
    private FormFile customDeclaration;
    private FormFile vehiclePicture;
    private FormFile supportingDocument;
    private String vehicleId;
    private String renewalDuration;
    private String reason;
    private String conversionType;
    private String vehicleRegistrationCode;
    private String mainRegion;
    private String supportDoc;
    private String vehicleTypeId;
    private String t1;
    private String t2;
    private String t3;
    private String withdrawnReason;
    private String withdrawnDate;
    private FormFile imgPath;
    private String busType;
    private String wheelNos;
    private String quotaMobileNumber;
    private String ownerMobileNumber;
    private String numberAxle;
    private String diplomatCode;
    private String licenseNo;
    private String drivinglicenseId;
    private String inspectionType;
    

    private String isDrug;
    private String alcoholTest;
    private String departureFrom;
    private String destination;
    private String departureTime;
    private String departureDate;
    private String arrivalTime;
    private String arrivalDate;
    private String noMale;
    private String noFemale;
    private String noChildren;
    private String inspectedBy;
    private String transferType;
    
    public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public String getInspectedBy() {
		return inspectedBy;
	}
	public void setInspectedBy(String inspectedBy) {
		this.inspectedBy = inspectedBy;
	}
	public String getInspectionType() {
		return inspectionType;
	}
	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}
	public String getIsDrug() {
		return isDrug;
	}
	public void setIsDrug(String isDrug) {
		this.isDrug = isDrug;
	}
	public String getAlcoholTest() {
		return alcoholTest;
	}
	public void setAlcoholTest(String alcoholTest) {
		this.alcoholTest = alcoholTest;
	}
	public String getDepartureFrom() {
		return departureFrom;
	}
	public void setDepartureFrom(String departureFrom) {
		this.departureFrom = departureFrom;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getNoMale() {
		return noMale;
	}
	public void setNoMale(String noMale) {
		this.noMale = noMale;
	}
	public String getNoFemale() {
		return noFemale;
	}
	public void setNoFemale(String noFemale) {
		this.noFemale = noFemale;
	}
	public String getNoChildren() {
		return noChildren;
	}
	public void setNoChildren(String noChildren) {
		this.noChildren = noChildren;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getDrivinglicenseId() {
		return drivinglicenseId;
	}
	public void setDrivinglicenseId(String drivinglicenseId) {
		this.drivinglicenseId = drivinglicenseId;
	}
	public String getDiplomatCode() {
		return diplomatCode;
	}
	public void setDiplomatCode(String diplomatCode) {
		this.diplomatCode = diplomatCode;
	}
	public String getNumberAxle() {
		return numberAxle;
	}
	public void setNumberAxle(String numberAxle) {
		this.numberAxle = numberAxle;
	}
	public String getQuotaMobileNumber() {
		return quotaMobileNumber;
	}
	public void setQuotaMobileNumber(String quotaMobileNumber) {
		this.quotaMobileNumber = quotaMobileNumber;
	}
	public String getOwnerMobileNumber() {
		return ownerMobileNumber;
	}
	public void setOwnerMobileNumber(String ownerMobileNumber) {
		this.ownerMobileNumber = ownerMobileNumber;
	}
	public String getWheelNos() {
		return wheelNos;
	}
	public void setWheelNos(String wheelNos) {
		this.wheelNos = wheelNos;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public FormFile getImgPath() {
		return imgPath;
	}
	public void setImgPath(FormFile imgPath) {
		this.imgPath = imgPath;
	}
	public String getWithdrawnReason() {
		return withdrawnReason;
	}
	public void setWithdrawnReason(String withdrawnReason) {
		this.withdrawnReason = withdrawnReason;
	}
	public String getWithdrawnDate() {
		return withdrawnDate;
	}
	public void setWithdrawnDate(String withdrawnDate) {
		this.withdrawnDate = withdrawnDate;
	}
	public String getT1() {
		return t1;
	}
	public void setT1(String t1) {
		this.t1 = t1;
	}
	public String getT2() {
		return t2;
	}
	public void setT2(String t2) {
		this.t2 = t2;
	}
	public String getT3() {
		return t3;
	}
	public void setT3(String t3) {
		this.t3 = t3;
	}
	public String getVehicleTypeId() {
		return vehicleTypeId;
	}
	public void setVehicleTypeId(String vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}
	public String getSupportDoc() {
		return supportDoc;
	}
	public void setSupportDoc(String supportDoc) {
		this.supportDoc = supportDoc;
	}
	public String getVehicleRegistrationCode() {
		return vehicleRegistrationCode;
	}
	public void setVehicleRegistrationCode(String vehicleRegistrationCode) {
		this.vehicleRegistrationCode = vehicleRegistrationCode;
	}
	public String getMainRegion() {
		return mainRegion;
	}
	public void setMainRegion(String mainRegion) {
		this.mainRegion = mainRegion;
	}
	public String getConversionType() {
		return conversionType;
	}
	public void setConversionType(String conversionType) {
		this.conversionType = conversionType;
	}
	public FormFile getVehiclePicture() {
		return vehiclePicture;
	}
	public void setVehiclePicture(FormFile vehiclePicture) {
		this.vehiclePicture = vehiclePicture;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRenewalDuration() {
		return renewalDuration;
	}
	public void setRenewalDuration(String renewalDuration) {
		this.renewalDuration = renewalDuration;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getSaleDeedAmount() {
		return saleDeedAmount;
	}
	public void setSaleDeedAmount(String saleDeedAmount) {
		this.saleDeedAmount = saleDeedAmount;
	}
	public String getSaleDeedDate() {
		return saleDeedDate;
	}
	public void setSaleDeedDate(String saleDeedDate) {
		this.saleDeedDate = saleDeedDate;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public FormFile getSupportingDocument() {
		return supportingDocument;
	}
	public void setSupportingDocument(FormFile supportingDocument) {
		this.supportingDocument = supportingDocument;
	}
	public FormFile getInvoice() {
		return invoice;
	}
	public void setInvoice(FormFile invoice) {
		this.invoice = invoice;
	}
	public FormFile getChallan() {
		return challan;
	}
	public void setChallan(FormFile challan) {
		this.challan = challan;
	}
	public FormFile getLetterOfAuthenticity() {
		return letterOfAuthenticity;
	}
	public void setLetterOfAuthenticity(FormFile letterOfAuthenticity) {
		this.letterOfAuthenticity = letterOfAuthenticity;
	}
	public FormFile getEmission() {
		return emission;
	}
	public void setEmission(FormFile emission) {
		this.emission = emission;
	}
	public FormFile getExemptionCertificate() {
		return exemptionCertificate;
	}
	public void setExemptionCertificate(FormFile exemptionCertificate) {
		this.exemptionCertificate = exemptionCertificate;
	}
	public FormFile getExciseInvoice() {
		return exciseInvoice;
	}
	public void setExciseInvoice(FormFile exciseInvoice) {
		this.exciseInvoice = exciseInvoice;
	}
	public FormFile getCustomDeclaration() {
		return customDeclaration;
	}
	public void setCustomDeclaration(FormFile customDeclaration) {
		this.customDeclaration = customDeclaration;
	}
	public String getVehicleHorsePower() {
		return vehicleHorsePower;
	}
	public void setVehicleHorsePower(String vehicleHorsePower) {
		this.vehicleHorsePower = vehicleHorsePower;
	}
	public String getVehicleKiloWatt() {
		return vehicleKiloWatt;
	}
	public void setVehicleKiloWatt(String vehicleKiloWatt) {
		this.vehicleKiloWatt = vehicleKiloWatt;
	}
	public String getLastExpiryDate() {
		return lastExpiryDate;
	}
	public void setLastExpiryDate(String lastExpiryDate) {
		this.lastExpiryDate = lastExpiryDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTransfereeAddress() {
		return transfereeAddress;
	}
	public void setTransfereeAddress(String transfereeAddress) {
		this.transfereeAddress = transfereeAddress;
	}
	public String getTransfereeRegion() {
		return transfereeRegion;
	}
	public void setTransfereeRegion(String transfereeRegion) {
		this.transfereeRegion = transfereeRegion;
	}
	public String getTransfereeExpiryDate() {
		return transfereeExpiryDate;
	}
	public void setTransfereeExpiryDate(String transfereeExpiryDate) {
		this.transfereeExpiryDate = transfereeExpiryDate;
	}
	public String getTransfereePhone() {
		return transfereePhone;
	}
	public void setTransfereePhone(String transfereePhone) {
		this.transfereePhone = transfereePhone;
	}
	public String getTransfereeVehicleRegistrationType() {
		return transfereeVehicleRegistrationType;
	}
	public void setTransfereeVehicleRegistrationType(
			String transfereeVehicleRegistrationType) {
		this.transfereeVehicleRegistrationType = transfereeVehicleRegistrationType;
	}
	public String getTransfereeLastRegistrationDate() {
		return transfereeLastRegistrationDate;
	}
	public void setTransfereeLastRegistrationDate(
			String transfereeLastRegistrationDate) {
		this.transfereeLastRegistrationDate = transfereeLastRegistrationDate;
	}
	public String getCustomerId1() {
		return customerId1;
	}
	public void setCustomerId1(String customerId1) {
		this.customerId1 = customerId1;
	}
	public String getTransfereeName() {
		return transfereeName;
	}
	public void setTransfereeName(String transfereeName) {
		this.transfereeName = transfereeName;
	}
	public String getTransfereeDzongkhag() {
		return transfereeDzongkhag;
	}
	public void setTransfereeDzongkhag(String transfereeDzongkhag) {
		this.transfereeDzongkhag = transfereeDzongkhag;
	}
	public String getTransfereeGewog() {
		return transfereeGewog;
	}
	public void setTransfereeGewog(String transfereeGewog) {
		this.transfereeGewog = transfereeGewog;
	}
	public String getTransferorCustomerId() {
		return transferorCustomerId;
	}
	public void setTransferorCustomerId(String transferorCustomerId) {
		this.transferorCustomerId = transferorCustomerId;
	}
	public String getTransfereeCustomerId() {
		return transfereeCustomerId;
	}
	public void setTransfereeCustomerId(String transfereeCustomerId) {
		this.transfereeCustomerId = transfereeCustomerId;
	}
	public String getVehicleRegistrationId() {
		return vehicleRegistrationId;
	}
	public void setVehicleRegistrationId(String vehicleRegistrationId) {
		this.vehicleRegistrationId = vehicleRegistrationId;
	}
	public String getVehicleRegistrationType() {
		return vehicleRegistrationType;
	}
	public void setVehicleRegistrationType(String vehicleRegistrationType) {
		this.vehicleRegistrationType = vehicleRegistrationType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getRenewalAmount() {
		return renewalAmount;
	}
	public void setRenewalAmount(String renewalAmount) {
		this.renewalAmount = renewalAmount;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public String getLetterDate() {
		return letterDate;
	}
	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public String getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	public String getTestFrequency() {
		return testFrequency;
	}
	public void setTestFrequency(String testFrequency) {
		this.testFrequency = testFrequency;
	}
	public String getcO() {
		return cO;
	}
	public void setcO(String cO) {
		this.cO = cO;
	}
	public String gethSU() {
		return hSU;
	}
	public void sethSU(String hSU) {
		this.hSU = hSU;
	}
	public String getTestLocation() {
		return testLocation;
	}
	public void setTestLocation(String testLocation) {
		this.testLocation = testLocation;
	}
	public String getTestedOn() {
		return testedOn;
	}
	public void setTestedOn(String testedOn) {
		this.testedOn = testedOn;
	}
	public String getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}
	public String getPetrolRadio() {
		return petrolRadio;
	}
	public void setPetrolRadio(String petrolRadio) {
		this.petrolRadio = petrolRadio;
	}
	public String getDieselRadio() {
		return dieselRadio;
	}
	public void setDieselRadio(String dieselRadio) {
		this.dieselRadio = dieselRadio;
	}
	public String getConversionDate() {
		return conversionDate;
	}
	public void setConversionDate(String conversionDate) {
		this.conversionDate = conversionDate;
	}
	public String getConversionReason() {
		return conversionReason;
	}
	public void setConversionReason(String conversionReason) {
		this.conversionReason = conversionReason;
	}
	public String getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public String getCancellationReason() {
		return cancellationReason;
	}
	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	public String getDuplicateIssueDate() {
		return duplicateIssueDate;
	}
	public void setDuplicateIssueDate(String duplicateIssueDate) {
		this.duplicateIssueDate = duplicateIssueDate;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	
	public String getCitizenID() {
		return citizenID;
	}
	public void setCitizenID(String citizenID) {
		this.citizenID = citizenID;
	}
	public String getLastRegistrationDate() {
		return lastRegistrationDate;
	}
	public void setLastRegistrationDate(String lastRegistrationDate) {
		this.lastRegistrationDate = lastRegistrationDate;
	}
	public String getPenality() {
		return penality;
	}
	public void setPenality(String penality) {
		this.penality = penality;
	}
	public String getRenewalDate() {
		return renewalDate;
	}
	public void setRenewalDate(String renewalDate) {
		this.renewalDate = renewalDate;
	}
	public String getPersonalRadio() {
		return personalRadio;
	}
	public void setPersonalRadio(String personalRadio) {
		this.personalRadio = personalRadio;
	}
	public String getOrganisationRadio() {
		return organisationRadio;
	}
	public void setOrganisationRadio(String organisationRadio) {
		this.organisationRadio = organisationRadio;
	}
	
	public String getVehiclePrefix() {
		return vehiclePrefix;
	}
	public void setVehiclePrefix(String vehiclePrefix) {
		this.vehiclePrefix = vehiclePrefix;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVanityNumber() {
		return vanityNumber;
	}
	public void setVanityNumber(String vanityNumber) {
		this.vanityNumber = vanityNumber;
	}
	public String getVehicleCompany() {
		return vehicleCompany;
	}
	public void setVehicleCompany(String vehicleCompany) {
		this.vehicleCompany = vehicleCompany;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getEngineType() {
		return engineType;
	}
	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getChasisNumber() {
		return chasisNumber;
	}
	public void setChasisNumber(String chasisNumber) {
		this.chasisNumber = chasisNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getLoadCapacity() {
		return loadCapacity;
	}
	public void setLoadCapacity(String loadCapacity) {
		this.loadCapacity = loadCapacity;
	}
	public String getDealersName() {
		return dealersName;
	}
	public void setDealersName(String dealersName) {
		this.dealersName = dealersName;
	}
	public String getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(String seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getHypothecatedTo() {
		return hypothecatedTo;
	}
	public void setHypothecatedTo(String hypothecatedTo) {
		this.hypothecatedTo = hypothecatedTo;
	}
	public String getUnladenWeight() {
		return unladenWeight;
	}
	public void setUnladenWeight(String unladenWeight) {
		this.unladenWeight = unladenWeight;
	}
	public String getLetterNo() {
		return letterNo;
	}
	public void setLetterNo(String letterNo) {
		this.letterNo = letterNo;
	}
	public String getManufactureYear() {
		return manufactureYear;
	}
	public void setManufactureYear(String manufactureYear) {
		this.manufactureYear = manufactureYear;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getManufactureCountry() {
		return manufactureCountry;
	}
	public void setManufactureCountry(String manufactureCountry) {
		this.manufactureCountry = manufactureCountry;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPurchaseType() {
		return purchaseType;
	}
	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}
	
	public String getVehicleCode() {
		return vehicleCode;
	}
	public void setVehicleCode(String vehicleCode) {
		this.vehicleCode = vehicleCode;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}
}
