package bt.gov.rsta.eralis.web.actionform.eralis_common;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.OffenceDTO;

public class EralisCommonActionForm extends ActionForm
{
	private static final long serialVersionUID = 1L;
	
	private String customerID;
	private String buyerName;
	private String buyerAddress;
	private String receiptDate;
	private String amount;
	private String OwnerType;
	private String titleOfcourtesy;
	private String firstname;
	private String middlename;
	private String lastName;
	private String occupation;
	private String nationality;
	private String CID;
	private String DOB;
	private String bloodGroup;
	private String fathersName;
	private String gender;
	private String identificationMarks;
	private String remarks;
	private String national;
	private String international;
	private String dzongkhag;
	private String gewog;
	private String village;
	private String country;
	private String address;
	private String contactAddress;
	private String phone;
	private String email;
	private FormFile upload;
	private String code;
	private String type;
	private String ministry;
	private String department;
	private String region;
	private String name;
	private String conductorName;
	private String vehicleNo;
	private String LLNo;
	private String accidentDate;
	private String accidentCause;
	private String accidentSite;
	private String licenseNo;
	private String driverName;
	private String permitID;
	private String registrationNo;
	private String baseoffice;
	private String routeBetween;
	private String to;
	private String vehicleType;
	private String carryingCapacity;
	private String permitIDNo;
	private String numberOfDeath;
	private String numberofInjured;
	private String numberofunInjured;
	private String registrationcertificate;
	private String issurance;
	private String certificateOfroadworthiness;
	private String emmissioncertificate;
	private String presentDzongkhag;
	private String manualFlag;
	private String transferorCustomerId;
	private String transfereeCustomerId;
	private String status;
	private String newVehicleNo;
	private String selectall;
	private String vehicleRegistrationType;
	private String personalOwnerName;
	private String caseNo;
	private String policeStation;
	private String occurrenceDate;
	private String occurrenceTime;
	private String reportDate;
	private String reportTime;
	private String occurencePlace;
	private String mobileNo;
	private String age;
	private String make;
	private String model;
	private String licenseStatus;
	private String duplicateIssueDate;
	private String transferDate;
	private String vehicleHorsePower;
	private String vehicleKiloWatt;
	private String engineCC;
	private String licensetype;	
	private String OwnerName;
	private String vPrefix;
	private String vehicleId;
	private String privateCompany;
	private FormFile fileMC;
	private FormFile fileApplForm;
	private FormFile supportDoc;
	private FormFile rcCertificate;
	private String applicationNo;
	private String certifyingDoctor;
	private String reason;
	private String saleDeedDate;
	private String saleDeedAmount;
	private String nocFees;
	private String penalty;
	private String organisationInfoId;
	private String personalInfoId;
	
	private String permitIssuedTo;
	private String licenseNumber;
	private String vehicleNumber;
	private String vehicleTypeBV;
	private String learnerNo;
	private String journeyPurpose;
	private String buyerCID;
	private String ownershipType; 
	private String driverDtls;
	private String vehilceDtls;
	private String killedDtls;
	private String injuredDtls;
	private String accidentCauseDtls;
	private String dateOfOccurrence;
	private String timeOfOccurrence;
	private String dateOfReport;
	private String timeOfReport;
	private String placeOfOccurrence;
	private String accidentType;
	private String accidentNature;
	private String permitNo;
	private String numberOfDays;
	private String contactRadio;
	private List<EralisCommonDTO> accidentDriverList;
	private List<EralisCommonDTO> accidentVehicleList;
	private List<EralisCommonDTO> injuredPersonList;
	private List<EralisCommonDTO> killedPersonList;
	
	private FormFile photo1;
	private FormFile photo2;
	private FormFile photo3;
	private FormFile photo4;
	private String informerType;
	private String informerName;
	private String distance;
	private String arrivalDate;
	private String arrivalTime;
	private String policeDivision;
	private String emergencyPhoneNo;
	private String guardianNo;
	private String mothersName;
	private String diplomatId;
	
	public String getDiplomatId() {
		return diplomatId;
	}
	public void setDiplomatId(String diplomatId) {
		this.diplomatId = diplomatId;
	}
	public String getMothersName() {
		return mothersName;
	}
	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}
	public String getGuardianNo() {
		return guardianNo;
	}
	public void setGuardianNo(String guardianNo) {
		this.guardianNo = guardianNo;
	}
	public String getEmergencyPhoneNo() {
		return emergencyPhoneNo;
	}
	public void setEmergencyPhoneNo(String emergencyPhoneNo) {
		this.emergencyPhoneNo = emergencyPhoneNo;
	}
	public String getInformerType() {
		return informerType;
	}

	public void setInformerType(String informerType) {
		this.informerType = informerType;
	}

	public String getInformerName() {
		return informerName;
	}

	public void setInformerName(String informerName) {
		this.informerName = informerName;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public List<EralisCommonDTO> getInjuredPersonList() {
		return injuredPersonList;
	}

	public void setInjuredPersonList(List<EralisCommonDTO> injuredPersonList) {
		this.injuredPersonList = injuredPersonList;
	}

	public List<EralisCommonDTO> getKilledPersonList() {
		return killedPersonList;
	}

	public void setKilledPersonList(List<EralisCommonDTO> killedPersonList) {
		this.killedPersonList = killedPersonList;
	}

	public String getPoliceDivision() {
		return policeDivision;
	}

	public void setPoliceDivision(String policeDivision) {
		this.policeDivision = policeDivision;
	}

	public FormFile getPhoto1() {
		return photo1;
	}

	public void setPhoto1(FormFile photo1) {
		this.photo1 = photo1;
	}

	public FormFile getPhoto2() {
		return photo2;
	}

	public void setPhoto2(FormFile photo2) {
		this.photo2 = photo2;
	}

	public FormFile getPhoto3() {
		return photo3;
	}

	public void setPhoto3(FormFile photo3) {
		this.photo3 = photo3;
	}

	public FormFile getPhoto4() {
		return photo4;
	}

	public void setPhoto4(FormFile photo4) {
		this.photo4 = photo4;
	}

	public List<EralisCommonDTO> getAccidentVehicleList() {
		return accidentVehicleList;
	}

	public void setAccidentVehicleList(List<EralisCommonDTO> accidentVehicleList) {
		this.accidentVehicleList = accidentVehicleList;
	}

	public List<EralisCommonDTO> getAccidentDriverList() {
		return accidentDriverList;
	}
	public void setAccidentDriverList(List<EralisCommonDTO> accidentDriverList) {
		this.accidentDriverList = accidentDriverList;
	}
	
	public String getContactRadio() {
		return contactRadio;
	}
	public void setContactRadio(String contactRadio) {
		this.contactRadio = contactRadio;
	}
	public String getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(String numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public String getPermitNo() {
		return permitNo;
	}
	public void setPermitNo(String permitNo) {
		this.permitNo = permitNo;
	}
	public String getAccidentType() {
		return accidentType;
	}
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	public String getAccidentNature() {
		return accidentNature;
	}
	public void setAccidentNature(String accidentNature) {
		this.accidentNature = accidentNature;
	}
	public String getTimeOfOccurrence() {
		return timeOfOccurrence;
	}
	public void setTimeOfOccurrence(String timeOfOccurrence) {
		this.timeOfOccurrence = timeOfOccurrence;
	}
	public String getDateOfReport() {
		return dateOfReport;
	}
	public void setDateOfReport(String dateOfReport) {
		this.dateOfReport = dateOfReport;
	}
	public String getTimeOfReport() {
		return timeOfReport;
	}
	public void setTimeOfReport(String timeOfReport) {
		this.timeOfReport = timeOfReport;
	}
	public String getPlaceOfOccurrence() {
		return placeOfOccurrence;
	}
	public void setPlaceOfOccurrence(String placeOfOccurrence) {
		this.placeOfOccurrence = placeOfOccurrence;
	}
	public String getDateOfOccurrence() {
		return dateOfOccurrence;
	}
	public void setDateOfOccurrence(String dateOfOccurrence) {
		this.dateOfOccurrence = dateOfOccurrence;
	}
	public String getAccidentCauseDtls() {
		return accidentCauseDtls;
	}
	public void setAccidentCauseDtls(String accidentCauseDtls) {
		this.accidentCauseDtls = accidentCauseDtls;
	}
	public String getKilledDtls() {
		return killedDtls;
	}
	public void setKilledDtls(String killedDtls) {
		this.killedDtls = killedDtls;
	}
	public String getInjuredDtls() {
		return injuredDtls;
	}
	public void setInjuredDtls(String injuredDtls) {
		this.injuredDtls = injuredDtls;
	}
	public String getDriverDtls() {
		return driverDtls;
	}
	public void setDriverDtls(String driverDtls) {
		this.driverDtls = driverDtls;
	}
	public String getVehilceDtls() {
		return vehilceDtls;
	}
	public void setVehilceDtls(String vehilceDtls) {
		this.vehilceDtls = vehilceDtls;
	}
	public String getOwnershipType() {
		return ownershipType;
	}
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}
	public String getBuyerCID() {
		return buyerCID;
	}
	public void setBuyerCID(String buyerCID) {
		this.buyerCID = buyerCID;
	}
	public String getJourneyPurpose() {
		return journeyPurpose;
	}
	public void setJourneyPurpose(String journeyPurpose) {
		this.journeyPurpose = journeyPurpose;
	}
	public String getLearnerNo() {
		return learnerNo;
	}
	public void setLearnerNo(String learnerNo) {
		this.learnerNo = learnerNo;
	}
	public String getPermitIssuedTo() {
		return permitIssuedTo;
	}
	public void setPermitIssuedTo(String permitIssuedTo) {
		this.permitIssuedTo = permitIssuedTo;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehicleTypeBV() {
		return vehicleTypeBV;
	}
	public void setVehicleTypeBV(String vehicleTypeBV) {
		this.vehicleTypeBV = vehicleTypeBV;
	}
	public String getPersonalInfoId() {
		return personalInfoId;
	}
	public void setPersonalInfoId(String personalInfoId) {
		this.personalInfoId = personalInfoId;
	}
	public String getOrganisationInfoId() {
		return organisationInfoId;
	}
	public void setOrganisationInfoId(String organisationInfoId) {
		this.organisationInfoId = organisationInfoId;
	}
	public String getSaleDeedDate() {
		return saleDeedDate;
	}
	public void setSaleDeedDate(String saleDeedDate) {
		this.saleDeedDate = saleDeedDate;
	}
	public String getSaleDeedAmount() {
		return saleDeedAmount;
	}
	public void setSaleDeedAmount(String saleDeedAmount) {
		this.saleDeedAmount = saleDeedAmount;
	}
	public String getNocFees() {
		return nocFees;
	}
	public void setNocFees(String nocFees) {
		this.nocFees = nocFees;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCertifyingDoctor() {
		return certifyingDoctor;
	}
	public void setCertifyingDoctor(String certifyingDoctor) {
		this.certifyingDoctor = certifyingDoctor;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public FormFile getFileMC() {
		return fileMC;
	}
	public void setFileMC(FormFile fileMC) {
		this.fileMC = fileMC;
	}
	public FormFile getFileApplForm() {
		return fileApplForm;
	}
	public void setFileApplForm(FormFile fileApplForm) {
		this.fileApplForm = fileApplForm;
	}
	public FormFile getSupportDoc() {
		return supportDoc;
	}
	public void setSupportDoc(FormFile supportDoc) {
		this.supportDoc = supportDoc;
	}
	public FormFile getRcCertificate() {
		return rcCertificate;
	}
	public void setRcCertificate(FormFile rcCertificate) {
		this.rcCertificate = rcCertificate;
	}
	public String getPrivateCompany() {
		return privateCompany;
	}
	public void setPrivateCompany(String privateCompany) {
		this.privateCompany = privateCompany;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getvPrefix() {
		return vPrefix;
	}
	public void setvPrefix(String vPrefix) {
		this.vPrefix = vPrefix;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getVehicleHorsePower() {
		return vehicleHorsePower;
	}
	public void setVehicleHorsePower(String vehicleHorsePower) {
		this.vehicleHorsePower = vehicleHorsePower;
	}
	public String getVehicleKiloWatt() {
		return vehicleKiloWatt;
	}
	public void setVehicleKiloWatt(String vehicleKiloWatt) {
		this.vehicleKiloWatt = vehicleKiloWatt;
	}
	
	
	public String getOwnerName() {
		return OwnerName;
	}
	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}
	public String getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(String licensetype) {
		this.licensetype = licensetype;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	public String getDuplicateIssueDate() {
		return duplicateIssueDate;
	}
	public void setDuplicateIssueDate(String duplicateIssueDate) {
		this.duplicateIssueDate = duplicateIssueDate;
	}
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCaseNo() {
		return caseNo;
	}
	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}
	public String getPoliceStation() {
		return policeStation;
	}
	public void setPoliceStation(String policeStation) {
		this.policeStation = policeStation;
	}
	public String getOccurrenceDate() {
		return occurrenceDate;
	}
	public void setOccurrenceDate(String occurrenceDate) {
		this.occurrenceDate = occurrenceDate;
	}
	public String getOccurrenceTime() {
		return occurrenceTime;
	}
	public void setOccurrenceTime(String occurrenceTime) {
		this.occurrenceTime = occurrenceTime;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}
	public String getOccurencePlace() {
		return occurencePlace;
	}
	public void setOccurencePlace(String occurencePlace) {
		this.occurencePlace = occurencePlace;
	}
	public String getPersonalOwnerName() {
		return personalOwnerName;
	}
	public void setPersonalOwnerName(String personalOwnerName) {
		this.personalOwnerName = personalOwnerName;
	}
	public String getOwnerType() {
		return OwnerType;
	}
	public void setOwnerType(String ownerType) {
		OwnerType = ownerType;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getBuyerAddress() {
		return buyerAddress;
	}
	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getVehicleRegistrationType() {
		return vehicleRegistrationType;
	}
	public void setVehicleRegistrationType(String vehicleRegistrationType) {
		this.vehicleRegistrationType = vehicleRegistrationType;
	}
	public String getSelectall() {
		return selectall;
	}
	public void setSelectall(String selectall) {
		this.selectall = selectall;
	}
	public String getNewVehicleNo() {
		return newVehicleNo;
	}
	public void setNewVehicleNo(String newVehicleNo) {
		this.newVehicleNo = newVehicleNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTransferorCustomerId() {
		return transferorCustomerId;
	}
	public void setTransferorCustomerId(String transferorCustomerId) {
		this.transferorCustomerId = transferorCustomerId;
	}
	public String getTransfereeCustomerId() {
		return transfereeCustomerId;
	}
	public void setTransfereeCustomerId(String transfereeCustomerId) {
		this.transfereeCustomerId = transfereeCustomerId;
	}
	public String getManualFlag() {
		return manualFlag;
	}
	public void setManualFlag(String manualFlag) {
		this.manualFlag = manualFlag;
	}
	public String getPresentDzongkhag() {
		return presentDzongkhag;
	}
	public void setPresentDzongkhag(String presentDzongkhag) {
		this.presentDzongkhag = presentDzongkhag;
	}
	public String getIssurance() {
		return issurance;
	}
	public void setIssurance(String issurance) {
		this.issurance = issurance;
	}
	public String getNumberOfDeath() {
		return numberOfDeath;
	}
	public void setNumberOfDeath(String numberOfDeath) {
		this.numberOfDeath = numberOfDeath;
	}
	public String getNumberofInjured() {
		return numberofInjured;
	}
	public void setNumberofInjured(String numberofInjured) {
		this.numberofInjured = numberofInjured;
	}
	public String getNumberofunInjured() {
		return numberofunInjured;
	}
	public void setNumberofunInjured(String numberofunInjured) {
		this.numberofunInjured = numberofunInjured;
	}
	public String getRegistrationcertificate() {
		return registrationcertificate;
	}
	public void setRegistrationcertificate(String registrationcertificate) {
		this.registrationcertificate = registrationcertificate;
	}
	
	public String getCertificateOfroadworthiness() {
		return certificateOfroadworthiness;
	}
	public void setCertificateOfroadworthiness(String certificateOfroadworthiness) {
		this.certificateOfroadworthiness = certificateOfroadworthiness;
	}
	public String getEmmissioncertificate() {
		return emmissioncertificate;
	}
	public void setEmmissioncertificate(String emmissioncertificate) {
		this.emmissioncertificate = emmissioncertificate;
	}
	public String getPermitIDNo() {
		return permitIDNo;
	}
	public void setPermitIDNo(String permitIDNo) {
		this.permitIDNo = permitIDNo;
	}
	
	public String getSearchpermitID() {
		return searchpermitID;
	}
	public void setSearchpermitID(String searchpermitID) {
		this.searchpermitID = searchpermitID;
	}
	private String seatCapacity;
	private String receiptNo;
	private String dateOfissue;
	private String validUpto;
	private String searchpermitID;
	
	
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getPermitID() {
		return permitID;
	}
	public void setPermitID(String permitID) {
		this.permitID = permitID;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public String getBaseoffice() {
		return baseoffice;
	}
	public void setBaseoffice(String baseoffice) {
		this.baseoffice = baseoffice;
	}
	public String getRouteBetween() {
		return routeBetween;
	}
	public void setRouteBetween(String routeBetween) {
		this.routeBetween = routeBetween;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getCarryingCapacity() {
		return carryingCapacity;
	}
	public void setCarryingCapacity(String carryingCapacity) {
		this.carryingCapacity = carryingCapacity;
	}
	public String getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(String seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getDateOfissue() {
		return dateOfissue;
	}
	public void setDateOfissue(String dateOfissue) {
		this.dateOfissue = dateOfissue;
	}
	public String getValidUpto() {
		return validUpto;
	}
	public void setValidUpto(String validUpto) {
		this.validUpto = validUpto;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getConductorName() {
		return conductorName;
	}
	public void setConductorName(String conductorName) {
		this.conductorName = conductorName;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getLLNo() {
		return LLNo;
	}
	public void setLLNo(String lLNo) {
		LLNo = lLNo;
	}
	public String getAccidentDate() {
		return accidentDate;
	}
	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}
	public String getAccidentCause() {
		return accidentCause;
	}
	public void setAccidentCause(String accidentCause) {
		this.accidentCause = accidentCause;
	}
	public String getAccidentSite() {
		return accidentSite;
	}
	public void setAccidentSite(String accidentSite) {
		this.accidentSite = accidentSite;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getTitleOfcourtesy() {
		return titleOfcourtesy;
	}
	public void setTitleOfcourtesy(String titleOfcourtesy) {
		this.titleOfcourtesy = titleOfcourtesy;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCID() {
		return CID;
	}
	public void setCID(String cID) {
		CID = cID;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIdentificationMarks() {
		return identificationMarks;
	}
	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getNational() {
		return national;
	}
	public void setNational(String national) {
		this.national = national;
	}
	public String getInternational() {
		return international;
	}
	public void setInternational(String international) {
		this.international = international;
	}
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getGewog() {
		return gewog;
	}
	public void setGewog(String gewog) {
		this.gewog = gewog;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public FormFile getUpload() {
		return upload;
	}
	public void setUpload(FormFile upload) {
		this.upload = upload;
	}

	public EralisCommonDTO getAccidentDriver(int index) 
	{
		while (this.accidentDriverList.size() <= index)
		{
			accidentDriverList.add(new EralisCommonDTO());
		}
		return ((EralisCommonDTO) accidentDriverList.get(index));
	}
	

	public EralisCommonDTO getInjuredPerson(int index) 
	{
		while (this.injuredPersonList.size() <= index)
		{
			injuredPersonList.add(new EralisCommonDTO());
		}
		return ((EralisCommonDTO) injuredPersonList.get(index));
	}
	
	public EralisCommonDTO getKilledPerson(int index) 
	{
		while (this.killedPersonList.size() <= index)
		{
			killedPersonList.add(new EralisCommonDTO());
		}
		return ((EralisCommonDTO) killedPersonList.get(index));
	}
	
	public EralisCommonDTO getAccidentVehicle(int index) 
	{
		while (this.accidentVehicleList.size() <= index)
		{
			accidentVehicleList.add(new EralisCommonDTO());
		}
		return ((EralisCommonDTO) accidentVehicleList.get(index));
	}
	public EralisCommonActionForm()
	{
		accidentDriverList = new ArrayList<EralisCommonDTO>();
		accidentDriverList.add(new EralisCommonDTO());

		accidentVehicleList = new ArrayList<EralisCommonDTO>();
		accidentVehicleList.add(new EralisCommonDTO());

		injuredPersonList = new ArrayList<EralisCommonDTO>();
		injuredPersonList.add(new EralisCommonDTO());

		killedPersonList = new ArrayList<EralisCommonDTO>();
		killedPersonList.add(new EralisCommonDTO());
	}
}
