package bt.gov.rsta.eralis.web.actionform;

import org.apache.struts.action.ActionForm;

public class TasklistAccordionActionForm extends ActionForm {


	private static final long serialVersionUID = 1L;
	private String filterServiceCode;
	private String filterServiceName;
	private String filterStatusCode;
	private String filterStatusName;
	private String limitRows;
	public String getFilterServiceCode() {
		return filterServiceCode;
	}
	public void setFilterServiceCode(String filterServiceCode) {
		this.filterServiceCode = filterServiceCode;
	}
	public String getFilterServiceName() {
		return filterServiceName;
	}
	public void setFilterServiceName(String filterServiceName) {
		this.filterServiceName = filterServiceName;
	}
	public String getFilterStatusCode() {
		return filterStatusCode;
	}
	public void setFilterStatusCode(String filterStatusCode) {
		this.filterStatusCode = filterStatusCode;
	}
	public String getFilterStatusName() {
		return filterStatusName;
	}
	public void setFilterStatusName(String filterStatusName) {
		this.filterStatusName = filterStatusName;
	}
	public String getLimitRows() {
		return limitRows;
	}
	public void setLimitRows(String limitRows) {
		this.limitRows = limitRows;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
