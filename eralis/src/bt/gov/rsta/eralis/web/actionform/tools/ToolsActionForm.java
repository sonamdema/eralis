package bt.gov.rsta.eralis.web.actionform.tools;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ToolsActionForm extends ActionForm
{
	private static final long serialVersionUID = 1L;
	private String duplicateCID;
	private String originalCID;
	private String licenseNumber;
	private String newIID;
	private String drivingLicense;
	private String duplicateLicense;
	private String endorsement;
	private String renewal;
	private String dealerName;
	private String vehicleCompany;
	private String engineCC;
	private String customerName;
	private String citizenID;
	private String vehicleModel;
	private String engineNo;
	private String chasisNo;
	private String yearofManufacture;
	private String countryofManufacture;
	private String invoiceNo;
	private String invoiceDate;
	private String routeFrom;
	private String to;
	private String blockLocation;
	private String blockDate;
	private String openDate;
	private FormFile upload;
	private String region;
	private String vehicleCode;
	private String vehicleNumber;
	private String vehicleType;
	private String engineType;
	private String status;
	private String hypothecatedRadio;
	private String postRadio;
	private String hypothecatedTo;
	private String letterNo;
	private String letterDate;
	private String removeDate;
	private String remarks;
	private String punchNo;
	private String punchDate;
	private String isWithDrawn;
    private String longName;
	private String learnerLicenseNo;
	private String issuedDate;
	private String expiryDate;
	private String base;
	private String nameoftheConductor;
	private String personalInfoId;
	private String oldIID;
	private String endorseDate;
	private String tableName;
	private String row_id;
	private String driveTypeId;
	private String customerId;
	private FormFile photoUrl1;
	private FormFile photoUrl2;
	private FormFile photoUrl3;
	private String isWithdrawn;
	
	private String punchDate1;
	private String letterNo1;
	private String letterDate1;
	private String isWithdrawn1;
	
	private String punchDate2;
	private String letterNo2;
	private String letterDate2;
	private String isWithdrawn2;

	private String punchDate3;
	private String letterNo3;
	private String letterDate3;
	private String isWithdrawn3;
	private String license_Punch_Info_Id;
	private String license_punch_details;
	private String licenseTypeId;
	private int photoId;
	private String licenseId;
	private String punchId;
	private String licenseNo;
	private String learnerNo;
	
	
	public String getLearnerNo() {
		return learnerNo;
	}
	public void setLearnerNo(String learnerNo) {
		this.learnerNo = learnerNo;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getPunchId() {
		return punchId;
	}
	public void setPunchId(String punchId) {
		this.punchId = punchId;
	}
	public String getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(String licenseId) {
		this.licenseId = licenseId;
	}
	public int getPhotoId() {
		return photoId;
	}
	public void setPhotoId(int photoId) {
		this.photoId = photoId;
	}
	public String getLicenseTypeId() {
		return licenseTypeId;
	}
	public void setLicenseTypeId(String licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}
	public String getLicense_punch_details() {
		return license_punch_details;
	}
	public void setLicense_punch_details(String license_punch_details) {
		this.license_punch_details = license_punch_details;
	}
	
	public String getLicense_Punch_Info_Id() {
		return license_Punch_Info_Id;
	}
	public void setLicense_Punch_Info_Id(String license_Punch_Info_Id) {
		this.license_Punch_Info_Id = license_Punch_Info_Id;
	}
	
	
	
	
	 
	 
	public String getIsWithdrawn1() {
		return isWithdrawn1;
	}
	public void setIsWithdrawn1(String isWithdrawn1) {
		this.isWithdrawn1 = isWithdrawn1;
	}
	public String getIsWithdrawn2() {
		return isWithdrawn2;
	}
	public void setIsWithdrawn2(String isWithdrawn2) {
		this.isWithdrawn2 = isWithdrawn2;
	}
	public String getIsWithdrawn3() {
		return isWithdrawn3;
	}
	public void setIsWithdrawn3(String isWithdrawn3) {
		this.isWithdrawn3 = isWithdrawn3;
	}
	public String getPunchDate1() {
		return punchDate1;
	}
	public void setPunchDate1(String punchDate1) {
		this.punchDate1 = punchDate1;
	}
	public String getLetterNo1() {
		return letterNo1;
	}
	public void setLetterNo1(String letterNo1) {
		this.letterNo1 = letterNo1;
	}
	public String getLetterDate1() {
		return letterDate1;
	}
	public void setLetterDate1(String letterDate1) {
		this.letterDate1 = letterDate1;
	}
	public String getPunchDate2() {
		return punchDate2;
	}
	public void setPunchDate2(String punchDate2) {
		this.punchDate2 = punchDate2;
	}
	public String getLetterNo2() {
		return letterNo2;
	}
	public void setLetterNo2(String letterNo2) {
		this.letterNo2 = letterNo2;
	}
	public String getLetterDate2() {
		return letterDate2;
	}
	public void setLetterDate2(String letterDate2) {
		this.letterDate2 = letterDate2;
	}
	public String getPunchDate3() {
		return punchDate3;
	}
	public void setPunchDate3(String punchDate3) {
		this.punchDate3 = punchDate3;
	}
	public String getLetterNo3() {
		return letterNo3;
	}
	public void setLetterNo3(String letterNo3) {
		this.letterNo3 = letterNo3;
	}
	public String getLetterDate3() {
		return letterDate3;
	}
	public void setLetterDate3(String letterDate3) {
		this.letterDate3 = letterDate3;
	}
	
	public String getIsWithdrawn() {
		return isWithdrawn;
	}
	public void setIsWithdrawn(String isWithdrawn) {
		this.isWithdrawn = isWithdrawn;
	}
	public FormFile getPhotoUrl1() {
		return photoUrl1;
	}
	public void setPhotoUrl1(FormFile photoUrl1) {
		this.photoUrl1 = photoUrl1;
	}
	public FormFile getPhotoUrl2() {
		return photoUrl2;
	}
	public void setPhotoUrl2(FormFile photoUrl2) {
		this.photoUrl2 = photoUrl2;
	}
	public FormFile getPhotoUrl3() {
		return photoUrl3;
	}
	public void setPhotoUrl3(FormFile photoUrl3) {
		this.photoUrl3 = photoUrl3;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDriveTypeId() {
		return driveTypeId;
	}
	public void setDriveTypeId(String driveTypeId) {
		this.driveTypeId = driveTypeId;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getRow_id() {
		return row_id;
	}
	public void setRow_id(String row_id) {
		this.row_id = row_id;
	}
	public String getEndorseDate() {
		return endorseDate;
	}
	public void setEndorseDate(String endorseDate) {
		this.endorseDate = endorseDate;
	}
	public String getOldIID() {
		return oldIID;
	}
	public void setOldIID(String oldIID) {
		this.oldIID = oldIID;
	}
	public String getPostRadio() {
		return postRadio;
	}
	public void setPostRadio(String postRadio) {
		this.postRadio = postRadio;
	}
	public String getHypothecatedRadio() {
		return hypothecatedRadio;
	}
	public void setHypothecatedRadio(String hypothecatedRadio) {
		this.hypothecatedRadio = hypothecatedRadio;
	}
	public String getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getNameoftheConductor() {
		return nameoftheConductor;
	}
	public void setNameoftheConductor(String nameoftheConductor) {
		this.nameoftheConductor = nameoftheConductor;
	}
	public String getIsWithDrawn() {
		return isWithDrawn;
	}
	public void setIsWithDrawn(String isWithDrawn) {
		this.isWithDrawn = isWithDrawn;
	}
	public String getLongName() {
		return longName;
	}
	public void setLongName(String longName) {
		this.longName = longName;
	}
	public String getLearnerLicenseNo() {
		return learnerLicenseNo;
	}
	public void setLearnerLicenseNo(String learnerLicenseNo) {
		this.learnerLicenseNo = learnerLicenseNo;
	}
	public String getPunchNo() {
		return punchNo;
	}
	public void setPunchNo(String punchNo) {
		this.punchNo = punchNo;
	}
	public String getPunchDate() {
		return punchDate;
	}
	public void setPunchDate(String punchDate) {
		this.punchDate = punchDate;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
	public String getHypothecatedTo() {
		return hypothecatedTo;
	}
	public void setHypothecatedTo(String hypothecatedTo) {
		this.hypothecatedTo = hypothecatedTo;
	}
	public String getLetterNo() {
		return letterNo;
	}
	public void setLetterNo(String letterNo) {
		this.letterNo = letterNo;
	}
	public String getLetterDate() {
		return letterDate;
	}
	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}
	public String getRemoveDate() {
		return removeDate;
	}
	public void setRemoveDate(String removeDate) {
		this.removeDate = removeDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getEngineType() {
		return engineType;
	}
	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleCode() {
		return vehicleCode;
	}
	public void setVehicleCode(String vehicleCode) {
		this.vehicleCode = vehicleCode;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public FormFile getUpload() {
		return upload;
	}
	public void setUpload(FormFile upload) {
		this.upload = upload;
	}
	public String getBlockLocation() {
		return blockLocation;
	}
	public void setBlockLocation(String blockLocation) {
		this.blockLocation = blockLocation;
	}
	public String getBlockDate() {
		return blockDate;
	}
	public void setBlockDate(String blockDate) {
		this.blockDate = blockDate;
	}
	public String getOpenDate() {
		return openDate;
	}
	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	public String getRouteFrom() {
		return routeFrom;
	}
	public void setRouteFrom(String routeFrom) {
		this.routeFrom = routeFrom;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getEngineNo() {
		return engineNo;
	}
	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}
	public String getChasisNo() {
		return chasisNo;
	}
	public void setChasisNo(String chasisNo) {
		this.chasisNo = chasisNo;
	}
	public String getYearofManufacture() {
		return yearofManufacture;
	}
	public void setYearofManufacture(String yearofManufacture) {
		this.yearofManufacture = yearofManufacture;
	}
	public String getCountryofManufacture() {
		return countryofManufacture;
	}
	public void setCountryofManufacture(String countryofManufacture) {
		this.countryofManufacture = countryofManufacture;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCitizenID() {
		return citizenID;
	}
	public void setCitizenID(String citizenID) {
		this.citizenID = citizenID;
	}
	
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getVehicleCompany() {
		return vehicleCompany;
	}
	public void setVehicleCompany(String vehicleCompany) {
		this.vehicleCompany = vehicleCompany;
	}
	public String getDrivingLicense() {
		return drivingLicense;
	}
	public void setDrivingLicense(String drivingLicense) {
		this.drivingLicense = drivingLicense;
	}
	public String getDuplicateLicense() {
		return duplicateLicense;
	}
	public void setDuplicateLicense(String duplicateLicense) {
		this.duplicateLicense = duplicateLicense;
	}
	public String getEndorsement() {
		return endorsement;
	}
	public void setEndorsement(String endorsement) {
		this.endorsement = endorsement;
	}
	public String getRenewal() {
		return renewal;
	}
	public void setRenewal(String renewal) {
		this.renewal = renewal;
	}
	
	public String getNewIID() {
		return newIID;
	}
	public void setNewIID(String newIID) {
		this.newIID = newIID;
	}
	
	public String getDuplicateCID() {
		return duplicateCID;
	}
	public void setDuplicateCID(String duplicateCID) {
		this.duplicateCID = duplicateCID;
	}
	public String getOriginalCID() {
		return originalCID;
	}
	public void setOriginalCID(String originalCID) {
		this.originalCID = originalCID;
	}
	//TOOLS
	public String getPersonalInfoId() {
		return personalInfoId;
	}
	public void setPersonalInfoId(String personalInfoId) {
		this.personalInfoId = personalInfoId;
	}
	

}
