package bt.gov.rsta.eralis.web.actionform.administration;

import java.util.ArrayList;
import java.util.List;
import org.apache.struts.action.ActionForm;
import bt.gov.rsta.eralis.dto.administration.PagePermissionDTO;

public class AdministrationActionForm extends ActionForm
{
	private static final long serialVersionUID = 1L;
	
	public AdministrationActionForm()
	{
		permissionList = new ArrayList<PagePermissionDTO>();
		permissionList.add(new PagePermissionDTO());
	}
	
	private String loginId;
	private String userName;
	private String emailId;
	private String mobileNumber;
	private String region;
	private String dzongkhag;
	private String role;
	private String isActiveCheckBox;
	private List<PagePermissionDTO> permissionList;
	private String regionDesc;
	private String regionNumber;
	private String[] roleList;
	private String gewog;
	private String ministry;
	private String showTaskList;
	private String base;
	private String jurisType;
	private String agencyCode;
	private String userType;
	
	private String masterId;
	private String masterName;
	private String masterDesc;
	private String masterOtherId;
	private String masterNumber;
	private String master_identifier;
	
	private String mailConfigId;
	private String smtpHost;
	private String smtpPort;
	private String debugFlag;
	private String ttlsFlag;
	private String senderAddress;
	private String senderPassword;
	private String smtpAuthFlag;
	
	private String vehicleHorsePower;
	private String vehicleKiloWatt;
	private String engineCC;
	private String vPrefix;
	private String vehicleConstantId;
	private String flag;
	private String rcCost;
	private String fitnessAmount;
	private String ownershipTransferFees;
	private String regionalTransferFee;
	private String conversionFee;
	private String transferTax;
	private String loadCapacity;
	private String seatCapacity;
	private String amount;
	private String licenseConstantId;
	private String requestType;
	private String serviceType;
	private String licenseCardCost;
	private String licensetype;
	private String penaltyPerDay;
	private String maxPenalty;
	private String penaltyId;
	private String unladenWeight;
	
	public String getUnladenWeight() {
		return unladenWeight;
	}
	public void setUnladenWeight(String unladenWeight) {
		this.unladenWeight = unladenWeight;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getJurisType() {
		return jurisType;
	}
	public void setJurisType(String jurisType) {
		this.jurisType = jurisType;
	}
	public String getAgencyCode() {
		return agencyCode;
	}
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getShowTaskList() {
		return showTaskList;
	}
	public void setShowTaskList(String showTaskList) {
		this.showTaskList = showTaskList;
	}
	public String getPenaltyId() {
		return penaltyId;
	}
	public void setPenaltyId(String penaltyId) {
		this.penaltyId = penaltyId;
	}
	public String getMaxPenalty() {
		return maxPenalty;
	}
	public void setMaxPenalty(String maxPenalty) {
		this.maxPenalty = maxPenalty;
	}
	public String getPenaltyPerDay() {
		return penaltyPerDay;
	}
	public void setPenaltyPerDay(String penaltyPerDay) {
		this.penaltyPerDay = penaltyPerDay;
	}
	public String getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(String licensetype) {
		this.licensetype = licensetype;
	}
	public String getLicenseConstantId() {
		return licenseConstantId;
	}
	public void setLicenseConstantId(String licenseConstantId) {
		this.licenseConstantId = licenseConstantId;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getLicenseCardCost() {
		return licenseCardCost;
	}
	public void setLicenseCardCost(String licenseCardCost) {
		this.licenseCardCost = licenseCardCost;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(String seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getLoadCapacity() {
		return loadCapacity;
	}
	public void setLoadCapacity(String loadCapacity) {
		this.loadCapacity = loadCapacity;
	}
	public String getVehicleHorsePower() {
		return vehicleHorsePower;
	}
	public void setVehicleHorsePower(String vehicleHorsePower) {
		this.vehicleHorsePower = vehicleHorsePower;
	}
	public String getVehicleKiloWatt() {
		return vehicleKiloWatt;
	}
	public void setVehicleKiloWatt(String vehicleKiloWatt) {
		this.vehicleKiloWatt = vehicleKiloWatt;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getvPrefix() {
		return vPrefix;
	}
	public void setvPrefix(String vPrefix) {
		this.vPrefix = vPrefix;
	}
	public String getVehicleConstantId() {
		return vehicleConstantId;
	}
	public void setVehicleConstantId(String vehicleConstantId) {
		this.vehicleConstantId = vehicleConstantId;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getRcCost() {
		return rcCost;
	}
	public void setRcCost(String rcCost) {
		this.rcCost = rcCost;
	}
	public String getFitnessAmount() {
		return fitnessAmount;
	}
	public void setFitnessAmount(String fitnessAmount) {
		this.fitnessAmount = fitnessAmount;
	}
	public String getOwnershipTransferFees() {
		return ownershipTransferFees;
	}
	public void setOwnershipTransferFees(String ownershipTransferFees) {
		this.ownershipTransferFees = ownershipTransferFees;
	}
	public String getRegionalTransferFee() {
		return regionalTransferFee;
	}
	public void setRegionalTransferFee(String regionalTransferFee) {
		this.regionalTransferFee = regionalTransferFee;
	}
	public String getConversionFee() {
		return conversionFee;
	}
	public void setConversionFee(String conversionFee) {
		this.conversionFee = conversionFee;
	}
	public String getTransferTax() {
		return transferTax;
	}
	public void setTransferTax(String transferTax) {
		this.transferTax = transferTax;
	}
	public String getMailConfigId() {
		return mailConfigId;
	}
	public void setMailConfigId(String mailConfigId) {
		this.mailConfigId = mailConfigId;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getDebugFlag() {
		return debugFlag;
	}
	public void setDebugFlag(String debugFlag) {
		this.debugFlag = debugFlag;
	}
	public String getTtlsFlag() {
		return ttlsFlag;
	}
	public void setTtlsFlag(String ttlsFlag) {
		this.ttlsFlag = ttlsFlag;
	}
	public String getSenderAddress() {
		return senderAddress;
	}
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}
	public String getSenderPassword() {
		return senderPassword;
	}
	public void setSenderPassword(String senderPassword) {
		this.senderPassword = senderPassword;
	}
	public String getSmtpAuthFlag() {
		return smtpAuthFlag;
	}
	public void setSmtpAuthFlag(String smtpAuthFlag) {
		this.smtpAuthFlag = smtpAuthFlag;
	}
	public String getMasterName() {
		return masterName;
	}
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}
	public String getMasterDesc() {
		return masterDesc;
	}
	public void setMasterDesc(String masterDesc) {
		this.masterDesc = masterDesc;
	}
	public String getMasterOtherId() {
		return masterOtherId;
	}
	public void setMasterOtherId(String masterOtherId) {
		this.masterOtherId = masterOtherId;
	}
	public String getMasterNumber() {
		return masterNumber;
	}
	public void setMasterNumber(String masterNumber) {
		this.masterNumber = masterNumber;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public String getGewog() {
		return gewog;
	}
	public void setGewog(String gewog) {
		this.gewog = gewog;
	}
	public String[] getRoleList() {
		return roleList;
	}
	public void setRoleList(String[] roleList) {
		this.roleList = roleList;
	}
	public String getMasterId() {
		return masterId;
	}
	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}
	public String getMaster_identifier() {
		return master_identifier;
	}
	public void setMaster_identifier(String master_identifier) {
		this.master_identifier = master_identifier;
	}
	public String getRegionDesc() {
		return regionDesc;
	}
	public void setRegionDesc(String regionDesc) {
		this.regionDesc = regionDesc;
	}
	public String getRegionNumber() {
		return regionNumber;
	}
	public void setRegionNumber(String regionNumber) {
		this.regionNumber = regionNumber;
	}
	public List<PagePermissionDTO> getPermissionList() {
		return permissionList;
	}
	public void setPermissionList(List<PagePermissionDTO> permissionList) {
		this.permissionList = permissionList;
	}
	public String getIsActiveCheckBox() {
		return isActiveCheckBox;
	}
	public void setIsActiveCheckBox(String isActiveCheckBox) {
		this.isActiveCheckBox = isActiveCheckBox;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	public PagePermissionDTO getCheckBox(int index)
	{
		while(this.permissionList.size() <= index)
		{
			permissionList.add(new PagePermissionDTO());
		}
		return ((PagePermissionDTO) permissionList.get(index));
	}
}
