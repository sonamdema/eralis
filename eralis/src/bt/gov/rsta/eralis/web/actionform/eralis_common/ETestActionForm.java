package bt.gov.rsta.eralis.web.actionform.eralis_common;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ETestActionForm extends ActionForm
{
	private static final long serialVersionUID = 1L;
	private String slNo;
	private String testDate;
	private String maxApplicants;
	private String testLocation;
	private String isActive;
	private String addTestDate;
	private String question;
	private String isAnswer;
	private String questionId;
	private String testDuration;
	private String testType;
	private String instituteName;
	private FormFile upload;
	private String rstaMaxApplicant;
	
	
	
	public FormFile getUpload() {
		return upload;
	}
	public void setUpload(FormFile upload) {
		this.upload = upload;
	}
	public String getRstaMaxApplicant() {
		return rstaMaxApplicant;
	}
	public void setRstaMaxApplicant(String rstaMaxApplicant) {
		this.rstaMaxApplicant = rstaMaxApplicant;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	public String getTestDuration() {
		return testDuration;
	}
	public void setTestDuration(String testDuration) {
		this.testDuration = testDuration;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getIsAnswer() {
		return isAnswer;
	}
	public void setIsAnswer(String isAnswer) {
		this.isAnswer = isAnswer;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAddTestDate() {
		return addTestDate;
	}
	public void setAddTestDate(String addTestDate) {
		this.addTestDate = addTestDate;
	}
	public String getSlNo() {
		return slNo;
	}
	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}
	public String getTestDate() {
		return testDate;
	}
	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}
	public String getMaxApplicants() {
		return maxApplicants;
	}
	public void setMaxApplicants(String maxApplicants) {
		this.maxApplicants = maxApplicants;
	}
	public String getTestLocation() {
		return testLocation;
	}
	public void setTestLocation(String testLocation) {
		this.testLocation = testLocation;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
}
