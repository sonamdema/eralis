package bt.gov.rsta.eralis.web.action;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import bt.gov.rsta.eralis.dto.StatusDTO;
import bt.gov.rsta.eralis.dto.TaskListServiceDTO;
import bt.gov.rsta.eralis.web.actionform.TasklistAccordionActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Priviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.dto.Service;
import bt.gov.rsta.framework.dto.WorkListDetailsDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;

public class TaskListAction extends Action
{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			TasklistAccordionActionForm actionForm = (TasklistAccordionActionForm)form;
			
			String totalLicenseAlert = CommonBusiness.getInstance().getTotalLicenseAlert();
			request.setAttribute("TOTAL_LICENSE_ALERT", totalLicenseAlert);
			
			String totalApplicationAlert = CommonBusiness.getInstance().getTotalApplicationAlert();
			request.setAttribute("TOTAL_APPLICATION_ALERT", totalApplicationAlert);
			
			String q = request.getParameter("q");
			
			if(q == null){
				String returnMapping = populateTaskList(request, response, userRolePriv, "10");
				return mapping.findForward(returnMapping);
			}
			if(q.equalsIgnoreCase("populateStatus")){
				populateStatus(request,response);				
			}
			if(q.equalsIgnoreCase("claimApplication"))
				claimApplication(request, response, userRolePriv);
			if(q.equalsIgnoreCase("releaseApplication"))
				releaseApplication(request, response, userRolePriv);
			if(q.equalsIgnoreCase("filterApplication"))
				filterApplication(request, response, userRolePriv, actionForm.getFilterServiceCode(), actionForm.getFilterStatusCode(), actionForm.getLimitRows());	
			
			if(q.equalsIgnoreCase("filterApplication"))
				actionForward = "tasklist-content";
			else
				actionForward = "tasklist";
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.error("Error TaskListAction[execute]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}                                
	
	private String populateTaskList(HttpServletRequest request, HttpServletResponse response, EralisUserRolePriviledge userRolePriv, String limit) throws Exception
	{
		HttpSession session = request.getSession();
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try
		{
			if(userRolePriv==null || userRolePriv.getRoles()==null || userRolePriv.getRoles().length == 0)
				return "GLOBAL_REDIRECT_LOGIN";
			
			Role currentRole = userRolePriv.getCurrentRole();
			
			if(StringUtils.isEmpty(limit))
				limit = "50";
			
			StringBuffer privQuery = new StringBuffer();
			int nLoop = 0;
			Service[] serviceArray = currentRole.getServices();
			
			/*
			 * Populating the group task
			 */
			for(Service service:serviceArray)
			{
				String serviceID = service.getServiceId();
				//privs for that service
				Priviledge[] privilegeArray = service.getPriviledges();
				for(Priviledge privilege:privilegeArray)
				{
					//get the total list of assigned privs for that user
					String privShortCode = privilege.getPriviledgeCode();
					if(nLoop>0){
						privQuery.append(" union ");
					}
					nLoop++;
					privQuery.append(" SELECT `Priv_Id` FROM `t_privilege_master` WHERE Service_Id = "+serviceID+" AND Priv_Short_Desc='"+privShortCode+"' ");
				}
			}
			
			nLoop=0;
			StringBuffer privIds =new StringBuffer( "(");
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(privQuery.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				if(nLoop>0){
					privIds.append(" , ");
				}
				nLoop++;
				privIds.append(rs.getString("Priv_Id"));
			}
			privIds.append(")");
			
			ConnectionManager.close(null, null, rs, pst);
			
			//populate all the group tasks respective of jurisdiction
			int count = 0;
			StringBuilder query = new StringBuilder("");
			Service[] svcs = userRolePriv.getCurrentRole().getServices();
			for(int i=0; i<svcs.length; i++)
			{
				Service svc = svcs[i];
				Priviledge[] privs = svc.getPriviledges();
				for(int j=0; j<privs.length; j++)
				{
					Priviledge priv = privs[j];
					if(count > 0)
					{
						query.append(" UNION ");
					}
					
					query.append(GET_ALL_WORKLIST_COMMON_1 
							+ priv.getPriviledgeCode() + GET_ALL_WORKLIST_COMMON_2 
							+ svc.getServiceCode() + GET_ALL_WORKLIST_COMMON_3 
							+ svc.getServiceCode() + GET_ALL_WORKLIST_COMMON_4 
							+ userRolePriv.getJurisdictionId() + GET_ALL_WORKLIST_COMMON_5
							+ userRolePriv.getJurisdictionTypeId());	
					count ++;
				}
			}
			System.out.println(query.toString());
			pst = conn.prepareStatement(query.toString());
			rs = pst.executeQuery();
			ArrayList<WorkListDetailsDTO> groupTaskListAll = new ArrayList<WorkListDetailsDTO>();
			ArrayList<WorkListDetailsDTO> groupTaskList = new ArrayList<WorkListDetailsDTO>();
			while(rs.next())
			{
				WorkListDetailsDTO dto = new WorkListDetailsDTO();
				
				

				dto.setServiceTypeNo(rs.getString("serviceTypeNo"));
				dto.setCustomerId(rs.getString("customer_Id"));
				dto.setCustomerName(rs.getString("customer_Name"));
				 
				dto.setAppNo(rs.getString("Application_Number"));
				dto.setServiceName(rs.getString("Service_Name"));
				dto.setServiceCode(rs.getString("Service_Short_Desc"));
				dto.setLastUpdatedDate(rs.getString("action_date"));
				dto.setStatusCode(rs.getString("Status_Type_Short_Desc"));
				dto.setStatusName(rs.getString("Status_Name"));
				dto.setActorName(rs.getString("Actor_Name"));
				groupTaskListAll.add(dto);	
			}
			
			groupTaskList = groupTaskListAll;
			
			/**
			 * Limit Rows
			 */
		    List<WorkListDetailsDTO> groupTaskSublist ;
			int len = groupTaskList.size();
			if(len>Integer.parseInt(limit)){
				 groupTaskSublist = groupTaskList.subList(0, Integer.parseInt(limit));
			}else{
				groupTaskSublist = groupTaskList;
			}
			session.setAttribute("groupTaskList", groupTaskSublist);
			
			ConnectionManager.close(null, null, rs, pst);
			
			/*
			 * Populating the own task list
			 */
			ArrayList<WorkListDetailsDTO> ownTaskListAll = new ArrayList<WorkListDetailsDTO>();
			
			int counter = 0;
			StringBuilder ownQuery = new StringBuilder("");
			Service[] svcs1 = userRolePriv.getCurrentRole().getServices();
			for(int i=0; i<svcs1.length; i++)
			{
				Service svc = svcs1[i];
				Priviledge[] privs = svc.getPriviledges();
				for(int j=0; j<privs.length; j++)
				{
					Priviledge priv = privs[j];
					if(counter > 0)
					{
						ownQuery.append(" UNION ");
					}
					ownQuery.append(GET_ALL_WORKLIST_OWN_1 + priv.getPriviledgeCode() + GET_ALL_WORKLIST_OWN_2 
							+ svc.getServiceCode() + GET_ALL_WORKLIST_OWN_3 + svc.getServiceCode() 
							+ GET_ALL_WORKLIST_OWN_4 + userRolePriv.getUserId() + GET_ALL_WORKLIST_OWN_5);
					counter ++;
				}
			}
			
			pst = conn.prepareStatement(ownQuery.toString());
			rs = pst.executeQuery();
			while(rs.next())
			{
				WorkListDetailsDTO dto = new WorkListDetailsDTO();
				dto.setServiceTypeNo(rs.getString("serviceTypeNo"));
				dto.setCustomerId(rs.getString("customer_Id"));
				dto.setCustomerName(rs.getString("customer_Name"));
				dto.setAppNo(rs.getString("Application_Number"));
				dto.setServiceName(rs.getString("Service_Name"));
				dto.setServiceCode(rs.getString("Service_Short_Desc"));
				dto.setLastUpdatedDate(rs.getString("action_date"));
				dto.setStatusCode(rs.getString("Status_Type_Short_Desc"));
				dto.setStatusName(rs.getString("Status_Name"));
				dto.setActorName(rs.getString("Actor_Name"));
				ownTaskListAll.add(dto);	
			}
			
			/**
			 * Limit Rows
			 */
		    List<WorkListDetailsDTO> myTaskSublist ;
			int lenMyTask = ownTaskListAll.size();
			if(lenMyTask>Integer.parseInt(limit)){
				myTaskSublist = ownTaskListAll.subList(0, Integer.parseInt(limit));
			}else{
				myTaskSublist = ownTaskListAll;
			}
			session.setAttribute("ownTaskList", myTaskSublist);
			populateServiceStatusMap(groupTaskList,request);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Log.error("Error occurred TaskListAction[populateTaskList]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return "tasklist";
	}
	
	public void populateServiceStatusMap(List<WorkListDetailsDTO> works, HttpServletRequest request) 
	{
		List<TaskListServiceDTO> servicesStatusMap = new ArrayList<TaskListServiceDTO>();
		if(works!=null){
			for(WorkListDetailsDTO work : works){
				String svcCode = work.getServiceCode();
				boolean serviceExists = false;
				for(TaskListServiceDTO svc : servicesStatusMap){
					if(svc.getServiceCode().trim().equalsIgnoreCase(svcCode)){
						//this means the service already exists in the list
						//therefore, we need to add only the status to the existing service object
						//this block will not increase the size of services list, only the relevant status list size will increase 
						StatusDTO status = new StatusDTO();
						status.setStatusCode(work.getStatusCode());
						status.setStatusName(work.getStatusName());
						
						svc.getStatusList().add(status);
						serviceExists = true;	//if this line executes, next if block will not be addressed
					}
				}
				if(!serviceExists){
					//this is for a new service will be added to the list
					TaskListServiceDTO svc = new TaskListServiceDTO();
					svc.setServiceCode(work.getServiceCode());
					svc.setServiceName(work.getServiceName());
					
					StatusDTO status = new StatusDTO();
					status.setStatusCode(work.getStatusCode());
					status.setStatusName(work.getStatusName());
					
					List<StatusDTO> statusList = new ArrayList<StatusDTO>();
					statusList.add(status);
					
					svc.setStatusList(statusList);
					servicesStatusMap.add(svc);
				}
			}
		}
		
		request.getSession().setAttribute("svc_status_list", servicesStatusMap);
	}
	
	private void claimApplication(HttpServletRequest request, HttpServletResponse response, EralisUserRolePriviledge userRolePriv) throws Exception
	{
		PrintWriter out = response.getWriter();
		String userId = userRolePriv.getUserId();
		String applicationNumber = request.getParameter("applicationNumber");
		String statusCode = request.getParameter("statusCode");
		StringBuffer buffer = new StringBuffer();
		
		//check if the task status is already claimed
		boolean isTaskStateOK = false;
		boolean isWorkflowStateOK = false;
		Connection connection = ConnectionManager.getConnection();
		connection.setAutoCommit(false);
		PreparedStatement preparedStatement = connection.prepareStatement(CHECK_TASK_STATUS_DESC);
		preparedStatement.setString(1,applicationNumber);
    	ResultSet resultSet = preparedStatement.executeQuery();
    	
    	if(resultSet.first()){
    	    String taskState = resultSet.getString("Status_Type_Short_Desc");
    	    if(taskState!=null && taskState.equalsIgnoreCase("CLAIMED")){
    	    	buffer.append("<xml-response>");
    	    	buffer.append("<status>");
				buffer.append("<status-code>" + "ERR" + "</status-code>");
				buffer.append("<status-name>" + "Application Number ["+applicationNumber+"] has already been claimed by ["+resultSet.getString("Assigned_User_Id")+"] " + "</status-name>");
				buffer.append("</status>");			
				buffer.append("</xml-response>");
    	    }
    	    else if(taskState!=null && taskState.equalsIgnoreCase("INITIATED")){
				isTaskStateOK = true;
    	    }
    	}
    	
    	//if isTaskStateOK is true then we have to make sure that the application is in the same WF state as the passed parameter
        if(isTaskStateOK){
    	preparedStatement = connection.prepareStatement(CHECK_WORKFLOW_STATUS_DESC);
		preparedStatement.setString(1,applicationNumber);
		resultSet = preparedStatement.executeQuery();
		if(resultSet.first()){
			String workflowState = resultSet.getString("Status_Type_Short_Desc");
			if(workflowState!=null && !workflowState.equalsIgnoreCase(statusCode)){
				buffer.append("<xml-response>");
    	    	buffer.append("<status>");
				buffer.append("<status-code>" + "ERR" + "</status-code>");
				buffer.append("<status-name>" + "Application Number ["+applicationNumber+"] has already been processed by another user. Kindly process a different application." + "</status-name>");
				buffer.append("</status>");
				buffer.append("</xml-response>");
			}
    	    else if(workflowState!=null && workflowState.equalsIgnoreCase(statusCode)){
				isWorkflowStateOK = true;
    	    }
			}
		}
    	
    	if(isTaskStateOK && isWorkflowStateOK){
        	//insert into task audit Table 
	    	preparedStatement = connection.prepareStatement(INSERT_TASK_AUDIT_FOR_CLAIMING);
	    	preparedStatement.setString(1, applicationNumber);
	    	preparedStatement.execute();
	    	int updatedCount = preparedStatement.getUpdateCount();
	    	if(updatedCount > 0){
	    		//next, update the status of the workflow table
	    		preparedStatement = connection.prepareStatement(UPDATE_TASK_FOR_CLAIMING);
	    		preparedStatement.setString(1, "CLAIMED");
	    		preparedStatement.setString(2, userId);
	    		preparedStatement.setString(3, applicationNumber);
		    	updatedCount = preparedStatement.executeUpdate();
		    	
		    	if(updatedCount > 0){
		    		connection.commit();
		            buffer.append("<xml-response>");
		        	buffer.append("<status>");
		    		buffer.append("<status-code>" + "OK" + "</status-code>");
		    		buffer.append("<status-name>" + "Application Number ["+applicationNumber+"] has been assigned to you " + "</status-name>");
		    		buffer.append("</status>");	
		    		buffer.append("</xml-response>");
		    	}else{
		    		connection.rollback();
		    	}
	    	}else{
	    		connection.rollback();
	    	}
        }
    	
    	ConnectionManager.close(connection, null, resultSet, preparedStatement);
		response.setContentType("text/xml");
		out.println(buffer);
		out.flush();
		out.close(); 
	}
	
	private void releaseApplication(HttpServletRequest request, HttpServletResponse response, EralisUserRolePriviledge userRolePriv) throws Exception
	{
		PrintWriter out = response.getWriter();
		String applicationNumber = request.getParameter("applicationNumber");
		String statusCode = request.getParameter("statusCode");
		StringBuffer buffer = new StringBuffer();
		
		//check if the task status is already unclaimed
		boolean isTaskStateOK = false;
		boolean isWorkflowStateOK = false;
		Connection connection = ConnectionManager.getConnection();
		connection.setAutoCommit(false);
		PreparedStatement preparedStatement = connection.prepareStatement(CHECK_TASK_STATUS_DESC);
		preparedStatement.setString(1,applicationNumber);
    	ResultSet resultSet = preparedStatement.executeQuery();
    	
    	if(resultSet.first()){
    	    String taskState = resultSet.getString("Status_Type_Short_Desc");
    	    if(taskState!=null && taskState.equalsIgnoreCase("CLAIMED")){
				isTaskStateOK = true;
    	    }
    	    else if(taskState!=null && taskState.equalsIgnoreCase("INITIATED")){
    	    	buffer.append("<xml-response>");
    	    	buffer.append("<status>");
				buffer.append("<status-code>" + "ERR" + "</status-code>");
				buffer.append("<status-name>" + "Application Number ["+applicationNumber+"] has already been unassigned earlier. " + "</status-name>");
				buffer.append("</status>");			
				buffer.append("</xml-response>");
    	    }
    	}
    	
    	//if isTaskStateOK is true then we have to make sure that the application is in the same WF state as the passed parameter
        if(isTaskStateOK){
    	preparedStatement = connection.prepareStatement(CHECK_WORKFLOW_STATUS_DESC);
		preparedStatement.setString(1,applicationNumber);
		resultSet = preparedStatement.executeQuery();
		if(resultSet.first()){
			String workflowState = resultSet.getString("Status_Type_Short_Desc");
			if(workflowState!=null && !workflowState.equalsIgnoreCase(statusCode)){
				buffer.append("<xml-response>");
    	    	buffer.append("<status>");
				buffer.append("<status-code>" + "ERR" + "</status-code>");
				buffer.append("<status-name>" + "Application Number ["+applicationNumber+"] has already been processed by another user. Kindly process a different application." + "</status-name>");
				buffer.append("</status>");
				buffer.append("</xml-response>");
			}
    	    else if(workflowState!=null && workflowState.equalsIgnoreCase(statusCode)){
				isWorkflowStateOK = true;
    	    }
			}
		}
        
        if(isTaskStateOK && isWorkflowStateOK){
        	//insert into task audit Table 
	    	preparedStatement = connection.prepareStatement(INSERT_TASK_AUDIT_FOR_CLAIMING);
	    	preparedStatement.setString(1, applicationNumber);
	    	preparedStatement.execute();
	    	int updatedCount = preparedStatement.getUpdateCount();
	    	if(updatedCount > 0){
	    		//next, update the status of the workflow table
	    		preparedStatement = connection.prepareStatement(UPDATE_TASK_FOR_CLAIMING);
	    		preparedStatement.setString(1, "INITIATED");
	    		preparedStatement.setString(2, null);
	    		preparedStatement.setString(3, applicationNumber);
		    	updatedCount = preparedStatement.executeUpdate();
		    	
		    	if(updatedCount > 0){
		    		connection.commit();
		            buffer.append("<xml-response>");
		        	buffer.append("<status>");
		    		buffer.append("<status-code>" + "OK" + "</status-code>");
		    		buffer.append("<status-name>" + "Application Number ["+applicationNumber+"] has been unassigned and sent to group task " + "</status-name>");
		    		buffer.append("</status>");	
		    		buffer.append("</xml-response>");
		    	}else{
		    		connection.rollback();
		    	}
	    	}else{
	    		connection.rollback();
	    	}
        }
		ConnectionManager.close(connection, null, resultSet, preparedStatement);
		response.setContentType("text/xml");
		out.println(buffer);
		out.flush();
		out.close();
	}
	
	private void filterApplication( HttpServletRequest request, HttpServletResponse response,EralisUserRolePriviledge userRolePriv, String serviceCode,String status, String limit)throws Exception
	{
		populateTaskList(request, response, userRolePriv, limit);
		HttpSession session = request.getSession();
		//filter group task list
		List<WorkListDetailsDTO> groupTaskListAll = (List<WorkListDetailsDTO>) session.getAttribute("groupTaskList");
		ArrayList<WorkListDetailsDTO> filteredGroupTaskList = new ArrayList<WorkListDetailsDTO>();
		for(WorkListDetailsDTO dto:groupTaskListAll){
			if(!StringUtils.isEmpty(serviceCode)){
				if(!StringUtils.isEmpty(status)){
					if(serviceCode.equalsIgnoreCase(dto.getServiceCode()) && status.equalsIgnoreCase(dto.getStatusCode())){
						filteredGroupTaskList.add(dto);
					}
				}else{
					if(serviceCode.equalsIgnoreCase(dto.getServiceCode())){
						filteredGroupTaskList.add(dto);
					}
				}
			}else{
				filteredGroupTaskList.add(dto);
			}
		}
		session.setAttribute("groupTaskList", filteredGroupTaskList);
		
		//filter my task list
		List<WorkListDetailsDTO> ownTaskListAll = (List<WorkListDetailsDTO>) session.getAttribute("ownTaskList");
		ArrayList<WorkListDetailsDTO> filteredOwnTaskListAll = new ArrayList<WorkListDetailsDTO>();
		for(WorkListDetailsDTO dto:ownTaskListAll){
			if(!StringUtils.isEmpty(serviceCode)){
				if(!StringUtils.isEmpty(status)){
					if(serviceCode.equalsIgnoreCase(dto.getServiceCode()) && status.equalsIgnoreCase(dto.getStatusCode())){
						filteredOwnTaskListAll.add(dto);
					}
				}else{
					if(serviceCode.equalsIgnoreCase(dto.getServiceCode())){
						filteredOwnTaskListAll.add(dto);
					}
				}
			}else{
				filteredOwnTaskListAll.add(dto);
			}
		}
		session.setAttribute("ownTaskList", filteredOwnTaskListAll);
	}
	
	private void populateStatus( HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		//Log.debug("TasklistAccordionAction#populateStatus");
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<xml-response>");
		String selectedServiceCode=request.getParameter("selectedServiceCode");
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		List<TaskListServiceDTO> servicesStatusMap = null;
		if(selectedServiceCode!=null){
			servicesStatusMap = (List<TaskListServiceDTO>) session.getAttribute("svc_status_list");
			if(servicesStatusMap!=null){
				List<String> tempStatus = new ArrayList<String>();
				List<StatusDTO> tempStatusDTO = new ArrayList<StatusDTO>();
				for(TaskListServiceDTO svc : servicesStatusMap){
					if(selectedServiceCode.equalsIgnoreCase("zero"))
					{
						List<StatusDTO> statusList = null;
						statusList = svc.getStatusList();
						for(StatusDTO status : statusList){
							if(tempStatus.contains(status.getStatusCode())){
								continue;
							}
							buffer.append("<status>");
							buffer.append("<status-code>" + status.getStatusCode() + "</status-code>");
							buffer.append("<status-name>" + status.getStatusName() + "</status-name>");
							buffer.append("</status>");
							tempStatus.add(status.getStatusCode());
						}
					}
					else
					{						
						if(svc.getServiceCode()!=null && svc.getServiceCode().equalsIgnoreCase(selectedServiceCode)){
							List<StatusDTO> statusList = null;
							statusList = svc.getStatusList();
							for(StatusDTO status : statusList){
								if(tempStatus.contains(status.getStatusCode())){
									continue;
								}
								buffer.append("<status>");
								buffer.append("<status-code>" + status.getStatusCode() + "</status-code>");
								buffer.append("<status-name>" + status.getStatusName() + "</status-name>");
								buffer.append("</status>");
								tempStatus.add(status.getStatusCode());
								StatusDTO statusDTO = new StatusDTO();
								statusDTO.setStatusCode(status.getStatusCode());
								statusDTO.setStatusName(status.getStatusCode());
								tempStatusDTO.add(statusDTO);
							}
							request.getSession().setAttribute("status_list", tempStatusDTO);
							break;
						}
					}
				}
			}
		}
		
		buffer.append("</xml-response>");
		response.setContentType("text/xml");
		out.println(buffer);
		out.flush();
		out.close();
		
	}
	
	
	private static final String GET_ALL_WORKLIST_COMMON_1 = "select w.Application_Number, w.Actor_Name, DATE_FORMAT(w.Action_Date,'%d-%m-%Y') Action_Date," +
			" st.Status_Name, st.Status_Type_Short_Desc, " +
	"s.Service_Name, s.Service_Short_Desc, t.Assigned_User_Id, stt.Status_Type_Short_Desc taskStatus," 
	+ "IF(Service_Short_Desc IN('ILL','RLL','DLL'), "
	+ "    (SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=w.Application_Number), "
	+ "     IF(Service_Short_Desc IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap "
	+ "      WHERE ap.Application_Number=w.Application_Number), "
	+ "     IF(Service_Short_Desc IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number FROM t_vehicle_application ap "
	+ "       WHERE ap.Application_Number=w.Application_Number),IF(Service_Short_Desc IN('ILL','RLL','DLL'),"
	+" (SELECT ap.`Learner_License_No` FROM t_learner_license_application ap WHERE ap.Application_Number=w.Application_Number),'') "
	+ "     ))) serviceTypeNo, "
	+ "    IF(Service_Short_Desc IN('ILL','RLL','DLL'), "
	+ "    (SELECT p.CID_Number FROM t_learner_license_application ap "
	+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
	+ "     IF(Service_Short_Desc IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap "
	+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
	+ "     IF(Service_Short_Desc IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap "
	+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number),'' "
	+ "     ) ) ) customer_Id, "
	+ "    "
	+ "    IF(Service_Short_Desc IN('ILL','RLL','DLL'), "
	+ "    (SELECT CONCAT(p.`First_Name`,' ',p.`Middle_Name`,' ',p.`Last_Name`) FROM t_learner_license_application ap "
	+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
	+ "     IF(Service_Short_Desc IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT CONCAT(p.`First_Name`,' ',p.`Middle_Name`,' ',p.`Last_Name`) FROM t_driving_license_application ap "
	+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
	+ "     IF(Service_Short_Desc IN('REGV','RV','TV','DV','CV'),(SELECT IF(ap.Vehicle_Registration_Type='P',(SELECT CONCAT(p.`First_Name`,' ',p.`Middle_Name`,' ',p.`Last_Name`) "
	+ "    FROM `t_personal_dtls` p WHERE p.Customer_Id=ap.Customer_Id), "
	+ "    (SELECT p.Organization_Name "
	+ "    FROM `t_organization_info` p WHERE p.Customer_Id=ap.Customer_Id)) FROM `t_vehicle_application` ap WHERE ap.Application_Number=w.Application_Number),'' "
	+ "     ))) customer_Name "
	+" from t_workflow_dtls w, t_status_master st,"
	+" t_service_master s, t_task_dtls t, t_status_master stt where w.Instance_Id in " +
	"("+
	"select t1.Instance_Id from t_task_dtls t1 where t1.Assigned_Priv_Id = " +
	"(select Priv_Id from t_privilege_master where Priv_Short_Desc='";
	
	private static final String GET_ALL_WORKLIST_COMMON_2 = "' and Service_Id=(select Service_Id from t_service_master where Service_Short_Desc='";
	
	private static final String GET_ALL_WORKLIST_COMMON_3 = "')) and t1.Application_Number in (select Application_Number from t_workflow_dtls" +
	" where Service_Id = (select Service_Id from t_service_master where Service_Short_Desc = '";
	
	private static final String GET_ALL_WORKLIST_COMMON_4 = "')) and t1.Task_State_Id = (select Status_Id from t_status_master where" +
	" Status_Type_Short_Desc = 'INITIATED' and Status_Type='T')) and w.Status_Id = st.Status_Id and w.Service_Id = s.Service_Id" +
	" and w.Instance_Id = t.Instance_Id and t.Task_State_Id=stt.Status_Id and w.Juris_Id = ";
	
	private static final String GET_ALL_WORKLIST_COMMON_5 = " and w.Juris_Type_Id = ";
	
	
	
	
	
	private static final String GET_ALL_WORKLIST_OWN_1 = "select w.Application_Number, w.Actor_Name, DATE_FORMAT(w.Action_Date,'%d-%m-%Y') Action_Date," +
			" st.Status_Name, st.Status_Type_Short_Desc, " 
			+ "IF(Service_Short_Desc IN('ILL','RLL','DLL'), "
			+ "    (SELECT ap.Learner_License_No  FROM t_learner_license_application ap WHERE ap.Application_Number=w.Application_Number), "
			+ "     IF(Service_Short_Desc IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT ap.Driving_License_No FROM t_driving_license_application ap "
			+ "      WHERE ap.Application_Number=w.Application_Number), "
			+ "     IF(Service_Short_Desc IN('REGV','RV','TV','DV','CV'),(SELECT ap.Vehicle_Number FROM t_vehicle_application ap "
			+ "       WHERE ap.Application_Number=w.Application_Number),'' "
			+ "     ))) serviceTypeNo, "
			+ "    IF(Service_Short_Desc IN('ILL','RLL','DLL'), "
			+ "    (SELECT p.CID_Number FROM t_learner_license_application ap "
			+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
			+ "     IF(Service_Short_Desc IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT p.CID_Number FROM t_driving_license_application ap "
			+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
			+ "     IF(Service_Short_Desc IN('REGV','RV','TV','DV','CV'),(SELECT p.CID_Number FROM t_vehicle_application ap "
			+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number),'' "
			+ "     ) ) ) customer_Id, "
			+ "    "
			+ "    IF(Service_Short_Desc IN('ILL','RLL','DLL'), "
			+ "    (SELECT CONCAT(p.`First_Name`,' ',p.`Middle_Name`,' ',p.`Last_Name`) FROM t_learner_license_application ap "
			+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
			+ "     IF(Service_Short_Desc IN('INDL','ICDL','RDL','DDL','EDL'),(SELECT CONCAT(p.`First_Name`,' ',p.`Middle_Name`,' ',p.`Last_Name`) FROM t_driving_license_application ap "
			+ "     LEFT JOIN t_personal_dtls p ON ap.Customer_Id = p.Customer_Id WHERE ap.Application_Number=w.Application_Number), "
			+ "     IF(Service_Short_Desc IN('REGV','RV','TV','DV','CV'),(SELECT IF(ap.Vehicle_Registration_Type='P',(SELECT CONCAT(p.`First_Name`,' ',p.`Middle_Name`,' ',p.`Last_Name`) "
			+ "    FROM `t_personal_dtls` p WHERE p.Customer_Id=ap.Customer_Id), "
			+ "    (SELECT p.Organization_Name "
			+ "    FROM `t_organization_info` p WHERE p.Customer_Id=ap.Customer_Id)) FROM `t_vehicle_application` ap WHERE ap.Application_Number=w.Application_Number),'' "
			+ "     ))) customer_Name,"
	+"s.Service_Name, s.Service_Short_Desc, t.Assigned_User_Id, stt.Status_Type_Short_Desc taskStatus from t_workflow_dtls w, t_status_master st,"+
	" t_service_master s, t_task_dtls t, t_status_master stt where w.Instance_Id in " +
	"("+
	"select t1.Instance_Id from t_task_dtls t1 where t1.Assigned_Priv_Id = (select Priv_Id from t_privilege_master where" +
	" Priv_Short_Desc='";
	
	private static final String GET_ALL_WORKLIST_OWN_2= "' and Service_Id=(select Service_Id from t_service_master where Service_Short_Desc='";
	
	private static final String GET_ALL_WORKLIST_OWN_3 = "')) and t1.Application_Number in (select Application_Number from t_workflow_dtls" +
	" where Service_Id = (select Service_Id from t_service_master where Service_Short_Desc = '";
	
	private static final String GET_ALL_WORKLIST_OWN_4 = "')) and t1.Task_State_Id = (select Status_Id from t_status_master where" +
	" Status_Type_Short_Desc = 'CLAIMED' and Status_Type='T') and t1.Assigned_User_Id = '";
	
	private static final String GET_ALL_WORKLIST_OWN_5 = "') and w.Status_Id = st.Status_Id and w.Service_Id = s.Service_Id" +
	" and w.Instance_Id = t.Instance_Id and t.Task_State_Id=stt.Status_Id";
	
	public static final String CHECK_TASK_STATUS_DESC  = ""
	    + "SELECT * "
	    + "FROM   t_task_dtls a, "
	    + "       t_status_master b "
	    + "WHERE  a.application_number =  ? "
	    + "       AND a.task_state_id = b.status_id ";
	
	public static final String CHECK_WORKFLOW_STATUS_DESC  = ""
		    + "SELECT * "
		    + "FROM   t_status_master b, "
		    + "       t_workflow_dtls c "
		    + "WHERE  c.application_number = ? "
		    + "       AND b.status_id = c.status_id ";
		
	public static String INSERT_TASK_AUDIT_FOR_CLAIMING = "INSERT INTO t_task_dtls_audit "
															+ "            (task_id, "
															+ "             instance_id, "
															+ "             seq_details_id, "
															+ "             application_number, "
															+ "             assigned_user_id, "
															+ "             assigned_priv_id, "
															+ "             task_state_id, "
															+ "             action_date, "
															+ "             task_remark) "
															+ "(SELECT task_id, "
															+ "        instance_id, "
															+ "        seq_details_id, "
															+ "        application_number, "
															+ "        assigned_user_id, "
															+ "        assigned_priv_id, "
															+ "        task_state_id, "
															+ "        action_date, "
															+ "        task_remark "
															+ " FROM   t_task_dtls "
															+ " WHERE  application_number = ?) ";

	public static String UPDATE_TASK_FOR_CLAIMING = "UPDATE t_task_dtls SET Task_State_Id=(SELECT Status_Id FROM t_status_master WHERE" +
		" Status_Type_Short_Desc=? AND Status_Type='T'), Assigned_User_Id = ?, Action_Date=CURRENT_TIMESTAMP WHERE" +
		" Application_Number=?";
}
