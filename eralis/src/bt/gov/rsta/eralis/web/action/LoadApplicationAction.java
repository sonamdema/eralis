package bt.gov.rsta.eralis.web.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import bt.gov.rsta.eralis.web.action.LoadApplicationBusiness;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.DriveTypeDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.DocumentVO;

public class LoadApplicationAction extends DispatchAction {
	
	ResultSet rs = null;
	PreparedStatement pst = null;
	Connection conn = null;
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date(System.currentTimeMillis());
	
	
	public ActionForward load_ill_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		String actionForward = "newlicense";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try 
		{
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			String driveTypeApplicationTable	=	"t_learner_licn_drive_type_application";
			String whereClause	=	" a.`Application_Number` = ?";
			
			List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().get_customer_drive_type(applicationNo,driveTypeApplicationTable,whereClause);
			request.setAttribute("DRIVETYPE_LIST", drivetypeList);
			request.setAttribute("DRIVE_TYPE_LIST_SIZE", drivetypeList.size());
			
			if(param.equalsIgnoreCase("DISPATCH"))
			{
				String learnerNo = LoadApplicationBusiness.getInstance().getGeneratedNumber(applicationNo, "LEARNER");
				dto.setLearnerlicenseno(learnerNo);
			}

			request.setAttribute("param", param);
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicenseapplicationList(dto);
			
			request.setAttribute("LicenseDTO", dto);
	
			String serviceName = "LL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	//vehicle
	public ActionForward load_vehicle_registration_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		    String actionForward = "new";
		    HttpSession session = request.getSession();
		    EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		    userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		   VehicleDTO dto = new VehicleDTO();
		
		try 
		{
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleRegistrationList(dto);
			
			if(param.equalsIgnoreCase("DISPATCH"))
			{
				String vehicleNumber = LoadApplicationBusiness.getInstance().getGeneratedNumber(applicationNo, "VEHICLE");
				dto.setVehicleNumber(vehicleNumber);
			}
			
			request.setAttribute("Vehicledto", dto);
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			request.setAttribute("param", param);
		}
			catch (Exception e) {
				e.printStackTrace();
			Log.error("Error LoadApplicationAction[load_vehicle_registration_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_vehicle_renewal_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		String actionForward = "renewal";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto=new VehicleDTO();
		
		try 
		{
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleRenewalList(dto, param);
			request.setAttribute("Vehicledto", dto);
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			request.setAttribute("param", param);
		} 
		catch (Exception e) 
		{
			Log.error("Error LoadApplicationAction[load_vehicle_renewal_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_vehicle_conversion_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		String actionForward = "conversion";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto=new VehicleDTO();
		
		try 
		{
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleConversionList(dto);
			request.setAttribute("Vehicledto", dto); 
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			String param = (String) request.getAttribute("PARAM");

			if(param.equalsIgnoreCase("DISPATCH"))
			{
				String vehicleNumber = LoadApplicationBusiness.getInstance().getGeneratedNumber(applicationNo, "VEHICLE");
				request.setAttribute("NEW_VEHICLE_NUMBER", vehicleNumber);
			}
			request.setAttribute("param", param);
		
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_vehicle_conversion_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_vehicle_duplication_app_for_verification(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "verifyduplication";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto=new VehicleDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleDuplicationList(dto);
			
			request.setAttribute("Vehicledto", dto);
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			
		
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_vehicle_duplication_app_for_verification]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_rrwc_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "rwc_replacement";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto=new VehicleDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleDuplicationList(dto);
			
			request.setAttribute("Vehicledto", dto);
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			
		
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_vehicle_duplication_app_for_verification]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_vehicle_duplication_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "duplication";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto=new VehicleDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleDuplicationList(dto);
			request.setAttribute("Vehicledto", dto);
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			String param = (String) request.getAttribute("PARAM");
			request.setAttribute("param", param);
		} 
		catch (Exception e) 
		{
			Log.error("Error LoadApplicationAction[load_vehicle_duplication_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_vehicle_transfer_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "transfer";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto=new VehicleDTO();
		
		try {
			
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().getvehicleTransferList(dto);
			request.setAttribute("Vehicledto", dto);
			
			String serviceName = "RC";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			request.setAttribute("param", param);
		
		    } 
		    catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_vehicle_transfer_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	//learner license renewal
	public ActionForward load_rll_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "renew";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicenserenewalList(dto);
			request.setAttribute("LicenseDTO", dto);
			String driveTypeApplicationTable	=	" t_learner_licn_drive_type ";
			String whereClause	=	" a.Learner_License_Id=?";
			String conditionValue=dto.getLearnerLicenseId();
			List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().get_customer_drive_type(conditionValue,driveTypeApplicationTable,whereClause);
			request.setAttribute("DRIVETYPE_LIST", drivetypeList);
			request.setAttribute("DRIVE_TYPE_LIST_SIZE", drivetypeList.size());
			
			String serviceName = "RLL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			request.setAttribute("param", param);
			
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
//Learner License Duplicate	
	public ActionForward load_dll_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "learnerlicenseduplicateapprove";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicenseduplicateList(dto);
			request.setAttribute("LicenseDTO", dto);
			
			String serviceName = "LL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			request.setAttribute("param", param);
			
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_dll_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
//Driving License	
	public ActionForward load_indl_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "noncommercial";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getnoncommercialList(dto);
			request.setAttribute("LicenseDTO", dto);
			
			String serviceName = "DL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			String param = (String) request.getAttribute("PARAM");

			if(param.equalsIgnoreCase("DISPATCH"))
			{
				String licenseNo = LoadApplicationBusiness.getInstance().getGeneratedNumber(applicationNo, "LICENSE");
				dto.setLicenseNo(licenseNo);
			}

			request.setAttribute("param", param);
			 
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
//commercial driving license	
	public ActionForward load_icdl_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "commercial";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try 
			{
			
				String serviceCode = (String) request.getAttribute("serviceCode");
				String applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getcommercialList(dto);
				request.setAttribute("LicenseDTO", dto);
				
				if(param.equalsIgnoreCase("DISPATCH"))
				{
					String licenseNo = LoadApplicationBusiness.getInstance().getGeneratedNumber(applicationNo, "LICENSE");
					dto.setLicenseNo(licenseNo);
				}

				request.setAttribute("param", param);
			} 
		catch (Exception e) 
			{
				Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "
						+ e);
				String owner = "";
				if (null != userRolePriv.getUserId()) {
					owner = userRolePriv.getUserId() + "("
							+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
//Driving license renewal	
	public ActionForward load_rdl_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "drivinglicenseRenewal";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicenseRenewalList(dto);
			request.setAttribute("LicenseDTO", dto);
			 
			/*String driveTypeApplicationTable	=	"t_driving_license_dtls";
			String whereClause	=	" a.`Driving_License_Id` = ?";
			String licenseId	=	dto.getDrivinglicenseId();
			
			List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().get_customer_drive_type(licenseId,driveTypeApplicationTable,whereClause);
			request.setAttribute("DRIVETYPE_LIST", drivetypeList);*/

			String driveTypeApplicationTable	=	" t_driving_license_drive_type ";
			String whereClause	=	" a.`License_Id` = ?";
			String licenseId = dto.getDrivinglicenseId();
			List<DriveTypeDTO> endorseDrivetypeList = CommonBusiness.getInstance().get_customer_drive_type(licenseId,driveTypeApplicationTable,whereClause);
			request.setAttribute("ENDORSE_DRIVE_TYPE_LIST", endorseDrivetypeList);

			
			String serviceName = "DL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			request.setAttribute("param", param);
			
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	//Endorsement license	
	public ActionForward load_edl_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		String actionForward = "Endorsement";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		LicenseDTO dto=new LicenseDTO();
		
		try 
		{
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicenseEndorsementList(dto);
			request.setAttribute("LicenseDTO", dto);
			
			String driveTypeApplicationTable	=	"";
			String whereClause	=	"";
			String customerId = dto.getCustomerId();
			
			driveTypeApplicationTable	=	" t_driving_license_drive_type ";
			whereClause	=	" a.Customer_Id = ?";
			List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().get_customer_drive_type(customerId,driveTypeApplicationTable,whereClause);
			request.setAttribute("DRIVETYPE_LIST", drivetypeList);
			
			driveTypeApplicationTable	=	" t_driving_license_application ";
			whereClause	=	"  a.Application_Number = ?";
			List<DriveTypeDTO> appliedDriveType = CommonBusiness.getInstance().get_customer_drive_type1(applicationNo);
			request.setAttribute("APPLIED_DRIVE_TYPE", appliedDriveType);
			
			String serviceName = "DL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			request.setAttribute("param", param);
		} 
		catch (Exception e) 
		{
			Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	//driving license duplicate
	public ActionForward load_ddl_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		String actionForward = "drivinglicenseduplicate";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try 
		{
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getdrivinglicensenduplicate(dto);
			request.setAttribute("LicenseDTO", dto);

			String driveTypeApplicationTable	=	" t_driving_license_drive_type ";
			String whereClause	=	" a.`License_Id` = ?";
			String licenseId = dto.getDrivinglicenseId();
			List<DriveTypeDTO> endorseDrivetypeList = CommonBusiness.getInstance().get_customer_drive_type(licenseId,driveTypeApplicationTable,whereClause);
			request.setAttribute("ENDORSE_DRIVE_TYPE_LIST", endorseDrivetypeList);
			 
			String serviceName = "DL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			request.setAttribute("param", param);
			
		} 
		catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_ddl_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_dll_app_for_verification(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String actionForward = "learnerLicenseDuplicate";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		LicenseDTO dto=new LicenseDTO();
		try {
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicenserenewalList(dto);
			request.setAttribute("LicenseDTO", dto);
			
			String serviceName = "LL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_dll_app_for_verification]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_ddl_app_for_verification(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response)
	{
		String actionForward = "drivinglicenseduplicateverify";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		try 
		{
			String applicationNo = (String) request.getAttribute("applicationNo");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getlicensenduplication(dto);
			request.setAttribute("LicenseDTO", dto);
			
			String driveTypeApplicationTable	=	" t_driving_license_drive_type ";
			String  whereClause	=	" a.`License_Id` = ?";
			String licenseId	=	dto.getDrivinglicenseId();
			List<DriveTypeDTO> endorseDrivetypeList = CommonBusiness.getInstance().get_customer_drive_type(licenseId,driveTypeApplicationTable,whereClause);
			request.setAttribute("ENDORSE_DRIVE_TYPE_LIST", endorseDrivetypeList);
			
			String serviceName = "DL";
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
		} 
		catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_dll_app_for_verification]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_resubmitted_app(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String actionForward = "resubmitForm";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		LicenseDTO dto=new LicenseDTO();
		VehicleDTO veh_dto=new VehicleDTO();
		ServiceDTO service_dto=new ServiceDTO();
		String serviceName ="";
		EralisCommonDTO personalDTO = null;
		EralisCommonDTO organisationDTO = null;

		List<DropDownDTO> titleOfcourtesyList = null ;
		List<DropDownDTO> occupationList = null;
		List<DropDownDTO> nationalityList = null;
		List<DropDownDTO> countryList = null;
		List<DropDownDTO> dzongkhagList = null;
		List<DropDownDTO> bloodgroupList = null;
		List<DropDownDTO> regionList = null;
		List<DropDownDTO> ownerTypeList = null;
		List<DropDownDTO> departmentList = null;
		List<DropDownDTO> privateCompanyList = null;
		List<DropDownDTO> ministryList = null;
		
		try {
			//serviceActionForm serviceForm = (serviceActionForm) form;
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			request.setAttribute("serviceCode", serviceCode);
			service_dto.setApplicationNo(applicationNo);
			veh_dto.setApplicationNumber(applicationNo);
			dto.setApplicationNo(applicationNo);
			String ownerType	=	null;
			String personalInfoId	=	null;
			String organisationInfoId	=	null;
			
			/*******GET LEARNER LICENSE APPLICATION DRIVE TYPE******/
			if(serviceCode.equalsIgnoreCase("ILL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "New Learner License");
				serviceName = "LL";
				List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().get_learner_drive_type_for_resubmit(applicationNo,serviceCode);
				request.setAttribute("DRIVETYPE_LIST", drivetypeList);
				request.setAttribute("DRIVE_TYPE_LIST_SIZE", drivetypeList.size());
				
				LoadApplicationBusiness.getInstance().getlicenseapplicationList(dto);
				personalInfoId	=	dto.getPersonalInfoId();
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("REGV"))
			{
				request.setAttribute("SERVICE_TITLE", "Vehicle Registration");
				serviceName = "RC";
				veh_dto = LoadApplicationBusiness.getInstance().get_vehicle_application_dtls(veh_dto);
				BeanUtils.copyProperties(service_dto, veh_dto);
				if("Personal".equalsIgnoreCase(veh_dto.getVehicleRegistrationType()))
				{
					ownerType	=	"PERSONAL";
					personalInfoId	=	veh_dto.getPersonalInfoId();
				}
				else
				{
					ownerType	=	"ORGANISATION";
					organisationInfoId	=	veh_dto.getOrganisationInfoId();
				}
				List<DropDownDTO> busTypeList = CommonBusiness.getInstance().getDropDownList("BUS_TYPE_LIST", null);
				request.setAttribute("busTypeList", busTypeList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);
				
				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
				request.setAttribute("engineTypeList", engineTypeList);
				
				List<DropDownDTO> dealersNameList = CommonBusiness.getInstance().getDropDownList("DEALERS_NAME_LIST", null);
				request.setAttribute("dealersNameList", dealersNameList);
				
				List<DropDownDTO> manufactureCountryList = CommonBusiness.getInstance().getDropDownList("MANUFACTURE_COUNTRY_LIST", null);
				request.setAttribute("manufactureCountryList", manufactureCountryList);
				
				ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				List<DropDownDTO> vehicleCancellationList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CANCELLATION_LIST", null);
				request.setAttribute("vehicleCancellationList", vehicleCancellationList);
			
				List<DropDownDTO> vehicleConversionList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CONVERSION_LIST", null);
				request.setAttribute("vehicleConversionList", vehicleConversionList); 
			
				dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> vehiclePrefixlist = CommonBusiness.getInstance().getDropDownList("VECHILE_PREFIX_LIST", null);
				request.setAttribute("vehiclePrefixlist", vehiclePrefixlist);
				
				List<DropDownDTO> hypothecatedToList = CommonBusiness.getInstance().getDropDownList("HYPOTHECATED_TO_LIST", null);
				request.setAttribute("hypothecatedToList", hypothecatedToList);
				
				List<DropDownDTO> registrationTypeList = CommonBusiness.getInstance().getDropDownList("GET_REGISTRATION_LIST", null);
				request.setAttribute("registrationTypeList", registrationTypeList);
			}
			else if(serviceCode.equalsIgnoreCase("RV"))
			{	
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "RC";
				request.setAttribute("SERVICE_TITLE", "Vehicle Renewal");
				veh_dto.setApplicationNumber(applicationNo);
				veh_dto = LoadApplicationBusiness.getInstance().getvehicleRenewalList(veh_dto, param);
				BeanUtils.copyProperties(service_dto, veh_dto);
				if("Personal".equalsIgnoreCase(veh_dto.getVehicleRegistrationType()))
				{
					ownerType	=	"PERSONAL";
					personalInfoId	=	veh_dto.getPersonalInfoId();
				}
				else
				{
					ownerType	=	"ORGANISATION";
					organisationInfoId	=	veh_dto.getOrganisationInfoId();
					
				}
				ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
				request.setAttribute("uploadedDocsList", uploadedDocsList);
				request.setAttribute("param", param);
			}
			else if(serviceCode.equalsIgnoreCase("TV"))
			{
				request.setAttribute("SERVICE_TITLE", "Vehicle Transfer");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "RC";
				
				veh_dto.setApplicationNumber(applicationNo);
				veh_dto = LoadApplicationBusiness.getInstance().getvehicleTransferList(veh_dto);
				if("Personal".equalsIgnoreCase(veh_dto.getVehicleRegistrationType()))
				{
					ownerType	=	"PERSONAL";
					personalInfoId	=	veh_dto.getPersonalInfoId();
				}
				else
				{
					ownerType	=	"ORGANISATION";
					organisationInfoId	=	veh_dto.getOrganisationInfoId();
					
				}
				ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);
				
				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
				request.setAttribute("engineTypeList", engineTypeList);
				
				List<DropDownDTO> dealersNameList = CommonBusiness.getInstance().getDropDownList("DEALERS_NAME_LIST", null);
				request.setAttribute("dealersNameList", dealersNameList);
				
				List<DropDownDTO> manufactureCountryList = CommonBusiness.getInstance().getDropDownList("MANUFACTURE_COUNTRY_LIST", null);
				request.setAttribute("manufactureCountryList", manufactureCountryList);
				
				List<DropDownDTO> registrationTypeList = CommonBusiness.getInstance().getDropDownList("GET_REGISTRATION_LIST", null);
				request.setAttribute("registrationTypeList", registrationTypeList);
				
				List<DropDownDTO> modelList = CommonBusiness.getInstance().getDropDownList("VECHILE_MODEL_LIST", dto.getVehicleCompany());
				request.setAttribute("modelList", modelList);
				BeanUtils.copyProperties(service_dto, veh_dto);
				request.setAttribute("Vehicledto", veh_dto);
				
				
			}
			else if(serviceCode.equalsIgnoreCase("CV"))
			{
				request.setAttribute("SERVICE_TITLE", "Vehicle Conversion");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "RC";
				
				serviceCode = (String) request.getAttribute("serviceCode");
				applicationNo = (String) request.getAttribute("applicationNo");
				
				veh_dto = LoadApplicationBusiness.getInstance().getvehicleConversionList(veh_dto);
				request.setAttribute("Vehicledto", veh_dto); 
				if("Personal".equalsIgnoreCase(veh_dto.getVehicleRegistrationType()))
				{
					ownerType	=	"PERSONAL";
					personalInfoId	=	veh_dto.getPersonalInfoId();
				}
				else
				{
					ownerType	=	"ORGANISATION";
					organisationInfoId	=	veh_dto.getOrganisationInfoId();
					
				}
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
				request.setAttribute("uploadedDocsList", uploadedDocsList);
				
				List<DropDownDTO> vehicleConversionList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CONVERSION_LIST", null);
				request.setAttribute("vehicleConversionList", vehicleConversionList); 
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);
				
				regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				BeanUtils.copyProperties(service_dto, veh_dto);
			}
			else if(serviceCode.equalsIgnoreCase("RLL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Learner License Renewal");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "LL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getlicenserenewalList(dto);
				personalInfoId	=	dto.getPersonalInfoId();
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("INDL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Non Commercial License Issuance");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "DL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getnoncommercialList(dto);
				personalInfoId	=	dto.getPersonalInfoId();
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("ORDINARY_DRIVE_TYPE", null);
				request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
				
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("ICDL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Commercial License Issuance");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "DL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getcommercialList(dto);
				request.setAttribute("LicenseDTO", dto);
				personalInfoId	=	dto.getPersonalInfoId();
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("COMMERCIAL_DRIVE_TYPE", null);
				request.setAttribute("COMMERCIAL_DRIVE_TYPE_LIST", driveTypeList);
				
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("RDL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Drving License Renewal");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "DL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getlicenseRenewalList(dto);
				request.setAttribute("LicenseDTO", dto);
				personalInfoId	=	dto.getPersonalInfoId();
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("COMMERCIAL_DRIVE_TYPE", null);
				request.setAttribute("COMMERCIAL_DRIVE_TYPE_LIST", driveTypeList);
				
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("DDL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Drving License Replacement");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "DL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getdrivinglicensenduplicate(dto);
				request.setAttribute("LicenseDTO", dto);
				personalInfoId	=	dto.getPersonalInfoId();
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("EDL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Drving License Endorsement");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "DL";
				
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
				request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getlicenseEndorsementList(dto);
				request.setAttribute("LicenseDTO", dto);
				personalInfoId	=	dto.getPersonalInfoId();
				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("DLL"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Learner License Replacement");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "LL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getlicenseduplicateList(dto);
				personalInfoId	=	dto.getPersonalInfoId();
				request.setAttribute("LicenseDTO", dto);

				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			else if(serviceCode.equalsIgnoreCase("TOP"))
			{
				ownerType	=	"PERSONAL";
				request.setAttribute("SERVICE_TITLE", "Learner License Replacement");
				applicationNo = (String) request.getAttribute("applicationNo");
				String param = (String) request.getAttribute("PARAM");
				serviceName = "LL";
				
				dto.setApplicationNo(applicationNo);
				LoadApplicationBusiness.getInstance().getTOPList(dto);
				personalInfoId	=	dto.getPersonalInfoId();
				request.setAttribute("LicenseDTO", dto);

				request.setAttribute("LicenseDTO", dto);
				BeanUtils.copyProperties(service_dto, dto);
			}
			
			LoadApplicationBusiness.getInstance().getETestResult(dto);
			
			String rejectionReason	=	LoadApplicationBusiness.getInstance().getRejectionReason(service_dto);
			
			
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,serviceName);
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			service_dto.setApplicationNumber(service_dto.getApplicationNo());
		
			if(!serviceCode.equalsIgnoreCase("TOP"))
			{
				LoadApplicationBusiness.getInstance().get_payment_dtls(service_dto);
			}
			if(ownerType=="PERSONAL")
			{
				personalDTO = CommonBusiness.getInstance().getPersonalInfoDtlsForEdit(personalInfoId);
				//BeanUtils.copyProperties(service_dto, personalDTO);

				titleOfcourtesyList = CommonBusiness.getInstance().getDropDownList("TITLE_OF_COURTESY_LIST", null);
				
				occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				
				nationalityList = CommonBusiness.getInstance().getDropDownList("NATIONALITY_LIST", null);
				
				countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				
				dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				
				bloodgroupList = CommonBusiness.getInstance().getDropDownList("BLOOD_GROUP_LIST", null);
				
				regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
			}
			else
			{
				organisationDTO = CommonBusiness.getInstance().getOrganisationInfoDtls(organisationInfoId);
				
				ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				departmentList = CommonBusiness.getInstance().getDropDownList("DEPARTMENT_LIST", null);
				request.setAttribute("departmentList", departmentList);
				
				privateCompanyList = CommonBusiness.getInstance().getDropDownList("PRIVATE_COMPANY_LIST", null);
				request.setAttribute("privateCompanyList", privateCompanyList);
			}
			request.setAttribute("ownerTypeList", ownerTypeList);
			request.setAttribute("ministryList", ministryList);
			request.setAttribute("ownerTypeList", ownerTypeList);
			request.setAttribute("departmentList", departmentList);
			request.setAttribute("privateCompanyList", privateCompanyList);
			
			request.setAttribute("titleOfcourtesyList", titleOfcourtesyList);
			request.setAttribute("personalOccupationList", occupationList);
			request.setAttribute("personalNationalityList", nationalityList);
			request.setAttribute("countryList", countryList);
			request.setAttribute("dzongkhagList", dzongkhagList);
			request.setAttribute("bloodgroupList", bloodgroupList);
			request.setAttribute("regionList", regionList);
			
			request.setAttribute("OWNER_TYPE", ownerType);
			request.setAttribute("PERSONALDTO", personalDTO);
			request.setAttribute("ORGANISATIONDTO", organisationDTO);
			service_dto.setReason(rejectionReason);
			request.setAttribute("ServiceDTO", service_dto);
			request.setAttribute("SERVICE_NAME", serviceName);
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error LoadApplicationAction[load_resubmitted_app]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_top_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		String actionForward = "newTOP";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try {
			
			String serviceCode = (String) request.getAttribute("serviceCode");
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getTOPList(dto);
			request.setAttribute("LicenseDTO", dto);
	
			if(param.equalsIgnoreCase("DISPATCH"))
			{
				String topNumber = LoadApplicationBusiness.getInstance().getGeneratedNumber(applicationNo, "TOP");
				dto.setTopNo(topNumber);
			}

			request.setAttribute("param", param);
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error LoadApplicationAction[load_top_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_top_replacement_app_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		String actionForward = "top_replacement";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		LicenseDTO dto=new LicenseDTO();
		
		try {
			
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			
			dto.setApplicationNo(applicationNo);
			LoadApplicationBusiness.getInstance().getTopReplacementDetails(dto);
			request.setAttribute("LicenseDTO", dto);
	
			request.setAttribute("param", param);
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error LoadApplicationAction[load_top_replacement_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}
	
	public ActionForward load_pbrp_for_approval(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		String actionForward = "pbrp_application_page";
	    HttpSession session = request.getSession();
	    EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
	    userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		VehicleDTO dto = new VehicleDTO();
		
		try 
		{
			String applicationNo = (String) request.getAttribute("applicationNo");
			String param = (String) request.getAttribute("PARAM");
			dto.setApplicationNumber(applicationNo);
			dto = LoadApplicationBusiness.getInstance().load_pbrp_for_approval(dto);
			
			request.setAttribute("Vehicledto", dto);
			
			ArrayList<DocumentVO> uploadedDocsList = CommonBusiness.getInstance().getUploadedFilesByApplicationNo(applicationNo,"PBRP");
			request.setAttribute("uploadedDocsList", uploadedDocsList);
			
			request.setAttribute("param", param);
		} catch (Exception e) {
			Log.error("Error LoadApplicationAction[load_ill_app_for_approval]: "+ e);
			String owner = "";
			if (null != userRolePriv.getUserId()) {
				owner = userRolePriv.getUserId() + "("+ userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}

		return mapping.findForward(actionForward);
	}

	
	
	/*
	private static final String DIFFERENT_VEHICLE_TYPE = "SELECT "
		+ "a.Description "
		+ "FROM "
		+ "t_vehicle_type_master a "
		+ "WHERE a.Vehicle_Type_Id=?";	*/
	

}
