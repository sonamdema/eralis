package bt.gov.rsta.eralis.web.action.vehicle;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.eralis.web.action.LoadApplicationBusiness;
import bt.gov.rsta.eralis.web.actionform.vehicle.VehicleActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.UserDetailsVO;

public class VehicleAction extends DispatchAction
{
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null, jurisId = null, jurisTypeId = null, regionId = null, baseId = null;

	public ActionForward new_registration(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().new_registration(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_registration]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward new_renewal(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String renewalType = request.getParameter("renewalType");
			
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			BeanUtils.copyProperties(dto, vehicleForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			dto.setRenewalType(renewalType);
			String result = VehicleBusiness.getInstance().new_renewal(dto,userVo);
			String[] resultArray = result.split("#");
			dto.setApplicationNumber(resultArray[1]);

			dto = LoadApplicationBusiness.getInstance().getvehicleRenewalList(dto, "APPROVE");
			String approvalStatus = VehicleBusiness.getInstance().renewal_application_approval(dto, userVo);

			//dto = LoadApplicationBusiness.getInstance().getlicenseRenewalList(dto);

			//String approvalStatus = LicenseBusiness.getInstance().driving_license_renewal_application_approval(dto, userVo);
			

			
			
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward saveInspectionDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			BeanUtils.copyProperties(dto, vehicleForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().saveInspectionDtls(dto,userVo);
            request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward new_transfer(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().new_transfer(dto,userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_transfer]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward new_duplication(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().new_duplication(dto,userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_duplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward fitnessReplacement(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().fitnessReplacement(dto,userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_duplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward new_cancellation(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception
    {
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().new_cancellation(dto,userVo);
			
			request.setAttribute("MESSAGE", result);
      		actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_cancellation]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward withdraw_veh_cancellation(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception
    {
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().withdraw_veh_cancellation(dto,userVo);
			
			request.setAttribute("MESSAGE", result);
      		actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_cancellation]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
  public ActionForward new_conversion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().new_conversion(dto,userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_conversion]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
  public ActionForward new_fitness(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null, result = "FAILURE";
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String checkedVals = request.getParameter("checkedVals");
			
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			result = VehicleBusiness.getInstance().new_fitness(dto, checkedVals, userVo);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_fitness]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

  public ActionForward new_emission(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().new_emission(dto,userVo);
			String[] resultArray = result.split("#");
			 
			request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("EMISSION_ID", resultArray[1]);
			
			actionForward = "emission_message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_emission]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
 
  
  public ActionForward print_emission_report(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
	
		try
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();

			String emissionId	=	request.getParameter("emissionId");
			
			BeanUtils.copyProperties(dto, vehicleForm);
			dto.setVehicleId(emissionId);
			
			dto = VehicleBusiness.getInstance().print_emission_report(dto);
			request.setAttribute("DTO", dto);
			request.setAttribute("EMISSION_ID", emissionId);
			actionForward = "emission_print";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_emission]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
  
	
  public ActionForward new_post(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			
			String result = VehicleBusiness.getInstance().new_post(dto);
			
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_post]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward new_print(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			
			String result = VehicleBusiness.getInstance().new_print(dto);
			
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[new_print]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	
	//approved vehicle
	public ActionForward registration_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().registration_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[registration_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward renewal_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			String vehicleNumber =request.getParameter("vehicleNumber");
			dto.setVehicleNumber(vehicleNumber);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().renewal_application_approval(dto, userVo);
           
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[renewal_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	
	
	public ActionForward conversion_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().conversion_application_approval(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[conversion_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	

	public ActionForward duplication_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().duplication_application_approval(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[duplication_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	

	public ActionForward rwc_duplication_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().rwc_duplication_application_approval(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[duplication_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	
	
	public ActionForward duplication_application_verify(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().duplication_application_verify(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[duplication_application_verify]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	

	public ActionForward passenger_bus_route_permit_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().passenger_bus_route_permit_approval(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[transfer_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	
	
	public ActionForward transfer_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().transfer_application_approval(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[transfer_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}	
	
	public ActionForward registration_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().registration_application_reject(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[registration_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward renewal_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().renewal_application_reject(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[renewal_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward transfer_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().transfer_application_reject(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[transfer_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward duplication_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().duplication_application_reject(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[duplication_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward conversion_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto=new VehicleDTO();
			
			BeanUtils.copyProperties(dto, vehicleForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNumber(applicationNo);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().conversion_application_reject(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[conversion_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward checkIfFitnessNEmissionValid(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String vehicleId = request.getParameter("vehicleId");
			
			String result = VehicleBusiness.getInstance().checkIfFitnessNEmissionValid(vehicleId);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[conversion_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward get_vehicle_details(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			String vehicleNo = request.getParameter("vehicleNo");
			String vehicleType = request.getParameter("vehicleType");
			String regionId = request.getParameter("regionId");
			String regionName = request.getParameter("regionName");
			String pageIdentifier = request.getParameter("pageIdentifier");
			String pageId = request.getParameter("pageId");
			
			VehicleDTO dto = VehicleBusiness.getInstance().get_vehicle_details(vehicleNo, vehicleType);
			request.setAttribute("VEHICLE_DETAILS", dto);
			
			request.setAttribute("page_identifier", pageIdentifier);
			request.setAttribute("page_id", pageId);
			request.setAttribute("REGION_ID", regionId);
			request.setAttribute("REGION_NAME", regionName);
			request.setAttribute("VEHICLE_NUMBER", vehicleNo);

			List<DropDownDTO> busTypeList = CommonBusiness.getInstance().getDropDownList("BUS_TYPE_LIST", null);
			request.setAttribute("busTypeList", busTypeList);
			
			List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
			request.setAttribute("vehicleTypeList", vehicleTypeList);

			List<DropDownDTO> colourList = CommonBusiness.getInstance().getDropDownList("COLOUR_LIST", null);
			request.setAttribute("colourList", colourList);
			
			List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
			request.setAttribute("regionList", regionList);
			
			List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
			request.setAttribute("vehicleCodeList", vehicleCodeList);
			
			List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
			request.setAttribute("vehicleCompanyList", vehicleCompanyList);
			
			List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
			request.setAttribute("engineTypeList", engineTypeList);
			
			List<DropDownDTO> dealersNameList = CommonBusiness.getInstance().getDropDownList("DEALERS_NAME_LIST", null);
			request.setAttribute("dealersNameList", dealersNameList);
			
			List<DropDownDTO> manufactureCountryList = CommonBusiness.getInstance().getDropDownList("MANUFACTURE_COUNTRY_LIST", null);
			request.setAttribute("manufactureCountryList", manufactureCountryList);
			
			List<DropDownDTO> registrationTypeList = CommonBusiness.getInstance().getDropDownList("GET_REGISTRATION_LIST", null);
			request.setAttribute("registrationTypeList", registrationTypeList);
			
			List<DropDownDTO> modelList = CommonBusiness.getInstance().getDropDownList("VECHILE_MODEL_LIST", dto.getVehicleCompany());
			request.setAttribute("modelList", modelList);
			
			if(pageId.equalsIgnoreCase("accident")){
				actionForward = "common_accident_entry";
			}
			else{
			actionForward = "update";
		   }
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[get_vehicle_details]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward update_vehicle_data(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			VehicleActionForm vehicleForm = (VehicleActionForm) form;
			VehicleDTO dto = new VehicleDTO();
			BeanUtils.copyProperties(dto, vehicleForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = VehicleBusiness.getInstance().update_vehicle_data(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error VehicleAction[update_vehicle_data]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	/**
	 *  Set values of user related information
	 */
	private void setUserValues(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge)session.getAttribute(Constants.USER_DETAILS);
		this.userId = userRolePriv.getUserId();
		this.userName = userRolePriv.getUserId();
		Role currentRole = userRolePriv.getCurrentRole();
		this.roleId = currentRole.getRoleId();
		this.roleName = currentRole.getRoleName();
		this.roleCode = currentRole.getRoleCode();
		this.jurisId = userRolePriv.getJurisdictionId();
		this.jurisTypeId = userRolePriv.getJurisdictionTypeId();
		this.regionId = userRolePriv.getRegionId();
		this.baseId = userRolePriv.getBaseId();
	}
	
	/*
	 * Copy user values to data transfer object
	 */
	private void copyUserValues(UserDetailsVO userVo)
	{
		userVo.setActorId(userId);
		userVo.setActorName(userName);
		userVo.setAssignedGroupId(assignedGroupId);
		userVo.setAssignedUserId(userId);
		userVo.setRoleId(roleId);
		userVo.setRoleName(roleName);
		userVo.setAssignedPrivId(assignedPrivId);
		userVo.setRoleCode(roleCode);
		userVo.setJurisdictionId(jurisId);
		userVo.setJurisdictionTypeId(jurisTypeId);
		userVo.setRegionId(regionId);
		userVo.setBaseId(baseId);
	}

}
