package bt.gov.rsta.eralis.web.action;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class LoadApplicationDAO {

	public static LoadApplicationDAO loadapplicationDAO;

	private static int duration = 0;
	private static int duration1 = 0;
	private static int validity = 0;
	private static int noncommercial = 0;
	private static int commercial = 0;

	ResultSet rs = null;
	PreparedStatement pst = null;
	Connection conn = null;

	SimpleDateFormat sourceSdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat targetSdf = new SimpleDateFormat("dd/MM/yyyy");

	Date date = new Date(System.currentTimeMillis());

	public static LoadApplicationDAO getInstance() {

		if (loadapplicationDAO == null)
			loadapplicationDAO = new LoadApplicationDAO();

		return loadapplicationDAO;

	}

	public String getGeneratedNumber(String applicationNo, String type)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String generatedNo = "", query = null;

		try {
			if (type.equalsIgnoreCase("VEHICLE"))
				query = GET_GENERATED_VEHICLE_NO;
			else if (type.equalsIgnoreCase("LICENSE"))
				query = GET_GENERATED_LICENSE_NO;
			else if (type.equalsIgnoreCase("LEARNER"))
				query = GET_GENERATED_LEARNER_NO;
			else if (type.equalsIgnoreCase("TOP"))
				query = GET_GENERATED_TOP_NO;

			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(query);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();

				generatedNo = rs.getString("generatedNumber");
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return generatedNo;
	}
	
	public ServiceDTO get_payment_dtls(ServiceDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			conn = ConnectionManager.getConnection();
			if (conn != null) 
			{ 
				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNumber()); 
				rs = pst.executeQuery();
				rs.first(); 
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid")); 
				dto.setReceiptNo(rs.getString("Receipt_No")); 
				dto.setReceiptDate(rs.getString("formatReceiptDate")); 
			}
		} 
		catch (Exception e) {
			throw new ERALISException("###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public LicenseDTO getlicenseapplicationList(LicenseDTO dto)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try

		{
			duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-learner-application"));
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_LICENSE_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();

				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Village_Name"));
				dto.setCertifyingDoctor(rs.getString("Certifying_Doctor"));
				dto.setRemarks(rs.getString("applicationRemarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setRegion(rs.getString("region_name"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setReceiptDate1(rs.getString("Receipt_Date"));
				dto.setReason(rs.getString("Task_Remark"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				dto.setAmount(rs.getString("amount_paid"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
				DateTime receiptDate1 = format.parseDateTime(rs.getString("Receipt_Date"));
				DateTime a = receiptDate1.plusMonths(duration);
				String p = a.toString();
				java.util.Date nextExpiry = sourceSdf.parse(p);
				String nextexpiry = targetSdf.format(nextExpiry);
				dto.setExpiryDate(nextexpiry);
			}
		} 
		catch (Exception e) {
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public LicenseDTO getlicenserenewalList(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				duration = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-learner-application"));
				int renewalDuration	=	0;
				pst = conn.prepareStatement(GET_LICENSE_RENEWAL_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();
				
				// dto = new LicenseDTO();
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setReason(rs.getString("Task_Remark"));
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Village_Name"));
				dto.setCertifyingDoctor(rs.getString("Certifying_Doctor"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setRegion(rs.getString("region_name"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setReceiptDate1(rs.getString("Receipt_Date"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setLicenseNo(rs.getString("Learner_License_No"));
				dto.setLearnerLicenseId(rs.getString("Learner_license_Id"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				dto.setIssuedate(rs.getString("Issue_Date"));
				
				renewalDuration	=	rs.getInt("Renewal_Duration");
				if(renewalDuration>1)
					dto.setRenewalDuration(Integer.toString(renewalDuration/12)+" Years");
				else 
					dto.setRenewalDuration(Integer.toString(renewalDuration/12)+" Year"); 
				pst = conn.prepareStatement(GET_EXPIRY_FOR_RENEWAL); 
				pst.setString(1, dto.getLearnerLicenseId());
				rs = pst.executeQuery();
				rs.first();

				java.util.Date expiryDate = sourceSdf.parse(rs.getString("expiryDate"));
				String expiryDate1 = targetSdf.format(expiryDate);
				dto.setExpiryDate(expiryDate1);

				DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
				DateTime lastExpiryDate = format.parseDateTime(rs.getString("expiryDate"));
				lastExpiryDate.plusMonths(renewalDuration);

				String nextexpiryDate = format.print(lastExpiryDate.plusMonths(renewalDuration));
				java.util.Date nextexpiry = sourceSdf.parse(nextexpiryDate);
				String nextDate1 = targetSdf.format(nextexpiry);
				dto.setNextExpiry(nextDate1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public LicenseDTO getlicenseduplicateList(LicenseDTO dto)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_LICENSE_DUPLICATION_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Village_Name"));
				dto.setCertifyingDoctor(rs.getString("Certifying_Doctor"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setRegion(rs.getString("region_name"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setReceiptDate1(rs.getString("Receipt_Date"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setLearnerLicenseId(rs.getString("Learner_License_Id"));
				dto.setLearnerlicenseno(rs.getString("Learner_License_No"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
			}
		} catch (Exception e) {
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseduplicateList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public LicenseDTO getlicensenduplication(LicenseDTO dto)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn
						.prepareStatement(GET_DRIVING_LICENSE_DUPLICATION_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());

				rs = pst.executeQuery();

				rs.first();

				// dto = new LicenseDTO();
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setRegion(rs.getString("region_name"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setReceiptDate1(rs.getString("Receipt_date"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setDrivinglicenseId(rs.getString("License_Id"));
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));

				pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_DUPLICATE);
				pst.setString(1, dto.getDrivinglicenseId());
				pst.setString(2, dto.getDrivinglicenseId());
				pst.setString(3, dto.getDrivinglicenseId());

				rs = pst.executeQuery();
				rs.first();

				java.util.Date expiryDate = sourceSdf.parse(rs
						.getString("expiryDate"));
				String expiryDate1 = targetSdf.format(expiryDate);
				dto.setExpiryDate(expiryDate1);
			}
		} catch (Exception e) {
			throw new ERALISException(
					"###Error at CommonDAO[getlicensenduplication]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public LicenseDTO getnoncommercialList(LicenseDTO dto) throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try
		{
			noncommercial = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-ordinary-license"));

			conn = ConnectionManager.getConnection();

			if (conn != null)
			{
				pst = conn.prepareStatement(GET_NONCOMMERCIAL_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();
				dto.setLearnerLicenseId(rs.getString("Learner_License_Info_Id"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setDriveTypeId(rs.getString("Drive_Type_Id"));
				dto.setReason(rs.getString("Task_Remark"));
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Village_Name"));
				dto.setRegion(rs.getString("region_name"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setReceiptDate1(rs.getString("Receipt_date"));
				dto.setLearnerlicenseno(rs.getString("driving_learner_license_no"));
				dto.setDrivetype(rs.getString("driveType"));
				dto.setRenewalDuration(rs.getString("Renewal_Duration"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setAmount(rs.getString("Amount_Paid"));
				
				/*now.add(Calendar.MONTH, +Integer.parseInt(rs.getString("Renewal_Duration")));
				String a = formatter.format(now.getTime());
				dto.setExpiryDate(a);*/
				
				DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
				DateTime receiptDate1 = format.parseDateTime(rs.getString("Receipt_Date"));
				DateTime a = receiptDate1.plusMonths(Integer.parseInt(rs.getString("Renewal_Duration")));
				String p = a.toString();
				java.util.Date nextExpiry = sourceSdf.parse(p);
				String nextexpiry = targetSdf.format(nextExpiry);
				dto.setExpiryDate(nextexpiry);
				dto.setIssueType(rs.getString("Issue_type"));
			}
		}
		catch (Exception e) {
			throw new ERALISException("###Error at CommonDAO[getlicenseapplicationList]:: " + e);
			
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public LicenseDTO getcommercialList(LicenseDTO dto) throws ERALISException,ERALISSystemException {
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try 
		{
			commercial = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-commercial-driving-license"));

			/*Calendar now = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");*/

			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_COMMERCIAL_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());

				rs = pst.executeQuery();
				rs.first();
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setRegion(rs.getString("region_name"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setCustomerId(rs.getString("Customer_ID"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setDriveTypeId(rs.getString("Drive_Type_Id"));  
				dto.setDrivinglicenseId(rs.getString("License_Id"));
				dto.setReason(rs.getString("Task_Remark"));
				dto.setDrivetype(rs.getString("Drive_Type_Name"));
				dto.setRenewalDuration(rs.getString("Renewal_Duration"));
				dto.setCID(rs.getString("CID_Number"));
				
				/*now.add(Calendar.MONTH, +Integer.parseInt(rs.getString("Renewal_Duration")));
				String a = formatter.format(now.getTime());
				dto.setExpiryDate(a);*/
				
				DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
				DateTime receiptDate1 = format.parseDateTime(rs.getString("Receipt_Date"));
				DateTime a = receiptDate1.plusMonths(Integer.parseInt(rs.getString("Renewal_Duration")));
				String p = a.toString();
				java.util.Date nextExpiry = sourceSdf.parse(p);
				String nextexpiry = targetSdf.format(nextExpiry);
				dto.setExpiryDate(nextexpiry);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	// vehicle 
	public VehicleDTO get_vehicle_application_dtls(VehicleDTO dto) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_VEHICLE_APPLICATION_DTLS);
				pst.setString(1, dto.getApplicationNumber());

				rs1 = pst.executeQuery();
				rs1.first();
				String customerId = rs1.getString("Customer_Id");
				dto.setRegistrationType(rs1.getString("Registration_Type"));
				dto.setAmount(rs1.getString("Amount"));
				dto.setVehicleRegistrationId(rs1.getString("Vehicle_Registration_Id"));
				dto.setVehicleNumber(rs1.getString("Vehicle_Number"));
				dto.setVehicleType(rs1.getString("Vehicle_Type_Id"));
				dto.setVanityNumber(rs1.getString("Vanity_Number"));
				dto.setVehicleCompany(rs1.getString("Vehicle_Company_Id"));
				dto.setVehicleModel(rs1.getString("Vehicle_Model_Id"));
				dto.setEngineType(rs1.getString("Engine_Type_Id"));
				dto.setEngineNumber(rs1.getString("Engine_Number"));
				dto.setChasisNumber(rs1.getString("Chasis_Number"));
				dto.setStatus(rs1.getString("Status"));
				dto.setColour(rs1.getString("Color"));
				dto.setPrice(rs1.getString("Price"));
				dto.setLoadCapacity(rs1.getString("Load_Capacity"));
				dto.setDealersName(rs1.getString("Dealer_Id"));
				dto.setSeatCapacity(rs1.getString("Seat_Capacity"));
				dto.setHypothecatedTo(rs1.getString("Hypothecated_To_Id"));
				dto.setUnladenWeight(rs1.getString("Unladen_Weight"));
				dto.setLetterNo(rs1.getString("Letter_Number"));
				dto.setLetterDate(rs1.getString("Letter_Date"));
				dto.setManufactureYear(rs1.getString("Manufacture_Year"));
				dto.setEngineCC(rs1.getString("Engine_CC"));
				dto.setManufactureCountry(rs1.getString("Manufactured_Country_Id"));
				dto.setPurchaseType(rs1.getString("Purchase_Type"));
				dto.setVehicleHorsePower(rs1.getString("Vehicle_Horse_Power"));
				dto.setVehicleKiloWatt(rs1.getString("Vehicle_Kilowatt"));
				dto.setRemarks(rs1.getString("Remarks"));
				dto.setReason(rs1.getString("Task_Remark"));
				dto.setPurchaseDate(rs1.getString("purchase_dates"));
				dto.setVehicleRegistrationType(rs1.getString("Vehicle_Registration_Type"));
				dto.setBusType(rs1.getString("Bus_Type_Id"));
				dto.setVehilceTypeDesc(rs1.getString("Description"));
				
				if ("P".equalsIgnoreCase(rs1.getString("Vehicle_Registration_Type")))
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setBloodGroup(rs.getString("blood_group_type"));
					dto.setVehicleRegistrationType("Personal");
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setVehicleRegistrationType("Personal");
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setName(rs.getString("NAME"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setDOB(rs.getString("dob"));
					if (null != rs.getString("Present_Contact_Address"))
						dto.setAddress(rs.getString("Present_Contact_Address"));
					else
						dto.setAddress("");
				} else {
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setOrganisationInfoId(rs.getString("Organization_Info_Id"));
					dto.setVehicleRegistrationType("Organization");
					dto.setName(rs.getString("Organization_Name"));
					dto.setAddress(rs.getString("Address"));
				}
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getvehicleRegistrationList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public VehicleDTO load_pbrp_for_approval(VehicleDTO dto) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_PASSENGER_BUS_ROUTE_PERMIT_APPLICATION_DTLS);
				pst.setString(1, dto.getApplicationNumber());
				rs = pst.executeQuery();
				rs.first();
				dto.setName(rs.getString("Applicant_Name"));
				dto.setDob(rs.getString("DOB"));
				dto.setGender(rs.getString("Gender"));
				dto.setDzongkhag(rs.getString("Permanent_Dzongkhag"));
				dto.setGewog(rs.getString("Permanenet_Gewog"));
				dto.setVillage(rs.getString("Permanent_Village"));
				dto.setPhone(rs.getString("Phone_No"));
				dto.setEmail(rs.getString("Email_Id"));
				dto.setAddress(rs.getString("Present_Address"));
				dto.setType(rs.getString("Route_Type"));
				dto.setBusType(rs.getString("Bus_Category"));
				dto.setRouteFrom(rs.getString("Route_From"));
				dto.setRouteTo(rs.getString("Route_To"));
				dto.setTiming(rs.getString("Timing"));
				dto.setTransport(rs.getString("Transport_Name"));
				dto.setFliePath(rs.getString("File_Path"));
				dto.setFileName(rs.getString("File_Name"));
				dto.setCitizenID(rs.getString("CID"));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getvehicleRegistrationList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	

	public VehicleDTO getvehicleRegistrationList(VehicleDTO dto) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_VEHICLE_REGISTRATION_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNumber());
				rs1 = pst.executeQuery();
				rs1.first();
				
				String customerId = rs1.getString("Customer_Id");
				if (rs1.getString("Vehicle_Registration_Type").equals("P")) 
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setVehicleRegistrationType("Personal");
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setName(rs.getString("NAME"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setCitizenID(rs.getString("CID_Number"));
					if (null != rs.getString("Present_Contact_Address"))
						dto.setAddress(rs.getString("Present_Contact_Address"));
					else
						dto.setAddress("");
				}
				else
				{
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setVehicleRegistrationType("Organization");
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Village_Name"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setAddress(rs.getString("Address"));
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setMinistry(rs.getString("Ministry_Name"));
					else
						dto.setMinistry("");
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setName(rs.getString("Department_Name"));
					else if(rs.getString("Description").equals("PRIVATE"))
						dto.setName(rs.getString("Private_Name"));
					else
						dto.setName(rs.getString("Organization_Name"));
					
					dto.setOwner(rs.getString("Owner_Name"));
					dto.setOwnerTypeDesc(rs.getString("Description"));
				}
				
				dto.setRegistrationType(rs1.getString("registrationTypeDesc"));
				dto.setNumberAxle(rs1.getString("Number_Of_Axle"));
				dto.setWheelNos(rs1.getString("Number_Of_Wheels"));
				dto.setOwnerMobileNumber(rs1.getString("Owner_Mobile_Number"));
				dto.setQuotaMobileNumber(rs1.getString("Quota_Holder_Mobile_Number"));

				dto.setBusType(rs1.getString("bus_type_name"));
				dto.setReason(rs1.getString("Task_Remark"));
				Calendar now = Calendar.getInstance();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
				String adate = formatter.format(now.getTime());
				dto.setRegistrationDate(adate);
				pst = conn.prepareStatement(GET_VALIDITY_DURATION);
				pst.setString(1, dto.getApplicationNumber());
				rs2 = pst.executeQuery();
				rs2.first();
				int validityDuration = rs2.getInt("Renewal_Duration");
				
				Calendar now1 = Calendar.getInstance();
			    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd");
			    now1.add(Calendar.MONTH, + validityDuration);
			    String bdate = formatter1.format(now1.getTime());
			    dto.setExpiryDate(bdate);

				//dto.setRegistrationType(rs1.getString("Registration_Type"));
				dto.setAmount(rs1.getString("Amount"));
				dto.setVehicleRegistrationId(rs1.getString("Vehicle_Reg_Code_Name"));
				dto.setRegion(rs1.getString("region_name"));
				dto.setVehicleNumber(rs1.getString("Vehicle_Number"));
				dto.setVehicleType(rs1.getString("Vehicle_Type_Name"));
				dto.setVanityNumber(rs1.getString("Vanity_Number"));
				dto.setVehicleCompany(rs1.getString("Vehicle_Company_Name"));
				dto.setVehicleModel(rs1.getString("Vehicle_Model_Name"));
				dto.setEngineType(rs1.getString("Engine_Name"));
				dto.setEngineNumber(rs1.getString("Engine_Number"));
				dto.setChasisNumber(rs1.getString("Chasis_Number"));
				dto.setStatus(rs1.getString("Status"));
				dto.setColour(rs1.getString("Color"));
				dto.setPrice(rs1.getString("Price"));
				dto.setLoadCapacity(rs1.getString("Load_Capacity"));
				dto.setDealersName(rs1.getString("Dealer_Name"));
				dto.setSeatCapacity(rs1.getString("Seat_Capacity"));
				dto.setHypothecatedTo(rs1.getString("Hypothecated_Name"));
				dto.setUnladenWeight(rs1.getString("Unladen_Weight"));
				dto.setLetterNo(rs1.getString("Letter_Number"));
				dto.setLetterDate(rs1.getString("letterdate"));
				dto.setManufactureYear(rs1.getString("Manufacture_Year"));
				dto.setEngineCC(rs1.getString("Engine_CC"));
				dto.setManufactureCountry(rs1.getString("Country_Name"));
				dto.setPurchaseType(rs1.getString("Purchase_Type"));
				dto.setVehicleHorsePower(rs1.getString("Vehicle_Horse_Power"));
				dto.setVehicleKiloWatt(rs1.getString("Vehicle_Kilowatt"));
				dto.setRemarks(rs1.getString("Remarks"));
				dto.setRenewalDuration(rs1.getString("Renewal_Duration"));

				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNumber());
				rs = pst.executeQuery();
				rs.first();

				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptDate"));

			}
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getvehicleRegistrationList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public VehicleDTO getvehicleRenewalList(VehicleDTO dto, String param) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs3 = null;

		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_VEHICLE_RENEWAL_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNumber());
				rs1 = pst.executeQuery();
				rs1.first();
				dto.setRenewalDuration(rs1.getString("Renewal_Duration"));
				dto.setVehicleType(rs1.getString("Vehicle_Type_Name"));
				String customerId = rs1.getString("Customer_Id");
				String vehicleId	=	rs1.getString("Vehicle_Id");
				getVehicleDtls(vehicleId,dto);
				
				

				dto.setLoadCapacity(rs1.getString("engineCCGvw"));
				dto.setVehicleRegistrationCode(rs1.getString("Vehicle_Reg_Code_Name"));
				dto.setSeatCapacity(rs1.getString("Seat_Capacity"));
				
				if (rs1.getString("Vehicle_Registration_Type").equals("P")) 
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setVehicleRegistrationType("Personal");
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setName(rs.getString("NAME"));
					dto.setPhone(rs.getString("Present_Phone_No"));
					dto.setEmail(rs.getString("Present_Phone_No"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setIsInternational(rs.getString("Is_International"));
					dto.setCountry(rs.getString("Country_Name"));
					dto.setAddress(rs.getString("Present_Contact_Address"));
					dto.setCitizenID(rs.getString("CID_Number"));
				} 
				else 
				{
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setOrganisationInfoId(rs.getString("Organization_Info_Id"));
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setVehicleRegistrationType("Organization");
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Village_Name"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setAddress(rs.getString("Address"));
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setMinistry(rs.getString("Ministry_Name"));
					else
						dto.setMinistry("");
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setName(rs.getString("Department_Name"));
					else if(rs.getString("Description").equals("PRIVATE"))
						dto.setName(rs.getString("Private_Name"));
					else
						dto.setName(rs.getString("Organization_Name"));
					
					dto.setOwner(rs.getString("Owner_Name"));
					dto.setOwnerTypeDesc(rs.getString("Description"));
				 }
					
			    dto.setVehicleNumber(rs1.getString("Vehicle_Number"));
				dto.setRemarks(rs1.getString("Remarks"));
				dto.setRegistrationDate(rs1.getString("rgdate"));
				dto.setLastExpiryDate(rs1.getString("exdate"));
				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNumber());
				rs = pst.executeQuery();
				rs.first();
				
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				dto.setReceiptNo(rs.getString("Receipt_No"));	
				dto.setReceiptDate(rs.getString("formatReceiptDate"));
				dto.setAppsubmissiondate(rs.getString("Created_On"));
				dto.setVehicleId(rs1.getString("Vehicle_Id"));
				dto.setReason(rs1.getString("Task_Remark"));
					
				pst = conn.prepareStatement(GET_VEHICLE_RENEWAL_DURATION);
			    pst.setString(1, dto.getApplicationNumber());
			    rs = pst.executeQuery();
			    rs.first();
			    int renewalDuration = 12;
			    
			    if(null != rs.getString("Renewal_Duration"))
			    	renewalDuration = Integer.parseInt(rs.getString("Renewal_Duration"));
			    	dto.setRenewalDuration(Integer.toString(renewalDuration));
				
				if(param.equalsIgnoreCase("APPROVE"))
				{
					pst = conn.prepareStatement(LAST_EXPIRY_DATE);
				    pst.setString(1,dto.getVehicleId());
				    pst.setString(2,dto.getVehicleId());
				    pst.setString(3,dto.getVehicleId());
				    rs3 = pst.executeQuery();
				    rs3.first();  
				    java.util.Date renewalDate = sourceSdf.parse(rs3.getString("expiryDate"));
				    dto.setRenewalDate(targetSdf.format(renewalDate));
				    
				    DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
					DateTime lastExpiryDate = format.parseDateTime(rs3.getString("expiryDate"));
					lastExpiryDate.plusMonths(renewalDuration);
					String expiryDate = format.print(lastExpiryDate.plusMonths(renewalDuration));
					java.util.Date expiry = sourceSdf.parse(expiryDate);
					String renewalExpiryDate = targetSdf.format(expiry);
					dto.setExpiryDate(renewalExpiryDate);
				}
				else if(param.equalsIgnoreCase("DISPATCH"))
				{
					pst = conn.prepareStatement(GET_RENEWAL_AND_EXPIRY_DATE);
				    pst.setString(1,dto.getApplicationNumber());
				    rs = pst.executeQuery();
				    rs.first();
				    dto.setRenewalDate(rs.getString("renewalDate"));
					dto.setExpiryDate(rs.getString("expiryDate"));
				}
			}
		  } 
	    catch (Exception e) 
		{
	    	e.printStackTrace();
			throw new ERALISException("###Error at loadApplicationDAO[getvehicleRenewalList]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public VehicleDTO getvehicleConversionList(VehicleDTO dto)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_VEHICLE_CONVERSION_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNumber());
				rs1 = pst.executeQuery();
				rs1.first();
				dto.setVehicleTypeId(rs1.getString("Vehicle_Type_Id"));
				String customerId = rs1.getString("Customer_Id");

				if (rs1.getString("Vehicle_Registration_Type").equals("P")) 
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setVehicleRegistrationType("Personal");
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setName(rs.getString("NAME"));
					dto.setPhone(rs.getString("Present_Phone_No"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setEmail(rs.getString("Present_Email"));
					dto.setIsInternational(rs.getString("Is_International"));
					dto.setCountry(rs.getString("Country_Name"));
					dto.setAddress(rs.getString("Present_Contact_Address"));
				} 
				else 
				{
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setOrganisationInfoId(rs.getString("Organization_Info_Id"));
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setVehicleRegistrationType("Organization");
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Village_Name"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setAddress(rs.getString("Address"));
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setMinistry(rs.getString("Ministry_Name"));
					else
						dto.setMinistry("");
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setName(rs.getString("Department_Name"));
					else if(rs.getString("Description").equals("PRIVATE"))
						dto.setName(rs.getString("Private_Name"));
					else
						dto.setName(rs.getString("Organization_Name"));
					
					dto.setOwner(rs.getString("Owner_Name"));
					dto.setOwnerTypeDesc(rs.getString("Description"));
				}
				
				dto.setConversionReasonId(rs1.getString("Conversion_Reason_Id"));
				dto.setVehicleRegistrationId(rs1.getString("Vehicle_Registration_Id"));
				dto.setRegion(rs1.getString("region_name"));
				dto.setVehicleType(rs1.getString("Vehicle_Type_Name"));
				dto.setConversionReason(rs1.getString("Reason"));
				dto.setVehicleCode(rs1.getString("Vehicle_Reg_Code_Name"));
				dto.setVehicleNumber(rs1.getString("Vehicle_Number"));
				dto.setReason(rs1.getString("Task_Remark"));
				
				pst = conn.prepareStatement(GET_VEHICLE_DETAILS);
				pst.setString(1, rs1.getString("Vehicle_Id"));
				rs = pst.executeQuery();
				rs.first();
				dto.setVehicleCompany(rs.getString("Vehicle_Company_Name"));
				dto.setVehicleModel(rs.getString("Vehicle_Model_Name"));
				dto.setColour(rs.getString("Colour"));
				dto.setChasisNumber(rs.getString("Chassis_Number"));
				dto.setEngineNumber(rs.getString("Engine_Number"));
				dto.setEngineType(rs.getString("Engine_Name"));
				dto.setSeatCapacity(rs.getString("Seat_Capacity"));
				dto.setLoadCapacity(rs.getString("Load_Capacity"));
				
				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNumber());
				rs = pst.executeQuery();
				rs.first();
				dto.setAmount(rs.getString("Amount_Paid"));
				
				if (null != rs.getString("Penalty_Paid"))
					dto.setPenalty(rs.getString("Penalty_Paid"));
				else
					dto.setPenalty("0.0");
				
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptDate"));
				dto.setVehicleId(rs1.getString("Vehicle_Id"));
				dto.setConversionDate(rs.getString("receiptDate"));
			}
		} catch (Exception e) {
			throw new ERALISException("###Error at loadApplicationDAO[getvehicleConversionList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public VehicleDTO getvehicleDuplicationList(VehicleDTO dto)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_VEHICLE_DUPLICATION_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNumber());
				rs1 = pst.executeQuery();
				rs1.first();
				String customerId = rs1.getString("Customer_Id");
				
				getVehicleDtls(rs1.getString("Vehicle_Id"), dto);
				
				if (rs1.getString("Vehicle_Registration_Type").equals("P")) 
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();

					dto.setVehicleRegistrationType("Personal");
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setName(rs.getString("NAME"));
					dto.setCitizenID(rs.getString("CID_Number"));
					dto.setPhone(rs.getString("Present_Phone_No"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setCountry(rs.getString("Country_Name"));
					dto.setAddress(rs.getString("Present_Contact_Address"));
					dto.setIsInternational(rs.getString("Is_International"));
				} 
				else
				{
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();

					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setVehicleRegistrationType("Organization");
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Village_Name"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setAddress(rs.getString("Address"));
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setMinistry(rs.getString("Ministry_Name"));
					else
						dto.setMinistry("");
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setName(rs.getString("Department_Name"));
					else if(rs.getString("Description").equals("PRIVATE"))
						dto.setName(rs.getString("Private_Name"));
					else
						dto.setName(rs.getString("Organization_Name"));
					
					dto.setOwner(rs.getString("Owner_Name"));
					dto.setOwnerTypeDesc(rs.getString("Description"));
				}

				dto.setRemarks(rs1.getString("Remarks"));
				dto.setRegion(rs1.getString("region_name"));

				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNumber());
				rs = pst.executeQuery();
				rs.first();

				dto.setAmount(rs.getString("Amount_Paid"));
				if (null != rs.getString("Penalty_Paid"))
					dto.setPenalty(rs.getString("Penalty_Paid"));
				else
					dto.setPenalty("0.0");
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptDate"));
				dto.setVehicleId(rs1.getString("Vehicle_Id"));
				dto.setVehicleNumber(rs1.getString("Vehicle_Number"));
				dto.setTrafficRemarks(rs1.getString("Traffic_Remarks"));
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ERALISException("###Error at loadApplicationDAO[getvehicleDuplicationList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public VehicleDTO getvehicleTransferList(VehicleDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ResultSet rs1 = null;

		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_TRANSFER_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNumber());
				rs1 = pst.executeQuery();
				rs1.first();

				String customerId = rs1.getString("Transferor_Customer_Id");
				String customerType = customerId.substring(0, 1);
				String customerId1 = rs1.getString("Transferee_Customer_Id");
				String customerType1 = customerId1.substring(0, 1);
				String vehicleId = rs1.getString("Vehicle_Id");
				
				getVehicleDtls(vehicleId,dto);

				if (customerType.equals("P")) 
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
					dto.setTransferorCustomerId(rs.getString("Customer_Id"));
					dto.setVehicleRegistrationType("Personal");
					dto.setName(rs.getString("NAME"));
					dto.setPhone(rs.getString("Present_Phone_No"));
					dto.setCitizenID(rs.getString("CID_Number"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Permanant_Village_Id"));
					dto.setCountry(rs.getString("Country_Name"));
					dto.setAddress(rs.getString("Present_Contact_Address"));
					dto.setIsInternational(rs.getString("Is_International"));
				} 
				else 
				{
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId);
					rs = pst.executeQuery();
					rs.first();
					dto.setOrganisationInfoId(rs.getString("Organization_Info_Id"));
					dto.setTransferorCustomerId(rs.getString("Customer_Id"));
					dto.setVehicleRegistrationType("Organization");
					dto.setPhone(rs.getString("Phone_Number"));
					dto.setDzongkhag(rs.getString("dzongkhag_name"));
					dto.setGewog(rs.getString("Gewog_Name"));
					dto.setVillage(rs.getString("Village_Name"));
					dto.setEmail(rs.getString("Email_Id"));
					dto.setAddress(rs.getString("Address"));
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setMinistry(rs.getString("Ministry_Name"));
					else
						dto.setMinistry("");
					
					if(rs.getString("Description").equals("GOVERNMENT"))
						dto.setName(rs.getString("Department_Name"));
					else if(rs.getString("Description").equals("PRIVATE"))
						dto.setName(rs.getString("Private_Name"));
					else
						dto.setName(rs.getString("Organization_Name"));
					
					dto.setOwner(rs.getString("Owner_Name"));
					dto.setOwnerTypeDesc(rs.getString("Description"));
				}
				
				if (customerType1.equals("P")) 
				{
					pst = conn.prepareStatement(GET_PERSONAL_DELAILS1);
					pst.setString(1, customerId1);
					rs = pst.executeQuery();
					rs.first();
					dto.setTransfereeVehicleRegistrationType("Personal");
					dto.setTransfereeCustomerId(rs.getString("Customer_Id"));
					dto.setTransfereeName(rs.getString("NAME"));
					dto.setTransfereeDzongkhag(rs.getString("dzongkhag_name"));
					dto.setTransfereeGewog(rs.getString("Gewog_Name"));
					dto.setTransfereeAddress(rs.getString("Present_Contact_Address"));
				}
				else
				{
					pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS1);
					pst.setString(1, customerId1);
					rs = pst.executeQuery();
					rs.first();
					
					dto.setTransfereeVehicleRegistrationType("Organization");
					dto.setTransfereeCustomerId(rs.getString("Customer_Id"));
					dto.setTransfereeName(rs.getString("Organization_Name"));
					dto.setTransfereeDzongkhag(rs.getString("dzongkhag_name"));
					dto.setTransfereeGewog(rs.getString("Gewog_Name"));
					dto.setTransfereeAddress(rs.getString("Address"));
				}
				
				Calendar now = Calendar.getInstance();
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				String adate = formatter.format(now.getTime());
				dto.setTransferDate(adate);
				dto.setReason(rs1.getString("Task_Remark"));
				dto.setVehicleNumber(rs1.getString("Vehicle_Number"));
				dto.setRemarks(rs1.getString("Remarks"));
				dto.setVehicleId(rs1.getString("Vehicle_Id"));
				dto.setSaleDeedAmount(rs1.getString("Sale_Deed_Amount"));
				dto.setSaleDeedDate(rs1.getString("Sale_Deed_Date"));
				dto.setTransferType(rs1.getString("Transfer_Type"));
				
				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNumber());
				rs = pst.executeQuery();
				rs.first();
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptDate"));
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ERALISException("###Error at loadApplicationDAO[getvehicleTransferList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}
	
	public LicenseDTO getlicenseRenewalList(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {

				duration1 = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-ordinary-license"));
				validity = Integer.parseInt(EralisCommonUtil.getNotificationProperty("default-validity-for-commercial-driving-license"));

				pst = conn.prepareStatement(GET_DRIVING_LICENSE_RENEWAL_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();
				dto.setCID(rs.getString("CID_Number"));
				dto.setDuration(rs.getString("Renewal_Duration"));
				String renewalDuration	=	rs.getString("Renewal_Duration");
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setRegion(rs.getString("region_name"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setDrivinglicenseId(rs.getString("License_Id"));
				dto.setIssuedate(rs.getString("issueDate"));
				
				pst = conn.prepareStatement(GET_RENEWAL_PAYMENT);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();

				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("receiptDate"));
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				
				pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_RENEWAL);
				pst.setString(1, dto.getDrivinglicenseId());
				pst.setString(2, dto.getDrivinglicenseId());
				pst.setString(3, dto.getDrivinglicenseId());
				rs = pst.executeQuery();
				rs.first();

				String expiryDateStr = rs.getString("expiryDate");
				java.util.Date expiryDate = sourceSdf.parse(rs.getString("expiryDate"));
				String expiryDate1 = targetSdf.format(expiryDate);
				dto.setExpiryDate(expiryDate1);
				
				DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
				DateTime lastExpiryDate = format.parseDateTime(expiryDateStr);
				
				String[] duration	=	renewalDuration.split("_");
				String p = null;
				if(duration[1].equalsIgnoreCase("month"))
				{
					DateTime a = lastExpiryDate.plusMonths(Integer.parseInt(duration[0]));
					p = a.toString();
				}
				else if(duration[1].equalsIgnoreCase("days"))
				{
					DateTime a = lastExpiryDate.plusDays(Integer.parseInt(duration[0]));
					p = a.toString();
				}
				
				java.util.Date nextExpiry = sourceSdf.parse(p);
				String stringNextExpiry = targetSdf.format(nextExpiry);
				
				//code to find renewal duration in year months and days

				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Format formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				formatter = new SimpleDateFormat("yyyy-MM-dd");
				formatter1 = new SimpleDateFormat("dd/MM/yyyy");
				
				java.util.Date startDate = ((DateFormat) formatter).parse(expiryDateStr);
				java.util.Date endDate = ((DateFormat) formatter1).parse(stringNextExpiry);
				String dateDifference	=	CommonDAO.getDateDifferenceInDDMMYYYY(endDate,startDate);
					
				dto.setRenewalDuration(dateDifference);
				String nextexpiry = targetSdf.format(nextExpiry);
				dto.setNextExpiry(nextexpiry);
				dto.setExpiryDate(expiryDateStr.substring(8, 10)+"/"+expiryDateStr.substring(5, 7)+"/"+expiryDateStr.substring(0, 4));
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException("###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}

	public LicenseDTO getlicenseEndorsementList(LicenseDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try 
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_DRIVING_LICENSE_ENDORSEMENT_APPLICATION_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();

				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setRegion(rs.getString("region_name"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setDrivinglicenseId(rs.getString("License_Id"));
				dto.setDriveTypeId(rs.getString("Drive_Type_Id"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setStatus(rs.getString("Is_Endorsement_Renewal"));
				String isEndorsementRenewal = rs.getString("Is_Endorsement_Renewal");
				if(isEndorsementRenewal.equalsIgnoreCase("Y"))
				{
					
					dto.setDuration(rs.getString("Renewal_Duration"));
					String renewalDuration	=	rs.getString("Renewal_Duration");
					
					pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_RENEWAL);
					pst.setString(1, dto.getDrivinglicenseId());
					pst.setString(2, dto.getDrivinglicenseId());
					pst.setString(3, dto.getDrivinglicenseId());
					rs = pst.executeQuery();
					rs.first();

					String expiryDateStr = rs.getString("expiryDate");
					java.util.Date expiryDate = sourceSdf.parse(rs.getString("expiryDate"));
					String expiryDate1 = targetSdf.format(expiryDate);
					dto.setExpiryDate(expiryDate1);
					
					DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
					DateTime lastExpiryDate = format.parseDateTime(expiryDateStr);
					
					String[] duration	=	renewalDuration.split("_");
					String p = null;
					if(duration[1].equalsIgnoreCase("month"))
					{
						DateTime a = lastExpiryDate.plusMonths(Integer.parseInt(duration[0]));
						p = a.toString();
					}
					else if(duration[1].equalsIgnoreCase("days"))
					{
						DateTime a = lastExpiryDate.plusDays(Integer.parseInt(duration[0]));
						p = a.toString();
					}
					
					java.util.Date nextExpiry = sourceSdf.parse(p);
					String stringNextExpiry = targetSdf.format(nextExpiry);
					
					//code to find renewal duration in year months and days

					Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Format formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					formatter1 = new SimpleDateFormat("dd/MM/yyyy");
					
					java.util.Date startDate = ((DateFormat) formatter).parse(expiryDateStr);
					java.util.Date endDate = ((DateFormat) formatter1).parse(stringNextExpiry);
					String dateDifference	=	CommonDAO.getDateDifferenceInDDMMYYYY(endDate,startDate);
						
					dto.setRenewalDuration(dateDifference);
					String nextexpiry = targetSdf.format(nextExpiry);
					dto.setNextExpiry(nextexpiry);
					dto.setExpiryDate(expiryDateStr.substring(8, 10)+"/"+expiryDateStr.substring(5, 7)+"/"+expiryDateStr.substring(0, 4));
				}
			}
		} 
		catch (Exception e) {
			throw new ERALISException("###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} 
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	// driving license_duplicate
	public LicenseDTO getdrivinglicensenduplicate(LicenseDTO dto)throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_DRIVING_LICENSE_DUPLICATE_APPLICATION);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				dto.setName(rs.getString("NAME"));
				dto.setGender(rs.getString("Gender"));
				dto.setOccupation(rs.getString("Occupation_Name"));
				dto.setBloodgroup(rs.getString("blood_group_type"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setGewog(rs.getString("Gewog_Name"));
				dto.setVillage(rs.getString("Permanant_Village_Id"));
				dto.setRegion(rs.getString("region_name"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setReceiptDate(rs.getString("receiptdate"));
				dto.setDOB(rs.getString("dob"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setAppsubmissiondate(rs.getString("submissiondate"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setRegionId(rs.getString("Region_Id"));
				dto.setReceiptDate1(rs.getString("Receipt_date"));
				dto.setCustomerId(rs.getString("Customer_Id"));
				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setDrivinglicenseId(rs.getString("License_Id"));
				dto.setAmount(rs.getString("Amount_Paid"));
				dto.setPenalty(rs.getString("Penalty_Paid"));
				
				pst = conn.prepareStatement(GET_EXPIRY_FOR_LICENSE_DUPLICATE);
				pst.setString(1, dto.getDrivinglicenseId());
				pst.setString(2, dto.getDrivinglicenseId());
				pst.setString(3, dto.getDrivinglicenseId());
				rs = pst.executeQuery();
				rs.first();
				java.util.Date expiryDate = sourceSdf.parse(rs.getString("expiryDate"));
				String expiryDate1 = targetSdf.format(expiryDate);
				dto.setExpiryDate(expiryDate1);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} 
		finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	public LicenseDTO getTOPList(LicenseDTO dto) throws ERALISException,ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{	
				pst = conn.prepareStatement(GET_TOP_LISTS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();

				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setVehicleNo(rs.getString("Vehicle_Number"));
				dto.setTopNo(rs.getString("TOP_Number"));
				dto.setRegion(rs.getString("region_name"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setExactLocation(rs.getString("Exact_Location"));
				dto.setAppsubmissiondate(rs.getString("appsubmission"));
				dto.setAmount(rs.getString("amount"));
				dto.setReceiptDate(rs.getString("receiptDate"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} 
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}
	
	public LicenseDTO getTOPReplacementDetails(LicenseDTO dto) throws ERALISException,ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try
		{
			conn = ConnectionManager.getConnection();

			if (conn != null) 
			{
				pst = conn.prepareStatement(GET_TOP_REPLACEMENT_DETAILS);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();

				dto.setLicenseNo(rs.getString("Driving_License_No"));
				dto.setVehicleNo(rs.getString("Vehicle_Number"));
				dto.setTopNo(rs.getString("TOP_Number"));
				dto.setRegion(rs.getString("region_name"));
				dto.setDzongkhag(rs.getString("dzongkhag_name"));
				dto.setExactLocation(rs.getString("Exact_Location"));
				dto.setAppsubmissiondate(rs.getString("appsubmission"));
				dto.setAmount(rs.getString("amount"));
				dto.setReceiptDate(rs.getString("receiptDate"));
				dto.setReceiptNo(rs.getString("Receipt_Number"));
				dto.setName(rs.getString("pname"));
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} 
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}
	
	public VehicleDTO getVehicleDtls(String vehicleId,VehicleDTO dto)throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try
		{
			conn = ConnectionManager.getConnection();

			if (conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_DTLS);
				pst.setString(1, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				
				dto.setColour(rs.getString("Colour"));
				dto.setChasisNumber(rs.getString("Chassis_Number"));
				dto.setEngineCC(rs.getString("Engine_CC"));
				dto.setLoadCapacity(rs.getString("Load_Capacity"));
				dto.setSeatCapacity(rs.getString("Seat_Capacity"));
				dto.setEngineNumber(rs.getString("Engine_Number"));
				dto.setVehicleHorsePower(rs.getString("Vehicle_Horse_Power"));
				dto.setVehicleKiloWatt(rs.getString("Vehicle_Kilowatt"));
				dto.setLastRegistrationDate(rs.getString("rgndate"));
				dto.setExpiryDate(rs.getString("expdate"));
				dto.setVehicleCompany(rs.getString("Vehicle_Company_Name"));
				dto.setVehicleModel(rs.getString("Vehicle_Model_Name"));
				dto.setEngineType(rs.getString("Engine_Name"));
				dto.setRegistrationDate(rs.getString("rgndate"));
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException(
					"###Error at loadApplicationDAO[getVehicleDetails]:: " + e);
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public String getRejectionReason(ServiceDTO dto) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			conn = ConnectionManager.getConnection();
		
			if (conn != null) {
				pst = conn.prepareStatement(GET_REJECTION_REASON);
				pst.setString(1, dto.getApplicationNo());
				rs = pst.executeQuery();
				rs.first();
				dto.setReason(rs.getString("Task_Remark"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
	
		return dto.getReason();
	}
	public LicenseDTO getETestResult(LicenseDTO dto) throws ERALISException, ERALISSystemException 
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			conn = ConnectionManager.getConnection();
		
			if (conn != null) {
		
				
				pst = conn.prepareStatement(GET_ETEST_RESULT);
				pst.setString(1, dto.getLearnerlicenseno());
				pst.setString(2, dto.getLearnerlicenseno());
				rs = pst.executeQuery();
				rs.first();
				dto.setTestmarks(rs.getString("Overall_Marks_Obtained"));
				dto.setStatus(rs.getString("Practical_Test_Status"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException(
					"###Error at loadApplicationDAO[getlicenseapplicationList]:: " + e);
		} finally 
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
	
		return dto;
	}

	
	// QUERIES...

	private static final String GET_LICENSE_APPLICATION_LISTS = "SELECT j.`Amount_Paid`,j.`Penalty_Paid`,c.`Personal_Info_Id`,i.`Task_Remark`,a.Region_Id,a.Receipt_Date," +
	"CONCAT(c.First_Name,c.Middle_Name,c.Last_Name) NAME,c.Gender, d.`Occupation_Name`,e.blood_group_type,f.dzongkhag_name," +
	"g.Gewog_Name,c.CID_Number,c.`Permanant_Village_Id` AS Village_Name,DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob," +
	"DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate,a.Receipt_Date," +
	"DATE_FORMAT(a.`App_Submission_Date`,'%d/%m/%Y') submissiondate,b.region_name,a.`Remarks` AS applicationRemarks ,a.`Certifying_Doctor`," +
	"a.`Receipt_No`,c.`Customer_Id`"
	+ "     FROM " + "   t_learner_license_application a "
	+ "   LEFT JOIN t_region_master b "
	+ "   ON a.`Region_id` = b.`region_id` "
	+ "   LEFT JOIN t_personal_dtls c "
	+ "   ON a.`Customer_Id` = c.`Customer_Id` "
	+ "   LEFT JOIN t_occupation_master d "
	+ "   ON c.`Occupation_Id`=d.`Occupation_Id` "
	+ "   LEFT JOIN t_blood_group_master e "
	+ "   ON c.`Blood_Group_Id`=e.`blood_group_id` "
	+ "   LEFT JOIN t_dzongkhag_master f "
	+ "   ON c.`Permanent_Dzongkhag_Id`=f.`dzongkhag_id` "
	+ "   LEFT JOIN t_gewog_master g "
	+ "   ON c.`Permanent_Gewog_Id`=g.`Gewog_Id` "
	+ "   LEFT JOIN t_village_master h "
	+ "   ON c.`Permanant_Village_Id`=h.`Village_Id` "
	+ " LEFT JOIN `t_task_dtls` i ON i.`Application_Number`=a.`Application_Number`" +
	" LEFT JOIN `t_payment_dtls` j ON j.`Application_Number`=a.`Application_Number`"
	+ "   WHERE a.Application_Number =?";

	private static final String GET_LICENSE_DUPLICATION_APPLICATION_LISTS = "SELECT "
		+ "  c.`Personal_Info_Id`, "
		+ "  a.Receipt_No, "
		+ "  b.region_name, "
		+ "  a.`Certifying_Doctor`, "
		+ "  a.`Remarks`, "
		+ "  c.`Customer_Id`, "
		+ "  a.`Learner_License_Id`, "
		+ "  a.`Learner_License_No`, "
		+ "  a.Region_Id, "
		+ "  a.Receipt_Date, "
		+ "  CONCAT( "
		+ "    c.First_Name, "
		+ "    c.Middle_Name, "
		+ "    c.Last_Name "
		+ "  ) NAME, "
		+ "  c.Gender, "
		+ "  `Occupation_Name`, "
		+ "  blood_group_type, "
		+ "  dzongkhag_name, "
		+ "  Gewog_Name, "
		+ "  Village_Name, "
		+ "  DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate, "
		+ "  DATE_FORMAT( "
		+ "    a.`App_Submission_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) submissiondate, "
		+ "  b.region_name, "
		+ "  pay.Amount_Paid, "
		+ "  pay.Penalty_Paid "
		+ "FROM "
		+ "  t_learner_license_application a "
		+ "  LEFT JOIN t_region_master b "
		+ "    ON a.`Region_id` = b.`region_id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN t_occupation_master d "
		+ "    ON c.`Occupation_Id` = d.`Occupation_Id` "
		+ "  LEFT JOIN t_blood_group_master e "
		+ "    ON c.`Blood_Group_Id` = e.`blood_group_id` "
		+ "  LEFT JOIN t_dzongkhag_master f "
		+ "    ON c.`Permanent_Dzongkhag_Id` = f.`dzongkhag_id` "
		+ "  LEFT JOIN t_gewog_master g "
		+ "    ON c.`Permanent_Gewog_Id` = g.`Gewog_Id` "
		+ "  LEFT JOIN t_village_master h "
		+ "    ON c.`Permanant_Village_Id` = h.`Village_Id` "
		+ "  LEFT JOIN t_payment_dtls pay "
		+ "    ON a.Application_Number = pay.Application_Number "
		+ "WHERE a.Application_Number = ?";

	private static final String GET_VEHICLE_REGISTRATION_APPLICATION_LISTS = "SELECT l.`bus_type_name`,"
			+ "   a.Vehicle_Registration_Type,a.Customer_Id,b.Vehicle_Reg_Code_Name,a.Amount,a.Vehicle_Registration_Id," +
				"c.region_name,a.Vehicle_Prefix, a.Vehicle_Number,d.Vehicle_Type_Name, a.Vanity_Number,e.Vehicle_Company_Name," +
				"f.Vehicle_Model_Name,g.Engine_Name,a.Engine_Number,a.Chasis_Number,a.Status,(SELECT Colour_Name FROM t_colour_master WHERE `Colour_Id`=a.Color)  Color,a.Price,a.Load_Capacity," +
				"h.Dealer_Name,a.Seat_Capacity,i.Hypothecated_Name,a.Unladen_Weight,a.Letter_Number," +
				"DATE_FORMAT(a.`Letter_Date`, '%d-%m-%Y') letterdate,a.Manufacture_Year,a.Engine_CC,j.Country_Name,a.Purchase_Type," +
				" a.Vehicle_Horse_Power,a.Vehicle_Kilowatt,a.Remarks,k.`Task_Remark`,a.Renewal_Duration ,a.`Registration_Type`," +
				"a.`Number_Of_Axle`,a.`Number_Of_Wheels`," +
				"a.`Owner_Mobile_Number`," +
				"a.`Quota_Holder_Mobile_Number`, m.`Registration_Type_Name` registrationTypeDesc"
			+ " FROM "
			+ " t_vehicle_application a "
			+ " LEFT JOIN t_vehicle_registration_code_master b "
			+ " ON a.Vehicle_Registration_Id = b.Vehicle_Registration_Id "
			+ " LEFT JOIN t_region_master c "
			+ " ON a.Region_Id = c.region_id "
			+ " LEFT JOIN t_vehicle_type_master d "
			+ " ON a.Vehicle_Type_Id = d.Vehicle_Type_Id "
			+ " LEFT JOIN t_vehicle_company_master e "
			+ " ON a.Vehicle_Company_Id = e.Vehicle_Company_Id "
			+ " LEFT JOIN t_vehicle_model_master f "
			+ " ON a.Vehicle_Model_Id = f.Vehicle_Model_Id "
			+ " LEFT JOIN t_engine_type_master g "
			+ " ON a.Engine_Type_Id = g.Engine_Type_Id "
			+ " LEFT JOIN t_dealer_master h "
			+ " ON a.Dealer_Id = h.Dealer_Id "
			+ " LEFT JOIN t_hypothecated_to_master i "
			+ " ON a.Hypothecated_To_Id = i.Hypothecated_Id "
			+ " LEFT JOIN t_country_master j "
			+ " ON a.Manufactured_Country_Id = j.Country_Id " 
			+ " LEFT JOIN `t_task_dtls` k ON k.`Application_Number`=a.`Application_Number` " 
			+ " LEFT JOIN `t_bus_type_master` l ON a.`Bus_Type_Id`=l.`bus_type_id` "
			+ " LEFT JOIN t_registration_type_master m ON a.`Registration_Type`=m.`Registration_Type_Id`"
			+ " WHERE a.Application_Number = ?";

	/*private static final String GET_VEHICLE_RENEWAL_APPLICATION_LISTS = "SELECT a.`Renewal_Duration`,e.`Description`," 
			+" c.`Task_Remark`,a.`Customer_Id`,a.`Vehicle_Id`," +
			"a.`Vehicle_Registration_Type`,a.`Vehicle_Number`,a.`Remarks`" +
			",DATE_FORMAT( b.`Registration_Date`,'%d/%m/%Y') rgdate," 
			+" DATE_FORMAT(b.`Expiry_Date`, '%d/%m/%Y') exdate,d.`Document_Path`  "
			+ " FROM t_vehicle_application a "
			+ " LEFT JOIN t_vehicle_registration_dtls b "
			+ " ON a.Vehicle_Number = b.Vehicle_Number " 
			+" LEFT JOIN `t_task_dtls` c ON c.`Application_Number`=a.`Application_Number`" 
			+" LEFT JOIN `t_document_dtls` d ON d.`Application_Number`=a.`Application_Number`" +
			" LEFT JOIN `t_vehicle_type_master` e ON e.`Vehicle_Type_Id`=b.`Vehicle_Type_Id`"
			+" WHERE a.Application_Number=?";*/
	

	private static final String GET_VEHICLE_RENEWAL_APPLICATION_LISTS = "SELECT "
		+ "  a.`Renewal_Duration`, "
		+ "  e.`Description`, "
		+ "  IFNULL(c.`Task_Remark`,'') Task_Remark, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Vehicle_Id`, "
		+ "  a.`Vehicle_Registration_Type`, "
		+ "  b.`Vehicle_Number`, "
		+ "  IFNULL(a.`Remarks`,'') Remarks, "
		+ "  DATE_FORMAT( "
		+ "    b.`Registration_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) rgdate, "
		+ "  DATE_FORMAT(b.`Expiry_Date`, '%d/%m/%Y') exdate, "
		+ "  d.`Document_Path`,b.Seat_Capacity,"
		+ "  IF(b.Vehicle_Type_Id = 18,"
		+ "  b.Vehicle_Horse_Power,"
		+ "  IF(b.Engine_Type_Id = 4,"
		+ "  b.Vehicle_Kilowatt,"
		+ "  IF("
		+ "  b.Vehicle_Type_Id IN (2, 3),"
		+ "  b.Load_Capacity,"
		+ "  IF( b.Vehicle_Type_Id IN (5, 6, 7), b.Engine_CC,"
		+ "  'NA'"
		+ "  )"
		+ "  )"
		+ "  )"
		+ "  )engineCCGvw ,e.Vehicle_Type_Name,f.Vehicle_Reg_Code_Name "
		+ " FROM "
		+ "  t_vehicle_application a "
		+ "  LEFT JOIN t_vehicle_registration_dtls b "
		+ "    ON a.Vehicle_Id = b.Vehicle_Reg_Dtls_Id "
		+ "  LEFT JOIN `t_task_dtls` c "
		+ "    ON c.`Application_Number` = a.`Application_Number` "
		+ "  LEFT JOIN `t_document_dtls` d "
		+ "    ON d.`Application_Number` = a.`Application_Number` "
		+ "  LEFT JOIN `t_vehicle_type_master` e "
		+ "    ON e.`Vehicle_Type_Id` = b.`Vehicle_Type_Id` " 
		+ "  LEFT JOIN t_vehicle_registration_code_master f on b.Vehicle_Registration_Id=f.Vehicle_Registration_Id"
		+ " WHERE a.Application_Number = ?";

	private static final String GET_VEHICLE_CONVERSION_APPLICATION_LISTS = "SELECT a.`Vehicle_Type_Id`,"
		+ "  a.`Customer_Id`, "
		+ "  a.`Vehicle_Registration_Type`, "
		+ "  a.`Conversion_Reason_Id`, "
		+ "  a.`Vehicle_Registration_Id`, "
		+ "  a.`Region_Id`, "
		+ "  a.`Vehicle_Type_Id`, "
		+ "  a.`Vehicle_Id`, "
		+ "  b.Reason, "
		+ "  c.Vehicle_Reg_Code_Name, "
		+ "  a.Vehicle_Prefix, "
		+ "  a.Vehicle_Number, "
		+ "  d.`Task_Remark`, "
		+ "  e.`Vehicle_Type_Name`, "
		+ "  f.`region_name` "
		+ "FROM "
		+ "  t_vehicle_application a "
		+ "  LEFT JOIN t_reason_master b "
		+ "    ON a.Conversion_Reason_Id = b.Reason_Id "
		+ "  LEFT JOIN t_vehicle_registration_code_master c "
		+ "    ON a.Vehicle_Registration_Id = c.Vehicle_Registration_Id "
		+ "  LEFT JOIN `t_task_dtls` d "
		+ "    ON d.`Application_Number` = a.`Application_Number` "
		+ "  LEFT JOIN t_vehicle_type_master e "
		+ "    ON a.`Vehicle_Type_Id` = e.`Vehicle_Type_Id` "
		+ "  LEFT JOIN t_region_master f "
		+ "    ON a.`Region_Id` = f.`region_id` "
		+ "WHERE a.Application_Number = ?";

	private static final String GET_VEHICLE_DUPLICATION_APPLICATION_LISTS = "SELECT "
		+ "  a.`Customer_Id`, "
		+ "  a.`Vehicle_Registration_Type`, "
		+ "  a.`Remarks`, "
		+ "  a.`Vehicle_Id`, "
		+ "  a.`Traffic_Remarks`, "
		+ "  a.Remarks, "
		+ "  b.`Vehicle_Number`, "
		+ "  c.`region_name` "
		+ "FROM "
		+ "  t_vehicle_application a "
		+ "  LEFT JOIN `t_vehicle_registration_dtls` b "
		+ "    ON b.`Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id` "
		+ "  LEFT JOIN t_region_master c "
		+ "    ON a.`Region_Id` = c.`region_id` "
		+ "WHERE a.Application_Number = ?";

	private static final String GET_VEHICLE_TRANSFER_APPLICATION_LISTS = "SELECT a.`Sale_Deed_Amount`,a.`Transfer_Type`,DATE_FORMAT(a.`Sale_Deed_Date`,'%d/%m/%Y') AS Sale_Deed_Date,"
			+ "a.`Transferee_Customer_Id`,a.`Transferor_Customer_Id`,a.`Vehicle_Id`,a.`Vehicle_Number`, "
			+ "DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receiptDate, "
			+ "a.Receipt_Number, "
			+ "a.Remarks ,b.`Task_Remark`"
			+ " FROM t_vehicle_application a" 
			+ " LEFT JOIN `t_task_dtls` b ON b.`Application_Number`=a.`Application_Number`"
			+ " WHERE a.Application_Number =?;";

	// //////////////
	private static final String GET_PERSONAL_DELAILS1 = "SELECT a.`Personal_Info_Id`,"
		+ "  k.`blood_group_type`, "
		+ "  k.`blood_group_type`, "
		+ "  DATE_FORMAT(a.`Date_Of_Birth`, '%d-%m-%Y') dob, "
		+ "  a.`Present_Email`, "
		+ "  a.`Is_International`, "
		+ "  a.`Present_Contact_Address`, "
		+ "  a.Customer_Id, "
		+ "  CONCAT( "
		+ "    a.First_Name, "
		+ "    a.Middle_Name, "
		+ "    a.Last_Name "
		+ "  ) NAME, "
		+ "  a.CID_Number, "
		+ "  a.Present_Phone_No, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  d.Country_Name, "
		+ "  a.Permanant_Village_Id AS Village_Name, "
		+ "  a.Permanant_Village_Id, "
		+ "  a.Permanent_Address "
		+ "FROM "
		+ "  t_personal_dtls a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Permanent_Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Permanent_Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_country_master d "
		+ "    ON a.Permanent_Country_Id = d.Country_Id "
		+ "  LEFT JOIN t_blood_group_master k "
		+ "    ON k.`blood_group_id` = a.`Blood_Group_Id` "
		+ "WHERE a.Customer_Id = ?";

	//private static final String GET_ORGANIZATION_DETAILS1 = "SELECT a.`Organization_Info_Id`,";
	/*private static final String GET_ORGANIZATION_DETAILS1 = "SELECT "
		+ "  a.Customer_Id, "
		+ "  a.`Village_Id`, "
		+ "  a.Organization_Name, "
		+ "  a.Phone_Number, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  i.Village_Name,a.Address,a.`Email_Id` "
		+ "FROM "
		+ "  t_organization_info a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_village_master i "
		+ "    ON a.Village_Id = i.Village_Id "
		+ "WHERE a.Customer_Id = ?";*/
	
	private static final String GET_ORGANIZATION_DETAILS1 = "SELECT a.Organization_Info_Id,"
		+ "  a.`Organization_Type_Id`, "
		+ "  a.`Address`, "
		+ "  a.Customer_Id, "
		+ "  a.Phone_Number, "
		+ "  b.dzongkhag_name, "
		+ "  c.Gewog_Name, "
		+ "  i.Village_Name, "
		+ "  a.Email_Id, "
		+ "  m.`Ministry_Name`, "
		+ "  d.`Department_Name`, "
		+ "  e.`Private_Name`, "
		+ "  o.`Owner_Name`, "
		+ "  o.`Description`," 
		+ "	 a.Organization_Name "
		+ "FROM "
		+ "  t_organization_info a "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Dzongkhag_Id = b.dzongkhag_id "
		+ "  LEFT JOIN t_gewog_master c "
		+ "    ON a.Gewog_Id = c.Gewog_Id "
		+ "  LEFT JOIN t_village_master i "
		+ "    ON a.Village_Id = i.Village_Id "
		+ "  LEFT JOIN t_ministry_master m "
		+ "    ON a.`Ministry_Id` = m.`Ministry_Id` "
		+ "  LEFT JOIN t_department_master d "
		+ "    ON a.`Department_Id` = d.`Department_Id` "
		+ "  LEFT JOIN t_private_company_master e "
		+ "    ON a.`Private_Id` = e.`Private_Id` "
		+ "  LEFT JOIN t_owner_master o "
		+ "    ON a.`Organization_Type_Id` = o.`Owner_Type_Id` "
		+ "WHERE a.Customer_Id = ?";
	
	private static final String GET_VEHICLE_DETAILS = "SELECT "
		+ "  b.`Vehicle_Company_Name`, "
		+ "  c.`Vehicle_Model_Name`, "
		+ "  a.`Colour`, "
		+ "  a.`Chassis_Number`, "
		+ "  a.`Engine_Number`, "
		+ "  d.`Engine_Name`, "
		+ "  a.`Seat_Capacity`, "
		+ "  a.`Load_Capacity` "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "  LEFT JOIN t_vehicle_company_master b "
		+ "    ON a.`Vehicle_Company_Id` = b.`Vehicle_Company_Id` "
		+ "  LEFT JOIN t_vehicle_model_master c "
		+ "    ON a.`Vehicle_Model_Id` = c.`Vehicle_Model_Id` "
		+ "  LEFT JOIN t_engine_type_master d "
		+ "    ON a.`Engine_Type_Id` = d.`Engine_Type_Id` "
		+ "WHERE a.`Vehicle_Reg_Dtls_Id` = ?";
	
	private static final String GET_VALIDITY_DURATION = "SELECT Renewal_Duration FROM t_vehicle_application a WHERE a.`Application_Number`=?";

	/*
	 * private static final String GET_PERSONAL_OWNER_NAME = "SELECT " +
	 * "  a.First_Name, " + "  CONCAT( " + "    a.First_Name, " + "    \" \", "
	 * + "    a.Middle_Name, " + "    \" \", " + "    a.Last_Name " +
	 * "  ) name " + "FROM " + "  t_personal_dtls a " +
	 * "WHERE a.Customer_Id = ?";
	 * 
	 * private static final String GET_ORGANIZATION_OWNER_NAME =
	 * "SELECT Organization_Name name FROM t_organization_info WHERE Customer_Id=?"
	 * ;
	 */

	private static final String GET_LICENSE_RENEWAL_APPLICATION_LISTS = "SELECT (select date_format(t_learner_license_dtls.Issue_Date,'%d/%m/%Y') " +
			" from t_learner_license_dtls where t_learner_license_dtls.Customer_Id=a.Customer_Id limit 1)Issue_Date,"
		+ "  c.Personal_Info_Id,c.CID_Number, "
		+ "  a.Receipt_No, "
		+ "  a.`Certifying_Doctor`, "
		+ "  a.`Remarks`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Learner_License_Id`, "
		+ "  COUNT(a.`Application_Number`) AS rowCount, "
		+ "  a.Region_Id, "
		+ "  a.Receipt_Date, "
		+ "  pay.Amount_Paid, "
		+ "  pay.Penalty_Paid, "
		+ "  CONCAT( "
		+ "    c.First_Name, "
		+ "    c.Middle_Name, "
		+ "    c.Last_Name "
		+ "  ) NAME, "
		+ "  c.Gender, "
		+ "  `Occupation_Name`, "
		+ "  blood_group_type, "
		+ "  dzongkhag_name, "
		+ "  Gewog_Name, "
		+ "  c.`Permanant_Village_Id` AS Village_Name, "
		+ "  DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate, "
		+ "  DATE_FORMAT( "
		+ "    a.`App_Submission_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) submissiondate, "
		+ "  b.region_name, "
		+ "  a.Learner_License_No, "
		+ "  i.`Task_Remark`, "
		+ "  a.`Renewal_Duration` "
		+ "FROM "
		+ "  t_learner_license_application a "
		+ "  LEFT JOIN t_region_master b "
		+ "    ON a.`Region_id` = b.`region_id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN t_occupation_master d "
		+ "    ON c.`Occupation_Id` = d.`Occupation_Id` "
		+ "  LEFT JOIN t_blood_group_master e "
		+ "    ON c.`Blood_Group_Id` = e.`blood_group_id` "
		+ "  LEFT JOIN t_dzongkhag_master f "
		+ "    ON c.`Permanent_Dzongkhag_Id` = f.`dzongkhag_id` "
		+ "  LEFT JOIN t_gewog_master g "
		+ "    ON c.`Permanent_Gewog_Id` = g.`Gewog_Id` "
		+ "  LEFT JOIN t_village_master h "
		+ "    ON c.`Permanant_Village_Id` = h.`Village_Id` "
		+ "  LEFT JOIN `t_task_dtls` i "
		+ "    ON i.`Application_Number` = a.`Application_Number` "
		+ "  LEFT JOIN t_payment_dtls pay "
		+ "    ON a.Application_Number = pay.Application_Number "
		+ "WHERE a.Application_Number = ?";


	private static final String GET_NONCOMMERCIAL_APPLICATION_LISTS = "SELECT "
		+ "  l.`Learner_License_Info_Id`, "
		+ "  c.`Personal_Info_Id`, "
		+ "  a.Receipt_Number, "
		+ "  j.`Drive_Type_Name` AS driveType, "
		+ "  a.`Drive_Type_Id`, "
		+ "  a.`Remarks`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Region_Id`, "
		+ "  a.`Renewal_Duration`, "
		+ "  c.`CID_Number`, "
		+ "  a.`Issue_type`, "
		+ "  CONCAT( "
		+ "    c.First_Name, "
		+ "    c.Middle_Name, "
		+ "    c.Last_Name "
		+ "  ) NAME, "
		+ "  c.Gender, "
		+ "  `Occupation_Name`, "
		+ "  blood_group_type, "
		+ "  dzongkhag_name, "
		+ "  Gewog_Name, "
		+ "  c.`Permanant_Village_Id` AS Village_Name, "
		+ "  DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate, "
		+ "  a.Receipt_Date, "
		+ "  DATE_FORMAT( "
		+ "    a.`App_Submission_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) submissiondate, "
		+ "  b.region_name, "
		+ "  l.Learner_License_No AS driving_learner_license_no, "
		+ "  i.`Task_Remark`, "
		+ "  pay.Amount_Paid "
		+ "FROM "
		+ "  t_driving_license_application a "
		+ "  LEFT JOIN t_region_master b "
		+ "    ON a.`Region_id` = b.`region_id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN t_occupation_master d "
		+ "    ON c.`Occupation_Id` = d.`Occupation_Id` "
		+ "  LEFT JOIN t_blood_group_master e "
		+ "    ON c.`Blood_Group_Id` = e.`blood_group_id` "
		+ "  LEFT JOIN t_dzongkhag_master f "
		+ "    ON c.`Permanent_Dzongkhag_Id` = f.`dzongkhag_id` "
		+ "  LEFT JOIN t_gewog_master g "
		+ "    ON c.`Permanent_Gewog_Id` = g.`Gewog_Id` "
		+ "  LEFT JOIN t_village_master h "
		+ "    ON c.`Permanant_Village_Id` = h.`Village_Id` "
		+ "  LEFT JOIN t_learner_license_dtls l "
		+ "    ON a.`Customer_Id` = l.`Customer_Id` "
		+ "  LEFT JOIN `t_task_dtls` i "
		+ "    ON i.`Application_Number` = a.`Application_Number` "
		+ "  LEFT JOIN `t_drive_type_master` j "
		+ "    ON j.`Drive_Type_Id` = a.`Drive_Type_Id` "
		+ "  LEFT JOIN t_payment_dtls pay "
		+ "    ON a.Application_Number = pay.Application_Number "
		+ "WHERE a.Application_Number = ?";

	private static final String GET_COMMERCIAL_APPLICATION_LISTS = "SELECT c.`Personal_Info_Id`,"
			+ "   a.`Receipt_Number`,a.`Remarks`,a.`Customer_Id`,a.`Driving_License_No`,a.`Drive_Type_Id`,a.`License_Id`,a.`Renewal_Duration`,c.`CID_Number`, "
			+ " CONCAT( "
			+ " c.First_Name, "
			+ "   "
			+ " c.Middle_Name, "
			+ "   "
			+ " c.Last_Name "
			+ " ) NAME , "
			+ " c.Gender, "
			+ " `Occupation_Name`, "
			+ " blood_group_type, "
			+ " dzongkhag_name, "
			+ " Gewog_Name, "
			+ " c.Permanant_Village_Id, "
			+ " DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob,"
			+ " DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate,a.Receipt_Date, "
			+ " DATE_FORMAT(a.`App_Submission_Date`, '%d/%m/%Y') submissiondate, "
			+ " b.region_name ,i.`Task_Remark`,j.`Drive_Type_Name`" + " FROM "
			+ " t_driving_license_application a "
			+ " LEFT JOIN t_region_master b "
			+ " ON a.`Region_id` = b.`region_id` "
			+ " LEFT JOIN t_personal_dtls c "
			+ " ON a.`Customer_Id` = c.`Customer_Id` "
			+ " LEFT JOIN t_occupation_master d "
			+ " ON c.`Occupation_Id`=d.`Occupation_Id` "
			+ " LEFT JOIN t_blood_group_master e "
			+ " ON c.`Blood_Group_Id`=e.`blood_group_id` "
			+ " LEFT JOIN t_dzongkhag_master f "
			+ " ON c.`Permanent_Dzongkhag_Id`=f.`dzongkhag_id` "
			+ " LEFT JOIN t_gewog_master g "
			+ " ON c.`Permanent_Gewog_Id`=g.`Gewog_Id` "
			+ " LEFT JOIN t_village_master h "
			+ " ON c.`Permanant_Village_Id`=h.`Village_Id`" 
			+" LEFT JOIN `t_task_dtls` i ON i.`Application_Number`=a.`Application_Number`  "
			+" LEFT JOIN `t_drive_type_master` j ON j.`Drive_Type_Id`=a.`Drive_Type_Id` "
			+ " WHERE a.Application_Number =?";

	private static final String GET_DRIVING_LICENSE_RENEWAL_APPLICATION_LISTS = "SELECT c.CID_Number,c.`Personal_Info_Id`,"
			+ "  c.`Permanant_Village_Id`,a.`Receipt_Number`,a.`Remarks`,c.`Customer_Id`,a.`Driving_License_No`,a.`License_Id`, "
			+ " CONCAT( "
			+ " c.First_Name, "
			+ "   "
			+ " c.Middle_Name, "
			+ "   "
			+ " c.Last_Name "
			+ " ) NAME , "
			+ " c.Gender, "
			+ " `Occupation_Name`, "
			+ "blood_group_type, "
			+ "dzongkhag_name, "
			+ "Gewog_Name, "
			+ "Village_Name, "
			+ " DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob,"
			+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate, "
			+ "  DATE_FORMAT(a.`App_Submission_Date`, '%d/%m/%Y') submissiondate,a.Renewal_Duration, "
			+ "   b.region_name,a.Receipt_Date," +
					"(select date_format(t_driving_license_dtls.Issue_Date,'%d/%m/%Y') " +
					"from t_driving_license_dtls where t_driving_license_dtls.Driving_License_Id=a.License_Id)issueDate "
			+ "   FROM "
			+ " t_driving_license_application a "
			+ " LEFT JOIN t_region_master b "
			+ " ON a.`Region_id` = b.`region_id` "
			+ " LEFT JOIN t_personal_dtls c "
			+ " ON a.`Customer_Id` = c.`Customer_Id` "
			+ " LEFT JOIN t_occupation_master d "
			+ " ON c.`Occupation_Id`=d.`Occupation_Id` "
			+ " LEFT JOIN t_blood_group_master e "
			+ " ON c.`Blood_Group_Id`=e.`blood_group_id` "
			+ " LEFT JOIN t_dzongkhag_master f "
			+ " ON c.`Permanent_Dzongkhag_Id`=f.`dzongkhag_id` "
			+ " LEFT JOIN t_gewog_master g "
			+ " ON c.`Permanent_Gewog_Id`=g.`Gewog_Id` "
			+ " LEFT JOIN t_village_master h "
			+ " ON c.`Permanant_Village_Id`=h.`Village_Id` "
			+ " WHERE a.Application_Number =?";

	private static final String GET_DRIVING_LICENSE_ENDORSEMENT_APPLICATION_LISTS = "SELECT c.`Personal_Info_Id`,a.Drive_Type_Id,"
		+ " c.`Permanant_Village_Id`,a.`Receipt_Number`,a.`Remarks`,a.`Customer_Id`,a.`Driving_License_No`,a.`License_Id`,c.`CID_Number`, "
		+ "  CONCAT( "
		+ "    c.First_Name, "
		+ "    c.Middle_Name, "
		+ "    c.Last_Name "
		+ "  ) NAME, "
		+ "  c.Gender, "
		+ "  `Occupation_Name`, "
		+ "  blood_group_type, "
		+ "  dzongkhag_name, "
		+ "  Gewog_Name, "
		+ "  DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate, "
		+ "  DATE_FORMAT( "
		+ "    a.`App_Submission_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) submissiondate, "
		+ "  b.region_name, "
		+ "  h.`Amount_Paid`,a.Is_TCB_Endorsement,a.`Is_Endorsement_Renewal`,a.`Renewal_Duration` "
		+ "FROM "
		+ "  t_driving_license_application a "
		+ "  LEFT JOIN t_region_master b "
		+ "    ON a.`Region_id` = b.`region_id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN t_occupation_master d "
		+ "    ON c.`Occupation_Id` = d.`Occupation_Id` "
		+ "  LEFT JOIN t_blood_group_master e "
		+ "    ON c.`Blood_Group_Id` = e.`blood_group_id` "
		+ "  LEFT JOIN t_dzongkhag_master f "
		+ "    ON c.`Permanent_Dzongkhag_Id` = f.`dzongkhag_id` "
		+ "  LEFT JOIN t_gewog_master g "
		+ "    ON c.`Permanent_Gewog_Id` = g.`Gewog_Id` "
		+ "  LEFT JOIN t_payment_dtls h "
		+ "    ON a.`Application_Number`=h.`Application_Number` "
		+ "WHERE a.Application_Number = ?";
	
	private static final String GET_DRIVING_LICENSE_DUPLICATION_APPLICATION_LISTS = "SELECT "
		+ "  c.`Permanant_Village_Id`, "
		+ "  a.`Receipt_Number`, "
		+ "  a.`Remarks`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Region_Id`, "
		+ "  a.`Driving_License_No`, "
		+ "  a.`License_Id`, "
		+ "  CONCAT( "
		+ "    c.First_Name, "
		+ "    c.Middle_Name, "
		+ "    c.Last_Name "
		+ "  ) NAME, "
		+ "  c.Gender, "
		+ "  `Occupation_Name`, "
		+ "  blood_group_type, "
		+ "  dzongkhag_name, "
		+ "  Gewog_Name, "
		+ "  Village_Name, "
		+ "  DATE_FORMAT(c.`Date_Of_Birth`, '%d/%m/%Y') dob, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptdate, "
		+ "  a.Receipt_date, "
		+ "  DATE_FORMAT( "
		+ "    a.`App_Submission_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) submissiondate, "
		+ "  b.region_name, "
		+ "  pay.Amount_Paid, "
		+ "  pay.Penalty_Paid "
		+ "FROM "
		+ "  t_driving_license_application a "
		+ "  LEFT JOIN t_region_master b "
		+ "    ON a.`Region_id` = b.`region_id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN t_occupation_master d "
		+ "    ON c.`Occupation_Id` = d.`Occupation_Id` "
		+ "  LEFT JOIN t_blood_group_master e "
		+ "    ON c.`Blood_Group_Id` = e.`blood_group_id` "
		+ "  LEFT JOIN t_dzongkhag_master f "
		+ "    ON c.`Permanent_Dzongkhag_Id` = f.`dzongkhag_id` "
		+ "  LEFT JOIN t_gewog_master g "
		+ "    ON c.`Permanent_Gewog_Id` = g.`Gewog_Id` "
		+ "  LEFT JOIN t_village_master h "
		+ "    ON c.`Permanant_Village_Id` = h.`Village_Id` "
		+ "  LEFT JOIN t_payment_dtls pay "
		+ "    ON a.Application_Number = pay.Application_Number "
		+ "WHERE a.Application_Number = ?";


	private static final String GET_DRIVING_LICENSE_DUPLICATE_APPLICATION = "SELECT "
		+ "  c.`Personal_Info_Id`, "
		+ "  a.Receipt_date, "
		+ "  a.Region_Id, "
		+ "  c.`Permanant_Village_Id`, "
		+ "  a.`Receipt_Number`, "
		+ "  a.`Remarks`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`Driving_License_No`, "
		+ "  a.`License_Id`, "
		+ "  CONCAT( "
		+ "    c.First_Name, "
		+ "    c.Middle_Name, "
		+ "    c.Last_Name "
		+ "  ) NAME, "
		+ "  c.Gender, "
		+ "  `Occupation_Name`, "
		+ "  blood_group_type, "
		+ "  dzongkhag_name, "
		+ "  Gewog_Name, "
		+ "  Village_Name, "
		+ "  DATE_FORMAT(c.`Date_Of_Birth`, '%d-%m-%Y') dob, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receiptdate, "
		+ "  DATE_FORMAT( "
		+ "    a.`App_Submission_Date`, "
		+ "    '%d-%m-%Y' "
		+ "  ) submissiondate, "
		+ "  b.region_name, "
		+ "  pay.Amount_Paid, "
		+ "  pay.Penalty_Paid "
		+ "FROM "
		+ "  t_driving_license_application a "
		+ "  LEFT JOIN t_region_master b "
		+ "    ON a.`Region_id` = b.`region_id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN t_occupation_master d "
		+ "    ON c.`Occupation_Id` = d.`Occupation_Id` "
		+ "  LEFT JOIN t_blood_group_master e "
		+ "    ON c.`Blood_Group_Id` = e.`blood_group_id` "
		+ "  LEFT JOIN t_dzongkhag_master f "
		+ "    ON c.`Permanent_Dzongkhag_Id` = f.`dzongkhag_id` "
		+ "  LEFT JOIN t_gewog_master g "
		+ "    ON c.`Permanent_Gewog_Id` = g.`Gewog_Id` "
		+ "  LEFT JOIN t_village_master h "
		+ "    ON c.`Permanant_Village_Id` = h.`Village_Id` "
		+ "  LEFT JOIN t_payment_dtls pay "
		+ "    ON a.Application_Number = pay.Application_Number "
		+ "WHERE a.Application_Number = ?";


	private static final String DIFFERENT_VEHICLE_TYPE = "SELECT *, "
			+ "b.Description " + "FROM " + "t_vehicle_application a "
			+ "LEFT JOIN t_vehicle_type_master b "
			+ "ON a.Vehicle_Type_Id = b.Vehicle_Type_Id "
			+ "WHERE a.Application_Number=?";
 private static final String LAST_EXPIRY_DATE =  "SELECT "
	 + "  IF( "
	 + "    a.`Vehicle_Reg_Dtls_Id` IN "
	 + "    (SELECT "
	 + "      `Vehicle_Id` "
	 + "    FROM "
	 + "      t_vehicle_renewal_dtls "
	 + "    WHERE `Vehicle_Id` = ?), "
	 + "    (SELECT "
	 + "      MAX(Expiry_Date) "
	 + "    FROM "
	 + "      t_vehicle_renewal_dtls "
	 + "    WHERE `Vehicle_Id` = ?), "
	 + "    a.`Expiry_Date` "
	 + "  ) expiryDate "
	 + "FROM "
	 + "  t_vehicle_registration_dtls a "
	 + "WHERE a.`Vehicle_Reg_Dtls_Id` = ?";
 
 private static final String GET_RENEWAL_AND_EXPIRY_DATE = "SELECT "
	 + "  DATE_FORMAT(Renewal_Date, '%d/%m/%Y') renewalDate, "
	 + "  DATE_FORMAT(Expiry_Date, '%d/%m/%Y') expiryDate "
	 + "FROM "
	 + "  t_vehicle_renewal_dtls "
	 + "WHERE Application_Number = ?";
 
 private static final String GET_VEHICLE_RENEWAL_DURATION = "SELECT a.Renewal_Duration FROM t_vehicle_application a WHERE a.`Application_Number`=?";

	private static final String DIFFERENT_VEHICLE_DESCRIPTION = "SELECT *, "
			+ "b.Description " + "FROM " + "t_vehicle_application a "
			+ "LEFT JOIN t_vehicle_registration_dtls d "
			+ "ON a.Vehicle_Number = d.Vehicle_Number "
			+ "LEFT JOIN t_vehicle_type_master b "
			+ "ON b.Vehicle_Type_Id = d.Vehicle_Type_Id "
			+ "WHERE a.Vehicle_Number=?";

	private static final String GET_EXPIRY_FOR_RENEWAL = "SELECT "
			+ "  IF( "
			+ "    a.`Learner_License_Info_Id` IN "
			+ "    (SELECT "
			+ "      b.`Learner_License_Id` "
			+ "    FROM "
			+ "      t_learner_license_renewal_dtls b WHERE b.`Learner_License_Id`=a.`Learner_License_Info_Id`), "
			+ "    (SELECT "
			+ "      MAX(c.Expiry_Date) "
			+ "    FROM "
			+ "      t_learner_license_renewal_dtls  c  WHERE c.`Learner_License_Id`=a.`Learner_License_Info_Id`), "
			+ "    a.`Expiry_Date` " + "  ) expiryDate " + "FROM "
			+ "  t_learner_license_dtls a "
			+ "WHERE a.`Learner_License_Info_Id` = ?";

	private static final String GET_EXPIRY_FOR_LICENSE_RENEWAL = "SELECT "
			+ "  IF( " + "    a.`Driving_License_Id` IN " + "    (SELECT "
			+ "      `License_Id` " + "    FROM "
			+ "      t_driving_license_renewal_dtls "
			+ "    WHERE `License_Id` = ?), " + "    (SELECT "
			+ "      MAX(Expiry_Date) " + "    FROM "
			+ "      t_driving_license_renewal_dtls "
			+ "    WHERE `License_Id` = ?), " + "    a.`Expiry_Date` "
			+ "  ) expiryDate " + "FROM " + "  t_driving_license_dtls a "
			+ "WHERE a.`Driving_License_Id` = ?";

	private static final String GET_LICENSE_TYPE = "SELECT  "
			+ "a.Driving_license_type_Id " + "FROM "
			+ "t_driving_license_dtls a " + "WHERE a.Driving_License_No=?";

	private static final String GET_EXPIRY_FOR_LICENSE_DUPLICATE = "SELECT "
		+ "  IF( "
		+ "    a.`Driving_License_Id` IN "
		+ "    (SELECT "
		+ "      `License_Id` "
		+ "    FROM "
		+ "      t_driving_license_renewal_dtls "
		+ "    WHERE `License_Id` = ?), "
		+ "    (SELECT "
		+ "      MAX(Expiry_Date) "
		+ "    FROM "
		+ "      t_driving_license_renewal_dtls "
		+ "    WHERE License_Id = ?), "
		+ "    a.`Expiry_Date` "
		+ "  ) expiryDate "
		+ "FROM "
		+ "  t_driving_license_dtls a "
		+ "WHERE a.`Driving_License_Id` = ?";

	private static final String GET_RENEWAL_PAYMENT = "SELECT "
			+ "a.Amount_Paid, " + "a.Penalty_Paid, "
			+ "DATE_FORMAT(a.Receipt_Date, '%d-%m-%Y') receiptDate, "
			+ "DATE_FORMAT(a.Receipt_Date, '%d/%m/%Y') formatReceiptDate, "
			+ "a.Receipt_No,DATE_FORMAT(a.Created_On, '%d/%m/%Y')Created_On " + "FROM " + "t_payment_dtls a "
			+ "WHERE a.Application_Number = ?";

	private static final String GET_TOP_LISTS = "SELECT "
		+ "  c.`Driving_License_No`, "
		+ "  e.`Vehicle_Number`, "
		+ "  a.`TOP_Number`, "
		+ "  d.`region_name`, "
		+ "  b.`dzongkhag_name`, "
		+ "  a.`Exact_Location`, "
		+ "  DATE_FORMAT( "
		+ "    a.App_Submission_Date, "
		+ "    '%d/%m/%Y' "
		+ "  ) appsubmission, "
		+ "  CONCAT('Nu', '.', a.`Amount`) amount, "
		+ "  a.`Receipt_Number`, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptDate,f.`Personal_Info_Id` "
		+ "FROM "
		+ "  `t_top_application` a "
		+ "  LEFT JOIN t_driving_license_dtls c "
		+ "    ON a.`License_Id` = c.`Driving_License_Id` "
		+ "  LEFT JOIN t_vehicle_registration_dtls e "
		+ "    ON a.`Vehicle_Id` = e.`Vehicle_Reg_Dtls_Id` "
		+ "  LEFT JOIN t_region_master d "
		+ "    ON a.Region_Id = d.Region_Id "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Dzongkhag_Id = b.dzongkhag_Id " 
		+ " 	LEFT JOIN `t_personal_dtls` f ON f.`Customer_Id`=a.`Customer_Id`"
		+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_TOP_REPLACEMENT_DETAILS = "SELECT "
		+ "  c.`Driving_License_No`, "
		+ "  e.`Vehicle_Number`, "
		+ "  a.`TOP_Number`, "
		+ "  d.`region_name`, "
		+ "  b.`dzongkhag_name`, "
		+ "  a.`Exact_Location`, "
		+ "  DATE_FORMAT( "
		+ "    a.App_Submission_Date, "
		+ "    '%d/%m/%Y' "
		+ "  ) appsubmission, "
		+ "  CONCAT('Nu', '.', a.`Amount`) amount, "
		+ "  a.`Receipt_Number`, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') receiptDate, "
		+ "  (SELECT "
		+ "    CONCAT( "
		+ "      `First_Name`, "
		+ "      ' ', "
		+ "      `Middle_Name`, "
		+ "      ' ', "
		+ "      `Last_Name` "
		+ "    ) "
		+ "  FROM "
		+ "    `t_personal_dtls` "
		+ "  WHERE Customer_Id = f.`Customer_Id`) pname "
		+ "FROM "
		+ "  `t_top_application` a "
		+ "  LEFT JOIN t_top_dtls f "
		+ "    ON a.`TOP_Number` = f.`TOP_Number` "
		+ "  LEFT JOIN t_driving_license_dtls c "
		+ "    ON f.`License_Id` = c.`Driving_License_Id` "
		+ "  LEFT JOIN t_vehicle_registration_dtls e "
		+ "    ON f.`Vehicle_Id` = e.`Vehicle_Reg_Dtls_Id` "
		+ "  LEFT JOIN t_region_master d "
		+ "    ON a.Region_Id = d.Region_Id "
		+ "  LEFT JOIN t_dzongkhag_master b "
		+ "    ON a.Dzongkhag_Id = b.dzongkhag_Id "
		+ "WHERE a.`Application_Number` = ?";

	private static final String GET_GENERATED_VEHICLE_NO = "SELECT Vehicle_Number AS generatedNumber FROM t_vehicle_registration_dtls WHERE Application_Number=?";

	private static final String GET_GENERATED_LICENSE_NO = "SELECT `Driving_License_No` AS generatedNumber FROM `t_driving_license_dtls` WHERE Application_Number=?";

	private static final String GET_GENERATED_LEARNER_NO = "SELECT `Learner_License_No` AS generatedNumber FROM `t_learner_license_dtls` WHERE Application_Number=?";

	private static final String GET_GENERATED_TOP_NO = "SELECT `TOP_Number` AS generatedNumber FROM `t_top_dtls` WHERE `Application_Number`=?";
	
	private static final String GET_VEHICLE_APPLICATION_DTLS	="SELECT b.Task_Remark,a.`Vehicle_Model_Id`,"
																+ "  a.`Customer_Id`, "
																+ "  a.`Registration_Type`, "
																+ "  a.`Amount`, "
																+ "  a.`Vehicle_Registration_Id`, "
																+ "  a.`Vehicle_Number`, "
																+ "  a.`Vehicle_Type_Id`, "
																+ "  a.`Vanity_Number`, "
																+ "  a.`Vehicle_Company_Id`, "
																+ "  a.`Engine_Type_Id`, "
																+ "  a.`Engine_Number`, "
																+ "  a.`Chasis_Number`, "
																+ "  a.`Status`, "
																+ "  a.`Color`, "
																+ "  a.`Price`, "
																+ "  a.`Load_Capacity`, "
																+ "  a.`Dealer_Id`, "
																+ "  a.`Seat_Capacity`, "
																+ "  a.`Hypothecated_To_Id`, "
																+ "  a.`Unladen_Weight`, "
																+ "  a.`Letter_Number`, "
																+ "  a.`Letter_Date`, "
																+ "  a.`Manufacture_Year`, "
																+ "  a.`Engine_CC`, "
																+ "  a.`Manufactured_Country_Id`, "
																+ "  a.`Purchase_Type`, "
																+ "  a.`Vehicle_Horse_Power`, "
																+ "  a.`Vehicle_Kilowatt`, "
																+ "  a.`Remarks`, "
																+ "  a.`Vehicle_Registration_Type`, "
																+ "  a.`Purchase_Date`, "
																+ "  DATE_FORMAT(a.`Purchase_Date`, '%d/%m/%Y') purchase_dates,a.`Bus_Type_Id`,c.`Description` "
																+ "FROM "
																+ "  `t_vehicle_application` a "
																+ "  LEFT JOIN `t_task_dtls` b "
																+ "    ON b.`Application_Number` = a.`Application_Number` "
																+" left join `t_vehicle_type_master` c on a.`Vehicle_Type_Id`=c.`Vehicle_Type_Id`"
																+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_VEHICLE_DTLS = "SELECT "
													+ "  a.`Colour`, "
													+ "  a.`Chassis_Number`, "
													+ "  a.`Engine_CC`, "
													+ "  a.`Load_Capacity`, "
													+ "  a.`Seat_Capacity`, "
													+ "  a.`Engine_Number`, "
													+ "  a.`Vehicle_Horse_Power`, "
													+ "  a.`Vehicle_Kilowatt`, "
													+ "  DATE_FORMAT( "
													+ "    a.`Registration_Date`, "
													+ "    '%d/%m/%Y' "
													+ "  ) rgndate, "
													+ "  DATE_FORMAT(a.`Expiry_Date`, '%d/%m/%Y') expdate, "
													+ "  b.`Vehicle_Company_Name`, "
													+ "  c.`Vehicle_Model_Name`, "
													+ "  d.`Engine_Name` "
													+ "FROM "
													+ "  `t_vehicle_registration_dtls` a "
													+ "  LEFT JOIN t_vehicle_company_master b ON a.`Vehicle_Company_Id`=b.`Vehicle_Company_Id` "
													+ "  LEFT JOIN t_vehicle_model_master c ON a.`Vehicle_Model_Id`=c.`Vehicle_Model_Id` "
													+ "  LEFT JOIN t_engine_type_master d ON a.`Engine_Type_Id`=d.`Engine_Type_Id` "
													+ "WHERE a.`Vehicle_Reg_Dtls_Id` = ?";
	
	private static final String GET_REJECTION_REASON	=	"SELECT COUNT(a.`Task_Remark`) AS rowCount,a.`Task_Remark` FROM `t_task_dtls` a WHERE a.`Application_Number`=?";
	
	private static final String GET_ETEST_RESULT	="SELECT "
													+ "  COUNT(*), "
													+ "  a.`Overall_Marks_Obtained`, "
													+ "  a.`Practical_Test_Status` "
													+ " FROM "
													+ "  `t_etest_application` a "
													+ " WHERE a.`Learner_License_No` = ? "
													+ "  AND a.`Application_Number` IN "
													+ "  (SELECT "
													+ "    MAX(b.`Application_Number`) "
													+ "  FROM "
													+ "    `t_etest_application` b "
													+ "  WHERE b.`Learner_License_No` =?)";
	
	
	private static final String GET_PASSENGER_BUS_ROUTE_PERMIT_APPLICATION_DTLS	="SELECT	CID,"+
															"  `Application_Number`,"+
															"  `Applicant_Name`,"+
															" DATE_FORMAT( `DOB`,'%d/%m/%Y') DOB,"+
															"  IF(`Gender`='F','Female','Male')Gender,"+
															"  `Permanent_Dzongkhag`,"+
															"  `Permanenet_Gewog`,"+
															"  `Permanent_Village`,"+
															"  `Phone_No`,"+
															"  `Email_Id`,"+
															"  `Present_Address`,"+
															"  `Route_Type`,"+
															"  (SELECT   `Name` FROM `t_passenger_bus_category_master` " +
															" WHERE `Passenger_Bus_Category_Id` =`Bus_Category_Id`) Bus_Category,"+
															"  `Route_From`,"+
															"  `Route_To`,"+
															"  `Timing`,"+
															"  `Transport_Name`,"+ 
															"  `File_Path`,"+
															"  `File_Name` "+
															"	FROM `t_passenger_bus_route_permit_application`  WHERE Application_Number=?";
	
}
