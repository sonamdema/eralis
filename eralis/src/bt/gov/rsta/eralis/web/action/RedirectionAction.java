package bt.gov.rsta.eralis.web.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import bt.gov.rsta.eralis.business.administration.AdministrationBusiness;
import bt.gov.rsta.eralis.business.etest.ETestBusiness;
import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.report.ReportBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.administration.AdministrationDTO;
import bt.gov.rsta.eralis.dto.administration.RoleDTO;
import bt.gov.rsta.eralis.dto.administration.UserDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.etest.ETestDTO;
import bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.master.MasterDTO;
import bt.gov.rsta.eralis.dto.report.ReportDTO;
import bt.gov.rsta.eralis.dto.token.TokenCounterDTO;
import bt.gov.rsta.eralis.dto.vehicle.FitnessMainDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.business.ServiceBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EmailConfigDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.PagePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;

public class RedirectionAction extends Action
{
	public ActionForward execute(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			Role currentRole = userRolePriv.getCurrentRole();
			String roleId = currentRole.getRoleId();
			String jurisId = userRolePriv.getJurisdictionId();
			String jurisTypeId = userRolePriv.getJurisdictionTypeId();
			String userId = userRolePriv.getUserId();
			String regionId = userRolePriv.getRegionId();
			String regionName = userRolePriv.getRegionName();
			String q = request.getParameter("q");
			String identityNo = request.getParameter("identityNo");
			
			actionForward = q;

			String page_id = request.getParameter("page_id");
			if(identityNo!=null)
				request.setAttribute("identityNo", identityNo);
			else
				request.setAttribute("identityNo", "empty");
			request.setAttribute("page_identifier", q); 
			request.setAttribute("page_id", page_id);
			request.setAttribute("REGION_ID", regionId);
			request.setAttribute("REGION_NAME", regionName);
			request.setAttribute("JURIS_TYPE_ID", jurisTypeId);
			request.setAttribute("JURIS_ID", jurisId);
			request.setAttribute("ROLE_CODE", currentRole.getRoleCode());
			
			/*String totalLicenseAlert = CommonBusiness.getInstance().getTotalLicenseAlert();
			request.setAttribute("TOTAL_LICENSE_ALERT", totalLicenseAlert);
			
			String totalApplicationAlert = CommonBusiness.getInstance().getTotalApplicationAlert();
			request.setAttribute("TOTAL_APPLICATION_ALERT", totalApplicationAlert);*/
			
			if(!(q.equals("account_setting")) && (!q.equals("STATISTIC_DTL_REPORT")))
			{
				PagePriviledge pagePriv = CommonBusiness.getInstance().getPagePriviledge(roleId, page_id);
				request.setAttribute("priviledge", pagePriv);
			}
			
			// if you need to dynamically populate anything from database for 
			// that particular page then write the code below
			if(q.equals("account_setting"))
			{
				UserDTO dto = CommonBusiness.getInstance().getUserDetails(userId);
				request.setAttribute("USER_DETAILS", dto);
			}
			if(q.equals("administration_role"))
			{
				List<RoleDTO> roleList = AdministrationBusiness.getInstance().getRoleList();
				request.setAttribute("roleList", roleList);
			}
			else if(q.equals("administration_user"))
			{
				List<UserDTO> userList = AdministrationBusiness.getInstance().getUserList();
				request.setAttribute("userList", userList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> roleList = CommonBusiness.getInstance().getDropDownList("ROLE_LIST", null);
				request.setAttribute("roleList", roleList);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> baseOfficeList = CommonBusiness.getInstance().getDropDownList("GET_BASE_OFFICE_ALL_LIST", null);
				request.setAttribute("baseOfficeList", baseOfficeList);
				
				List<DropDownDTO> jurisTypeList = CommonBusiness.getInstance().getDropDownList("GET_JURIS_TYPE_LIST", null);
				request.setAttribute("jurisTypeList", jurisTypeList);
			}
			else if(q.equals("online_payment_update"))
			{
				List<AdministrationDTO> applicationList = AdministrationBusiness.getInstance().getPeningPaymentList();
				request.setAttribute("applicationList", applicationList);
			}
			else if(q.equals("report_configuration"))
			{
				List<String> tableList = AdministrationBusiness.getInstance().getTableList();
				request.setAttribute("tableList", tableList);
			}
			else if(q.equals("common_noc_issuance"))
			{
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
			}
			else if(q.equals("vehicle_registration") || q.equals("vehicle_renewal") || q.equals("vehicle_update_print_status") 
			|| q.equals("vehicle_update_information") || q.equals("vehicle_duplication") || q.equals("vehicle_emission_test")
			|| q.equals("vehicle_fitness_test") || q.equals("vehicle_cancellation") || q.equals("vehicle_conversion")
			|| q.equals("vehicle_transfer"))
			
			{
				if(q.equals("vehicle_fitness_test"))
				{
					List<DropDownDTO> userList = CommonBusiness.getInstance().getDropDownList("GET_MVI_USER", userRolePriv.getJurisdictionTypeId()+"~"+userRolePriv.getJurisdictionId());
					request.setAttribute("userList", userList);
				}

				List<DropDownDTO> diplomatList = CommonBusiness.getInstance().getDropDownList("GET_DIPLOMAT_LIST", null);
				request.setAttribute("diplomatList", diplomatList);

				List<DropDownDTO> busTypeList = CommonBusiness.getInstance().getDropDownList("BUS_TYPE_LIST", null);
				request.setAttribute("busTypeList", busTypeList);
				
				List<DropDownDTO> colourList = CommonBusiness.getInstance().getDropDownList("COLOUR_LIST", null);
				request.setAttribute("colourList", colourList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);
				
				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
				request.setAttribute("engineTypeList", engineTypeList);
				
				List<DropDownDTO> dealersNameList = CommonBusiness.getInstance().getDropDownList("DEALERS_NAME_LIST", null);
				request.setAttribute("dealersNameList", dealersNameList);
				
				List<DropDownDTO> manufactureCountryList = CommonBusiness.getInstance().getDropDownList("MANUFACTURE_COUNTRY_LIST", null);
				request.setAttribute("manufactureCountryList", manufactureCountryList);
				
				List<DropDownDTO> ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				List<DropDownDTO> vehicleCancellationList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CANCELLATION_LIST", null);
				request.setAttribute("vehicleCancellationList", vehicleCancellationList);
			
				List<DropDownDTO> vehicleConversionList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CONVERSION_LIST", null);
				request.setAttribute("vehicleConversionList", vehicleConversionList); 
			
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> vehiclePrefixlist = CommonBusiness.getInstance().getDropDownList("VECHILE_PREFIX_LIST", null);
				request.setAttribute("vehiclePrefixlist", vehiclePrefixlist);
				
				List<DropDownDTO> hypothecatedToList = CommonBusiness.getInstance().getDropDownList("HYPOTHECATED_TO_LIST", null);
				request.setAttribute("hypothecatedToList", hypothecatedToList);
				
				List<DropDownDTO> registrationTypeList = CommonBusiness.getInstance().getDropDownList("GET_REGISTRATION_LIST", null);
				request.setAttribute("registrationTypeList", registrationTypeList);
				
				
				
				
				
				if(q.equals("vehicle_emission_test"))
				{
					String coBefore2005 = EralisCommonUtil.getNotificationProperty("vehicle-before-2005-co-emission-level");
					String hsuBefore2005 = EralisCommonUtil.getNotificationProperty("vehicle-before-2005-hsu-emission-level");
					String coAfter2005 = EralisCommonUtil.getNotificationProperty("vehicle-after-2005-co-emission-level");
					String hsuAfter2005 = EralisCommonUtil.getNotificationProperty("vehicle-after-2005-hsu-emission-level");
					
					request.setAttribute("CO_BEFORE_2005", coBefore2005);
					request.setAttribute("HSU_BEFORE_2005", hsuBefore2005);
					request.setAttribute("CO_AFTER_2005", coAfter2005);
					request.setAttribute("HSU_AFTER_2005", hsuAfter2005);
				}
				else if(q.equals("vehicle_fitness_test"))
				{
					List<FitnessMainDTO> fitnessItemList = VehicleBusiness.getInstance().getFitnessItemList();
					request.setAttribute("FITNESS_ITEM_LIST", fitnessItemList);
				}
			}
			else if(q.equals("top_cancellation"))
			{
				List<DropDownDTO> vehicleCancellationList = CommonBusiness.getInstance().getDropDownList("VEHICLE_CANCELLATION_LIST", null);
				request.setAttribute("vehicleCancellationList", vehicleCancellationList);
			}
			else if(q.equals("candidate_list"))
			{
				List<DropDownDTO> testDatelist = ETestBusiness.getInstance().getCandidateTestDateList(jurisId, jurisTypeId);
				request.setAttribute("TEST_DATE_LIST", testDatelist);
			}
			else if(q.equals("common_personal_information"))
			{
				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				List<DropDownDTO> titleOfcourtesyList = CommonBusiness.getInstance().getDropDownList("TITLE_OF_COURTESY_LIST", null);
				request.setAttribute("titleOfcourtesyList", titleOfcourtesyList);
				
				List<DropDownDTO> occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				request.setAttribute("occupationList", occupationList);
				
				List<DropDownDTO> nationalityList = CommonBusiness.getInstance().getDropDownList("NATIONALITY_LIST", null);
				request.setAttribute("nationalityList", nationalityList);
				
				List<DropDownDTO> countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				request.setAttribute("countryList", countryList);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> bloodgroupList = CommonBusiness.getInstance().getDropDownList("BLOOD_GROUP_LIST", null);
				request.setAttribute("bloodgroupList", bloodgroupList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				
			}
			else if(q.equals("common_organization_information"))
			{
				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				List<DropDownDTO> departmentList = CommonBusiness.getInstance().getDropDownList("DEPARTMENT_LIST", null);
				request.setAttribute("departmentList", departmentList);
				
				List<DropDownDTO> diplomatList = CommonBusiness.getInstance().getDropDownList("GET_DIPLOMAT_LIST_FOR_ORGANISTAION", null);
				request.setAttribute("diplomatList", diplomatList);
				
				List<DropDownDTO> privateCompanyList = CommonBusiness.getInstance().getDropDownList("PRIVATE_COMPANY_LIST", null);
				request.setAttribute("privateCompanyList", privateCompanyList);
			}
			else if(q.equals("common_route_permit"))
			{
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> baseofficeList = CommonBusiness.getInstance().getDropDownList("BASE_OFFICE_LIST", null);
				request.setAttribute("baseofficeList", baseofficeList);
			
			}
			
		//deepak	
			else if(q.equals("common_route_permit_renewal"))
			{
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> baseofficeList = CommonBusiness.getInstance().getDropDownList("BASE_OFFICE_LIST", null);
				request.setAttribute("baseofficeList", baseofficeList);
			
			}
			
			
			else if(q.equals("common_vehicle_history"))
			{

				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);
				
				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
				request.setAttribute("engineTypeList", engineTypeList);
				
				List<DropDownDTO> dealersNameList = CommonBusiness.getInstance().getDropDownList("DEALERS_NAME_LIST", null);
				request.setAttribute("dealersNameList", dealersNameList);
				
				List<DropDownDTO> manufactureCountryList = CommonBusiness.getInstance().getDropDownList("MANUFACTURE_COUNTRY_LIST", null);
				request.setAttribute("manufactureCountryList", manufactureCountryList);
				
				List<DropDownDTO> registrationTypeList = CommonBusiness.getInstance().getDropDownList("GET_REGISTRATION_LIST", null);
				request.setAttribute("registrationTypeList", registrationTypeList);
				
				List<DropDownDTO> modelList = CommonBusiness.getInstance().getDropDownList("VECHILE_MODEL_LIST",null);
				request.setAttribute("modelList", modelList);
				
				VehicleDTO dto = VehicleBusiness.getInstance().get_vehicle_details("0", "0");
				request.setAttribute("VEHICLE_DETAILS", dto);
				
				List<DropDownDTO> titleOfcourtesyList = CommonBusiness.getInstance().getDropDownList("TITLE_OF_COURTESY_LIST", null);
				request.setAttribute("titleOfcourtesyList", titleOfcourtesyList);
				
				List<DropDownDTO> occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				request.setAttribute("personalOccupationList", occupationList);
				
				List<DropDownDTO> nationalityList = CommonBusiness.getInstance().getDropDownList("NATIONALITY_LIST", null);
				request.setAttribute("personalNationalityList", nationalityList);
				
				List<DropDownDTO> countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				request.setAttribute("countryList", countryList);
				
				List<DropDownDTO> bloodgroupList = CommonBusiness.getInstance().getDropDownList("BLOOD_GROUP_LIST", null);
				request.setAttribute("bloodgroupList", bloodgroupList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> accidentCauseList = CommonBusiness.getInstance().getDropDownList("ACCIDENT_CAUSE_LIST", null);
				request.setAttribute("accidentCauseList", accidentCauseList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
			}
			else if(q.equals("common_accident_entry"))
			{
				List<DropDownDTO> accident_cause_DE = CommonBusiness.getInstance().getDropDownList("ACCIDENT_CAUSE_MASTER", "DE");
				request.setAttribute("ACCIDENT_CAUSE_DE", accident_cause_DE);

				List<DropDownDTO> accident_cause_RC = CommonBusiness.getInstance().getDropDownList("ACCIDENT_CAUSE_MASTER", "RC");
				request.setAttribute("ACCIDENT_CAUSE_RC", accident_cause_RC);

				List<DropDownDTO> accident_cause_WC = CommonBusiness.getInstance().getDropDownList("ACCIDENT_CAUSE_MASTER", "WC");
				request.setAttribute("ACCIDENT_CAUSE_WC", accident_cause_WC);

				List<DropDownDTO> accident_cause_MF = CommonBusiness.getInstance().getDropDownList("ACCIDENT_CAUSE_MASTER", "MF");
				request.setAttribute("ACCIDENT_CAUSE_MF", accident_cause_MF);

				List<DropDownDTO> ownerTypeList = CommonBusiness.getInstance().getDropDownList("OWNER_TYPE_LIST", null);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> accidentCauseList = CommonBusiness.getInstance().getDropDownList("ACCIDENT_CAUSE_LIST", null);
				request.setAttribute("accidentCauseList", accidentCauseList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> policeDivisionList = CommonBusiness.getInstance().getDropDownList("POLICE_DIVISION_LIST", null);
				request.setAttribute("policeDivisionList", policeDivisionList);

				List<DropDownDTO> modelList = CommonBusiness.getInstance().getDropDownList("MODEL_LIST_ALL", null);
				request.setAttribute("modelList", modelList);
				
				List<DropDownDTO> registrationTypeList = CommonBusiness.getInstance().getDropDownList("GET_REGISTRATION_LIST", null);
				request.setAttribute("registrationTypeList", registrationTypeList);
				
				List<DropDownDTO> vehicleCodeList = CommonBusiness.getInstance().getDropDownList("VECHILE_CODE_LIST", null);
				request.setAttribute("vehicleCodeList", vehicleCodeList);

				List<DropDownDTO> countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				request.setAttribute("countryList", countryList);

				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				

				List<DropDownDTO> occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				request.setAttribute("occupationList", occupationList);
				

				List<DropDownDTO> informerList = CommonBusiness.getInstance().getDropDownList("GET_ACCIDENT_INFORMER_LIST", null);
				request.setAttribute("informerList", informerList);
				
			}
			else if (q.equals("license_offence")|| q.equals("license_non_commerical")|| q.equals("license_commerical")|| q.equals("common_license_history")
					|| q.equals("license_duplication_learner")|| q.equals("license_license_offence") || q.equals("license_renewal")
					|| q.equals("license_duplication")|| q.equals("license_endorsement")|| q.equals("license_cancellation") || q.equals("license_license_suspension")
					|| q.equals("license_drive_type_suspension"))
			{

				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				List<DropDownDTO> titleOfcourtesyList = CommonBusiness.getInstance().getDropDownList("TITLE_OF_COURTESY_LIST", null);
				request.setAttribute("titleOfcourtesyList", titleOfcourtesyList);
				
				List<DropDownDTO> occupationList = CommonBusiness.getInstance().getDropDownList("OCCUPATION_LIST", null);
				request.setAttribute("personalOccupationList", occupationList);
				
				List<DropDownDTO> nationalityList = CommonBusiness.getInstance().getDropDownList("NATIONALITY_LIST", null);
				request.setAttribute("personalNationalityList", nationalityList);
				
				List<DropDownDTO> countryList = CommonBusiness.getInstance().getDropDownList("COUNTRY_LIST", null);
				request.setAttribute("countryList", countryList);
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				
				List<DropDownDTO> bloodgroupList = CommonBusiness.getInstance().getDropDownList("BLOOD_GROUP_LIST", null);
				request.setAttribute("bloodgroupList", bloodgroupList);
				
				if (q.equals("license_offence"))
				{
					List<DropDownDTO> OffenceList = CommonBusiness.getInstance().getDropDownList("OFFENCE_LIST", null);
					request.setAttribute("OffenceList", OffenceList);
					
					String repeatedOffenceTypeId = CommonBusiness.getInstance().getRepeatedOffenceType();
					request.setAttribute("repeatedOffenceTypeId", repeatedOffenceTypeId);
					
				}
				
				
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("ORDINARY_DRIVE_TYPE", null);
				request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
				
				List<DropDownDTO> commercialDriveTypeList = CommonBusiness.getInstance().getDropDownList("COMMERCIAL_DRIVE_TYPE", null);
				request.setAttribute("COMMERCIAL_DRIVE_TYPE_LIST", commercialDriveTypeList);
				
				request.setAttribute("DRIVE_TYPE_LIST_SIZE", driveTypeList.size());
			}
			else if (q.equals("license_new") || q.equals("license_renewal_learner"))
			{
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("ORDINARY_DRIVE_TYPE", null);
				request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
				
				request.setAttribute("DRIVE_TYPE_LIST_SIZE", driveTypeList.size());
			}
			else if(q.equals("common_charge_calculator"))
			{
				List<DropDownDTO> vehicleTypeList = CommonBusiness.getInstance().getDropDownList("VEHICLE_TYPE_LIST", null);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				List<DropDownDTO> engineTypeList = CommonBusiness.getInstance().getDropDownList("ENGINE_TYPE_LIST", null);
				request.setAttribute("engineTypeList", engineTypeList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
			}
			/**** MASTER TABLE :: START ****/
			else if(q.equals("master_region"))
			{
				List<MasterDTO> regionList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("regionList", regionList);
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Region Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_dzongkhag"))
			{
				List<MasterDTO> dzongkhagList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("dzongkhagList", dzongkhagList);
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Dzongkhag Master");
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				actionForward = "master_management";
			}
			else if(q.equals("master_gewog"))
			{
				List<MasterDTO> gewogList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("gewogList", gewogList);
				request.setAttribute("HEADER", "Gewog Master");
				
				List<DropDownDTO> dzongkhagList = CommonBusiness.getInstance().getDropDownList("DZONGKHAG_LIST_ALL", null);
				request.setAttribute("dzongkhagList", dzongkhagList);
				request.setAttribute("TYPE", q);
				
				actionForward = "master_management";
			}
			else if(q.equals("master_ministry"))
			{
				List<MasterDTO> ministryList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("ministryList", ministryList);
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Ministry Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_department"))
			{
				List<MasterDTO> departmentList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("departmentList", departmentList);
				
				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Department Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_country"))
			{
				List<MasterDTO> countryList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("countryList", countryList);
				
				List<DropDownDTO> ministryList = CommonBusiness.getInstance().getDropDownList("MINISTRY_LIST", null);
				request.setAttribute("ministryList", ministryList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Country Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_offence"))
			{
				List<MasterDTO> offenceList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("offenceList", offenceList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Offence Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_accident"))
			{
				List<MasterDTO> accidentList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("accidentList", accidentList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Accident Cause Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_action"))
			{
				List<MasterDTO> actionList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("actionList", actionList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Action Taken Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_designation"))
			{
				List<MasterDTO> designationList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("designationList", designationList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Designation Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_drive"))
			{
				List<MasterDTO> driveTypeList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("driveTypeList", driveTypeList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Drive Type Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_engine"))
			{
				List<MasterDTO> engineTypeList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("engineTypeList", engineTypeList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Engine Type Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_hypo"))
			{
				List<MasterDTO> hypothecatedList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("hypothecatedList", hypothecatedList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Hypothecated Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_occupation"))
			{
				List<MasterDTO> occupationList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("occupationList", occupationList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Occupation Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_owner"))
			{
				List<MasterDTO> ownerTypeList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("ownerTypeList", ownerTypeList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Occupation Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_courtesy"))
			{
				List<MasterDTO> courtesyList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("courtesyList", courtesyList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Title of Courtesy Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_company"))
			{
				List<MasterDTO> vehicleCompanyList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Vehicle Company Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_model"))
			{
				List<MasterDTO> vehicleModelList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("vehicleModelList", vehicleModelList);
				
				List<DropDownDTO> vehicleCompanyList = CommonBusiness.getInstance().getDropDownList("VECHILE_COMPANY_LIST", null);
				request.setAttribute("vehicleCompanyList", vehicleCompanyList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Vehicle Model Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_vehicle_type"))
			{
				List<MasterDTO> vehicleTypeList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("vehicleTypeList", vehicleTypeList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Vehicle Type Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_registration_code"))
			{
				List<MasterDTO> registrationCodeList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("registrationCodeList", registrationCodeList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Vehicle Registration Code Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_dealer"))
			{
				List<MasterDTO> dealerList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("dealerList", dealerList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Dealer Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_cancellation_reasons"))
			{
				List<MasterDTO> reasonsList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("reasonsList", reasonsList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Cancellation Reason Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_base_office"))
			{
				List<MasterDTO> baseOfficeList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("baseOfficeList", baseOfficeList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Base Office Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_blood_group"))
			{
				List<MasterDTO> bloodGroupList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("bloodGroupList", bloodGroupList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Blood Group Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_practical_test_criteria"))
			{
				List<MasterDTO> practicalCriteriaList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("practicalCriteriaList", practicalCriteriaList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Practical Test Criteria Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_village"))
			{
				List<MasterDTO> villageList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("villageList", villageList);
				
				List<DropDownDTO> gewogList = CommonBusiness.getInstance().getDropDownList("GEWOG_LIST_ALL", null);
				request.setAttribute("gewogList", gewogList);
				
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Village Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("email_configuration"))
			{
				EmailConfigDTO emailConfigDetails = CommonBusiness.getInstance().getEmailConfigDetails();
				request.setAttribute("EMAIL_CONFIG", emailConfigDetails);
			}
			else if(q.equals("master_colour"))
			{
				List<MasterDTO> colourList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("colourList", colourList);
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Colour Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_diplomats"))
			{
				List<MasterDTO> diplomatList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("diplomatList", diplomatList);
				List<DropDownDTO> organisationList = CommonBusiness.getInstance().getDropDownList("ORGANISATION_LIST", null);
				request.setAttribute("organisationList", organisationList);
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Diplomat Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_private_company"))
			{
				List<MasterDTO> privateCompnayMaster = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("privateCompnayMaster", privateCompnayMaster);
				/*List<DropDownDTO> organisationList = CommonBusiness.getInstance().getDropDownList("ORGANISATION_LIST", null);
				request.setAttribute("organisationList", organisationList);*/
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Private Company Master");
				
				actionForward = "master_management";
			}
			else if(q.equals("master_route"))
			{
				List<MasterDTO> routeMasterList = CommonBusiness.getInstance().getMasterTableList(q);
				request.setAttribute("routeMasterList", routeMasterList);
				/*List<DropDownDTO> organisationList = CommonBusiness.getInstance().getDropDownList("ORGANISATION_LIST", null);
				request.setAttribute("organisationList", organisationList);*/
				request.setAttribute("TYPE", q);
				request.setAttribute("HEADER", "Route Master");
				
				actionForward = "master_management";
			}
			
			/**** MASTER TABLE :: END ****/
			
			/**** E-Test :: START ****/
			else if(q.equals("etest_configuration"))
			{
				List<ETestDTO> testDtlsList = ETestBusiness.getInstance().getETestConfigDtls(jurisId, jurisTypeId);
				request.setAttribute("etestDtls", testDtlsList);
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
			}
			else if(q.equals("etest_question_answer_management"))
			{
				List<DropDownDTO> questionList = CommonBusiness.getInstance().getDropDownList("ETEST_QUESTION_LIST", null);
				request.setAttribute("questionList", questionList);
			}
			else if(q.equals("practical_test_management"))
			{
				ETestDTO testDtls = ETestBusiness.getInstance().getTestConfigDetails(jurisId, jurisTypeId);
				request.setAttribute("TEST_DETAILS", testDtls);
				
				List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("ORDINARY_DRIVE_TYPE", null);
				request.setAttribute("DRIVE_TYPE_LIST", driveTypeList);
				
				List<PracticalCriteriaDTO> practicalCriteriaList = ETestBusiness.getInstance().getPracticalCriteriaList();
				request.setAttribute("PRACTICAL_CRITERIA_LIST", practicalCriteriaList);
			}
			/**** E-Test :: END ****/
			
			/*******TOOLS*******/
			else if (q.equals("tools_replace_CID"))
			{
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
			}
			else if (q.equals("tools_replace_IID"))
			{
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
			}
			else if (q.equals("tools_delete_drive_type"))
			{
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
			}else if (q.equals("tools_road_block_entry"))
			{
				List<DropDownDTO> routeList = CommonBusiness.getInstance().getDropDownList("ROUTE_LIST", null);
				request.setAttribute("routeList", routeList);
			}
			else if (q.equals("tools_license_punch"))
			{
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
			}
			else if (q.equals("accounting_configuration"))
			{
				List<EralisCommonDTO> vehicleConstantDtls = AdministrationBusiness.getInstance().getVehicleConstant();
				request.setAttribute("VEHICLE_CONSTANT_DTLS", vehicleConstantDtls);
				List<EralisCommonDTO> licenseConstantDtls = AdministrationBusiness.getInstance().getLicenseConstant();
				request.setAttribute("LICENSE_CONSTANT_DTLS", licenseConstantDtls);
				List<EralisCommonDTO> getPenaltyConstant = AdministrationBusiness.getInstance().getPenaltyConstant();
				request.setAttribute("PENALTY_CONSTANT_DTLS", getPenaltyConstant);
			}
			else if(q.equals("license_approve_reprint"))
			{
				List<LicenseDTO> reprintList = LicenseBusiness.getInstance().getReprintList(jurisId, jurisTypeId);
				request.setAttribute("REPRINT_LIST", reprintList);
			}
			else if(q.equals("STATISTIC_DTL_REPORT"))
			{
				List<EralisCommonDTO> statisticDtlReport = ReportBusiness.getInstance().getStatisticDtlReport(page_id,jurisId, jurisTypeId);
				request.setAttribute("STATISTIC_DTL_REPORT", statisticDtlReport);
				request.setAttribute("REPORT_TYPE", page_id);
				actionForward = "statistic_dtl_report";
			}
			else if(q.equals("book_etest"))
			{
				List<EralisCommonDTO> statisticDtlReport = ReportBusiness.getInstance().getStatisticDtlReport(page_id,jurisId, jurisTypeId);
				request.setAttribute("STATISTIC_DTL_REPORT", statisticDtlReport);
				request.setAttribute("REPORT_TYPE", page_id);
				actionForward = "statistic_dtl_report";
			}
			else if(q.equals("etest_candidate_list"))
			{
				
				List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
				request.setAttribute("regionList", regionList);
				actionForward = q;
			}
			else if(q.equals("passengerBusInspection"))
			{
				
				List<DropDownDTO> routeList = CommonBusiness.getInstance().getDropDownList("ALL_ROUTE_LIST", null);
				request.setAttribute("routeList", routeList);
				actionForward = q;
			}
			else if(q.equals("token_counter"))
			{
				List<TokenCounterDTO> tokenList = CommonBusiness.getInstance().getCounterList(userRolePriv.getJurisdictionTypeId(),userRolePriv.getJurisdictionId(),userRolePriv.getUserId());
				request.setAttribute("tokenList", tokenList);
			 
				actionForward = q; 
			}
		} 
		catch (Exception e)
		{e.printStackTrace();
			Log.error("Error RedirectionAction[execute]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
}
