package bt.gov.rsta.eralis.web.action.eralis_common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import bt.gov.rsta.eralis.business.eralis_common.EralisCommonBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.noc.NOCDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.eralis.web.actionform.eralis_common.EralisCommonActionForm;
import bt.gov.rsta.eralis.web.actionform.vehicle.VehicleActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.UserDetailsVO;

public class EralisCommonAction extends DispatchAction
{
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null, regionId = null, baseId = null;
	
	public ActionForward noc_issuance(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			String result = EralisCommonBusiness.getInstance().noc_issuance(dto, userRolePriv.getJurisdictionId(), userRolePriv.getRegionId(), userRolePriv.getBaseId());
			
			if(null != result){
				NOCDTO nocDTO = EralisCommonBusiness.getInstance().get_noc_dtls(result);
				request.setAttribute("NOC_DETAILS", nocDTO);
				actionForward = "noc_letter";
			}
			else {
				request.setAttribute("MESSAGE", "NOC_FAILURE");
				actionForward = "message";
			}
		}
		catch (Exception e) 
		{
			Log.error("Error EralisCommonAction[noc_issuance]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward personal_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String userType = request.getParameter("userType");
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			if(userType!=null)
			{
				if(userType.equalsIgnoreCase("CITIZEN"))
				{
					this.setCitizenUserValues(request);
				}
			}
			else
			{
				this.setUserValues(request);
			}
			//this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = EralisCommonBusiness.getInstance().personal_info(dto, userVo);
			
			String[] tempArray = result.split("#");
			String responseStr = tempArray[0];
			//String customerId = tempArray[1];
			
			request.setAttribute("MESSAGE", responseStr);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.error("Error EralisCommonAction[personal_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_personal_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			dto.setManualFlag("N");
			String result = EralisCommonBusiness.getInstance().edit_personal_info(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.error("Error EralisCommonAction[personal_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward edit_receipt_dtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo	=	request.getParameter("applicationNo");
			String serviceType	=	request.getParameter("serviceType");

			dto.setApplicationNo(applicationNo);
			dto.setServiceType(serviceType);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			dto.setManualFlag("N");
			
			String result = EralisCommonBusiness.getInstance().edit_receipt_dtls(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error EralisCommonAction[personal_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward delete_personal_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = EralisCommonBusiness.getInstance().delete_personal_info(dto, userVo);
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error EralisCommonAction[personal_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward organization_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = EralisCommonBusiness.getInstance().organization_info(dto,userVo);
			String[] tempArray = result.split("#");
			String responseStr = tempArray[0];
			String customerCode = tempArray[1];
			request.setAttribute("MESSAGE", responseStr);
			request.setAttribute("CUSTOMER_CODE", customerCode);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error EralisCommonAction[organizational_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward delete_organization_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String result = EralisCommonBusiness.getInstance().delete_organization_info(dto);
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error EralisCommonAction[organizational_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_organization_info(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
			EralisCommonDTO dto=new EralisCommonDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = EralisCommonBusiness.getInstance().edit_organization_info(dto,userVo);
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error EralisCommonAction[organizational_info]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
		
		public ActionForward new_accident_entry(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
		{
			String actionForward = null;
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
			userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
			
			try 
			{
				EralisCommonActionForm commonForm = (EralisCommonActionForm) form;
				  
				this.setUserValues(request);
				UserDetailsVO userVo = new UserDetailsVO();
				this.copyUserValues(userVo);
				
				String result = EralisCommonBusiness.getInstance().new_accident_entry(commonForm, userVo);
				request.setAttribute("MESSAGE","ACCIDENT_ENTRY_SUCCESS");
				request.setAttribute("CASE_NO",result);
				actionForward = "message";
				
			}
			catch (Exception e) 
			{
				Log.error("Error EralisCommonAction[accident_info]: "+e);
				String owner = "";
				if(null != userRolePriv.getUserId()){
					owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
				}
				String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
				request.setAttribute("errorCode", messagekey);
				actionForward = "GLOBAL_REDIRECT_ERROR";
			}
			
			return mapping.findForward(actionForward);
		}
	
		public ActionForward license_history(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
		{
			String actionForward = null;
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
			userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
			
			try 
			{
				EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
				EralisCommonDTO dto=new EralisCommonDTO();
				
				BeanUtils.copyProperties(dto, licenseForm);
				
				//String result = EralisCommonBusiness.getInstance().personal_info(dto);
			}
			catch (Exception e) 
			{
				Log.error("Error EralisCommonAction[license_history]: "+e);
				String owner = "";
				if(null != userRolePriv.getUserId()){
					owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
				}
				String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
				request.setAttribute("errorCode", messagekey);
				actionForward = "GLOBAL_REDIRECT_ERROR";
			}
			
			return mapping.findForward(actionForward);
		}
		public ActionForward vehicle_history(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
		{
			String actionForward = null;
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
			userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
			
			try 
			{
				EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
				EralisCommonDTO dto=new EralisCommonDTO();
				
				BeanUtils.copyProperties(dto, licenseForm);
				
			//	String result = EralisCommonBusiness.getInstance().personal_info(dto);
			}
			catch (Exception e) 
			{
				Log.error("Error EralisCommonAction[vehicle_history]: "+e);
				String owner = "";
				if(null != userRolePriv.getUserId()){
					owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
				}
				String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
				request.setAttribute("errorCode", messagekey);
				actionForward = "GLOBAL_REDIRECT_ERROR";
			}
			
			return mapping.findForward(actionForward);
		}
	
		public ActionForward route_permit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
		{
			String actionForward = null;
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
			userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
			
			try 
			{
				EralisCommonActionForm licenseForm = (EralisCommonActionForm) form;
				EralisCommonDTO dto=new EralisCommonDTO();
				BeanUtils.copyProperties(dto, licenseForm);

				//String dateOfissue = request.getParameter("RenewalUpto");
				//String validity = request.getParameter("validity");

				//dto.setValidUpto(validity);
				//dto.setDateOfissue(dateOfissue);
				this.setUserValues(request);
				UserDetailsVO userVo = new UserDetailsVO();
				this.copyUserValues(userVo);
				
				String result = EralisCommonBusiness.getInstance().route_permit(dto, userVo);
				
				String[] resultArray = result.split("#");
				 
				request.setAttribute("PERMIT_ID", resultArray[0]);
				request.setAttribute("MESSAGE", resultArray[1]);
				
				actionForward = "permit_message";
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.error("Error EralisCommonAction[route_permit]: "+e);
				String owner = "";
				if(null != userRolePriv.getUserId()){
					owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
				}
				String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
				request.setAttribute("errorCode", messagekey);
				actionForward = "GLOBAL_REDIRECT_ERROR";
			}
			
			return mapping.findForward(actionForward);
		}
		
		public ActionForward print_permit_report(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
		{
			String actionForward = null;
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
			userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
			try
			{
				String permitId	=	request.getParameter("permitId");
				EralisCommonDTO dto = new EralisCommonDTO();
				dto = EralisCommonBusiness.getInstance().get_permit_dtls(permitId);
				request.setAttribute("PERMIT_DETAILS", dto);
				actionForward = "route_permit_issuance";
			}
			catch (Exception e) 
			{
				Log.error("Error EralisCommonAction[print_permit_report]: "+e);
				String owner = "";
				if(null != userRolePriv.getUserId()){
					owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
				}
				String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
				request.setAttribute("errorCode", messagekey);
				actionForward = "GLOBAL_REDIRECT_ERROR";
			}
			
			return mapping.findForward(actionForward);
		}
	
	
		/**
		 *  Set values of user related information
		 */
		private void setUserValues(HttpServletRequest request)
		{
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge)session.getAttribute(Constants.USER_DETAILS);
			this.userId = userRolePriv.getUserId();
			this.userName = userRolePriv.getUserName();
			Role currentRole = userRolePriv.getCurrentRole();
			this.roleId = currentRole.getRoleId();
			this.roleName = currentRole.getRoleName();
			this.roleCode = currentRole.getRoleCode();
			this.regionId = userRolePriv.getRegionId();
			this.baseId = userRolePriv.getBaseId();
		}
		
		/*
		 * Copy user values to data transfer object
		 */
		private void copyUserValues(UserDetailsVO userVo)
		{
			userVo.setActorId(userId);
			userVo.setActorName(userName);
			userVo.setAssignedGroupId(assignedGroupId);
			userVo.setAssignedUserId(userId);
			userVo.setRoleId(roleId);
			userVo.setRoleName(roleName);
			userVo.setAssignedPrivId(assignedPrivId);
			userVo.setRoleCode(roleCode);
			userVo.setRegionId(regionId);
			userVo.setBaseId(baseId);
		}
		private void setCitizenUserValues(HttpServletRequest request)
		{ 
			this.userId = "Citizen";
			this.userName = "Citizen";
			this.roleId = "0";
			this.roleName = "Citizen";
			this.roleCode = "Citizen"; 
		}

}
