package bt.gov.rsta.eralis.web.action.tools;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import bt.gov.rsta.eralis.business.tools.ToolsBusiness;
import bt.gov.rsta.eralis.dto.tools.ToolsDTO;
import bt.gov.rsta.eralis.web.actionform.tools.ToolsActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.UserDetailsVO;


public class ToolsAction extends DispatchAction 
  {
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null,jurisId = null, jurisTypeId = null, regionId = null, baseId = null;
	
	public ActionForward replace_CID(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward 	= null;
		HttpSession session 	= request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv 			= (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{	
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().replace_CID(dto);
			request.setAttribute("duplicateCID", dto.getNewCID());
			request.setAttribute("formType", "replaceCID");
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[replace_CID]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	public ActionForward replace_IID(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String customerID	=	null;
		try 
		{
			customerID	=	request.getParameter("customerID");
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO(); 
			dto.setCustomerId(customerID);
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().replace_IID(dto);
			request.setAttribute("result", result); 
			
			request.setAttribute("formType", "replaceIID");
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[replace_IID]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	public ActionForward delete_Drive_Type(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String driveTypeId = request.getParameter("driveTypeId");
			String licenseNumber = request.getParameter("licenseNumber");
			String customerId = request.getParameter("customerId");
			
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			dto.setDriveTypeId(driveTypeId);
			dto.setLicenseNumber(licenseNumber);
			dto.setCustomerId(customerId);
			
			String result = ToolsBusiness.getInstance().delete_Drive_Type(dto);
			 
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[delete_Drive_Type]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	public ActionForward change_Driving_License_Status(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().change_Driving_License_Status(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[change_Driving_License_Status]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	public ActionForward vehicle_Purchase(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().vehicle_Purchase(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[vehicle_Purchase]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}

	public ActionForward road_Block_Entry(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			BeanUtils.copyProperties(dto, toolsForm);
			String routeFrom	=	request.getParameter("routeFrom");
			String routeTo	=	request.getParameter("routeTo");
			dto.setRouteFrom(routeFrom);
			dto.setRouteTo(routeTo);
			String result = ToolsBusiness.getInstance().road_Block_Entry(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[road_Block_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	public ActionForward edit_Road_Block_Entry(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO(); 
			dto.setRouteFrom(request.getParameter("routeFrom"));
			dto.setRouteTo(request.getParameter("routeTo"));
			BeanUtils.copyProperties(dto, toolsForm);
			String result = ToolsBusiness.getInstance().edit_Road_Block_Entry(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[road_Block_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	public ActionForward delete_Road_Block_Entry(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO(); 
			dto.setRow_id(request.getParameter("row_id")); 
			BeanUtils.copyProperties(dto, toolsForm);
			String result = ToolsBusiness.getInstance().delete_Road_Block_Entry(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[delete_Road_Block_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	
	public ActionForward hypothecated_To(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().hypothecated_To(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[hypothecated_To]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
	}
	
	public ActionForward license_Punch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			BeanUtils.copyProperties(dto, toolsForm);
			String result = ToolsBusiness.getInstance().license_Punch(dto, userVo);
		}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[license_Punch]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}

	public ActionForward save_pending_task(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			BeanUtils.copyProperties(dto, toolsForm);
			String result = ToolsBusiness.getInstance().save_pending_task(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[license_Punch]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	public ActionForward delete_License_Punch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String punchId	=	request.getParameter("punchId");
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			dto.setPunchId(punchId);
			BeanUtils.copyProperties(dto, toolsForm);
			String result = ToolsBusiness.getInstance().delete_License_Punch(dto);
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[license_Punch]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
	}
	public ActionForward withdraw_License_Punch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String punchId	=	request.getParameter("punchId");
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			dto.setPunchId(punchId);
			BeanUtils.copyProperties(dto, toolsForm);

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = ToolsBusiness.getInstance().withdraw_License_Punch(dto, userVo);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[license_Punch]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	public ActionForward license_Print(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().license_Print(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[license_Print]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	
	public ActionForward refresh_Course_Entry(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ToolsActionForm toolsForm = (ToolsActionForm) form;
			ToolsDTO dto = new ToolsDTO();
			
			BeanUtils.copyProperties(dto, toolsForm);
			
			String result = ToolsBusiness.getInstance().refresh_Course_Entry(dto);
			
			}
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[refresh_Course_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
		
		}
	public ActionForward getLicenseHolderList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		ToolsDTO dto = new ToolsDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String cidNumber 	= request.getParameter("cid");
			String licenseNo 	= request.getParameter("licenseNo");
			
			dto.setCitizenID(cidNumber);
			dto.setLicenseNumber(licenseNo);
			
		  	List<ToolsDTO> licenseHolder = ToolsBusiness.getInstance().license_holder_list(dto);
			request.setAttribute("LICENSE_HOLDER_LIST", licenseHolder);
			
			actionForward = "tools_license_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[getLicenseHolderList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward getEndorsementLicenseDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		ToolsDTO dto = new ToolsDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String customerId 	= request.getParameter("customerId"); 
			dto.setCustomerId(customerId);  
		  	List<ToolsDTO> endorseIIDList = ToolsBusiness.getInstance().getEndorsementLicenseDetails(dto);
			request.setAttribute("LICENSE_HOLDER_LIST", endorseIIDList);
			 
			actionForward = "endorsement_IID_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[refresh_Course_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward getRoadBlockList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		ToolsDTO dto = new ToolsDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{ 
			String routeTo 	= request.getParameter("routeTo"); 
			String routeFrom 	= request.getParameter("routeFrom"); 
			   
			dto.setRouteFrom(routeFrom);
			dto.setRouteTo(routeTo);
			
			
		  	List<ToolsDTO> roadBlockList = ToolsBusiness.getInstance().getRoadBlockList(dto);
			request.setAttribute("ROAD_BLOCK_LIST", roadBlockList);

			 
			actionForward = "road_block";
		} 
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[getRoadBlockList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward getDriveType(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		ToolsDTO dto = new ToolsDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{ 
			String customerId 	= request.getParameter("customerId"); 
			 
			dto.setCustomerId(customerId);  
			
		  	List<ToolsDTO> driveType = ToolsBusiness.getInstance().getDriveType(dto);
			request.setAttribute("DRIVE_TYPE", driveType);

			request.setAttribute("DRIVE_TYPE_LIST_SIZE", driveType.size());
			 
			actionForward = "drive_type";
		} 
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[refresh_Course_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward licensePunchList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		ToolsDTO dto = new ToolsDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{  
			String customerId 	= request.getParameter("customerId"); 
			dto.setCustomerId(customerId);  
			
		  	List<ToolsDTO> licensePunchList = ToolsBusiness.getInstance().licensePunchList(dto);
			request.setAttribute("LICENSE_PUNCH_LIST", licensePunchList);
			actionForward = "license_punch_list";
		} 
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[refresh_Course_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	public ActionForward licensePunchDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{	
		ToolsDTO dto = new ToolsDTO();
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{  
			String customerId 	= request.getParameter("customerId"); 
			 
			dto.setCustomerId(customerId);  
			
		  	List<ToolsDTO> licensePunchDetails = ToolsBusiness.getInstance().licensePunchDetails(dto);
			request.setAttribute("LICENSE_PUNCH_DETAIL", licensePunchDetails);

			request.setAttribute("LICENSE_PUNCH_DETAIL_SIZE", licensePunchDetails.size());
			 
			actionForward = "license_punch_details";
		} 
		catch (Exception e) 
		{
			Log.error("Error ToolsAction[refresh_Course_Entry]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	/**
	 *  Set values of user related information
	 */
	private void setUserValues(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge)session.getAttribute(Constants.USER_DETAILS);
		this.userId = userRolePriv.getUserId();
		this.userName = userRolePriv.getUserId();
		Role currentRole = userRolePriv.getCurrentRole();
		this.roleId = currentRole.getRoleId();
		this.roleName = currentRole.getRoleName();
		this.roleCode = currentRole.getRoleCode();
		this.jurisId = userRolePriv.getJurisdictionId();
		this.jurisTypeId = userRolePriv.getJurisdictionTypeId();
		this.regionId = userRolePriv.getRegionId();
		this.baseId = userRolePriv.getBaseId();
	}
	
	/*
	 * Copy user values to data transfer object
	 */
	private void copyUserValues(UserDetailsVO userVo)
	{
		userVo.setActorId(userId);
		userVo.setActorName(userName);
		userVo.setAssignedGroupId(assignedGroupId);
		userVo.setAssignedUserId(userId);
		userVo.setRoleId(roleId);
		userVo.setRoleName(roleName);
		userVo.setAssignedPrivId(assignedPrivId);
		userVo.setRoleCode(roleCode);
		userVo.setJurisdictionId(jurisId);
		userVo.setJurisdictionTypeId(jurisTypeId);
		userVo.setRegionId(regionId);
		userVo.setBaseId(baseId);
	}
	
	
  }
