package bt.gov.rsta.eralis.web.action.payment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import bt.gov.rsta.eralis.business.payment.PaymentBusiness;

public class PaymentAction extends DispatchAction{
	
	public ActionForward onlinePayment(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String application_no	=	request.getParameter("application_no");
		String service_name		=	request.getParameter("service_name");
		String amount	=	request.getParameter("amount");
		
		String bfsOrderNo	=	PaymentBusiness.getInstance().processPaymentDTLS(application_no,service_name,amount);
		request.setAttribute("BFS_ORDER_NO", bfsOrderNo);
		request.setAttribute("APPLICATION_NO", application_no);
		return mapping.findForward("online_payment_receipt");
	}
}