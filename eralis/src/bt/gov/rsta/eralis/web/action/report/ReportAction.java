package bt.gov.rsta.eralis.web.action.report;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sun.xml.internal.fastinfoset.util.StringArray;

import bt.gov.rsta.eralis.business.administration.AdministrationBusiness;
import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.report.ReportBusiness;
import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.report.ReportDTO;
import bt.gov.rsta.eralis.dto.report.ReportParameterDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.web.actionform.service.serviceActionForm;

public class ReportAction extends DispatchAction 
{
	public ActionForward reportRedirect(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String q = request.getParameter("q");
			if(q.equalsIgnoreCase("traffic_report"))
			{
				List<DropDownDTO> reportTitle = CommonBusiness.getInstance().getDropDownList("GET_REPORT_TITLE_FOR_TRAFFIC", null);
				request.setAttribute("REPORT_TITLE", reportTitle);
			}
			else if(q.equalsIgnoreCase("emission_report"))
			{
				List<DropDownDTO> reportTitle = CommonBusiness.getInstance().getDropDownList("GET_REPORT_TITLE_FOR_EMISSION", null);
				request.setAttribute("REPORT_TITLE", reportTitle);
			}
			else if(q.equalsIgnoreCase("headoffice_report"))
			{
				List<DropDownDTO> reportTitle = CommonBusiness.getInstance().getDropDownList("GET_REPORT_TITLE_FOR_HEADOFFICE", null);
				request.setAttribute("REPORT_TITLE", reportTitle);
			}
			else
			{
				List<DropDownDTO> reportTitle = CommonBusiness.getInstance().getDropDownList("GET_REPORT_TITLE", null);
				request.setAttribute("REPORT_TITLE", reportTitle);
			}
			
			
			actionForward = "report";
		}
		catch (Exception e) 
		{
			Log.error("Error ReportAction[execute]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward generateReport(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			serviceActionForm serviceActionForm = (serviceActionForm) form;
			ReportDTO dto = new ReportDTO();
			BeanUtils.copyProperties(dto, serviceActionForm);
			String reportId = request.getParameter("reportId");
			List<ReportDTO> reportHeaderList = ReportBusiness.getInstance().get_report_header(reportId,dto);
			String[][] arrayList = ReportBusiness.getInstance().get_report_details(reportId,dto);
			dto = ReportBusiness.getInstance().getReportParam(reportId);

			request.setAttribute("DTO", dto);
			request.setAttribute("REPORT_HEADER", reportHeaderList);
			request.setAttribute("REPORT_LIST", arrayList);
			actionForward = "report-detail";
		}
		catch (Exception e) 
		{
			Log.error("Error ReportAction[get_report_details]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward get_report_details(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ReportDTO dto = new ReportDTO();
			String reportId = request.getParameter("reportId");
			dto = ReportBusiness.getInstance().getReportParam(reportId);

			request.setAttribute("DTO", dto);
			actionForward = "report_criteria";
		}
		catch (Exception e) 
		{
			Log.error("Error ReportAction[get_report_details]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
}
