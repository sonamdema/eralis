package bt.gov.rsta.eralis.web.action.license;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.web.action.LoadApplicationBusiness;
import bt.gov.rsta.eralis.web.actionform.license.LicenseActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.UserDetailsVO;

public class LicenseAction extends DispatchAction
{
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null,jurisId = null, jurisTypeId = null, regionId = null, baseId = null;
	
	public ActionForward learner_license_new(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String driveTypeIds = request.getParameter("driveTypeIds");
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			dto.setDriveTypeList(driveTypeIds);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().new_issue(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[new_issue]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward formResubmit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result ="";
		try 
		{
			String serviceCode = request.getParameter("serviceCode");
			String formType = request.getParameter("formType");
			String driveTypeIds = request.getParameter("driveTypeIds");
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			dto.setDriveTypeList(driveTypeIds);
			dto.setFormType(formType);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			result = LicenseBusiness.getInstance().learner_and_license_resubmit(serviceCode,dto, userVo);
			
			
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[new_issue]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_renewal_learner(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_renewal_learner(dto, userVo);

			String[] resultArray = result.split("#");
			
			dto.setApplicationNo(resultArray[1]);
//			dto = LoadApplicationBusiness.getInstance().getlicenseRenewalList(dto);
//			String approvalStatus = LicenseBusiness.getInstance().driving_license_renewal_application_approval(dto, userVo);
			
			dto = LoadApplicationBusiness.getInstance().getlicenserenewalList(dto);
			String approvalStatus = LicenseBusiness.getInstance().license_renewal_application_approval(dto, userVo);
			
			request.setAttribute("approvalStatus", approvalStatus);
 
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_renewal_learner]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward pay_offence(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseDTO dto=new LicenseDTO();
			String amount	=	request.getParameter("amount");
			
			dto.setOffenceId(request.getParameter("offenceId"));
			dto.setReceiptDate(request.getParameter("receiptDate"));
			dto.setReceiptNo(request.getParameter("receiptNo"));
			dto.setTINno(request.getParameter("TINno"));
			dto.setAmount(amount);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().pay_offence(dto, userVo);
            request.setAttribute("MESSAGE", result);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_renewal_learner]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_license_offence(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			String result = LicenseBusiness.getInstance().new_issue(dto, null);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_license_offence]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward request_reprint(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().request_reprint(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[request_reprint]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward get_reprint_details(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String requestId = request.getParameter("requestId");
			LicenseDTO dto = LicenseBusiness.getInstance().get_reprint_details(requestId);
			
			request.setAttribute("REPRINT_DETAILS", dto);
			actionForward = "reprint_details";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[get_reprint_details]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward approve_request_reprint(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			String requestId = request.getParameter("requestId");
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			String result = LicenseBusiness.getInstance().approve_request_reprint(requestId, licenseForm.getRemarks(), userVo);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[approve_request_reprint]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_duplication_learner(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_duplication_learner(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplication_learner]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_non_commerical(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_non_commerical(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_non_commerical]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_commerical(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_commerical(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{e.printStackTrace();
			Log.error("Error LicenseAction[license_commerical]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_renewal(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			String result = LicenseBusiness.getInstance().license_renewal(dto, userVo);
			String[] resultArray = result.split("#");

			dto.setApplicationNo(resultArray[1]);
			dto = LoadApplicationBusiness.getInstance().getlicenseRenewalList(dto);
			String approvalStatus = LicenseBusiness.getInstance().driving_license_renewal_application_approval(dto, userVo);
			
			
			request.setAttribute("approvalStatus", approvalStatus);
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_renewal]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_duplication(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_duplication(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplication]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward license_endorsement(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String isRenewalProcess = request.getParameter("isRenewalProcessId");
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_endorsement(dto, userVo,isRenewalProcess);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_endorsement]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_suspension(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String formType = request.getParameter("FORM_TYPE");
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			dto.setFormType(formType);
			BeanUtils.copyProperties(dto, licenseForm);
			
			String result = LicenseBusiness.getInstance().license_suspension(dto);
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_license_suspension]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_drive_type_suspension(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String result = LicenseBusiness.getInstance().license_drive_type_suspension(dto);
            request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_drive_type_suspension]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_offence(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
		{
			String actionForward = null;
			HttpSession session = request.getSession();
			EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
			userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
			
			try
			{
				LicenseActionForm licenseForm = (LicenseActionForm) form;
				LicenseDTO dto=new LicenseDTO();
				
				this.setUserValues(request);
				UserDetailsVO userVo = new UserDetailsVO();
				this.copyUserValues(userVo);
				BeanUtils.copyProperties(dto, licenseForm);
				String offenceFormType = request.getParameter("offenceFormType");
				//dto.setIsForeign(isForeign);
				String tinNo = dto.getTinPrefix()+dto.getTINno();
				dto.setTINnoFor(tinNo);
				dto.setTINnoP(tinNo);
				dto.setTINno(tinNo);
				String result = LicenseBusiness.getInstance().license_offence(dto,offenceFormType, userVo);
				request.setAttribute("MESSAGE", result);
				
				actionForward = "message";
			}
			catch (Exception e) 
			{
				Log.error("Error LicenseAction[license_offence]: "+e);
				String owner = "";
				if(null != userRolePriv.getUserId()){
					owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
				}
				String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
				request.setAttribute("errorCode", messagekey);
				actionForward = "GLOBAL_REDIRECT_ERROR";
			}
			
			return mapping.findForward(actionForward);
		}
	
	public ActionForward license_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			String regionId =request.getParameter("regionId");
			dto.setRegionId(regionId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_renewal_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_renewal_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_renewal_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward non_commercial_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);

			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().non_commercial_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[non_commercial_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward commercial_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			//String customerId=request.getParameter("customerId");
			//dto.setCustomerId(customerId);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().commercial_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[commercial_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward driving_license_renewal_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().driving_license_renewal_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[driving_license_renewal_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward driving_license_endorsement_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().driving_license_endorsement_application_approval(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[driving_license_endorsement_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_application_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			
			
			
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[non_commercial_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward commercial_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().commercial_application_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[commercial_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward driving_license_renewal_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().driving_license_renewal_application_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[driving_license_renewal_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward driving_license_endorsement_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().driving_license_endorsement_application_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[driving_license_endorsement_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_duplicate_application_verify(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_duplicate_application_verify(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplicate_application_verify]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_duplicate_application_approve(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_duplicate_application_approve(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplicate_application_approve]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward driving_license_verify(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().driving_license_verify(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplicate_application_approve]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward driving_license_duplicate_approve(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().driving_license_duplicate_approve(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplicate_application_approve]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward license_duplicate_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_duplicate_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_duplicate_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward learner_duplicate_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().learner_duplicate_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[learner_duplicate_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward license_cancellation(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String formType = request.getParameter("FORM_TYPE");
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			dto.setFormType(formType);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().license_cancellation(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_cancellation]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}public ActionForward withdraw_license_cancellation(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			dto.setFormType(request.getParameter("FORM_TYPE"));
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			String result = LicenseBusiness.getInstance().withdraw_license_cancellation(dto, userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_cancellation]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward top_issuance(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().top_issuance(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[license_cancellation]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward top_application_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().top_application_approve(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[top_application_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward top_application_reject(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto=new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().top_application_reject(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[top_application_reject]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward top_cancellation(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
	
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().top_cancellation(dto, userVo);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[top_cancellation]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward top_replacement(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			
			BeanUtils.copyProperties(dto, licenseForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().top_replacement(dto, userVo);
			String[] resultArray = result.split("#");
            request.setAttribute("MESSAGE", resultArray[0]);
			request.setAttribute("APPLICATION_NO", resultArray[1]);
			request.setAttribute("USER_NAME", userRolePriv.getUserName());
			actionForward = "application_message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[top_replacement]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward top_replacement_approval(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			LicenseActionForm licenseForm = (LicenseActionForm) form;
			LicenseDTO dto = new LicenseDTO();
			BeanUtils.copyProperties(dto, licenseForm);
			
			String applicationNo =request.getParameter("applicationNo");
			dto.setApplicationNo(applicationNo);
			String customerId=request.getParameter("customerId");
			dto.setCustomerId(customerId);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = LicenseBusiness.getInstance().top_replacement_approval(dto, userVo);
            request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e) 
		{
			Log.error("Error LicenseAction[top_replacement_approval]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	/**
	 *  Set values of user related information
	 */
	private void setUserValues(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge)session.getAttribute(Constants.USER_DETAILS);
		this.userId = userRolePriv.getUserId();
		this.userName = userRolePriv.getUserId();
		Role currentRole = userRolePriv.getCurrentRole();
		this.roleId = currentRole.getRoleId();
		this.roleName = currentRole.getRoleName();
		this.roleCode = currentRole.getRoleCode();
		this.jurisId = userRolePriv.getJurisdictionId();
		this.jurisTypeId = userRolePriv.getJurisdictionTypeId();
		this.regionId = userRolePriv.getRegionId();
		this.baseId = userRolePriv.getBaseId();
	}
	
	/*
	 * Copy user values to data transfer object
	 */
	private void copyUserValues(UserDetailsVO userVo)
	{
		userVo.setActorId(userId);
		userVo.setActorName(userName);
		userVo.setAssignedGroupId(assignedGroupId);
		userVo.setAssignedUserId(userId);
		userVo.setRoleId(roleId);
		userVo.setRoleName(roleName);
		userVo.setAssignedPrivId(assignedPrivId);
		userVo.setRoleCode(roleCode);
		userVo.setJurisdictionId(jurisId);
		userVo.setJurisdictionTypeId(jurisTypeId);
		userVo.setRegionId(regionId);
		userVo.setBaseId(baseId);
	}
	

}
