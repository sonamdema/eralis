package bt.gov.rsta.eralis.web.action.administration;

import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.google.gson.Gson;
import bt.gov.rsta.eralis.business.administration.AdministrationBusiness;
import bt.gov.rsta.eralis.dto.administration.AdministrationDTO;
import bt.gov.rsta.eralis.dto.administration.PagePermissionDTO;
import bt.gov.rsta.eralis.dto.administration.PrivDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.web.actionform.administration.AdministrationActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.business.ServiceBusiness;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.ldapconfig.LdapPasswordUpdate;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.web.actionform.service.serviceActionForm;

public class AdministrationAction extends DispatchAction
{
	public ActionForward add_user(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			AdministrationActionForm adminForm = (AdministrationActionForm) form;
			AdministrationDTO dto = new AdministrationDTO();
			BeanUtils.copyProperties(dto, adminForm);
			
			String result = AdministrationBusiness.getInstance().add_user(dto);
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[add_user]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward add_role(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String role = request.getParameter("role");
			String isActive = request.getParameter("isActive");
			
			String result = AdministrationBusiness.getInstance().add_role(role, isActive);
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[add_role]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	
	public ActionForward activateDeactivateRole(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null, message = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String roleId = request.getParameter("roleId");
			String typeFlag = request.getParameter("typeFlag");
			
			String result = AdministrationBusiness.getInstance().activateDeactivateRole(roleId, typeFlag);
			
			if(result.equalsIgnoreCase("SUCCESS") && typeFlag.equalsIgnoreCase("Y"))
				message = "ROLE_ACTIVATE_SUCCESS";
			else if(result.equalsIgnoreCase("FAILURE") && typeFlag.equalsIgnoreCase("Y"))
				message = "ROLE_ACTIVATE_FAILURE";
			if(result.equalsIgnoreCase("SUCCESS") && typeFlag.equalsIgnoreCase("N"))
				message = "ROLE_DEACTIVATE_SUCCESS";
			else if(result.equalsIgnoreCase("FAILURE") && typeFlag.equalsIgnoreCase("N"))
				message = "ROLE_DEACTIVATE_FAILURE";
			
			request.setAttribute("MESSAGE", message);
			
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[activateDeactivateRole]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward user_management(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null, message = null, result = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String loginId = request.getParameter("loginId");
			String typeFlag = request.getParameter("typeFlag");
			String emailAddress = request.getParameter("emailId");
			String mobileNumber = request.getParameter("mobileNumber");
			
			
			if(typeFlag.equalsIgnoreCase("RESET_PASSWORD"))
			{
				String password = Constants.ERALIS_PASSWORD_BUILDER+"@"+loginId;
				boolean isPasswordReset = LdapPasswordUpdate.resetUserPassword(loginId, password);
				
				if(isPasswordReset)
				{
					result = "SUCCESS";
					
					NotificationDTO notifyDTO = new NotificationDTO();
					notifyDTO.setEmailAddress(emailAddress);
					notifyDTO.setLoginId(loginId);
					notifyDTO.setPassword(password);
					notifyDTO.setMobileNumber(mobileNumber);
					
					EmailModelVO emailVO = new EmailModelVO();
					emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_USER_PASSWORD_RESET);
					AdministrationBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					
					SMSModelVO smsVO = new SMSModelVO();
					smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_USER_PASSWORD_RESET);
					AdministrationBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
				}
			}
			else
				result = AdministrationBusiness.getInstance().user_management(loginId, typeFlag);
			
			if(result.equalsIgnoreCase("SUCCESS") && typeFlag.equalsIgnoreCase("DELETE_USER"))
				message = "DELETE_USER_SUCCESS";
			else if(result.equalsIgnoreCase("FAILURE") && typeFlag.equalsIgnoreCase("DELETE_USER"))
				message = "DELETE_USER_FAILURE";
			if(result.equalsIgnoreCase("SUCCESS") && typeFlag.equalsIgnoreCase("RESET_PASSWORD"))
				message = "RESET_PASSWORD_SUCCESS";
			else if(result.equalsIgnoreCase("FAILURE") && typeFlag.equalsIgnoreCase("RESET_PASSWORD"))
				message = "RESET_PASSWORD_FAILURE";
			
			request.setAttribute("MESSAGE", message);
			
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[userManagement]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_user(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			AdministrationActionForm adminForm = (AdministrationActionForm) form;
			AdministrationDTO dto = new AdministrationDTO();
			
			BeanUtils.copyProperties(dto, adminForm);
			
			String result = AdministrationBusiness.getInstance().edit_user(dto);
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[edit_user]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getRolePermissionDtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String roleId = request.getParameter("roleId");
			String roleName = request.getParameter("roleName");
			String isActive = request.getParameter("isActive");
			
			AdministrationActionForm adminForm = (AdministrationActionForm)form;
			
			List<PagePermissionDTO> pagePermissionList = AdministrationBusiness.getInstance().getPagePermissionList(roleId);
			adminForm.setPermissionList(pagePermissionList);
			
			List<PagePermissionDTO> servicePrivList = AdministrationBusiness.getInstance().getServicePrivList(roleId);
			request.setAttribute("servicePrivList", servicePrivList);
			
			List<PrivDTO> previousPrivList = AdministrationBusiness.getInstance().getPreviousPrivList(roleId);
			request.setAttribute("previousPrivList", previousPrivList);
			
			List<String> pageIdList = AdministrationBusiness.getInstance().getPageList();
			
			int serviceCount = AdministrationBusiness.getInstance().getServiceCount();
			
			request.setAttribute("roleId", roleId);
			request.setAttribute("roleName", roleName);
			request.setAttribute("isActive", isActive);
			request.setAttribute("pageIdList", pageIdList); 
			request.setAttribute("serviceCount", Integer.toString(serviceCount));
			
			actionForward = "page_permission";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[getRolePermissionDtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward update_permission_dtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String roleId = request.getParameter("roleId");
			String roleName = request.getParameter("roleName");
			String isActive = request.getParameter("isActive");
			String checkedVals = request.getParameter("checkedVals");
			String serviceCheckedVals = request.getParameter("serviceCheckedVals");
			
			AdministrationActionForm adminForm = (AdministrationActionForm)form;
			AdministrationDTO dto = new AdministrationDTO();
			
			BeanUtils.copyProperties(dto, adminForm);
			dto.setRole(roleName);
			dto.setRoleId(roleId);
			dto.setIsActiveCheckBox(isActive);
			dto.setCheckedVals(checkedVals);
			dto.setServiceCheckedVals(serviceCheckedVals);
			
			String result = AdministrationBusiness.getInstance().update_permission_dtls(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[update_permission_dtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward add_master_data(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			AdministrationActionForm adminForm = (AdministrationActionForm)form;
			AdministrationDTO dto = new AdministrationDTO();
			
			BeanUtils.copyProperties(dto, adminForm);
			
			String result = AdministrationBusiness.getInstance().add_master_data(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[add_master_data]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_master_data(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			AdministrationActionForm adminForm = (AdministrationActionForm)form;
			AdministrationDTO dto = new AdministrationDTO();
			
			BeanUtils.copyProperties(dto, adminForm);
			
			String result = AdministrationBusiness.getInstance().edit_master_data(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[edit_master_data]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward delete_master_data(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String id = request.getParameter("id");
			String typeFlag = request.getParameter("typeFlag");
			
			AdministrationDTO dto = new AdministrationDTO();
			dto.setMasterId(id);
			dto.setMaster_identifier(typeFlag);
			
			String result = AdministrationBusiness.getInstance().delete_master_data(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[delete_master_data]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward updateMailConfiguration(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			AdministrationActionForm actionForm = (AdministrationActionForm)form;
			AdministrationDTO dto = new AdministrationDTO();
			BeanUtils.copyProperties(dto, actionForm);
			
			String result = AdministrationBusiness.getInstance().updateMailConfiguration(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[updateMailConfiguration]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward accounting_configuration(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String id = request.getParameter("id");
			String typeFlag = request.getParameter("typeFlag");
			
			AdministrationDTO dto = new AdministrationDTO();
			dto.setMasterId(id);
			dto.setMaster_identifier(typeFlag);
			
			//String result = AdministrationBusiness.getInstance().delete_master_data(dto);
			//request.setAttribute("MESSAGE", result);
			//actionForward = "message";
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[delete_master_data]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward edit_vehicle_constant(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	"";
		try
		{
			AdministrationActionForm adminstrationForm = (AdministrationActionForm) form;
			EralisCommonDTO dto = new EralisCommonDTO();
			BeanUtils.copyProperties(dto, adminstrationForm);

			 result = AdministrationBusiness.getInstance().edit_vehicle_constant(dto);
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[edit_vehicle_constant]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward edit_license_constant(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	"";
		try
		{
			AdministrationActionForm adminstrationForm = (AdministrationActionForm) form;
			EralisCommonDTO dto = new EralisCommonDTO();
			BeanUtils.copyProperties(dto, adminstrationForm);

			 result = AdministrationBusiness.getInstance().edit_license_constant(dto);
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[edit_license_constant]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	public ActionForward edit_penalty_constant(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	"";
		try
		{
			AdministrationActionForm adminstrationForm = (AdministrationActionForm) form;
			EralisCommonDTO dto = new EralisCommonDTO();
			BeanUtils.copyProperties(dto, adminstrationForm);

			 result = AdministrationBusiness.getInstance().edit_penalty_constant(dto);
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[edit_penalty_constant]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward getColumnList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			String jsonText = "";
			Gson gson = new Gson();
			
			String tableName = request.getParameter("tableName");
			List<DropDownDTO> columnList = AdministrationBusiness.getInstance().getColumnList(tableName);
			jsonText = gson.toJson(columnList);
			jsonText = new String(jsonText.getBytes("UTF-8"), "UTF-8");
			out.println(jsonText);
			out.flush();
		} 
		catch (Exception e)
		{
			Log.error("Error AdministrationAction[getColumnList]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
		}
		
		return null;
	}
	
	public ActionForward learnerLicenseDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String applicationNo	=	null;
			String learnerNo	=	request.getParameter("learnerNo");
			String cid	=	request.getParameter("cid");
			String testType = request.getParameter("testType");
			String issueType = request.getParameter("issueType");
			String form_type	=	request.getParameter("form_type");
			String issueBasedOn	=	request.getParameter("issueBasedOn");
			actionForward = form_type;
			String messageType=null;
			if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
			{
				if("Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
				{
					issueType = issueBasedOn;
					if("DL".equalsIgnoreCase(issueBasedOn))
					{
						testType = "ENDORSEMENT";	
					}
					else if("Learner_License".equalsIgnoreCase(issueBasedOn))
					{
						testType = "NEW";	
					}
					
				}
				
				ServiceDTO dto = ServiceBusiness.getInstance().getLearnerLicense(learnerNo,cid, testType,issueType);
				applicationNo	=	dto.getApplicationNumber();
				if(dto.getRowCount().equalsIgnoreCase("0"))
			  	{
		 			request.setAttribute("parameters", "Invalid selection, please select correct issue type and test type OR Learner No. or CID");
		 			actionForward = "serviceMessage";
		 			messageType="checkExist";
		 			request.setAttribute("messageType", messageType);
			  	}
			  	else
			  	{
			  		
			  		if("0".equalsIgnoreCase(dto.getStatus()))
			  		{
			  			request.setAttribute("parameters", dto.getReason());
			 			actionForward = "serviceMessage";
			 			messageType	=	"validation";
			  			request.setAttribute("messageType", messageType);
			  		}
			  		else
			  		{

						actionForward = "book_driving_test";
				  		EralisCommonDTO commonDTO = CommonBusiness.getInstance().getlearnerRenewalInfoDtls(dto.getPersonalInfoId(),"");
				  		
				  		/*String latestExpiryDate = ServiceBusiness.getInstance().getLatestExpiryDate(commonDTO.getLearnerLicenseId(), "LEARNER");
				  		request.setAttribute("LATEST_EXPIRY_DATE", latestExpiryDate);*/
				  		
				  		if("HAS_OFFENCE".equalsIgnoreCase(commonDTO.getStatus()))
				  		{
				  			request.setAttribute("parameters", "Learner License Has Offence");
				 			actionForward = "serviceMessage";
				 			request.setAttribute("messageType", "validation");
				  		}
				  		else
				  		{
				  			request.setAttribute("customerId", commonDTO.getCustomerID());
				  			request.setAttribute("name", commonDTO.getName());
				  			request.setAttribute("bloodgroup", commonDTO.getBloodGroup());
				  			request.setAttribute("dob", commonDTO.getDOB());
				  			request.setAttribute("country", commonDTO.getCountry());
				  			request.setAttribute("village", commonDTO.getVillage());
				  			request.setAttribute("dzongkhag", commonDTO.getDzongkhag());
				  			request.setAttribute("gewog", commonDTO.getGewog());
				  			request.setAttribute("address", commonDTO.getAddress());
				  			request.setAttribute("learnerlicenseNo", commonDTO.getLLNo());
				  			request.setAttribute("region", commonDTO.getRegion());
				  			request.setAttribute("expirydate", commonDTO.getExpiryDate());
				  			request.setAttribute("issuedate", commonDTO.getDateOfissue());
				  			request.setAttribute("learnerLicenseId", dto.getLearnerLicenseId());
				  			
					  		if(form_type.equalsIgnoreCase("renew_learner_search"))
					  		{
					  			ApplicationDataVO appVO = new ApplicationDataVO();
					  			appVO.setLicenseno(learnerNo);
					  			
					  			//List<DriveTypeDTO> drivetypeList = CommonBusiness.getInstance().drivetype_list(learnerNo);
								//request.setAttribute("DRIVETYPE_LIST", drivetypeList);
								
					  			List<LicenseDTO> learnerLicenseRenewalList = CommonBusiness.getInstance().getlearnerlicense(appVO);
								request.setAttribute("RENEWAL_LEARNER_LICENSE_LIST", learnerLicenseRenewalList);
							  
								actionForward = "renew_learner_dtls";
					  		}
					  		else if(form_type.equalsIgnoreCase("learner_license_duplicate"))
					  		{
					  			ApplicationDataVO appVO = new ApplicationDataVO();
					  			appVO.setLicenseno(learnerNo);
					  			
								List<LicenseDTO> learnerDuplicationList = CommonBusiness.getInstance().getlearnerduplicationlist(appVO);
								request.setAttribute("DUPLICATE_LEARNER_LIST", learnerDuplicationList);
								
					  			actionForward = form_type;
					  		}
					  		else if(form_type.equalsIgnoreCase("issuance_non_commercial_license"))
					  		{
					  			List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
								request.setAttribute("regionList", regionList);
								
					  			actionForward = form_type;
					  		}
					  		else
					  		{
					  			request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
					 			request.setAttribute("firstName", dto.getFirstName());
					 			request.setAttribute("middleName", dto.getMiddleName());
					 			request.setAttribute("lastName", dto.getLastName());
								request.setAttribute("cid", dto.getCid());
								request.setAttribute("gender", dto.getGender());
								request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
								request.setAttribute("presentEmail", dto.getPresentEmail());
								request.setAttribute("image", dto.getImage());
					  			request.setAttribute("rowCount", dto.getRowCount());
								request.setAttribute("learnerNo", learnerNo);
								request.setAttribute("cid", cid);
								request.setAttribute("dob", dto.getDob());
								
								request.setAttribute("Test_Date", dto.getTestDate());
								request.setAttribute("Test_Location_Id", dto.getTestLocation());
								request.setAttribute("Drive_Type_Id", dto.getDrivetype());
								request.setAttribute("TEST_TYPE", testType);
								request.setAttribute("CUSTOMER_ID", dto.getCustomerID());
								
								
								
					  		}
					  		List<DropDownDTO> regionList = CommonBusiness.getInstance().getDropDownList("REGION_LIST", null);
							request.setAttribute("regionList", regionList);
							
							List<DropDownDTO> locationList = CommonBusiness.getInstance().getDropDownList("GET_ONLINE_LOCATION_LIST", null);
							request.setAttribute("locationList", locationList);
				  			
				  		}
			  			
			  		}
			  	}
			}
			else
			{
				testType = "NEW";
				ServiceDTO dto = ServiceBusiness.getInstance().getOtherIssueTypeBookDtls(learnerNo,cid, testType,issueType);
				applicationNo	=	dto.getApplicationNumber();
				
				
				if(dto.getStatus().equalsIgnoreCase("DRIVING_LICENSE_ALREADY_EXISTED"))
			  	{
		 			request.setAttribute("parameters", "Sorry! Driving License is already issued in this CID No., Please book with your Driving License No.");
		 			actionForward = "serviceMessage";
		 			messageType="checkExist";
		 			request.setAttribute("messageType", "validation");
			  	}
				else
				{
					request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
		 			request.setAttribute("firstName", dto.getFirstName());
		 			request.setAttribute("middleName", dto.getMiddleName());
		 			request.setAttribute("lastName", dto.getLastName());
					request.setAttribute("cid", dto.getCid());
					request.setAttribute("gender", dto.getGender());
					request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
					request.setAttribute("presentEmail", dto.getPresentEmail());
					request.setAttribute("image", dto.getImage());
		  			request.setAttribute("rowCount", dto.getRowCount());
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("cid", cid);
					request.setAttribute("dob", dto.getDob());
					
					request.setAttribute("Test_Date", dto.getTestDate());
					request.setAttribute("Test_Location_Id", dto.getTestLocation());
					request.setAttribute("Drive_Type_Id", dto.getDrivetype());
					request.setAttribute("TEST_TYPE", testType);
					request.setAttribute("CUSTOMER_ID", dto.getCustomerID());
				}
			}
			if("book_driving_test".equalsIgnoreCase(form_type))
			{
				request.setAttribute("test_applicationNo", applicationNo);
				List<ServiceDTO> testLocationList = CommonBusiness.getInstance().getTestLocationForAdmin();
				request.setAttribute("baseofficeList", testLocationList);
				String bookedTestDtls = "0";
				if(applicationNo!=null)
				{
					bookedTestDtls = CommonBusiness.getInstance().bookedTestDtls(applicationNo);
				}
				request.setAttribute("BOOKED_TEST_DTLS", bookedTestDtls);
				
				if(testType.equals("NEW"))
				{
					List<DropDownDTO> driveType = CommonBusiness.getInstance().getDropDownList("GET_LEARNER_DRIVE_TYPE", learnerNo);
					request.setAttribute("DRIVE_TYPE", driveType);
				}
				else if(testType.equals("ENDORSEMENT"))
				{
					List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
					request.setAttribute("DRIVE_TYPE", driveTypeList);
				}
				else
				{
					actionForward = "book_driving_test";
				}
			}
		}
		catch (Exception e)
		{e.printStackTrace();
			Log.error("Error ServiceAction[learnerLicenseDetails]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		return mapping.findForward(actionForward);
	}
	

	public ActionForward bookDrivingTest(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		String result	=	null;
		try
		{
			String issueType	=	request.getParameter("issueType");
			serviceActionForm serviceForm = (serviceActionForm) form;
			ServiceDTO sdto = new ServiceDTO();
			BeanUtils.copyProperties(sdto, serviceForm);
			String learnerNo	=	sdto.getLearnerNo();
			String cid			=	null;
			String issueBasedOn = request.getParameter("issueBasedOn");
			String testType =  request.getParameter("testType");
			if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
			{
				if("Learner_License".equalsIgnoreCase(issueBasedOn) || "DL".equalsIgnoreCase(issueBasedOn))
				{
					issueType = issueBasedOn;
					if("DL".equalsIgnoreCase(issueBasedOn))
					{
						testType = "ENDORSEMENT";	
					}
					else if("Learner_License".equalsIgnoreCase(issueBasedOn))
					{
						testType = "NEW";	
					}
				}
				sdto.setIssueType(issueType);
				sdto.setTestType(testType);
			}
			
			result = CommonBusiness.getInstance().bookDrivingTest(sdto,issueType,cid,learnerNo);
			String[] temp = result.split("#");
			
			if(temp[0].equalsIgnoreCase("SUCCESS"))
			{
				if("Driving_Institue".equalsIgnoreCase(issueType) || "Learner_License".equalsIgnoreCase(issueType) || "DL".equalsIgnoreCase(issueType))
				{
					ServiceDTO dto = ServiceBusiness.getInstance().getLearnerLicense(learnerNo,cid,testType,issueType);
		 			request.setAttribute("testMessage", "1");
		 			request.setAttribute("titleCourtesy", dto.getTitleCourtesy());
		 			request.setAttribute("firstName", dto.getFirstName());
		 			request.setAttribute("middleName", dto.getMiddleName());
		 			request.setAttribute("lastName", dto.getLastName());
					request.setAttribute("cid", dto.getCid());
					request.setAttribute("gender", dto.getGender());
					request.setAttribute("presentPhoneNo", dto.getPresentPhoneNo());
					request.setAttribute("presentEmail", dto.getPresentEmail());
					request.setAttribute("image", dto.getImage());
					request.setAttribute("rowCount", dto.getRowCount());
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("dob", dto.getDob());
					request.setAttribute("test_applicationNo", dto.getApplicationNumber());
					request.setAttribute("Test_Date", dto.getTestDate());
					request.setAttribute("Test_Location_Id", dto.getTestLocation());
					request.setAttribute("Drive_Type_Id", dto.getDrivetype());
					request.setAttribute("TEST_TYPE", sdto.getTestType());
					
				}
				else
				{
					request.setAttribute("testMessage", "1");
		 			request.setAttribute("titleCourtesy", "");
		 			request.setAttribute("firstName",  "");
		 			request.setAttribute("middleName",  "");
		 			request.setAttribute("lastName",  "");
					request.setAttribute("cid",  "");
					request.setAttribute("gender",  "");
					request.setAttribute("presentPhoneNo",  "");
					request.setAttribute("presentEmail",  "");
					request.setAttribute("image",  "");
					request.setAttribute("rowCount",  "");
					request.setAttribute("learnerNo", learnerNo);
					request.setAttribute("dob",  "");
					request.setAttribute("test_applicationNo",  "");
					request.setAttribute("Test_Date",  "");
					request.setAttribute("Test_Location_Id",  "");
					request.setAttribute("Drive_Type_Id",  "");
					request.setAttribute("TEST_TYPE",  "");
				}
				
			 
				

				List<ServiceDTO> testLocationList = CommonBusiness.getInstance().getTestLocation();
				request.setAttribute("baseofficeList", testLocationList);
				
				if(sdto.getTestType().equals("NEW"))
				{
					List<DropDownDTO> driveType = CommonBusiness.getInstance().getDropDownList("GET_LEARNER_DRIVE_TYPE", learnerNo);
					request.setAttribute("DRIVE_TYPE", driveType);
				}
				else if(sdto.getTestType().equals("ENDORSEMENT"))
				{
					List<DropDownDTO> driveTypeList = CommonBusiness.getInstance().getDropDownList("DRIVE_TYPE_LIST", null);
					request.setAttribute("DRIVE_TYPE", driveTypeList);
				}

				String bookedTestDtls = "0";
				request.setAttribute("BOOKED_TEST_DTLS", bookedTestDtls);
				
				request.setAttribute("BFS_ORDER_NO", temp[1]);
				request.setAttribute("APPLICATION_NO", "BOOKING");
				
				//actionForward = "book_driving_test"; 
				actionForward = "online_payment_receipt";
			}
			else if(temp[0].equalsIgnoreCase("NO_SEAT_AVAILABLE"))
			{
				actionForward = "serviceMessage"; 
				request.setAttribute("messageType", "validation");
				request.setAttribute("parameters", "Sorry! All seats are booked. Either choose another test date or contact nearest RSTA office for help.");
				
			}
		}
		catch (Exception e)
		{
			Log.error("Error ServiceAction[organizationVehicleRenewaldtls]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
}
