package bt.gov.rsta.eralis.web.action.eralis_common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import bt.gov.rsta.eralis.business.etest.ETestBusiness;
import bt.gov.rsta.eralis.dto.etest.ETestDTO;
import bt.gov.rsta.eralis.web.actionform.eralis_common.ETestActionForm;
import bt.gov.rsta.framework.business.CommonBusiness;
import bt.gov.rsta.framework.dto.EralisUserRolePriviledge;
import bt.gov.rsta.framework.dto.Role;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;

public class ETestAction extends DispatchAction
{
	private String userName = null, userId = null, roleId = null, roleName = null, roleCode = null, userCode = null, assignedGroupId = null, assignedPrivId = null;
	
	public ActionForward login(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		
		try 
		{
			String learnerNo = (String)request.getAttribute("LEARNER_NO");
			String dob = (String)request.getAttribute("DOB");
			
			ApplicationDataVO vo = ETestBusiness.getInstance().login(learnerNo, dob);
			request.setAttribute("MESSAGE", vo.getStatus());
			
			if(vo.getStatus().equalsIgnoreCase("LOGIN_SUCCESS"))
			{
				actionForward = "test_page";
				request.setAttribute("NAME", vo.getFullName());
				request.setAttribute("LEARNER_NO", learnerNo);
				request.setAttribute("APPLICATION_NO", vo.getApplicationNumber());
			}
			else
			{
				actionForward = "GLOBAL_REDIRECT_TEST_LOGIN";
			}
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[login]: "+e);
			String owner = "CITIZEN";
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward start_test(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		
		try 
		{
			String learnerNo = request.getParameter("learnerNo");
			String name = request.getParameter("name");
			String appNo = request.getParameter("appNo");
			
			ETestDTO dto = ETestBusiness.getInstance().start_test(learnerNo);
			request.setAttribute("TEST_DETAILS", dto);
			request.setAttribute("NAME", name);
			request.setAttribute("LEARNER_NO", learnerNo);
			request.setAttribute("APPLICATION_NO", appNo);
			session.setAttribute("QUESTION_LIST", dto.getQuestionIdList());
			request.setAttribute("MESSAGE", dto.getMessage());
			
			if(dto.getMessage().equals("FOUND"))
			{
				actionForward = "start_test";
			}
			else
			{
				actionForward = "message";
			}
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[start_test]: "+e);
			String owner = "CITIZEN";
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward get_question(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		
		try 
		{
			ArrayList<Integer> questionIdList = (ArrayList<Integer>) session.getAttribute("QUESTION_LIST");
			String questionId = request.getParameter("questionId");
			String identifier = request.getParameter("identifier");
			
			if(identifier.equalsIgnoreCase("NEXT"))
			{
				if(!questionIdList.isEmpty())
				{
					Iterator<Integer> it = questionIdList.iterator();
					while(it.hasNext()) {
						Integer listQuestionId = it.next();
						if(questionId.equals(Integer.toString(listQuestionId))){
							it.remove();
						}
					}
				}
			}
			
			String latestQuestionId = Integer.toString(questionIdList.get(0));
			
			ETestDTO dto = ETestBusiness.getInstance().get_question(latestQuestionId, identifier);
			request.setAttribute("QUESTION_DETAILS", dto);
			
			actionForward = "question_page";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[get_question]: "+e);
			String owner = "CITIZEN";
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_test_details(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ETestActionForm eTestActionForm = (ETestActionForm) form;
			ETestDTO dto = new ETestDTO();
			BeanUtils.copyProperties(dto, eTestActionForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = ETestBusiness.getInstance().edit_test_details(dto, userRolePriv.getJurisdictionId(), userRolePriv.getJurisdictionTypeId(),userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[edit_test_details]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward add_test_details(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try
		{
			ETestActionForm eTestActionForm = (ETestActionForm) form;
			ETestDTO dto = new ETestDTO();
			BeanUtils.copyProperties(dto, eTestActionForm);
			
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			
			String result = ETestBusiness.getInstance().add_test_details(dto, userRolePriv.getJurisdictionId(), userRolePriv.getJurisdictionTypeId(),userVo);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[add_test_details]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward add_questions(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			ETestActionForm eTestActionForm = (ETestActionForm) form;
			ETestDTO dto = new ETestDTO();
			String options[] = request.getParameterValues("options");
			BeanUtils.copyProperties(dto, eTestActionForm);
			dto.setOptions(options);
			
			String result = ETestBusiness.getInstance().add_questions(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[add_questions]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward edit_question(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			ETestActionForm eTestActionForm = (ETestActionForm) form;
			ETestDTO dto = new ETestDTO();
			String options[] = request.getParameterValues("options");
			BeanUtils.copyProperties(dto, eTestActionForm);
			dto.setOptions(options);
			
			String result = ETestBusiness.getInstance().edit_question(dto);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[add_questions]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward delete_question(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String questionId = request.getParameter("questionId");
			String result = ETestBusiness.getInstance().delete_question(questionId);
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[delete_question]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward get_edit_dtls(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		try 
		{
			String questionId = request.getParameter("questionId");
			ETestDTO editDtls = ETestBusiness.getInstance().get_edit_dtls(questionId);
			request.setAttribute("editDtls", editDtls);
			request.setAttribute("questionId", questionId);
			
			actionForward = "etest_edit";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[delete_question]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward process_result(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		
		try
		{
			String testResult = request.getParameter("resultTextBox");
			String name = request.getParameter("name");
			String learnerNo = request.getParameter("learnerNo");
			String applicationNo = request.getParameter("applicationNo");
			ETestDTO dto = ETestBusiness.getInstance().process_result(testResult, name, learnerNo, applicationNo);
			
			request.setAttribute("RESULT", dto);
			actionForward = "result_page";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[process_result]: "+e);
			String owner = "CITIZEN";
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward exit_test(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		
		try 
		{
			session.removeAttribute("QUESTION_LIST");
			request.setAttribute("MESSAGE", "TEST_COMPLETED");
			actionForward = "GLOBAL_REDIRECT_TEST_LOGIN";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[exit_test]: "+e);
			String owner = "CITIZEN";
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward submit_marksheet(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String result	=	null;
			String learnerNo = request.getParameter("learnerNo");
			String practicalMarksObtained = request.getParameter("practicalMarksObtained");
			String overAllMarksObtained = request.getParameter("overAllMarksObtained");
			String appNo = request.getParameter("appNo");
			String practicalTestStatus = request.getParameter("practicalTestStatus");
			String theoryMarksObtained = request.getParameter("theoryMarksObtained");
			String isViva = request.getParameter("isViva");
			String theoryTestStatus = request.getParameter("theoryTestStatus");
			String type = request.getParameter("type");
			String driveType = request.getParameter("driveType");
			
			ETestDTO dto = new ETestDTO();
			dto.setLearnerNo(learnerNo);
			dto.setPracticalMarksObtained(practicalMarksObtained);
			dto.setOverAllMarksObtained(overAllMarksObtained);
			dto.setAppNo(appNo);
			dto.setPracticalTestStatus(practicalTestStatus);
			dto.setMarksObtained(theoryMarksObtained);
			dto.setIsViva(isViva);
			dto.setTestStatus(theoryTestStatus);
			dto.setDriveType(driveType);
			this.setUserValues(request);
			UserDetailsVO userVo = new UserDetailsVO();
			this.copyUserValues(userVo);
			if("CID".equalsIgnoreCase(type))
			{
				//result = ETestBusiness.getInstance().CID_submit_marksheet(dto, userVo);
				result = ETestBusiness.getInstance().submit_marksheet(dto, userVo);
				}
			else
			{
				result = ETestBusiness.getInstance().submit_marksheet(dto, userVo);
			}
			
			request.setAttribute("MESSAGE", result);
			
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[submit_marksheet]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward admin_candidate_list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String testDate = request.getParameter("testDate");
			String jurisId = request.getParameter("jurisId");
			String jurisTypeId = request.getParameter("jurisTypeId");
			
			List<ETestDTO> candidateList = ETestBusiness.getInstance().candidate_list(testDate, jurisId, jurisTypeId);
			request.setAttribute("candidate_list", candidateList);
			
			actionForward = "admin_candidate_list_result";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[candidate_list]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}

	public ActionForward cancelBooking(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String applicationNo = request.getParameter("applicationNo");
			 
			
			String result = ETestBusiness.getInstance().candidate_list(applicationNo);
			
			request.setAttribute("MESSAGE", result);
			actionForward = "message";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[candidate_list]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	public ActionForward candidate_list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		String actionForward = null;
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = new EralisUserRolePriviledge();
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		
		try 
		{
			String testDate = request.getParameter("testDate");
			String jurisId = request.getParameter("jurisId");
			String jurisTypeId = request.getParameter("jurisTypeId");
			
			List<ETestDTO> candidateList = ETestBusiness.getInstance().candidate_list(testDate, jurisId, jurisTypeId);
			request.setAttribute("candidate_list", candidateList);
			
			actionForward = "candidate_list_result";
		}
		catch (Exception e)
		{
			Log.error("Error ETestAction[candidate_list]: "+e);
			String owner = "";
			if(null != userRolePriv.getUserId()){
				owner = userRolePriv.getUserId() + "(" + userRolePriv.getUserName() + ")";
			}
			String messagekey = CommonBusiness.getInstance().insertErrorLog(EralisCommonUtil.getStackTraceAsString(e), owner);
			request.setAttribute("errorCode", messagekey);
			actionForward = "GLOBAL_REDIRECT_ERROR";
		}
		
		return mapping.findForward(actionForward);
	}
	
	/**
	 *  Set values of user related information
	 */
	private void setUserValues(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		EralisUserRolePriviledge userRolePriv = (EralisUserRolePriviledge)session.getAttribute(Constants.USER_DETAILS);
		this.userId = userRolePriv.getUserId();
		this.userName = userRolePriv.getUserName();
		Role currentRole = userRolePriv.getCurrentRole();
		this.roleId = currentRole.getRoleId();
		this.roleName = currentRole.getRoleName();
		this.roleCode = currentRole.getRoleCode();
	}
	
	/*
	 * Copy user values to data transfer object
	 */
	private void copyUserValues(UserDetailsVO userVo)
	{
		userVo.setActorId(userId);
		userVo.setActorName(userName);
		userVo.setAssignedGroupId(assignedGroupId);
		userVo.setAssignedUserId(userId);
		userVo.setRoleId(roleId);
		userVo.setRoleName(roleName);
		userVo.setAssignedPrivId(assignedPrivId);
		userVo.setRoleCode(roleCode);
	}
}
