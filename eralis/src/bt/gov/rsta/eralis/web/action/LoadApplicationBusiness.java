package bt.gov.rsta.eralis.web.action;

import java.sql.Connection;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dto.ServiceDTO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class LoadApplicationBusiness {

	Connection conn = null;
	private static LoadApplicationBusiness loadapplicationBusiness;
	
	public static LoadApplicationBusiness getInstance()
	{
		if(loadapplicationBusiness == null)
			loadapplicationBusiness = new LoadApplicationBusiness();
		
		return loadapplicationBusiness;
	}
	
	public String getGeneratedNumber(String applicationNo, String type) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getGeneratedNumber(applicationNo, type);
	}
	
	public ServiceDTO get_payment_dtls(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().get_payment_dtls(dto);
	}
	public LicenseDTO getlicenseapplicationList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getlicenseapplicationList(dto);
	}
	public LicenseDTO getlicenserenewalList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getlicenserenewalList(dto);
	}
	public LicenseDTO getlicenseduplicateList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getlicenseduplicateList(dto);
	}
	public LicenseDTO getlicensenduplication(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getlicensenduplication(dto);
	}
	public LicenseDTO getdrivinglicensenduplicate(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getdrivinglicensenduplicate(dto);
	}
//vehicle	
	public VehicleDTO getvehicleRegistrationList(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getvehicleRegistrationList(dto);
	}
	public VehicleDTO load_pbrp_for_approval(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().load_pbrp_for_approval(dto);
	}
	public VehicleDTO get_vehicle_application_dtls(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().get_vehicle_application_dtls(dto);
	}
	public VehicleDTO getvehicleRenewalList(VehicleDTO dto, String param) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getvehicleRenewalList(dto, param);
	}
	public VehicleDTO getvehicleConversionList(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getvehicleConversionList(dto);
	}
	public VehicleDTO getvehicleDuplicationList(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getvehicleDuplicationList(dto);
	}
	public VehicleDTO getvehicleTransferList(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getvehicleTransferList(dto);
	}
	public LicenseDTO getnoncommercialList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getnoncommercialList(dto);
	}
	public LicenseDTO getcommercialList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getcommercialList(dto);
	}
	public LicenseDTO getlicenseRenewalList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getlicenseRenewalList(dto);
	}
	public LicenseDTO getlicenseEndorsementList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getlicenseEndorsementList(dto);
	}
	//For TOP
	public LicenseDTO getTOPList(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getTOPList(dto);
	}
	
	public LicenseDTO getTopReplacementDetails(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getTOPReplacementDetails(dto);
	}
	
	public String getRejectionReason(ServiceDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getRejectionReason(dto);
	}
	
	public LicenseDTO getETestResult(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		return LoadApplicationDAO.getInstance().getETestResult(dto);
	}
	
	public static void main(String[] args) throws ParseException {
		
		SimpleDateFormat sourceSdf = new SimpleDateFormat("yyyy-MM-dd");
		String expiryDateStr = "2016-09-01";
		java.util.Date expiryDate = sourceSdf.parse(expiryDateStr);
		
		Date today = new Date(System.currentTimeMillis());
		int dateDiff = today.compareTo(expiryDate);
		
		System.out.println(dateDiff);
	}
}
