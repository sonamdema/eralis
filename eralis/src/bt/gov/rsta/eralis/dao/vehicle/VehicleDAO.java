package bt.gov.rsta.eralis.dao.vehicle;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import bt.gov.rsta.eralis.business.payment.PaymentBusiness;
import bt.gov.rsta.eralis.dto.DuplicateDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.dto.vehicle.FitnessItemDTO;
import bt.gov.rsta.eralis.dto.vehicle.FitnessMainDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.WorkflowManager;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.vo.WorkflowDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class VehicleDAO {
	private static VehicleDAO vehicleDAO;
	private static int duration = 0;
	private static int duration1 = 0;

	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date(System.currentTimeMillis());
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
	String dateString = simpleDateFormat.format(new java.util.Date());

	public static VehicleDAO getInstance() {
		if (vehicleDAO == null)
			vehicleDAO = new VehicleDAO();

		return vehicleDAO;
	}

	public synchronized String new_registration(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		String applicationNumber = null;

		try {
			pst = conn.prepareStatement(CONTROL_MECHANISM);
			pst.setString(1, dto.getCustomerId());
			pst.setString(2, dto.getEngineNumber());
			pst.setString(3, dto.getChasisNumber());
			pst.setString(4, dto.getVehicleType());
			rs = pst.executeQuery();
			rs.first();

			if (rs.getString("rowCount").equalsIgnoreCase("0")) {
				pst = conn.prepareStatement(CHECK_IF_ALREADY_APPLIED);
				pst.setString(1, dto.getCustomerId());
				pst.setString(2, dto.getEngineNumber());
				pst.setString(3, dto.getChasisNumber());
				pst.setString(4, dto.getVehicleType());
				rs = pst.executeQuery();
				rs.first();

				if (rs.getString("rowCount").equalsIgnoreCase("0")) {
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_vehicle_application", dto.getPageId(),null, conn);
					// applicationNumber =
					// EralisCommonUtil.generateApplicationNumberFormat("t_vehicle_application",
					// dto.getPageId(), conn);
					String serviceid = EralisCommonUtil.getServiceIdAsString(
							dto.getPageId(), conn);

					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION);
					pst.setString(1, applicationNumber);
					pst.setString(2, "NEW");
					pst.setString(3, dto.getVehicleRegistrationType());
					pst.setString(4, dto.getCustomerId());
					pst.setString(5, dto.getRegistrationType());
					pst.setString(6, dto.getVehicleRegistrationId());
					pst.setString(7, dto.getRegion());
					pst.setString(8, dto.getVehicleType());
					pst.setString(9, dto.getVanityNumber());
					pst.setString(10, dto.getVehicleCompany());
					pst.setString(11, dto.getVehicleModel());
					pst.setString(12, dto.getEngineType());
					pst.setString(13, dto.getEngineNumber());
					pst.setString(14, dto.getChasisNumber());
					pst.setString(15, dto.getStatus());
					pst.setString(16, dto.getColour());
					pst.setString(17, dto.getPrice());
					pst.setString(18, dto.getLoadCapacity());
					pst.setString(19, dto.getDealersName());
					pst.setString(20, dto.getSeatCapacity());
					String vehicleHorsePower;
					if (dto.getVehicleHorsePower().equalsIgnoreCase("")) {
						vehicleHorsePower = "0.00";
					} else {
						vehicleHorsePower = dto.getVehicleHorsePower();
					}
					pst.setString(21, vehicleHorsePower);
					String vehilceKiloWatt;
					if (dto.getVehicleKiloWatt().equalsIgnoreCase("")) {
						vehilceKiloWatt = "0.00";
					} else {
						vehilceKiloWatt = dto.getVehicleKiloWatt();
					}
					pst.setString(22, vehilceKiloWatt);
					pst.setString(23, dto.getUnladenWeight());
					java.util.Date purchaseDate = sourceSdf.parse(dto.getPurchaseDate());
					String purchaseDate1 = targetSdf.format(purchaseDate);
					pst.setString(24, purchaseDate1);
					pst.setString(25, dto.getManufactureYear());
					pst.setString(26, dto.getEngineCC());
					pst.setString(27, dto.getManufactureCountry());
					pst.setString(28, dto.getPurchaseType());
					pst.setString(29, dto.getRemarks());
					pst.setString(30, userVo.getActorId());
					pst.setString(31, userVo.getActorId());
					pst.setString(32, dto.getRenewalDuration());
					java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr1 = targetSdf.format(receiptDate1);
					pst.setString(33, dto.getReceiptNo());
					pst.setString(34, receiptDateStr1);
					pst.setString(35, dto.getBusType());
					pst.setString(36, dto.getWheelNos());
					pst.setString(37, dto.getNumberAxle());
					pst.setString(38, dto.getQuotaMobileNumber());
					pst.setString(39, dto.getOwnerMobileNumber());
					pst.setString(40, dto.getDiplomatCode());

					int count = pst.executeUpdate();

					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);

						/*
						 * Code to attachments
						 */
						DocumentVO documentVO = new DocumentVO();

						if (null != dto.getInvoice()
								&& dto.getInvoice().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto.getInvoice()
									.getFileData());
							documentVO.setName(dto.getInvoice().getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getChallan()
								&& dto.getChallan().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto.getChallan()
									.getFileData());
							documentVO.setName(dto.getChallan().getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getLetterOfAuthenticity()
								&& dto.getLetterOfAuthenticity().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto
									.getLetterOfAuthenticity().getFileData());
							documentVO.setName(dto.getLetterOfAuthenticity()
									.getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getEmission()
								&& dto.getEmission().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto.getEmission()
									.getFileData());
							documentVO.setName(dto.getEmission().getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getExemptionCertificate()
								&& dto.getExemptionCertificate().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto
									.getExemptionCertificate().getFileData());
							documentVO.setName(dto.getExemptionCertificate()
									.getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getExciseInvoice()
								&& dto.getExciseInvoice().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto.getExciseInvoice()
									.getFileData());
							documentVO.setName(dto.getExciseInvoice()
									.getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getCustomDeclaration()
								&& dto.getCustomDeclaration().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto
									.getCustomDeclaration().getFileData());
							documentVO.setName(dto.getCustomDeclaration()
									.getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						if (null != dto.getVehiclePicture()
								&& dto.getVehiclePicture().getFileSize() > 0) {
							documentVO.setServiceName("VEHICLE_REGISTRATION");
							documentVO.setFileContent(dto.getVehiclePicture()
									.getFileData());
							documentVO.setName(dto.getVehiclePicture()
									.getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,
									applicationNumber);
						}

						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNumber);
						paymentDTO.setServiceId(serviceid);
						paymentDTO.setApplicationType("NEW");
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setPenalty(dto.getPenalty());
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						java.util.Date receiptDate = sourceSdf.parse(dto
								.getReceiptDate());
						String receiptDateStr = targetSdf.format(receiptDate);
						paymentDTO.setReceiptDate(receiptDateStr);
						result = CommonDAO.getInstance().insertPaymentDetails(
								paymentDTO, conn);

						//result = "SUCCESS";
					}
				} else
					result = "ALREADY_APPLIED";
			} else {
				result = "DUPLICATE";
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[new_registration]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result + "#" + applicationNumber;
	}

	
	public synchronized String saveInspectionDtls(VehicleDTO dto,UserDetailsVO userVo, Connection conn) throws ERALISException,ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		try {
				int count = 1;
				pst = conn.prepareStatement(INSERT_PASSENGER_BUS_INSPECTION);
				pst.setString(count++ , dto.getVehicleId());
				pst.setString(count++ , dto.getDrivinglicenseId());
				pst.setString(count++ , dto.getInspectionType());
				pst.setString(count++ , dto.getIsDrug());
				pst.setString(count++ , dto.getAlcoholTest());
				pst.setString(count++ , dto.getDepartureFrom());
				pst.setString(count++ , dto.getDestination());
				pst.setString(count++ , dto.getDepartureTime());
				if(null!=dto.getDepartureDate() && !dto.getDepartureDate().equalsIgnoreCase(""))
				{
					java.util.Date departureDate = sourceSdf.parse(dto.getDepartureDate());
					String departureDateStr = targetSdf.format(departureDate);
					pst.setString(count++ , departureDateStr);
				}
				else
				{
					pst.setString(count++ , "0000-00-00");
				}
				pst.setString(count++ , dto.getArrivalTime());

				if(null!=dto.getArrivalDate() && !dto.getArrivalDate	().equalsIgnoreCase(""))
				{
					java.util.Date arrivalDate = sourceSdf.parse(dto.getArrivalDate());
					String arrivalDateStr = targetSdf.format(arrivalDate);
					pst.setString(count++ , arrivalDateStr);
				}
				else
				{
					pst.setString(count++ , "0000-00-00");
				}
				
				pst.setString(count++ , dto.getNoMale());
				pst.setString(count++ , dto.getNoFemale());
				pst.setString(count++ , dto.getNoChildren());
				pst.setString(count++ , userVo.getActorId());

				pst.setString(count++ , userVo.getJurisdictionTypeId());
				pst.setString(count++ , userVo.getJurisdictionId());
				count = pst.executeUpdate();
				if (count > 0) {
					
					result = "SUCCESS";
					
				}
				

		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[saveInspectionDtls]:: "+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}

	public synchronized String new_renewal(VehicleDTO dto,UserDetailsVO userVo, Connection conn) throws ERALISException,ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		String applicationNumber = null;
		//String bfsOrderNo = null;
		try {
			DuplicateDTO dupDTO = new DuplicateDTO();
			dupDTO.setVehicleId(dto.getVehicleId());
			dupDTO.setRequestType("VEHICLE");
			dupDTO.setServiceType("RENEWAL");

			String isDuplicate = null;
			String expiryDate = null;
			if (dto.getBfsNo() != null) {
				Connection conn1 = ConnectionManager.getConnection();
				isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn1);
				if (isDuplicate.equals("N")) {
					pst = conn1.prepareStatement(LAST_EXPIRY_DATE);
					pst.setString(1, dto.getVehicleId());
					pst.setString(2, dto.getVehicleId());
					pst.setString(3, dto.getVehicleId());
					rs = pst.executeQuery();
					rs.first();
					expiryDate = rs.getString("expiryDate");
				}
				ConnectionManager.close(conn1);
			} else {
				isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				if (isDuplicate.equals("N")) {
					pst = conn.prepareStatement(LAST_EXPIRY_DATE);
					pst.setString(1, dto.getVehicleId());
					pst.setString(2, dto.getVehicleId());
					pst.setString(3, dto.getVehicleId());
					rs = pst.executeQuery();
					rs.first();
					expiryDate = rs.getString("expiryDate");
				}
			}

			if (isDuplicate.equals("N")) {
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_vehicle_application", dto.getPageId(),dto.getBfsNo(), conn);
				String serviceid = EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);

				
				pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_RENEWAL);
				pst.setString(1, applicationNumber);
				pst.setString(2, "RENEWAL");
				pst.setString(3, dto.getVehicleRegistrationType());
				pst.setString(4, dto.getCustomerId());
				pst.setString(5, dto.getVehicleNumber());
				pst.setString(6, dto.getRemarks());
				pst.setString(7, userVo.getActorId());
				pst.setString(8, userVo.getActorId());
				pst.setString(9, dto.getVehicleId());
				pst.setString(10, dto.getRenewalDuration());
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDateStr = targetSdf.format(receiptDate);
				pst.setString(11, dto.getReceiptNo());
				pst.setString(12, receiptDateStr);
				int count = pst.executeUpdate();

				pst = conn
						.prepareStatement(UPDATE_T_VEHICLE_APPLICATION_WITH_EXPIRY_DATE);
				pst.setString(1, expiryDate);
				pst.setString(2, applicationNumber);
				count = pst.executeUpdate();

				if (count > 0) {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);

					/*
					 * Code to attachments
					 */

						if (null != dto.getSupportingDocument() && dto.getSupportingDocument().getFileSize() > 0) {
							DocumentVO documentVO = new DocumentVO();
							documentVO.setServiceName("VEHICLE_RENEWAL");
							documentVO.setFileContent(dto.getSupportingDocument().getFileData());
							documentVO.setName(dto.getSupportingDocument().getFileName());
							documentVO.setDocumentType("RC");
							CommonDAO.getInstance().fileUploader(documentVO,applicationNumber);
						}

					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("RENEWAL");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setPenalty(dto.getPenalty());
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr1 = targetSdf.format(receiptDate1);
					paymentDTO.setReceiptDate(receiptDateStr1);
					result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
				}    
 
				//result = "SUCCESS"; 
			} else
				result = "VEHICLE_DUPLICATE_TRANSACTION";

			// if(null != bfsOrderNo)
			if (result.equalsIgnoreCase("VEHICLE_DUPLICATE_TRANSACTION")) {
				String[] isDuplicateArray = isDuplicate.split("#");
				String receiptNo = isDuplicateArray[1];
				applicationNumber = isDuplicateArray[2];
				applicationNumber = applicationNumber + "#" + receiptNo;
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_renewal]:: "
					+ e);

		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result + "#" + applicationNumber + "#";
	}
	
	public synchronized String online_fitness_payment(String applicationNo,String amount,String penalty, Connection conn) throws ERALISException,ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try {

			PaymentDTO paymentDTO = new PaymentDTO();
			paymentDTO.setApplicationNo(applicationNo);
			paymentDTO.setAmount(amount);
			paymentDTO.setPenalty(penalty);
			result = CommonDAO.getInstance().updatePayment(paymentDTO, conn);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_renewal]:: "
					+ e);

		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public synchronized String new_transfer(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;

		try {
			DuplicateDTO dupDTO = new DuplicateDTO();
			dupDTO.setVehicleId(dto.getVehicleId());
			dupDTO.setRequestType("VEHICLE");
			dupDTO.setServiceType("TRANSFER");

			String isDuplicate = CommonDAO.getInstance()
					.checkIfDuplicateApplication(dupDTO, conn);

			if (isDuplicate.equals("N")) {
				applicationNumber = EralisCommonUtil
						.generateApplicationNumberFormat(
								"t_vehicle_application", dto.getPageId(), null,
								conn);
				String serviceid = EralisCommonUtil.getServiceIdAsString(
						dto.getPageId(), conn);

				String customerType = dto.getTransfereeCustomerId().substring(
						0, 1);

				pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_TRANSFER);
				pst.setString(1, applicationNumber);
				pst.setString(2, "TRANSFER");
				pst.setString(3, dto.getVehicleRegistrationType());
				pst.setString(4, dto.getVehicleNumber());
				pst.setString(5, dto.getTransferorCustomerId());
				java.util.Date saleDeedDate = sourceSdf.parse(dto
						.getSaleDeedDate());
				String saleDeedDate1 = targetSdf.format(saleDeedDate);
				pst.setString(6, saleDeedDate1);
				pst.setString(7, customerType);
				pst.setString(8, dto.getTransfereeCustomerId());
				pst.setString(9, dto.getSaleDeedAmount());
				pst.setString(10, dto.getRemarks());
				pst.setString(11, userVo.getActorId());
				pst.setString(12, userVo.getActorId());
				pst.setString(13, dto.getVehicleId());
				java.util.Date receiptDate1 = sourceSdf.parse(dto
						.getReceiptDate());
				String receiptDateStr1 = targetSdf.format(receiptDate1);
				pst.setString(14, dto.getReceiptNo());
				pst.setString(15, receiptDateStr1);
				pst.setString(16, dto.getTransferType());
				int count = pst.executeUpdate();

				if (count > 0) {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);
					/*
					 * Code to attachments
					 */
					if (null != dto.getSupportingDocument() && dto.getSupportingDocument().getFileSize() > 0) {
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("VEHICLE_TRANSFER");
						documentVO.setFileContent(dto.getSupportingDocument()
								.getFileData());
						documentVO.setName(dto.getSupportingDocument()
								.getFileName());
						documentVO.setDocumentType("RC");
						CommonDAO.getInstance().fileUploader(documentVO, 
								applicationNumber);
					}  

					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("TRANSFER");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setPenalty(dto.getPenalty());
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receiptDate = sourceSdf.parse(dto
							.getReceiptDate());
					String receiptDateStr = targetSdf.format(receiptDate);
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(
							paymentDTO, conn);

					//result = "SUCCESS";
				}
			} else
				result = "VEHICLE_DUPLICATE_TRANSACTION";
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_transfer]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result + "#" + applicationNumber;
	}
	
	

	public synchronized String fitnessReplacement(VehicleDTO dto,UserDetailsVO userVo, Connection conn) throws ERALISException,ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		try {
			
			DuplicateDTO dupDTO = new DuplicateDTO();
			dupDTO.setVehicleId(dto.getVehicleId());
			dupDTO.setRequestType("VEHICLE");
			dupDTO.setServiceType("RWC_REPLACEMENT");
			String isDuplicate = null;
			isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);

			if (isDuplicate.equals("N")) {
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_vehicle_application", dto.getPageId(),dto.getBfsNo(), conn);
				String serviceid = EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);

				String receiptDateStr = "0000-00-00";
				if (dto.getReceiptDate() != null) {
					java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
					receiptDateStr = targetSdf.format(receiptDate1);
				}
				pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_DUPLICATION);
				pst.setString(1, applicationNumber);
				pst.setString(2, "RWC_REPLACEMENT");
				pst.setString(3, dto.getVehicleRegistrationType());
				pst.setString(4, dto.getCustomerId());
				pst.setString(5, dto.getVehicleNumber());
				pst.setString(6, dto.getRemarks());
				pst.setString(7, userVo.getActorId());
				pst.setString(8, userVo.getActorId());
				pst.setString(9, dto.getVehicleId());
				pst.setString(10, dto.getReceiptNo());
				pst.setString(11, receiptDateStr);
				int count = pst.executeUpdate();
				if (count > 0) {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);

					/*
					 * Code to attachments
					 */
					if (null != dto.getSupportingDocument() && dto.getSupportingDocument().getFileSize()>0) {
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("RWC_REPLACEMENT");
						documentVO.setFileContent(dto.getSupportingDocument().getFileData());
						documentVO.setName(dto.getSupportingDocument().getFileName());
						documentVO.setDocumentType("RC");
						CommonDAO.getInstance().fileUploader(documentVO,applicationNumber);
					}

					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("RWC_REPLACEMENT");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(
							paymentDTO, conn);
				}
				//result = "SUCCESS";
			}
			else
			{
				String[] isDuplicateArray = isDuplicate.split("#");
				applicationNumber = isDuplicateArray[2];
				result = "VEHICLE_DUPLICATE_TRANSACTION";
			}

		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[new_duplication]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result + "#" + applicationNumber;
	}

	
	public synchronized String new_duplication(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		String bfsOrderNo = null;
		try {
			DuplicateDTO dupDTO = new DuplicateDTO();
			dupDTO.setVehicleId(dto.getVehicleId());
			dupDTO.setRequestType("VEHICLE");
			dupDTO.setServiceType("DUPLICATION");
			String isDuplicate = null;
			if (dto.getBfsNo() != null) {
				Connection conn1 = ConnectionManager.getConnection();
				isDuplicate = CommonDAO.getInstance()
						.checkIfDuplicateApplication(dupDTO, conn1);
				ConnectionManager.close(conn1);
			} else {
				isDuplicate = CommonDAO.getInstance()
						.checkIfDuplicateApplication(dupDTO, conn);
			}

			if (isDuplicate.equals("N")) {
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_vehicle_application", dto.getPageId(),dto.getBfsNo(), conn);
				String serviceid = EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);

				if (null != dto.getBfsNo()) {
					bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(dto.getService(),applicationNumber, dto.getServiceName());

					if (null == bfsOrderNo || bfsOrderNo.equalsIgnoreCase("")) {
						return "BFS_ORDER_NO_FAILURE" + "#" + null + "#" + null;
					}
				}

				String receiptDateStr = "0000-00-00";
				if (dto.getReceiptDate() != null) {
					java.util.Date receiptDate1 = sourceSdf.parse(dto
							.getReceiptDate());
					receiptDateStr = targetSdf.format(receiptDate1);
				}
				pst = conn
						.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_DUPLICATION);
				pst.setString(1, applicationNumber);
				pst.setString(2, "DUPLICATION");
				pst.setString(3, dto.getVehicleRegistrationType());
				pst.setString(4, dto.getCustomerId());
				pst.setString(5, dto.getVehicleNumber());
				pst.setString(6, dto.getRemarks());
				pst.setString(7, userVo.getActorId());
				pst.setString(8, userVo.getActorId());
				pst.setString(9, dto.getVehicleId());
				pst.setString(10, dto.getReceiptNo());
				pst.setString(11, receiptDateStr);
				int count = pst.executeUpdate();
				if (count > 0) {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);

					/*
					 * Code to attachments
					 */
					if (null != dto.getSupportingDocument()
							&& dto.getSupportingDocument().getFileSize() > 0) {
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("VEHICLE_DUPLICATION");
						documentVO.setFileContent(dto.getSupportingDocument()
								.getFileData());
						documentVO.setName(dto.getSupportingDocument()
								.getFileName());
						documentVO.setDocumentType("RC");
						CommonDAO.getInstance().fileUploader(documentVO,
								applicationNumber);
					}

					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("DUPLICATION");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(
							paymentDTO, conn);
				}
				//result = "SUCCESS";
			} else {
				result = "VEHICLE_DUPLICATE_TRANSACTION";
			}

			// if(null!=dto.getBfsNo())
			if (result.equalsIgnoreCase("VEHICLE_DUPLICATE_TRANSACTION")) {
				String[] isDuplicateArray = isDuplicate.split("#");
				String receiptNo = isDuplicateArray[1];
				applicationNumber = isDuplicateArray[2];
				bfsOrderNo = receiptNo;
				applicationNumber = applicationNumber + "#" + bfsOrderNo;
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[new_duplication]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result + "#" + applicationNumber + "#" + bfsOrderNo;
	}

	
	public String new_cancellation(VehicleDTO dto, UserDetailsVO userVo,
			Connection conn) throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		String cancellationId = "";

		try {
			pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_CANCELLATION,PreparedStatement.RETURN_GENERATED_KEYS);
			pst.setString(1, dto.getVehicleId());
			pst.setString(2, dto.getCustomerId());
			java.util.Date CancellationDate = sourceSdf.parse(dto
					.getCancellationDate());
			String CancellationDate1 = targetSdf.format(CancellationDate);
			pst.setString(3, CancellationDate1);
			pst.setString(4, dto.getCancellationReason());
			pst.setString(5, dto.getReceiptNo());
			java.util.Date ReceiptDate = sourceSdf.parse(dto.getReceiptDate());
			String ReceiptDate1 = targetSdf.format(ReceiptDate);
			pst.setString(6, ReceiptDate1);
			pst.setString(7, userVo.getActorId());
			pst.setString(8, userVo.getActorId());
			pst.setString(9, userVo.getRegionId());
			pst.setString(10, userVo.getBaseId());

			int count = pst.executeUpdate();

			if (count > 0) {
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					cancellationId = "VEH_CAN_" + rs.getString(1);
				}
				DocumentVO documentVO = new DocumentVO();
				documentVO.setServiceName("VEHICLE_CANCELLATION");
				documentVO.setFileContent(dto.getImgPath().getFileData());
				documentVO.setName(dto.getImgPath().getFileName());
				documentVO.setDocumentType("VEHICLE_CANCELLATION");
				CommonDAO.getInstance()
						.fileUploader(documentVO, cancellationId);

				result = "SUCCESS";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[new_cancellation]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public String withdraw_veh_cancellation(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		String withdrawId = "";

		try {
			pst = conn.prepareStatement(
					INSERT_INTO_T_VEHICLE_CANCELLATION_AUDIT,PreparedStatement.RETURN_GENERATED_KEYS);
			java.util.Date Cancellationdate = sourceSdf.parse(dto
					.getWithdrawnDate());
			String cancellationdate = targetSdf.format(Cancellationdate);

			pst.setString(1, cancellationdate);
			pst.setString(2, dto.getWithdrawnReason());
			pst.setString(3, dto.getVehicleId());
			int count = pst.executeUpdate();
			if (count > 0) {
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					withdrawId = "VEH_WITHDRAW_" + rs.getString(1);
				}
				DocumentVO documentVO = new DocumentVO();
				documentVO.setServiceName("VEH_WITHDRAW");
				documentVO.setFileContent(dto.getImgPath().getFileData());
				documentVO.setName(dto.getImgPath().getFileName());
				documentVO.setDocumentType("VEH_WITHDRAW");
				CommonDAO.getInstance().fileUploader(documentVO, withdrawId);

				pst = conn
						.prepareStatement(DELETE_FROM_T_VEHICLE_CANCEALLTION_DTLS);
				pst.setString(1, dto.getVehicleId());
				int count1 = pst.executeUpdate();
				if (count1 > 0) {
					result = "SUCCESS";
				}
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[new_cancellation]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public synchronized String new_conversion(VehicleDTO dto,UserDetailsVO userVo, Connection conn) throws ERALISException,ERALISSystemException 
	{
		PreparedStatement pst = null;
	    String result = "FAILURE";
	    ResultSet rs = null;
	    String applicationNumber = null;

        try 
        {
    	  	DuplicateDTO dupDTO = new DuplicateDTO();
			dupDTO.setVehicleId(dto.getVehicleId());
			dupDTO.setRequestType("VEHICLE");
	  		dupDTO.setServiceType("CONVERSION");
	  
	  		String isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
  		  
  		  	if(isDuplicate.equals("N"))
  		  	{
  		  		applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_vehicle_application", dto.getPageId(),null, conn);
  		  		String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);  
      	  
  		  		pst = conn.prepareStatement(GET_REGISTRATION_TYPE);
  		  		rs = pst.executeQuery();
  		  		rs.first();
  		  		String registrationType =	rs.getString("Registration_Type_Id");
  		  
	      	  	pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_CONVERSION);
	      	  	pst.setString(1, applicationNumber);
	      	  	pst.setString(2, "CONVERSION");
	      	  	pst.setString(3, dto.getVehicleRegistrationType());
	      	  	pst.setString(4, registrationType);
	      	  	pst.setString(5, dto.getCustomerId());
	      	  	pst.setString(6, dto.getVehicleNumber());
	      	  	pst.setString(7, dto.getConversionReason());
	      	  	pst.setString(8, dto.getVehicleRegistrationId());
	      	  	pst.setString(9, dto.getRegion());
	      	  	pst.setString(10, dto.getVehicleType());
	      	  	pst.setString(11, dto.getVehiclePrefix());
	      	  	pst.setString(12, dto.getRemarks());
	      	  	pst.setString(13, userVo.getActorId());
	      	  	pst.setString(14, userVo.getActorId());
	      	  	pst.setString(15, dto.getVehicleId());
	      	  	java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
	      	  	String receiptDateStr1 = targetSdf.format(receiptDate1);
	      	  	pst.setString(16, dto.getReceiptNo());
	      	  	pst.setString(17, receiptDateStr1);
	      	  	int count = pst.executeUpdate();
		      
			  if(count > 0)
			  {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);
					
					/*
						 * Code to attachments
						 */
					if(null != dto.getSupportingDocument() && dto.getSupportingDocument().getFileSize()> 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("VEHICLE_CONVERSION");
						documentVO.setFileContent(dto.getSupportingDocument().getFileData());
						documentVO.setName(dto.getSupportingDocument().getFileName());
						documentVO.setDocumentType("RC");
						CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
					}
						
					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("CONVERSION");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr = targetSdf.format(receiptDate);
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
				}
			  
			  //result = "SUCCESS";
  		  }
  		  else
		  	result = "VEHICLE_DUPLICATE_TRANSACTION";
        } 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_conversion]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+"#"+applicationNumber;
	}

	public String new_fitness(VehicleDTO dto, String checkedVals,UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;

		try {
			
			
			pst = conn.prepareStatement(COUNT_VEHICLE_FITNESS_NA);
      	  	pst.setString(1, dto.getVehicleId());
	  		rs = pst.executeQuery();
	  		rs.first();
			if(rs.getInt("rowCount")==0)
			{
				pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_APPLICATION_FITNESS,PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, dto.getCustomerId());
				pst.setString(2, dto.getVehicleId());
				java.util.Date testDate = sourceSdf.parse(dto.getTestedOn());
				String testDateStr = targetSdf.format(testDate);
				pst.setString(3, testDateStr);
				java.util.Date validUntilDate = sourceSdf
						.parse(dto.getValidUntil());
				String validUntilStr = targetSdf.format(validUntilDate); 
				pst.setString(4, validUntilStr);
				pst.setString(5, dto.getRemarks());
				pst.setString(6, userVo.getActorId());
				pst.setDate(7, date);
	
				String receiptDateStr = null;
				if (!dto.getReceiptDate().equals("00-00-0000")) {
					java.util.Date receiptDate = sourceSdf.parse(dto
							.getReceiptDate());
					receiptDateStr = targetSdf.format(receiptDate);
				}
	
				pst.setString(8, dto.getReceiptNo());
				pst.setString(9, receiptDateStr);
				pst.setString(10, userVo.getRegionId());
				pst.setString(11, userVo.getBaseId());
				pst.setString(12, dto.getInspectedBy());
				
				int count = pst.executeUpdate();
				String fitnessId = "0";
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					fitnessId = rs.getString(1);
				}
				if (count > 0) {
					String[] itemArray = checkedVals.split("#");
					for (int i = 0; i < itemArray.length; i++) {
						String[] itemArray1 = itemArray[i].split("~");
						pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_FITNESS_ITEM_DTLS);
						pst.setString(1, dto.getVehicleId());
						pst.setString(2, itemArray1[0]);
						pst.setString(3, itemArray1[1]);
						count = pst.executeUpdate();
					}
	
					if (count > 0) {
						String applicationNo = fitnessId;//"PN116"+fitnessId;
						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNo);
						paymentDTO.setServiceId("116");
						//paymentDTO.setPenalty(dto.getPenalty());
						paymentDTO.setApplicationType("FITNESS"); 
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						String receiptDateStr1 = null;
						if(!dto.getReceiptDate().equals("00-00-0000")){
							java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
							receiptDateStr1 = targetSdf.format(receiptDate1);
						}
						paymentDTO.setReceiptDate(receiptDateStr1);
						result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
						
						//result = "SUCCESS";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_fitness]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public String new_emission(VehicleDTO dto, UserDetailsVO userVo,
			Connection conn) throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		String emissionId = null;

		try {
			pst = conn.prepareStatement(UPDATE_EMISSION_RETESTED);
			pst.setString(1, dto.getVehicleId());
			pst.executeUpdate();

			pst = conn.prepareStatement(
					INSERT_INTO_T_VEHICLE_APPLICATION_EMISSION,
					PreparedStatement.RETURN_GENERATED_KEYS);
			pst.setString(1, dto.getVehicleId());
			java.util.Date TestDate = sourceSdf.parse(dto.getTestedOn());
			String TestDate1 = targetSdf.format(TestDate);
			pst.setString(2, TestDate1);
			if ("FAIL".equalsIgnoreCase(dto.getTestResult())) {
				pst.setString(3, null);
			} else {
				java.util.Date ValidUntil = sourceSdf
						.parse(dto.getValidUntil());
				String ValidUntil1 = targetSdf.format(ValidUntil);
				pst.setString(3, ValidUntil1);
			}
			pst.setString(4, dto.getTestLocation());
			if (dto.getPersonalRadio().equals("P"))
				pst.setString(5, "P");
			else
				pst.setString(5, "D");
			pst.setString(6, dto.getcO());
			pst.setString(7, dto.getT1());
			pst.setString(8, dto.getT2());
			pst.setString(9, dto.getT3());
			pst.setString(10, dto.gethSU());
			pst.setString(11, dto.getTestResult());
			pst.setString(12, userVo.getActorId());
			pst.setDate(13, date);
			pst.setString(14, userVo.getActorId());
			pst.setDate(15, date);
			pst.setString(16, dto.getAmount());
			if ("FAIL".equalsIgnoreCase(dto.getTestResult())) {
				pst.setString(17, "N");
			} else {
				pst.setString(17, "Y");
			}
			int count = pst.executeUpdate();

			rs = pst.getGeneratedKeys();
			while (rs.next()) {
				emissionId = rs.getString(1);
			}

			if (count > 0)
				result = "SUCCESS";
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_emission]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result + '#' + emissionId;
	}

	public String new_post(VehicleDTO dto, Connection conn)
			throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			pst = conn.prepareStatement(INSERT_NEW_POST_STATUS);
			pst.setString(2, dto.getPostedDate());
			int count = pst.executeUpdate();

			if (count > 0)
				result = "SUCCESS";
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_post]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public String new_print(VehicleDTO dto, Connection conn)
			throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			pst = conn.prepareStatement(INSERT_NEW_PRINT_STATUS);
			pst.setString(2, dto.getPrintDate());
			int count = pst.executeUpdate();

			if (count > 0)
				result = "SUCCESS";
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleDAO[new_print]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	// approved vehicle
	public synchronized NotificationDTO registration_application_approval(VehicleDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null, pst1 = null;
		notityDTO.setStatus("FAILURE");
		ResultSet rs = null;

		try {
			if (conn != null) {
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					
				
					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_REGISTRATION_DTLS);
					pst.setString(1, userVo.getActorId());
					pst.setString(2, userVo.getRegionId());
					pst.setString(3, userVo.getBaseId());
					pst.setString(4, dto.getApplicationNumber());
					int count = pst.executeUpdate();
	
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("109");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(dto.getRegion());
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
					}
	
					EralisCommonDTO commonDTO = new EralisCommonDTO();
					pst = conn.prepareStatement(GET_VECHILE_NUMBER);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					dto.setVehicleRegistrationId(rs.getString("Vehicle_Registration_Id"));
					dto.setRegion(rs.getString("Region_Id"));
					dto.setVehiclePrefix(rs.getString("Vehicle_Prefix"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Id"));
					dto.setDiplomatCode(rs.getString("Diplomat_Code"));
					dto.setSl_No(rs.getString("Sl_No"));
	
					int slNo = rs.getInt("Sl_No");
					
					if (dto.getVehicleRegistrationId().equalsIgnoreCase("6")) {
						int newSlNo = slNo + 1;
						String newVehicleNo = dto.getDiplomatCode() + "CD-"+ newSlNo;
						commonDTO.setLatestVehicleNumber(newVehicleNo);
						pst = conn.prepareStatement(UPDATE_DIPLOMAT_NUMBER_SEQUENCE);
						pst.setInt(1, newSlNo);
						pst.setString(2, "6");
						pst.setString(3, dto.getDiplomatCode());
						pst.executeUpdate();
	
					}
					else if (dto.getVehicleRegistrationId().equalsIgnoreCase("3") 	|| dto.getVehicleRegistrationId().equalsIgnoreCase("7"))
					{
						String newVehicleNo = null;
						int newSlNo = slNo + 1;
						if (dto.getVehicleRegistrationId().equalsIgnoreCase("3")) {
							newVehicleNo = "BHT " + newSlNo;
						} else if (dto.getVehicleRegistrationId().equalsIgnoreCase("7")) {
							newVehicleNo = "BHUTAN " + newSlNo;
						}
						commonDTO.setLatestVehicleNumber(newVehicleNo);
						pst = conn.prepareStatement(UPDATE_BTH_AND_BHUTAN_NUMBER_SEQUENCE);
						pst.setInt(1, newSlNo);
						pst.setString(2, dto.getVehicleRegistrationId());
						pst.executeUpdate();
					}
					else {
						commonDTO = EralisCommonUtil.generateVehicleNumberFormat(
								conn, dto.getVehicleRegistrationId(),
								dto.getRegion(), dto.getVehicleType());
	
						pst = conn.prepareStatement(UPDATE_NUMBER_SEQUENCE);
						pst.setString(1, commonDTO.getNewVehicleNo());
						pst.setString(2, commonDTO.getvPrefix());
						pst.setString(3, dto.getVehicleRegistrationId());
						pst.setString(4, dto.getVehicleType());
						pst.setString(5, dto.getRegion());
						pst.executeUpdate();
					}
	
					pst1 = conn.prepareStatement(UPDATE_VEHICLE_NUMBER);
					pst1.setString(1, commonDTO.getLatestVehicleNumber());
					pst1.setString(2, dto.getApplicationNumber());
					pst1.executeUpdate();
					
					/*
					 * String query =
					 * "UPDATE t_vehicle_registration_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=? WHERE a.`Application_Number`=?"
					 * ; pst = conn.prepareStatement(query); pst.setString(1,
					 * userVo.getRegionId()); pst.setString(2, userVo.getBaseId());
					 * pst.setString(3, dto.getApplicationNumber());
					 * pst.executeUpdate();
					 */
	
					pst = conn.prepareStatement(GET_VALIDITY_DURATION);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					int validityDuration = rs.getInt("Renewal_Duration");
	
					pst = conn.prepareStatement(GET_RECEIPT_DATE);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					DateTimeFormatter format = DateTimeFormat
							.forPattern("yyyy-MM-dd");
					DateTime receiptDate1 = format.parseDateTime(rs
							.getString("Receipt_Date"));
					DateTime a = receiptDate1.plusMonths(validityDuration);
					String p = a.toString();
					java.util.Date nextExpiry = targetSdf.parse(p);
					String nextexpiry = targetSdf.format(nextExpiry);
	
					pst = conn.prepareStatement(UPDATE_DATES);
					pst.setString(1, rs.getString("Receipt_Date"));
					pst.setString(2, nextexpiry);
					pst.setDate(3, date);
					pst.setString(4, dto.getApplicationNumber());
					count = pst.executeUpdate();
	
					pst = conn.prepareStatement(GET_REGISTRATION_DETAILS);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					dto.setCustomerId(dto.getCustomerId());
					String Customer_Id = rs.getString("Customer_Id").substring(0, 1);
					
	
					insertIntoPrintList(rs.getString("Vehicle_Reg_Dtls_Id"),dto.getApplicationNumber(),"109", userVo,conn);
	
					if (Customer_Id.equals("P")) {
						pst = conn
								.prepareStatement(GET_PERSONAL_REGISTRATION_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO.setCustomerName(rs.getString("pname"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setEmailAddress(rs.getString("Present_Email"));
						notityDTO.setMobileNumber(rs.getString("Present_Phone_No"));
						notityDTO.setVehicleNo(rs.getString("Vehicle_Number"));
	
						notityDTO.setStatus("SUCCESS");
					} else {
						pst = conn
								.prepareStatement(GET_ORGANISATION_REGISTRATION_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO
								.setCustomerName(rs.getString("Organization_Name"));
						notityDTO.setEmailAddress(rs.getString("Email_Id"));
						notityDTO.setMobileNumber(rs.getString("Phone_Number"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setVehicleNo(rs.getString("Vehicle_Number"));
	
						notityDTO.setStatus("SUCCESS");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[registration_application_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
			ConnectionManager.close(null, null, null, pst1);
		}

		return notityDTO;
	}

	public synchronized NotificationDTO renewal_application_approval(VehicleDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		notityDTO.setStatus("FAILURE");
		ResultSet rs = null;

		try {
			if (conn != null) {
				
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_RENEWAL_DTLS);
					pst.setString(1, userVo.getActorId());
					pst.setString(2, dto.getApplicationNumber());
	
					int count = pst.executeUpdate();
	
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("110");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
						new WorkflowManager(conn).logCompletedWorkflow(vo);
					}
	
					String query1 = "UPDATE t_vehicle_renewal_dtls a SET a.`Dispatched_By`=?, a.`Dispatched_Date`=? WHERE a.`Application_Number`=?";
					pst = conn.prepareStatement(query1);
					pst.setString(1, userVo.getActorId());
					pst.setDate(2, date);
					pst.setString(3, dto.getApplicationNumber());
					count = pst.executeUpdate();
	
					String query = "UPDATE t_vehicle_renewal_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=? WHERE a.`Application_Number`=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, userVo.getRegionId());
					pst.setString(2, userVo.getBaseId());
					pst.setString(3, dto.getApplicationNumber());
					pst.executeUpdate();
	
					pst = conn.prepareStatement(GET_RENEWAL_DETAILS);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					dto.setCustomerId(dto.getCustomerId());
					
					insertIntoPrintList(rs.getString("Vehicle_Id"),dto.getApplicationNumber(),"110", userVo,conn);
	
					String Customer_Id = rs.getString("Customer_Id").substring(0, 1);
	
					if (Customer_Id.equals("P")) {
						pst = conn
								.prepareStatement(GET_PERSONAL_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO.setCustomerName(rs.getString("pname"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setEmailAddress(rs.getString("Present_Email"));
						notityDTO.setMobileNumber(rs.getString("Present_Phone_No"));
	
						notityDTO.setStatus("SUCCESS");
					} else {
						pst = conn.prepareStatement(GET_ORGANISATION_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO.setCustomerName(rs.getString("Organization_Name"));
						notityDTO.setEmailAddress(rs.getString("Email_Id"));
						notityDTO.setMobileNumber(rs.getString("Phone_Number"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
	
						notityDTO.setStatus("SUCCESS");
	
					}
	
					pst = conn.prepareStatement(UPDATE_EXPIRY_DATE);
					java.util.Date expiryDate = sourceSdf.parse(dto.getExpiryDate());
					String expiryDate1 = targetSdf.format(expiryDate);
					pst.setString(1, expiryDate1);
					pst.setString(2, dto.getApplicationNumber());
					pst.executeUpdate();
				}
			}
		} catch (Exception e) {
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[renewal_application_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return notityDTO;
	}

	public synchronized NotificationDTO conversion_application_approval(VehicleDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null, pst1 = null, pst2 = null;
		notityDTO.setStatus("FAILURE");
		ResultSet rs = null;
		String newVehicleId = null;

		try {
			if (conn != null) {
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					/*
					 * INSERT TRANSFERED VEHILCE DTLS INTO VEHICLE_REGISTRATION
					 */
					pst = conn.prepareStatement(INSERT_VEH_TRANSFER_DTLS_INTO_VEH_REG,PreparedStatement.RETURN_GENERATED_KEYS);
					pst.setString(1, dto.getApplicationNumber());
					int count = pst.executeUpdate();
	
					rs = pst.getGeneratedKeys();
					while (rs.next()) {
						newVehicleId = rs.getString(1);
					}
					
					insertIntoPrintList(newVehicleId,dto.getApplicationNumber(),"113", userVo,conn);
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("113");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(dto.getRegion());
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
					}
	
					pst = conn.prepareStatement(GET_VECHILE_NUMBER);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					dto.setVehicleRegistrationId(rs.getString("Vehicle_Registration_Id"));
					dto.setRegion(rs.getString("Region_Id"));
					dto.setVehiclePrefix(rs.getString("Vehicle_Prefix"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Id"));
					String receiptDate = rs.getString("Receipt_Date");
					String conversionType = rs.getString("conversionType");
	
					EralisCommonDTO commonDTO = EralisCommonUtil.generateVehicleNumberFormat(conn,
									dto.getVehicleRegistrationId(),
									userVo.getRegionId(), dto.getVehicleType());
	
					pst1 = conn.prepareStatement(UPDATE_VEHICLE_NUMBER);
					pst1.setString(1, commonDTO.getLatestVehicleNumber());
					pst1.setString(2, dto.getApplicationNumber());
					pst1.executeUpdate();
	
					pst1 = conn.prepareStatement(INSERT_INTO_CONVERSION);
					pst1.setString(1, dto.getApplicationNumber());
					pst1.setString(2, dto.getVehicleId());
					pst1.executeUpdate();
	
					pst2 = conn.prepareStatement(UPDATE_NUMBER_SEQUENCE);
					pst2.setString(1, commonDTO.getNewVehicleNo());
					pst2.setString(2, commonDTO.getvPrefix());
					pst2.setString(3, dto.getVehicleRegistrationId());
					pst2.setString(4, dto.getVehicleType());
					pst2.setString(5, userVo.getRegionId());
					pst2.executeUpdate();
	
					/*
					 * pst = conn.prepareStatement(UPDATE_CONVERSION_DATES);
					 * pst.setDate(1, date); pst.setString(2,
					 * dto.getApplicationNumber()); pst.executeUpdate();
					 */
	
					String query = "UPDATE t_vehicle_registration_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=?, a.Vehicle_Type_Id=?, `Vehicle_Conversion_Date` = ? WHERE a.`Application_Number`=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, userVo.getRegionId());
					pst.setString(2, userVo.getBaseId());
					pst.setString(3, dto.getVehicleType());
					pst.setDate(4, date);
					pst.setString(5, dto.getApplicationNumber());
					pst.executeUpdate();
	
					if (conversionType.trim().equalsIgnoreCase("Taxi to Private Conversion") || conversionType.trim().equalsIgnoreCase("Government to private Conversion")) {
						
						DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
						DateTime receipDateTime = format.parseDateTime(receiptDate);
						receipDateTime.plusYears(1);
						String expiryDate = format.print(receipDateTime.plusYears(1));
						java.util.Date expiry = targetSdf.parse(expiryDate);
						String expiryDateStr = targetSdf.format(expiry);
	
						pst = conn.prepareStatement(INSERT_INTO_RENEWAL_UPON_CONVERSION);
						pst.setString(1, dto.getApplicationNumber());
						pst.executeUpdate();
	
						pst = conn.prepareStatement(UPDATE_T_RENEWAL_DTLS_UPON_CONVERSION);
						pst.setString(1, expiryDateStr);
						pst.setString(2, userVo.getRegionId());
						pst.setString(3, userVo.getBaseId());
						pst.setString(4, newVehicleId);
						pst.setString(5, dto.getApplicationNumber());
						pst.executeUpdate();
					} else {
						pst = conn.prepareStatement(GET_VECHILE_NUMBER);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
						String oldVehicleId = rs.getString("Vehicle_Id");
	
						pst = conn.prepareStatement(UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_ID_IN_RENEWAL);
						pst.setString(1, newVehicleId);
						pst.setString(2, oldVehicleId);
						pst.executeUpdate();
	
						pst = conn
								.prepareStatement(UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_ID_IN_EMISSION_TEST);
						pst.setString(1, newVehicleId);
						pst.setString(2, oldVehicleId);
						pst.executeUpdate();
	
						pst = conn
								.prepareStatement(UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_IN_FITNESS_TEST);
						pst.setString(1, newVehicleId);
						pst.setString(2, oldVehicleId);
						pst.executeUpdate();
	
						pst = conn
								.prepareStatement(UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_IN_FITNESS_ITEM);
						pst.setString(1, newVehicleId);
						pst.setString(2, oldVehicleId);
						pst.executeUpdate();
					}
	
					pst = conn.prepareStatement(GET_REGISTRATION_DETAILS);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					dto.setCustomerId(dto.getCustomerId());
					String Customer_Id = rs.getString("Customer_Id").substring(0, 1);
					
					pst = conn
							.prepareStatement(INSERT_INTO_T_VEHICLE_CANCELLATION_DTLS);
					pst.setString(1, dto.getVehicleId());
					pst.setString(2, Customer_Id);
					pst.setDate(3, date);
					pst.setString(4, dto.getApplicationNumber());
					pst.setString(5, userVo.getActorId());
					pst.setDate(6, date);
					pst.executeUpdate();
	
					if (Customer_Id.equals("P")) {
						pst = conn
								.prepareStatement(GET_PERSONAL_REGISTRATION_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO.setCustomerName(rs.getString("pname"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setEmailAddress(rs.getString("Present_Email"));
						notityDTO.setMobileNumber(rs.getString("Present_Phone_No"));
						notityDTO.setVehicleNo(rs.getString("Vehicle_Number"));
	
						notityDTO.setStatus("SUCCESS");
					} else {
						pst = conn
								.prepareStatement(GET_ORGANISATION_REGISTRATION_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO
								.setCustomerName(rs.getString("Organization_Name"));
						notityDTO.setEmailAddress(rs.getString("Email_Id"));
						notityDTO.setMobileNumber(rs.getString("Phone_Number"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setVehicleNo(rs.getString("Vehicle_Number"));
	
						notityDTO.setStatus("SUCCESS");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[conversion_application_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, rs, pst);
			ConnectionManager.close(null, null, null, pst1);
			ConnectionManager.close(null, null, null, pst2);
		}

		return notityDTO;
	}

	public synchronized NotificationDTO rwc_duplication_application_approval(VehicleDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException {
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		notityDTO.setStatus("FAILURE");

		try {
			if (conn != null) {
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_FITNESS_TEST_REPLACEMENT);
					pst.setDate(1, date);
					pst.setString(2, userVo.getRegionId());
					pst.setString(3, userVo.getBaseId());
					pst.setString(4, dto.getApplicationNumber());
					int count = pst.executeUpdate();
	
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("121");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(dto.getRegion());
	
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
						new WorkflowManager(conn).logCompletedWorkflow(vo);
						notityDTO.setStatus("SUCCESS");
						/*
						pst = conn.prepareStatement(UPDATE_DUPLICATION_DATES);
						pst.setDate(1, date);
						pst.setString(2, dto.getApplicationNumber());
						pst.executeUpdate();
	
						String query = "UPDATE t_vehicle_duplicate_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=? WHERE a.`Application_Number`=?";
						pst = conn.prepareStatement(query);
						pst.setString(1, userVo.getRegionId());
						pst.setString(2, userVo.getBaseId());
						pst.setString(3, dto.getApplicationNumber());
						pst.executeUpdate();*/
						
					}
				}
			}
		} catch (Exception e) {
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[rwc_duplication_application_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return notityDTO;
	}


	public synchronized NotificationDTO duplication_application_approval(
			VehicleDTO dto, UserDetailsVO userVo, Connection conn)
			throws ERALISException, ERALISSystemException {
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		notityDTO.setStatus("FAILURE");
		ResultSet rs = null;

		try {
			if (conn != null) {
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_DUPLICATION_DTLS);
					pst.setString(1, dto.getApplicationNumber());
	
					int count = pst.executeUpdate();
	
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("112");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(dto.getRegion());
	
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
						pst = conn.prepareStatement(UPDATE_DUPLICATION_DATES);
	
						pst.setDate(1, date);
						pst.setString(2, dto.getApplicationNumber());
						pst.executeUpdate();
	
						String query = "UPDATE t_vehicle_duplicate_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=? WHERE a.`Application_Number`=?";
						pst = conn.prepareStatement(query);
						pst.setString(1, userVo.getRegionId());
						pst.setString(2, userVo.getBaseId());
						pst.setString(3, dto.getApplicationNumber());
						pst.executeUpdate();
	
						pst = conn.prepareStatement(GET_DUPLICATION_DETAILS);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
						dto.setCustomerId(dto.getCustomerId());
						String Customer_Id = rs.getString("Customer_Id").substring(0, 1);
						
						insertIntoPrintList(rs.getString("Vehicle_Id"),dto.getApplicationNumber(),"112", userVo,conn);
	
						if (Customer_Id.equals("P")) {
							pst = conn
									.prepareStatement(GET_PERSONAL_DUPLICATION_CUSTOMER_INFORMATION);
							pst.setString(1, dto.getApplicationNumber());
							rs = pst.executeQuery();
							rs.first();
	
							notityDTO.setApplicationNo(dto.getApplicationNumber());
							notityDTO.setCustomerName(rs.getString("pname"));
							notityDTO.setApprovalDate(rs.getString("currentDate"));
							notityDTO
									.setEmailAddress(rs.getString("Present_Email"));
							notityDTO.setMobileNumber(rs
									.getString("Present_Phone_No"));
							notityDTO.setStatus("SUCCESS");
						} else {
							pst = conn
									.prepareStatement(GET_ORGANISATION_DUPLICATION_CUSTOMER_INFORMATION);
							pst.setString(1, dto.getApplicationNumber());
							rs = pst.executeQuery();
							rs.first();
	
							notityDTO.setApplicationNo(dto.getApplicationNumber());
							notityDTO.setCustomerName(rs
									.getString("Organization_Name"));
							notityDTO.setEmailAddress(rs.getString("Email_Id"));
							notityDTO.setMobileNumber(rs.getString("Phone_Number"));
							notityDTO.setApprovalDate(rs.getString("currentDate"));
							notityDTO.setStatus("SUCCESS");
						}
					}
				}
			}
		} catch (Exception e) {
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[duplication_application_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return notityDTO;
	}

	public synchronized String duplication_application_verify(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				pst = conn.prepareStatement(UPDATE_TRAFFIC_POLICE_REMARKS);
				pst.setString(1, dto.getRemarks());
				pst.setString(2, dto.getApplicationNumber());

				int count = pst.executeUpdate();
				if (count > 0) {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(dto.getApplicationNumber());
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId("112");
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(dto.getRegion());
					new WorkflowManager(conn).logVerificationWorkFlow(vo);
				}
			}

			result = "SUCCESS";
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[duplication_application_verify]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}


	public synchronized NotificationDTO passenger_bus_route_permit_approval(VehicleDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		notityDTO.setStatus("FAILURE");
		ResultSet rs = null;

		try {
			if (conn != null) {
				
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					String customerId = EralisCommonUtil.generateCustomerCodeFormat("P", conn);
					pst = conn.prepareStatement(CHECK_IF_CID_EXISTED_IN_PIS);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					if(rs.getInt("rowCount")==0)
					{
						pst = conn.prepareStatement(INSERT_PASSENGER_BUS_APPLICANT_IN_PIS);
						pst.setString(1, customerId);
						pst.setString(2, userVo.getActorId());
						pst.setString(3, dto.getApplicationNumber()); 
						pst.executeUpdate();
						
					}
					pst = conn.prepareStatement(INSERT_INTO_T_PESSENGER_BUS_ROUTE_PERMIT_DTLS);
					pst.setString(1, dto.getApplicationNumber());
					//pst.executeUpdate();
					int count = pst.executeUpdate();
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("111");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(dto.getRegion());
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
						notityDTO.setStatus("SUCCESS");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[passenger_bus_route_permit_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return notityDTO;
	}


	public synchronized NotificationDTO transfer_application_approval(
			VehicleDTO dto, UserDetailsVO userVo, Connection conn)
			throws ERALISException, ERALISSystemException {
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		notityDTO.setStatus("FAILURE");
		ResultSet rs = null;

		try {
			if (conn != null) {
				int countApprove = checkApplicationApproved(dto.getApplicationNumber(),conn);
				if(countApprove==1)
				{
					String newCustomer = null;
					String vehicleId = null;
	
					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_TRANSFER_DTLS);
					pst.setString(1, dto.getApplicationNumber());
					int count = pst.executeUpdate();
					if (count > 0) {
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNumber());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("111");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(dto.getRegion());
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
					}
	
					pst = conn.prepareStatement(GET_VECHILE_NUMBER);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
	
					newCustomer = rs.getString("Transferee_Customer_Id");
					vehicleId = rs.getString("Vehicle_Id");
					insertIntoPrintList(rs.getString("Vehicle_Id"),dto.getApplicationNumber(),"111", userVo,conn);
					pst = conn
							.prepareStatement(UPDATE_TRANSFEREE_CUSTOMER_ID_IN_VEHICLE_REG);
					pst.setString(1, newCustomer);
					pst.setString(2, newCustomer.substring(0, 1));
					pst.setString(3, vehicleId);
					pst.executeUpdate();
	
					pst = conn
							.prepareStatement(UPDATE_TRANSFEREE_CUSTOMER_ID_IN_VEHICLE_RENEWAL);
					pst.setString(1, newCustomer);
					pst.setString(2, vehicleId);
					pst.executeUpdate();
	
					pst = conn.prepareStatement(UPDATE_TRANSFER_DATES);
					pst.setDate(1, date);
					pst.setString(2, dto.getApplicationNumber());
					pst.executeUpdate();
	
					String query = "UPDATE t_vehicle_transfer_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=? WHERE a.`Application_Number`=?";
					pst = conn.prepareStatement(query);
					pst.setString(1, userVo.getRegionId());
					pst.setString(2, userVo.getBaseId());
					pst.setString(3, dto.getApplicationNumber());
					pst.executeUpdate();
	
					pst = conn.prepareStatement(GET_TRANSFER_DETAILS);
					pst.setString(1, dto.getApplicationNumber());
					rs = pst.executeQuery();
					rs.first();
					dto.setTransfereeCustomerId(dto.getTransfereeCustomerId());
					String Customer_Id = rs.getString("Transferee_Customer_Id")
							.substring(0, 1);
	
					if (Customer_Id.equals("P")) {
						pst = conn
								.prepareStatement(GET_TRANSFER_PERSONAL_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO.setCustomerName(rs.getString("pname"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setEmailAddress(rs.getString("Present_Email"));
						notityDTO.setMobileNumber(rs.getString("Present_Phone_No"));
						notityDTO.setStatus("SUCCESS");
					} else {
						pst = conn
								.prepareStatement(GET_TRANSFER_ORGANISATION_CUSTOMER_INFORMATION);
						pst.setString(1, dto.getApplicationNumber());
						rs = pst.executeQuery();
						rs.first();
	
						notityDTO.setApplicationNo(dto.getApplicationNumber());
						notityDTO
								.setCustomerName(rs.getString("Organization_Name"));
						notityDTO.setEmailAddress(rs.getString("Email_Id"));
						notityDTO.setMobileNumber(rs.getString("Phone_Number"));
						notityDTO.setApprovalDate(rs.getString("currentDate"));
						notityDTO.setStatus("SUCCESS");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[transfer_application_approval]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return notityDTO;
	}

	public synchronized String registration_application_reject(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNumber());
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId("109");
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				result = "SUCCESS";
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[registration_application_reject]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public synchronized String renewal_application_reject(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNumber());
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId("110");
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				result = "SUCCESS";
			}
		}

		catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[renewal_application_reject]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public synchronized String transfer_application_reject(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNumber());
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId("111");
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				result = "SUCCESS";
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[transfer_application_reject]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public synchronized String duplication_application_reject(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNumber());
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId("112");
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				result = "SUCCESS";
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[duplication_application_reject]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public synchronized String conversion_application_reject(VehicleDTO dto,
			UserDetailsVO userVo, Connection conn) throws ERALISException,
			ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNumber());
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId("113");
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				result = "SUCCESS";
			}
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at VehicleDAO[conversion_application_reject]:: "
							+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	public List<FitnessMainDTO> getFitnessItemList() throws ERALISException,
			ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<FitnessMainDTO> mainFitnessItemList = new ArrayList<FitnessMainDTO>();
		FitnessMainDTO dto;

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_MAIN_FITNESS_ITEMS);
				rs = pst.executeQuery();

				while (rs.next()) {
					dto = new FitnessMainDTO();
					dto.setItemId(rs.getString("Item_Id"));
					dto.setItemName(rs.getString("Item_Name"));
					List<FitnessItemDTO> fitnessItemList = getSubFitnessItemList(
							dto.getItemId(), conn);
					dto.setFitnessItemList(fitnessItemList);

					mainFitnessItemList.add(dto);
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return mainFitnessItemList;
	}

	private List<FitnessItemDTO> getSubFitnessItemList(String itemId,
			Connection conn) throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<FitnessItemDTO> fitnessItemList = new ArrayList<FitnessItemDTO>();
		FitnessItemDTO dto;

		try {
			pst = conn.prepareStatement(GET_FITNESS_ITEMS);
			pst.setString(1, itemId);
			rs = pst.executeQuery();

			while (rs.next()) {
				dto = new FitnessItemDTO();
				dto.setItemId(rs.getString("Item_Id"));
				dto.setItemName(rs.getString("Item_Name"));

				fitnessItemList.add(dto);
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, rs, pst);
		}

		return fitnessItemList;
	}

	public String getFittnessValidity(String testDate, String vehicleId,
			String vehicleTypeDesc) throws ERALISException,
			ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String validity = null;

		try {
			conn = ConnectionManager.getConnection();
			String expiryDate = null;

			pst = conn.prepareStatement(CHECK_IF_FITNESS_FIRST_TIME);
			pst.setString(1, vehicleId);
			rs = pst.executeQuery();
			rs.first();
			int count = rs.getInt("rowCount");
			if (count == 0) {
				pst = conn.prepareStatement(GET_VEHICLE_REGISTARTION_DATE);
				pst.setString(1, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				expiryDate = rs.getString("Registration_Date");
			} else {
				//pst = conn.prepareStatement(LAST_EXPIRY_DATE);
				pst = conn.prepareStatement( "SELECT "
						+ "IF( "
						+ "a.`Vehicle_Reg_Dtls_Id` IN "
						+ "(SELECT "
						+ "`Vehicle_Id` "
						+ "FROM "
						+ "`t_vehicle_fitness_test_dtls` WHERE Vehicle_Id=?), (SELECT "
						+ "MAX(Validity) "
						+ "FROM "
						+ "`t_vehicle_fitness_test_dtls` WHERE Vehicle_Id=?), "
						+ "a.`Registration_Date` "
						+ ") expiryDate "
						+ "FROM "
						+ "`t_vehicle_registration_dtls` a "
						+ "WHERE a.`Vehicle_Reg_Dtls_Id`=?");
				pst.setString(1, vehicleId);
				pst.setString(2, vehicleId);
				pst.setString(3, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				expiryDate = rs.getString("expiryDate");
			}

			pst = conn.prepareStatement(DIFFERENT_VEHICLE_DESCRIPTION);
			pst.setString(1, vehicleId);
			rs = pst.executeQuery();
			rs.first();

			if ((rs.getString("Description").equals("TAXI"))
					|| (rs.getString("Description").equals("HEAVY_VEHICLE"))
					|| (rs.getString("Description").equals("MEDIUM_BUS"))
					|| (rs.getString("Description").equals("HEAVY_BUS"))
					|| (rs.getString("Description").equals("MEDIUM_VEHICLE"))) {
				duration = Integer
						.parseInt(EralisCommonUtil
								.getNotificationProperty("renewal-expirydate-commercial-duration"));

				DateTimeFormatter format = DateTimeFormat
						.forPattern("yyyy-MM-dd");
				DateTime lastExpiryDate = format.parseDateTime(expiryDate);
				lastExpiryDate.plusMonths(duration);
				String expiryDate1 = format.print(lastExpiryDate
						.plusMonths(duration));
				java.util.Date expiry = targetSdf.parse(expiryDate1);
				String renewalExpiryDate = sourceSdf.format(expiry);

				validity = renewalExpiryDate;
			}

			else if ((rs.getString("Description").equals("TWO_WHEELER"))
					|| (rs.getString("Description").equals("LIGHT_VEHICLE"))
					|| (rs.getString("Description").equals("TRACTOR"))
					|| (rs.getString("Description").equals("EARTH_MOVING_EQUIPMENT"))) {
				duration1 = Integer
						.parseInt(EralisCommonUtil
								.getNotificationProperty("renewal-expirydate-noncommercial-duration"));

				DateTimeFormatter format = DateTimeFormat
						.forPattern("yyyy-MM-dd");
				DateTime lastExpiryDate = format.parseDateTime(expiryDate);
				lastExpiryDate.plusMonths(duration1);
				String expiryDate1 = format.print(lastExpiryDate
						.plusMonths(duration1));
				java.util.Date expiry = targetSdf.parse(expiryDate1);
				String renewalExpiryDate = sourceSdf.format(expiry);

				validity = renewalExpiryDate;
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return validity;
	}

	public EralisCommonDTO getVehicleModelDtls(String vehicleModelId)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			pst = conn.prepareStatement(GET_VEHICLE_MODEL_DTLS);
			pst.setString(1, vehicleModelId);
			rs = pst.executeQuery();
			rs.first();
			dto.setEngineCC(rs.getString("Engine_CC_Kilowatt_Horsepower"));
			dto.setSeatCapacity(rs.getString("Seating_Capacity"));
			dto.setUnladenWeight(rs.getString("Unladen_Weight"));

		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}

	public EralisCommonDTO getLicensAndLearnerDtls(String licenseNo)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			String query = null;
			if (licenseNo.contains("ll")) {
				query = GET_LEARNER_DTLS_FOR_ACCIDENT;
			} else {
				query = GET_LICENSE_DTLS_FOR_ACCIDENT;

			}
			pst = conn.prepareStatement(query);
			pst.setString(1, licenseNo);
			rs = pst.executeQuery();
			while (rs.next()) {

				dto.setName(rs.getString("driverName"));
				dto.setCID(rs.getString("CID_Number"));
				dto.setDOB(rs.getString("Date_Of_Birth"));
				dto.setMobileNo(rs.getString("Present_Phone_No"));
				dto.setGender(rs.getString("Gender"));
			}

		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}

	public EralisCommonDTO getVehicleDtls(String vehicleNo, String vehicleType)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			String query = GET_VEHICLE_DTLS_FOR_ACCIDENT;

			pst = conn.prepareStatement(query);
			pst.setString(1, vehicleNo);
			pst.setString(2, vehicleType);
			rs = pst.executeQuery();
			while (rs.next()) {
				dto.setModel(rs.getString("Vehicle_Model_Id"));
				dto.setCompany(rs.getString("Vehicle_Company_Id"));
			}

		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}
	
	public EralisCommonDTO getVehiclePreviousPayment(String vehicleId,String viewRenewalDurations) throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String amount = null;
		String renewalDuration = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			String query = GET_VEHICLE_PREVIOUS_PAYMENT;
			pst = conn.prepareStatement(query);
			pst.setString(1, vehicleId);
			rs = pst.executeQuery();
			while (rs.next()) {
				dto.setAmount(rs.getString("previousPayment"));
				dto.setRenewalDate(rs.getString("renewalDuration"));
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}

	public EralisCommonDTO validateChasisEngineNo(String engineNumber,String chasisNumber) throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		try {
			conn = ConnectionManager.getConnection();
			String query = VALIDATE_ENGINENO_CHASIS;
			pst = conn.prepareStatement(query);
			pst.setString(1, chasisNumber);
			pst.setString(2, engineNumber);
			rs = pst.executeQuery();
			while (rs.next()) {
				if(rs.getInt("countChassisNo")>0)
				{
					dto.setChasisStatus("ALREADY_EXIST");
				}
				if(rs.getInt("countEngineNo")>0)
				{
					dto.setEngineNoStatus("ALREADY_EXIST");
				}
			}

		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		return dto;
	}

	public String checkIfFitnessNEmissionValid(String vehicleId)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_EMISSION_DETAILS);
				pst.setString(1, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				String Emission_Test_Type = rs.getString("Emission_Test_Type");

				if (rowCount == 0)
					result = "NO_EMISSION_RECORD";
				else if (Emission_Test_Type.equalsIgnoreCase("F"))
					result = "EMISSION_INVALID";

				pst = conn.prepareStatement(GET_FITNESS_DETAILS);
				pst.setString(1, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				rowCount = rs.getInt("rowCount");

				if (rowCount == 0)
					result = "NO_FITNESS_RECORD";
			}
		} catch (Exception e) {
			result = "FAILURE";
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return result;
	}

	public VehicleDTO print_emission_report(VehicleDTO dto)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();
			if (conn != null) {
				pst = conn.prepareStatement(GET_EMISSION_TEST_DTLS);
				pst.setString(1, dto.getVehicleId());
				rs = pst.executeQuery();
				rs.first();
				dto.setVehicleNumber(rs.getString("Vehicle_Number"));
				dto.setChasisNumber(rs.getString("Chassis_Number"));
				dto.setEngineNumber(rs.getString("Engine_Number"));
				dto.setRegistrationDate(rs.getString("Registration_Date"));
				dto.setVehicleModel(rs.getString("Vehicle_Model_Name"));
				dto.setcO(rs.getString("CO"));
				dto.sethSU(rs.getString("HSU"));
				dto.setT1(rs.getString("T1"));
				dto.setT2(rs.getString("T2"));
				dto.setT3(rs.getString("T3"));
				dto.setEmissionTestType(rs.getString("Emission_Test_Type"));
				dto.setTestLocation(rs.getString("Test_Location"));
				dto.setExpiryDate(rs.getString("Validity"));
				dto.setTestedOn(rs.getString("Tested_On"));
				dto.setVehicleCompany(rs.getString("Vehicle_Company_Name"));
				dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
				dto.setAmount(rs.getString("Price"));
				dto.setTestResult(rs.getString("Test_Result"));
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public VehicleDTO get_vehicle_details(String vehicleNo, String vehicleType)
			throws ERALISException, ERALISSystemException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		VehicleDTO dto = new VehicleDTO();

		try {
			conn = ConnectionManager.getConnection();

			if (conn != null) {
				pst = conn.prepareStatement(GET_VEHICLE_DETAILS);
				pst.setString(1, vehicleNo);
				pst.setString(2, vehicleType);
				rs = pst.executeQuery();
				while (rs.next()) {
					dto.setVehicleId(rs.getString("Vehicle_Reg_Dtls_Id"));
					dto.setRegistrationType(rs.getString("Registration_Type"));
					dto.setVehicleRegistrationId(rs
							.getString("Vehicle_Registration_Id"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Id"));
					dto.setVanityNumber(rs.getString("Vanity_Number"));
					dto.setVehicleCompany(rs.getString("Vehicle_Company_Id"));
					dto.setRegistrationDate(rs.getString("registrationDate"));
					dto.setVehicleModel(rs.getString("Vehicle_Model_Id"));
					dto.setEngineType(rs.getString("Engine_Type_Id"));
					dto.setEngineNumber(rs.getString("Engine_Number"));
					dto.setChasisNumber(rs.getString("Chassis_Number"));
					dto.setStatus(rs.getString("Status"));
					dto.setColour(rs.getString("Colour"));
					dto.setPrice(rs.getString("Price"));
					dto.setLoadCapacity(rs.getString("Load_Capacity"));
					dto.setDealersName(rs.getString("Dealer_Id"));
					dto.setSeatCapacity(rs.getString("Seat_Capacity"));
					dto.setVehicleHorsePower(rs
							.getString("Vehicle_Horse_Power"));
					dto.setVehicleKiloWatt(rs.getString("Vehicle_Kilowatt"));
					dto.setUnladenWeight(rs.getString("Unladen_Weight"));
					dto.setManufactureYear(rs.getString("Manufacture_Year"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setManufactureCountry(rs
							.getString("Manufacture_Country_Id"));
					dto.setPurchaseType(rs.getString("Purchase_Type"));
					dto.setRegion(rs.getString("Region_Id"));
					dto.setBusType(rs.getString("Bus_Type_Id"));
					if (null != rs.getString("Remarks"))
						dto.setRemarks(rs.getString("Remarks"));
					else
						dto.setRemarks("");

					System.out.println(dto.getRemarks());
				}
			}
		} catch (Exception e) {
			throw new ERALISException();
		} finally {
			ConnectionManager.close(conn, null, rs, pst);
		}

		return dto;
	}

	public String update_vehicle_data(VehicleDTO dto, UserDetailsVO userVo,
			Connection conn) throws ERALISException, ERALISSystemException {
		PreparedStatement pst = null;
		String result = "FAILURE";

		try {
			if (conn != null) {
				pst = conn.prepareStatement(UPDATE_VEHICLE_DATA);
				pst.setString(1, dto.getRegistrationType());
				pst.setString(2, dto.getVehicleRegistrationId());
				pst.setString(3, dto.getRegion());
				pst.setString(4, dto.getVehicleType());
				pst.setString(5, dto.getVanityNumber());
				pst.setString(6, dto.getVehicleCompany());
				java.util.Date registrationDate = sourceSdf.parse(dto
						.getRegistrationDate());
				String registrationDateStr = targetSdf.format(registrationDate);
				pst.setString(7, registrationDateStr);
				pst.setString(8, dto.getVehicleModel());
				pst.setString(9, dto.getEngineType());
				pst.setString(10, dto.getEngineNumber());
				pst.setString(11, dto.getChasisNumber());
				pst.setString(12, dto.getStatus());
				pst.setString(13, dto.getColour());
				pst.setString(14, dto.getPrice());
				pst.setString(15, dto.getLoadCapacity());
				pst.setString(16, dto.getDealersName());
				pst.setString(17, dto.getSeatCapacity());
				pst.setString(18, dto.getVehicleHorsePower());
				pst.setString(19, dto.getVehicleKiloWatt());
				pst.setString(20, dto.getUnladenWeight());
				pst.setString(21, dto.getManufactureYear());
				pst.setString(22, dto.getEngineCC());
				pst.setString(23, dto.getManufactureCountry());
				pst.setString(24, dto.getPurchaseType());
				pst.setString(25, userVo.getActorId());
				pst.setDate(26, date);
				pst.setString(27, dto.getRemarks());
				pst.setString(28, dto.getBusType());
				pst.setString(29, dto.getVehicleId());
				pst.setString(30, dto.getVehicleTypeId());

				int count = pst.executeUpdate();

				if (count > 0)
					result = "SUCCESS";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}

	
	public String insertIntoPrintList(String vehicleId,String applicationNo,String serviceId, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;

		try {
			if (conn != null) {
				
				pst = conn.prepareStatement(GET_VEHICLE_NO_AND_VEHICLE_TYPE);
				pst.setString(1, vehicleId);
				rs = pst.executeQuery();
				rs.first();
				System.out.println(rs.getString("Vehicle_Number")+"?"+rs.getString("Vehicle_Type_Id"));
				int colCount = 1;
				pst = conn.prepareStatement(INSERT_INTO_PRINT_DTLS);
				pst.setString(colCount++, applicationNo);
				pst.setString(colCount++, serviceId);
				pst.setString(colCount++, rs.getString("Vehicle_Number"));
				pst.setString(colCount++, vehicleId);
				pst.setString(colCount++, userVo.getJurisdictionId());
				pst.setString(colCount++, userVo.getJurisdictionTypeId());
				pst.setString(colCount++, rs.getString("Vehicle_Type_Id"));
				int count = pst.executeUpdate();
				if (count > 0)
					result = "SUCCESS";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}
	
	public int checkApplicationApproved(String applicationNo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		int result = 0;
		ResultSet rs = null;
		try 
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(CHECK_APPLICATION_APPROVED);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				result = rs.getInt("rowCount");
			}
		} 
		catch (Exception e) 
		{
			
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	
	/*
	 * Queries
	 */
	
	private static final String GET_VEHICLE_NO_AND_VEHICLE_TYPE = "SELECT a.`Vehicle_Number`,a.`Vehicle_Type_Id` FROM t_vehicle_registration_dtls a WHERE a.`Vehicle_Reg_Dtls_Id`=?";
	
	
	private static final String INSERT_INTO_PRINT_DTLS = "INSERT INTO `t_print_dtls` "
												+ "            (`Application_Number`, "
												+ "             `Document_Type`, "
												+ "             `Service_Id`, "
												+ "             `Identity_Number`, "
												+ "             `Identity_Id`, "
												+ "             `Jurisdiction_Id`, "
												+ "             `Jurisdiction_Type_Id`,Vehicle_Type_Id) "
												+ "VALUES (?,'RC',?,?,?,?,?,?);";

	private static final String UPDATE_VEHICLE_DATA = "UPDATE "
			+ "  `t_vehicle_registration_dtls` " + "SET "
			+ "  `Registration_Type` = ?, "
			+ "  `Vehicle_Registration_Id` = ?, " + "  `Region_Id` = ?, "
			+ "  `Vehicle_Type_Id` = ?, " + "  `Vanity_Number` = ?, "
			+ "  `Vehicle_Company_Id` = ?, " + "  `Registration_Date` = ?, "
			+ "  `Vehicle_Model_Id` = ?, " + "  `Engine_Type_Id` = ?, "
			+ "  `Engine_Number` = ?, " + "  `Chassis_Number` = ?, "
			+ "  `Status` = ?, " + "  `Colour` = ?, " + "  `Price` = ?, "
			+ "  `Load_Capacity` = ?, " + "  `Dealer_Id` = ?, "
			+ "  `Seat_Capacity` = ?, " + "  `Vehicle_Horse_Power` = ?, "
			+ "  `Vehicle_Kilowatt` = ?, " + "  `Unladen_Weight` = ?, "
			+ "  `Manufacture_Year` = ?, " + "  `Engine_CC` = ?, "
			+ "  `Manufacture_Country_Id` = ?, " + "  `Purchase_Type` = ?, "
			+ "  `Updated_By` = ?, " + "  `Updated_On` = ?  ,"
			+ "  `Remarks`=?,Bus_Type_Id=?"
			+ "  WHERE `Vehicle_Reg_Dtls_Id` = ? AND Vehicle_Type_Id = ?";

	private static final String GET_VEHICLE_DETAILS = "SELECT "
			+ "  a.`Vehicle_Reg_Dtls_Id`, " + "  a.`Registration_Type`, "
			+ "  a.`Vehicle_Registration_Id`, " + "  a.`Vehicle_Type_Id`, "
			+ "  a.`Vanity_Number`, " + "  a.`Vehicle_Company_Id`, "
			+ "  DATE_FORMAT( " + "    a.`Registration_Date`, "
			+ "    '%d/%m/%Y' " + "  ) registrationDate, "
			+ "  a.`Vehicle_Model_Id`, " + "  a.`Engine_Type_Id`, "
			+ "  a.`Engine_Number`, " + "  a.`Chassis_Number`, "
			+ "  a.`Status`, " + "  a.`Colour`, " + "  a.`Price`, "
			+ "  a.`Load_Capacity`, " + "  a.`Dealer_Id`, "
			+ "  a.`Seat_Capacity`, " + "  a.`Vehicle_Horse_Power`, "
			+ "  a.`Vehicle_Kilowatt`, " + "  a.`Unladen_Weight`, "
			+ "  a.`Manufacture_Year`, " + "  a.`Engine_CC`, "
			+ "  a.`Manufacture_Country_Id`, " + "  a.`Purchase_Type`, "
			+ "  `Region_Id`,a.Remarks,a.Bus_Type_Id " + "FROM "
			+ "  t_vehicle_registration_dtls a "
			+ "WHERE a.`Vehicle_Number` = ? " + "  AND a.`Vehicle_Type_Id` = ?";

	private static final String LAST_EXPIRY_DATE = "SELECT " + "  IF( "
			+ "    a.`Vehicle_Reg_Dtls_Id` IN " + "    (SELECT "
			+ "      Vehicle_Id " + "    FROM "
			+ "      t_vehicle_renewal_dtls " + "    WHERE Vehicle_Id = ?), "
			+ "    (SELECT " + "      MAX(Expiry_Date) " + "    FROM "
			+ "      t_vehicle_renewal_dtls " + "    WHERE Vehicle_Id = ?), "
			+ "    a.`Expiry_Date` " + "  ) expiryDate " + "FROM "
			+ "  t_vehicle_registration_dtls a "
			+ " WHERE a.Vehicle_Reg_Dtls_Id = ?";

	private static final String UPDATE_T_VEHICLE_APPLICATION_WITH_EXPIRY_DATE = "UPDATE t_vehicle_application a SET a.`Expiry_Date`=? WHERE a.`Application_Number`=?";

	private static final String DIFFERENT_VEHICLE_DESCRIPTION = "SELECT "
			+ "  b.`Description` " + "FROM "
			+ "  t_vehicle_registration_dtls a "
			+ "  LEFT JOIN t_vehicle_type_master b "
			+ "    ON a.`Vehicle_Type_Id` = b.`Vehicle_Type_Id` "
			+ "WHERE a.`Vehicle_Reg_Dtls_Id` = ?";

	private static final String GET_EMISSION_DETAILS = "SELECT COUNT(*) rowCount,a.`Emission_Test_Type` FROM t_vehicle_emission_test_dtls a WHERE a.`Vehicle_Id`=?";

	private static final String GET_FITNESS_DETAILS = "SELECT COUNT(*) rowCount FROM `t_vehicle_fitness_test_dtls` WHERE Vehicle_Id=?";

	private static final String GET_MAIN_FITNESS_ITEMS = "SELECT Item_Id, Item_Name FROM t_main_fitness_item_master ORDER BY Item_Id";

	private static final String GET_FITNESS_ITEMS = "SELECT a.Item_Id,a.Item_Name FROM t_fitness_item_master a WHERE a.`Main_Item_Id`=? ORDER BY a.`Item_Id`";

	private static final String CONTROL_MECHANISM = "SELECT "
			+ " COUNT(*) rowCount " + "FROM "
			+ " t_vehicle_registration_dtls a " + "WHERE a.`Customer_Id` = ? "
			+ " AND a.`Engine_Number` = ? " + " AND a.`Chassis_Number` = ? "
			+ " AND a.`Vehicle_Type_Id` = ?";

	private static final String CHECK_IF_ALREADY_APPLIED = "SELECT "
			+ "  COUNT(*) rowCount " + "FROM " + "  t_vehicle_application a "
			+ "WHERE a.`Customer_Id` = ? " + "  AND a.`Engine_Number` = ? "
			+ "  AND a.`Chasis_Number` = ? " + "  AND a.`Vehicle_Type_Id` = ?";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION = "INSERT INTO t_vehicle_application ( "
			+ "  Application_Number, "
			+ "  Application_Type, "
			+ "  Vehicle_Registration_Type, "
			+ "  Customer_Id, "
			+ "  Registration_Type, "
			+ "  Vehicle_Registration_Id, "
			+ "  Region_Id, "
			+ "  Vehicle_Type_Id, "
			+ "  Vanity_Number, "
			+ "  Vehicle_Company_Id, "
			+ "  Vehicle_Model_Id, "
			+ "  Engine_Type_Id, "
			+ "  Engine_Number, "
			+ "  Chasis_Number, "
			+ "  STATUS, "
			+ "  Color, "
			+ "  Price, "
			+ "  Load_Capacity, "
			+ "  Dealer_Id, "
			+ "  Seat_Capacity, "
			+ "  Vehicle_Horse_Power, "
			+ "  Vehicle_Kilowatt, "
			+ "  Unladen_Weight, "
			+ "  Purchase_Date, "
			+ "  Manufacture_Year, "
			+ "  Engine_CC, "
			+ "  Manufactured_Country_Id, "
			+ "  Purchase_Type, "
			+ "  App_Submission_Date, "
			+ "  Remarks, "
			+ "  Created_By, "
			+ "  Created_On, "
			+ "  Updated_By, "
			+ "  Updated_On,Renewal_Duration,Receipt_Number,Receipt_Date,Bus_Type_Id,Number_Of_Wheels,Number_Of_Axle,Quota_Holder_Mobile_Number,Owner_Mobile_Number,Diplomat_Code "
			+ ") "
			+ "VALUES "
			+ "  ( "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    ?, "
			+ "    CURRENT_TIMESTAMP, "
			+ "    ?, "
			+ "    ?, "
			+ "    CURRENT_TIMESTAMP, "
			+ "    ?, "
			+ "    CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?" + "  )";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_RENEWAL = "INSERT INTO t_vehicle_application "
			+ "            (Application_Number, "
			+ "             Application_Type, "
			+ "             Vehicle_Registration_Type, "
			+ "             Customer_Id, "
			+ "             Vehicle_Number, "
			+ "             App_Submission_Date, "
			+ "             Remarks, "
			+ "             Created_By, "
			+ "             Created_On, "
			+ "             Updated_By, "
			+ "             Updated_On,`Vehicle_Id`,`Renewal_Duration`,Receipt_Number,Receipt_Date) "
			+ "VALUES (?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP, "
			+ "        ?, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP,?,?,?,?);";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_TRANSFER = "INSERT INTO t_vehicle_application "
			+ "    	      (Application_Number, "
			+ "    	      Application_Type, "
			+ "    	      Vehicle_Registration_Type, "
			+ "    	      Vehicle_Number, "
			+ "    	      Transferor_Customer_Id, "
			+ "    	      Sale_Deed_Date, "
			+ "    	      Transferee_Type, "
			+ "    	      Transferee_Customer_Id, "
			+ "    	      Sale_Deed_Amount, "
			+ "    	      App_Submission_Date, "
			+ "		      Remarks, "
			+ "		      Created_By, "
			+ "		      Created_On, "
			+ "		      Updated_By, "
			+ "		      Updated_On,Vehicle_Id,Receipt_Number,Receipt_Date,Transfer_Type) "
			+ "    	 VALUES (?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         CURRENT_TIMESTAMP, "
			+ "    	         ?, "
			+ "    	         ?, "
			+ "    	         CURRENT_TIMESTAMP, "
			+ "    	         ?, " + "    	         CURRENT_TIMESTAMP,?,?,?,?);";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_DUPLICATION = "INSERT INTO t_vehicle_application "
			+ "            (Application_Number, "
			+ "             Application_Type, "
			+ "             Vehicle_Registration_Type, "
			+ "             Customer_Id, "
			+ "             Vehicle_Number, "
			+ "             App_Submission_Date, "
			+ "             Remarks, "
			+ "             Created_By, "
			+ "             Created_On, "
			+ "             Updated_By, "
			+ "             Updated_On,Vehicle_Id,Receipt_Number,Receipt_Date) "
			+ "VALUES (?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP, "
			+ "        ?, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP,?,?,?);";

	private static final String INSERT_INTO_T_VEHICLE_CANCELLATION = "INSERT INTO t_vehicle_cancellation_dtls "
			+ "            (Vehicle_Id, "
			+ "             Customer_Id, "
			+ "             Cancellation_Date, "
			+ "             Cancellation_Reason_Id, "
			+ "             Receipt_No, "
			+ "             Receipt_Date, "
			+ "             Created_By, "
			+ "             Created_On, "
			+ "             Updated_By, "
			+ "             Updated_On,Processed_Region_Id,Processed_Base_Id) "
			+ "VALUES (?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP, "
			+ "        ?, "
			+ "        CURRENT_TIMESTAMP, ?, ?);";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_CONVERSION = "INSERT INTO t_vehicle_application "
			+ "	(Application_Number, "
			+ "	Application_Type, "
			+ "	Vehicle_Registration_Type, "
			+ "	Registration_Type, "
			+ "	Customer_Id, "
			+ "	Vehicle_Number, "
			+ "	Conversion_Reason_Id, "
			+ "	Vehicle_Registration_Id, "
			+ "	Region_Id, "
			+ "	Vehicle_Type_Id, "
			+ "	Vehicle_Prefix, "
			+ "	App_Submission_Date, "
			+ "	App_Verification_Date, "
			+ "	App_Approval_Date, "
			+ "	Remarks, "
			+ "	Created_By, "
			+ "	Created_On, "
			+ "	Updated_By, "
			+ "	Updated_On,Vehicle_Id,Receipt_Number,Receipt_Date) "
			+ "	VALUES"
			+ "(?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	?, "
			+ "	CURRENT_TIMESTAMP, "
			+ "	CURRENT_TIMESTAMP, "
			+ "	CURRENT_TIMESTAMP, "
			+ "	?, "
			+ "	?, "
			+ "	CURRENT_TIMESTAMP, "
			+ "	?, " + "	CURRENT_TIMESTAMP,?,?,?)";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_FITNESS = "INSERT INTO `t_vehicle_fitness_test_dtls` ( "
			+ "  `Customer_Id`, "
			+ "  `Vehicle_Id`, "
			+ "  `Test_Date`, "
			+ "  `Validity`, "
			+ "  `Remarks`, "
			+ "  `Created_By`, "
			+ "  `Created_On`,Receipt_Number,Receipt_Date,Processed_Region_Id,Processed_Base_Id,Inspected_By "
			+ ") "
			+ "VALUES "
			+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String INSERT_INTO_T_VEHICLE_FITNESS_ITEM_DTLS = "INSERT INTO `t_vehicle_fitness_item_dtls` (`Vehicle_Id`, `Item_Id`, `Test_Result`) VALUES (?, ?, ?)";

	private static final String INSERT_INTO_T_VEHICLE_APPLICATION_EMISSION = "INSERT INTO t_vehicle_emission_test_dtls ( "
			+ "  Vehicle_Id, "
			+ "  Tested_On, "
			+ "  Validity, "
			+ "  Test_Location, "
			+ "  Emission_Test_Type, "
			+ "  CO, "
			+ "  T1, "
			+ "  T2, "
			+ "  T3, "
			+ "  HSU, "
			+ "  Test_Result, "
			+ "  Created_By, "
			+ "  Created_On, "
			+ "  Updated_By, "
			+ "  Updated_On ,Price,Is_Retested"
			+ ") "
			+ "VALUES "
			+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String INSERT_NEW_POST_STATUS = "";
	private static final String INSERT_NEW_PRINT_STATUS = "";

	// approved vehicle

	private static final String INSERT_INTO_T_VEHICLE_REGISTRATION_DTLS = "INSERT INTO t_vehicle_registration_dtls ( "
			+ "  Application_Number, "
			+ "  Vehicle_Registration_Type, "
			+ "  Customer_Id, "
			+ "  Registration_Type, "
			+ "  Amount, "
			+ "  Vehicle_Registration_Id, "
			+ "  Region_Id, "
			+ "  Vehicle_Number, "
			+ "  Vehicle_Type_Id, "
			+ "  Vanity_Number, "
			+ "  Vehicle_Company_Id, "
			+ "  Registration_Date, "
			+ "  Vehicle_Model_Id, "
			+ "  Expiry_Date, "
			+ "  Engine_Type_Id, "
			+ "  Engine_Number, "
			+ "  Chassis_Number, "
			+ "  Receipt_Date, "
			+ "  Receipt_Number, "
			+ "  STATUS, "
			+ "  Colour, "
			+ "  Price, "
			+ "  Load_Capacity, "
			+ "  Dealer_Id, "
			+ "  Seat_Capacity, "
			+ "  Hypothecated_Id, "
			+ "  Unladen_Weight, "
			+ "  Letter_No, "
			+ "  Letter_Date, "
			+ "  Manufacture_Year, "
			+ "  Engine_CC, "
			+ "  Manufacture_Country_Id, "
			+ "  Remarks, "
			+ "  Purchase_Type, "
			+ "  Vehicle_Horse_Power,"
			+ "  Vehicle_Kilowatt,"
			+ "  App_Submission_Date, "
			+ "  App_Approval_Date, "
			+ "  Created_By, "
			+ "  Created_On, "
			+ "  Bus_Type_Id, "
			+ "  Number_Of_Axle, "
			+ "  Number_Of_Wheels, "
			+ "  Quota_Holder_Mobile_Number, "
			+ "  Owner_Mobile_Number,`Processed_Region_Id`,`Processed_Base_Id`)"
			+ " SELECT "
			+ "    Application_Number, "
			+ "    Vehicle_Registration_Type, "
			+ "    Customer_Id, "
			+ "    Registration_Type, "
			+ "    Amount, "
			+ "    Vehicle_Registration_Id, "
			+ "    Region_Id, "
			+ "    Vehicle_Number, "
			+ "    Vehicle_Type_Id, "
			+ "    Vanity_Number, "
			+ "    Vehicle_Company_Id, "
			+ "    App_Submission_Date, "
			+ "    Vehicle_Model_Id, "
			+ "    Expiry_Date, "
			+ "    Engine_Type_Id, "
			+ "    Engine_Number, "
			+ "    Chasis_Number, "
			+ "    Receipt_Date, "
			+ "    Receipt_Number, "
			+ "    STATUS, "
			+ "    Color, "
			+ "    Price, "
			+ "    Load_Capacity, "
			+ "    Dealer_Id, "
			+ "    Seat_Capacity, "
			+ "    Hypothecated_To_Id, "
			+ "    Unladen_Weight, "
			+ "    Letter_Number, "
			+ "    Letter_Date, "
			+ "    Manufacture_Year, "
			+ "    Engine_CC, "
			+ "    Manufactured_Country_Id, "
			+ "    Remarks, "
			+ "    Purchase_Type, "
			+ "    Vehicle_Horse_Power,"
			+ "    Vehicle_Kilowatt,"
			+ "    App_Submission_Date, "
			+ "    App_Approval_Date, "
			+ "    ?, "
			+ "    CURDATE(),"
			+ "    Bus_Type_Id,"
			+ "    Number_Of_Axle, "
			+ "    Number_Of_Wheels, "
			+ "    Quota_Holder_Mobile_Number, "
			+ "    Owner_Mobile_Number,?,?"
			+ "    FROM t_vehicle_application "
			+ "    WHERE Application_Number=?";

	private static final String INSERT_INTO_T_VEHICLE_CONVERSION_DTLS = "INSERT INTO t_vehicle_registration_dtls ( "
			+ "  Application_Number, "
			+ "  Vehicle_Registration_Type, "
			+ "  Customer_Id, "
			+ "  Registration_Type, "
			+ "  Amount, "
			+ "  Vehicle_Registration_Id, "
			+ "  Region_Id, "
			+ "  Vehicle_Prefix, "
			+ "  Vehicle_Number, "
			+ "  Vehicle_Type_Id, "
			+ "  Vanity_Number, "
			+ "  Vehicle_Company_Id, "
			+ "  Registration_Date, "
			+ "  Vehicle_Model_Id, "
			+ "  Expiry_Date, "
			+ "  Engine_Type_Id, "
			+ "  Engine_Number, "
			+ "  Chassis_Number, "
			+ "  Receipt_Date, "
			+ "  Receipt_Number, "
			+ "  STATUS, "
			+ "  Colour, "
			+ "  Price, "
			+ "  Load_Capacity, "
			+ "  Dealer_Id, "
			+ "  Seat_Capacity, "
			+ "  Hypothecated_Id, "
			+ "  Unladen_Weight, "
			+ "  Letter_No, "
			+ "  Letter_Date, "
			+ "  Manufacture_Year, "
			+ "  Engine_CC, "
			+ "  Manufacture_Country_Id, "
			+ "  Remarks, "
			+ "  Purchase_Type, "
			+ "  Vehicle_Horse_Power,"
			+ "  Vehicle_Kilowatt,"
			+ "  App_Submission_Date, "
			+ "  App_Approval_Date, "
			+ "  Created_By, "
			+ "  Created_On, "
			+ "  Updated_By, "
			+ "  Updated_On) "
			+ "SELECT "
			+ "    Application_Number, "
			+ "    Vehicle_Registration_Type, "
			+ "    Customer_Id, "
			+ "    Registration_Type, "
			+ "    Amount, "
			+ "    Vehicle_Registration_Id, "
			+ "    Region_Id, "
			+ "    Vehicle_Prefix, "
			+ "    Vehicle_Number, "
			+ "    Vehicle_Type_Id, "
			+ "    Vanity_Number, "
			+ "    Vehicle_Company_Id, "
			+ "    App_Submission_Date, "
			+ "    Vehicle_Model_Id, "
			+ "    Expiry_Date, "
			+ "    Engine_Type_Id, "
			+ "    Engine_Number, "
			+ "    Chasis_Number, "
			+ "    Receipt_Date, "
			+ "    Receipt_Number, "
			+ "    STATUS, "
			+ "    Color, "
			+ "    Price, "
			+ "    Load_Capacity, "
			+ "    Dealer_Id, "
			+ "    Seat_Capacity, "
			+ "    Hypothecated_To_Id, "
			+ "    Unladen_Weight, "
			+ "    Letter_Number, "
			+ "    Letter_Date, "
			+ "    Manufacture_Year, "
			+ "    Engine_CC, "
			+ "    Manufactured_Country_Id, "
			+ "    Remarks, "
			+ "    Purchase_Type, "
			+ "    Vehicle_Horse_Power,"
			+ "    Vehicle_Kilowatt,"
			+ "    App_Submission_Date, "
			+ "    App_Approval_Date, "
			+ "    Created_By, "
			+ "    Created_On, "
			+ "    Updated_By, "
			+ "    Updated_On "
			+ "    FROM t_vehicle_application "
			+ "    WHERE Application_Number=?";

	private static final String GET_VECHILE_NUMBER = "SELECT "
			+ "  a.Vehicle_Registration_Id, "
			+ "  a.Region_Id, "
			+ "  a.Vehicle_Prefix, "
			+ "  a.Vehicle_Type_Id, "
			+ "  a.Receipt_Date, "
			+ " a.Diplomat_Code, "
			+ "if(a.Vehicle_Registration_Id=6,(select Sl_No from t_vehicle_number_sequence "
			+ "          where Diplomat_Code=a.Diplomat_Code and Registration_Code_Id=6), "
			+ "         if(a.Vehicle_Registration_Id=3,(select Sl_No from t_vehicle_number_sequence "
			+ "          where Registration_Code_Id=3),'')) Sl_No,"
			+ "  (SELECT "
			+ "    `Reason` "
			+ "  FROM "
			+ "    `t_reason_master` "
			+ "  WHERE `Reason_Id` = a.`Conversion_Reason_Id`) conversionType,a.* "
			+ "FROM " + "  t_vehicle_application a "
			+ "WHERE a.Application_Number = ?";

	private static final String UPDATE_VEHICLE_NUMBER = "UPDATE "
			+ "  t_vehicle_registration_dtls " + "SET "
			+ "  Vehicle_Number = ? " + "WHERE Application_Number = ?";

	private static final String UPDATE_NUMBER_SEQUENCE = "UPDATE "
			+ "  t_vehicle_number_sequence a " + "SET " + "  Sl_No = ?, "
			+ "  a.Vehicle_Prefix = ? " + "WHERE a.Registration_Code_Id = ? "
			+ "  AND a.Vehicle_Type_Id = ? " + "  AND a.Region_Id = ?";

	private static final String UPDATE_DIPLOMAT_NUMBER_SEQUENCE = "UPDATE "
			+ "t_vehicle_number_sequence a " + "SET " + "Sl_No = ? "
			+ "WHERE a.Registration_Code_Id = ? " + "AND a.Diplomat_Code=?";

	private static final String UPDATE_BTH_AND_BHUTAN_NUMBER_SEQUENCE = "UPDATE "
			+ "t_vehicle_number_sequence a "
			+ "SET "
			+ "Sl_No = ? "
			+ "WHERE a.Registration_Code_Id = ? ";

	private static final String UPDATE_DATES = "UPDATE t_vehicle_registration_dtls "
			+ "SET "
			+ "`Registration_Date` = ?, "
			+ "`Expiry_Date`=?, "
			+ "`App_Approval_Date`=? " + "WHERE " + "Application_Number=?;";

	private static final String UPDATE_CONVERSION_DATES = "UPDATE t_vehicle_registration_dtls "
			+ "SET "
			+ "`Vehicle_Conversion_Date` = ? "
			+ "WHERE "
			+ "Application_Number=?;";

	private static final String INSERT_INTO_T_VEHICLE_RENEWAL_DTLS = "INSERT INTO t_vehicle_renewal_dtls "
			+ "(Application_Number, "
			+ "Vehicle_Id, "
			+ "Customer_Id, "
			+ "Renewal_Date, "
			+ "Expiry_Date, "
			+ "Renewal_Duration, "
			+ "Amount, "
			+ "Receipt_No, "
			+ "Receipt_Date, "
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On) "
			+ "SELECT "
			+ "Application_Number, "
			+ "Vehicle_Id, "
			+ "Customer_Id, "
			+ "Expiry_Date, "
			+ "Expiry_Date, "
			+ "Renewal_Duration, "
			+ "Renewal_Amount, "
			+ "Receipt_Number, "
			+ "Receipt_Date, "
			+ "Remarks, "
			+ "?, "
			+ "CURDATE(), "
			+ "Updated_By, "
			+ "Updated_On "
			+ "FROM t_vehicle_application "
			+ "WHERE Application_Number =?";

	private static final String UPDATE_TRANSFER_DATES = "UPDATE t_vehicle_transfer_dtls "
			+ "SET "
			+ "`Sale_Deed_Date` = ? "
			+ "WHERE "
			+ "Application_Number=?;";

	private static final String INSERT_INTO_T_VEHICLE_DUPLICATION_DTLS = "INSERT INTO t_vehicle_duplicate_dtls "
			+ "(Application_Number, "
			+ "Vehicle_Id, "
			+ "Customer_Id, "
			+ "Receipt_No, "
			+ "Receipt_Date,"
			+ "Traffic_Remarks,"
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On) "
			+ "SELECT "
			+ "Application_Number, "
			+ "Vehicle_Id, "
			+ "Customer_Id, "
			+ "Receipt_Number, "
			+ "Receipt_Date, "
			+ "Traffic_Remarks,"
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On "
			+ "FROM t_vehicle_application " + "WHERE Application_Number =?";

	private static final String INSERT_INTO_T_VEHICLE_FITNESS_TEST_REPLACEMENT = "INSERT INTO t_vehicle_fitness_test_replacement "
			+ "(Application_Number, "
			+ "Vehicle_Id, "
			+ "Customer_Id, "
			+ "Receipt_No, "
			+ "Receipt_Date,"
			+ "Traffic_Remarks,"
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On,Duplicate_Issue_Date,Processed_Region_Id,Processed_Base_Id) "
			+ "SELECT "
			+ "Application_Number, "
			+ "Vehicle_Id, "
			+ "Customer_Id, "
			+ "Receipt_Number, "
			+ "Receipt_Date, "
			+ "Traffic_Remarks,"
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On,?,?,?  "
			+ "FROM t_vehicle_application " + "WHERE Application_Number =?";
	
	private static final String UPDATE_DUPLICATION_DATES = "UPDATE t_vehicle_duplicate_dtls "
			+ "SET "
			+ "`Duplicate_Issue_Date` = ? "
			+ "WHERE "
			+ "Application_Number=?;";

	private static final String INSERT_INTO_T_VEHICLE_TRANSFER_DTLS = "INSERT INTO t_vehicle_transfer_dtls "
			+ "(Application_Number, "
			+ "Vehicle_Id, "
			+ "Transferor_Customer_Id, "
			+ "Transferee_Type, "
			+ "Transferee_Customer_Id, "
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On,Receipt_Number,Receipt_Date,Transfer_Type) "

			+ "SELECT "
			+ "Application_Number, "
			+ "Vehicle_Id, "
			+ "Transferor_Customer_Id, "
			+ "Transferee_Type, "
			+ "Transferee_Customer_Id, "
			+ "Remarks, "
			+ "Created_By, "
			+ "Created_On, "
			+ "Updated_By, "
			+ "Updated_On,Receipt_Number,Receipt_Date,Transfer_Type "
			+ "FROM t_vehicle_application " + "WHERE Application_Number = ?;";

	private static final String DIFFERENT_VEHICLE_TYPE = "SELECT "
			+ "a.Description " + "FROM " + "t_vehicle_type_master a "
			+ "WHERE a.Vehicle_Type_Id=?";

	private static final String INSERT_INTO_PRINT_DTLS1 = "INSERT INTO t_print_dtls ( "
			+ "  Application_Number, "
			+ "  Document_Type, "
			+ "  Service_Id, "
			+ "  Jurisdiction_Id, "
			+ "  Identity_Number, "
			+ "  Identity_Id, "
			+ "  Jurisdiction_Type_Id "
			+ ") "
			+ "SELECT "
			+ "  a.Application_Number, "
			+ "  'RC', "
			+ "  (SELECT "
			+ "    Service_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
			+ "  (SELECT "
			+ "    Juris_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
			+ "  d.Vehicle_Number, "
			+ "  a.Vehicle_Id, "
			+ "  (SELECT "
			+ "    Juris_Type_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
			+ "FROM ";

	private static final String INSERT_INTO_PRINT_DTLS2 = "  a "
			+ "  LEFT JOIN `t_vehicle_registration_dtls` d ON d.Vehicle_Reg_Dtls_Id=a.Vehicle_Id "
			+ "WHERE a.Application_Number = ?";

	private static final String GET_VALIDITY_DURATION = "SELECT Renewal_Duration FROM t_vehicle_application a WHERE a.`Application_Number`=?";

	private static final String GET_RECEIPT_DATE = "SELECT a.Receipt_Date FROM `t_payment_dtls` a WHERE a.`Application_Number`=?";

	/*
	 * private static final String GET_CUSTOMER_INFORMATION = "SELECT " +
	 * "CONCAT( " + "b.`First_Name`, " + "' ', " + "b.`Middle_Name`, " + "' ', "
	 * + "b.`Last_Name` " + ") pname, " + "c.`Organization_Name`, " +
	 * "b.`Present_Email`, " + "b.`Present_Phone_No`, " +
	 * "DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate " + "FROM " +
	 * "`t_vehicle_registration_dtls` a " + "LEFT JOIN t_personal_dtls b " +
	 * "ON a.`Customer_Id` = b.`Customer_Id` " +
	 * "LEFT JOIN  `t_organization_info` c " +
	 * "ON a.`Customer_Id` = c.`Customer_Id` " +
	 * "WHERE a.`Application_Number` = ?";
	 */

	private static final String GET_PERSONAL_CUSTOMER_INFORMATION = "SELECT COUNT( b.`First_Name`),"
			+ " COUNT( b.`First_Name`),"
			+ "  CONCAT( "
			+ "    b.`First_Name`, "
			+ "    ' ', "
			+ "    b.`Middle_Name`, "
			+ "    ' ', "
			+ "    b.`Last_Name` "
			+ "  ) pname, "
			+ "  b.`Present_Email`, "
			+ "  b.`Present_Phone_No`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_renewal_dtls a "
			+ "  LEFT JOIN t_personal_dtls b "
			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_ORGANISATION_CUSTOMER_INFORMATION = "SELECT "
			+ "  b.`Organization_Name`, "
			+ "  b.`Email_Id`, "
			+ "  b.`Phone_Number`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_renewal_dtls a "
			+ "  LEFT JOIN t_organization_info b "
			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_RENEWAL_DETAILS = "SELECT a.Vehicle_Id,"
			+ "a.`Customer_Id` " + "FROM `t_vehicle_renewal_dtls` a "
			+ "WHERE `Application_Number` = ?";

	private static final String GET_REGISTRATION_DETAILS = "SELECT "
			+ "a.`Customer_Id`,a.Vehicle_Reg_Dtls_Id "
			+ "FROM `t_vehicle_registration_dtls` a "
			+ "WHERE `Application_Number` = ?";

	private static final String GET_TRANSFER_DETAILS = "SELECT "
			+ "a.`Transferee_Customer_Id` "
			+ "FROM `t_vehicle_transfer_dtls` a "
			+ "WHERE `Application_Number` = ?";

	private static final String INSERT_INTO_RENEWAL_UPON_CONVERSION = "INSERT INTO `t_vehicle_renewal_dtls` ( "
			+ "  `Application_Number`, "
			+ "  `Customer_Id`, "
			+ "  `Renewal_Date`, "
			+ "  `Receipt_No`, "
			+ "  `Receipt_Date`, "
			+ "  `Created_By`, "
			+ "  `Created_On` "
			+ ") "
			+ "SELECT "
			+ "  a.`Application_Number`, "
			+ "  a.`Customer_Id`, "
			+ "  a.`Receipt_Date`, "
			+ "  a.`Receipt_Number`, "
			+ "  a.`Receipt_Date`, "
			+ "  a.`Created_By`, "
			+ "  a.`Created_On` "
			+ "FROM "
			+ "  t_vehicle_application a "
			+ "WHERE a.Application_Number = ?";

	private static final String UPDATE_T_RENEWAL_DTLS_UPON_CONVERSION = "UPDATE "
			+ "  t_vehicle_renewal_dtls a "
			+ "SET "
			+ "  a.`Expiry_Date` = ?, "
			+ "  a.`Renewal_Duration` = '12', "
			+ "  a.`Remarks` = 'UPDATED BY SYSTEM UPON CONVERSION', "
			+ "  a.`Processed_Region_Id` = ?, "
			+ "  a.`Processed_Base_Id` = ?, a.Vehicle_Id = ? "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_PERSONAL_REGISTRATION_CUSTOMER_INFORMATION = "SELECT "
			+ "  a.`Vehicle_Number`, "
			+ "  CONCAT( "
			+ "    b.`First_Name`, "
			+ "    ' ', "
			+ "    b.`Middle_Name`, "
			+ "    ' ', "
			+ "    b.`Last_Name` "
			+ "  ) pname, "
			+ "  b.`Present_Email`, "
			+ "  b.`Present_Phone_No`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_registration_dtls a "
			+ "  LEFT JOIN t_personal_dtls b "
			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_ORGANISATION_REGISTRATION_CUSTOMER_INFORMATION = "SELECT "
			+ "  a.`Vehicle_Number`, "
			+ "  b.`Organization_Name`, "
			+ "  b.`Email_Id`, "
			+ "  b.`Phone_Number`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_registration_dtls a "
			+ "  LEFT JOIN t_organization_info b "
			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_TRANSFER_PERSONAL_CUSTOMER_INFORMATION = "SELECT COUNT( b.`First_Name`), "
			+ "COUNT( b.`First_Name`), "
			+ "CONCAT( "
			+ "b.`First_Name`, "
			+ "' ', "
			+ "b.`Middle_Name`, "
			+ "' ', "
			+ "b.`Last_Name` "
			+ ") pname, "
			+ "b.`Present_Email`, "
			+ "b.`Present_Phone_No`, "
			+ "DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "t_vehicle_transfer_dtls a "
			+ "LEFT JOIN t_personal_dtls b "
			+ "ON a.`Transferee_Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_TRANSFER_ORGANISATION_CUSTOMER_INFORMATION = "SELECT "
			+ "  b.`Organization_Name`, "
			+ "  b.`Email_Id`, "
			+ "  b.`Phone_Number`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_transfer_dtls a "
			+ "  LEFT JOIN t_organization_info b "
			+ "    ON a.`Transferee_Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_DUPLICATION_DETAILS = "SELECT a.Vehicle_Id,"
			+ "a.`Customer_Id` " + "FROM `t_vehicle_duplicate_dtls` a "
			+ "WHERE `Application_Number` = ?";

	private static final String GET_PERSONAL_DUPLICATION_CUSTOMER_INFORMATION = "SELECT COUNT( b.`First_Name`),"
			+ " COUNT( b.`First_Name`),"
			+ "  CONCAT( "
			+ "    b.`First_Name`, "
			+ "    ' ', "
			+ "    b.`Middle_Name`, "
			+ "    ' ', "
			+ "    b.`Last_Name` "
			+ "  ) pname, "
			+ "  b.`Present_Email`, "
			+ "  b.`Present_Phone_No`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_duplicate_dtls a "
			+ "  LEFT JOIN t_personal_dtls b "
			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_ORGANISATION_DUPLICATION_CUSTOMER_INFORMATION = "SELECT "
			+ "  b.`Organization_Name`, "
			+ "  b.`Email_Id`, "
			+ "  b.`Phone_Number`, "
			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
			+ "FROM "
			+ "  t_vehicle_duplicate_dtls a "
			+ "  LEFT JOIN t_organization_info b "
			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
			+ "WHERE a.`Application_Number` = ?";

	private static final String GET_REGISTRATION_TYPE = "SELECT Registration_Type_Id FROM t_registration_type_master WHERE Description='CONVERSION'";

	private static final String GET_CONVERSION_VEHICLE_NUMBER = "SELECT * FROM t_vehicle_registration_dtls a WHERE a.Vehicle_Number=?";

	private static final String UPDATE_CONVERSION_DETAILS = "UPDATE t_vehicle_registration_dtls "
			+ "SET "
			+ "Vanity_Number = ?, "
			+ "Vehicle_Company_Id = ?, "
			+ "Vehicle_Model_Id = ?, "
			+ "Engine_Type_Id = ?, "
			+ "Engine_Number = ?, "
			+ "Chassis_Number = ?, "
			+ "STATUS = ?, "
			+ "Colour = ?, "
			+ "Price = ?, "
			+ "Load_Capacity = ?, "
			+ "Dealer_Id = ?, "
			+ "Seat_Capacity = ?, "
			+ "Vehicle_Horse_Power =?, "
			+ "Vehicle_Kilowatt = ?, "
			+ "Hypothecated_Id = ?, "
			+ "Unladen_Weight = ?, "
			// + "Letter_No = ?, "
			// + "Letter_Date = ?, "
			// + "Purchase_Date = ?, "
			+ "Manufacture_Year = ?, "
			+ "Engine_CC = ?, "
			+ "Manufacture_Country_Id = ?, "
			+ "Remarks = ?, "
			+ "Purchase_Type = ? " + "WHERE Application_Number = ?";

	private static final String INSERT_INTO_T_VEHICLE_CANCELLATION_DTLS = "INSERT INTO "
			+ "`t_vehicle_cancellation_dtls` "
			+ "(`Vehicle_Id`,"
			+ "`Customer_Id`,"
			+ "`Cancellation_Date`,"
			+ "`Cancellation_Reason_Id`,"
			+ "`Created_By`,"
			+ "`Created_On`) "
			+ "VALUES "
			+ "(?,"
			+ "?,"
			+ "?,"
			+ "(SELECT `Conversion_Reason_Id` FROM `t_vehicle_application` WHERE `Application_Number` = ?),"
			+ "?," + "?)";

	private static final String GET_PREVIOUS_VEHICLE_NUMBER = "SELECT "
			+ "a.Vehicle_Number " + "FROM t_vehicle_registration_dtls a "
			+ "WHERE Application_Number = ?";

	private static final String UPDATE_EXPIRY_DATE = "UPDATE "
			+ " t_vehicle_renewal_dtls " + "SET " + "`Expiry_Date` = ? "
			+ "WHERE Application_Number = ?";

	private static final String UPDATE_TRAFFIC_POLICE_REMARKS = "UPDATE `t_vehicle_application` "
			+ "SET "
			+ "  `Traffic_Remarks` = ? "
			+ "WHERE `Application_Number` = ?";

	private static final String INSERT_INTO_CONVERSION = "INSERT INTO `t_vehicle_conversion` "
			+ "  (`New_Application_Number`, "
			+ "   `Old_Vehicle_Id`) "
			+ "VALUES (?, " + "        ?);";

	private static final String INSERT_VEH_TRANSFER_DTLS_INTO_VEH_REG = "INSERT INTO  `t_vehicle_registration_dtls` "
			+ "            ( "
			+ "             `Application_Number`, "
			+ "             `Vehicle_Registration_Type`, "
			+ "             `Customer_Id`, "
			+ "             `Registration_Type`, "
			+ "             `Amount`, "
			+ "             `Vehicle_Registration_Id`, "
			+ "             `Region_Id`, "
			+ "             `Vehicle_Prefix`, "
			+ "             `Vehicle_Number`, "
			+ "             `Vanity_Number`, "
			+ "             `Vehicle_Company_Id`, "
			+ "             `Registration_Date`, "
			+ "             `Vehicle_Model_Id`, "
			+ "             `Expiry_Date`, "
			+ "             `Engine_Type_Id`, "
			+ "             `Engine_Number`, "
			+ "             `Chassis_Number`, "
			+ "             `Receipt_Date`, "
			+ "             `Receipt_Number`, "
			+ "             `Status`, "
			+ "             `Colour`, "
			+ "             `Price`, "
			+ "             `Load_Capacity`, "
			+ "             `Dealer_Id`, "
			+ "             `Seat_Capacity`, "
			+ "             `Vehicle_Horse_Power`, "
			+ "             `Vehicle_Kilowatt`, "
			+ "             `Hypothecated_Id`, "
			+ "             `Unladen_Weight`, "
			+ "             `Letter_No`, "
			+ "             `Letter_Date`, "
			+ "             `Purchase_Date`, "
			+ "             `Manufacture_Year`, "
			+ "             `Engine_CC`, "
			+ "             `Manufacture_Country_Id`, "
			+ "             `Remarks`, "
			+ "             `Purchase_Type`, "
			+ "             `Vehicle_Renewal_Date`, "
			+ "             `Vehicle_Transfer_Date`, "
			+ "             `Vehicle_Duplicate_Date`, "
			+ "             `Vehicle_Cancellation_Date`, "
			+ "             `Vehicle_Conversion_Date` "
			+ "            ) "
			+ "             "
			+ "SELECT "
			+ "  a.`Application_Number`, "
			+ "             b.`Vehicle_Registration_Type`, "
			+ "             b.`Customer_Id`, "
			+ "             b.`Registration_Type`, "
			+ "             b.`Amount`, "
			+ "             b.`Vehicle_Registration_Id`, "
			+ "             a.`Region_Id`, "
			+ "             b.`Vehicle_Prefix`, "
			+ "             b.`Vehicle_Number`, "
			+ "             b.`Vanity_Number`, "
			+ "             b.`Vehicle_Company_Id`, "
			+ "             b.`Registration_Date`, "
			+ "             b.`Vehicle_Model_Id`, "
			+ "             b.`Expiry_Date`, "
			+ "             b.`Engine_Type_Id`, "
			+ "             b.`Engine_Number`, "
			+ "             b.`Chassis_Number`, "
			+ "             b.`Receipt_Date`, "
			+ "             b.`Receipt_Number`, "
			+ "             b.`Status`, "
			+ "             b.`Colour`, "
			+ "             b.`Price`, "
			+ "             b.`Load_Capacity`, "
			+ "             b.`Dealer_Id`, "
			+ "             b.`Seat_Capacity`, "
			+ "             b.`Vehicle_Horse_Power`, "
			+ "             b.`Vehicle_Kilowatt`, "
			+ "             b.`Hypothecated_Id`, "
			+ "             b.`Unladen_Weight`, "
			+ "             b.`Letter_No`, "
			+ "             b.`Letter_Date`, "
			+ "             b.`Purchase_Date`, "
			+ "             b.`Manufacture_Year`, "
			+ "             b.`Engine_CC`, "
			+ "             b.`Manufacture_Country_Id`, "
			+ "             b.`Remarks`, "
			+ "             b.`Purchase_Type`, "
			+ "             b.`Vehicle_Renewal_Date`, "
			+ "             b.`Vehicle_Transfer_Date`, "
			+ "             b.`Vehicle_Duplicate_Date`, "
			+ "             b.`Vehicle_Cancellation_Date`, "
			+ "             b.`Vehicle_Conversion_Date` "
			+ "FROM "
			+ "  `t_vehicle_application` a "
			+ "  LEFT JOIN `t_vehicle_registration_dtls` b "
			+ "    ON b.`Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id` WHERE a.`Application_Number`=?";

	private static final String UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_ID_IN_RENEWAL = "UPDATE `t_vehicle_renewal_dtls` SET `Vehicle_Id`=? WHERE vehicle_id=?;";

	private static final String UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_ID_IN_EMISSION_TEST = "UPDATE `t_vehicle_emission_test_dtls` SET `Vehicle_Id`=? WHERE `Vehicle_Id`=?";

	private static final String UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_IN_FITNESS_TEST = "UPDATE `t_vehicle_fitness_test_dtls` SET `Vehicle_Id`=? WHERE `Vehicle_Id`=?;";

	private static final String UPDATE_NEW_VEHICLE_ID_AGAINST_OLD_VEHICLE_IN_FITNESS_ITEM = "UPDATE `t_vehicle_fitness_item_dtls` SET `Vehicle_Id`=? WHERE `Vehicle_Id`=?;";

	private static final String UPDATE_CONVERTED_NEW_VEHICLE_NO_IN_VEHICLE_APPLICATION = "UPDATE `t_vehicle_application` SET `Vehicle_Number` =? WHERE `Application_Number`=?";

	private static final String UPDATE_TRANSFEREE_CUSTOMER_ID_IN_VEHICLE_REG = "UPDATE `t_vehicle_registration_dtls` SET `Customer_Id`=?,Vehicle_Registration_Type=? WHERE `Vehicle_Reg_Dtls_Id`=?";

	private static final String UPDATE_TRANSFEREE_CUSTOMER_ID_IN_VEHICLE_RENEWAL = "UPDATE `t_vehicle_renewal_dtls` SET `Customer_Id`=? WHERE `Vehicle_Id`=?";

	private static final String GET_EMISSION_TEST_DTLS = "SELECT "
			+ "  COUNT(*) rowCount, " + "  a.`Vehicle_Number`, "
			+ "  a.`Chassis_Number`, " + "  a.`Engine_Number`, "
			+ "  DATE_FORMAT( " + "    a.`Registration_Date`, "
			+ "    '%d/%m/%Y' " + "  ) Registration_Date, "
			+ "  c.`Vehicle_Model_Name`, " + "  b.`CO`, " + "  b.`HSU`, "
			+ "  b.`Emission_Test_Type`, " + "  b.`Test_Location`, "
			+ "  DATE_FORMAT(b.`Validity`, '%d/%m/%Y') Validity, "
			+ "  DATE_FORMAT(b.`Tested_On`, '%d/%m/%Y') Tested_On, "
			+ "  b.T1, " + "  b.T2, " + "  b.T3, "
			+ "  d.`Vehicle_Company_Name`, "
			+ "  e.`Vehicle_Type_Name`,b.`Price`,b.`Test_Result` " + "FROM "
			+ "  `t_vehicle_registration_dtls` a "
			+ "  LEFT JOIN `t_vehicle_emission_test_dtls` b "
			+ "    ON b.`Vehicle_Id` = a.`Vehicle_Reg_Dtls_Id` "
			+ "  LEFT JOIN `t_vehicle_model_master` c "
			+ "    ON c.`Vehicle_Model_Id` = a.`Vehicle_Model_Id` "
			+ "  LEFT JOIN t_vehicle_company_master d "
			+ "    ON a.`Vehicle_Company_Id` = d.`Vehicle_Company_Id` "
			+ "  LEFT JOIN `t_vehicle_type_master` e "
			+ "    ON a.`Vehicle_Type_Id` = e.`Vehicle_Type_Id` "
			+ "WHERE b.Emission_Test_Id = ?";

	private static final String UPDATE_EMISSION_RETESTED = "UPDATE `t_vehicle_emission_test_dtls` "
			+ "SET " + "  `Is_Retested` = 'Y' " + "WHERE `Vehicle_Id`=?;";

	private static final String CHECK_IF_FITNESS_FIRST_TIME = "SELECT COUNT(*) rowCount FROM `t_vehicle_fitness_test_dtls` a WHERE a.`Vehicle_Id`=?";

	private static final String GET_VEHICLE_REGISTARTION_DATE = "SELECT a.`Registration_Date` FROM `t_vehicle_registration_dtls` a WHERE a.`Vehicle_Reg_Dtls_Id`=?";

	private static final String INSERT_INTO_T_VEHICLE_CANCELLATION_AUDIT = "INSERT INTO `t_vehicle_cancellation_audit` ( "
			+ "  Vehicle_Cancellation_Id,`Vehicle_Id`, "
			+ "  `Customer_Id`, "
			+ "  `Cancellation_Date`, "
			+ "  `Cancellation_Reason_Id`, "
			+ "  `Receipt_No`, "
			+ "  `Receipt_Date`, "
			+ "  `Processed_Region_Id`, "
			+ "  `Processed_Base_Id`, "
			+ "  `Created_By`, "
			+ "  `Created_On`, "
			+ "  `Updated_By`, "
			+ "  `Updated_On`, "
			+ "  `Withdrawn_Date`, "
			+ "  `Withdrawn_Reason` "
			+ ") "
			+ "(SELECT "
			+ "  Vehicle_Cancellation_Id,`Vehicle_Id`, "
			+ "  `Customer_Id`, "
			+ "  `Cancellation_Date`, "
			+ "  `Cancellation_Reason_Id`, "
			+ "  `Receipt_No`, "
			+ "  `Receipt_Date`, "
			+ "  `Processed_Region_Id`, "
			+ "  `Processed_Base_Id`, "
			+ "  `Created_By`, "
			+ "  `Created_On`, "
			+ "  `Updated_By`, "
			+ "  `Updated_On`, "
			+ "  ?, "
			+ "  ? "
			+ "FROM "
			+ "  `t_vehicle_cancellation_dtls` " + "WHERE `Vehicle_Id` = ?)";

	private static final String DELETE_FROM_T_VEHICLE_CANCEALLTION_DTLS = "DELETE FROM `t_vehicle_cancellation_dtls` WHERE `Vehicle_Id`=?";

	private static final String GET_VEHICLE_MODEL_DTLS = "SELECT COUNT(a.`Vehicle_Model_Id`) rowCount,"
			+ "a.`Engine_CC_Kilowatt_Horsepower`,a.`Seating_Capacity`,a.`Unladen_Weight`"
			+ " FROM `t_vehicle_model_master` a"
			+ " WHERE a.`Vehicle_Model_Id`=?";

	private static final String GET_LICENSE_DTLS_FOR_ACCIDENT = "SELECT  CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`)driverName,b.`CID_Number`,date_format(b.`Date_Of_Birth`,'%d/%m/%Y') Date_Of_Birth,b.`Present_Phone_No`,b.`Gender` FROM t_driving_license_dtls a "
			+ "LEFT JOIN t_personal_dtls b ON a.`Customer_Id`=b.`Customer_Id` "
			+ "WHERE a.`Driving_License_No`=?";

	private static final String GET_LEARNER_DTLS_FOR_ACCIDENT = "SELECT  CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`)driverName,b.`CID_Number`,date_format(b.`Date_Of_Birth`,'%d/%m/%Y') Date_Of_Birth,b.`Present_Phone_No`,b.`Gender` FROM t_learner_license_dtls a "
			+ "LEFT JOIN t_personal_dtls b ON a.`Customer_Id`=b.`Customer_Id` "
			+ "WHERE a.`Learner_License_No`=?";

	private static final String GET_VEHICLE_DTLS_FOR_ACCIDENT = "SELECT b.`Vehicle_Model_Id`,b.`Vehicle_Company_Id` FROM t_vehicle_registration_dtls a "
			+ " LEFT JOIN t_vehicle_model_master b ON a.`Vehicle_Model_Id`=b.`Vehicle_Model_Id` "
			+ " WHERE " + " a.Vehicle_Number=? AND a.`Vehicle_Type_Id`=?";

	
	private static final String INSERT_PASSENGER_BUS_INSPECTION = "INSERT INTO `t_passenger_bus_inspection_dtls` "+
																    "( "+
																    " `Vehicle_Id`, "+
																    " `License_Id`, "+
																    " `Inspection_Type`, "+
																    " `Is_Drug_Abuse`, "+
																    " `Alcohol_Test`, "+
																    " `Departure_From`, "+
																    " `Destination`, "+
																    " `Departure_Time`, "+
																    " `Departure_Date`, "+
																    " `Arrival_Time`, "+
																    " `Arrival_Date`, "+
																    " `Total_Male`, "+
																    " `Total_Female`, "+
																    " `Total_Children`, "+
																    " `Created_By`,`Juris_Type_Id`,`Juris_Id`) "+
																	" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
	
	
	/*private static final String GET_PASSENGER_BUS_ROUTE_PERMIT_APPLICATION_DTLS	="SELECT	CID,"+
																		"  `Applicant_Name`,"+
																		" `DOB`,"+
																		"  `Gender`,"+
																		"  `Permanent_Dzongkhag`,"+
																		"  `Permanenet_Gewog`,"+
																		"  `Permanent_Village`,"+
																		"  `Phone_No`,"+
																		"  `Email_Id`,"+
																		"  `Present_Address`,"+
																		"  `Route_Type`"+
																		"	FROM `t_passenger_bus_route_permit_application`  WHERE Application_Number=?";
	*/
	
	
	
	
	
	private static final String INSERT_PASSENGER_BUS_APPLICANT_IN_PIS	= "INSERT INTO `t_personal_dtls`"+
														            "(`Customer_Id`,"+
														            " `First_Name` ,"+
														            " `Nationality_Id`,"+
														            " `Date_Of_Birth`,"+
														            " `CID_Number`,"+
														            " `Gender`,"+
														            " `Is_International`,"+
														            " `Permanent_Dzongkhag_Id`,"+
														            " `Permanent_Gewog_Id`,"+
														            " `Permanant_Village_Id`,"+
														            " `Present_Contact_Address`,"+
														            " `Present_Phone_No`,"+
														            " `Present_Email`,"+
														            " `Created_By`)"+
														            " (SELECT ?,"+
																	  "`Applicant_Name`,'2',"+
																	 " `DOB`,CID,"+
																	 " `Gender`,'N',"+
																	 " `Permanent_Dzongkhag`,"+
																	 " `Permanenet_Gewog`,"+
																	 " `Permanent_Village`,"+
																	 " `Present_Address` ,"+
																	 " `Phone_No`,"+
																	 " `Email_Id`,"+
																	 " ?"+
																	 "	FROM `t_passenger_bus_route_permit_application` WHERE Application_Number=?)	";
	
	
	private static final String CHECK_IF_CID_EXISTED_IN_PIS	= "SELECT COUNT(*)rowCount FROM t_personal_dtls a WHERE "+
													" a.`CID_Number` = (SELECT CID FROM `t_passenger_bus_route_permit_application` "+
													" WHERE Application_Number=?) ";
	
	
	
	
	private static final String INSERT_INTO_T_PESSENGER_BUS_ROUTE_PERMIT_DTLS	= "INSERT INTO `t_passenger_bus_route_permit_dtls`"+
										    "       (`Application_Number`,"+
										    "         `Applicant_Name`,"+
										    "         `DOB`,"+
										    "         `Gender`,"+
										    "         `Permanent_Dzongkhag`,"+
										    "         `Permanenet_Gewog`,"+
										    "         `Permanent_Village`,"+
										    "         `Phone_No`,"+
										    "         `Email_Id`,"+
										    "         `Present_Address`,"+
										    "         `Route_Type`,"+
										    "         `Bus_Category_Id`,"+
										    "         `Route_From`,"+
										    "         `Route_To`,"+
										    "         `Timing`,"+
										    "         `Transport_Name`,"+
										    "         `Process_Region_Id`,"+
										    "         `Process_Base_Id`,"+
										    "         `File_Path`,"+
										    "         `File_Name`,"+
										    "         `Created_By`,"+
										    "         `Created_On`,"+
										    "         `App_Submission_Date`,"+
										    "         `App_Verification_Date`,"+
										    "         `App_Approval_Date`,"+
										    "         `Application_Type`,"+
										    "         `CID`)"+
										    "         (SELECT `Application_Number`,"+
										    "         `Applicant_Name`,"+
										    "         `DOB`,"+
										    "         `Gender`,"+
										    "         `Permanent_Dzongkhag`,"+
										    "         `Permanenet_Gewog`,"+
										    "         `Permanent_Village`,"+
										    "         `Phone_No`,"+
										    "         `Email_Id`,"+
										    "         `Present_Address`,"+
										    "         `Route_Type`,"+
										    "         `Bus_Category_Id`,"+
										    "         `Route_From`,"+
										    "         `Route_To`,"+
										    "         `Timing`,"+
										    "         `Transport_Name`,"+
										    "         `Process_Region_Id`,"+
										    "         `Process_Base_Id`,"+
										    "         `File_Path`,"+
										    "         `File_Name`,"+
										    "         `Created_By`,"+
										    "         `Created_On`,"+
										    "         `App_Submission_Date`,"+
										    "         `App_Verification_Date`,"+
										    "         `App_Approval_Date`,"+
										    "         `Application_Type`,"+
										    "         `CID` FROM t_passenger_bus_route_permit_application WHERE Application_Number=?)";
	
	private static final String CHECK_APPLICATION_APPROVED ="SELECT COUNT(a.`Status_Id`) rowCount FROM t_workflow_dtls a " +
	" WHERE a.`Application_Number`=? AND a.`Status_Id` IN ('1','11','4')";
	
	
	private static final String VALIDATE_ENGINENO_CHASIS = "select (select count(*) from t_vehicle_registration_dtls a " +
														"where a.Chassis_Number=?) countChassisNo,"
														+"(select count(*) from t_vehicle_registration_dtls a where  a.Engine_Number=?) countEngineNo";

	private static final String GET_VEHICLE_PREVIOUS_PAYMENT = "SELECT "
											+ "  IF(b.Expiry_Date IS NULL, "
											+ "  (SELECT c.Amount_Paid FROM t_payment_dtls c WHERE c.Application_Number=a.Application_Number), "
											+ " (SELECT c.Amount_Paid FROM t_payment_dtls c WHERE c.Application_Number=b.Application_Number))" 
											+ " previousPayment ," 
											+" IF(b.Expiry_Date IS NULL,"
											+"(SELECT d.Renewal_Duration FROM t_vehicle_application d WHERE d.Application_Number=a.Application_Number),"
											+"(SELECT d.Renewal_Duration FROM t_vehicle_application d WHERE d.Application_Number=b.Application_Number)) renewalDuration"
											+ " FROM "
											+ "  t_vehicle_registration_dtls a "
											+ "  LEFT JOIN t_vehicle_renewal_dtls b "
											+ "    ON a.Vehicle_Reg_Dtls_Id = b.Vehicle_Id "
											+ " WHERE a.Vehicle_Reg_Dtls_Id = ? "
											+ " ORDER BY b.Expiry_Date DESC "
											+ " LIMIT 1 ;";
									
	
	private static final String COUNT_VEHICLE_FITNESS_NA = "SELECT COUNT(*)rowCount FROM t_vehicle_fitness_test_dtls a WHERE a.Receipt_Number='NA' AND a.Vehicle_Id=?";

	
}