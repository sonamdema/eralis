package bt.gov.rsta.eralis.dao.eralis_common;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.OffenceDTO;
import bt.gov.rsta.eralis.dto.noc.NOCDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.web.actionform.eralis_common.EralisCommonActionForm;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.ConnectionManagerAggregator;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class EralisCommonDAO {
	
	private static EralisCommonDAO eraliscommonDAO;
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
	
	public static EralisCommonDAO getInstance()
	{
		if(eraliscommonDAO == null)
			eraliscommonDAO = new EralisCommonDAO();
		
		return eraliscommonDAO;
	}
	
	public String noc_issuance(EralisCommonDTO dto, String jurisId, String regionId, String baseId, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String nocNumber = null;
		
		try 
		{
			if(conn != null)
			{ 
				Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDateStr = targetSdf.format(receiptDate);
				
				Date saleDeedDate = sourceSdf.parse(dto.getSaleDeedDate());
				String saleDeedDateStr = targetSdf.format(saleDeedDate);
				
				String nocId = EralisCommonUtil.generateNOCFormat(jurisId, conn);
				
				pst = conn.prepareStatement(INSERT_INTO_NOC_DTLS);
				pst.setString(1, nocId);
				pst.setString(2, dto.getOwnerType());
				pst.setString(3, dto.getCustomerID());
				pst.setString(4, dto.getVehicleId());
				pst.setString(5, dto.getBuyerName());
				pst.setString(6, dto.getBuyerAddress());
				pst.setString(7, dto.getReceiptNo());
				pst.setString(8, receiptDateStr);
				pst.setString(9, dto.getNocFees());
				pst.setString(10, dto.getAmount());
				pst.setString(11, saleDeedDateStr);
				pst.setString(12, dto.getSaleDeedAmount());
				pst.setString(13, jurisId);
				pst.setDate(14, date);
				pst.setString(15, regionId);
				pst.setString(16, baseId);
				pst.setString(17, dto.getBuyerCID());
				int count = pst.executeUpdate();
				
				if(count > 0)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_VEHICLE_CANCELLATION_DTLS);
					pst.setString(1, dto.getVehicleId());
					pst.setString(2, dto.getCustomerID());
					pst.setDate(3, date);
					pst.setString(4, dto.getReceiptNo());
					pst.setString(5, receiptDateStr);
					pst.setString(6, "SYSTEM");
					pst.setDate(7, date);
					
					count = pst.executeUpdate();  
					
					if(count > 0)
						nocNumber = nocId;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			nocNumber = null;
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return nocNumber;
	}
	
	public NOCDTO get_noc_dtls(String nocNo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		NOCDTO dto = new NOCDTO();
		String vehicleId = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				dto.setNocNumber(nocNo);
				
				pst = conn.prepareStatement(GET_NOC_DETAILS);
				pst.setString(1, nocNo);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setBuyerName(rs.getString("Buyer_Name"));
					dto.setBuyerAddress(rs.getString("Buyer_Address"));
					dto.setReceiptNo(rs.getString("Receipt_Number"));
					dto.setReceiptDate(rs.getString("receiptDate"));
					dto.setCancellationDate(rs.getString("cancellationDate"));
					dto.setAmountPaid(rs.getString("Amount"));
					dto.setOfficeName(rs.getString("region"));
					dto.setBuyerCID(rs.getString("Buyer_CID"));
					vehicleId = rs.getString("Vehicle_Id");
					String ownerType = rs.getString("Seller_Type");
					String customerId = rs.getString("Seller_Customer_Id");
					
					if(ownerType.equalsIgnoreCase("P"))
					{
						pst = conn.prepareStatement(GET_PERSONAL_DETAILS);
						pst.setString(1, customerId);
						rs = pst.executeQuery();
						rs.first();
						
						dto.setSellerName(rs.getString("pname"));
					}
					else
					{
						String organizationName = null, department = null, ministry = null;
						
						pst = conn.prepareStatement(GET_ORGANIZATION_DETAILS);
						pst.setString(1, customerId);
						rs = pst.executeQuery();
						rs.first();
						
						if(rs.getString("Organization_Name") != null)
							organizationName = rs.getString("Organization_Name");
						else
							organizationName = "";
						
						if(rs.getString("department") != null)
							department = rs.getString("department");
						else
							department = "";
						
						if(rs.getString("ministry") != null)
							ministry = rs.getString("ministry");
						else
							ministry = "";
						
						dto.setSellerName(organizationName+" "+department+" "+ministry);
					}
					
					pst = conn.prepareStatement(GET_LAST_RENEWED_UPTO_DATE);
					pst.setString(1, vehicleId);
					pst.setString(2, vehicleId);
					pst.setString(3, vehicleId);
					rs = pst.executeQuery();
					rs.first();
					dto.setLastRenewalDate(rs.getString("expiryDate"));
					
					pst = conn.prepareStatement(GET_VEHICLE_DETAILS);
					pst.setString(1, vehicleId);
					rs = pst.executeQuery();
					rs.first();
					dto.setModel(rs.getString("vehicleModel"));
					dto.setMake(rs.getString("make"));
					dto.setEngineNo(rs.getString("Engine_Number"));
					dto.setChassisNo(rs.getString("Chassis_Number"));
					dto.setManufactureYear(rs.getString("Manufacture_Year"));
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public synchronized String personal_info(EralisCommonDTO dto, UserDetailsVO userVo, Connection conn) throws Exception
	{
		String customerId = null;
		PreparedStatement pst = null;
		String result = "FAILURE";
		DocumentVO vo = new DocumentVO();
		
		try 
		{
			customerId = EralisCommonUtil.generateCustomerCodeFormat("P", conn);
			
			if(null != dto.getUpload() && 0 != dto.getUpload().getFileSize())
			{
				vo.setFileContent(dto.getUpload().getFileData());
				vo.setName(dto.getUpload().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().imageUploader(vo, customerId);
			}
			
			/*if(vo != null)
			{*/
				pst = conn.prepareStatement(INSERT_PERSONAL_INFO_DETAILS);
				pst.setString(1, customerId);
				pst.setString(2, dto.getTitleOfcourtesy());
				pst.setString(3, dto.getFirstname());
				pst.setString(4, dto.getMiddlename());
				pst.setString(5, dto.getLastName());
				pst.setString(6, dto.getOccupation());
				pst.setString(7, dto.getNationality());
				pst.setString(8, dto.getCID());
				
				if(dto.getManualFlag().equals("N")){
					pst.setString(9, dto.getDOB());
				} else {
					Date dobDate = sourceSdf.parse(dto.getDOB());
					String dob = targetSdf.format(dobDate);
					pst.setString(9, dob);
				}
					
				pst.setString(10, dto.getBloodGroup());
				pst.setString(11, dto.getFathersName());
				pst.setString(12,dto.getGender());
				pst.setString(13, dto.getIdentificationMarks());
				pst.setString(14, dto.getRemarks());
				
				if(dto.getNational().equals("N"))
					pst.setString(15, "N");
				else
					pst.setString(15, "Y");
				
				if(null != dto.getDzongkhag())
					pst.setString(16, dto.getDzongkhag());
				else
					pst.setString(16, "0");
				
				if(null != dto.getGewog())
					pst.setString(17, dto.getGewog());
				else
					pst.setString(17, "0");
				
				pst.setString(18,dto.getVillage());
				
				if(null != dto.getCountry())
					pst.setString(19,dto.getCountry());
				else
					pst.setString(19, "0");
				
				pst.setString(20,dto.getAddress());
				pst.setString(21, dto.getPresentDzongkhag());
				pst.setString(22, dto.getContactAddress());
				pst.setString(23, dto.getPhone());
				pst.setString(24, dto.getEmail());
				pst.setString(25, vo.getUploadURL());
				pst.setString(26, vo.getUuid());
				pst.setString(27, userVo.getActorId()+"("+userVo.getActorName()+")");
				pst.setDate(28, date);
				pst.setString(29, dto.getContactRadio());
				pst.setString(30, dto.getMinistry());
				pst.setString(31, dto.getDepartment());
				pst.setString(32, dto.getGuardianNo());
				pst.setString(33, dto.getMothersName());
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "SUCCESS";
			//}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		return result+'#'+customerId;
	}

	public synchronized String edit_personal_info(EralisCommonDTO dto, UserDetailsVO userVo, Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		DocumentVO vo = new DocumentVO();
		
		try 
		{
			pst = conn.prepareStatement(GET_OLD_IMAGE_PATH);
			pst.setString(1, dto.getPersonalInfoId());
			rs = pst.executeQuery();
			rs.first();
			String imagePath = rs.getString("Image_Path");
			
			if(null != dto.getUpload() && dto.getUpload().getFileSize() > 0)
			{
				if(null != imagePath)
				{
					File file = new File(imagePath);
					
					if(file.exists())
					{
						file.delete();
						
						vo.setFileContent(dto.getUpload().getFileData());
						vo.setName(dto.getUpload().getFileName());
						vo.setDocumentType("IMAGE");
						vo = CommonDAO.getInstance().imageUploader(vo, dto.getCustomerID());
					}
				}
				else
				{
					vo.setFileContent(dto.getUpload().getFileData());
					vo.setName(dto.getUpload().getFileName());
					vo.setDocumentType("IMAGE");
					vo = CommonDAO.getInstance().imageUploader(vo, dto.getCustomerID());
				}
			}
			
			pst = conn.prepareStatement(UPDATE_PERSONAL_INFO);
			pst.setString(1, dto.getTitleOfcourtesy());
			pst.setString(2, dto.getFirstname());
			pst.setString(3, dto.getMiddlename());
			pst.setString(4, dto.getLastName());
			pst.setString(5, dto.getOccupation());
			pst.setString(6, dto.getNationality());
			Date dobDate = sourceSdf.parse(dto.getDOB());
			String dob = targetSdf.format(dobDate);
			pst.setString(7, dob);
			pst.setString(8, dto.getBloodGroup());
			pst.setString(9, dto.getFathersName());
			pst.setString(10, dto.getGender());
			pst.setString(11, dto.getRemarks());
			if(dto.getNational().equals("N"))
				pst.setString(12, "N");
			else
				pst.setString(12, "Y");
			pst.setString(13, dto.getDzongkhag());
			pst.setString(14, dto.getGewog());
			pst.setString(15, dto.getVillage());
			pst.setString(16, dto.getCountry());
			pst.setString(17, dto.getAddress());
			pst.setString(18, dto.getPresentDzongkhag());
			pst.setString(19, dto.getContactAddress());
			pst.setString(20, dto.getPhone());
			pst.setString(21, dto.getEmail());
			
			if(null != dto.getUpload() && dto.getUpload().getFileSize() > 0)
			{
				pst.setString(22, vo.getUploadURL());
				pst.setString(23, vo.getUuid());
			}
			else
			{
				pst.setString(22, imagePath);
				pst.setString(23, vo.getUuid());
			}
			
			pst.setString(24, userVo.getActorId());
			pst.setDate(25, date);
			pst.setString(26, dto.getCID());
			
			pst.setString(27, dto.getContactRadio());
			if(dto.getContactRadio()!=null && dto.getContactRadio().equalsIgnoreCase("Official"))
			{
				pst.setString(28, dto.getMinistry());
				pst.setString(29, dto.getDepartment());
			}
			else
			{
				pst.setString(28, "0");
				pst.setString(29, "0");
			}
			pst.setString(30, dto.getGuardianNo());
			pst.setString(31, dto.getMothersName());
			pst.setString(32, dto.getPersonalInfoId());
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[edit_personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public synchronized String edit_receipt_dtls(EralisCommonDTO dto, UserDetailsVO userVo, Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		DocumentVO vo = new DocumentVO();
		
		try 
		{
			pst = conn.prepareStatement(UPDATE_T_PAYMENT_DTLS);
			java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
			String receiptDateStr = targetSdf.format(receiptDate);
			pst.setString(1,receiptDateStr);
			pst.setString(2, dto.getReceiptNo());
			pst.setString(3, dto.getAmount());
			pst.setString(4, dto.getPenalty());
			pst.setString(5, dto.getApplicationNo());
			int count = pst.executeUpdate();
			if(count > 0)
			{
				if("LL".equalsIgnoreCase(dto.getServiceType()))
				{
					pst = conn.prepareStatement(UPDATE_LEARNER_APPLICATION_RECEIPT);
					pst.setString(1,receiptDateStr);
					pst.setString(2, dto.getReceiptNo());
					pst.setString(3, dto.getApplicationNo());
					pst.executeUpdate();
					result = "SUCCESS";
				}
				else if("DL".equalsIgnoreCase(dto.getServiceType()))
				{
					pst = conn.prepareStatement(UPDATE_LICENSE_APPLICATION_RECEIPT);
					pst.setString(1,receiptDateStr);
					pst.setString(2, dto.getReceiptNo());
					pst.setString(3, dto.getApplicationNo());
					pst.executeUpdate();
					result = "SUCCESS";
				}
				else if("RC".equalsIgnoreCase(dto.getServiceType()))
				{
					pst = conn.prepareStatement(UPDATE_VEHICLE_APPLICATION_RECEIPT);
					pst.setString(1,dto.getAmount());
					pst.setString(2,receiptDateStr);
					pst.setString(3, dto.getReceiptNo());
					pst.setString(4, dto.getApplicationNo());
					pst.executeUpdate();
					result = "SUCCESS";
				}
			}else{
				result	=	"NO_CHANGES";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[edit_personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public synchronized String delete_personal_info(EralisCommonDTO dto, UserDetailsVO userVo, Connection conn) throws Exception
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(CHECK_IF_PERSONAL_INFO_USED_FOR_LEARNER);
			pst.setString(1, dto.getCustomerID());
			rs = pst.executeQuery();
			rs.first();
			int count = rs.getInt("rowCount");
			
			if(count > 0)
				result = "ALREADY_USED_FOR_LEARNER";
			else
			{
				pst = conn.prepareStatement(CHECK_IF_PERSONAL_INFO_USED_FOR_DL);
				pst.setString(1, dto.getCustomerID());
				rs = pst.executeQuery();
				rs.first();
				count = rs.getInt("rowCount");
				
				if(count > 0)
					result = "ALREADY_USED_FOR_DL";
				else
				{
					pst = conn.prepareStatement(CHECK_IF_PERSONAL_INFO_USED_FOR_VEHICLE);
					pst.setString(1, dto.getCustomerID());
					rs = pst.executeQuery();
					rs.first();
					count = rs.getInt("rowCount");
					
					if(count > 0)
						result = "ALREADY_USED_FOR_VEHICLE";
					else
					{
						pst = conn.prepareStatement(DELETE_PERSONAL_INFO);
						pst.setString(1, dto.getPersonalInfoId());
						count = pst.executeUpdate();
						
						if(count > 0)
							result = "SUCCESS";
					}
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[delete_personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	
	public synchronized String organization_info(EralisCommonDTO dto, Connection conn,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String customerId	=	null;
		try 
		{
			customerId = EralisCommonUtil.generateCustomerCodeFormat("O", conn);
			
			pst = conn.prepareStatement(INSERT_NEW_ORGANIZATION_INFO_DETAILS);
			pst.setString(1, dto.getType());
			pst.setString(2, customerId);
			pst.setString(3, dto.getMinistry());
			pst.setString(4, dto.getPrivateCompany());
			pst.setString(5, dto.getDepartment());
			pst.setString(6, dto.getDiplomatId());
			pst.setString(7, dto.getName());
			pst.setString(8, dto.getRegion());
			pst.setString(9, dto.getDzongkhag());
			pst.setString(10,"0");
			pst.setString(11, "0");
			pst.setString(12, dto.getAddress());
			pst.setString(13, dto.getPhone());
			pst.setString(14, dto.getEmail());
			pst.setString(15, userVo.getActorId());

			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[organization_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+'#'+customerId;
	}
	
	public synchronized String edit_organization_info(EralisCommonDTO dto, Connection conn, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(EDIT_ORGANIZATION_INFO);
			pst.setString(1, dto.getType());
			pst.setString(2, dto.getMinistry());
			pst.setString(3, dto.getPrivateCompany());
			pst.setString(4, dto.getDepartment());
			pst.setString(5, dto.getName());
			pst.setString(6, dto.getRegion());
			pst.setString(7, dto.getDzongkhag());
			pst.setString(8,"0");
			pst.setString(9, "0");
			pst.setString(10, dto.getAddress());
			pst.setString(11, dto.getPhone());
			pst.setString(12, dto.getEmail());
			pst.setString(13, dto.getRemarks());
			pst.setString(14, userVo.getActorId());
			pst.setString(15, dto.getDiplomatId());
			pst.setString(16, dto.getOrganisationInfoId());
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[edit_organization_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public synchronized String delete_organization_info(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(CHECK_IF_ORGANIZATION_INFO_IS_USED);
			pst.setString(1, dto.getCustomerID());
			rs = pst.executeQuery();
			rs.first();
			
			int rowCount = rs.getInt("rowCount");
			
			if(rowCount > 0)
			{
				result = "ALREADY_USED";
			}
			else
			{
				pst = conn.prepareStatement(DELETE_ORGANIZATION);
				pst.setString(1, dto.getOrganisationInfoId());
				
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[delete_organization_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String accident_info(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_ACCIDENT_DETAILS);
			pst.setString(1, dto.getCustomerID());
			pst.setString(2, dto.getTitleOfcourtesy());
			pst.setString(3, dto.getFirstname());
			pst.setString(4, dto.getMiddlename());
			pst.setString(5, dto.getLastName());
			pst.setString(6, dto.getOccupation());
			pst.setString(7, dto.getNationality());
			pst.setString(8, dto.getCID());
			pst.setString(9, dto.getDOB());
			pst.setString(10, dto.getBloodGroup());
			pst.setString(11, dto.getFathersName());
			pst.setString(12, dto.getGender());
			pst.setString(13, dto.getIdentificationMarks());
			pst.setString(14, dto.getRemarks());
			pst.setString(15, dto.getNational());
			
			pst.setString(17, dto.getDzongkhag());
			pst.setString(18, dto.getGewog());
			pst.setString(19, dto.getVillage());
			pst.setString(20, dto.getCountry());
			pst.setString(21, dto.getAddress());
			pst.setString(22, dto.getContactAddress());
			pst.setString(23, dto.getPhone());	
			pst.setString(24, dto.getEmail());
			pst.setString(20, dto.getCode());
			pst.setString(21, dto.getType());
			pst.setString(22, dto.getMinistry());
			pst.setString(23, dto.getDepartment());	
			pst.setString(24, dto.getRegion());
			pst.setString(24, dto.getConductorName());
			pst.setString(24, dto.getVehicleNo());
			pst.setString(24, dto.getLLNo());
			pst.setString(24, dto.getAccidentDate());
			pst.setString(24, dto.getAccidentCause());
			pst.setString(24, dto.getAccidentSite());
			
			
			
			
		
			
			
			

			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String vehicle_history(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_VEHICLE_HISTORY_DETAILS);
			pst.setString(1, dto.getCustomerID());
			pst.setString(2, dto.getTitleOfcourtesy());
			pst.setString(3, dto.getFirstname());
			pst.setString(4, dto.getMiddlename());
			pst.setString(5, dto.getLastName());
			pst.setString(6, dto.getOccupation());
			pst.setString(7, dto.getNationality());
			pst.setString(8, dto.getCID());
			pst.setString(9, dto.getDOB());
			pst.setString(10, dto.getBloodGroup());
			pst.setString(11, dto.getFathersName());
			pst.setString(12, dto.getGender());
			pst.setString(13, dto.getIdentificationMarks());
			pst.setString(14, dto.getRemarks());
			pst.setString(15, dto.getNational());
			
			pst.setString(17, dto.getDzongkhag());
			pst.setString(18, dto.getGewog());
			pst.setString(19, dto.getVillage());
			pst.setString(20, dto.getCountry());
			pst.setString(21, dto.getAddress());
			pst.setString(22, dto.getContactAddress());
			pst.setString(23, dto.getPhone());	
			pst.setString(24, dto.getEmail());
			pst.setString(20, dto.getCode());
			pst.setString(21, dto.getType());
			pst.setString(22, dto.getMinistry());
			pst.setString(23, dto.getDepartment());	
			pst.setString(24, dto.getRegion());
			pst.setString(24, dto.getConductorName());
			pst.setString(24, dto.getVehicleNo());
			pst.setString(24, dto.getLLNo());
			pst.setString(24, dto.getAccidentDate());
			pst.setString(24, dto.getAccidentCause());
			pst.setString(24, dto.getAccidentSite());
			pst.setString(26, dto.getLicenseNo());
			
			
			
			
		
			
			
			

			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[vehicle_history]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String license_history(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_LICENSE_HISTORY_DETAILS);
			pst.setString(1, dto.getCustomerID());
			pst.setString(2, dto.getTitleOfcourtesy());
			pst.setString(3, dto.getFirstname());
			pst.setString(4, dto.getMiddlename());
			pst.setString(5, dto.getLastName());
			pst.setString(6, dto.getOccupation());
			pst.setString(7, dto.getNationality());
			pst.setString(8, dto.getCID());
			pst.setString(9, dto.getDOB());
			pst.setString(10, dto.getBloodGroup());
			pst.setString(11, dto.getFathersName());
			pst.setString(12, dto.getGender());
			pst.setString(13, dto.getIdentificationMarks());
			pst.setString(14, dto.getRemarks());
			pst.setString(15, dto.getNational());
			
			pst.setString(17, dto.getDzongkhag());
			pst.setString(18, dto.getGewog());
			pst.setString(19, dto.getVillage());
			pst.setString(20, dto.getCountry());
			pst.setString(21, dto.getAddress());
			pst.setString(22, dto.getContactAddress());
			pst.setString(23, dto.getPhone());	
			pst.setString(24, dto.getEmail());
			pst.setString(20, dto.getCode());
			pst.setString(21, dto.getType());
			pst.setString(22, dto.getMinistry());
			pst.setString(23, dto.getDepartment());	
			pst.setString(24, dto.getRegion());
			pst.setString(24, dto.getConductorName());
			pst.setString(24, dto.getVehicleNo());
			pst.setString(24, dto.getLLNo());
			pst.setString(24, dto.getAccidentDate());
			pst.setString(24, dto.getAccidentCause());
			pst.setString(24, dto.getAccidentSite());
			pst.setString(26, dto.getLicenseNo());
			
			
			
			
		
			
			
			

			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String route_permit(EralisCommonDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String permitID = null;
		String result = "FAILURE";
		ResultSet rs = null;
		try 
		{	
         //  conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(CHECK_ROUTE_PERMIT_NO);
				pst.setString(1, dto.getPermitNo());
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
			    if(rowCount > 0){
			    	pst = conn.prepareStatement(UPDATE_ROUTE_PERMIT_DETAILS_RENEWAL);
					pst.setString(1, dto.getPermitNo());
					int count = pst.executeUpdate();
			    	
					if(count > 0){
						pst = conn.prepareStatement(UPDATE_ROUTE_PERMIT_DETAILS);
						
						java.util.Date dateOfIssue = sourceSdf.parse(dto.getDateOfissue());
						String dateOfIssueStr = targetSdf.format(dateOfIssue);
						pst.setString(1, dateOfIssueStr);
						java.util.Date validityDate = sourceSdf.parse(dto.getValidUpto());
						String validityDateStr = targetSdf.format(validityDate);
						pst.setString(2, validityDateStr);
						pst.setString(3, dto.getPermitNo());
						int count1 = pst.executeUpdate();
						if(count1 > 0){
							permitID  = dto.getPermitNo();
							result = "SUCCESS";
							PaymentDTO paymentDTO = new PaymentDTO();
							paymentDTO.setApplicationNo(permitID);
							paymentDTO.setServiceId("117");
							paymentDTO.setApplicationType("ROUTE_PERMIT_RENEWAL");
							paymentDTO.setAmount(dto.getAmount());
							//paymentDTO.setPenalty(dto.getPenalty());
							paymentDTO.setPenalty("0");
							paymentDTO.setCreatedBy(userVo.getActorId());
							paymentDTO.setReceiptNo(dto.getReceiptNo());
							java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
							String receiptDateStr1 = targetSdf.format(receiptDate1);
							paymentDTO.setReceiptDate(receiptDateStr1);
							result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
					  }
						
					}
			   
			       }
			else{
			
				String permitId = EralisCommonUtil.generatePermitNoFormat(conn, dto.getRegion());
				String permitIdArray[]	=	permitId.split("/");
				String permitSlNo = permitIdArray[2];
				pst = conn.prepareStatement(INSERT_ROUTE_PERMIT_DETAILS);
				pst.setString(1, permitId);
				pst.setString(2, dto.getDriverName());
				
				if(dto.getPermitIssuedTo().equals("FV"))
					pst.setString(3, dto.getLicenseNo());
				else
					pst.setString(3, dto.getLicenseNumber());
				
				pst.setString(4, dto.getAddress());
				pst.setString(5, dto.getPhone());
				if(dto.getPermitIssuedTo().equals("FV"))
					pst.setString(6, dto.getVehicleNo());
				else
					pst.setString(6, dto.getVehicleNumber());
				
				pst.setString(7, dto.getRegion());
				pst.setString(8, dto.getBaseoffice());
				pst.setString(9, dto.getRouteBetween());
				pst.setString(10, dto.getTo());
				if(dto.getPermitIssuedTo().equals("FV"))
					pst.setString(11, dto.getVehicleType());
				else
					pst.setString(11, dto.getVehicleTypeBV());
				
				pst.setString(12, dto.getCarryingCapacity());
				if(dto.getEngineCC()!=null)
				{
					pst.setString(13, dto.getEngineCC());
				}
				else
				{
					pst.setString(13, "0");
				}
				pst.setString(14, dto.getSeatCapacity());
				pst.setString(15, dto.getAmount());
				pst.setString(16, dto.getReceiptNo());
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDateStr = targetSdf.format(receiptDate);
				pst.setString(17, receiptDateStr);
				java.util.Date dateOfIssue = sourceSdf.parse(dto.getDateOfissue());
				String dateOfIssueStr = targetSdf.format(dateOfIssue);
				pst.setString(18, dateOfIssueStr);
				java.util.Date validityDate = sourceSdf.parse(dto.getValidUpto());
				String validityDateStr = targetSdf.format(validityDate);
				pst.setString(19, validityDateStr);
				pst.setString(20, dto.getRemarks());
				pst.setString(21, userVo.getActorId());
				pst.setDate(22, date);
				pst.setString(23, userVo.getRegionId());
				pst.setString(24, userVo.getBaseId());
				pst.setString(25, dto.getJourneyPurpose());
				pst.setString(26, permitSlNo);
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String dateString = simpleDateFormat.format(new Date());
				String year = dateString.split("/")[2];
				pst.setString(27, year);
				pst.setString(28, dto.getPermitIssuedTo());
				int count = pst.executeUpdate();
				
				if(count > 0){
					permitID = permitId;
					result = "SUCCESS";
					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(permitID);
					paymentDTO.setServiceId("116");
					paymentDTO.setApplicationType("ROUTE_PERMIT");
					paymentDTO.setAmount(dto.getAmount());
					//paymentDTO.setPenalty(dto.getPenalty());
					paymentDTO.setPenalty("0");
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receiptDate1 = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr1 = targetSdf.format(receiptDate1);
					paymentDTO.setReceiptDate(receiptDateStr1);
					result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
			  }
		     } 
			}
		}
		catch (Exception e) 
		{
			permitID = null;
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[route_permit]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return permitID+'#'+result;
	}
	
	public EralisCommonDTO get_permit_dtls(String permitID) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto = new EralisCommonDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PERMIT_DETAILS);
				pst.setString(1, permitID);
				rs = pst.executeQuery();
				rs.first();
				
				dto.setPermitID(rs.getString("Permit_No"));
				dto.setDriverName(rs.getString("Driver_Name"));
				dto.setLicenseNo(rs.getString("Driver_License_No"));
				dto.setAddress(rs.getString("Driver_Address"));
				dto.setPhone(rs.getString("Driver_Phone_No"));
				dto.setVehicleNo(rs.getString("Registration_No"));
				dto.setRegion(rs.getString("region"));
				dto.setBaseoffice(rs.getString("baseOffice"));
				dto.setRouteBetween(rs.getString("Route_Between"));
				dto.setTo(rs.getString("Route_To"));
				dto.setVehicleType(rs.getString("vehicleType"));
				dto.setCarryingCapacity(rs.getString("Carrying_Capacity"));
				dto.setEngineCC(rs.getString("Engine_CC"));
				dto.setSeatCapacity(rs.getString("Seat_Capacity"));
				dto.setAmount(rs.getString("Amount"));
				dto.setReceiptNo(rs.getString("Receipt_No"));
				dto.setReceiptDate(rs.getString("Receipt_Date"));
				dto.setDateOfissue(rs.getString("Issue_Date"));
				dto.setValidUpto(rs.getString("validity"));
				dto.setRemarks(rs.getString("Remarks"));
				dto.setJourneyPurpose(rs.getString("Journey_Purpose"));
			}
		} 
		catch (Exception e)
		{	
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	public String new_accident_entry(EralisCommonActionForm commonForm, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		try 
		{
			
			String caseNo = null;
			String caseSlNo = null;
			int reportYear = Integer.parseInt(commonForm.getDateOfReport().substring(6, 10));
			
			pst = conn.prepareStatement(GET_LATEST_CASE_NO);
			pst.setString(1, commonForm.getPoliceDivision());
			pst.setString(2, commonForm.getPoliceStation());
			pst.setString(3, commonForm.getPoliceDivision());
			pst.setString(4, commonForm.getPoliceStation());
			pst.setInt(5, reportYear);
			rs = pst.executeQuery();
			if (!rs.next() ) {
				pst = conn.prepareStatement("SELECT Police_Division_Name,(SELECT Police_Station_Code" +
										" FROM t_police_station_master WHERE Police_Station_Id=?" +
										") Police_Station_Code FROM t_police_division_master WHERE Police_Division_Id=?");
				
				pst.setString(1, commonForm.getPoliceStation());
				pst.setString(2, commonForm.getPoliceDivision());
				rs = pst.executeQuery();
				rs.first();
				caseSlNo = "1";
				caseNo = "RBP/"+ rs.getString("Police_Division_Name")+"/"+ rs.getString("Police_Station_Code")+"/"+reportYear+"/1";
			}
			else
			{
				rs.first();
				int lastCaseNo = rs.getInt("Case_Sl_No");
				caseSlNo = Integer.toString(lastCaseNo+1);
				caseNo ="RBP/"+ rs.getString("Police_Division_Name")+"/"+ rs.getString("Police_Station_Code")+"/"+reportYear+"/"+caseSlNo;
			}

			int count=1;
			pst = conn.prepareStatement(INSERT_INTO_ACCIDENT_DTLS,PreparedStatement.RETURN_GENERATED_KEYS);
			pst.setString(count++, caseNo);
			pst.setString(count++, commonForm.getPoliceDivision());
			pst.setString(count++, commonForm.getPoliceStation());
			Date dateOfOccurrence = sourceSdf.parse(commonForm.getDateOfOccurrence());
			String dateOfOccurrenceStr = targetSdf.format(dateOfOccurrence);
			pst.setString(count++, dateOfOccurrenceStr);
			pst.setString(count++, commonForm.getTimeOfOccurrence());
			
			Date dateOfReport = sourceSdf.parse(commonForm.getDateOfReport());
			String dateOfReportStr = targetSdf.format(dateOfReport);
			
			pst.setString(count++, dateOfReportStr);
			pst.setString(count++, commonForm.getTimeOfReport());
			pst.setString(count++, commonForm.getPlaceOfOccurrence());
			pst.setString(count++, commonForm.getDzongkhag());
			pst.setString(count++, commonForm.getGewog());
			pst.setString(count++, commonForm.getVillage());
			pst.setString(count++, commonForm.getAccidentType());
			pst.setString(count++, commonForm.getAccidentNature());
			pst.setString(count++, caseSlNo);
			pst.setString(count++, commonForm.getInformerType());
			pst.setString(count++, commonForm.getInformerName());
			pst.setString(count++, commonForm.getDistance());

			Date arrivalDate = sourceSdf.parse(commonForm.getArrivalDate());
			String arrivalDateStr = targetSdf.format(arrivalDate);
			
			
			pst.setString(count++, arrivalDateStr);
			pst.setString(count++, commonForm.getArrivalTime());
			pst.setString(count++, commonForm.getRemarks());
			
			int count1 = pst.executeUpdate();
			
			String accidentDtlsId = null;
			rs = pst.getGeneratedKeys();
			while (rs.next()) {
				accidentDtlsId = rs.getString(1);
			}
			
			if(count1 > 0)
			{
				
				for(EralisCommonDTO driverList : commonForm.getAccidentDriverList())
				{
					count=1;
					pst = conn.prepareStatement(INSERT_INTO_ACCIDENT_DRIVER_VEHICLE_DTLS);
					pst.setString(count++, accidentDtlsId);
					pst.setString(count++, driverList.getNationality());
					pst.setString(count++, driverList.getLicenseNo());
					pst.setString(count++, driverList.getName());
					pst.setString(count++, driverList.getCID());
					Date dob = sourceSdf.parse(driverList.getDOB());
					String stingDob = targetSdf.format(dob);
					pst.setString(count++, stingDob);
					pst.setString(count++, driverList.getMobileNo());
					pst.setString(count++, driverList.getGender());
					pst.setString(count++, driverList.getVehicleOrigin());
					pst.setString(count++, driverList.getVehicleNo());
					pst.setString(count++, driverList.getVehicleType());
					pst.setString(count++, driverList.getCompany());
					pst.setString(count++, driverList.getModel());
					pst.setString(count++, driverList.getOwner());

					pst.setString(count++, driverList.getAlcoholContent());
					pst.setString(count++, driverList.getIsDrugAbuse());
					pst.executeUpdate();
				}
				 
				for(EralisCommonDTO injuredList : commonForm.getInjuredPersonList())
				{
					count=1;
					if(injuredList.getNationality()!=null && !injuredList.getNationality().equalsIgnoreCase(""))
					{
						pst = conn.prepareStatement(INSERT_INTO_ACCIDENT_INJURED_DTLS);
						pst.setString(count++, accidentDtlsId);
						pst.setString(count++, injuredList.getName());
						pst.setString(count++, injuredList.getAge());
						pst.setString(count++, injuredList.getGender());
						pst.setString(count++, injuredList.getAddress());
						pst.setString(count++, injuredList.getNationality());
						pst.setString(count++, injuredList.getCID());
						pst.setString(count++, injuredList.getMobileNo());
						pst.setString(count++, injuredList.getOccupation());
						pst.setString(count++, injuredList.getInjurydescription());
						pst.executeUpdate();
					}
				}
				

				for(EralisCommonDTO killedList : commonForm.getKilledPersonList())
				{
					count=1;
					if(killedList.getNationality()!=null && !killedList.getNationality().equalsIgnoreCase(""))
					{
						pst = conn.prepareStatement(INSERT_INTO_ACCIDENT_KILLED_DTLS);
						pst.setString(count++, accidentDtlsId);
						pst.setString(count++, killedList.getName());
						pst.setString(count++, killedList.getAge());
						pst.setString(count++, killedList.getGender());
						pst.setString(count++, killedList.getAddress());
						pst.setString(count++, killedList.getNationality());
						pst.setString(count++, killedList.getCID());
						pst.setString(count++, killedList.getMobileNo());
						pst.setString(count++, killedList.getOccupation());
						pst.setString(count++, killedList.getInjurydescription());
						pst.executeUpdate();
					}
					
				}
				
 
				String accidentCauseDtlsArray[]	=	commonForm.getAccidentCauseDtls().split("#");
				for(int j=1;j<accidentCauseDtlsArray.length;j++)
				{
					count=1;
					pst = conn.prepareStatement(INSERT_INTO_ACCIDENT_CAUSE_DTLS);
					pst.setString(count++, accidentDtlsId);
					pst.setString(count++, accidentCauseDtlsArray[j]);
					pst.executeUpdate();
				}
				result = caseNo;
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at eraliscommonDAO[new_accident_entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String getDataFromAggregator(String orderNo) throws  Exception
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = null;
		
		try {
			conn = ConnectionManagerAggregator.getConnection();
			
			if(conn != null)
			{
				String sql = "SELECT Application_Id,Payment_Date,bfs_bfsTxnId,bfs_txnAmount,bfs_debitAuthCode FROM `t_payment` WHERE bfs_orderNo=?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, orderNo);
				rs = pst.executeQuery();
				rs.first();
				
				result = rs.getString("Application_Id")+"#"+rs.getString("Payment_Date")+"#"+rs.getString("bfs_bfsTxnId")+"#"+rs.getString("bfs_txnAmount")+"#"+rs.getString("bfs_debitAuthCode");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			ConnectionManagerAggregator.close(conn, null, rs, pst);
		}
		
		return result;
	}
	
	
	/*
	 * Queries
	 */
	
	private static final  String GET_LATEST_CASE_NO = "SELECT a.`Case_Sl_No`," +
												"(SELECT Police_Division_Name FROM t_police_division_master" +
												" WHERE Police_Division_Id = ?)Police_Division_Name," +
												"(SELECT Police_Station_Code FROM t_police_station_master WHERE Police_Station_Id=?) Police_Station_Code" +
													" FROM t_accident_dtls a" +
													" WHERE a.`Police_Division_Id`=? AND a.`Police_Station_Id`=?" +
													" AND DATE_FORMAT(a.`Date_Of_Report`,'%Y')=?" +
													"  ORDER BY a.`Case_Sl_No` DESC LIMIT 1";
	
	private static final String INSERT_PERSONAL_INFO_DETAILS = "INSERT INTO `t_personal_dtls` ( "
		+ "  `Customer_Id`, "
		+ "  `Title_Of_Courtesy_Id`, "
		+ "  `First_Name`, "
		+ "  `Middle_Name`, "
		+ "  `Last_Name`, "
		+ "  `Occupation_Id`, "
		+ "  `Nationality_Id`, "
		+ "  `CID_Number`, "
		+ "  `Date_Of_Birth`, "
		+ "  `Blood_Group_Id`, "
		+ "  `Fathers_Name`, "
		+ "  `Gender`, "
		+ "  `Identification_Mark`, "
		+ "  `Remarks`, "
		+ "  `Is_International`, "
		+ "  `Permanent_Dzongkhag_Id`, "
		+ "  `Permanent_Gewog_Id`, "
		+ "  `Permanant_Village_Id`, "
		+ "  `Permanent_Country_Id`, "
		+ "  `Permanent_Address`, "
		+ "  `Present_Dzongkhag_Id`, "
		+ "  `Present_Contact_Address`, "
		+ "  `Present_Phone_No`, "
		+ "  `Present_Email`, "
		+ "  `Image_Path`, "
		+ "  `UUID`, "
		+ "  `Created_By`, "
		+ "  `Created_On`, "
		+ "  `Contact_Type`, "
		+ "  `Ministry_Id`, "
		+ "  `Department_Id`,Guardian_Contact_No,Mothers_Name "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?)";
	
	private static final String CHECK_IF_PERSONAL_INFO_USED_FOR_LEARNER = "SELECT COUNT(*) rowCount FROM `t_driving_license_dtls` a WHERE a.`Customer_Id`=?";
	
	private static final String CHECK_IF_PERSONAL_INFO_USED_FOR_DL = "SELECT COUNT(*) rowCount FROM `t_learner_license_dtls` a WHERE a.`Customer_Id`=?";
	
	private static final String CHECK_IF_PERSONAL_INFO_USED_FOR_VEHICLE = "SELECT COUNT(*) rowCount FROM `t_vehicle_registration_dtls` a WHERE a.`Customer_Id`=?";
	
	private static final String DELETE_PERSONAL_INFO = "DELETE FROM `t_personal_dtls` WHERE Personal_Info_Id=?";
	
	private static final String GET_OLD_IMAGE_PATH = "SELECT Image_Path FROM `t_personal_dtls` WHERE Personal_Info_Id=?";
	
	 public static final String UPDATE_PERSONAL_INFO = "UPDATE "
														 + "  `t_personal_dtls` "
														 + "SET "
														 + "  `Title_Of_Courtesy_Id` = ?, "
														 + "  `First_Name` = ?, "
														 + "  `Middle_Name` = ?, "
														 + "  `Last_Name` = ?, "
														 + "  `Occupation_Id` = ?, "
														 + "  `Nationality_Id` = ?, "
														 + "  `Date_Of_Birth` = ?, "
														 + "  `Blood_Group_Id` = ?, "
														 + "  `Fathers_Name` = ?, "
														 + "  `Gender` = ?, "
														 + "  `Remarks` = ?, "
														 + "  `Is_International` = ?, "
														 + "  `Permanent_Dzongkhag_Id` = ?, "
														 + "  `Permanent_Gewog_Id` = ?, "
														 + "  `Permanant_Village_Id` = ?, "
														 + "  `Permanent_Country_Id` = ?, "
														 + "  `Permanent_Address` = ?, "
														 + "  `Present_Dzongkhag_Id` = ?, "
														 + "  `Present_Contact_Address` = ?, "
														 + "  `Present_Phone_No` = ?, "
														 + "  `Present_Email` = ?, "
														 + "  `Image_Path` = ?, "
														 + "  `UUID` = ?, "
														 + "  `Updated_By` = ?, "
														 + "  `Updated_On` = ?," 
														 + "   CID_Number=?, "
														 + "  `Contact_Type` = ?, "
														 + "  `Ministry_Id` = ?, "
														 + "  `Department_Id` = ?,Guardian_Contact_No=?,Mothers_Name=? "
														 + " WHERE `Personal_Info_Id` = ?";
	
	private static final String INSERT_NEW_ORGANIZATION_INFO_DETAILS="INSERT INTO `t_organization_info` ( "
		+ "  `Organization_Type_Id`, "
		+ "  `Customer_Id`, "
		+ "  `Ministry_Id`, "
		+ "  `Private_Id`, "
		+ "  `Department_Id`, "
		+ "  `Diplomat_Id`, "
		+ "  `Organization_Name`, "
		+ "  `Region_Id`, "
		+ "  `Dzongkhag_Id`, "
		+ "  `Gewog_Id`, "
		+ "  `Village_Id`, "
		+ "  `Address`, "
		+ "  `Phone_Number`, "
		+ "  `Email_Id`,`Created_By` "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String CHECK_IF_ORGANIZATION_INFO_IS_USED = "SELECT COUNT(*) rowCount FROM t_vehicle_registration_dtls a WHERE a.`Customer_Id`=?";
	
	private static final String DELETE_ORGANIZATION = "DELETE FROM `t_organization_info` WHERE Organization_Info_Id=?";
	
	private static final String EDIT_ORGANIZATION_INFO = "UPDATE "
		+ "  `t_organization_info` "
		+ "SET "
		+ "  `Organization_Type_Id` = ?, "
		+ "  `Ministry_Id` = ?, "
		+ "  `Private_Id` = ?, "
		+ "  `Department_Id` = ?, "
		+ "  `Organization_Name` = ?, "
		+ "  `Region_Id` = ?, "
		+ "  `Dzongkhag_Id` = ?, "
		+ "  `Gewog_Id` = ?, "
		+ "  `Village_Id` = ?, "
		+ "  `Address` = ?, "
		+ "  `Phone_Number` = ?, "
		+ "  `Email_Id` = ?, "
		+ "  `Remarks` = ? , `Update_By`=? , `Updated_On`=CURRENT_TIMESTAMP ,`Diplomat_Id`=?"
		+ " WHERE `Organization_Info_Id` = ?";
	
	private static final String INSERT_ACCIDENT_DETAILS = "";
	
	private static final String INSERT_VEHICLE_HISTORY_DETAILS = "";
	
	private static final String INSERT_LICENSE_HISTORY_DETAILS = "";
	
	private static final String INSERT_ROUTE_PERMIT_DETAILS = "INSERT INTO `t_permit_dtls` ( "
		+ "  `Permit_No`, "
		+ "  `Driver_Name`, "
		+ "  `Driver_License_No`, "
		+ "  `Driver_Address`, "
		+ "  `Driver_Phone_No`, "
		+ "  `Registration_No`, "
		+ "  `Region_Id`, "
		+ "  `Base_Office_Id`, "
		+ "  `Route_Between`, "
		+ "  `Route_To`, "
		+ "  `Vehicle_Type_Id`, "
		+ "  `Carrying_Capacity`, "
		+ "  `Engine_CC`, "
		+ "  `Seat_Capacity`, "
		+ "  `Amount`, "
		+ "  `Receipt_No`, "
		+ "  `Receipt_Date`, "
		+ "  `Issue_Date`, "
		+ "  `Validity`, "
		+ "  `Remarks`, "
		+ "  `Created_By`, "
		+ "  `Created_Date`,Processed_Region_Id,Processed_Base_Id,Journey_Purpose,Permit_Sl_No,Permit_Year,Permit_Type "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)";
	
	private static final String INSERT_INTO_NOC_DTLS = "INSERT INTO `t_noc_dtls` ( "
		+ "  `NOC_ID`, "
		+ "  `Seller_Type`, "
		+ "  `Seller_Customer_Id`, "
		+ "  `Vehicle_Id`, "
		+ "  `Buyer_Name`, "
		+ "  `Buyer_Address`, "
		+ "  `Receipt_Number`, "
		+ "  `Receipt_Date`, "
		+ "  `Amount`, "
		+ "  `Transfer_Amount`, "
		+ "  `Sale_Deed_Date`, "
		+ "  `Sale_Deed_Amount`, "
		+ "  `location_id`, "
		+ "  `dated`,Processed_Region_Id,Processed_Base_Id,Buyer_CID "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String GET_NOC_DETAILS = "SELECT "
		+ "  a.`Seller_Type`, "
		+ "  a.`Seller_Customer_Id`, "
		+ "  (SELECT "
		+ "    Vehicle_Number "
		+ "  FROM "
		+ "    t_vehicle_registration_dtls "
		+ "  WHERE `Vehicle_Reg_Dtls_Id` = a.`Vehicle_Id`) Vehicle_Number, "
		+ "  a.`Vehicle_Id`, "
		+ "  a.`Buyer_Name`, "
		+ "  a.`Buyer_Address`, "
		+ "  a.`Receipt_Number`, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d-%m-%Y') receiptDate, "
		+ "  DATE_FORMAT(a.`dated`, '%d-%m-%Y') cancellationDate, "
		+ "  a.`Amount`, "
		+ "  (SELECT "
		+ "    `region_name` "
		+ "  FROM "
		+ "    t_region_master "
		+ "  WHERE region_id = a.`location_id`) region,Buyer_CID "
		+ "FROM "
		+ "  t_noc_dtls a "
		+ "WHERE a.`NOC_ID` = ?";
	
	private static final String GET_PERSONAL_DETAILS = "SELECT "
														+ "  CONCAT( "
														+ "    a.`First_Name`, "
														+ "    \" \", "
														+ "    a.`Middle_Name`, "
														+ "    \" \", "
														+ "    a.`Last_Name` "
														+ "  ) pname, "
														+ "  a.`CID_Number`, "
														+ "  a.`Permanant_Village_Id`, "
														+ "  g.`Gewog_Name`, "
														+ "  d.`dzongkhag_name`, "
														+ "  a.`Present_Contact_Address`, "
														+ "  a.`Image_Path` "
														+ "FROM "
														+ "  t_personal_dtls a "
														+ "  LEFT JOIN t_gewog_master g "
														+ "    ON a.`Permanent_Gewog_Id` = g.`Gewog_Id` "
														+ "  LEFT JOIN t_dzongkhag_master d "
														+ "    ON a.`Permanent_Dzongkhag_Id` = d.`dzongkhag_id` "
														+ "WHERE a.`Customer_Id` = ?";

	private static final String GET_ORGANIZATION_DETAILS = "SELECT "
															+ "  if(a.Organization_Type_Id='2',e.Private_Name,d.Department_Name)Organization_Name, "
															+ "  (SELECT "
															+ "    `Department_Name` "
															+ "  FROM "
															+ "    t_department_master "
															+ "  WHERE `Department_Id` = a.`Department_Id`) department, "
															+ "  (SELECT "
															+ "    `Ministry_Name` "
															+ "  FROM "
															+ "    t_ministry_master "
															+ "  WHERE `Ministry_Id` = a.`Ministry_Id`) ministry, "
															+ "  a.`Address` "
															+ "FROM "
															+ "  t_organization_info a "
															+ "  LEFT JOIN t_owner_master b "
															+ "    ON a.`Organization_Type_Id` = b.`Owner_Type_Id` "
															+ "  LEFT JOIN t_ministry_master m "
															+ "    ON a.`Ministry_Id` = m.`Ministry_Id` "
															+ "  LEFT JOIN t_department_master d "
															+ "    ON a.`Department_Id` = d.`Department_Id` " 
															+"	left join t_private_company_master e on a.Private_Id=e.Private_Id"
															+ "	WHERE a.`Customer_Id` = ?";
	
	private static final String GET_LAST_RENEWED_UPTO_DATE = "SELECT "
		+ "  IF( "
		+ "    a.`Vehicle_Reg_Dtls_Id` IN "
		+ "    (SELECT "
		+ "      `Vehicle_Id` "
		+ "    FROM "
		+ "      t_vehicle_renewal_dtls "
		+ "    WHERE `Vehicle_Id` = ?), "
		+ "    (SELECT "
		+ "      MAX(Expiry_Date) "
		+ "    FROM "
		+ "      t_vehicle_renewal_dtls "
		+ "    WHERE `Vehicle_Id` = ?), "
		+ "    a.`Expiry_Date` "
		+ "  ) expiryDate "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE a.`Vehicle_Reg_Dtls_Id` = ?";
	
	private static final String GET_VEHICLE_DETAILS = "SELECT "
		+ "  (SELECT "
		+ "    Vehicle_Model_Name "
		+ "  FROM "
		+ "    t_vehicle_model_master "
		+ "  WHERE Vehicle_Model_Id = a.`Vehicle_Model_Id`) vehicleModel, "
		+ "  DATE_FORMAT( "
		+ "    a.`Registration_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) initialDateOfRegistration, "
		+ "  (SELECT "
		+ "    `Vehicle_Type_Name` "
		+ "  FROM "
		+ "    t_vehicle_type_master "
		+ "  WHERE `Vehicle_Type_Id` = a.`Vehicle_Type_Id`) typeOfVehicle, "
		+ "  (SELECT "
		+ "    `Vehicle_Company_Name` "
		+ "  FROM "
		+ "    `t_vehicle_company_master` "
		+ "  WHERE `Vehicle_Company_Id` = a.`Vehicle_Company_Id`) make, "
		+ "  a.`Chassis_Number`, "
		+ "  a.`Engine_Number`, "
		+ "  a.`Manufacture_Year`, "
		+ "  a.`Unladen_Weight`, "
		+ "  a.`Load_Capacity`, "
		+ "  a.`Seat_Capacity`, "
		+ "  a.`Colour` "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE a.`Vehicle_Reg_Dtls_Id`=?";
	
	private static final String INSERT_INTO_T_VEHICLE_CANCELLATION_DTLS = "INSERT INTO `eralis_db`.`t_vehicle_cancellation_dtls` ( "
		+ "  `Vehicle_Id`, "
		+ "  `Customer_Id`, "
		+ "  `Cancellation_Date`, "
		+ "  `Cancellation_Reason_Id`, "
		+ "  `Receipt_No`, "
		+ "  `Receipt_Date`, "
		+ "  `Created_By`, "
		+ "  `Created_On` "
		+ ") "
		+ "VALUES "
		+ "  ( "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    (SELECT "
		+ "      Reason_Id "
		+ "    FROM "
		+ "      t_reason_master "
		+ "    WHERE Reason_Description = 'NOC_ISSUED'), "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ? "
		+ "  )";
	
	
	private static final String GET_PERMIT_DETAILS = "SELECT "
		+ "  a.`Permit_No`, "
		+ "  a.`Driver_Name`, "
		+ "  a.`Driver_License_No`, "
		+ "  a.`Driver_Address`, "
		+ "  a.`Driver_Phone_No`, "
		+ "  a.`Registration_No`, "
		+ "  (SELECT "
		+ "    `region_name` "
		+ "  FROM "
		+ "    `t_region_master` "
		+ "  WHERE `region_id` = a.`Region_Id`) region, "
		+ "  (SELECT "
		+ "    `base_office_name` "
		+ "  FROM "
		+ "    `t_base_office_master` "
		+ "  WHERE `base_office_id` = a.`Base_Office_Id`) baseOffice, "
		+ "  a.`Route_Between`, "
		+ "  a.`Route_To`, "
		+ "  (SELECT "
		+ "    `Vehicle_Type_Name` "
		+ "  FROM "
		+ "    `t_vehicle_type_master` "
		+ "  WHERE `Vehicle_Type_Id` = a.`Vehicle_Type_Id`) vehicleType, "
		+ "  a.`Carrying_Capacity`, "
		+ "  a.`Engine_CC`, "
		+ "  a.`Seat_Capacity`, "
		+ "  a.`Amount`, "
		+ "  a.`Receipt_No`, "
		+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') Receipt_Date, "
		+ "  DATE_FORMAT(a.`Issue_Date`, '%d/%m/%Y') Issue_Date, "
		+ "  DATE_FORMAT(a.`Validity`, '%d/%m/%Y') validity, "
		+ "  a.`Remarks`, "
		+ "  a.`Created_By`, "
		+ "  a.`Created_Date`, "
		+ "  a.`Updated_By`, "
		+ "  a.`Updated_On`,a.`Journey_Purpose` "
		+ "FROM "
		+ "  `t_permit_dtls` a "
		+ "WHERE a.Permit_No = ?";
	
	private static final String UPDATE_T_PAYMENT_DTLS	=	"UPDATE `t_payment_dtls` a " +
																"SET a.`Receipt_Date`=?,a.`Receipt_No`=?,a.`Amount_Paid`=?,a.`Penalty_Paid`=? " +
																"WHERE a.`Application_Number`=?";

	private static final String UPDATE_LEARNER_APPLICATION_RECEIPT	=	"UPDATE `t_learner_license_application` a " +
																		"SET " +
																		"a.`Receipt_Date`=?," +
																		"a.`Receipt_No`=? " +
																		"WHERE a.`Application_Number`=?;";
	
	private static final String UPDATE_LICENSE_APPLICATION_RECEIPT	=	"UPDATE `t_driving_license_application` a " +
																		"SET " +
																		"a.`Receipt_Date`=?," +
																		"a.`Receipt_No`=? " +
																		"WHERE a.`Application_Number`=?;";
	
	private static final String UPDATE_VEHICLE_APPLICATION_RECEIPT	=	"UPDATE `t_vehicle_application` a "
																		+ "SET a.`Amount`=?, "
																		+ "a.`Receipt_Date`=?, "
																		+ "a.`Receipt_Number`=? "
																		+ "WHERE a.`Application_Number`=?";
	private static final String INSERT_INTO_ACCIDENT_DTLS	=	"INSERT INTO  `t_accident_dtls`"
																		            +"("
																		            +" `Case_No`,"
																		            +"`Police_Division_Id`,"
																		            +"`Police_Station_Id`,"
																		            +"`Date_Of_Occurence`,"
																		            +"`Time_Of_Occurence`,"
																		            +"`Date_Of_Report`,"
																		            +"`Time_Of_Report`,"
																		            +"`Place_Of_Occurence`,"
																		            +"`Dzongkhag_Id`,"
																		            +"`Gewog_Id`,"
																		            +"`Village_Id`,"
																		            +"`Type_Of_Accident`,"
																		            +"`Nature_Of_Accident`,Case_Sl_No,`Informer_Type`," +
																            		" `Infermer_Name`,`Distance`,`Arrival_Date`,`Arrival_Time`,Remarks)"
																		            +" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
	
	public static final String INSERT_INTO_ACCIDENT_DRIVER_VEHICLE_DTLS	=	"INSERT INTO `t_accident_driver_vehicle_dtls`"
																            +"("
																            +" `Accident_Dtls_Id`,"
																            +"`Driver_Nationality`,"
																            +"`LL_DL_No`,"
																            +"`Driver_Name`,"
																            +"`CID`,"
																            +"`Dob`,"
																            +"`Mobile_No`,"
																            +"`Sex`,"
																            +"`Vehicle_Origin`,"
																            +"`Vehicle_No`,"
																            +"`Vehicle_Type`,"
																            +"`Vehicle_Company`,"
																            +"`Vehicle_Model`,"
																            +"`Owner_Type`,`alcoholContent`,`isDrugAbuse`)"
																            +"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

	public static final String INSERT_INTO_ACCIDENT_INJURED_DTLS	="INSERT INTO `t_accident_injured_dtls`"
																            +"("
																            +" `Accident_Dtls_Id`,"
																            +"`Name`,"
																            +"`Age`,"
																            +"`Gender`,"
																            +"`Address`,"
																            +"`Nationality`,"
																            +" `CID`,"
																            +"`Mobile_No`,`Occupation`,`Injury_Description`)"
																            +"VALUES (?,?,?,?,?,?,?,?,?,?);";
	

	public static final String INSERT_INTO_ACCIDENT_KILLED_DTLS	="INSERT INTO `t_accident_killed_dtls`"
																            +"("
																            +" `Accident_Dtls_Id`,"
																            +"`Name`,"
																            +"`Age`,"
																            +"`Gender`,"
																            +"`Address`,"
																            +"`Nationality`,"
																            +" `CID`,"
																            +"`Mobile_No`,`Occupation`,`Injury_Description`)"
																            +"VALUES (?,?,?,?,?,?,?,?,?,?);";
	
	
	//t_accident_killed_dtls
	
	public static final String INSERT_INTO_ACCIDENT_CAUSE_DTLS	=	"INSERT INTO `t_accident_cause_dtls` "
																+ "  ( Accident_Dtls_Id, "
																+ "    `Accident_Cause_Id` ) "
																+ "VALUES (?,?);";
	
	public static final String CHECK_ROUTE_PERMIT_NO = "SELECT COUNT(a.Permit_No) AS rowCount FROM t_permit_dtls a WHERE a.Permit_No=?";
	
	public static final String UPDATE_ROUTE_PERMIT_DETAILS="UPDATE t_permit_dtls a SET a.Issue_Date =?,a.Validity =? WHERE a.Permit_No=?";
	
	public static final String UPDATE_ROUTE_PERMIT_DETAILS_RENEWAL= "INSERT INTO t_renewal_permit_dtls ( "
																										+ "  Permit_Details_Id, "
																										+ "  Permit_Sl_No, "
																										+ "  Permit_Year, "
																										+ "  Permit_No, "
																										+ "  Driver_Name, "
																										+ "  Driver_License_No, "
																										+ "  Driver_Address, "
																										+ "  Driver_Phone_No, "
																										+ "  Registration_No, "
																										+ "  Region_Id, "
																										+ "  Base_Office_Id, "
																										+ "  Route_Between, "
																										+ "  Route_To, "
																										+ "  Vehicle_Type_Id, "
																										+ "  Carrying_Capacity, "
																										+ "  Engine_CC, "
																										+ "  Seat_Capacity, "
																										+ "  Amount, "
																										+ "  Receipt_No, "
																										+ "  Receipt_Date, "
																										+ "  Issue_Date, "
																										+ "  Validity, "
																										+ "  Remarks, "
																										+ "  Processed_Region_Id, "
																										+ "  Processed_Base_Id, "
																										+ "  Created_By, "
																										+ "  Created_Date, "
																										+ "  Updated_By, "
																										+ "  Updated_On, "
																										+ "  Journey_Purpose, "
																										+ "  Permit_Type "
																										+ ") "
																										+ "SELECT "
																										+ "  Permit_Details_Id, "
																										+ "  Permit_Sl_No, "
																										+ "  Permit_Year, "
																										+ "  Permit_No, "
																										+ "  Driver_Name, "
																										+ "  Driver_License_No, "
																										+ "  Driver_Address, "
																										+ "  Driver_Phone_No, "
																										+ "  Registration_No, "
																										+ "  Region_Id, "
																										+ "  Base_Office_Id, "
																										+ "  Route_Between, "
																										+ "  Route_To, "
																										+ "  Vehicle_Type_Id, "
																										+ "  Carrying_Capacity, "
																										+ "  Engine_CC, "
																										+ "  Seat_Capacity, "
																										+ "  Amount, "
																										+ "  Receipt_No, "
																										+ "  Receipt_Date, "
																										+ "  Issue_Date, "
																										+ "  Validity, "
																										+ "  Remarks, "
																										+ "  Processed_Region_Id, "
																										+ "  Processed_Base_Id, "
																										+ "  Created_By, "
																										+ "  Created_Date, "
																										+ "  Updated_By, "
																										+ "  Updated_On, "
																										+ "  Journey_Purpose, "
																										+ "  Permit_Type "
																										+ "FROM "
																										+ "  t_permit_dtls"
																										+"   WHERE Permit_No = ?";
	
}
