package bt.gov.rsta.eralis.dao.administration;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bt.gov.rsta.eralis.dto.administration.AdministrationDTO;
import bt.gov.rsta.eralis.dto.administration.CheckBoxDTO;
import bt.gov.rsta.eralis.dto.administration.PagePermissionDTO;
import bt.gov.rsta.eralis.dto.administration.PrivDTO;
import bt.gov.rsta.eralis.dto.administration.RoleDTO;
import bt.gov.rsta.eralis.dto.administration.UserDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.util.MasterSQLConstants;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class AdministrationDAO 
{
	private static AdministrationDAO adminDAO;
	
	public static AdministrationDAO getInstance()
	{
		if(adminDAO == null)
			adminDAO = new AdministrationDAO();
		
		return adminDAO;
	}

	public List<RoleDTO> getRoleList() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<RoleDTO> roleList = new ArrayList<RoleDTO>();
		RoleDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_ROLE_LIST);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new RoleDTO();
					dto.setRoleId(rs.getString("role_id"));
					dto.setRoleName(rs.getString("role_name"));
					dto.setRoleStatus(rs.getString("is_active"));
					
					roleList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("### Error at AdministrationDAO[getRoleList]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return roleList;
	}
	public List<AdministrationDTO> getPeningPaymentList() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<AdministrationDTO> applicationList = new ArrayList<AdministrationDTO>();
		AdministrationDTO dto;
		
		try 
		{
			conn = ConnectionManager.getOnlineDbConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PENDING_PAYMENT_APPLICATION);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new AdministrationDTO();
					dto.setReceiptDate(rs.getString("Receipt_Date"));
					dto.setReceiptNo(rs.getString("Receipt_No"));
					dto.setApplicationNo(rs.getString("Application_Number"));
					dto.setApplicationType(rs.getString("Application_Type"));
					dto.setAmount(rs.getString("Amount_Paid"));
					
					applicationList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("### Error at AdministrationDAO[getPeningPaymentList]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return applicationList;
	}
	
	public List<UserDTO> getUserList() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<UserDTO> userList = new ArrayList<UserDTO>();
		UserDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(null != conn)
			{
				pst = conn.prepareStatement(GET_USER_LIST);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new UserDTO();
					
					dto.setLoginId(rs.getString("login_id"));
					dto.setUserName(rs.getString("name"));
					dto.setEmailAddress(rs.getString("email_id"));
					dto.setMobileNumber(rs.getString("mobile_number"));
					dto.setShowTaskList(rs.getString("show_tasklist"));
					dto.setAgencyCode(rs.getString("agency_code"));
					dto.setIsActive(rs.getString("is_active"));
					dto.setRoleName(rs.getString("roleName"));
					dto.setRegion(rs.getString("regionName"));
					dto.setDzongkhag(rs.getString("dzongkhagName"));
					dto.setBaseOffice(rs.getString("baseOfficeName"));
					dto.setJurisType(rs.getString("jurisType"));
					dto.setJurisTypeDesc(rs.getString("jurisTypeDesc"));
					dto.setJurisTypeId(rs.getString("juris_type_id"));
					dto.setRegionId(rs.getString("region_id"));
					dto.setBaseOfficeId(rs.getString("base_office_id"));
					dto.setRoleId(rs.getString("roleId"));
					dto.setDzongkhagId(rs.getString("dzongkhag_id"));
					
					userList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at AdministrationDAO[getUserList]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return userList;
	}
	
	public List<String> getTableList() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<String> tableList = new ArrayList<String>();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				DatabaseMetaData dbmd = conn.getMetaData();
	            String[] types = {"TABLE"};
	            rs = dbmd.getTables(null, null, "%", types);
	            while (rs.next()) 
	            {
	                String tableName = rs.getString("TABLE_NAME");
	                tableList.add(tableName);
	            }
			}
		} 
		catch (Exception e)
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return tableList;
	}
	
	public String add_user(AdministrationDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "USER_ADD_FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_INTO_T_USER_MASTER);
			pst.setString(1, dto.getLoginId());
			pst.setString(2, dto.getUserName());
			pst.setString(3, dto.getEmailId());
			pst.setString(4, dto.getMobileNumber());
			
			if("on".equalsIgnoreCase(dto.getShowTaskList()))
				pst.setString(5, "Y");
			else
				pst.setString(5, "N");
			
			pst.setString(6, dto.getAgencyCode());
			
			int count = pst.executeUpdate();
			
			if(count > 0)
			{
				for(int i=0; i<dto.getRoleList().length; i++)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_USER_ROLE_MAPPING);
					pst.setString(1, dto.getLoginId());
					pst.setString(2, dto.getRoleList()[i]);
					
					count = pst.executeUpdate();
				}
				
				if(count > 0)
				{
					pst = conn.prepareStatement(GET_JURIS_TYPE_DESCRIPTION);
					pst.setString(1, dto.getJurisType());
					rs = pst.executeQuery();
					rs.first();
					String jurisTypeDesc = rs.getString("Jurisdiction_Type_Desc");
					
					pst = conn.prepareStatement(INSERT_INTO_T_USER_JURISDICTION_MAPPING);
					pst.setString(1, dto.getLoginId());
					pst.setString(2, dto.getRegion());
					pst.setString(3, dto.getBase());
					pst.setString(4, dto.getDzongkhag());
					
					if(jurisTypeDesc.equalsIgnoreCase("REGIONAL"))
						pst.setString(5, dto.getRegion());
					else 
						pst.setString(5, dto.getBase());
						
					pst.setString(6, dto.getJurisType());
					
					count = pst.executeUpdate();
					
					if(count > 0)
						result = "USER_ADD_SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[add_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String add_role(String role, String isActive, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "ROLE_ADD_FAILURE", roleId = null;
		
		try 
		{
			pst = conn.prepareStatement(INSERT_INTO_T_ROLE_MASTER, PreparedStatement.RETURN_GENERATED_KEYS);
			pst.setString(1, role);
			pst.setString(2, role.replaceAll(" ", "_").toUpperCase());
			pst.setString(3, isActive);
			
			int count = pst.executeUpdate();
			
			rs = pst.getGeneratedKeys();
			while(rs.next())
			{
				roleId = rs.getString(1);
			}
			
			if(count > 0 && roleId != null)
			{
				pst = conn.prepareStatement(GET_PAGE_ID_LIST);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					pst = conn.prepareStatement(INSERT_INTO_ROLE_PAGE_MAPPING);
					pst.setString(1, roleId);
					pst.setString(2, rs.getString("page_id"));
					
					pst.executeUpdate();
				}
				
				result = "ROLE_ADD_SUCCESS";
			}
		}
		catch (Exception e)
		{
			result = "ROLE_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[add_role]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	public String activateDeactivateRole(String roleId, String typeFlag, Connection conn) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		PreparedStatement pst = null;
		
		try
		{
			pst = conn.prepareStatement(UPDATE_T_ROLE_MASTER_WITH_TYPE_FLAG);
			pst.setString(1, typeFlag);
			pst.setString(2, roleId);
			
			int count = pst.executeUpdate();
			
			if(count > 0)
			{
				result = "SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[activateDeactivateRole]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String user_management(String loginId, String typeFlag, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE", query = null;
		
		try 
		{
			if(typeFlag.equalsIgnoreCase("DELETE_USER"))
				query = UPDATE_T_USER_MASTER_WITH_DELETE_FLAG;
			
			pst = conn.prepareStatement(query);
			
			if(typeFlag.equalsIgnoreCase("DELETE_USER"))
			{
				pst.setString(1, "Y");
				pst.setString(2, loginId);
			}
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[user_management]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String edit_user(AdministrationDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "USER_EDIT_FAILURE";
		
		try
		{
			pst = conn.prepareStatement(UPDATE_T_USER_MASTER_WITH_EDIT_DTLS);
			pst.setString(1, dto.getUserName());
			pst.setString(2, dto.getEmailId());
			pst.setString(3, dto.getMobileNumber());
			
			if("on".equalsIgnoreCase(dto.getShowTaskList()))
				pst.setString(4, "Y");
			else
				pst.setString(4, "N");
			
			pst.setString(5, dto.getAgencyCode());
			
			if(dto.getIsActiveCheckBox().equals("on"))
				pst.setString(6, "Y");
			else
				pst.setString(6, "N");
			
			pst.setString(7, dto.getLoginId());
			
			int count = pst.executeUpdate();
			
			if(count > 0)
			{
				pst = conn.prepareStatement(DELETE_FROM_T_USER_ROLE_MAPPING);
				pst.setString(1, dto.getLoginId());
				count = pst.executeUpdate();
				
				if(count > 0)
				{
					for(int i=0; i<dto.getRoleList().length; i++)
					{
						pst = conn.prepareStatement(INSERT_INTO_T_USER_ROLE_MAPPING);
						pst.setString(1, dto.getLoginId());
						pst.setString(2, dto.getRoleList()[i]);
						
						count = pst.executeUpdate();
					}
				}
				
				if(count > 0)
				{
					pst = conn.prepareStatement(GET_JURIS_TYPE_DESCRIPTION);
					pst.setString(1, dto.getJurisType());
					rs = pst.executeQuery();
					rs.first();
					String jurisTypeDesc = rs.getString("Jurisdiction_Type_Desc");
					
					pst = conn.prepareStatement(UPDATE_T_USER_JURISDICTION_MAPPING);
					pst.setString(1, dto.getRegion());
					if(!"".equals(dto.getBase()))
						pst.setString(2, dto.getBase());
					else
						pst.setString(2, "-1");
					
					pst.setString(3, dto.getDzongkhag());
					if(jurisTypeDesc.equalsIgnoreCase("REGIONAL"))
						pst.setString(4, dto.getRegion());
					else
						pst.setString(4, dto.getBase());
					pst.setString(5, dto.getJurisType());
					pst.setString(6, dto.getLoginId());
					
					count = pst.executeUpdate();
					
					if(count > 0)
						result = "USER_EDIT_SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "USER_EDIT_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[edit_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public List<PagePermissionDTO> getPagePermissionList(String roleId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PagePermissionDTO> permissionList = new ArrayList<PagePermissionDTO>();
		PagePermissionDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PAGE_PERMISSION_LIST);
				pst.setString(1, roleId);
				
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new PagePermissionDTO();
					
					dto.setPageId(rs.getString("page_id"));
					dto.setMenuName(rs.getString("menu_name"));
					dto.setPageName(rs.getString("page_name"));
					dto.setIsNew(rs.getString("is_new"));
					dto.setIsEdit(rs.getString("is_edit"));
					dto.setIsDelete(rs.getString("is_delete"));
					dto.setIsDisabled(rs.getString("is_disabled"));
					
					permissionList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at AdministrationDAO[getPagePermissionList]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return permissionList;
	}
	
	public List<PagePermissionDTO> getServicePrivList(String roleId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PagePermissionDTO> servicePrivList = new ArrayList<PagePermissionDTO>();
		PagePermissionDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_SERVICE_LIST);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new PagePermissionDTO();
					dto.setServiceId(rs.getString("Service_Id"));
					dto.setServiceName(rs.getString("Service_Name"));
					
					List<PrivDTO> privList = getPrivilegeList(dto.getServiceId(), conn);
					dto.setPrivileges(privList);
					
					servicePrivList.add(dto);
				}
			}
		} 
		catch (Exception e)
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return servicePrivList;
	}
	
	private List<PrivDTO> getPrivilegeList(String serviceId, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PrivDTO> privList = new ArrayList<PrivDTO>();
		PrivDTO dto;
		
		try
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PRIVILEDGE_LIST);
				//pst.setString(1, "SUBMITTER");
				pst.setString(1, serviceId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new PrivDTO();
					dto.setPrivId(rs.getString("Priv_Id"));
					dto.setPrivName(rs.getString("Priv_Name"));
					
					privList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return privList;
	}
	
	public List<PrivDTO> getPreviousPrivList(String roleId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PrivDTO> privList = new ArrayList<PrivDTO>();
		PrivDTO dto;
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PREVIOUS_PRIV_LIST);
				pst.setString(1, roleId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new PrivDTO();
					dto.setPrivId(rs.getString("Priv_Id"));
					
					privList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return privList;
	}
	
	public List<String> getPageList() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<String> pageIdList = new ArrayList<String>();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PAGE_ID_LIST);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					String pageId = rs.getString("page_id");
					
					pageIdList.add(pageId);
				}
			}
		}
		catch (Exception e) 
		{
			throw new ERALISException("###Error at AdministrationDAO[getPageList]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return pageIdList;
	}
	
	public int getServiceCount() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int serviceCount = 0;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_SERVICE_COUNT);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					serviceCount = rs.getInt("rowCount");
				}
			}
		}
		catch (Exception e) 
		{
			throw new ERALISException("###Error at AdministrationDAO[getServiceCount]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return serviceCount;
	}
	
	public String update_permission_dtls(AdministrationDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "PERMISSION_UPDATE_FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(UPDATE_T_ROLE_MASTER);
			pst.setString(1, dto.getRole());
			pst.setString(2, (dto.getRole().replaceAll(" ", "_")).toUpperCase());
			pst.setString(3, dto.getIsActiveCheckBox());
			pst.setString(4, dto.getRoleId());
			
			int count = pst.executeUpdate();
			
			if(count > 0)
			{
				String[] tempArr = dto.getCheckedVals().split("#");
				
				for(int i = 0; i < tempArr.length; i++)
				{
					String[] checkboxArr = tempArr[i].split("~");
					
					pst = conn.prepareStatement(UPDATE_T_ROLE_PAGE_MAPPING);
					pst.setString(1, checkboxArr[1]);
					pst.setString(2, checkboxArr[2]);
					pst.setString(3, checkboxArr[3]);
					pst.setString(4, checkboxArr[4]);
					pst.setString(5, checkboxArr[0]);
					pst.setString(6, dto.getRoleId());
					
					count = pst.executeUpdate();
				}
				
				pst = conn.prepareStatement(CHECK_IF_PRIV_EXISTS);
				pst.setString(1, dto.getRoleId());
				rs = pst.executeQuery();
				rs.first();
				
				int rowCount = rs.getInt("rowCount");
				
				if(rowCount > 0)
				{
					pst = conn.prepareStatement(DELETE_FROM_T_ROLE_PRIV_MAPPING);
					pst.setString(1, dto.getRoleId());
					
					count = pst.executeUpdate();
				}
				
				String[] tempArr1 = dto.getServiceCheckedVals().split("#");
				
				for(int j = 0; j < tempArr1.length; j++)
				{
					String[] checkedCheckBox = tempArr1[j].split("~");
					
					for(int k = 0; k < checkedCheckBox.length; k++)
					{
						if(!(checkedCheckBox[k].equals("0")))
						{
							pst = conn.prepareStatement(INSERT_INTO_T_ROLE_PRIV_MAPPING);
							pst.setString(1, checkedCheckBox[k]);
							pst.setString(2, dto.getRoleId());
							
							count = pst.executeUpdate();
						}
					}
				}
				
				if(count > 0)
					result = "PERMISSION_UPDATE_SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "PERMISSION_UPDATE_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[update_permission_dtls]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String add_master_data(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result = "MASTER_ADD_FAILED";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				if(dto.getMaster_identifier().equals("ADD_REGION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_REGION);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
					pst.setString(3, dto.getMasterNumber());
				}
				else if(dto.getMaster_identifier().equals("ADD_DZONGKHAG"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_DZONGKHAG);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_GEWOG"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_GEWOG);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_MINISTRY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_MINISTRY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
				}
				else if(dto.getMaster_identifier().equals("ADD_DEPARTMENT"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_DEPARTMENT);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_COUNTRY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_COUNTRY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
				}
				else if(dto.getMaster_identifier().equals("ADD_OFFENCE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_OFFENCE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterNumber());
				}
				else if(dto.getMaster_identifier().equals("ADD_ACCIDENT"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_ACCIDENT);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_ACTION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_ACTION_TAKEN);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
				}
				else if(dto.getMaster_identifier().equals("ADD_DESIGNATION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_DESIGNATION);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_DRIVETYPE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_DRIVETYPE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_ENGINETYPE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_ENGINETYPE);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_HYPOTHECATED"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_HYPOTHECATED);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_OCCUPATION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_OCCUPATION);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_OWNER"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_OWNERTYPE);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_COURTESY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_COURTESY);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_VEHICLE_COMPANY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_VEHICLE_COMPANY);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_VEHICLE_MODEL"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_VEHICLE_MODEL);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getEngineCC());
					pst.setString(4, dto.getSeatCapacity());
					pst.setString(5, dto.getUnladenWeight());
				}
				else if(dto.getMaster_identifier().equals("ADD_VEHICLE_TYPE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_VEHICLE_TYPE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterNumber());
				}
				else if(dto.getMaster_identifier().equals("ADD_REGISTRATION_CODE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_REGISTRATION_CODE);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_DEALER"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_DEALER);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_REASON"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_REASON);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_BASE_OFFICE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_BASE_OFFICE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_BLOOD_GROUP"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_BLOOD_GROUP);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_PRACTICAL_CRITERIA"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_PRACTICAL_CRITERIA);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterNumber());
				}
				else if(dto.getMaster_identifier().equals("ADD_VILLAGE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_VILLAGE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
				}
				else if(dto.getMaster_identifier().equals("ADD_COLOUR"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_COLOUR);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
				}
				else if(dto.getMaster_identifier().equals("ADD_DIPLOMATS"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_DIPLOMATS);
					pst.setString(1, dto.getMasterDesc());
					pst.setString(2, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_PRIVATE_COMPANY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_PRIVATE_COMPANY_MASTER);
					pst.setString(1, dto.getMasterName());
				}
				else if(dto.getMaster_identifier().equals("ADD_ROUTE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.ADD_ROUTE_MASTER);
					pst.setString(1, dto.getMasterName());
				}
				
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "MASTER_ADD_SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "MASTER_ADD_FAILED";
			ConnectionManager.rollbackConnection(conn);
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return result;
		
	}
	
	public String edit_master_data(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result = "MASTER_EDIT_FAILED";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				if(dto.getMaster_identifier().equals("EDIT_REGION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_REGION);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
					pst.setString(3, dto.getMasterNumber());
					pst.setString(4, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_DZONGKHAG"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_DZONGKHAG);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_GEWOG"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_GEWOG);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_MINISTRY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_MINISTRY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_DEPARTMENT"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_DEPARTMENT);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_COUNTRY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_COUNTRY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_OFFENCE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_OFFENCE_DATA);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterNumber());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_ACCIDENT"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_ACCIDENT);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_ACTION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_ACTION_TAKEN);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_DESIGNATION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_DESIGNATION);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_DRIVETYPE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_DRIVETYPE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_ENGINETYPE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_ENGINETYPE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_HYPOTHECATED"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_HYPOTHECATED);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_OCCUPATION"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_OCCUPATION);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_OWNER"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_OWNERTYPE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_COURTESY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_COURTESY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_VEHICLE_COMPANY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_VEHICLE_COMPANY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_VEHICLE_MODEL"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_VEHICLE_MODEL);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getEngineCC());
					pst.setString(4, dto.getSeatCapacity());
					pst.setString(5, dto.getUnladenWeight());
					pst.setString(6, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_VEHICLE_TYPE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_VEHICLE_TYPE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterNumber());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_REGISTRATION_CODE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_REGISTRATION_CODE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_DEALER"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_DEALER);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_REASON"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_REASON);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_BASE_OFFICE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_BASE_OFFICE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_BLOOD_GROUP"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_BLOOD_GROUP);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_PRACTICAL_CRITERIA"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_PRACTICAL_CRITERIA);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterNumber());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_VILLAGE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_VILLAGE);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterOtherId());
					pst.setString(3, dto.getMasterId());
				}

				else if(dto.getMaster_identifier().equals("EDIT_COLOUR"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_COLOUR);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterDesc());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_DIPLOMATS"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_DIPLOMATS);
					pst.setString(1, dto.getMasterDesc());
					pst.setString(2, dto.getMasterName());
					pst.setString(3, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_PRIVATE_COMPANY"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_PRIVATE_COMPANY);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				else if(dto.getMaster_identifier().equals("EDIT_ROUTE"))
				{
					pst = conn.prepareStatement(MasterSQLConstants.EDIT_ROUTE_MASTER);
					pst.setString(1, dto.getMasterName());
					pst.setString(2, dto.getMasterId());
				}
				
				
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "MASTER_EDIT_SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "MASTER_EDIT_FAILED";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return result;
		
	}
	public String delete_master_data(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result = "MASTER_DELETE_FAILED";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				if(dto.getMaster_identifier().equals("DELETE_REGION"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_REGION);
				else if(dto.getMaster_identifier().equals("DELETE_DZONGKHAG"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_DZONGKHAG);
				else if(dto.getMaster_identifier().equals("DELETE_GEWOG"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_GEWOG);
				else if(dto.getMaster_identifier().equals("DELETE_MINISTRY"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_MINISTRY);
				else if(dto.getMaster_identifier().equals("DELETE_DEPARTMENT"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_DEPARTMENT);
				else if(dto.getMaster_identifier().equals("DELETE_COUNTRY"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_COUNTRY);
				else if(dto.getMaster_identifier().equals("DELETE_OFFENCE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_OFFENCE);
				else if(dto.getMaster_identifier().equals("DELETE_ACCIDENT"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_ACCIDENT);
				else if(dto.getMaster_identifier().equals("DELETE_ACTION"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_ACTION_TAKEN);
				else if(dto.getMaster_identifier().equals("DELETE_DESIGNATION"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_DESIGNATION);
				else if(dto.getMaster_identifier().equals("DELETE_DRIVETYPE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_DRIVETYPE);
				else if(dto.getMaster_identifier().equals("DELETE_ENGINETYPE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_ENGINETYPE);
				else if(dto.getMaster_identifier().equals("DELETE_HYPOTHECATED"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_HYPOTHECATED);
				else if(dto.getMaster_identifier().equals("DELETE_OCCUPATION"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_OCCUPATION);
				else if(dto.getMaster_identifier().equals("DELETE_OWNER"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_OWNERTYPE);
				else if(dto.getMaster_identifier().equals("DELETE_COURTESY"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_COURTESY);
				else if(dto.getMaster_identifier().equals("DELETE_VEHICLE_COMPANY"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_VEHICLE_COMPANY);
				else if(dto.getMaster_identifier().equals("DELETE_VEHICLE_MODEL"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_VEHICLE_MODEL);
				else if(dto.getMaster_identifier().equals("DELETE_VEHICLE_TYPE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_VEHICLE_TYPE);
				else if(dto.getMaster_identifier().equals("DELETE_REGISTRATION_CODE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_REGISTRATION_CODE);
				else if(dto.getMaster_identifier().equals("DELETE_DEALER"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_DEALER);
				else if(dto.getMaster_identifier().equals("DELETE_REASON"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_REASON);
				else if(dto.getMaster_identifier().equals("DELETE_BASE_OFFICE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_BASE_OFFICE);
				else if(dto.getMaster_identifier().equals("DELETE_BLOOD_GROUP"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_BLOOD_GROUP);
				else if(dto.getMaster_identifier().equals("DELETE_PRACTICAL_CRITERIA"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_PRACTICAL_CRITERIA);
				else if(dto.getMaster_identifier().equals("DELETE_VILLAGE"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_VILLAGE);
				else if(dto.getMaster_identifier().equals("DELETE_COLOUR"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_COLOUR);
				else if(dto.getMaster_identifier().equals("DELETE_DIPLOMATS"))
					pst = conn.prepareStatement(MasterSQLConstants.DELETE_DIPLOMATS);
				
				
				pst.setString(1, dto.getMasterId());
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "MASTER_DELETE_SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "MASTER_DELETE_FAILED";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return result;
	}
	
	public List<DropDownDTO> getColumnList(String tableName) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DropDownDTO> columnList = new ArrayList<DropDownDTO>();
		DropDownDTO dto;
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				String query = "SELECT * FROM " + tableName;
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				
				// The column count starts from 1
				for (int i = 1; i <= columnCount; i++ ) 
				{
					dto = new DropDownDTO();
					String name = rsmd.getColumnName(i);
					String dataType = rsmd.getColumnTypeName(i);
					dto.setHeaderName(name);
					dto.setHeaderId(dataType);
					columnList.add(dto);
				}
			}
		} 
		catch (Exception e)
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return columnList;
	}
	public List<EralisCommonDTO> getVehicleConstant() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> vehilceConstant = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_VEHICLE_CONSTANT_DTLS);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setVehicleConstantId(rs.getString("Vehicle_Constant_Id"));
					dto.setLoadCapacity(rs.getString("Loading_Capacity"));
					dto.setSeatCapacity(rs.getString("Seating_Capacity"));
					dto.setVehicleHorsePower(rs.getString("Vehicle_HP"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setFlag(rs.getString("Flag"));
					dto.setAmount(rs.getString("Amount"));
					dto.setRcCost(rs.getString("RC_Cost"));
					dto.setFitnessAmount(rs.getString("Fitness_Amount"));
					dto.setOwnershipTransferFees(rs.getString("ownership_transfer_fee"));
					dto.setRegionalTransferFee(rs.getString("regional_transfer_fee"));
					dto.setConversionFee(rs.getString("conversion_fee"));
					dto.setTransferTax(rs.getString("transfer_tax"));
					dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
					
					vehilceConstant.add(dto);
				}	
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return vehilceConstant;
		
	}
	public List<EralisCommonDTO> getLicenseConstant() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> vehilceConstant = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_LICENSE_CONSTANT_DTLS);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new EralisCommonDTO();
					 
					dto.setLicenseConstantId(rs.getString("License_Constant_Id"));
					dto.setRequestType(rs.getString("Request_Type"));
					dto.setServiceType(rs.getString("Service_Type"));

					if(rs.getString("License_Type").equalsIgnoreCase("L"))
						dto.setLicensetype("Learner License");
					else if(rs.getString("License_Type").equalsIgnoreCase("N"))
						dto.setLicensetype("Non Commercial License");
					else if(rs.getString("License_Type").equalsIgnoreCase("C"))
						dto.setLicensetype("Commercial License");
					dto.setAmount(rs.getString("Amount"));
					dto.setLicenseCardCost(rs.getString("License_Card_Cost"));
					
					vehilceConstant.add(dto);
				}	
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return vehilceConstant;
		
	}
	public List<EralisCommonDTO> getPenaltyConstant() throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<EralisCommonDTO> vehilceConstant = new ArrayList<EralisCommonDTO>();
		EralisCommonDTO dto;
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PENALTY_CONSTANT_DTLS);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new EralisCommonDTO();
					dto.setPenaltyId(rs.getString("Penalty_Id"));
					dto.setRequestType(rs.getString("Request_Type"));
					dto.setServiceType(rs.getString("Service_Type"));
					dto.setPenaltyPerDay(rs.getString("Penalty_Per_Day"));
					dto.setMaxPenalty(rs.getString("Max_Penalty"));
					vehilceConstant.add(dto);
				}	
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return vehilceConstant;
		
	}
	public String edit_vehicle_constant(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(UPDATE_VEHICLE_CONSTANT);
			pst.setString(1, dto.getLoadCapacity());
			pst.setString(2, dto.getSeatCapacity());
			pst.setString(3, dto.getVehicleHorsePower());
			pst.setString(4, dto.getEngineCC());
			pst.setString(5, dto.getFlag());
			pst.setString(6, dto.getAmount());
			pst.setString(7, dto.getRcCost());
			pst.setString(8, dto.getFitnessAmount());
			pst.setString(9, dto.getOwnershipTransferFees());
			pst.setString(10, dto.getRegionalTransferFee());
			pst.setString(11, dto.getConversionFee());
			pst.setString(12, dto.getTransferTax());
			pst.setString(13, dto.getVehicleConstantId());
			 
			int count = pst.executeUpdate();			
			
			if(count > 0)
				result = "SUCCESS";

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[add_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String edit_license_constant(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(UPDATE_LICENSE_CONSTANT);
			pst.setString(1, dto.getAmount());
			pst.setString(2, dto.getLicenseCardCost());
			pst.setString(3, dto.getLicenseConstantId());
			int count = pst.executeUpdate();			
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[add_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String edit_penalty_constant(EralisCommonDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(UPDATE_PENALTY_CONSTANT);
			pst.setString(1, dto.getPenaltyPerDay());
			pst.setString(2, dto.getMaxPenalty());
			pst.setString(3, dto.getPenaltyId());
			int count = pst.executeUpdate();			
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationDAO[add_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	//----------------------- QUERIES --------------------------------------------------
	
	
	

	private static final String GET_PENDING_PAYMENT_APPLICATION = "SELECT a.`Receipt_Date`,a.`Receipt_No`,a.`Application_Number`," +
			"a.`Amount_Paid`,a.`Application_Type`" +
			" FROM t_payment_dtls a WHERE a.`Is_Payment_Successful`='N'";
	
	private static final String GET_ROLE_LIST = "SELECT role_id, role_name, is_active FROM t_role_master";
	
	private static final String GET_USER_LIST = "SELECT "
		+ "  a.login_id, "
		+ "  a.name, "
		+ "  a.email_id, "
		+ "  a.mobile_number, "
		+ "  a.show_tasklist, "
		+ "  a.agency_code, "
		+ "  a.is_active, "
		+ "  GROUP_CONCAT( "
		+ "    (SELECT "
		+ "      role_name "
		+ "    FROM "
		+ "      t_role_master "
		+ "    WHERE role_id = b.`role_id`) "
		+ "  ) roleName, "
		+ "  (SELECT "
		+ "    region_name "
		+ "  FROM "
		+ "    t_region_master "
		+ "  WHERE region_id = d.`region_id`) regionName, "
		+ "  (SELECT "
		+ "    dzongkhag_name "
		+ "  FROM "
		+ "    t_dzongkhag_master "
		+ "  WHERE dzongkhag_id = d.`dzongkhag_id`) dzongkhagName, "
		+ "  IF( "
		+ "    d.`base_office_id` IS NOT NULL, "
		+ "    (SELECT "
		+ "      `base_office_name` "
		+ "    FROM "
		+ "      `t_base_office_master` "
		+ "    WHERE `base_office_id` = d.`base_office_id`), "
		+ "    '' "
		+ "  ) baseOfficeName, "
		+ "  (SELECT "
		+ "    `Jurisdiction_Type` "
		+ "  FROM "
		+ "    `t_jurisdiction_type_master` "
		+ "  WHERE `Juris_Type_Id` = d.`juris_type_id`) jurisType, "
		+ "  (SELECT "
		+ "    `Jurisdiction_Type_Desc` "
		+ "  FROM "
		+ "    `t_jurisdiction_type_master` "
		+ "  WHERE `Juris_Type_Id` = d.`juris_type_id`) jurisTypeDesc, "
		+ "  d.`juris_type_id`, "
		+ "  d.`region_id`, "
		+ "  d.`base_office_id`, "
		+ "  d.`dzongkhag_id`, "
		+ "  GROUP_CONCAT(c.`role_id`) roleId "
		+ "FROM "
		+ "  t_user_master a "
		+ "  LEFT JOIN t_user_role_mapping b "
		+ "    ON a.`login_id` = b.`login_id` "
		+ "  LEFT JOIN t_role_master c "
		+ "    ON c.`role_id` = b.`role_id` "
		+ "  LEFT JOIN t_user_jurisdiction_mapping d "
		+ "    ON a.`login_id` = d.`login_id` "
		+ "WHERE a.`is_delete` = 'N' "
		+ "GROUP BY a.`login_id`";
	
	private static final String INSERT_INTO_T_USER_MASTER = "INSERT INTO `t_user_master` ( "
															+ "  `login_id`, "
															+ "  `name`, "
															+ "  `email_id`, "
															+ "  `mobile_number`, "
															+ "  `show_tasklist`, "
															+ "  `agency_code` "
															+ ") "
															+ "VALUES "
															+ "  (?, ?, ?, ?, ?, ?)";
	
	private static final String INSERT_INTO_T_USER_ROLE_MAPPING = "INSERT INTO `t_user_role_mapping` (`login_id`,`role_id`) VALUES (?,?)";
	
	private static final String INSERT_INTO_T_USER_JURISDICTION_MAPPING = "INSERT INTO `t_user_jurisdiction_mapping` ( "
																			+ "  `login_id`, "
																			+ "  `region_id`, "
																			+ "  `base_office_id`, "
																			+ "  `dzongkhag_id`, "
																			+ "  `jurisdiction_id`, "
																			+ "  `juris_type_id` "
																			+ ") "
																			+ "VALUES "
																			+ "  (?, ?, ?, ?, ?, ?)";
	
	private static final String INSERT_INTO_T_ROLE_MASTER = "INSERT INTO `t_role_master` "
															+ "            (`role_name`, "
															+ "             `role_short_desc`, "
															+ "             `is_active`) "
															+ "VALUES (?, "
															+ "        ?, "
															+ "        ?);";
	
	private static final String GET_JURIS_TYPE_DESCRIPTION = "SELECT Jurisdiction_Type_Desc FROM `t_jurisdiction_type_master` WHERE Juris_Type_Id=?";
	
	private static final String GET_PAGE_ID_LIST = "SELECT `page_id` FROM t_page_master WHERE page_link IS NOT NULL";
	
	private static final String GET_SERVICE_COUNT = "SELECT COUNT(*) rowCount FROM t_service_master";
	
	private static final String INSERT_INTO_ROLE_PAGE_MAPPING = "INSERT INTO t_role_page_mapping (role_id, page_id) VALUES (?,?)";
	
	private static final String UPDATE_T_ROLE_MASTER_WITH_TYPE_FLAG = "UPDATE t_role_master SET is_active=? WHERE role_id=?";
	
	private static final String UPDATE_T_USER_MASTER_WITH_DELETE_FLAG = "UPDATE `t_user_master` SET `is_delete`=? WHERE `login_id`=?";
	
	private static final String UPDATE_T_USER_MASTER_WITH_EDIT_DTLS = "UPDATE "
		+ "  `t_user_master` "
		+ "SET "
		+ "  `name` = ?, "
		+ "  `email_id` = ?, "
		+ "  `mobile_number` = ?, "
		+ "  `show_tasklist` = ?, "
		+ "  `agency_code` = ?, "
		+ "  `is_active` = ? "
		+ "WHERE `login_id` = ?";
	
	private static final String UPDATE_T_USER_ROLE_MAPPING = "UPDATE t_user_role_mapping a SET a.`role_id`=? WHERE a.`login_id`=?";
	
	private static final String DELETE_FROM_T_USER_ROLE_MAPPING = "DELETE FROM `t_user_role_mapping` WHERE `login_id` = ?";
	
	private static final String UPDATE_T_USER_JURISDICTION_MAPPING = "UPDATE "
																	+ "  `t_user_jurisdiction_mapping` "
																	+ "SET "
																	+ "  `region_id` = ?, "
																	+ "  `base_office_id` = ?, "
																	+ "  `dzongkhag_id` = ?, "
																	+ "  `jurisdiction_id` = ?, "
																	+ "  `juris_type_id` = ? "
																	+ "WHERE `login_id` = ?";
	
	private static final String GET_PAGE_PERMISSION_LIST = "SELECT "
															+ "  a.`page_id`, "
															+ "  c.`menu_name`, "
															+ "  b.`page_name`, "
															+ "  a.`is_new`, "
															+ "  a.`is_edit`, "
															+ "  a.`is_delete`, "
															+ "  a.`is_disabled` "
															+ "FROM "
															+ "  t_role_page_mapping a "
															+ "  LEFT JOIN t_page_master b "
															+ "    ON a.`page_id` = b.`page_id` "
															+ "  LEFT JOIN t_menu_master c "
															+ "    ON b.`parent_menu_id` = c.`menu_id` "
															+ "WHERE a.`role_id` = ? "
															+ "ORDER BY b.`page_id`, "
															+ "  c.`menu_id`";
	
	private static final String UPDATE_T_ROLE_MASTER = "UPDATE "
														+ "  `t_role_master` "
														+ "SET "
														+ "  `role_name` = ?, "
														+ "  `role_short_desc` = ?, "
														+ "  `is_active` = ? "
														+ "WHERE `role_id` = ?";
	
	private static final String UPDATE_T_ROLE_PAGE_MAPPING = "UPDATE t_role_page_mapping a SET a.`is_new`=?, a.`is_edit`=?, a.`is_delete`=?, a.`is_disabled`=? WHERE a.`page_id`=? AND a.`role_id`=?";
	
	private static final String GET_SERVICE_LIST = "SELECT Service_Id, Service_Name FROM t_service_master";
	
	private static final String GET_PRIVILEDGE_LIST = "SELECT Priv_Id, Priv_Name FROM t_privilege_master WHERE Service_Id=?";
	
	private static final String CHECK_IF_PRIV_EXISTS = "SELECT COUNT(*) rowCount FROM t_role_priv_mapping WHERE Role_Id=?";
	
	private static final String DELETE_FROM_T_ROLE_PRIV_MAPPING = "DELETE FROM t_role_priv_mapping WHERE Role_Id=?";
	
	private static final String INSERT_INTO_T_ROLE_PRIV_MAPPING = "INSERT INTO `t_role_priv_mapping`(`Priv_Id`,`Role_Id`) VALUES (?,?)";
	
	private static final String GET_PREVIOUS_PRIV_LIST = "SELECT "
		+ "  a.Priv_Id "
		+ "FROM "
		+ "  t_privilege_master a "
		+ "  LEFT JOIN t_role_priv_mapping b "
		+ "    ON a.`Priv_Id` = b.`Priv_Id` "
		+ "WHERE b.`Role_Id` = ?";
	
	private static final String GET_VEHICLE_CONSTANT_DTLS	=	" SELECT "
																+ "  a.*, "
																+ "  b.`Vehicle_Type_Name` "
																+ "FROM "
																+ "  `t_vehicle_constant` a "
																+ "  LEFT JOIN `t_vehicle_type_master` b "
																+ "    ON b.`Vehicle_Type_Id` = a.`Vehicle_Type_Id`";
	private static final String UPDATE_VEHICLE_CONSTANT	=	 "UPDATE `t_vehicle_constant` "
														+ "SET "
														+ "  `Loading_Capacity` = ?, "
														+ "  `Seating_Capacity` = ?, "
														+ "  `Vehicle_HP` = ?, "
														+ "  `Engine_CC` = ?, "
														+ "  `Flag` = ?, "
														+ "  `Amount` = ?, "
														+ "  `RC_Cost` = ?, "
														+ "  `Fitness_Amount` = ?, "
														+ "  `ownership_transfer_fee` = ?, "
														+ "  `regional_transfer_fee` = ?, "
														+ "  `conversion_fee` = ?, "
														+ "  `transfer_tax` = ? "
														+ "WHERE `Vehicle_Constant_Id` = ?;";
	private static final String GET_LICENSE_CONSTANT_DTLS	=	"SELECT * FROM `t_license_constant`";
	private static final String UPDATE_LICENSE_CONSTANT	=	"UPDATE `t_license_constant` "
															+ "SET "
															+ "  `Amount` = ?, "
															+ "  `License_Card_Cost` = ? "
															+ "WHERE `License_Constant_Id` = ?;";
	private static final String GET_PENALTY_CONSTANT_DTLS	=	"SELECT * FROM t_penalty_constant";
	private static final String UPDATE_PENALTY_CONSTANT	=	"UPDATE `t_penalty_constant` "
															+ "SET "
															+ "  `Penalty_Per_Day` = ?, "
															+ "  `Max_Penalty` = ? "
															+ "WHERE `Penalty_Id` = ?;";
}
