	package bt.gov.rsta.eralis.dao.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.tools.ToolsDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.MenuVO;
import bt.gov.rsta.framework.vo.SubMenuVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;



public class ToolsDAO 
  {
    private static ToolsDAO toolsDAO;
    SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/mm/yyyy");
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
    SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	public static ToolsDAO getInstance()
	{
		if(toolsDAO == null)
			toolsDAO = new ToolsDAO();
		
		return toolsDAO;
	}
	public String replace_CID(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{	conn = ConnectionManager.getConnection();
		
			if(conn != null)
			{
				pst = conn.prepareStatement(INSERT_REPLACE_CID_DETAILS);
			
				pst.setString(1, dto.getOriginalCID());
				pst.setString(2, dto.getPersonalInfoId());
				
				int count = pst.executeUpdate();
				if(count > 0)
				{	result 	= "SUCCESS";
					dto.setNewCID( dto.getOriginalCID());
				} 
			}
		}
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[replace_CID]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String replace_IID(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String query = null;
		String selectQuery= null;
		ResultSet rs 	= null;
		String Personal_Info_Id = null;
		try 
		{
				query = UPDATE_REPLACE_IID;
			pst = conn.prepareStatement(query);
			pst.setString(1, dto.getNewIID());
			pst.setString(2, dto.getRow_id()); 
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
				selectQuery	=	GET_PERSONAL_INFO_ID_FROM_LICENSE_ID;
				pst = conn.prepareStatement(selectQuery); 				
				pst.setString(1, dto.getCustomerId());
				rs	=	pst.executeQuery();
				rs.first();  
				dto.setPersonalInfoId(rs.getString("Personal_Info_Id"));
				 
		} 
		catch (Exception e) 
		{	 e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[replace_IID]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String delete_Drive_Type(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		int count=0;
		String query=null;
		ResultSet rs 	= null;
		try 
		{ 
			  
				String[] driveTypeArray = dto.getDriveTypeId().split("#");
				for (int i = 0; i < driveTypeArray.length; i++) {
					if (!(driveTypeArray[i].equals("0"))) {
						String[] driveTableArray = driveTypeArray[i].split("_");
						
						if (!(driveTableArray[0].equals("0"))) {
							
							query = "DELETE FROM `t_driving_license_drive_type` WHERE `Customer_Id`=? "; 
							pst = conn.prepareStatement(query+" AND `Drive_Type_Id`=?");
							pst.setString(1, dto.getCustomerId());
							pst.setString(2, driveTableArray[0]);
							pst.executeUpdate();
						}
					}
				}
			 	pst	=	conn.prepareStatement("SELECT l.`Customer_Id`  FROM `t_driving_license_dtls` l   WHERE l.`Driving_License_No`=?");
				pst.setString(1, dto.getLicenseNumber()); 
				rs	=	pst.executeQuery();
				rs.first();
				dto.setCustomerId(rs.getString("Customer_Id")); 
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[delete_Drive_Type]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
    
	public String change_Driving_License_Status(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_CHANGE_DRIVING_LICENSE_STATUS);
			pst.setString(1, dto.getDrivingLicense());
			pst.setString(2, dto.getDuplicateLicense());
			pst.setString(2, dto.getEndorsement());
			pst.setString(2, dto.getRenewal());
			pst.setString(1, dto.getPostRadio());
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[change_Driving_License_Status]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String vehicle_Purchase(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_VEHICLE_PURCHASE);
			pst.setString(1, dto.getDealerName());
			pst.setString(2, dto.getVehicleCompany());
			pst.setString(1, dto.getEngineCC());
			pst.setString(2, dto.getCustomerName());
			pst.setString(2, dto.getCitizenID());
			pst.setString(1, dto.getVehicleModel());
			pst.setString(2, dto.getEngineNo());
			pst.setString(1, dto.getChasisNo());
			pst.setString(2, dto.getYearofManufacture());
			pst.setString(2, dto.getCountryofManufacture());
			pst.setString(1, dto.getInvoiceNo());
			pst.setString(2, dto.getInvoiceDate());
			
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[vehicle_Purchase]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String edit_Road_Block_Entry(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String update ="";
		int counts = 6;
		String photo1	=	"";
		String photo2	=	"";
		String photo3	=	"";
		
		try 		
		{
						
			DocumentVO vo = new DocumentVO();
			
			if(!dto.getPhotoUrl1().getFileName().equalsIgnoreCase(""))
			{
				vo.setFileContent(dto.getPhotoUrl1().getFileData());
				vo.setName(dto.getPhotoUrl1().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().roadBlockImageUploader(vo, dto.getPhotoId());
				photo1	=	vo.getUploadURL();
				update	=	" , Photo1_Url=?";
			}
			if(!dto.getPhotoUrl2().getFileName().equalsIgnoreCase(""))
			{
				vo.setFileContent(dto.getPhotoUrl2().getFileData());
				vo.setName(dto.getPhotoUrl2().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().roadBlockImageUploader(vo, dto.getPhotoId());
				photo2	=	vo.getUploadURL();
				update	=	update+" , Photo2_Url=?";
			}
			if(!dto.getPhotoUrl3().getFileName().equalsIgnoreCase(""))
			{
				vo.setFileContent(dto.getPhotoUrl3().getFileData());
				vo.setName(dto.getPhotoUrl3().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().roadBlockImageUploader(vo, dto.getPhotoId());
				photo3	=	vo.getUploadURL();
				update	=		update+" , Photo3_Url=?";
			}
			pst = conn.prepareStatement(EDIT_ROAD_BLOCK_ENTRY+update+" WHERE  r.`Road_Block_Id`=?"); 
		
			Date BlockDate = sourceSdf.parse(dto.getBlockDate());
			String blockDt = targetSdf.format(BlockDate);
			
			 
			pst.setString(1,blockDt);
			pst.setString(2, dto.getRouteFrom());
			pst.setString(3, dto.getRouteTo());
			pst.setString(4, dto.getBlockLocation());

			Date openDate = sourceSdf.parse(dto.getOpenDate());
			String openDt = targetSdf.format(openDate);
			pst.setString(5,openDt); 
			
			if(!dto.getPhotoUrl1().getFileName().equalsIgnoreCase(""))
			{
				pst.setString(counts, photo1);
				counts=counts+1;
			}
			if(!dto.getPhotoUrl2().getFileName().equalsIgnoreCase(""))
			{
				pst.setString(counts,  photo2);
				counts=counts+1;
			}
			if(!dto.getPhotoUrl3().getFileName().equalsIgnoreCase(""))
			{
				pst.setString(counts,  photo3);
				counts=counts+1;
			} 
			pst.setString(counts, dto.getRow_id()); 
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[edit_Road_Block_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String road_Block_Entry(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String photo1	=	"";
		String photo2	=	"";
		String photo3	=	"";
		Random randomGenerator = new Random();
			
		try {
			
			int  randomNumber = randomGenerator.nextInt(50) + 1;
			DocumentVO vo = new DocumentVO();
			
			if(!dto.getPhotoUrl1().getFileName().equalsIgnoreCase(""))
			{
				vo.setFileContent(dto.getPhotoUrl1().getFileData());
				vo.setName(dto.getPhotoUrl1().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().roadBlockImageUploader(vo, randomNumber);
				photo1	=	vo.getUploadURL();
			}
			if(!dto.getPhotoUrl2().getFileName().equalsIgnoreCase(""))
			{
				vo.setFileContent(dto.getPhotoUrl2().getFileData());
				vo.setName(dto.getPhotoUrl2().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().roadBlockImageUploader(vo, randomNumber);
				photo2	=	vo.getUploadURL();
			}
			if(!dto.getPhotoUrl3().getFileName().equalsIgnoreCase(""))
			{
				vo.setFileContent(dto.getPhotoUrl3().getFileData());
				vo.setName(dto.getPhotoUrl3().getFileName());
				vo.setDocumentType("IMAGE");
				vo = CommonDAO.getInstance().roadBlockImageUploader(vo, randomNumber);
				photo3	=	vo.getUploadURL();
			}
			pst = conn.prepareStatement(INSERT_ROAD_BLOCK_ENTRY); 
			pst.setString(1, dto.getRouteFrom());
			pst.setString(2, dto.getRouteTo());
			pst.setString(3, dto.getBlockLocation());
			Date BlockDate = sourceSdf.parse(dto.getBlockDate());
			String blockDt = targetSdf.format(BlockDate);
			pst.setString(4, blockDt);
			Date openDate = sourceSdf.parse(dto.getOpenDate());
			String openDt = targetSdf.format(openDate);
			pst.setString(5, openDt);
			pst.setString(6, photo1);
			pst.setString(7, photo2);
			pst.setString(8, photo3);
			pst.setString(9, dto.getCreatedBy());
			pst.setString(10, dto.getCreatedOn());
			pst.setInt(11,randomNumber);
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[road_Block_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String hypothecated_To(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_HYPOTHECATED_TO);
			pst.setString(1, dto.getRegion());
			pst.setString(2, dto.getVehicleCode());
			pst.setString(1, dto.getVehicleNumber());
			pst.setString(1, dto.getVehicleType());
			pst.setString(1, dto.getEngineType());
			pst.setString(1, dto.getStatus());
			pst.setString(1, dto.getHypothecatedRadio());
			
			pst.setString(1, dto.getHypothecatedTo());
			pst.setString(1, dto.getLetterNo());
			pst.setString(1, dto.getLetterDate());
			pst.setString(1, dto.getRemoveDate());
			pst.setString(1, dto.getRemarks());
			
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[hypothecated_To]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String delete_License_Punch(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(DELETE_LICENSE_PUNCH);
			pst.setString(1,dto.getPunchId());
			pst.executeUpdate();
			result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	
	public String save_pending_task(ToolsDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(INSERT_INTO_T_PENDING_TASK);
			pst.setString(1, dto.getCustomerId());
			pst.setString(2, dto.getRemarks());
			pst.setString(3, userVo.getActorId());
			int count	=	pst.executeUpdate();
			if(count>0)
			{
				result = "SUCCESS";
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String license_Punch(ToolsDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs	=	null;
		try 
		{
			SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
			pst = conn.prepareStatement(INSERT_LICENSE_PUNCH);
			pst.setString(1, dto.getLicenseId());
			Date punchDate = sourceSdf.parse(dto.getPunchDate());
			String Pdate = targetSdf.format(punchDate);
			pst.setString(2, Pdate);
			pst.setString(3, "N");
			pst.setString(4, dto.getLetterNo());
			Date letterDate = sourceSdf.parse(dto.getLetterDate());
			String lDate = targetSdf.format(letterDate);
			pst.setString(5, lDate);
			pst.setString(6, userVo.getActorId());
			int count	=	pst.executeUpdate();
			if(count>0)
			{
				pst = conn.prepareStatement(COUNT_PUNCHED_DATA);
				pst.setString(1, dto.getLicenseId());
				rs = pst.executeQuery();
				rs.first();
				if(rs.getInt("rowCount")>2)
				{
					pst = conn.prepareStatement(INSERT_PUNCHED_DRIVING_LICENSE_INTO_CANCELLATION);
					pst.setString(1, dto.getLicenseId());
					pst.setString(2, dto.getCustomerId());
					pst.setString(3, userVo.getActorId());
					count	=pst.executeUpdate();
					if(count>0)
					{
						pst = conn.prepareStatement(UPDATE_DRIVING_LICENSE_STATUS);
						pst.setString(1, "C");
						pst.setString(2, dto.getLicenseId());
						pst.executeUpdate();
					}
				}
				result = "SUCCESS";
				
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String withdraw_License_Punch(ToolsDTO dto,UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			java.util.Date letterDate = simpleDateFormat.parse(dto.getLetterDate());
			String lDate = targetSdf.format(letterDate);
			Format formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date today = new Date(System.currentTimeMillis());
			String withdrawnDate = formatter.format(today);
			
			pst = conn.prepareStatement(UPDATE_LICENSE_PUNCH); 
			pst.setString(1, "Y");
			pst.setString(2, withdrawnDate);
			pst.setString(3, dto.getLetterNo());
			pst.setString(4, lDate);
			pst.setString(5, dto.getRemarks());
			pst.setString(6, userVo.getActorId());
			pst.setString(7, withdrawnDate);
			pst.setString(8, dto.getPunchId());
			int count = pst.executeUpdate();
			if(count>0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String license_Print(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_LICENSE_PRINT);
			pst.setString(1, dto.getLongName());
			
			
			
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[license_Print]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	public String refresh_Course_Entry (ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			pst = conn.prepareStatement(INSERT_REFRESH_COURSE_ENTRY);
			pst.setString(1, dto.getLearnerLicenseNo());
			pst.setString(1, dto.getIssuedDate());
			pst.setString(1, dto.getExpiryDate());
			pst.setString(1, dto.getBase());
			pst.setString(1, dto.getNameoftheConductor());

			
			
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[refresh_Course_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}
	
	public List<ToolsDTO> license_holder_list(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null;
		String where	=	"";
		String query	=	"";
		List<ToolsDTO> licenseHolderList = new ArrayList<ToolsDTO>();
		try
		{	int count=1;
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				//CHECKING NON EMPTY VARIABLE
				if(!dto.getCitizenID().equalsIgnoreCase("NA")){
					where	=	" AND p.CID_Number = ? ";
				}
				if(!dto.getLicenseNumber().equalsIgnoreCase("NA")){
					where	=	where + " AND dl.Driving_License_No = ? ";
				}

				query	=	GET_LICENSE_HOLDER_DETAILS;
				pst = conn.prepareStatement(query+where);
				//CLAUSE VALUE
				if(!dto.getCitizenID().equalsIgnoreCase("NA")){
					pst.setString(count, dto.getCitizenID());
					count	=	count+1;
				}
				if(!dto.getLicenseNumber().equalsIgnoreCase("NA")){
					pst.setString(count, dto.getLicenseNumber());
					count	=	count+1;
				}
				
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new ToolsDTO();
					dto.setLicenseId(rs.getString("Driving_License_Id"));
					dto.setPersonalInfoId(rs.getString("Personal_Info_Id")); 
					dto.setCustomerId(rs.getString("Customer_Id")); 
					dto.setCitizenID(rs.getString("CID_Number")); 
					if(rs.getString("Middle_Name").equalsIgnoreCase(""))
					{
						dto.setCustomerName(rs.getString("First_Name")+" "+rs.getString("Last_Name")); 
					}else{
						dto.setCustomerName(rs.getString("First_Name")+" "+rs.getString("Middle_Name")+" "+rs.getString("Last_Name"));
					}
					dto.setLicenseNumber(rs.getString("Driving_License_No")); 
					dto.setRegion(rs.getString("region_name"));  
					dto.setDzongkhag(rs.getString("dzongkhag_name"));  
					dto.setIssuedDate(rs.getString("Issue_Date"));  
					dto.setExpiryDate(rs.getString("Expiry_Date")); 
					
					licenseHolderList.add(dto);
				}
			
			}
		} 
		catch (Exception e) 
		{	e.printStackTrace();
			throw new ERALISException("###Error at ToolsDAO[license_holder_list]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		return  licenseHolderList;
	}
	public List<ToolsDTO> getEndorsementLicenseDetails(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		List<ToolsDTO> endorsmentList = new ArrayList<ToolsDTO>();
		String customerId=null;
		try
		{	 
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				customerId	=	dto.getCustomerId();
				pst = conn.prepareStatement(GET_ENDORSEMENT_LIST); 	
				pst.setString(1, customerId);   
				//pst.setString(2, customerId);  
				
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new ToolsDTO();
					dto.setCustomerId(customerId);
					dto.setNewIID(rs.getString("IID")); 
					dto.setLicenseNumber(rs.getString("Driving_License_No")); 
					dto.setIssuedDate(rs.getString("Issue_Date")); 
					dto.setRow_id(rs.getString("Driving_License_Id"));
					endorsmentList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{	e.printStackTrace();
			throw new ERALISException("###Error at ToolsDAO[getEndorsementLicenseDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return  endorsmentList;
	}
	public List<ToolsDTO> getRoadBlockList(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		List<ToolsDTO> roadBlockList = new ArrayList<ToolsDTO>();
		String whereClause	=	null;
		try
		{	 
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				if(!dto.getRouteFrom().equalsIgnoreCase("Na")){
					whereClause	=	" AND b.`Route_From_Id`=? ";
				}
				if(!dto.getRouteTo().equalsIgnoreCase("Na"))
				{
					whereClause	=	whereClause+" AND b.`Route_To_Id`=?"; 
				}
				pst = conn.prepareStatement(GET_ROAD_BLOCK_LIST+whereClause);
				int count	=	1;
				if(!dto.getRouteFrom().equalsIgnoreCase("Na")){
					pst.setString(count, dto.getRouteFrom());
					count++;
				}
				if(!dto.getRouteTo().equalsIgnoreCase("Na")){
					pst.setString(count, dto.getRouteTo());
				} 
				rs = pst.executeQuery();
				 
				while(rs.next())
				{
					dto = new ToolsDTO();
					dto.setRouteFrom(rs.getString("routeFrom"));
					dto.setRouteTo(rs.getString("routeTo"));
					dto.setBlockLocation(rs.getString("Block_Location"));
					dto.setBlockDate(rs.getString("Block_Date"));
					dto.setRow_id(rs.getString("Road_Block_Id"));
					dto.setOpenDate(rs.getString("Open_Date"));
					dto.setRouteFromId(rs.getString("Route_From_Id"));
					dto.setRouteToId(rs.getString("Route_To_Id"));
					dto.setPhotoId(rs.getInt("Photo_Id"));

					//dto.setPhotoUrl1(rs.getString("Photo1_Url"));
				//	dto.setPhotoUrl2(rs.getString("Photo2_Url"));
					//dto.setPhotoUrl3(rs.getString("Photo3_Url"));
					
					roadBlockList.add(dto);
				}
			
			}
		} 
		catch (Exception e) 
		{	 e.printStackTrace();
			throw new ERALISException("###Error at ToolsDAO[getEndorsementLicenseDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return  roadBlockList;
	}
	public List<ToolsDTO> getDriveType(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		String customerId	=	null;
		List<ToolsDTO> driveType = new ArrayList<ToolsDTO>();
		try
		{	 
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				customerId	=	dto.getCustomerId();
				pst = conn.prepareStatement(GET_LICENSE_DRIVE_TYPE); 
				pst.setString(1, customerId);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new ToolsDTO();
					
					dto.setCustomerId(customerId);
					dto.setDriveTypeId(rs.getString("Drive_Type_Id")); 
					dto.setDriveTypeName(rs.getString("Drive_Type_Name"));
					driveType.add(dto);
				}
			
			}
		} 
		catch (Exception e) 
		{	 
			throw new ERALISException("###Error at ToolsDAO[getEndorsementLicenseDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return  driveType;
	}
	public String delete_Road_Block_Entry(ToolsDTO dto, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 		{
			pst = conn.prepareStatement(DELETE_ROAD_BLOCK); 
			pst.setString(1, dto.getRow_id()); 
			
			
			
			int count = pst.executeUpdate();
			
			if(count > 0)
				result = "SUCCESS";
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsDAO[road_Block_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	 
	public List<ToolsDTO> licensePunchList(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		String customerId	=	null;
		List<ToolsDTO> driveType = new ArrayList<ToolsDTO>();
		try
		{	 
			conn = ConnectionManager.getConnection();

			if(conn != null)
			{
				customerId	=	dto.getCustomerId();
				pst = conn.prepareStatement(GET_LICENSE_PUNCH_LIST); 
				pst.setString(1, customerId); 
				
				rs = pst.executeQuery();
				 
				while(rs.next())
				{
					dto = new ToolsDTO();
					
					dto.setCustomerId(customerId);
					dto.setRow_id(rs.getString("License_Punch_Info_Id"));
					dto.setLicenseNumber(rs.getString("Driving_License_No"));
					
					dto.setLetterDate(rs.getString("Letter_Date"));
					dto.setLetterNo(rs.getString("Letter_No"));
					if(null!=rs.getString("Is_Withdrawn"))
					{
						if("N".equalsIgnoreCase(rs.getString("Is_Withdrawn")))
						{
							dto.setIsWithDrawn("Punched");
						}
						else if("Y".equalsIgnoreCase(rs.getString("Is_Withdrawn")))
						{
							dto.setIsWithDrawn("Withdrawn");
						}
						
					}
					dto.setPunchDate(rs.getString("Punch_Date")); 
					dto.setWithdrawnDate(rs.getString("Withdrawn_Date"));
					dto.setWithdrawnLetterNo(rs.getString("Withdrawn_Letter_No"));
					dto.setRemarks(rs.getString("Remarks"));
					dto.setWithdrawnLetterDate(rs.getString("Withrdawn_Letter_Date"));
					
					driveType.add(dto);
				}
			
			}
		} 
		catch (Exception e) 
		{	 
			throw new ERALISException("###Error at ToolsDAO[getEndorsementLicenseDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return  driveType;
	} 
	public List<ToolsDTO> licensePunchDetails(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs 	= null; 
		String customerId	=	null;
		List<ToolsDTO> punchDetails = new ArrayList<ToolsDTO>();
		try
		{	 
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
				customerId	=	dto.getCustomerId();
				pst = conn.prepareStatement(GET_LICENSE_PUNCH_DETAILS); 
				pst.setString(1, customerId); 
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new ToolsDTO();
					dto.setCustomerId(customerId);
					dto.setRow_id(rs.getString("License_Punch_Info_Id"));
					dto.setLicenseNumber(rs.getString("Driving_License_No"));
					dto.setIsWithDrawn(rs.getString("Is_Withdrawn"));
					dto.setLetterDate(rs.getString("Letter_Date"));
					dto.setLetterNo(rs.getString("Letter_No"));
					dto.setPunchDate(rs.getString("Punch_Date"));  
					
					punchDetails.add(dto);
				}
			
			}
		} 
		catch (Exception e) 
		{	 
			throw new ERALISException("###Error at ToolsDAO[getEndorsementLicenseDetails]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return  punchDetails;
	}
/*
 * Queries
 */
   private static final String INSERT_REPLACE_CID_DETAILS = "UPDATE t_personal_dtls " +
   															"SET CID_Number=? " +
   															"WHERE Personal_Info_Id=?";

   private static final String GET_REGION_LICENSE_HOLDER_LIST	=	"SELECT  dl.`Driving_License_Type_Id`,dl.`Customer_Id`,p.`Personal_Info_Id`, " +
																	"p.`CID_Number`," +
																	" p.`First_Name`," +
																	" p.`Middle_Name`, " +
																	"p.`Last_Name`, " +
																	"dl.`Driving_License_No` ," +
																	" r.`region_name`," +
																	" d.`dzongkhag_name`," +
																	" DATE_FORMAT(dl.`Issue_Date`, '%d/%m/%Y') Issue_Date, " +
																	" DATE_FORMAT(dl.`Expiry_Date`, '%d/%m/%Y') Expiry_Date "														   
																   + " FROM "
																   + "  t_driving_license_dtls dl "
																   + "  LEFT JOIN t_personal_dtls p "
																   + "    ON p.`Customer_Id` = dl.`Customer_Id` "
																   + "  LEFT JOIN t_dzongkhag_master d "
																   + "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` "
																   + "  LEFT JOIN t_gewog_master g "
																   + "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` "
																   + "  LEFT JOIN t_blood_group_master b "
																   + "    ON b.`blood_group_id` = p.`Blood_Group_Id` "
																   + "  LEFT JOIN t_region_master r "
																   + "    ON r.`region_id` = d.`region_id` "
	   															   + "WHERE dl.`Driving_License_Id`>0 AND " 
	   														 	   + " dl.`Status`='A' AND p.`Customer_Id` NOT IN (SELECT c.`Customer_Id` FROM `t_driving_license_cancelled` c)  ";
   
   
   
   
   
   private static final String GET_LICENSE_HOLDER_DETAILS = "SELECT  dl.`Issue_Date` AS non_issue_date,p.`Date_Of_Birth`,dl.`Driving_License_Type_Id`,dl.`Customer_Id`,p.`Personal_Info_Id`,  "
															+ "p.`CID_Number`,"
															+ " p.`First_Name`,"
															+ " p.`Middle_Name`, "
															+ "p.`Last_Name`, "
															+ "dl.`Driving_License_No` ,"
															+ " r.`region_name`,"
															+ " d.`dzongkhag_name`,"
															+ " DATE_FORMAT(dl.`Issue_Date`, '%d-%m-%Y') AS Issue_Date,"
															+ "DATE_FORMAT(dl.`Expiry_Date`, '%d-%m-%Y') AS Expiry_Date, "
															+ "v.`Village_Name`"
															+ ",dl.Driving_License_Id"
															+ " FROM "
															+ "  t_driving_license_dtls dl "
															+ "  LEFT JOIN t_personal_dtls p "
															+ "    ON p.`Customer_Id` = dl.`Customer_Id` "
															+ "  LEFT JOIN t_dzongkhag_master d "
															+ "    ON d.`dzongkhag_id` = p.`Permanent_Dzongkhag_Id` "
															+ "  LEFT JOIN t_gewog_master g "
															+ "    ON g.`Gewog_Id` = p.`Permanent_Gewog_Id` "
															+ "  LEFT JOIN t_blood_group_master b "
															+ "    ON b.`blood_group_id` = p.`Blood_Group_Id` "
															+ "  LEFT JOIN t_region_master r "
															+ "    ON r.`region_id` = dl.`region_id` "
															+ "LEFT JOIN t_village_master v "
															+ "ON v.`Village_Id`=p.`Permanant_Village_Id` "
															+ "WHERE dl.Driving_License_Id>0 ";

   
   private static final String GET_ENDORSEMENT_LIST		=	"SELECT "
															   + "  l.`IID` AS IID, "
															   + "  l.`Driving_License_No`,DATE_FORMAT (l.`Issue_Date`,'%d/%m/%Y') Issue_Date,l.`Driving_License_Id`"
															   + " FROM "
															   + "  t_driving_license_dtls l "
															   + " WHERE l.`Customer_Id` = ?";
   
   private static final String GET_LICENSE_DRIVE_TYPE = "SELECT a.`Drive_Type_Id`,b.`Drive_Type_Name` FROM `t_driving_license_drive_type` a LEFT JOIN `t_drive_type_master` b ON b.`Drive_Type_Id`=a.`Drive_Type_Id` WHERE a.`Customer_Id`=?";
	   
	   /*"SELECT "
														   + "  t.`Drive_Type_Name`, "
														   + "  t.`Drive_Type_Id` , " 
														   + "  'endorsement' AS tableName "
														   + " FROM "
														   + "  t_driving_license_endorsement e "
														   + "  LEFT JOIN `t_drive_type_master` t "
														   + "    ON t.`Drive_Type_Id` = e.`Drive_Type_Id` "
														   + " WHERE e.`Customer_Id` = ? "
														   + " UNION "
														   + " SELECT "
														   + "    t.`Drive_Type_Name`, "
														   + "    t.`Drive_Type_Id` , " 
														   + "    'license' AS tableName "
														   + " FROM "
														   + "    `t_driving_license_dtls` l "
														   + "    LEFT JOIN `t_drive_type_master` t "
														   + "      ON t.Drive_Type_Id = l.`Drive_Type_Id` "
														   + " WHERE l.`Customer_Id` = ? AND l.Drive_Type_Id>0";*/

   private static final String GET_ROAD_BLOCK_LIST 	= 	"SELECT   Photo_Id,t.`dzongkhag_name` AS routeTo,   f.`dzongkhag_name` AS routeFrom, " +
   														" b.`Block_Location`, " +
   														"DATE_FORMAT(b.`Block_Date`, '%d/%m/%Y') AS Block_Date," +
   														"DATE_FORMAT(b.`Open_Date`, '%d/%m/%Y') AS Open_Date, " +
   														" b.`Road_Block_Id`,b.`Route_From_Id`,b.`Route_To_Id`," +
   														"b.`Photo1_Url`,b.`Photo2_Url`,b.`Photo3_Url` " +
   														"FROM  t_road_block_dtls b   " +
   														"LEFT JOIN `t_dzongkhag_master` f     ON f.`dzongkhag_id` = b.`Route_From_Id`   " +
   														"LEFT JOIN `t_dzongkhag_master` t     ON t.`dzongkhag_id` = b.`Route_To_Id`  " +
   														"WHERE  b.`Road_Block_Id`>0 ";
   
   private static final String EDIT_ROAD_BLOCK_ENTRY = "UPDATE `t_road_block_dtls` r " +
   														"SET r.`Block_Date`=?," +
   														"r.`Route_From_Id`=?," +
   														"r.`Route_To_Id`=?," +
   														"r.`Block_Location`=?," +
   														"r.`Open_Date`=?";
   
   private static final String GET_LICENSE_PUNCH_LIST	=	"SELECT "
														   + "  p.`License_Punch_Info_Id`, "
														   + "  l.`Driving_License_No`, "
														   + "  p.`Is_Withdrawn`, "
														   + "  DATE_FORMAT(p.`Letter_Date`, '%d/%m/%Y') AS Letter_Date, "
														   + "  p.`Letter_No`, "
														   + "  DATE_FORMAT(p.`Punch_Date`, '%d/%m/%Y') AS Punch_Date, "
														   + "  l.`Issue_Date`, "
														   + "  l.`Expiry_Date`, "
														   + "  DATE_FORMAT(p.`Withdrawn_Date`, '%d/%m/%Y') AS Withdrawn_Date, "
														   + "  p.`Withdrawn_Letter_No`, "
														   + "  DATE_FORMAT( "
														   + "    p.`Withrdawn_Letter_Date`, "
														   + "    '%d/%m/%Y' "
														   + "  ) AS Withrdawn_Letter_Date,p.`Remarks` "
														   + "FROM "
														   + "  t_driving_license_dtls l "
														   + "  LEFT JOIN `t_license_punch_dtls` p "
														   + "    ON p.`Driving_License_Id` = l.`Driving_License_Id` "
														   + "WHERE l.`Customer_Id` = ? "
														   + "  AND p.`License_Punch_Info_Id` > 0";
   
   private static final String GET_LICENSE_PUNCH_DETAILS	=	"SELECT p.`License_Punch_Info_Id`,l.`Driving_License_No`,p.`Is_Withdrawn`," +
   															"DATE_FORMAT(p.`Letter_Date`, '%d/%m/%Y') AS  Letter_Date,p.`Letter_No`," +
   															"DATE_FORMAT(p.`Punch_Date`, '%d/%m/%Y') AS  Punch_Date " +
															"FROM t_driving_license_dtls l  " +
															"LEFT JOIN `t_license_punch_dtls` p " +
															"ON p.`Driving_License_Id` = l.`Driving_License_Id` " +
															"WHERE	l.`Customer_Id`=? AND p.`License_Punch_Info_Id`>0 AND p.`Is_Withdrawn`='N'";
   
   private static final String DELETE_ROAD_BLOCK	=	"DELETE FROM t_road_block_dtls WHERE Road_Block_Id=?";
   
   private static final String UPDATE_LICENSE_PUNCH	=	"UPDATE "
													   + "  `t_license_punch_dtls` a "
													   + "SET "
													   + "  a.`Is_Withdrawn` = ? , "
													   + "  a.`Withdrawn_Date`=?, "
													   + "  a.`Withdrawn_Letter_No`=?, "
													   + "  a.`Withrdawn_Letter_Date`=?, "
													   + "  a.`Remarks`=?, "
													   + "  a.`Updated_By`=?, "
													   + "  a.`Updated_On`=? "
													   + "WHERE a.`License_Punch_Info_Id` = ?";
   
   private static final String DELETE_LICENSE_PUNCH	=	"DELETE FROM t_license_punch_dtls WHERE license_punch_info_id=?";
   
   private static final String INSERT_DELETE_DRIVE_TYPE_DETAILS = "";
   
   private static final String INSERT_CHANGE_DRIVING_LICENSE_STATUS = "";
   
   private static final String INSERT_VEHICLE_PURCHASE =""; 
   
   private static final String INSERT_HYPOTHECATED_TO ="";
   private static final String INSERT_LICENSE_PUNCH ="INSERT INTO  " +
					   								"`t_license_punch_dtls`" +
					   								"(`Driving_License_Id`,`Punch_Date`, `Is_Withdrawn`,`Letter_No`, `Letter_Date`,Created_By,Created_On)" +
					   								"VALUES " +
					   								"( ?,?,?,?,?,?,CURRENT_TIMESTAMP);";
   private static final String INSERT_LICENSE_PRINT ="";
   
   private static final String INSERT_REFRESH_COURSE_ENTRY ="";
   
   private static final String INSERT_ROAD_BLOCK_ENTRY	=	"INSERT INTO `t_road_block_dtls` ( "
														   + "  `Route_From_Id`, "
														   + "  `Route_To_Id`, "
														   + "  `Block_Location`, "
														   + "  `Block_Date`, "
														   + "  `Open_Date`, "
														   + "  `Photo1_Url`, "
														   + "  `Photo2_Url`, "
														   + "  `Photo3_Url`, "
														   + "  `Created_By`, "
														   + "  `Created_On`,`Photo_Id` "
														   + ") "
														   + "VALUES "
														   + "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?) ;";
   
   private static final String 	GET_PERSONAL_INFO_ID_FROM_LICENSE_ID=	"SELECT a.`Personal_Info_Id` FROM `t_personal_dtls` a WHERE a.`Customer_Id`=?";
   
   private static final String UPDATE_REPLACE_IID	=	"UPDATE t_driving_license_dtls SET IID = ? WHERE `Driving_License_Id`=?";
   
   private static final String COUNT_PUNCHED_DATA	=	"SELECT COUNT(*) AS rowCount FROM `t_license_punch_dtls` a WHERE a.`Driving_License_Id`=? AND a.`Is_Withdrawn`='N'";
   
   private static final String INSERT_PUNCHED_DRIVING_LICENSE_INTO_CANCELLATION	=	"INSERT INTO `t_driving_license_cancelled` "
																				   + "            ( "
																				   + "            `License_Id`, "
																				   + "             `Customer_Id`, "
																				   + "             `Cancellation_Date`, "
																				   + "             `Status`, "
																				   + "             `Cancellation_Reason`, "
																				   + "             `Created_On`, "
																				   + "             `Created_By` "
																				   + "             ) "
																				   + "VALUES ( "
																				   + "        ?, "
																				   + "        ?, "
																				   + "        CURRENT_TIMESTAMP, "
																				   + "        '', "
																				   + "        'License Punched', "
																				   + "        CURRENT_TIMESTAMP, "
																				   + "        ?);";
   
   private static final String UPDATE_DRIVING_LICENSE_STATUS	=	"UPDATE t_driving_license_dtls a SET a.`Status`=? WHERE a.`Driving_License_Id`=?";
   
   private static final String INSERT_INTO_T_PENDING_TASK	=	"INSERT INTO  `t_pending_task` "
															   + "            (`Customer_Id`, "
															   + "             `Remarks`, "
															   + "             `Created_By`, "
															   + "             `Created_On`) "
															   + "VALUES (?, "
															   + "        ?, "
															   + "        ?, "
															   + "        CURRENT_TIMESTAMP);";
  
  }