package bt.gov.rsta.eralis.dao.etest;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.mysql.jdbc.Statement;

import bt.gov.rsta.eralis.dto.etest.ETestDTO;
import bt.gov.rsta.eralis.dto.etest.OptionsDTO;
import bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class ETestDAO 
{
	private static ETestDAO etestDAO = null;
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public static ETestDAO getInstance()
	{
		if(etestDAO == null)
			etestDAO = new ETestDAO();
		
		return etestDAO;
	}
	
	public ApplicationDataVO login(String learnerNo, String dob) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ApplicationDataVO vo = new ApplicationDataVO();
		java.util.Date today = new java.util.Date();
		vo.setStatus("LOGIN_FAILURE");
		
		try {
			String todayStr = targetSdf.format(today);
			
			conn = ConnectionManager.getConnection();
			
			if(conn != null) {
				java.util.Date dobUtil = sourceSdf.parse(dob);
				String tempDob = targetSdf.format(dobUtil);
				
				pst = conn.prepareStatement(CHECK_IF_LEARNER_REGISTERED);
				pst.setString(1, learnerNo);
				pst.setString(2, tempDob);
				rs = pst.executeQuery();
				rs.first();
				int rowCount = rs.getInt("rowCount");
				
				if(rowCount > 0) 
				{
					pst = conn.prepareStatement(CHECK_IF_LEARNER_ALREADY_PASSED);
					pst.setString(1, learnerNo); 
					rs = pst.executeQuery();
					while(rs.next())
					{
						if(rs.getString("Prev_Theory_Test_Status").equalsIgnoreCase("P"))
							vo.setStatus("THEORY_ALREADY_PASSED");
						else if(!(todayStr.equals(rs.getString("testDate"))))
							vo.setStatus("TEST_DATE_MISMATCH");
						else if(rs.getString("todaysTheoryStatus").equalsIgnoreCase("F"))
						{
							if(todayStr.equals(rs.getString("testDate")))
								vo.setStatus("THEORY_ALREADY_ATTEMPTED_FAILED");
						}
						else   
						{
							vo.setStatus("SUCCESS");
							vo.setApplicationNumber(rs.getString("Application_Number"));
						}
					}
					
					if(vo.getStatus().equalsIgnoreCase("SUCCESS"))
					{
						pst = conn.prepareStatement(GET_LEARNER_HOLDER_NAME);
						pst.setString(1, learnerNo);
						rs = pst.executeQuery();
						rs.first();
						vo.setFullName(rs.getString("pname"));
						vo.setStatus("LOGIN_SUCCESS");
					}
				}
				else
					vo.setStatus("NOT_REGISTERED");
			}
		} 
		catch (Exception e) {
			vo.setStatus("LOGIN_FAILURE");
			throw new ERALISException("###Error at ETestDAO[login]: "+e);
		}
		finally {
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return vo;
	}
	
	public synchronized ETestDTO start_test(String learnerNo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ETestDTO dto = new ETestDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				java.util.Date today = new java.util.Date();
				String todayStr = targetSdf.format(today);
					
				pst = conn.prepareStatement(GET_TEST_LOCATION_FOR_ETEST);
				pst.setString(1, learnerNo);
				pst.setString(2, todayStr);
				rs = pst.executeQuery();
				rs.first();
				String testLocationId = rs.getString("Test_Location_Id");
				String juristTypeId = rs.getString("Juris_Type_Id");
				
				pst = conn.prepareStatement(GET_TEST_DURATION);
				pst.setString(1, testLocationId);
				pst.setString(2, juristTypeId);
				pst.setString(3, todayStr);
				rs = pst.executeQuery();
				if(rs.first())
				{
					dto.setTestDuration(rs.getString("Test_Duration"));
					dto.setMaxQuestions(rs.getString("Max_Question"));
					
					pst = conn.prepareStatement(GET_FIRST_QUESTION_ID);
					rs = pst.executeQuery();
					rs.first();
					String questionId = rs.getString("questionId");
					dto.setQuestionId(questionId);
					
					pst = conn.prepareStatement(GET_LAST_QUESTION_ID);
					rs = pst.executeQuery();
					rs.first();
					String lastQuestionId = rs.getString("questionId");
					dto.setLastQuestionId(lastQuestionId);
					
					ArrayList<Integer> questionIdList = generateQuestionIdList(conn, dto.getQuestionId(), dto.getLastQuestionId(), dto.getMaxQuestions());
					dto.setQuestionIdList(questionIdList);
					
					dto.setQuestionId(Integer.toString(questionIdList.get(0)));
					dto.setMessage("FOUND");
				}
				else
				{
					dto.setMessage("NOT_FOUND");
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at ETestDAO[start_test]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	private ArrayList<Integer> generateQuestionIdList(Connection conn, String minQuestionId, String maxQuestionId, String maxQuestions) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		
		try 
		{
			int min = Integer.parseInt(minQuestionId);
			int maxQuestion = Integer.parseInt(maxQuestions);
			int max = Integer.parseInt(maxQuestionId);
			
			Random randomGenerator = new Random();
			while(numbers.size() < maxQuestion)
			{
				int random = randomGenerator.nextInt((max - min) + 1) + min;
				if(!numbers.contains(random))
				{
					pst = conn.prepareStatement(CHECK_IF_QUESTION_EXISTS);
					pst.setString(1, Integer.toString(random));
					rs = pst.executeQuery();
					rs.first();
					int rowCount = rs.getInt("rowCount");
					
					if(rowCount > 0) {
						numbers.add(random);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at ETestDAO[generateQuestionIdList]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return numbers;
	}
	
	public static void main(String[] args) 
	{
		int min = 3;
		int maxQuestion = 20;
		int max = 35;
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		Random randomGenerator = new Random();
		while(numbers.size() < maxQuestion){
			int random = randomGenerator.nextInt((max - min) + 1) + min;
			if(!numbers.contains(random)){
				numbers.add(random);
			}
		}
		
		for(int i=0; i<numbers.size(); i++){
			System.out.println(i+":"+numbers.get(i));
		}
	}
	
	public synchronized ETestDTO get_question(String questionId, String identifier) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ETestDTO dto = new ETestDTO();
		String query = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				//if(identifier.equalsIgnoreCase("INITIAL"))
				query = GET_QUESTION_DETAILS;
				//else
				//	query = GET_NEXT_QUESTION_DETAILS;
				
				pst = conn.prepareStatement(query);
				pst.setString(1, questionId);
				rs = pst.executeQuery();
				rs.first();
				dto.setQuestion(rs.getString("Question"));
				dto.setQuestionId(rs.getString("Question_Id"));
				dto.setName(rs.getString("Image_Name"));
				dto.setPath(rs.getString("Image_Path"));
				
				List<OptionsDTO> optionsList = getOptionsList(conn, dto.getQuestionId());
				dto.setOptionsList(optionsList);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at ETestDAO[start_test]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public List<ETestDTO> getETestConfigDtls(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ETestDTO> etestList = new ArrayList<ETestDTO>();
		ETestDTO dto;
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_ETEST_CONFIG_DTLS);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new ETestDTO();
					dto.setSlNo(rs.getString("Sl_No"));
					dto.setTestDate(rs.getString("Test_Date"));
					dto.setMaxApplicants(rs.getString("Max_Applicants"));
					dto.setTestLocationId(rs.getString("Test_Location_Id"));
					dto.setTestLocation(rs.getString("test_location"));
					dto.setIsActive(rs.getString("Is_Active"));
					dto.setTestDuration(rs.getString("Test_Duration"));
					dto.setTheoryFullMarks(rs.getString("Theory_Full_Point"));
					dto.setTheoryPassMarks(rs.getString("Theory_Pass_Point"));
					dto.setPracticalFullMarks(rs.getString("Practical_Full_Point"));
					dto.setPracticalPassMarks(rs.getString("Partical_Pass_Point"));
					dto.setOverAllPassPercent(rs.getString("Overall_Pass_Percent"));
					dto.setInstituteName(rs.getString("Institute_Name"));
					dto.setTestType(rs.getString("Test_Type"));
					
					etestList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisCommonDAO[getETestConfigDtls]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return etestList;
	}
	
	public ETestDTO getTestConfigDetails(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ETestDTO dto = new ETestDTO();
		
		try
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_ETEST_CONFIG_DTLS);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto.setSlNo(rs.getString("Sl_No"));
					dto.setTestDate(rs.getString("Test_Date"));
					dto.setMaxApplicants(rs.getString("Max_Applicants"));
					dto.setTestLocationId(rs.getString("Test_Location_Id"));
					dto.setTestLocation(rs.getString("test_location"));
					dto.setIsActive(rs.getString("Is_Active"));
					dto.setTestDuration(rs.getString("Test_Duration"));
					dto.setTheoryFullMarks(rs.getString("Theory_Full_Point"));
					dto.setTheoryPassMarks(rs.getString("Theory_Pass_Point"));
					dto.setPracticalFullMarks(rs.getString("Practical_Full_Point"));
					dto.setPracticalPassMarks(rs.getString("Partical_Pass_Point"));
					dto.setOverAllPassPercent(rs.getString("Overall_Pass_Percent"));
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at EralisCommonDAO[getTestConfigDetails]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public String edit_test_details(ETestDTO dto, String jurisId, String jurisTypeId, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "EDIT_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				java.util.Date testDate = sourceSdf.parse(dto.getTestDate());
				String testDate1 = targetSdf.format(testDate);
				
				pst = conn.prepareStatement(UPDATE_T_ETEST_CONFIG_MASTER);
				pst.setString(1, testDate1);
				pst.setString(2, dto.getTestDuration());
				pst.setString(3, dto.getMaxApplicants());
				pst.setString(4, jurisId);
				pst.setString(5, jurisTypeId);
				if(dto.getIsActive().equalsIgnoreCase("on"))
					pst.setString(6, "Y");
				else
					pst.setString(6, "N");
				pst.setString(7, dto.getTestType());
				pst.setString(8, dto.getInstituteName());
				pst.setString(9,  userVo.getActorId());
				pst.setString(10, dto.getSlNo());
				
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "EDIT_SUCCESS";
			}
		} 
		catch (Exception e)
		{
			result = "EDIT_FAILURE";
			throw new ERALISException("###Error at ETestDAO[edit_test_details]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return result;
	}
	
	public String add_test_details(ETestDTO dto, String jurisId, String jurisTypeId, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "ADD_FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				java.util.Date testDate = sourceSdf.parse(dto.getAddTestDate());
				String testDate1 = targetSdf.format(testDate);
				
				pst = conn.prepareStatement(INSERT_INTO_ETEST_CONFIG_MASTER);
				pst.setString(1, testDate1);
				pst.setString(2, dto.getTestDuration());
				pst.setString(3, dto.getMaxApplicants());
				pst.setString(4, dto.getRstaMaxApplicant());
				pst.setString(5, jurisId);
				pst.setString(6, jurisTypeId);
				if(dto.getIsActive().equalsIgnoreCase("on"))
					pst.setString(7, "Y");
				else
					pst.setString(7, "N");

				pst.setString(8, dto.getTestType());
				pst.setString(9, dto.getInstituteName());
				pst.setString(10, userVo.getActorId());
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "ADD_SUCCESS";
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[edit_test_details]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return result;
	}
	
	public String add_questions(Connection conn, ETestDTO dto) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "ADD_FAILURE", questionId = null, optionId = null;
		
		try 
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(INSERT_INTO_T_ETEST_QUESTION_MASTER, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, dto.getQuestion());
				pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				while(rs.next())
					questionId = rs.getString(1);
					/**PICTURE QUESTION STARTS**/
					if(null != dto.getUpload() && 0 != dto.getUpload().getFileSize())
					{
						DocumentVO vo = new DocumentVO();
						vo.setFileContent(dto.getUpload().getFileData());
						vo.setName(dto.getUpload().getFileName());
						vo.setDocumentType("IMAGE");
						vo = imageUploader(vo, questionId);
						
						pst = conn.prepareStatement(UPDATE_ETEST_QUESTION_IMAGE);
						pst.setString(1, vo.getUploadURL());
						pst.setString(2, vo.getName());
						pst.setString(3, questionId);
						pst.executeUpdate();
					}
					/**PICTURE QUESTION ENDS**/
				
				for(int i=0; i<dto.getOptions().length; i++)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_ETEST_OPTION_MASTER, PreparedStatement.RETURN_GENERATED_KEYS);
					pst.setString(1, dto.getOptions()[i]);
					pst.setString(2, questionId);
					pst.executeUpdate();
					rs = pst.getGeneratedKeys();
					while(rs.next())
						optionId = rs.getString(1);
					
					System.out.println("Is answer: "+Integer.parseInt(dto.getIsAnswer()));
					
					if(i == Integer.parseInt(dto.getIsAnswer()))
					{
						pst = conn.prepareStatement(INSERT_INTO_T_ETEST_QUESTION_ANSWER_MAPPING);
						pst.setString(1, questionId);
						pst.setString(2, optionId);
						
						int count = pst.executeUpdate();
						
						if(count > 0)
							result = "ADD_SUCCESS";
					}
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[edit_test_details]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	public DocumentVO imageUploader(DocumentVO documentVo, String imageName)throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String filePath = null, uuid = null;

		try {
			filePath = EralisCommonUtil.getProperty("config.system.imageWritePath")+ "/ETEST_QUESTION_IMAGE/"+ imageName+".jpg";
			Log.debug("filePath = " + filePath);
			// check if the directory structure exists or not, if not then
			// create the directory structure
			new File(EralisCommonUtil.getProperty("config.system.imageWritePath")).mkdirs();
			File newFile = new File(filePath);
			FileOutputStream fileOutputStream;
			fileOutputStream = new FileOutputStream(newFile);
			fileOutputStream.write(documentVo.getFileContent());
			fileOutputStream.flush();
			fileOutputStream.close();
			uuid = UUID.randomUUID().toString();
		} catch (Exception e) {
			throw new ERALISException("### Error at CommonDAO[imageUploader]: exception:: "+ e.getMessage());
		} finally {
			documentVo.setUploadURL(filePath);
			documentVo.setUuid(uuid);
			documentVo.setName(imageName+".jpg");
			ConnectionManager.close(conn, null, null, pst);
		}
		return documentVo;
	}
	
	public String edit_question(Connection conn, ETestDTO dto) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "ADD_FAILURE", optionId = null;
		
		try 
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(UPDATE_T_ETEST_QUESTION_MASTER);
				pst.setString(1, dto.getQuestion());
				pst.setString(2, dto.getQuestionId());
				int count = pst.executeUpdate();
				
				if(count > 0)
				{
					/**PICTURE QUESTION STARTS**/
					if(null != dto.getUpload() && 0 != dto.getUpload().getFileSize())
					{
						DocumentVO vo = new DocumentVO();
						vo.setFileContent(dto.getUpload().getFileData());
						vo.setName(dto.getUpload().getFileName());
						vo.setDocumentType("IMAGE");
						vo = imageUploader(vo, dto.getQuestionId());
						/*
						pst = conn.prepareStatement(UPDATE_ETEST_QUESTION_IMAGE);
						pst.setString(1, vo.getUploadURL());
						pst.setString(2, vo.getName());
						pst.setString(3, dto.getQuestionId());
						pst.executeUpdate();*/
					}
					/**PICTURE QUESTION ENDS**/
					pst = conn.prepareStatement(DELETE_FROM_T_ETEST_QUESTION_ANSWER_MAPPING);
					pst.setString(1, dto.getQuestionId());
					count = pst.executeUpdate();
					
					if(count > 0)
					{
						pst = conn.prepareStatement(DELETE_FROM_T_ETEST_OPTIONS_MASTER);
						pst.setString(1, dto.getQuestionId());
						count = pst.executeUpdate();
						
						if(count > 0)
						{
							for(int i=0; i<dto.getOptions().length; i++)
							{
								pst = conn.prepareStatement(INSERT_INTO_T_ETEST_OPTION_MASTER, PreparedStatement.RETURN_GENERATED_KEYS);
								pst.setString(1, dto.getOptions()[i]);
								pst.setString(2, dto.getQuestionId());
								pst.executeUpdate();
								rs = pst.getGeneratedKeys();
								while(rs.next())
									optionId = rs.getString(1);
								
								if(i == Integer.parseInt(dto.getIsAnswer()))
								{
									pst = conn.prepareStatement(INSERT_INTO_T_ETEST_QUESTION_ANSWER_MAPPING);
									pst.setString(1, dto.getQuestionId());
									pst.setString(2, optionId);
									
									count = pst.executeUpdate();
									
									if(count > 0)
										result = "EDIT_SUCCESS";
								}
							}
						}
					}
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			result = "ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[edit_test_details]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	public String delete_question(Connection conn, String questionId) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "DELETE_FAILURE";;
		
		try
		{
			pst = conn.prepareStatement(DELETE_FROM_T_ETEST_QUESTION_ANSWER_MAPPING);
			pst.setString(1, questionId);
			int count = pst.executeUpdate();
			
			if(count > 0)
			{
				pst = conn.prepareStatement(DELETE_FROM_T_ETEST_OPTIONS_MASTER);
				pst.setString(1, questionId);
				count = pst.executeUpdate();
				
				if(count > 0)
				{
					pst = conn.prepareStatement(DELETE_FROM_T_ETEST_QUESTIONS_MASTER);
					pst.setString(1, questionId);
					count = pst.executeUpdate();
					
					if(count > 0)
						result = "DELETE_SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			result = "DELETE_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[delete_question]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return result;
	}
	
	public ETestDTO get_edit_dtls(String questionId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ETestDTO dto = new ETestDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_QUESTION_DETAILS);
				pst.setString(1, questionId);
				rs = pst.executeQuery();
				rs.first();
				dto.setQuestion(rs.getString("Question"));
				dto.setName(rs.getString("Image_Name"));
				dto.setPath(rs.getString("Image_Path"));
				
				pst = conn.prepareStatement(GET_ANSWER_DETAILS);
				pst.setString(1, questionId);
				rs = pst.executeQuery();
				rs.first();
				dto.setIsAnswer(rs.getString("Option_Id"));
				
				List<OptionsDTO> optionsList = getOptionsList(conn, questionId);
				dto.setOptionsList(optionsList);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error in ETestDAO[get_edit_dtls]: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	private List<OptionsDTO> getOptionsList(Connection conn, String questionId) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<OptionsDTO> optionsList = new ArrayList<OptionsDTO>();
		OptionsDTO dto;
		
		try 
		{
			pst = conn.prepareStatement(GET_OPTIONS_LIST);
			pst.setString(1, questionId);
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				dto = new OptionsDTO();
				dto.setOptionId(rs.getString("Option_Id"));
				dto.setOptionName(rs.getString("Option"));
				
				optionsList.add(dto);
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at ETestDAO[getOptionsList]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
			Collections.shuffle(optionsList);
		}
		
		return optionsList;
	}
	
	public ETestDTO process_result(String testResult, String name, String learnerNo, String applicationNo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ETestDTO dto = new ETestDTO();
		int marksObtained = 0, questionCounter = 0;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				String[] tempArray = testResult.split("#");
				for(int i=0; i<tempArray.length; i++)
				{
					String[] tempArray1 = tempArray[i].split("~");
					
					pst = conn.prepareStatement(GET_QUESTION_ANSWER_MAPPING);
					pst.setString(1, tempArray1[0]);
					pst.setString(2, tempArray1[1]);
					rs = pst.executeQuery();
					rs.first();
					
					int rowCount = rs.getInt("rowCount");
					
					if(rowCount > 0)
					{
						marksObtained += 2;
					}
					
					questionCounter++;
				}
			}
		}
		catch (Exception e)
		{
			throw new ERALISException("###Error at ETestDAO[process_result]: "+e);
		}
		finally
		{
			try 
			{
				pst = conn.prepareStatement(GET_PASS_PERCENT_FOR_THEORY);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				int theoryPassPoint = rs.getInt("Theory_Pass_Point");
				
				dto.setTheoryFullMarks(rs.getString("Theory_Full_Point"));
				
				pst = conn.prepareStatement(GET_TEST_RESULT);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				
				dto.setName(name);
				dto.setLearnerNo(learnerNo);
				dto.setTestDate(rs.getString("testDate"));
				dto.setTestLocation(rs.getString("testLocation"));
				dto.setQuestionsAttempted(Integer.toString(questionCounter));
				dto.setMarksObtained(Integer.toString(marksObtained));
				
				if(marksObtained >= theoryPassPoint)
					dto.setTestStatus("P");
				else
					dto.setTestStatus("F"); 
				
				pst = conn.prepareStatement(UPDATE_TEST_RESULT);
				pst.setString(1, Integer.toString(marksObtained));
				pst.setString(2, dto.getTestStatus());
				pst.setString(3, applicationNo);
				pst.executeUpdate();
			} 
			catch (SQLException e)
			{
				throw new ERALISException("###Error at ETestDAO[process_result]: "+e);
			}
			
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public List<PracticalCriteriaDTO> getPracticalCriteriaList() throws ERALISSystemException, ERALISException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PracticalCriteriaDTO> practicalCriterialList = new ArrayList<PracticalCriteriaDTO>();
		PracticalCriteriaDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_PRACTICAL_CRITERIA_LIST);
				rs = pst.executeQuery();
				while(rs.next())
				{
					dto = new PracticalCriteriaDTO();
					dto.setCriteriaId(rs.getString("Practical_Criteria_Id"));
					dto.setCriteriaName(rs.getString("Criteria_Name"));
					dto.setCriteriaPoint(rs.getString("Full_Point"));
					
					practicalCriterialList.add(dto);
				}
			}
		}
		catch (Exception e)
		{
			throw new ERALISException("###Error at ETestDAO[getPracticalCriterialList] "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return practicalCriterialList;
	}
	
	public String submit_marksheet(Connection conn, ETestDTO dto, UserDetailsVO vo) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "MARKSHEET_FAILURE";
		Date date = new Date(System.currentTimeMillis());
		
		try 
		{
			pst = conn.prepareStatement(UPDATE_T_ETEST_APPLICATION);
			pst.setString(1, dto.getMarksObtained());
			pst.setString(2, dto.getTestStatus());
			pst.setString(3, dto.getPracticalMarksObtained());
			pst.setString(4, dto.getPracticalTestStatus());
			pst.setString(5, dto.getOverAllMarksObtained());
			pst.setString(6, "N");
			pst.setString(7, vo.getActorId()+"("+vo.getActorName()+")");
			pst.setDate(8, date);
			pst.setString(9, dto.getAppNo());
			
			int count = pst.executeUpdate();
			
			if(count > 0)
			{
				result = "MARKSHEET_SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "MARKSHEET_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[submit_marksheet]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	
	public String candidate_list(Connection conn, String applicationNo) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			pst = conn.prepareStatement(CANCEL_ETEST_BOOKING);
			pst.setString(1, applicationNo);
			int count = pst.executeUpdate();
			if(count > 0)
			{
				result = "SUCCESS";
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[candidate_list]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	
	
	public String CID_submit_marksheet(Connection conn, ETestDTO dto, UserDetailsVO vo) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "MARKSHEET_FAILURE";
		Date date = new Date(System.currentTimeMillis());
		ResultSet resultSet = null;
		String generatedId = "0";
		try 
		{
			pst = conn.prepareStatement(INSERT_INTO_ETEST_APPLICATION,PreparedStatement.RETURN_GENERATED_KEYS);
			pst.setString(1, dto.getLearnerNo());
			int count = pst.executeUpdate();
			if(count > 0)
			{
				resultSet = pst.getGeneratedKeys();
				while (resultSet.next()) {
					generatedId = resultSet.getString(1);
				}
				pst = conn.prepareStatement(UPDATE_T_ETEST_APPLICATION_AGAINST_CID);
				pst.setString(1, dto.getMarksObtained());
				pst.setString(2, dto.getTestStatus());
				pst.setString(3, dto.getPracticalMarksObtained());
				pst.setString(4, dto.getPracticalTestStatus());
				pst.setString(5, dto.getOverAllMarksObtained());
				pst.setString(6, "N");
				pst.setString(7, vo.getActorId()+"("+vo.getActorName()+")");
				pst.setDate(8, date);
				pst.setString(9, dto.getDriveType());
				pst.setString(10, vo.getActorId()+"("+vo.getActorName()+")");
				pst.setDate(11, date);
				pst.setString(12, "NEW");
				pst.setString(13, generatedId);
				count	=	pst.executeUpdate();
				if(count > 0)
				{
					result = "MARKSHEET_SUCCESS";
				}
				
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			result = "MARKSHEET_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ETestDAO[submit_marksheet]: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public List<DropDownDTO> getTestDateList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DropDownDTO> testDateList = new ArrayList<DropDownDTO>();
		DropDownDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_TEST_DATE_LIST);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new DropDownDTO();
					dto.setHeaderId(rs.getString("testDate"));
					dto.setHeaderName(rs.getString("Test_Date"));
					
					testDateList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return testDateList;
	}
	public List<DropDownDTO> getCandidateTestDateList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DropDownDTO> testDateList = new ArrayList<DropDownDTO>();
		DropDownDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_CANDIDATE_TEST_DATE_LIST);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new DropDownDTO();
					dto.setHeaderId(rs.getString("testDate"));
					dto.setHeaderName(rs.getString("Test_Date"));
					
					testDateList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return testDateList;
	}
	
	public List<ETestDTO> candidate_list(String testDate, String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ETestDTO> candidateList = new ArrayList<ETestDTO>();
		ETestDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_CANDIDATE_LIST);
				pst.setString(1, jurisId);
				pst.setString(2, jurisTypeId);
				pst.setString(3, testDate);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new ETestDTO();
					dto.setAppNo(rs.getString("Application_Number"));
					dto.setLearnerNo(rs.getString("Learner_License_No"));
					dto.setName(rs.getString("pname"));
					dto.setPhoneNo(rs.getString("Present_Phone_No"));
					dto.setEmailId(rs.getString("Present_Email"));
					dto.setContactAddres(rs.getString("Present_Contact_Address"));
					dto.setIssueType(rs.getString("Issue_Type"));
					dto.setCID(rs.getString("cid_number"));
					
					if(rs.getString("Test_Type").equals("NEW"))
						dto.setTestType("New");
					else
						dto.setTestType("Endorsement");
					
					dto.setDriveType(rs.getString("driveType"));

					dto.setTheoryMarksObtained(rs.getString("Theory_Marks_Obtained"));
					dto.setPracticalMarksObtained(rs.getString("Practical_Marks_Obtained"));
					dto.setPracticalTestStatus(rs.getString("Practical_Test_Status"));
					dto.setTheoryTestStatus(rs.getString("Theory_Test_Status"));
					candidateList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return candidateList;
	}
	
	/*** Query section ***/
	
	private static final String GET_SL_NO = "SELECT Sl_No FROM t_etest_config_master a WHERE a.`Test_Date`=? AND a.`Test_Location_Id`=? AND a.`Juris_Type_Id`=?";
	
	private static final String GET_CANDIDATE_LIST = "SELECT a.Application_Number,a.`Customer_Id`, "
		+ "  a.`Learner_License_No`, "
		+ "IF(a.`Issue_Type`='Driving_Institue' OR a.`Issue_Type`='Learner_License', "
		+ "  (SELECT b.`CID_Number` FROM `t_personal_dtls` b WHERE b.`Customer_Id`=a.`Customer_Id`) "
		+ "  ,a.`Customer_Id`) cid_number,"
		+ "  IF(a.`Issue_Type`='Driving_Institue' OR a.`Issue_Type`='Learner_License', "
		+ "  (SELECT CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) FROM `t_personal_dtls` b WHERE b.`Customer_Id`=a.`Customer_Id`) "
		+ "  ,(SELECT CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) FROM `t_personal_dtls` b WHERE b.`CID_Number`=a.`Customer_Id` LIMIT 1)) pname, "
		+ "  IF(a.`Issue_Type`='Driving_Institue' OR a.`Issue_Type`='Learner_License', "
		+ "  (SELECT b.`Present_Phone_No` FROM `t_personal_dtls` b WHERE b.`Customer_Id`=a.`Customer_Id`) "
		+ "  ,(SELECT b.`Present_Phone_No` FROM `t_personal_dtls` b WHERE b.`CID_Number`=a.`Customer_Id` LIMIT 1)) Present_Phone_No, "
		+ "  IF(a.`Issue_Type`='Driving_Institue' OR a.`Issue_Type`='Learner_License', "
		+ "  (SELECT b.`Present_Email` FROM `t_personal_dtls` b WHERE b.`Customer_Id`=a.`Customer_Id`) "
		+ "  ,(SELECT b.`Present_Email` FROM `t_personal_dtls` b WHERE b.`CID_Number`=a.`Customer_Id` LIMIT 1)) Present_Email, "
		+ "  IF(a.`Issue_Type`='Driving_Institue' OR a.`Issue_Type`='Learner_License', "
		+ "  (SELECT b.`Present_Contact_Address` FROM `t_personal_dtls` b WHERE b.`Customer_Id`=a.`Customer_Id`) "
		+ "  ,(SELECT b.`Present_Contact_Address` FROM `t_personal_dtls` b WHERE b.`CID_Number`=a.`Customer_Id` LIMIT 1)) Present_Contact_Address, "
		+ "  a.`Test_Type`, "
		+ "  a.`Issue_Type`, "
		+ "  (SELECT "
		+ "    `Drive_Type_Name` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = a.`Drive_Type_Id`) driveType," +
		" IF(a.`Theory_Test_Status`='P','Passed',(IF(a.`Theory_Test_Status`='F','FAILED','NONE'))) Theory_Test_Status, "
		+ "  a.`Theory_Marks_Obtained`, a.`Practical_Marks_Obtained`," +
				"IF(a.`Practical_Test_Status`='P','Passed',(IF(a.`Practical_Test_Status`='F','FAILED','NONE'))) Practical_Test_Status  "
		+ "FROM "
		+ "  t_etest_application a"
		+ "  LEFT JOIN t_personal_dtls b "
		+ "    ON a.`Customer_Id` = b.`Customer_Id` "
		+ "WHERE a.`Test_Location_Id` = ? "
		+ "  AND a.`Juris_Type_Id` = ? "
		+ "  AND a.`Test_Date` = ?";
	
	private static final String GET_CANDIDATE_TEST_DATE_LIST = "SELECT "
																+ "  DATE_FORMAT(Test_Date, '%d-%m-%Y') Test_Date, "
																+ "  DATE_FORMAT(Test_Date, '%Y-%m-%d') AS testDate "
																+ "FROM "
																+ "  t_etest_config_master "
																+ "WHERE `Test_Location_Id` = ? "
																+ "  AND `Juris_Type_Id` = ? "
																+ "ORDER BY DATE(Test_Date) DESC, "
																+ "  Test_Date ASC";
															
	private static final String GET_TEST_DATE_LIST = "SELECT "
													+ "  DATE_FORMAT(Test_Date, '%d-%m-%Y') Test_Date, "
													+ "  DATE_FORMAT(Test_Date, '%Y-%m-%d') AS testDate "
													+ "FROM "
													+ "  t_etest_config_master "
													+ "WHERE `Test_Location_Id` = ? "
													+ "  AND `Juris_Type_Id` = ? "
													+ "  AND Test_Date > CURRENT_DATE "
													+ "ORDER BY Test_Date";
	
	private static final String UPDATE_T_ETEST_QUESTION_MASTER = "UPDATE `t_etest_question_master` a SET a.`Question`=? WHERE a.`Question_Id`=?";
	
	private static final String GET_QUESTION_DETAILS = "SELECT a.Question_Id, a.`Question`,a.`Image_Name`,a.`Image_Path` FROM t_etest_question_master a WHERE a.`Question_Id`=?";
	
	private static final String GET_NEXT_QUESTION_DETAILS = "SELECT a.Question_Id, a.`Question` FROM t_etest_question_master a WHERE a.`Question_Id` > ? LIMIT 1";
	
	private static final String GET_OPTIONS_LIST = "SELECT a.Option_Id, a.Option FROM t_etest_option_master a WHERE a.`Question_Id`=?";
	
	private static final String GET_ANSWER_DETAILS = "SELECT a.Option_Id FROM `t_etest_question_answer_mapping` a WHERE a.`Question_Id`=?";
	
	private static final String GET_ETEST_CONFIG_DTLS = "SELECT "
		+ "  Sl_No, "
		+ "  DATE_FORMAT(a.Test_Date, '%d-%m-%Y') Test_Date, "
		+ "  a.`Test_Duration`, "
		+ "  a.Max_Applicants, "
		+ "  a.Test_Location_Id,"
		+ "  IF( "
		+ "    a.`Juris_Type_Id` = '1', "
		+ "    (SELECT "
		+ "      `region_name` AS test_location "
		+ "    FROM "
		+ "      t_region_master "
		+ "    WHERE `region_id` = a.`Test_Location_Id`), "
		+ "    (SELECT "
		+ "      `base_office_name` AS test_location "
		+ "    FROM "
		+ "      `t_base_office_master` "
		+ "    WHERE `base_office_id` = a.`Test_Location_Id`) "
		+ "  ) test_location, "
		+ "  Is_Active, "
		+ "  a.`Theory_Pass_Point`, "
		+ "  a.`Partical_Pass_Point`, "
		+ "  a.`Theory_Full_Point`, "
		+ "  a.`Practical_Full_Point`, "
		+ "  a.`Overall_Pass_Percent`, "
		+ " a.`Test_Type`,"
		+ " a.`Institute_Name` "
		+ "FROM "
		+ "  `t_etest_config_master` a "
		+ "WHERE a.Test_Location_Id = ? "
		+ "  AND a.`Juris_Type_Id` = ? ORDER BY a.`Test_Date` DESC";
	
	private static final String UPDATE_T_ETEST_CONFIG_MASTER = "UPDATE `t_etest_config_master` a SET " +
			"a.`Test_Date`=?, a.`Test_Duration`=?, a.`Max_Applicants`=?, a.Test_Location_Id=?, a.Juris_Type_Id=?, a.Is_Active=?,a.`Test_Type`=?,a.`Institute_Name`=?,a.`Update_By`=?,a.`Updated_On`=CURRENT_TIMESTAMP WHERE a.`Sl_No`=?";
	
	private static final String INSERT_INTO_ETEST_CONFIG_MASTER = "INSERT INTO `t_etest_config_master` ( "
		+ "  `Test_Date`, "
		+ "  `Test_Duration`, "
		+ "  `Max_Applicants`," 
		+ " RSTA_Max_Applicants, "
		+ "  `Test_Location_Id`, "
		+ "  `Juris_Type_Id`, "
		+ "  `Is_Active`," 
		+ "`Test_Type`,"
		+ "`Institute_Name`,Created_By "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String INSERT_INTO_T_ETEST_QUESTION_MASTER = "INSERT INTO `t_etest_question_master` (`Question`) VALUES (?)";
	
	private static final String INSERT_INTO_T_ETEST_OPTION_MASTER = "INSERT INTO `t_etest_option_master` (`Option`, `Question_Id`) VALUES (?, ?)";
	
	private static final String INSERT_INTO_T_ETEST_QUESTION_ANSWER_MAPPING = "INSERT INTO `t_etest_question_answer_mapping` "
																				+ "            (`Question_Id`, "
																				+ "             `Option_Id`) "
																				+ "VALUES (?, "
																				+ "        ?)";
	
	private static final String DELETE_FROM_T_ETEST_QUESTION_ANSWER_MAPPING = "DELETE FROM `t_etest_question_answer_mapping` WHERE Question_Id=?";
	
	private static final String DELETE_FROM_T_ETEST_OPTIONS_MASTER = "DELETE FROM `t_etest_option_master` WHERE Question_Id=?";
	
	private static final String DELETE_FROM_T_ETEST_QUESTIONS_MASTER = "DELETE FROM `t_etest_question_master` WHERE Question_Id=?";
	
	private static final String CHECK_IF_LEARNER_REGISTERED = "SELECT COUNT(*) rowCount FROM `t_etest_application` a WHERE a.`Learner_License_No`=? AND a.`DOB`=?";
	
	private static final String GET_LEARNER_HOLDER_NAME = "SELECT "
														+ "  CONCAT( "
														+ "    c.`First_Name`, "
														+ "    ' ', "
														+ "    c.`Middle_Name`, "
														+ "    ' ', "
														+ "    c.`Last_Name` "
														+ "  ) pname "
														+ "FROM "
														+ "  t_etest_application a "
														+ "  LEFT JOIN t_learner_license_dtls b "
														+ "    ON a.`Learner_License_No` = b.`Learner_License_No` "
														+ "  LEFT JOIN t_personal_dtls c "
														+ "    ON b.`Customer_Id` = c.`Customer_Id` "
														+ "WHERE a.`Learner_License_No` = ?";
	
	private static final String GET_TEST_LOCATION_FOR_ETEST = "SELECT a.Test_Location_Id,a.`Juris_Type_Id` FROM t_etest_application a WHERE a.`Learner_License_No`=?  AND a.`Test_Date`=?";
	
	private static final String GET_TEST_DURATION = "SELECT `Test_Duration`,Max_Question FROM `t_etest_config_master` " +
												" WHERE `Test_Location_Id`=? AND `juris_type_id`=? AND `Test_Date`=?";
	
	private static final String GET_FIRST_QUESTION_ID = "SELECT MIN(a.`Question_Id`) questionId FROM t_etest_question_master a ";
	
	private static final String GET_LAST_QUESTION_ID = "SELECT MAX(a.`Question_Id`) questionId FROM t_etest_question_master a ";
	
	private static final String GET_QUESTION_ANSWER_MAPPING = "SELECT COUNT(*) rowCount FROM `t_etest_question_answer_mapping` a WHERE a.`Question_Id`=? AND a.`Option_Id`=?";
	
	private static final String GET_TEST_RESULT = "SELECT DATE_FORMAT(b.`Test_Date`, '%d-%m-%Y') testDate," +
													"IF(b.`Juris_Type_Id` = '1'," +
													"(SELECT region_name FROM t_region_master WHERE region_id = b.`Test_Location_Id`)," +
													"(SELECT base_office_name FROM t_base_office_master WHERE `Base_Office_Id` = b.`Test_Location_Id`)) testLocation " +
													" FROM t_etest_application a " +
													" LEFT JOIN t_etest_config_master b   " +
													" ON a.`Test_Location_Id` = b.`Test_Location_Id` AND a.`Juris_Type_Id`=b.`Juris_Type_Id`" +
													" AND a.`Test_Date`=b.`Test_Date` " +
													" WHERE a.`Application_Number` = ? ";
	
	private static final String GET_PASS_PERCENT_FOR_THEORY = "SELECT "
		+ "  b.`Theory_Full_Point`, "
		+ "  b.`Theory_Pass_Point` "
		+ "FROM "
		+ "  t_etest_application a "
		+ "  LEFT JOIN t_etest_config_master b "
		+ "    ON a.`Test_Location_Id` = b.`Sl_No` "
		+ "WHERE a.`Application_Number` = ?";
	
	private static final String UPDATE_TEST_RESULT = "UPDATE t_etest_application a SET a.`Theory_Marks_Obtained`=?, a.`Theory_Test_Status`=?, a.`Is_etest`='Y' WHERE a.`Application_Number`=?";
	
	private static final String CHECK_IF_LEARNER_ALREADY_PASSED = "SELECT "
																	+ "  a.Application_Number, "
																	+ "  a.Learner_License_No, "
																	+ "  a.DOB, "
																	+ "  a.Test_Location_Id, "
																	+ "  DATE_FORMAT(a.Test_Date, '%Y-%m-%d') testDate, "
																	+ "  (IF((SELECT b.Theory_Test_Status FROM `t_etest_application` b WHERE" +
																			" b.`Learner_License_No` = a.Learner_License_No AND b.`Theory_Test_Status` = 'P')='P','P','N'))" +
																			" Prev_Theory_Test_Status,a.`Theory_Test_Status` AS todaysTheoryStatus," +
																			"a.Is_License_Issued "
																	+ " FROM "
																	+ "  `t_etest_application` a"
																	+ " WHERE a.Learner_License_No = ? "
																	+ "  AND a.Is_License_Issued = 'N' AND a.`Test_Type`='NEW' "
																	+ " ORDER BY a.Test_Date DESC "
																	+ " LIMIT 1";
	
	private static final String CHECK_IF_QUESTION_EXISTS = "SELECT COUNT(*) rowCount FROM `t_etest_question_master` a WHERE a.`Question_Id`=?";
	
	private static final String GET_PRACTICAL_CRITERIA_LIST = "SELECT Practical_Criteria_Id, Criteria_Name, Full_Point FROM `t_practical_test_criteria_master`";
	
	private static final String UPDATE_T_ETEST_APPLICATION = "UPDATE "
		+ "  `t_etest_application` a "
		+ "SET "
		+ "  a.`Theory_Marks_Obtained` = ?, "
		+ "  a.`Theory_Test_Status` = ?, "
		+ "  a.`Practical_Marks_Obtained` = ?, "
		+ "  a.`Practical_Test_Status` = ?, "
		+ "  a.`Overall_Marks_Obtained` = ?, "
		+ "  a.`Is_License_Issued` = ?, "
		+ "  a.`Updated_By` = ?, "
		+ "  a.`Updated_On` = ? "
		+ "WHERE a.`Application_Number` = ?";
	
	private static final String INSERT_INTO_ETEST_APPLICATION	=	"INSERT INTO `t_etest_application` ( "
																	+ "  `Learner_License_No`, "
																	+ "  `Customer_Id`, "
																	+ "  `DOB` "
																	+ ") "
																	+ "SELECT "
																	+ "  `CID_Number`, "
																	+ "  `Customer_Id`, "
																	+ "  `Date_Of_Birth` "
																	+ "FROM "
																	+ "  `t_personal_dtls` WHERE CID_Number=?";
	
	private static final String UPDATE_T_ETEST_APPLICATION_AGAINST_CID	=	"UPDATE "
																			+ "  `t_etest_application` a "
																			+ "SET "
																			+ "  a.`Theory_Marks_Obtained` = ?, "
																			+ "  a.`Theory_Test_Status` = ?, "
																			+ "  a.`Practical_Marks_Obtained` = ?, "
																			+ "  a.`Practical_Test_Status` = ?, "
																			+ "  a.`Overall_Marks_Obtained` = ?, "
																			+ "  a.`Is_License_Issued` = ?, "
																			+ "  a.`Updated_By` = ?, "
																			+ "  a.`Updated_On` = ?, "
																			+ "  a.`Drive_Type_Id`=?,a.`Created_By`=?,a.`Created_On`=?,a.`Test_Type`=? "
																			+ "WHERE a.`Application_Number` = ? ;";
	
	private static final String UPDATE_ETEST_QUESTION_IMAGE = "UPDATE `t_etest_question_master` "
																+ "SET "
																+ "  `Image_Path` = ?, "
																+ "  `Image_Name` = ? "
																+ "WHERE `Question_Id` = ?";
	
	
	private static final String CANCEL_ETEST_BOOKING = "DELETE FROM t_etest_application WHERE Application_Number=?";
	
	
}




