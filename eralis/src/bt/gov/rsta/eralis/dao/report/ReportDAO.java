package bt.gov.rsta.eralis.dao.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.dto.common.DailyTaskUserDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.report.ReportDTO;
import bt.gov.rsta.eralis.dto.report.ReportParameterDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class ReportDAO 
{
	private static ReportDAO reportDAO = null;
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public static ReportDAO getInstance()
	{
		if(reportDAO == null)
			reportDAO = new ReportDAO();
		
		return reportDAO;
	}
	
	public String getReportName(String reportId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String reportName = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPORT_NAME);
				pst.setString(1, reportId);
				rs = pst.executeQuery();
				rs.first();
				
				reportName = rs.getString("page_name");
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return reportName;
	}
	
	private int getRows(ResultSet res){
	    int totalRows = 0;
	    try {
	        res.last();
	        totalRows = res.getRow();
	        res.beforeFirst();
	    } 
	    catch(Exception ex)  {
	        return 0;
	    }
	    return totalRows ;    
	}

	
	
	
	public String[][] get_report_details(String reportId, ReportDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ReportDTO> reportList = new ArrayList<ReportDTO>();
		String arrayList[][]=null;
		arrayList = new String[0][0];
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPORT_QUERY_FROM_REPORT_ID);
				pst.setString(1, reportId);
				rs = pst.executeQuery();
				rs.first();
				
				String query =	rs.getString("Report_Query");
				String whereParam =	rs.getString("Report_where_Param");
				String[] getIndividualParam	=	whereParam.split("~");
				pst = conn.prepareStatement(query);
				String[] paramVal	=	dto.getParamVal().split("#");
				int l	=	1;
				int m	=	0;
				
				
				for(int k=1; k<paramVal.length;k++)
				{
					String[] paramDataType	=	getIndividualParam[m].split(" ");
					String[] getDataType	=	paramDataType[0].split("\\.");
					String dataType			=	getDataType[2];
					String[] betweenParam	=	paramVal[k].split("&");
					String[] noBetween	=	paramVal[k].split("\\^");
					
					if(dataType.equalsIgnoreCase("DATE"))
					{
						java.util.Date sqlDat1 = sourceSdf.parse(betweenParam[0]);
						String dateVal = targetSdf.format(sqlDat1);
						
						pst.setString(l, dateVal);
						if(!noBetween[1].equalsIgnoreCase("NoBeTwEeN"))
						{
							l++;
							noBetween	=	betweenParam[1].split("\\^");
							java.util.Date sqlDat2 = sourceSdf.parse(noBetween[0]);
							String dateVal1 = targetSdf.format(sqlDat2);
							pst.setString(l, dateVal1);
						}
					}
					else
					{
						pst.setString(l, noBetween[0]);
						if(!noBetween[1].equalsIgnoreCase("NoBeTwEeN"))
						{
							l++;
							noBetween	=	noBetween[0].split("\\^");
							pst.setString(l, betweenParam[1]);
						}
					}
					l++;
					m++;
				}
				rs = pst.executeQuery();
				
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				String columnArray[] = new String[columnCount];
				// The column count starts from 1
				int arrayCol	=	0;
				for (int i = 1; i <= columnCount; i++ )
				{
					columnArray[arrayCol]=rsmd.getColumnLabel(i);
					rsmd.getColumnTypeName(i);
					arrayCol++;
				}
				
				
				//pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				int a=0;
				int totalRow=getRows(rs);
				dto.setTotalCol(columnArray.length);
				dto.setTotalRow(totalRow);
				arrayList = new String[totalRow][columnArray.length];
				while(rs.next())
				{
					 for(int i=0;i<columnArray.length;i++)
					 {
						 arrayList[a][i]=rs.getString(columnArray[i]);
					 }
					 a++;
				}
				//dto.setColumnArray(arrayList);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return arrayList;
	}
	
	public List<ReportDTO> get_report_header(String reportId,ReportDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ReportDTO> reportList = new ArrayList<ReportDTO>();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPORT_QUERY_FROM_REPORT_ID);
				pst.setString(1, reportId);
				rs = pst.executeQuery();
				rs.first();
				String query =	rs.getString("Report_Query");
				String whereParam =	rs.getString("Report_where_Param");
				String[] getIndividualParam	=	whereParam.split("~");
				
				pst = conn.prepareStatement(query);
				String[] paramVal	=	dto.getParamVal().split("#");
				int l	=	1;
				int m	=	0;
				for(int k=1; k<paramVal.length;k++)
				{

					String[] paramDataType	=	getIndividualParam[m].split(" ");
					String[] getDataType	=	paramDataType[0].split("\\.");
					String dataType			=	getDataType[2];
					//System.out.println(dataType);
					String[] betweenParam	=	paramVal[k].split("&");
					//System.out.println(paramVal[k]);
					String[] noBetween	=	paramVal[k].split("\\^");
					
					if(dataType.equalsIgnoreCase("DATE"))
					{
						java.util.Date sqlDat1 = sourceSdf.parse(betweenParam[0]);
						String dateVal = targetSdf.format(sqlDat1);
						
						pst.setString(l, dateVal);
						if(!noBetween[1].equalsIgnoreCase("NoBeTwEeN"))
						{
							l++;
							noBetween	=	betweenParam[1].split("\\^");
							java.util.Date sqlDat2 = sourceSdf.parse(noBetween[0]);
							String dateVal1 = targetSdf.format(sqlDat2);
							pst.setString(l, dateVal1);
						}
					}
					else
					{
						pst.setString(l, noBetween[0]);
						if(!noBetween[1].equalsIgnoreCase("NoBeTwEeN"))
						{
							l++;
							noBetween	=	noBetween[0].split("\\^");
							pst.setString(l, betweenParam[1]);
						}
					}
					l++;
					m++;
				}
				rs = pst.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				// The column count starts from 1
				for (int i = 1; i <= columnCount; i++ ) 
				{
					dto = new ReportDTO();
					//dto.setHeaderCol(rsmd.getColumnName(i));
					dto.setHeaderCol(rsmd.getColumnLabel(i));
					reportList.add(dto);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return reportList;
	}
	
	public List<EralisCommonDTO> getStatisticDtlReport(String reportType,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		EralisCommonDTO dto;
		List<EralisCommonDTO> getStatisticDtlReport = new ArrayList<EralisCommonDTO>();
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				String query = null;
				if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED"))
					query = GET_TOTAL_VEHICLE_REGISTERED_DTLS;
				if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED"))
					query = GET_TOTAL_VEHICLE_RENEWAL_DTLS;
				if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_ISSUED"))
					query = GET_TOTAL_LIC_ISSUE_DTLS;
				if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_RENEWED"))
					query = GET_TOTAL_LIC_RENEWED_DTLS;
				if(reportType.equalsIgnoreCase("STATS_TOTAL_LL_ISSUED"))
					query = GET_TOTAL_LL_ISSUED_DTLS;
				if(reportType.equalsIgnoreCase("STATS_TOTAL_DL_PRINTED"))
					query = GET_TOTAL_DL_PRINT_DTLS;
				
				pst = conn.prepareStatement(query);
				pst.setString(1, jurisTypeId);
				pst.setString(2, jurisId);
				pst.setString(3, jurisId);
				if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED") || reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED"))
				{
					pst.setString(4, jurisTypeId);
					pst.setString(5, jurisId);
					pst.setString(6, jurisId);
				}
				rs = pst.executeQuery();
				while (rs.next())
				{
					dto = new EralisCommonDTO();
					if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED"))
					{
						dto.setCID(rs.getString("CID_Number"));
						dto.setName(rs.getString("Customer_Name"));
						dto.setPhone(rs.getString("Present_Phone_No"));
						dto.setAddress(rs.getString("Present_Contact_Address"));
						dto.setVehicleNo(rs.getString("Vehicle_Number"));
						dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
						dto.setModel(rs.getString("Vehicle_Model_Name"));
						dto.setCompany(rs.getString("Vehicle_Company_Name"));
						dto.setEngineCC(rs.getString("Engine_CC"));
						dto.setEngineNumber(rs.getString("Engine_Number"));
						dto.setChasisNumber(rs.getString("Chassis_Number"));
						dto.setEngineType(rs.getString("Engine_Name"));
						dto.setCountry(rs.getString("Country_Name"));
						dto.setDealerName(rs.getString("Dealer_Name"));
						dto.setPrice(rs.getString("Price"));
						dto.setColor(rs.getString("Colour"));
						dto.setType(rs.getString("Description"));
						dto.setRegistrationDate(rs.getString("Registration_Date"));
						dto.setRegion(rs.getString("region_name"));
						dto.setBaseoffice(rs.getString("base_office_name"));
						dto.setRemarks(rs.getString("Remarks"));
					}
					else if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED"))
					{
						dto.setCID(rs.getString("CID_Number"));
						dto.setName(rs.getString("Customer_Name"));
						dto.setPhone(rs.getString("Present_Phone_No"));
						dto.setAddress(rs.getString("Address"));
						dto.setVehicleNo(rs.getString("Vehicle_Number"));
						dto.setVehicleType(rs.getString("Vehicle_Type_Name"));
						dto.setModel(rs.getString("Vehicle_Model_Name"));
						dto.setCompany(rs.getString("Vehicle_Company_Name"));
						dto.setEngineCC(rs.getString("Engine_CC"));
						dto.setEngineNumber(rs.getString("Engine_Number"));
						dto.setRenewalDate(rs.getString("Renewal_Date"));
						dto.setExpiryDate(rs.getString("Expiry_Date"));
						dto.setReceiptNo(rs.getString("Receipt_No"));
						dto.setReceiptDate(rs.getString("Receipt_Date"));
						dto.setTotalDuration(rs.getString("Renewal_Duration"));
						dto.setBaseoffice(rs.getString("base_office_name"));
						dto.setRegion(rs.getString("region_name"));
						dto.setRemarks(rs.getString("Remarks"));
					}
					else if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_ISSUED"))
					{
						dto.setCID(rs.getString("CID_Number"));
						dto.setName(rs.getString("customerName"));
						dto.setDOB(rs.getString("dob"));
						dto.setLearnerLicenseNo(rs.getString("Learner_License_No"));
						dto.setLicenseNo(rs.getString("Driving_License_No"));
						dto.setDrivetype(rs.getString("driveTypes"));
						dto.setDateOfissue(rs.getString("issueDate"));
						dto.setExpiryDate(rs.getString("expiryDate"));
						dto.setBaseoffice(rs.getString("baseOffice"));
						dto.setRegion(rs.getString("region"));
					}
					else if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_RENEWED"))
					{
						dto.setCID(rs.getString("CID_Number"));
						dto.setName(rs.getString("customerName"));
						dto.setLicenseNo(rs.getString("Driving_License_No"));
						dto.setRenewalDate(rs.getString("Renewal_Date"));
						dto.setExpiryDate(rs.getString("Expiry_Date"));
						dto.setReceiptNo(rs.getString("Receipt_No"));
						dto.setReceiptDate(rs.getString("Receipt_Date"));
						dto.setBaseoffice(rs.getString("base_office_name"));
						dto.setRegion(rs.getString("region_name"));
						dto.setRemarks(rs.getString("Remarks"));
					}
					else if(reportType.equalsIgnoreCase("STATS_TOTAL_LL_ISSUED"))
					{
						dto.setCID(rs.getString("CID_Number"));
						dto.setName(rs.getString("Customer_Name"));
						dto.setLearnerLicenseNo(rs.getString("Learner_License_No")); 
						dto.setDateOfissue(rs.getString("Issue_Date")); 
						dto.setExpiryDate(rs.getString("Expiry_Date")); 
						dto.setReceiptNo(rs.getString("Receipt_No")); 
						dto.setReceiptDate(rs.getString("Receipt_Date")); 
						dto.setBaseoffice(rs.getString("base_office_name")); 
						dto.setRegion(rs.getString("region_name")); 
						dto.setRemarks(rs.getString("Remarks")); 
					}
					else if(reportType.equalsIgnoreCase("STATS_TOTAL_DL_PRINTED"))
					{
						dto.setCID(rs.getString("CID_Number"));
						dto.setName(rs.getString("customerName"));
						dto.setServiceType(rs.getString("Service_Name")); 
						dto.setLicenseNo(rs.getString("Identity_Number")); 
					}
					
					getStatisticDtlReport.add(dto);
				}
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return getStatisticDtlReport;
	}
	
	public ReportDTO getReportParam(String reportId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null; 
		ResultSet rs = null;
		ReportDTO dto = new ReportDTO();
		try
		{
			conn = ConnectionManager.getConnection();
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPORT_QUERY_FROM_REPORT_ID);
				pst.setString(1, reportId);
				rs = pst.executeQuery();
				rs.first();
				dto.setDescription(rs.getString("Report_Description"));
				dto.setWhereParam(rs.getString("Report_where_Param"));
				
				
				/*String query =	rs.getString("Report_Query");
				dto.setDescription(rs.getString("Report_Description"));
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				dto.setTotalCol(rsmd.getColumnCount());*/
			}
		}
		catch (Exception e) 
		{e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	//--------------------------------------------------- QUERIES ---------------------------------------------------------------------
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_REGION_WISE = "SELECT "
																		+ "  (SELECT "
																		+ "    region_name "
																		+ "  FROM "
																		+ "    t_region_master "
																		+ "  WHERE region_id = a.`Processed_Region_Id`) region, "
																		+ "  COUNT(*) rcount "
																		+ "FROM "
																		+ "  t_vehicle_registration_dtls a "
																		+ "WHERE IF( "
																		+ "    'ALL' = ?, "
																		+ "    1 = 1, "
																		+ "    a.`Processed_Region_Id` = ? "
																		+ "  ) "
																		+ "  AND a.`Created_On` BETWEEN ? "
																		+ "  AND ? "
																		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
																		+ "  (SELECT "
																		+ "    Vehicle_Id "
																		+ "  FROM "
																		+ "    t_vehicle_cancellation_dtls) "
																		+ "GROUP BY a.`Processed_Region_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_BASE_WISE = "SELECT "
																			+ "  (SELECT "
																			+ "    `base_office_name` "
																			+ "  FROM "
																			+ "    `t_base_office_master` "
																			+ "  WHERE `base_office_id` = a.`Processed_Base_Id`) base, "
																			+ "  COUNT(*) rcount "
																			+ "FROM "
																			+ "  t_vehicle_registration_dtls a "
																			+ "WHERE IF( "
																			+ "    'ALL' = ?, "
																			+ "    1 = 1, "
																			+ "    a.`Processed_Base_Id` = ? "
																			+ "  ) "
																			+ "  AND a.`Processed_Base_Id` IS NOT NULL "
																			+ "  AND a.`Created_On` BETWEEN ? "
																			+ "  AND ? "
																			+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
																			+ "  (SELECT "
																			+ "    Vehicle_Id "
																			+ "  FROM "
																			+ "    t_vehicle_cancellation_dtls) "
																			+ "GROUP BY a.`Processed_Base_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_COMPANY_WISE = "SELECT "
		+ "  (SELECT "
		+ "    `Vehicle_Company_Name` "
		+ "  FROM "
		+ "    `t_vehicle_company_master` "
		+ "  WHERE `Vehicle_Company_Id` = a.`Vehicle_Company_Id`) company, "
		+ "  COUNT(*) rcount "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE IF( "
		+ "    'ALL' = ?, "
		+ "    1 = 1, "
		+ "    a.`Vehicle_Company_Id` = ? "
		+ "  ) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ? "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "GROUP BY a.`Vehicle_Company_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_MODEL_WISE = "SELECT "
		+ "  (SELECT "
		+ "    `Vehicle_Model_Name` "
		+ "  FROM "
		+ "    `t_vehicle_model_master` "
		+ "  WHERE `Vehicle_Model_Id` = a.`Vehicle_Model_Id`) model, "
		+ "  COUNT(*) rcount "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE IF( "
		+ "    'ALL' = ?, "
		+ "    1 = 1, "
		+ "    a.`Vehicle_Model_Id` = ? "
		+ "  ) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ? "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "GROUP BY a.`Vehicle_Model_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_OWNER_TYPE_WISE = "SELECT "
		+ "  IF( "
		+ "    a.Vehicle_Registration_Type = 'O', "
		+ "    (SELECT "
		+ "      `Owner_Name` "
		+ "    FROM "
		+ "      `t_owner_master` "
		+ "    WHERE `Owner_Type_Id` = b.`Organization_Type_Id`), "
		+ "    'Government/Individual/Company/NGO/Corporation' "
		+ "  ) ownerType, "
		+ "  COUNT(*) rcount "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "  LEFT JOIN t_organization_info b "
		+ "    ON a.`Customer_Id` = b.`Customer_Id` "
		+ "WHERE IF( "
		+ "    'ALL' = ?, "
		+ "    1 = 1, "
		+ "    b.`Organization_Type_Id` = ? "
		+ "  ) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ? "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "GROUP BY b.`Organization_Type_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_VEHICLE_TYPE_WISE = "SELECT "
		+ "  (SELECT "
		+ "    `Vehicle_Type_Name` "
		+ "  FROM "
		+ "    `t_vehicle_type_master` "
		+ "  WHERE `Vehicle_Type_Id` = a.`Vehicle_Type_Id`) vehicleType, "
		+ "  COUNT(*) rcount "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE IF( "
		+ "    'ALL' = ?, "
		+ "    1 = 1, "
		+ "    a.`Vehicle_Type_Id` = ? "
		+ "  ) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ? "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "GROUP BY a.`Vehicle_Type_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_ENGINE_TYPE_WISE = "SELECT "
		+ "  (SELECT "
		+ "    `Engine_Name` "
		+ "  FROM "
		+ "    `t_engine_type_master` "
		+ "  WHERE `Engine_Type_Id` = a.`Engine_Type_Id`) engineType, "
		+ "  COUNT(*) rcount "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE IF( "
		+ "    'ALL' = ?, "
		+ "    1 = 1, "
		+ "    a.`Engine_Type_Id` = ? "
		+ "  ) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ? "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "GROUP BY a.`Engine_Type_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_DEALER_WISE = "SELECT "
		+ "  (SELECT "
		+ "    `Dealer_Name` "
		+ "  FROM "
		+ "    `t_dealer_master` "
		+ "  WHERE `Dealer_Id` = a.`Dealer_Id`) dealer, "
		+ "  COUNT(*) rcount "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE IF( "
		+ "    'ALL' = ?, "
		+ "    1 = 1, "
		+ "    a.`Dealer_Id` = ? "
		+ "  ) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ? "
		+ "  AND a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "GROUP BY a.`Dealer_Id`";
	
	private static final String GET_VEHICLE_REGISTRATION_STATISTICS_DETAILED = "SELECT "
		+ "  a.`Vehicle_Number`, "
		+ "  (SELECT "
		+ "    `Vehicle_Type_Name` "
		+ "  FROM "
		+ "    `t_vehicle_type_master` "
		+ "  WHERE `Vehicle_Type_Id` = a.`Vehicle_Type_Id`) vehicleType, "
		+ "  (SELECT "
		+ "    `Vehicle_Company_Name` "
		+ "  FROM "
		+ "    `t_vehicle_company_master` "
		+ "  WHERE `Vehicle_Company_Id` = a.`Vehicle_Company_Id`) vehicleCompany, "
		+ "  DATE_FORMAT( "
		+ "    a.`Registration_Date`, "
		+ "    '%d/%m/%Y' "
		+ "  ) registrationDate, "
		+ "  (SELECT "
		+ "    `Vehicle_Model_Name` "
		+ "  FROM "
		+ "    `t_vehicle_model_master` "
		+ "  WHERE `Vehicle_Model_Id` = a.`Vehicle_Model_Id`) vehicleModel, "
		+ "  (SELECT "
		+ "    `Engine_Name` "
		+ "  FROM "
		+ "    `t_engine_type_master` "
		+ "  WHERE `Engine_Type_Id` = a.`Engine_Type_Id`) engineType, "
		+ "  a.`Engine_Number`, "
		+ "  a.`Chassis_Number`, "
		+ "  a.`Colour`, "
		+ "  a.`Price`, "
		+ "  a.`Load_Capacity`, "
		+ "  (SELECT "
		+ "    `Dealer_Name` "
		+ "  FROM "
		+ "    `t_dealer_master` "
		+ "  WHERE `Dealer_Id` = a.`Dealer_Id`) dealer, "
		+ "  a.`Seat_Capacity`, "
		+ "  a.`Vehicle_Horse_Power`, "
		+ "  a.`Vehicle_Kilowatt`, "
		+ "  a.`Unladen_Weight`, "
		+ "  a.`Manufacture_Year`, "
		+ "  a.`Engine_CC`, "
		+ "  (SELECT "
		+ "    country_name "
		+ "  FROM "
		+ "    `t_country_master` "
		+ "  WHERE `Country_Id` = a.`Manufacture_Country_Id`) manufactureCountry "
		+ "FROM "
		+ "  t_vehicle_registration_dtls a "
		+ "WHERE a.`Vehicle_Reg_Dtls_Id` NOT IN "
		+ "  (SELECT "
		+ "    Vehicle_Id "
		+ "  FROM "
		+ "    t_vehicle_cancellation_dtls) "
		+ "  AND a.`Created_On` BETWEEN ? "
		+ "  AND ?";
	
	private static final String GET_REPORT_NAME = "SELECT page_name FROM t_page_master WHERE page_id=?";
	
	private static final String GET_NEW_VEHICLE_REGISTRATION_REPORT = "SELECT DATE_FORMAT(a.`Registration_Date`,'%d/%m/%Y') AS Registration_Date,j.`Vehicle_Company_Name`,a.`Manufacture_Year`,d.`Vehicle_Model_Name`,e.`Vehicle_Type_Name`,f.`Engine_Name`,"+
																		" a.`Vehicle_Registration_Type`,h.`Dealer_Name`,i.`region_name`,"+
																		" CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) customerName, b.`Present_Contact_Address`,a.`Vehicle_Number`,"+
																		" a.`Engine_CC`,a.`Price`,a.`Status` " +
																		"FROM `t_vehicle_renewal_dtls` z" +
																		" LEFT JOIN `t_vehicle_registration_dtls` a ON a.`Vehicle_Reg_Dtls_Id`=z.`Vehicle_Id` " +
																		"LEFT JOIN `t_vehicle_model_master` d ON d.`Vehicle_Model_Id`=a.`Vehicle_Model_Id` " +
																		"LEFT JOIN `t_vehicle_type_master` e ON e.`Vehicle_Type_Id`=a.`Vehicle_Type_Id` " +
																		"LEFT JOIN `t_engine_type_master` f ON f.`Engine_Type_Id`=a.`Engine_Type_Id` " +
																		"LEFT JOIN `t_vehicle_cancellation_dtls` g ON g.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` " +
																		"LEFT JOIN `t_dealer_master` h ON h.`Dealer_Id` = a.`Dealer_Id` " +
																		"LEFT JOIN `t_region_master` i ON i.`region_id`=a.`Region_Id` " +
																		"LEFT JOIN `t_vehicle_company_master` j ON j.`Vehicle_Company_Id`=a.`Vehicle_Company_Id` " +
																		" LEFT JOIN t_vehicle_transfer_dtls k ON k.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` "+
																		" LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id`  OR a.`Customer_Id`=k.`Transferee_Customer_Id` "+
																		"WHERE " +
																		" a.`Vehicle_Registration_Id`>0 " ;
																		
	private static final String GET_VEHICLE_RENEWAL_DTLS	=	"SELECT DATE_FORMAT(z.`Renewal_Date`,'%d/%m/%Y') AS Renewal_Date,j.`Vehicle_Company_Name`,a.`Manufacture_Year`,d.`Vehicle_Model_Name`,e.`Vehicle_Type_Name`,f.`Engine_Name`,"+
														" a.`Vehicle_Registration_Type`,h.`Dealer_Name`,i.`region_name`,"+
														" CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) customerName, b.`Present_Contact_Address`,a.`Vehicle_Number`,"+
														" a.`Engine_CC`,a.`Price`,a.`Status` " +
														"FROM `t_vehicle_renewal_dtls` z" +
														" LEFT JOIN `t_vehicle_registration_dtls` a ON a.`Vehicle_Reg_Dtls_Id`=z.`Vehicle_Id` " +
														"LEFT JOIN `t_vehicle_model_master` d ON d.`Vehicle_Model_Id`=a.`Vehicle_Model_Id` " +
														"LEFT JOIN `t_vehicle_type_master` e ON e.`Vehicle_Type_Id`=a.`Vehicle_Type_Id` " +
														"LEFT JOIN `t_engine_type_master` f ON f.`Engine_Type_Id`=a.`Engine_Type_Id` " +
														"LEFT JOIN `t_vehicle_cancellation_dtls` g ON g.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` " +
														"LEFT JOIN `t_dealer_master` h ON h.`Dealer_Id` = a.`Dealer_Id` " +
														"LEFT JOIN `t_region_master` i ON i.`region_id`=a.`Region_Id` " +
														"LEFT JOIN `t_vehicle_company_master` j ON j.`Vehicle_Company_Id`=a.`Vehicle_Company_Id` " +
														" LEFT JOIN t_vehicle_transfer_dtls k ON k.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` "+
														" LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id`  OR a.`Customer_Id`=k.`Transferee_Customer_Id` "+
														"WHERE " +
														" a.`Vehicle_Registration_Id`>0 " ;
														
	private static final String GET_VEHICLE_DUPLICATION_DTLS	=	"SELECT DATE_FORMAT(z.`Duplicate_Issue_Date`,'%d/%m/%Y') AS Duplicate_Issue_Date,j.`Vehicle_Company_Name`,a.`Manufacture_Year`,d.`Vehicle_Model_Name`,e.`Vehicle_Type_Name`,f.`Engine_Name`,"+
															" a.`Vehicle_Registration_Type`,h.`Dealer_Name`,i.`region_name`,"+
															" CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) customerName, b.`Present_Contact_Address`,a.`Vehicle_Number`,"+
															" a.`Engine_CC`,a.`Price`,a.`Status` " +
															"FROM `t_vehicle_duplicate_dtls` z" +
															" LEFT JOIN `t_vehicle_registration_dtls` a ON a.`Vehicle_Reg_Dtls_Id`=z.`Vehicle_Id` " +
															"LEFT JOIN `t_vehicle_model_master` d ON d.`Vehicle_Model_Id`=a.`Vehicle_Model_Id` " +
															"LEFT JOIN `t_vehicle_type_master` e ON e.`Vehicle_Type_Id`=a.`Vehicle_Type_Id` " +
															"LEFT JOIN `t_engine_type_master` f ON f.`Engine_Type_Id`=a.`Engine_Type_Id` " +
															"LEFT JOIN `t_vehicle_cancellation_dtls` g ON g.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` " +
															"LEFT JOIN `t_dealer_master` h ON h.`Dealer_Id` = a.`Dealer_Id` " +
															"LEFT JOIN `t_region_master` i ON i.`region_id`=a.`Region_Id` " +
															"LEFT JOIN `t_vehicle_company_master` j ON j.`Vehicle_Company_Id`=a.`Vehicle_Company_Id` " +
															" LEFT JOIN t_vehicle_transfer_dtls k ON k.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` "+
															" LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id`  OR a.`Customer_Id`=k.`Transferee_Customer_Id` "+
															"WHERE " +
															" a.`Vehicle_Registration_Id`>0 " ;
	private static final String GET_VEHICLE_TRANSFER_DTLS	=	"SELECT DATE_FORMAT(z.`Transfer_Date`,'%d/%m/%Y') AS Transfer_Date,j.`Vehicle_Company_Name`,a.`Manufacture_Year`,d.`Vehicle_Model_Name`,e.`Vehicle_Type_Name`,f.`Engine_Name`,"+
														" a.`Vehicle_Registration_Type`,h.`Dealer_Name`,i.`region_name`,"+
														" CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) customerName, b.`Present_Contact_Address`,a.`Vehicle_Number`,"+
														" a.`Engine_CC`,a.`Price`,a.`Status` " +
														"FROM `t_vehicle_transfer_dtls` z" +
														" LEFT JOIN `t_vehicle_registration_dtls` a ON a.`Vehicle_Reg_Dtls_Id`=z.`Vehicle_Id` " +
														"LEFT JOIN `t_vehicle_model_master` d ON d.`Vehicle_Model_Id`=a.`Vehicle_Model_Id` " +
														"LEFT JOIN `t_vehicle_type_master` e ON e.`Vehicle_Type_Id`=a.`Vehicle_Type_Id` " +
														"LEFT JOIN `t_engine_type_master` f ON f.`Engine_Type_Id`=a.`Engine_Type_Id` " +
														"LEFT JOIN `t_vehicle_cancellation_dtls` g ON g.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` " +
														"LEFT JOIN `t_dealer_master` h ON h.`Dealer_Id` = a.`Dealer_Id` " +
														"LEFT JOIN `t_region_master` i ON i.`region_id`=a.`Region_Id` " +
														"LEFT JOIN `t_vehicle_company_master` j ON j.`Vehicle_Company_Id`=a.`Vehicle_Company_Id` " +
														" LEFT JOIN t_vehicle_transfer_dtls k ON k.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` "+
														" LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id`  OR a.`Customer_Id`=k.`Transferee_Customer_Id` "+
														"WHERE " +
														" a.`Vehicle_Registration_Id`>0 " ;
	private static final String GET_VEHICLE_CANCELLATION_DTLS	=	"SELECT DATE_FORMAT(z.`Cancellation_Date`,'%d/%m/%Y') AS Cancellation_Date,j.`Vehicle_Company_Name`,a.`Manufacture_Year`,d.`Vehicle_Model_Name`,e.`Vehicle_Type_Name`,f.`Engine_Name`,"+
															" a.`Vehicle_Registration_Type`,h.`Dealer_Name`,i.`region_name`,"+
															" CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) customerName, b.`Present_Contact_Address`,a.`Vehicle_Number`,"+
															" a.`Engine_CC`,a.`Price`,a.`Status` " +
															"FROM `t_vehicle_cancellation_dtls` z" +
															" LEFT JOIN `t_vehicle_registration_dtls` a ON a.`Vehicle_Reg_Dtls_Id`=z.`Vehicle_Id` " +
															"LEFT JOIN `t_vehicle_model_master` d ON d.`Vehicle_Model_Id`=a.`Vehicle_Model_Id` " +
															"LEFT JOIN `t_vehicle_type_master` e ON e.`Vehicle_Type_Id`=a.`Vehicle_Type_Id` " +
															"LEFT JOIN `t_engine_type_master` f ON f.`Engine_Type_Id`=a.`Engine_Type_Id` " +
															"LEFT JOIN `t_vehicle_cancellation_dtls` g ON g.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` " +
															"LEFT JOIN `t_dealer_master` h ON h.`Dealer_Id` = a.`Dealer_Id` " +
															"LEFT JOIN `t_region_master` i ON i.`region_id`=a.`Region_Id` " +
															"LEFT JOIN `t_vehicle_company_master` j ON j.`Vehicle_Company_Id`=a.`Vehicle_Company_Id` " +
															" LEFT JOIN t_vehicle_transfer_dtls k ON k.`Vehicle_Id`=a.`Vehicle_Reg_Dtls_Id` "+
															" LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id`  OR a.`Customer_Id`=k.`Transferee_Customer_Id` "+
															"WHERE " +
															" a.`Vehicle_Registration_Id`>0 " ;
	private static final String	GET_VEHICLE_DRIVING_LICENSE_DTLS ="select concat(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) as customerName," +
															" b.`Present_Contact_Address`,b.`Present_Phone_No` "+
															",b.`CID_Number`,a.`Status`,DATE_FORMAT(a.`Issue_Date`,'%d/%m/%Y') AS Issue_Date ,a.`Driving_License_No`" +
															",c.`region_name`"+
															 " from `t_driving_license_dtls` a"+
															" left join `t_personal_dtls` b on b.`Customer_Id`=a.`Customer_Id` "+
															" left join `t_region_master` c on c.`region_id`=a.`Region_Id` " +
															" WHERE a.`Driving_License_Id`>0 ";
	private static final String GET_DRIVING_LICENSE_RENEWAL_DTLS	=	"select concat(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) as customerName," +
																" b.`Present_Contact_Address`,b.`Present_Phone_No` "+
																",b.`CID_Number`,a.`Status`,DATE_FORMAT(d.`Renewal_Date`,'%d/%m/%Y') AS Renewal_Date ,a.`Driving_License_No`" +
																",c.`region_name`"+
																 " from `t_driving_license_renewal_dtls` d" +
																 " LEFT JOIN `t_driving_license_dtls` a ON a.`Driving_License_Id`=d.`License_Id`"+
																" left join `t_personal_dtls` b on b.`Customer_Id`=a.`Customer_Id` "+
																" left join `t_region_master` c on c.`region_id`=a.`Region_Id` " +
																" WHERE a.`Driving_License_Id`>0 ";
	private static final String GET_DRIVING_LICENSE_DUPLICATE_DTLS	=	"select concat(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) as customerName," +
																	" b.`Present_Contact_Address`,b.`Present_Phone_No` "+
																	",b.`CID_Number`,a.`Status`,DATE_FORMAT(d.`Duplication_Date`,'%d/%m/%Y') AS Duplication_Date ,a.`Driving_License_No`" +
																	",c.`region_name`"+
																	 " from `t_driving_license_duplicate` d" +
																	 " LEFT JOIN `t_driving_license_dtls` a ON a.`Driving_License_Id`=d.`License_Id`"+
																	" left join `t_personal_dtls` b on b.`Customer_Id`=a.`Customer_Id` "+
																	" left join `t_region_master` c on c.`region_id`=a.`Region_Id` " +
																	" WHERE a.`Driving_License_Id`>0 ";
	private static final String GET_DRIVING_LICENSE_ENDORSMENT_DTLS	=	"select concat(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) as customerName," +
																	" b.`Present_Contact_Address`,b.`Present_Phone_No` "+
																	",b.`CID_Number`,a.`Status`,DATE_FORMAT(d.`Endorsed_Date`,'%d/%m/%Y') AS Endorsed_Date ,a.`Driving_License_No`" +
																	",c.`region_name`"+
																	 " from `t_driving_license_endorsement` d" +
																	 " LEFT JOIN `t_driving_license_dtls` a ON a.`Driving_License_No`=d.`License_No`"+
																	" left join `t_personal_dtls` b on b.`Customer_Id`=a.`Customer_Id` "+
																	" left join `t_region_master` c on c.`region_id`=a.`Region_Id` " +
																	"	LEFT JOIN `t_drive_type_master` e ON e.`Drive_Type_Id`=d.`Drive_Type_Id`  " +
																	" WHERE a.`Driving_License_Id`>0 ";
	private static final String GET_DRIVING_LICENSE_CANCELLATION_DTLS	=	"select concat(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) as customerName," +
																	" b.`Present_Contact_Address`,b.`Present_Phone_No` "+
																	",b.`CID_Number`,a.`Status`,DATE_FORMAT(d.`Cancellation_Date`,'%d/%m/%Y') AS Cancellation_Date ,a.`Driving_License_No`" +
																	",c.`region_name`"+
																	 " from `t_driving_license_cancelled` d" +
																	 " LEFT JOIN `t_driving_license_dtls` a ON a.`Driving_License_No`=d.`License_No`"+
																	" left join `t_personal_dtls` b on b.`Customer_Id`=a.`Customer_Id` "+
																	" left join `t_region_master` c on c.`region_id`=a.`Region_Id` " +
																	"	LEFT JOIN `t_drive_type_master` e ON e.`Drive_Type_Id`=d.`Drive_Type_Id`  " +
																	" WHERE a.`Driving_License_Id`>0 ";
	private static final String GET_OFFENCE_DTLS	=	"SELECT CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) AS customerName, b.`Present_Contact_Address`,b.`Present_Phone_No` "
												+ ",b.`CID_Number`,a.`Status`,DATE_FORMAT(d.`Offence_Date`,'%d/%m/%Y') AS Offence_Date ,a.`Driving_License_No`,c.`region_name`, "
												+ "(SELECT e.`Offence_Name` FROM `t_offence_type_master` e WHERE e.`Offence_Type_Id`=d.`Offence_Type_Id1`) AS offence1, "
												+ "(SELECT e.`Offence_Name` FROM `t_offence_type_master` e WHERE e.`Offence_Type_Id`=d.`Offence_Type_Id2`) AS offence2, "
												+ "(SELECT e.`Offence_Name` FROM `t_offence_type_master` e WHERE e.`Offence_Type_Id`=d.`Offence_Type_Id3`) AS offence3, "
												+ "d.`Inspected_By`,d.`Time_Of_Inspection`,d.`Traffic_Branch`,d.`Place_Of_Inspection`,d.`Document_Seized` "
												+ ",d.`Remarks` "
												+ "FROM `t_offence_information` d "
												+ "LEFT JOIN `t_driving_license_dtls` a ON a.`Driving_License_No`=d.`License_Number` "
												+ "LEFT JOIN `t_personal_dtls` b ON b.`Customer_Id`=a.`Customer_Id` "
												+ "LEFT JOIN `t_region_master` c ON c.`region_id`=a.`Region_Id` "
												+ "WHERE  d.`Offence_License_Type`=?";
	private static final String GET_ROUTE_PERMIT	=	"SELECT DATE_FORMAT( a.`Validity`,'%d/%m/%Y') AS permitVlidity," +
												" a.*,b.`Vehicle_Type_Name`,c.`region_name`,d.`base_office_name`," +
												"DATE_FORMAT(a.`Issue_Date`,'%d/%m/%Y') AS permitIssue_Date" +
												" FROM `t_permit_dtls` a " +
												" LEFT JOIN `t_vehicle_type_master` b ON b.`Vehicle_Type_Id` = a.`Vehicle_Type_Id` " +
												" LEFT JOIN `t_region_master` c ON c.`region_id`=a.`Region_Id` " +
												" LEFT JOIN t_base_office_master d ON d.`base_office_id`=a.`Base_Office_Id`" +
												" WHERE a.`Permit_Details_Id`>0 ";
	private static final String GET_TOP_DTLS	=	"SELECT "
											+ "  CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) AS customerName, "
											+ "  a.`License_Number`, "
											+ "  b.`Present_Phone_No`,e.`Seat_Capacity`,a.`Vehicle_Number`,c.`region_name`,d.`dzongkhag_name` "
											+ "  ,a.`TOP_Number`,a.`Exact_Location`,DATE_FORMAT(a.`Issue_Date`,'%d/%m/%Y') AS Issue_Date, "
											+ "  DATE_FORMAT(a.`Expiry_Date`,'%d/%m/%Y') AS Expiry_Date,a.`Receipt_Number` "
											+ "FROM "
											+ "  `t_top_dtls` a "
											+ "  LEFT JOIN t_personal_dtls b "
											+ "    ON b.`Customer_Id` = a.`Customer_Id` "
											+ "  LEFT JOIN t_region_master c "
											+ "    ON c.`region_id` = a.`Region_Id` "
											+ "  LEFT JOIN t_dzongkhag_master d "
											+ "    ON d.`dzongkhag_id` = a.`Dzongkhag_Id` "
											+ "  LEFT JOIN `t_vehicle_registration_dtls` e "
											+ "    ON e.`Vehicle_Number` = a.`Vehicle_Number`";
private static final  String GET_ROAD_WORTHINESS_CERTIFICATE	="SELECT "
															 +" (SELECT CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) FROM t_personal_dtls b WHERE b.`Customer_Id`=a.`Customer_Id`) personalName,"
															 +" (SELECT c.`Organization_Name` FROM t_organization_info c WHERE c.`Customer_Id`=a.`Customer_Id` ) AS organisationName, "
															 +" DATE_FORMAT(a.Test_Date,'%d/%m/%Y') AS test_date,"
															 +" DATE_FORMAT(a.Validity,'%d/%m/%Y') AS validity,"
															 +" a.remarks," +
															 "a.Vehicle_Number"
															 +" FROM"
															 +" `t_vehicle_fitness_test_dtls` a WHERE a.Vehicle_Fitness_Test_Id>=0 ";
private static final  String GET_VEHICLE_EMISSION_TRANSFER	=	"SELECT "
													+ "  a.`Vehicle_Number`, "
													+ " CONCAT(c.`First_Name`,' ',c.`Middle_Name`,' ',c.`Last_Name`) AS personalName, "
													+ "  d.`Organization_Name` ,e.`dzongkhag_name`,DATE_FORMAT(a.`Tested_On`,'%d/%m/%Y') AS testDate, "
													+ "DATE_FORMAT(a.`Validity`,'%d/%m/%Y') AS validity,a.`CO`,a.`HSU`,a.`Test_Frequency`,a.`Test_Result`,a.`Emission_Test_Type`" 
													+ ",a.`Remarks` "
													+ "FROM "
													+ "  `t_vehicle_emission_test_dtls` a "
													+ "  LEFT JOIN t_vehicle_transfer_dtls b "
													+ "    ON b.`Vehicle_Number` = a.`Vehicle_Number` "
													+ "  LEFT JOIN `t_personal_dtls` c "
													+ "    ON c.`Customer_Id` = b.`Transferee_Customer_Id` "
													+ "  LEFT JOIN t_organization_info d "
													+ "    ON d.`Customer_Id` = b.`Transferee_Customer_Id` "
													+ "   LEFT JOIN  `t_dzongkhag_master` e ON e.`dzongkhag_id`=a.`Test_Location` "
													+ "WHERE a.`Vehicle_Number` = b.`Vehicle_Number` ";
private static final  String GET_VEHICLE_EMISSION_RESGISTRATION= "  SELECT "
													+ "    a.`Vehicle_Number`, "
													+ "    CONCAT(c.`First_Name`,' ',c.`Middle_Name`,' ',c.`Last_Name`) AS personalName, "
													+ "    d.`Organization_Name` ,e.`dzongkhag_name`,DATE_FORMAT(a.`Tested_On`,'%d/%m/%Y') AS testDate, "
													+ "DATE_FORMAT(a.`Validity`,'%d/%m/%Y') AS validity,a.`CO`,a.`HSU`,a.`Test_Frequency`,a.`Test_Result`,a.`Emission_Test_Type` "
													+ ",a.`Remarks` "
													+ "  FROM "
													+ "    `t_vehicle_emission_test_dtls` a "
													+ "    LEFT JOIN t_vehicle_registration_dtls b "
													+ "      ON b.`Vehicle_Number` = a.`Vehicle_Number` "
													+ "      AND a.`Vehicle_Number` NOT IN "
													+ "      (SELECT "
													+ "        z.`Vehicle_Number` "
													+ "      FROM "
													+ "        `t_vehicle_transfer_dtls` z) "
													+ "    LEFT JOIN `t_personal_dtls` c "
													+ "      ON c.`Customer_Id` = b.`Customer_Id` "
													+ "    LEFT JOIN t_organization_info d "
													+ "      ON d.`Customer_Id` = b.`Customer_Id` "
													+ "   LEFT JOIN  `t_dzongkhag_master` e ON e.`dzongkhag_id`=a.`Test_Location` "
													+ "  WHERE a.`Vehicle_Number` = b.`Vehicle_Number`";
	private static final  String GET_CONVERSION_DTLS	=	"SELECT "
													+ "  a.`Vehicle_Number`, "
													+ "  k.`Vehicle_Type_Name` "
													+ "FROM "
													+ "  `t_vehicle_registration_dtls` a "
													+ "  LEFT JOIN `t_vehicle_transfer_dtls` b  ON b.`Vehicle_Number` = a.`Vehicle_Number` "
													+ "  LEFT JOIN t_personal_dtls d   ON d.`Customer_Id` = b.`Transferee_Customer_Id` "
													+ "  LEFT JOIN t_organization_info g ON g.`Customer_Id` = b.`Transferee_Customer_Id` "
													+ "  LEFT JOIN `t_vehicle_type_master` k   ON k.`Vehicle_Type_Id` = a.`Vehicle_Type_Id` "
													+ "WHERE a.`Registration_Type` = 3 "
													+ "  AND a.`Vehicle_Number` IN "
													+ "  (SELECT c.`Vehicle_Number` FROM`t_vehicle_transfer_dtls` c) "
													+ "  UNION "
													+ "  SELECT "
													+ "    e.`Vehicle_Number`, "
													+ "    e.`Vehicle_Number` AS customerId "
													+ "  FROM "
													+ "    `t_vehicle_registration_dtls` e "
													+ "    LEFT JOIN t_personal_dtls h  ON h.`Customer_Id` = e.`Customer_Id` "
													+ "    LEFT JOIN `t_organization_info` i ON i.`Customer_Id` = e.`Customer_Id` "
													+ "    LEFT JOIN `t_vehicle_type_master` l  ON l.`Vehicle_Type_Id` = e.`Vehicle_Type_Id` "
													+ "    LEFT JOIN `t_vehicle_type_master` k  ON k.`Vehicle_Type_Id` = e.`Vehicle_Type_Id` "
													+ "  WHERE e.`Vehicle_Number` NOT IN "
													+ "    (SELECT f.`Vehicle_Number`  FROM t_vehicle_transfer_dtls f) "
													+ "    AND e.`Registration_Type` = 3";
	private static final  String GET_LEARNER_LICENSE_ISSUE	="SELECT "
													+ "  a.`Learner_License_No`,DATE_FORMAT(a.`Issue_Date`,'%d/%m/%Y') AS issueDate, "
													+ "  a.`Receipt_No`,DATE_FORMAT(a.`Expiry_Date`,'%d/%m/%Y') AS expiryDate, "
													+ "  DATE_FORMAT(a.`Receipt_Date`,'%d/%m/%Y') AS receiptDate, "
													+ "  CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) AS customerName, "
													+ "  b.`CID_Number`,b.`Present_Phone_No`, c.`region_name` "
													+ "FROM "
													+ "  `t_learner_license_dtls` a "
													+ "  LEFT JOIN t_personal_dtls b ON b.`Customer_Id` = a.`Customer_Id` "
													+ "  LEFT JOIN `t_region_master` c ON c.`region_id`=a.`Region_Id`"
													+ " WHERE a.`Learner_License_Info_Id`>0 ";
	private static final  String GET_LEARNER_LICENSE_DRIVE_TYPE	=	"SELECT "
																+ "  b.`Drive_Type_Name` "
																+ "FROM "
																+ "  `t_learner_licn_drive_type` a "
																+ "  LEFT JOIN `t_drive_type_master` b "
																+ "    ON b.`Drive_Type_Id` = a.`Drive_Type_Id` "
																+ "WHERE a.`Learner_License_No` = ?";
	private static final  String GET_LEARNER_LICENSE_RENEWAL	="SELECT "
														+ "  a.`Learner_License_No`,DATE_FORMAT(a.`Renewal_Date`,'%d/%m/%Y') AS renewalDate, "
														+ "  DATE_FORMAT(a.`Expiry_Date`,'%d/%m/%Y') AS expiryDate,a.`Receipt_No`, "
														+ "  DATE_FORMAT(a.`Receipt_Date`,'%d/%m/%Y') AS receiptDate,a.`Remarks`, "
														+ "  CONCAT(b.`First_Name`,' ',b.`Middle_Name`,' ',b.`Last_Name`) AS customerName "
														+ "FROM "
														+ "  `t_learner_license_renewal_dtls` a "
														+ "  LEFT JOIN `t_personal_dtls` B ON b.`Customer_Id`= a.`Customer_Id` "
														+ " WHERE a.`Learner_License_Renewal_Id`>0 ";
	private static final  String GET_LEARNER_LICENSE_DUPLICATION = "SELECT "
															+ "  a.`Learner_License_No`, "
															+ "  CONCAT( "
															+ "    b.`First_Name`, "
															+ "    ' ', "
															+ "    b.`Middle_Name`, "
															+ "    ' ', "
															+ "    b.`Last_Name` "
															+ "  ) AS customerName, "
															+ "  b.`CID_Number`, "
															+ "  b.`Present_Phone_No`, "
															+ "  a.`Receipt_No`, "
															+ "  a.`Remarks`, "
															+ "  DATE_FORMAT(a.`Issue_Date`, '%d/%m/%Y') AS issueDate, "
															+ "  DATE_FORMAT(a.`Receipt_Date`, '%d/%m/%Y') AS receiptDate "
															+ "FROM "
															+ "  `t_learner_license_duplicate_dtls` a "
															+ "  LEFT JOIN `t_personal_dtls` b "
															+ "    ON b.`Customer_Id` = a.`Customer_Id` "
															+ " WHERE a.`Learner_License_Duplicate_Id`>0 ";
	private static final  String GET_LEARNER_LICENSE_STATISTICS	= "SELECT COUNT(d.`Learner_License_Info_Id`) AS rowCount,e.`region_name`,'NEW' AS processType FROM `t_learner_license_dtls` d "
															+ " LEFT JOIN `t_region_master` e ON e.`region_id`=d.`Region_Id` "
															+ " WHERE d.`Learner_License_Info_Id`>0 ";
	private static final  String GET_LEARNER_LICENSE_RENEWL_STATISTICS	=	"SELECT COUNT(a.`Learner_License_Renewal_Id`) rowCount,c.`region_name`,'RENEW' AS processType FROM `t_learner_license_renewal_dtls` a "
																	+ " LEFT JOIN `t_learner_license_application` b ON b.`Application_Number`=a.`Application_Number` "
																	+ " LEFT JOIN `t_region_master` c ON c.`region_id`=b.`Region_Id` "
																	+ " WHERE b.`Application_Type`='RENEWAL' ";
	private static final  String GET_LEARNER_LICENSE_DUPLICATION_STATISTICS	=	"SELECT COUNT(f.`Learner_License_Duplicate_Id`) rowCount,h.`region_name`,'DUPLICATE' AS processType FROM `t_learner_license_duplicate_dtls` f "
																			+ " LEFT JOIN `t_learner_license_application` g ON g.`Application_Number`=f.`Application_Number` "
																			+ " LEFT JOIN `t_region_master` h ON h.`region_id`=g.`Region_Id` "
																			+ " WHERE g.`Application_Type`='DUPLICATION' ";
	private static final  String GET_LEARNER_LICENSE_DRIVE_TYPE_STATISTICS	=	"SELECT COUNT(a.`Learner_License_Info_Id`) AS rowCount,c.`Drive_Type_Name`,d.`region_name` FROM `t_learner_license_dtls` a "
																		+ " LEFT JOIN `t_learner_licn_drive_type` b ON b.`Learner_License_No`=a.`Learner_License_No` "
																		+ " LEFT JOIN `t_drive_type_master` c ON c.`Drive_Type_Id`=b.`Drive_Type_Id` "
																		+ " LEFT JOIN `t_region_master` d ON d.`region_id`=a.`Region_Id` "
																		+ " WHERE a.`Learner_License_Info_Id`>0 ";
	private static final  String GET_DRIVING_LICENSE_REGISTRATION_OUTSTANDING	=	"SELECT "
																		+ "  a.`Driving_License_No` AS License_No, "
																		+ "  a.`Expiry_Date`, "
																		+ " DATE_FORMAT(a.`Expiry_Date`,'%d/%m/%Y') AS expiryDate, "
																		+ "  CONCAT(d.`First_Name`,' ',d.`Middle_Name`,' ',d.`Last_Name`) AS customerName, "
																		+ "  d.`Present_Contact_Address`,d.`Present_Phone_No`, "
																		+ "  e.`region_name` "
																		+ "FROM "
																		+ "  `t_driving_license_dtls` a "
																		+ "  LEFT JOIN `t_personal_dtls` d ON d.`Customer_Id`=a.`Customer_Id` "
																		+ "  LEFT JOIN `t_region_master` e ON e.`region_id`=a.`Region_Id` "
																		+ "WHERE a.`Driving_License_No` NOT IN "
																		+ "  (SELECT "
																		+ "    b.`License_No` "
																		+ "  FROM "
																		+ "    `t_driving_license_renewal_dtls` b) ";
	
	
	private static final  String GET_DRIVING_LICENSE_RENEWAL_OUTSTANDING	=	"SELECT "
																		+ "  f.`License_No`, "
																		+ "  f.`Expiry_Date`," 
																		+ " DATE_FORMAT(f.`Expiry_Date`,'%d/%m/%Y') AS expiryDate, "
																		+ "  CONCAT( "
																		+ "    g.`First_Name`, "
																		+ "    ' ', "
																		+ "    g.`Middle_Name`, "
																		+ "    ' ', "
																		+ "    g.`Last_Name` "
																		+ "  ) AS customerName, "
																		+ "  g.`Present_Contact_Address`, "
																		+ "  g.`Present_Phone_No`, "
																		+ "  i.`region_name` "
																		+ "FROM "
																		+ "  `t_driving_license_renewal_dtls` f "
																		+ "  LEFT JOIN `t_personal_dtls` g "
																		+ "    ON g.`Customer_Id` = f.`Customer_Id` "
																		+ "  LEFT JOIN `t_driving_license_dtls` h "
																		+ "    ON h.`Driving_License_No` = f.`License_No` "
																		+ "  LEFT JOIN `t_region_master` i "
																		+ "    ON i.`region_id` = h.`Region_Id` "
																		+ "WHERE f.`Driving_License_Renewal_Id`>0";
	
	private static final  String GET_REPORT_QUERY_FROM_REPORT_ID	=	"SELECT "
																		+ "  a.`Report_Query`, "
																		+ "  a.`Report_Description`, "
																		+ "  a.`Report_where_Param` "
																		+ "FROM "
																		+ "  t_report_configuration a "
																		+ "WHERE a.`Report_Configuration_Id` = ?";

		
	private static final String GET_TOTAL_VEHICLE_REGISTERED_DTLS = "SELECT "
																	+ "  e.CID_Number,CONCAT(e.First_Name,\" \",e.Middle_Name,\" \",e.Last_Name)Customer_Name,e.Present_Phone_No,e.`Present_Contact_Address`, "
																	+ "  a.Vehicle_Number,b.Vehicle_Type_Name,c.Vehicle_Model_Name,d.Vehicle_Company_Name, "
																	+ "  a.Engine_CC,a.Engine_Number,a.Chassis_Number,j.Engine_Name,f.Country_Name,g.Dealer_Name,a.Price," +
																			"(SELECT `Colour_Name` FROM `t_colour_master` WHERE `Colour_Id`=a.Colour) Colour, "
																	+ "  k.`Description`, "
																	+ "  DATE_FORMAT(a.Registration_Date,'%d/%m/%Y')Registration_Date,h.region_name,i.base_office_name,a.Remarks "
																	+ "FROM "
																	+ "  t_vehicle_registration_dtls a "
																	+ "  LEFT JOIN t_vehicle_type_master b ON a.Vehicle_Type_Id=b.Vehicle_Type_Id "
																	+ "  LEFT JOIN t_vehicle_model_master c ON a.Vehicle_Model_Id=c.Vehicle_Model_Id "
																	+ "  LEFT JOIN t_vehicle_company_master d ON a.Vehicle_Company_Id=d.Vehicle_Company_Id "
																	+ "  LEFT JOIN t_personal_dtls e ON a.Customer_Id=e.Customer_Id "
																	+ "  LEFT JOIN t_country_master f ON a.Manufacture_Country_Id=f.Country_Id "
																	+ "  LEFT JOIN t_dealer_master g ON a.Dealer_Id=g.Dealer_Id "
																	+ "  LEFT JOIN t_region_master h ON a.Processed_Region_Id=h.region_id "
																	+ "  LEFT JOIN t_base_office_master i ON a.Processed_Base_Id=i.base_office_id "
																	+ "  LEFT JOIN t_engine_type_master j ON a.Engine_Type_Id=j.Engine_Type_Id "
																	+ "  LEFT JOIN `t_registration_type_master` k ON a.`Registration_Type`=k.`Registration_Type_Id` "
																	+ "WHERE a.Created_On = CURDATE()   AND k.`Description` NOT IN ('CONVERSION') "
																	+ "  AND a.Vehicle_Registration_Type = 'P' "
																	+ "  AND IF( "
																	+ "    '1' = ?, "
																	+ "    a.`Processed_Region_Id` = ?  and (a.`Processed_Base_Id`='-1' OR a.`Processed_Base_Id` IS NULL) , "
																	+ "    a.`Processed_Base_Id` = ? "
																	+ "  )"
																	+ "  UNION "
																	+ "  SELECT "
																	+ " '' CID_Number,e.Organization_Name,e.`Phone_Number`,e.Address, "
																	+ " a.Vehicle_Number,b.Vehicle_Type_Name, c.Vehicle_Model_Name,d.Vehicle_Company_Name, "
																	+ " a.Engine_CC,a.Engine_Number,a.Chassis_Number,j.Engine_Name,f.Country_Name,g.Dealer_Name,a.Price," +
																			"(SELECT `Colour_Name` FROM `t_colour_master` WHERE `Colour_Id`=a.Colour) Colour, "
																	+ " k.`Description`, "
																	+ " DATE_FORMAT(a.Registration_Date,'%d/%m/%Y')Registration_Date,h.region_name,i.base_office_name,a.Remarks "
																	+ "FROM "
																	+ "  t_vehicle_registration_dtls a "
																	+ "  LEFT JOIN t_vehicle_type_master b ON a.Vehicle_Type_Id=b.Vehicle_Type_Id "
																	+ "  LEFT JOIN t_vehicle_model_master c ON a.Vehicle_Model_Id=c.Vehicle_Model_Id "
																	+ "  LEFT JOIN t_vehicle_company_master d ON a.Vehicle_Company_Id=d.Vehicle_Company_Id "
																	+ "  LEFT JOIN t_organization_info e ON a.Customer_Id=e.Customer_Id "
																	+ "  LEFT JOIN t_country_master f ON a.Manufacture_Country_Id=f.Country_Id "
																	+ "  LEFT JOIN t_dealer_master g ON a.Dealer_Id=g.Dealer_Id "
																	+ "  LEFT JOIN t_region_master h ON a.Processed_Region_Id=h.region_id "
																	+ "  LEFT JOIN t_base_office_master i ON a.Processed_Base_Id=i.base_office_id "
																	+ "  LEFT JOIN t_engine_type_master j ON a.Engine_Type_Id=j.Engine_Type_Id "
																	+ "  LEFT JOIN `t_registration_type_master` k ON a.`Registration_Type`= k.`Registration_Type_Id` "
																	+ "WHERE "
																	+ "a.Created_On = CURDATE()"
																	+ "  AND a.Vehicle_Registration_Type = 'O' "
																	+ "  AND k.`Description` NOT IN ('CONVERSION')"
																	+ "  AND IF( "
																	+ "    '1' = ?, "
																	+ "    a.`Processed_Region_Id` = ? and (a.`Processed_Base_Id`='-1' OR a.`Processed_Base_Id` IS NULL), "
																	+ "    a.`Processed_Base_Id` = ? "
																	+ "  )";
		
		private static final String GET_TOTAL_VEHICLE_RENEWAL_DTLS = "SELECT "
													+ "  CONCAT(e.First_Name,\" \",  e.Middle_Name, \" \",  e.Last_Name) Customer_Name, "
													+ "  e.CID_Number, "
													+ "  e.Present_Phone_No, "
													+ "  e.`Permanent_Address` AS Address, "
													+ "  a.Vehicle_Number, "
													+ "  b.Vehicle_Type_Name, "
													+ "  c.Vehicle_Model_Name, "
													+ "  d.Vehicle_Company_Name, "
													+ "  a.Engine_CC, "
													+ "  a.Engine_Number, "
													+ "  a.Chassis_Number, "
													+ "  j.Engine_Name, "
													+ "  f.Country_Name, "
													+ "  g.Dealer_Name, "
													+ "  a.Price, "
													+ "  a.Colour, "
													+ "  z.`Renewal_Date`, "
													+ "  z.`Expiry_Date`, "
													+ "  z.`Receipt_No`, "
													+ "  z.`Receipt_Date`, "
													+ "  z.`Renewal_Duration`, "
													+ "  i.`base_office_name`, "
													+ "  h.`region_name`, "
													+ "  z.`Created_By`, "
													+ "  z.`Created_On`, "
													+ "  z.`Remarks` "
													+ "FROM "
													+ "  `t_vehicle_renewal_dtls` z "
													+ "  LEFT JOIN `t_vehicle_registration_dtls` a "
													+ "    ON z.`Vehicle_Id` = a.`Vehicle_Reg_Dtls_Id` "
													+ "  LEFT JOIN t_vehicle_type_master b "
													+ "    ON a.Vehicle_Type_Id = b.Vehicle_Type_Id "
													+ "  LEFT JOIN t_vehicle_model_master c "
													+ "    ON a.Vehicle_Model_Id = c.Vehicle_Model_Id "
													+ "  LEFT JOIN t_vehicle_company_master d "
													+ "    ON a.Vehicle_Company_Id = d.Vehicle_Company_Id "
													+ "  LEFT JOIN t_personal_dtls e "
													+ "    ON a.Customer_Id = e.Customer_Id "
													+ "  LEFT JOIN t_country_master f "
													+ "    ON a.Manufacture_Country_Id = f.Country_Id "
													+ "  LEFT JOIN t_dealer_master g "
													+ "    ON a.Dealer_Id = g.Dealer_Id "
													+ "  LEFT JOIN t_region_master h "
													+ "    ON z.Processed_Region_Id = h.region_id "
													+ "  LEFT JOIN t_base_office_master i "
													+ "    ON z.Processed_Base_Id = i.base_office_id "
													+ "  LEFT JOIN t_engine_type_master j "
													+ "    ON a.Engine_Type_Id = j.Engine_Type_Id "
													+ "WHERE z.`Created_On` = CURDATE() "
													+ "  AND IF( "
													+ "    '1' = ?, "
													+ "    z.`Processed_Region_Id` = ?  and " 
													+ "  (z.`Processed_Base_Id`='-1' OR z.`Processed_Base_Id` IS NULL)," 
													+ "    z.`Processed_Base_Id` = ? "
													+ "  )"
													+ "  AND a.Vehicle_Registration_Type = 'P' "
													+ "  UNION "
													+ "  SELECT "
													+ "  e.Organization_Name AS Customer_Name, "
													+ "  \"\" CID_Number, "
													+ "  e.`Phone_Number`, "
													+ "  e.Address, "
													+ "  a.Vehicle_Number, "
													+ "  b.Vehicle_Type_Name, "
													+ "  c.Vehicle_Model_Name, "
													+ "  d.Vehicle_Company_Name, "
													+ "  a.Engine_CC, "
													+ "  a.Engine_Number, "
													+ "  a.Chassis_Number, "
													+ "  j.Engine_Name, "
													+ "  f.Country_Name, "
													+ "  g.Dealer_Name, "
													+ "  a.Price, "
													+ "  a.Colour, "
													+ "  z.`Renewal_Date`, "
													+ "  z.`Expiry_Date`, "
													+ "  z.`Receipt_No`, "
													+ "  z.`Receipt_Date`, "
													+ "  z.`Renewal_Duration`, "
													+ "  i.`base_office_name`, "
													+ "  h.`region_name`, "
													+ "  z.`Created_By`, "
													+ "  z.`Created_On`, "
													+ "  z.`Remarks` "
													+ "FROM "
													+ "  `t_vehicle_renewal_dtls` z "
													+ "  LEFT JOIN t_vehicle_registration_dtls a "
													+ "    ON z.`Vehicle_Id` = a.`Vehicle_Reg_Dtls_Id` "
													+ "  LEFT JOIN t_vehicle_type_master b "
													+ "    ON a.Vehicle_Type_Id = b.Vehicle_Type_Id "
													+ "  LEFT JOIN t_vehicle_model_master c "
													+ "    ON a.Vehicle_Model_Id = c.Vehicle_Model_Id "
													+ "  LEFT JOIN t_vehicle_company_master d "
													+ "    ON a.Vehicle_Company_Id = d.Vehicle_Company_Id "
													+ "  LEFT JOIN t_organization_info e "
													+ "    ON a.Customer_Id = e.Customer_Id "
													+ "  LEFT JOIN t_country_master f "
													+ "    ON a.Manufacture_Country_Id = f.Country_Id "
													+ "  LEFT JOIN t_dealer_master g "
													+ "    ON a.Dealer_Id = g.Dealer_Id "
													+ "  LEFT JOIN t_region_master h "
													+ "    ON z.Processed_Region_Id = h.region_id "
													+ "  LEFT JOIN t_base_office_master i "
													+ "    ON z.Processed_Base_Id = i.base_office_id "
													+ "  LEFT JOIN t_engine_type_master j "
													+ "    ON a.Engine_Type_Id = j.Engine_Type_Id "
													+ "WHERE z.`Created_On`=CURDATE() "
													+ "  AND IF( "
													+ "    '1' = ?, "
													+ "    z.`Processed_Region_Id` = ? and  (z.`Processed_Base_Id`='-1' OR z.`Processed_Base_Id` IS NULL), "
													+ "    z.`Processed_Base_Id` = ? "
													+ "  )"
													+ "  AND a.Vehicle_Registration_Type = 'O'";
		
		private static final String GET_TOTAL_LIC_ISSUE_DTLS =  "SELECT "
											+ "  (SELECT region_name FROM t_region_master WHERE region_id=a.Region_Id) region, "
											+ "  (SELECT base_office_name FROM t_base_office_master WHERE base_office_id=a.Processed_Base_Id) baseOffice, "
											+ "  CONCAT( "
											+ "    b.First_Name, "
											+ "    ' ', "
											+ "    b.Middle_Name, "
											+ "    ' ', "
											+ "    b.Last_Name "
											+ "  ) customerName, "
											+ "  b.CID_Number, "
											+ "  DATE_FORMAT(b.Date_Of_Birth, '%d/%m/%Y') dob, "
											+ "  a.Learner_License_No, "
											+ "  a.Driving_License_No, "
											+ "  DATE_FORMAT(a.Issue_Date,'%d/%m/%Y') issueDate, "
											+ "  IF( "
											+ "    a.Driving_License_Id IN "
											+ "    (SELECT "
											+ "      License_Id "
											+ "    FROM "
											+ "      t_driving_license_renewal_dtls "
											+ "    WHERE License_Id = a.Driving_License_Id), "
											+ "    (SELECT "
											+ "      DATE_FORMAT(MAX(Expiry_Date),'%d/%m/%Y') "
											+ "    FROM "
											+ "      t_driving_license_renewal_dtls "
											+ "    WHERE License_Id = a.Driving_License_Id), "
											+ "    DATE_FORMAT(a.Expiry_Date,'%d/%m/%Y') "
											+ "  ) expiryDate, "
											+ "  GROUP_CONCAT((SELECT Drive_Type_Name FROM t_drive_type_master WHERE Drive_Type_Id=c.Drive_Type_Id)) AS driveTypes "
											+ "FROM "
											+ "  t_driving_license_dtls a "
											+ "  LEFT JOIN t_personal_dtls b "
											+ "    ON a.Customer_Id = b.Customer_Id "
											+ "  LEFT JOIN t_driving_license_drive_type c "
											+ "    ON a.Driving_License_Id = c.License_Id "
											+ "WHERE a.Driving_License_Id NOT IN "
											+ "  (SELECT "
											+ "    License_Id "
											+ "  FROM "
											+ "    t_driving_license_cancelled) "
											+ "     AND a.`Created_On`=CURDATE() "
											+ "  AND IF( "
											+ "    '1' = ?, "
											+ "    a.`Processed_Region_Id` = ? and  (a.`Processed_Base_Id`='-1' OR a.`Processed_Base_Id` IS NULL) , "
											+ "    a.`Processed_Base_Id` = ? "
											+ "  )"
											+ "    GROUP BY a.Driving_License_Id";
		

		private static final String GET_TOTAL_LIC_RENEWED_DTLS = "SELECT "
										+ "CONCAT(t_personal_dtls.First_Name,\" \", "
										+ "  t_personal_dtls.Middle_Name,\" \", "
										+ "  t_personal_dtls.Last_Name)customerName "
										+ "  , "
										+ "  t_personal_dtls.CID_Number, "
										+ "  t_driving_license_dtls.Driving_License_No, "
										+ "  t_driving_license_renewal_dtls.Renewal_Date, "
										+ "  t_driving_license_renewal_dtls.Expiry_Date, "
										+ "  t_driving_license_renewal_dtls.Receipt_No, "
										+ "  t_driving_license_renewal_dtls.Receipt_Date, "
										+ "  t_base_office_master.base_office_name, "
										+ "  t_region_master.region_name, "
										+ "  t_driving_license_renewal_dtls.Created_By, "
										+ "  t_driving_license_renewal_dtls.Created_On, "
										+ "  t_driving_license_renewal_dtls.Remarks "
										+ "FROM "
										+ "  t_driving_license_renewal_dtls "
										+ " LEFT JOIN t_driving_license_dtls "
										+ "    ON t_driving_license_renewal_dtls.License_Id = t_driving_license_dtls.Driving_License_Id "
										+ " LEFT JOIN t_personal_dtls "
										+ "    ON t_driving_license_renewal_dtls.Customer_Id = t_personal_dtls.Customer_Id "
										+ " LEFT JOIN t_region_master "
										+ "    ON t_driving_license_renewal_dtls.Processed_Region_Id = t_region_master.region_id "
										+ " LEFT JOIN t_base_office_master "
										+ "    ON t_driving_license_renewal_dtls.Processed_Base_Id = t_base_office_master.base_office_id "
										+ "WHERE t_driving_license_renewal_dtls.`Created_On`=CURDATE()"
										+ "  AND IF( "
										+ "    '1' = ?, "
										+ "    t_driving_license_renewal_dtls.`Processed_Region_Id` = ?  and  (t_driving_license_renewal_dtls.`Processed_Base_Id`='-1' OR t_driving_license_renewal_dtls.`Processed_Base_Id` IS NULL), "
										+ "    t_driving_license_renewal_dtls.`Processed_Base_Id` = ? "
										+ "  )";

		private static final String GET_TOTAL_LL_ISSUED_DTLS = "SELECT CONCAT(t_personal_dtls.First_Name,\" \",t_personal_dtls.Middle_Name,\" \",t_personal_dtls.Last_Name) AS Customer_Name, "
															+ "t_personal_dtls.CID_Number, "
															+ "t_learner_license_dtls.Learner_License_No, "
															+ "t_learner_license_dtls.Issue_Date, "
															+ "t_learner_license_dtls.Expiry_Date, "
															+ "t_learner_license_dtls.Receipt_No, "
															+ "t_learner_license_dtls.Receipt_Date, "
															+ "t_base_office_master.base_office_name, "
															+ "t_region_master.region_name, "
															+ "`t_learner_license_dtls`.`Created_By`, "
															+ "`t_learner_license_dtls`.`Created_On`, "
															+ "`t_learner_license_dtls`.`Remarks` "
															+ "FROM t_learner_license_dtls  LEFT JOIN t_personal_dtls ON t_learner_license_dtls.Customer_Id=t_personal_dtls.Customer_Id "
															+ " LEFT JOIN t_region_master ON t_learner_license_dtls.Processed_Region_Id=t_region_master.region_id "
															+ " LEFT JOIN t_base_office_master ON t_learner_license_dtls.Processed_Base_id=t_base_office_master.base_office_id "
															+ "WHERE t_learner_license_dtls.`Created_On`=CURDATE()"
															+ "  AND IF( "
															+ "    '1' = ?, "
															+ "    t_learner_license_dtls.`Processed_Region_Id` = ?  and  (t_learner_license_dtls.`Processed_Base_Id`='-1' OR t_learner_license_dtls.`Processed_Base_Id` IS NULL) , "
															+ "    t_learner_license_dtls.`Processed_Base_Id` = ? "
															+ "  )";
		
		
		private static final String GET_TOTAL_DL_PRINT_DTLS = 	 " SELECT "
			+ " 												(SELECT  "
			+ " 											    `Service_Name` " 
			+ " 													  FROM "
			+ " 													    `t_service_master` " 
			+ " 													  WHERE `Service_Id` = a.`Service_Id`) Service_Name, "
			+ " 													  a.`Identity_Number`, "
			+ " 													IF(a.`Service_Id`='114',(SELECT `t_personal_dtls`.`CID_Number` FROM `t_top_application` " 
			+ " 													LEFT JOIN `t_personal_dtls` ON `t_top_application`.`Customer_Id`=`t_personal_dtls`.`Customer_Id` " 
			+ " 													WHERE `t_top_application`.`Application_Number`=a.`Application_Number`), "
			+ " 													(SELECT `t_personal_dtls`.`CID_Number` FROM `t_driving_license_application` " 
			+ " 													LEFT JOIN `t_personal_dtls` ON `t_driving_license_application`.`Customer_Id`=`t_personal_dtls`.`Customer_Id` " 
			+ " 													WHERE `t_driving_license_application`.`Application_Number`=a.`Application_Number`  ))CID_Number, "
			+ " 													IF(a.`Service_Id`='114',(SELECT CONCAT(`t_personal_dtls`.`First_Name`,' ',`t_personal_dtls`.`Middle_Name`,' ',`t_personal_dtls`.`Last_Name`) FROM `t_top_application` " 
			+ " 													LEFT JOIN `t_personal_dtls` ON `t_top_application`.`Customer_Id`=`t_personal_dtls`.`Customer_Id` " 
			+ " 													WHERE `t_top_application`.`Application_Number`=a.`Application_Number`), "
			+ " 													(SELECT CONCAT(`t_personal_dtls`.`First_Name`,' ',`t_personal_dtls`.`Middle_Name`,' ',`t_personal_dtls`.`Last_Name`) FROM `t_driving_license_application` " 
			+ " 													LEFT JOIN `t_personal_dtls` ON `t_driving_license_application`.`Customer_Id`=`t_personal_dtls`.`Customer_Id` " 
			+ " 													WHERE `t_driving_license_application`.`Application_Number`=a.`Application_Number`  ))customerName "
			+ " 													FROM "
			+ " 													  `t_print_dtls` a "  
			+ " 													WHERE a.`Printed_On`=CURDATE() "
			+ "  													 AND IF("
			+ "     '1' = ?,"
			+ "     a.`Jurisdiction_Id`=?"
			+ "      AND a.`Jurisdiction_Type_Id`=1 ,"
			+ "      a.`Jurisdiction_Id`=? AND a.`Jurisdiction_Type_Id`=2"
			+ "    )";
		
		
		
		
		
		
}