package bt.gov.rsta.eralis.dao.license;

import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.business.payment.PaymentBusiness;
import bt.gov.rsta.eralis.dto.DuplicateDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.license.OffenceDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EralisCommonUtil;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.WorkflowManager;
import bt.gov.rsta.framework.vo.DocumentVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.vo.WorkflowDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class LicenseDAO 
{
	private static LicenseDAO licenseDAO;
	SimpleDateFormat sourceSdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date(System.currentTimeMillis());
	private static int duration = 0;
	
	public static LicenseDAO getInstance() {
		if (licenseDAO == null)
			licenseDAO = new LicenseDAO();
		return licenseDAO;
	}
	
	public synchronized String new_issue(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	null;
		try 
		{
			if (conn != null)
			{
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_learner_license_application", dto.getPageId(),null, conn);
				String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
				pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_APPLICATION);
				pst.setString(1, applicationNumber);
				pst.setString(2, "NEW");
				pst.setString(3, dto.getCustomerId());
				pst.setString(4, null);
				pst.setString(5, dto.getRegion());
				pst.setString(6, dto.getReceiptNo());
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDate1 = targetSdf.format(receiptDate);
				pst.setString(7, receiptDate1);
				pst.setString(8, dto.getCertifyingDoctor());
				pst.setString(9, dto.getRemarks());
				pst.setString(10,  userVo.getActorId());
				pst.setString(11, userVo.getActorId());
				int count = pst.executeUpdate();
				if (count > 0) 
				{
					String[] driveTypeArray = dto.getDriveTypeList().split("#");
					for (int i = 0; i < driveTypeArray.length; i++) {
						if (!(driveTypeArray[i].equals("0"))) {
							pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_DRIVE_TYPE_APPLICATION);
							pst.setString(1, applicationNumber);
							pst.setString(2, driveTypeArray[i]);
							count = pst.executeUpdate();
						}
					}
				}
				if (count > 0) {
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);
					DocumentVO documentVO = new DocumentVO();
					if(null != dto.getFileMC() && dto.getFileMC().getFileSize() > 0)
					{
						documentVO.setServiceName("LEARNER");
						documentVO.setFileContent(dto.getFileMC().getFileData());
						documentVO.setName(dto.getFileMC().getFileName());
						documentVO.setDocumentType("LL");
						CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
					}
					if(null != dto.getFileApplForm() && dto.getFileApplForm().getFileSize() > 0)
					{
						documentVO.setServiceName("LEARNER");
						documentVO.setFileContent(dto.getFileApplForm().getFileData());
						documentVO.setName(dto.getFileApplForm().getFileName());
						documentVO.setDocumentType("LL");
						CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
					}

					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("NEW");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setPenalty("0");
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr = targetSdf.format(receipt);
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
					//result = "SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result+"#"+applicationNumber;
	}
	public synchronized String[][] pay_offence(LicenseDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		String arrayList[][] = new String[4][5];
		PreparedStatement pst = null,pst1 = null,pst2 = null,pst3 = null;
		ResultSet rs1 = null,rs2 = null,rs3 = null;
		arrayList[3][0] = "FAILURE";
		try {
			if(conn != null)
			{
				pst=conn.prepareStatement(UPDATE_OFFENCE_PAYMENT);
				pst.setString(1, dto.getAmount());
				pst.setString(2, "Y");
				pst.setString(3, dto.getReceiptNo());
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String offenceReceiptDate = targetSdf.format(receiptDate);
				pst.setString(4, offenceReceiptDate);
				pst.setString(5, userVo.getActorId());
				pst.setString(6, dto.getTINno());
				pst.setString(7, dto.getOffenceId());
				pst.executeUpdate();
				
				pst1 = conn.prepareStatement(GET_VEHICLE_DL_LL_ID_FROM_OFFENCE);
				pst1.setString(1, dto.getOffenceId());
				rs1 = pst1.executeQuery();
				rs1.first();
				
				
				pst2 = conn.prepareStatement(GET_VEHICLE_REGISTRATION_TYPE);
				pst2.setString(1, rs1.getString("Vehicle_Id"));
				rs2 = pst2.executeQuery();
				rs2.first();
				if(rs2.getString("Vehicle_Registration_Type").equals("P")){
					pst3 = conn.prepareStatement(GET_OWNER_PHONE_NUMBER);
				}
				else {
					pst3 = conn.prepareStatement(GET_ORGANISATION_CONTACT_DTLS);
				}
					pst3.setString(1, rs1.getString("Vehicle_Id"));
					rs3 = pst3.executeQuery();
					rs3.first();
					arrayList[0][0] = rs3.getString("pname");
					arrayList[0][1] = rs3.getString("currentDate");
					arrayList[0][2] = rs3.getString("Present_Email");
					arrayList[0][3] = rs3.getString("Present_Phone_No");
					arrayList[0][4] = dto.getTINno();
					
				if(rs1.getString("License_Id")!=null && rs1.getInt("License_Id")>0)
				{
					pst2 = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FROM_DRIVING_LICENSE);
					pst2.setString(1, rs1.getString("License_Id"));
					rs2 = pst2.executeQuery();
					rs2.first();
					arrayList[1][0] = rs2.getString("pname");
					arrayList[1][1] = rs2.getString("currentDate");
					arrayList[1][2] = rs2.getString("Present_Email");
					arrayList[1][3] = rs2.getString("Present_Phone_No");
					arrayList[1][4] = dto.getTINno();
				}
				else if(rs1.getString("Learner_License_Id")!=null  && rs1.getInt("Learner_License_Id")>0)
				{
					pst2 = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FROM_LEARNER_LICENSE);
					pst2.setString(1, rs1.getString("Learner_License_Id"));
					rs2 = pst2.executeQuery();
					rs2.first();
					
					arrayList[2][0] = rs2.getString("pname");
					arrayList[2][1] = rs2.getString("currentDate");
					arrayList[2][2] = rs2.getString("Present_Email");
					arrayList[2][3] = rs2.getString("Present_Phone_No");
					arrayList[2][4] = dto.getTINno();
				}
					arrayList[3][0] = "SUCCESS";
					
			}
		}
		catch (Exception e) {
			arrayList[3][0] = "FAILURE";
			e.printStackTrace();
			throw new ERALISException("###Error at licenseDAO[pay_offence]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
			ConnectionManager.close(null, null, rs1, pst1);
			ConnectionManager.close(null, null, rs2, pst2);
			ConnectionManager.close(null, null, rs3, pst3);
			
		}
		return arrayList;

	}
	
	public synchronized String license_renewal_learner(LicenseDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		
		String isDuplicate =  null;
		try {

			if(conn != null)
			{
				
				DuplicateDTO dupDTO = new DuplicateDTO();
				dupDTO.setLearnerId(dto.getLearnerLicenseId());
				dupDTO.setRequestType("LEARNER");
				dupDTO.setServiceType("RENEWAL");
				/*if(null!=dto.getBfsNo())
				{
					conn1 = ConnectionManager.getConnection();
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn1);
					//ConnectionManager.close(conn1);
				}
				else
				{
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				}*/
				if(dto.getBfsNo()!=null)
				{	
					Connection conn1 = ConnectionManager.getConnection();
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn1);
					ConnectionManager.close(conn1);
				}
				else
				{
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				}
				
				if(isDuplicate.equals("N"))
				{
					
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_learner_license_application", dto.getPageId(),dto.getBfsNo(), conn);
					String serviceid = EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
					
					
					
					pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_RENEWAL_APPLICATION);
					pst.setString(1, applicationNumber);
					pst.setString(2, "RENEWAL");
					pst.setString(3, dto.getCustomerId());
					pst.setString(4, dto.getLicenseNo());
					pst.setString(5, dto.getRegion());
					pst.setString(6, dto.getReceiptNo());
					
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDate1 = targetSdf.format(receiptDate);
					
					pst.setString(7, receiptDate1);
					pst.setString(8, null);
					pst.setString(9, dto.getRemarks());
					//pst.setDate(10, date);
					pst.setString(10, userVo.getActorId());
					//pst.setDate(12, date);
					pst.setString(11, userVo.getActorId());
					//pst.setDate(14, date);
					pst.setString(12, dto.getLearnerLicenseId());
					pst.setString(13, dto.getRenewalDuration());
					
					int count = pst.executeUpdate();
					
					if(count > 0)
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);
						if(null!=dto.getSupportDoc())
						{
							DocumentVO documentVO = new DocumentVO();
							documentVO.setServiceName("LEARNER");
							documentVO.setFileContent(dto.getSupportDoc().getFileData());
							documentVO.setName(dto.getSupportDoc().getFileName());
							documentVO.setDocumentType("RLL");
							CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
						}
						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNumber);
						paymentDTO.setServiceId(serviceid);
						paymentDTO.setApplicationType("RENEW");
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setPenalty("0");
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
						String receiptDateStr = targetSdf.format(receipt);
						paymentDTO.setReceiptDate(receiptDateStr);
						result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
						
						//result = "SUCCESS";
					}
				}
				else
					result = "LEARNER_DUPLICATE_TRANSACTION";
				
				//if(null!=dto.getsNo())
				if(result.equalsIgnoreCase("LEARNER_DUPLICATE_TRANSACTION"))
				  {
					  String[] isDuplicateArray	=	isDuplicate.split("#");
					  String receiptNo		=	isDuplicateArray[1];
					  applicationNumber		=	isDuplicateArray[2];
					  //bfsOrderNo			=	receiptNo; old code
					  applicationNumber	=	applicationNumber+"#"+receiptNo;  
				  }	
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_renewal_learner]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
			if(null!=dto.getBfsNo() && isDuplicate.equals("N"))
			{
				
			}
		}
		//return result+"#"+applicationNumber+"#"+bfsOrderNo; old code
		return result+"#"+applicationNumber;

	}

	
	public synchronized String license_duplication_learner(LicenseDTO dto, UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		String bfsOrderNo =	null;
		try {

			if(conn != null)
			{
				DuplicateDTO dupDTO = new DuplicateDTO();
				dupDTO.setLearnerId(dto.getLearnerLicenseId());
				dupDTO.setRequestType("LEARNER");
				dupDTO.setServiceType("DUPLICATION");
				String isDuplicate = null;
				if(dto.getBfsNo()!=null)
				{	
					Connection conn1 = ConnectionManager.getConnection();
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn1);
					ConnectionManager.close(conn1);
				}
				else
				{
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				}
				
				  
				if(isDuplicate.equals("N"))
				{
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_learner_license_application", dto.getPageId(),dto.getBfsNo(), conn);
					String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
					
					if(null!=dto.getBfsNo())
					  {
						  bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(dto.getService(),applicationNumber,dto.getServiceName());
						 
						  if(null==bfsOrderNo || bfsOrderNo.equalsIgnoreCase(""))
						  {
							  return "BFS_ORDER_NO_FAILURE"+"#"+null+"#"+null;
						  }
							  
					  }	  
					
					pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_DUPLICATION_APPLICATION);
					pst.setString(1, applicationNumber);
					pst.setString(2, "DUPLICATION");
					pst.setString(3, dto.getCustomerId());
					pst.setString(4, dto.getLicenseNo());
					pst.setString(5, dto.getRegion());
					pst.setString(6, dto.getReceiptNo());
					
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDate1 = targetSdf.format(receiptDate);
					
					pst.setString(7, receiptDate1);
					pst.setString(8, null);
					pst.setString(9, dto.getRemarks());
					pst.setString(10, userVo.getActorId());
					pst.setString(11, userVo.getActorId());
					pst.setString(12, dto.getLearnerLicenseId());
					int count = pst.executeUpdate();
					
					if(count > 0)
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);
						
						if(null != dto.getSupportDoc() && dto.getSupportDoc().getFileSize() > 0)
						{
							DocumentVO documentVO = new DocumentVO();
							documentVO.setServiceName("LEARNER");
							documentVO.setFileContent(dto.getSupportDoc().getFileData());
							documentVO.setName(dto.getSupportDoc().getFileName());
							documentVO.setDocumentType("LL");
							CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
						}
						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNumber);
						paymentDTO.setServiceId(serviceid);
						paymentDTO.setApplicationType("DUPLICATE");
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setPenalty("0");
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
						String receiptDateStr = targetSdf.format(receipt);
						paymentDTO.setReceiptDate(receiptDateStr);
						result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
						
						//result = "SUCCESS";
					}
				}
				else
					result = "LEARNER_DUPLICATE_TRANSACTION";
				
				//if(null!=dto.getBfsNo())
				if(result.equalsIgnoreCase("LEARNER_DUPLICATE_TRANSACTION"))
				{
					String[] isDuplicateArray	=	isDuplicate.split("#");
					String receiptNo		=	isDuplicateArray[1];
					applicationNumber		=	isDuplicateArray[2];
					bfsOrderNo			=	receiptNo;
					applicationNumber	=	applicationNumber+"#"+bfsOrderNo;  
				}	
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_duplication_learner]:: "
							+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber+"#"+bfsOrderNo;

	}
	
	
	
	public synchronized String license_non_commerical(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException

	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		try {

			if(conn != null)
			{
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(),dto.getBfsNo(), conn);
				//applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(), conn);
				String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
				pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_NON_COMMERCIAL_APPLICATION);
				//issueType Issue_type
				pst.setString(1, applicationNumber);
				pst.setString(2, "NONCOMMERCIAL");
				pst.setString(3,"N");
				pst.setString(4, null);
				pst.setString(5, dto.getLLNo());
				pst.setString(6, dto.getCustomerId());
				pst.setString(7, null);
				pst.setString(8, dto.getRegion());
				pst.setString(9, null);
				pst.setString(10, null);
				pst.setString(11, null);
				pst.setString(12, null);
				pst.setString(13, null);
				pst.setString(14, null);
				pst.setString(15, dto.getDrivetype());
				pst.setString(16, null);
				pst.setString(17, dto.getRemarks());
				pst.setString(18, dto.getRenewalDuration());
				pst.setString(19, dto.getReceiptNo());
				
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDate1 = targetSdf.format(receiptDate);
				
				pst.setString(20, receiptDate1);
				pst.setString(21, null);
				//pst.setDate(21, date);
				pst.setString(22, userVo.getActorId());
				//pst.setDate(23, date);
				pst.setString(23, userVo.getActorId());
				pst.setString(24, dto.getIssueType());
				//pst.setDate(25, date);
				
				int count = pst.executeUpdate();
				
				if(count > 0)
				{
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);

					if(null!=dto.getRcCertificate())
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("LICENSE");
						documentVO.setFileContent(dto.getRcCertificate().getFileData());
						documentVO.setName(dto.getRcCertificate().getFileName());
						documentVO.setDocumentType("DL");
						CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
					}
					if(null!=dto.getFileApplForm())
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("LICENSE");
						documentVO.setFileContent(dto.getFileApplForm().getFileData());
						documentVO.setName(dto.getFileApplForm().getFileName());
						documentVO.setDocumentType("DL");
						CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
					}
					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("NEW");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setPenalty("0");
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr = targetSdf.format(receipt);
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
					
					//result = "SUCCESS";
				}
			
			}
		}
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_non_commerical]:: " + e);
		}
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber;

	}

	public synchronized String license_commerical(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException

	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		
		try {

			if(conn != null)
			{
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(),dto.getBfsNo(), conn);
				String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
				
				pst = conn.prepareStatement(INSERT_COMMERCIAL_LICENSE_DETAILS);
				
				pst.setString(1, applicationNumber);
				pst.setString(2, "COMMERCIAL");
				pst.setString(3,"C" );
				pst.setString(4, null);
				pst.setString(5, null);
				pst.setString(6, dto.getCustomerId());
				pst.setString(7, dto.getLicenseNo());
				pst.setString(8, dto.getRegion());
				pst.setString(9, null);
				pst.setString(10, null);
				pst.setString(11, null);
				pst.setString(12, null);
				pst.setString(13, null);
				pst.setString(14, null);
				pst.setString(15, dto.getDrivetype());
				pst.setString(16, null);
				pst.setString(17, dto.getRemarks());
				pst.setString(18, dto.getReceiptNo());
				
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDate1 = targetSdf.format(receiptDate);
				
				pst.setString(19, receiptDate1);
				pst.setString(20, null);
				//pst.setDate(21, date);
				pst.setString(21, userVo.getActorId());
				//pst.setDate(23, date);
				pst.setString(22, userVo.getActorId());
				//pst.setDate(25, date);
				pst.setString(23, dto.getDrivinglicenseId());
				pst.setString(24, dto.getRenewalDuration());
				
				int count = pst.executeUpdate();
				
				if(count > 0)
				{
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);
					
					PaymentDTO paymentDTO = new PaymentDTO();
					paymentDTO.setApplicationNo(applicationNumber);
					paymentDTO.setServiceId(serviceid);
					paymentDTO.setApplicationType("NEW");
					paymentDTO.setAmount(dto.getAmount());
					paymentDTO.setPenalty("0");
					paymentDTO.setCreatedBy(userVo.getActorId());
					paymentDTO.setReceiptNo(dto.getReceiptNo());
					java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr = targetSdf.format(receipt);
					paymentDTO.setReceiptDate(receiptDateStr);
					result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
					
					//result = "SUCCESS";
				}
			
			}
		}
		catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_commerical]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber;

	}

	public synchronized String license_renewal(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		
		try {

			if(conn != null)
			{
				DuplicateDTO dupDTO = new DuplicateDTO();
				dupDTO.setLicenseId(dto.getDrivinglicenseId());
				dupDTO.setRequestType("LICENSE");
				dupDTO.setServiceType("LICENSERENEWAL");
					 
				String isDuplicate = null;
				
				if(dto.getBfsNo()!=null)
				{	
					Connection conn1 = ConnectionManager.getConnection();
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn1);
					ConnectionManager.close(conn1);
				}
				else
				{
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				}
				  		  
				if(isDuplicate.equals("N"))
				{
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(),dto.getBfsNo(), conn);
					String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
					
									
					
					
					pst = conn.prepareStatement(INSERT_RENEWAL_LICENSE_DETAILS);
					pst.setString(1, applicationNumber);
					pst.setString(2, "LICENSERENEWAL");
					pst.setString(3,null );
					pst.setString(4, null);
					pst.setString(5, null);
					pst.setString(6, dto.getCustomerId());
					pst.setString(7, dto.getLicenseNo());
					pst.setString(8, dto.getDrivinglicenseId());
					pst.setString(9, dto.getRegion());
					pst.setString(10, null);
					pst.setString(11, null);
					pst.setString(12, null);
					pst.setString(13, null);
					pst.setString(14, null);
					pst.setString(15, null);
					pst.setString(16, null);
					pst.setString(17, null);
					pst.setString(18, dto.getRemarks());
					pst.setString(19, dto.getReceiptNo());
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDate1 = targetSdf.format(receiptDate);
					pst.setString(20, receiptDate1);
					pst.setString(21, null);
					pst.setString(22, userVo.getActorId());
					pst.setString(23, userVo.getActorId());
					pst.setString(24, dto.getRenewalDuration());
					int count = pst.executeUpdate();
					
					if(count > 0)
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);
						
						if(null!=dto.getSupportDoc())
						{
							DocumentVO documentVO = new DocumentVO();
							documentVO.setServiceName("LICENSE");
							documentVO.setFileContent(dto.getSupportDoc().getFileData());
							documentVO.setName(dto.getSupportDoc().getFileName());
							documentVO.setDocumentType("DL");
							CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
						}
						
						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNumber);
						paymentDTO.setServiceId(serviceid);
						paymentDTO.setApplicationType("RENEW");
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setPenalty(dto.getPenalty());
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
						String receiptDateStr = targetSdf.format(receipt);
						paymentDTO.setReceiptDate(receiptDateStr);
						result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
						//result = "SUCCESS";
					}
				}
				else
					result = "LICENSE_DUPLICATE_TRANSACTION";
				
				 //if(null!=dto.getBfsNo())
				  if(result.equalsIgnoreCase("LICENSE_DUPLICATE_TRANSACTION"))
				  {
					  String[] isDuplicateArray	=	isDuplicate.split("#");
					  String receiptNo		=	isDuplicateArray[1];
					  applicationNumber		=	isDuplicateArray[2];
					  //bfsOrderNo			=	receiptNo;
					  applicationNumber	=	applicationNumber+"#"+receiptNo;  
				  }
			}
		}
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_renewal]:: " + e);
		} 
		finally 
		{
			ConnectionManager.close(null, null, null, pst);
		}
		return result+"#"+applicationNumber+"#";
	}


	public synchronized String license_duplication(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException

	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		String bfsOrderNo =	null;
		
		try 
		{
			if(conn != null)
			{
				DuplicateDTO dupDTO = new DuplicateDTO();
				dupDTO.setLicenseId(dto.getDrivinglicenseId());
				dupDTO.setRequestType("LICENSE");
				dupDTO.setServiceType("LICENSEDUPLICATION");
					  
				String isDuplicate = null;
				if(dto.getBfsNo()!=null)
				{	
					Connection conn1 = ConnectionManager.getConnection();
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn1);
					ConnectionManager.close(conn1);
				}
				else
				{
					isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				}
				
				if(isDuplicate.equals("N"))
				{
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(),dto.getBfsNo(), conn);
					String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
					
					if(null!=dto.getBfsNo())
					  {
						  bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(dto.getService(),applicationNumber,dto.getServiceName());
						 
						  if(null==bfsOrderNo || bfsOrderNo.equalsIgnoreCase(""))
						  {
							  return "BFS_ORDER_NO_FAILURE"+"#"+null+"#"+null;
						  }
					  }	
					
					
					pst = conn.prepareStatement(INSERT_LICENSE_DUPLICATION_DETAILS);
					
					pst.setString(1, applicationNumber);
					pst.setString(2, "LICENSEDUPLICATION");						
					pst.setString(3,null );
					pst.setString(4, null);
					pst.setString(5, null);
					pst.setString(6, dto.getCustomerId());
					pst.setString(7, dto.getLicenseNo());
					pst.setString(8, dto.getRegionId());
					pst.setString(9, null);
					pst.setString(10, null);
					pst.setString(11, null);
					pst.setString(12, null);
					pst.setString(13, null);
					pst.setString(14, null);
					pst.setString(15, null);
					pst.setString(16, null);
					pst.setString(17, dto.getRemarks());
					pst.setString(18, dto.getReceiptNo());
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDate1 = targetSdf.format(receiptDate);
					pst.setString(19, receiptDate1);
					pst.setString(20, null);
					pst.setString(21, userVo.getActorId());
					pst.setString(22, userVo.getActorId());
					pst.setString(23, dto.getDrivinglicenseId());
					int count = pst.executeUpdate();
					
					if(count > 0)
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);
						if(null!=dto.getSupportDoc())
						{
							DocumentVO documentVO = new DocumentVO();
							documentVO.setServiceName("DUPLICATION");
							documentVO.setFileContent(dto.getSupportDoc().getFileData());
							documentVO.setName(dto.getSupportDoc().getFileName());
							documentVO.setDocumentType("DL");
							CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
						}
						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNumber);
						paymentDTO.setServiceId(serviceid);
						paymentDTO.setApplicationType("DUPLICATION");
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setPenalty("0");
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
						String receiptDateStr = targetSdf.format(receipt);
						paymentDTO.setReceiptDate(receiptDateStr);
						result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
						
						//result = "SUCCESS";
					}
				}
				else
					result = "LICENSE_DUPLICATE_TRANSACTION";
				
				//if(null!=dto.getBfsNo())
				if(result.equalsIgnoreCase("LICENSE_DUPLICATE_TRANSACTION"))
				  {
					  String[] isDuplicateArray	=	isDuplicate.split("#");
					  String receiptNo		=	isDuplicateArray[1];
					  applicationNumber		=	isDuplicateArray[2];
					  bfsOrderNo			=	receiptNo;
					  applicationNumber	=	applicationNumber+"#"+bfsOrderNo;  
				  }	
			}
		}
		catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_duplication]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber+"#"+bfsOrderNo;
	}
	
	
	public synchronized String license_endorsement(LicenseDTO dto,UserDetailsVO userVo,String isRenewalProcess, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		try {

			if(conn != null)
			{
				DuplicateDTO dupDTO = new DuplicateDTO();
				dupDTO.setLicenseId(dto.getDrivinglicenseId());
				dupDTO.setRequestType("LICENSE");
				dupDTO.setServiceType("LICENSEENDORSE");
					  
				String isDuplicate = CommonDAO.getInstance().checkIfDuplicateApplication(dupDTO, conn);
				  		  
				if(isDuplicate.equals("N"))
				{
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(),dto.getBfsNo(), conn);
					//applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_driving_license_application", dto.getPageId(), conn);
					String serviceid = EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
					
					String driveType = null;
					if(dto.getTcbEndorsement().equals("Y"))
					{
						for(int i=0; i<dto.getDrivetypeTCB().length; i++)
						{
							if(i == 0)
								driveType = dto.getDrivetypeTCB()[i];
							else
								driveType = driveType+"~"+dto.getDrivetypeTCB()[i];
						}
					}
					else
					{
						driveType = dto.getDrivetype();
					}
					
					pst = conn.prepareStatement(INSERT_LICENSE_ENDORSEMENT_DETAILS);
					pst.setString(1, applicationNumber);
					pst.setString(2, "LICENSEENDORSE");
					pst.setString(3,dto.getLicensetype());
					pst.setString(4, null);
					pst.setString(5, null);
					pst.setString(6, dto.getCustomerId());
					pst.setString(7, dto.getLicenseNo());
					pst.setString(8, dto.getRegion());
					pst.setString(9, null);
					pst.setString(10, null);
					pst.setString(11, null);
					pst.setString(12, null);
					pst.setString(13, null);
					pst.setString(14, null);
					//pst.setString(15, dto.getDrivetype());
					pst.setString(15, driveType);
					pst.setString(16, null);
					pst.setString(17, dto.getRemarks());
					pst.setString(18, dto.getReceiptNo());
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDate1 = targetSdf.format(receiptDate);
					pst.setString(19, receiptDate1);
					pst.setString(20, null);
					pst.setString(21, userVo.getActorId());
					pst.setString(22, userVo.getActorId());
					pst.setString(23, dto.getDrivinglicenseId());
					pst.setString(24, dto.getTcbEndorsement());
					pst.setString(25, isRenewalProcess);
					pst.setString(26, dto.getRenewalDuration());
					int count = pst.executeUpdate();
				
					if(count > 0)
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);
						
						if(null!=dto.getSupportDoc())
						{
							DocumentVO documentVO = new DocumentVO();
							documentVO.setServiceName("LICENSE");
							documentVO.setFileContent(dto.getSupportDoc().getFileData());
							documentVO.setName(dto.getSupportDoc().getFileName());
							documentVO.setDocumentType("DL");
							CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
						}
						PaymentDTO paymentDTO = new PaymentDTO();
						paymentDTO.setApplicationNo(applicationNumber);
						paymentDTO.setServiceId(serviceid);
						paymentDTO.setApplicationType("ENDORSEMENT");
						paymentDTO.setAmount(dto.getAmount());
						paymentDTO.setPenalty("0");
						paymentDTO.setCreatedBy(userVo.getActorId());
						paymentDTO.setReceiptNo(dto.getReceiptNo());
						java.util.Date receipt = sourceSdf.parse(dto.getReceiptDate());
						String receiptDateStr = targetSdf.format(receipt);
						paymentDTO.setReceiptDate(receiptDateStr);
						result = CommonDAO.getInstance().insertPaymentDetails(paymentDTO, conn);
						
						//result = "SUCCESS";
					}
				}
				else
					result = "LICENSE_DUPLICATE_TRANSACTION";
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_renewal]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result+"#"+applicationNumber;
	}

	public synchronized String license_cancellation(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null,pst1 = null;
		String result = "FAILURE";
		ResultSet rs = null;
		String cancellationId	=	"";

		try 
		{
			if (conn != null) 
			{
				if(dto.getFormType().equalsIgnoreCase("DL"))
				{
					pst = conn.prepareStatement(INSERT_DRIVING_LICENSE_CANCELLATION_DETAILS, PreparedStatement.RETURN_GENERATED_KEYS);
				}
				else if(dto.getFormType().equalsIgnoreCase("LL"))
				{
					pst = conn.prepareStatement(INSERT_LEARNER_LICENSE_CANCELLATION_DETAILS, PreparedStatement.RETURN_GENERATED_KEYS);
				}
				pst.setString(1, dto.getCustomerId());
				if(dto.getFormType().equalsIgnoreCase("DL"))
				{
					pst.setString(2, dto.getDrivinglicenseId());
				}
				else if(dto.getFormType().equalsIgnoreCase("LL"))
				{
					pst.setString(2, dto.getLearnerLicenseId());
				}
				java.util.Date Cancellationdate = sourceSdf.parse(dto.getCancellationdate());
				String cancellationdate = targetSdf.format(Cancellationdate);
				pst.setString(3,cancellationdate );
				pst.setString(4,"C");
				pst.setString(5, dto.getCancellationReason());
				pst.setString(6,userVo.getActorName());
				//pst.setDate(8,date);
				//pst.setString(7,userVo.getActorName());
				int count=pst.executeUpdate();	
				
				if (count > 0)
				{
					String serviceName = null;
					if(dto.getFormType().equalsIgnoreCase("DL"))
					{
						pst1=conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_DTLS);
						pst1.setString(1,"C");
						pst1.setString(2,dto.getDrivinglicenseId());
						int count1 = pst1.executeUpdate();
						if (count1 > 0)
						{
							result = "SUCCESS";
							cancellationId = "LIC_CAN_"; 
							serviceName = "LICENSE_CANCELLATION";
						}
					}
					else if(dto.getFormType().equalsIgnoreCase("LL"))
					{
						pst1=conn.prepareStatement(UPDATE_CANCELLED_T_LEARNER_LICENSE_DTLS);
						pst1.setString(1,"C");
						pst1.setString(2,dto.getLearnerLicenseId());
						int count1 = pst1.executeUpdate();
						if (count1 > 0)
						{
							result = "SUCCESS";
							cancellationId = "LL_CAN_"; 
							serviceName = "LEARNER_CANCELLATION";
						}
					}
					if(result.equalsIgnoreCase("SUCCESS"))
					{
						rs = pst.getGeneratedKeys();
						while (rs.next()) {
							cancellationId = cancellationId+rs.getString(1);
						}
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName(serviceName);
						documentVO.setFileContent(dto.getImgPath().getFileData());
						documentVO.setName(dto.getImgPath().getFileName());
						documentVO.setDocumentType(serviceName);
						CommonDAO.getInstance().fileUploader(documentVO, cancellationId);
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_cancellation]:: " + e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
			ConnectionManager.close(null, null, null, pst1);
		}

		return result;
	}
	public synchronized String withdraw_license_cancellation(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		String licenseWithdrawId	=	"";
		String licenseId = null;
		String deleteQuery = "";
		String service = null;
		try 
		{
			if (conn != null) 
			{
				if(dto.getFormType().equalsIgnoreCase("DL"))
				{
					pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_CANCELLATION_AUDIT,PreparedStatement.RETURN_GENERATED_KEYS);
					licenseId = dto.getDrivinglicenseId();
					licenseWithdrawId = "LICENSE_WITHDRAW_";
					service = "LICENSE_WITHDRAW";
					deleteQuery = DELETE_DRIVING_LICENSE_CANCELLATION;
				}
				else if(dto.getFormType().equalsIgnoreCase("LL"))
				{
					pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_CANCELLATION_AUDIT,PreparedStatement.RETURN_GENERATED_KEYS);
					licenseId = dto.getLearnerLicenseId();
					licenseWithdrawId = "LEARNER_WITHDRAW_";
					service = "LEARNER_WITHDRAW";
					deleteQuery = DELETE_LEARNER_LICENSE_CANCELLATION;
				}
				java.util.Date Cancellationdate = sourceSdf.parse(dto.getWithdrawnDate());
				String cancellationdate = targetSdf.format(Cancellationdate);
				pst.setString(1, dto.getWithdrawnReason());
				pst.setString(2,cancellationdate ); 
				pst.setString(3, licenseId);
				
				int count = pst.executeUpdate();
				if(count>0)
				{
					rs = pst.getGeneratedKeys();
					
					while (rs.next()) {
						licenseWithdrawId = licenseWithdrawId+rs.getString(1);
					}
					
					DocumentVO documentVO = new DocumentVO();
					documentVO.setServiceName(service);
					documentVO.setFileContent(dto.getImgPath().getFileData());
					documentVO.setName(dto.getImgPath().getFileName());
					documentVO.setDocumentType(service);
					CommonDAO.getInstance().fileUploader(documentVO, licenseWithdrawId);
					
					pst=conn.prepareStatement(deleteQuery);
					pst.setString(1,licenseId);
					int count1 = pst.executeUpdate();
					if(count1>0)
					{
						result	=	"SUCCESS";
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_cancellation]:: " + e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}
	
	public String license_suspension(LicenseDTO dto, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";

		ResultSet rs = null;
		String suspensionId	=	"";
		DocumentVO documentVO = new DocumentVO();
		try {
			
			if(dto.getFormType().equalsIgnoreCase("DL"))
			{
				documentVO.setServiceName("LICENSE_SUSPENSION");
				documentVO.setDocumentType("LICENSE_SUSPENSION");
				suspensionId = "LIC_SUSPN_";
				
				pst = conn.prepareStatement(INSERT_LICENSE_SUSPENSION_DETAILS,PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, dto.getDrivinglicenseId());
			}
			else if(dto.getFormType().equalsIgnoreCase("LL"))
			{	
				documentVO.setServiceName("LEARNER_SUSPENSION");
				documentVO.setDocumentType("LEARNER_SUSPENSION");
				suspensionId ="LL_SUSPN_";
				
				pst = conn.prepareStatement(INSERT_LEARNER_LICENSE_SUSPENSION_DETAILS,PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, dto.getLearnerLicenseId());
			}
			
			pst.setString(2, dto.getCustomerId());
			java.util.Date startDate = sourceSdf.parse(dto.getStartDate());
			String startdate = targetSdf.format(startDate);
			pst.setString(3, startdate);
			java.util.Date endDate = sourceSdf.parse(dto.getEndDate());
			String enddate = targetSdf.format(endDate);
			pst.setString(4, enddate);
			pst.setString(5, dto.getReason());
			pst.setDate(6, date);
			pst.setString(7, null);
			pst.setDate(8, date);
			pst.setString(9, null);
			int count = pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			while (rs.next()) 
			{
				suspensionId = suspensionId + rs.getString(1);
			}
			
			/**ATTACHMENT CODE STARTS HERE**/
			documentVO.setFileContent(dto.getImgPath().getFileData());
			documentVO.setName(dto.getImgPath().getFileName());
			CommonDAO.getInstance().fileUploader(documentVO, suspensionId);
			if (count > 0)
				result = "SUCCESS";
		}

		catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_license_suspension]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;

	}

	public String license_drive_type_suspension(LicenseDTO dto, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		ResultSet rs = null;
		String suspensionId	=	"";
		try {
			pst = conn.prepareStatement(INSERT_DRIVE_TYPE_SUSPENSION_DETAILS, PreparedStatement.RETURN_GENERATED_KEYS);
			pst.setString(1, dto.getDrivinglicenseId());
			pst.setString(2, dto.getCustomerId());
			pst.setString(3, dto.getDrivetype());
			java.util.Date startDate = sourceSdf.parse(dto.getStartDate());
			String startdate = targetSdf.format(startDate);
			pst.setString(4, startdate);
			java.util.Date endDate = sourceSdf.parse(dto.getEndDate());
			String enddate = targetSdf.format(endDate);
			pst.setString(5, enddate);
			pst.setString(6, dto.getReason());
			/*java.util.Date reissueDate = sourceSdf.parse(dto.getReissuedate());
			String reissuedDate = targetSdf.format(reissueDate);*/
			pst.setString(7, enddate);
			pst.setString(8, null);
			pst.setDate(9, date);
			pst.setString(10, null);
			pst.setDate(11, date);
			pst.setString(12, null);
			int count = pst.executeUpdate();
			if (count > 0)
			{
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					suspensionId ="DRV_TYP_SUSPN_"+ rs.getString(1);
				}
				DocumentVO documentVO = new DocumentVO();
				documentVO.setServiceName("DRIVE_TYPE_SUSPENSION");
				documentVO.setFileContent(dto.getImgPath().getFileData());
				documentVO.setName(dto.getImgPath().getFileName());
				documentVO.setDocumentType("DRIVE_TYPE_SUSPENSION");
				CommonDAO.getInstance().fileUploader(documentVO, suspensionId);
				result = "SUCCESS";
			}
		}
		catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_drive_type_suspension]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return result;
	}

	public synchronized String[][] license_offence(LicenseDTO dto,String offenceFormType,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		String arrayList[][] = new String[4][5];
		NotificationDTO notifyDTO = new NotificationDTO();
		notifyDTO.setStatus("FAILURE");
		PreparedStatement pst = null;
		ResultSet rs = null;
		String offenceId = null;
		try
		{
			if(null!=offenceFormType && "F".equalsIgnoreCase(offenceFormType))
			{
				pst = conn.prepareStatement(INSERT_OFFENCE_INFORMATION_FOREIGN, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, offenceFormType);
				pst.setString(2, dto.getVehicleNoFor());
				if("F".equalsIgnoreCase(offenceFormType))
				{
					pst.setString(3, dto.getLicenseNoFor());
					pst.setString(4, dto.getMobileNoFor());
				}
				else
				{
					pst.setString(3, dto.getCID());
					pst.setString(4, dto.getMobile());
				}
				pst.setString(5, dto.getVehicleType());
				pst.setString(6, dto.getOwnerNameFor());

				if("F".equalsIgnoreCase(offenceFormType))
				{
					pst.setString(7, dto.getDiverNameFor());
				}
				else
				{
					pst.setString(7, dto.getName());
				}
				
				String OffenceDate = null;
				java.util.Date offenceDate = sourceSdf.parse(dto.getOffencedateFor());
				OffenceDate = targetSdf.format(offenceDate);
				pst.setString(8, OffenceDate);
				pst.setString(9, dto.getTimeofInspectionFor());
				pst.setString(10, dto.getInspectedbyFor());
				pst.setString(11, dto.getInspectedTypeFor());
				pst.setString(12, dto.getTrafficbranchFor());
				pst.setString(13, dto.getPlaceofInspectionFor());
				pst.setString(14, dto.getRegionfor());
				if("on".equals(dto.getLearnerlicense()))
					pst.setString(15, "Y");
				else
					pst.setString(15, "N");
				
				if("on".equals(dto.getBluebook()))
					pst.setString(16, "Y");
				else
					pst.setString(16, "N");
				pst.setString(17, dto.getTINnoFor());
				pst.setString(18,dto.getRemarksFor());
				pst.setString(19, userVo.getActorName());
				pst.setString(20, userVo.getRegionId());
				pst.setString(21, userVo.getBaseId());
				pst.setString(22, dto.getAddress());
				int count = pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				while (rs.next())
				{
					offenceId = rs.getString(1);
				}
				if (count > 0) 
				{
					for(OffenceDTO offence : dto.getOffenceListF())
					{
						String[] offenceArray = offence.getOffenceId().split("_");
						pst = conn.prepareStatement(INSERT_INTO_OFFENCE_DETAILS);
						pst.setString(1, offenceId);
						pst.setString(2, offenceArray[0]);
						pst.executeUpdate();
					}
					
					if(null != dto.getOffence5() && dto.getOffence5().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence5().getFileData());
						documentVO.setName(dto.getOffence5().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					if(null != dto.getOffence6() && dto.getOffence6().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence6().getFileData());
						documentVO.setName(dto.getOffence6().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					arrayList[0][0] = "";
					arrayList[0][1] = "";
					arrayList[0][2] = "";
					arrayList[0][3] = "";
					arrayList[0][4] = dto.getTINno();
					arrayList[3][0] = "SUCCESS";
				}
			}
			else if(null!=offenceFormType && "P".equalsIgnoreCase(offenceFormType))
			{
				pst = conn.prepareStatement(INSERT_OFFENCE_INFORMATION_FOREIGN, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, offenceFormType);
				pst.setString(2, dto.getVehicleNoP());
				if("F".equalsIgnoreCase(offenceFormType))
				{
					pst.setString(3, dto.getLicenseNoP());
					pst.setString(4, dto.getMobileNoP());
				}
				else
				{
					pst.setString(3, dto.getCID());
					pst.setString(4, dto.getMobile());
				}
				pst.setString(5, dto.getVehicleType());
				pst.setString(6, dto.getOwnerNameP());

				if("F".equalsIgnoreCase(offenceFormType))
				{
					pst.setString(7, dto.getDiverNameP());
				}
				else
				{
					pst.setString(7, dto.getName());
				}
				
				String OffenceDate = null;
				java.util.Date offenceDate = sourceSdf.parse(dto.getOffencedateP());
				OffenceDate = targetSdf.format(offenceDate);
				pst.setString(8, OffenceDate);
				pst.setString(9, dto.getTimeofInspectionP());
				pst.setString(10, dto.getInspectedbyP());
				pst.setString(11, dto.getInspectedTypeP());
				pst.setString(12, dto.getTrafficbranchP());
				pst.setString(13, dto.getPlaceofInspectionP());
				pst.setString(14, dto.getRegionP());
				if("on".equals(dto.getLearnerlicense()))
					pst.setString(15, "Y");
				else
					pst.setString(15, "N");
				
				if("on".equals(dto.getBluebook()))
					pst.setString(16, "Y");
				else
					pst.setString(16, "N");
				pst.setString(17, dto.getTINnoP());
				pst.setString(18,dto.getRemarksP());
				pst.setString(19, userVo.getActorName());
				pst.setString(20, userVo.getRegionId());
				pst.setString(21, userVo.getBaseId());
				pst.setString(22, dto.getAddress());
				int count = pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				while (rs.next())
				{
					offenceId = rs.getString(1);
				}
				if (count > 0) 
				{
					for(OffenceDTO offence : dto.getOffenceList())
					{
						String[] offenceArray = offence.getOffenceId().split("_");
						pst = conn.prepareStatement(INSERT_INTO_OFFENCE_DETAILS);
						pst.setString(1, offenceId);
						pst.setString(2, offenceArray[0]);
						pst.executeUpdate();
					}
					
					if(null != dto.getOffence1() && dto.getOffence1().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence1().getFileData());
						documentVO.setName(dto.getOffence1().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					if(null != dto.getOffence2() && dto.getOffence2().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence2().getFileData());
						documentVO.setName(dto.getOffence2().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					arrayList[0][0] = "";
					arrayList[0][1] = "";
					arrayList[0][2] = "";
					arrayList[0][3] = "";
					arrayList[0][4] = dto.getTINno();
					arrayList[3][0] = "SUCCESS";
				}
			}
			else if(null!=offenceFormType && "B".equalsIgnoreCase(offenceFormType))
			{
				pst = conn.prepareStatement(INSERT_OFFENCE_INFORMATION, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, offenceFormType);
				pst.setString(2, dto.getVehicleId());
				pst.setString(3, dto.getLearnerLicenseId());
				pst.setString(4, dto.getDrivinglicenseId());
				java.util.Date offenceDate = sourceSdf.parse(dto.getOffencedate());
				String OffenceDate = targetSdf.format(offenceDate);
				pst.setString(5, OffenceDate);
				pst.setString(6, dto.getTimeofInspection());
				pst.setString(7, dto.getInspectedby());
				pst.setString(8, dto.getInspectedType());
				pst.setString(9, dto.getTrafficbranch());
				pst.setString(10, dto.getPlaceofInspection());
				pst.setString(11, dto.getRegion());
				if("on".equals(dto.getLearnerlicense()))
					pst.setString(12, "Y");
				else
					pst.setString(12, "N");
				
				if("on".equals(dto.getBluebook()))
					pst.setString(13, "Y");
				else
					pst.setString(13, "N");
				pst.setString(14, dto.getTINno());
				pst.setString(15,dto.getRemarks());
				pst.setString(16, userVo.getActorName());
				pst.setString(17, userVo.getRegionId());
				pst.setString(18, userVo.getBaseId());
				if(dto.getCustomerId()!=null && !dto.getCustomerId().equalsIgnoreCase(""))
				{
					pst.setString(19, "Y");
				}
				else
				{
					pst.setString(19, "N");
				}
				pst.setString(20, dto.getCustomerId());
				int count = pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					offenceId = rs.getString(1);
				}
				if (count > 0) 
				{
					for(OffenceDTO offence : dto.getOffenceListB())
					{
						String[] offenceArray = offence.getOffenceId().split("_");
						pst = conn.prepareStatement(INSERT_INTO_OFFENCE_DETAILS);
						pst.setString(1, offenceId);
						pst.setString(2, offenceArray[0]);
						pst.executeUpdate();
					}
					
					if(null != dto.getOffence3() && dto.getOffence3().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence3().getFileData());
						documentVO.setName(dto.getOffence3().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					if(null != dto.getOffence4() && dto.getOffence4().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence4().getFileData());
						documentVO.setName(dto.getOffence4().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					pst = conn.prepareStatement(GET_VEHICLE_REGISTRATION_TYPE);
					pst.setString(1, dto.getVehicleId());
					rs = pst.executeQuery();
					rs.first();
					if(rs.getString("Vehicle_Registration_Type").equals("P")){
						pst = conn.prepareStatement(GET_OWNER_PHONE_NUMBER);
					}
					else {
						pst = conn.prepareStatement(GET_ORGANISATION_CONTACT_DTLS);
					}
						pst.setString(1, dto.getVehicleId());
						rs = pst.executeQuery();
						rs.first();
						arrayList[0][0] = rs.getString("pname");
						arrayList[0][1] = rs.getString("currentDate");
						arrayList[0][2] = rs.getString("Present_Email");
						arrayList[0][3] = rs.getString("Present_Phone_No");
						arrayList[0][4] = dto.getTINno();
						
					if(dto.getDrivinglicenseId()!=null  && !dto.getDrivinglicenseId().equalsIgnoreCase("0"))
					{
						pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FROM_DRIVING_LICENSE);
						pst.setString(1, dto.getDrivinglicenseId());
						rs = pst.executeQuery();
						rs.first();
						arrayList[1][0] = rs.getString("pname");
						arrayList[1][1] = rs.getString("currentDate");
						arrayList[1][2] = rs.getString("Present_Email");
						arrayList[1][3] = rs.getString("Present_Phone_No");
						arrayList[1][4] = dto.getTINno();
					}
					else if(dto.getLearnerLicenseId()!=null && !dto.getLearnerLicenseId().equalsIgnoreCase("0"))
					{
						pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FROM_LEARNER_LICENSE);
						pst.setString(1, dto.getLearnerLicenseId());
						rs = pst.executeQuery();
						rs.first();
						
						arrayList[2][0] = rs.getString("pname");
						arrayList[2][1] = rs.getString("currentDate");
						arrayList[2][2] = rs.getString("Present_Email");
						arrayList[2][3] = rs.getString("Present_Phone_No");
						arrayList[2][4] = dto.getTINno();
					}
						arrayList[3][0] = "SUCCESS";
				}
			}
			/*else if(null!=offenceFormType && "P".equalsIgnoreCase(offenceFormType))
			{
				pst = conn.prepareStatement(INSERT_OFFENCE_INFORMATION, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, "N");
				pst.setString(2, dto.getVehicleId());
				pst.setString(3, dto.getLearnerLicenseId());
				pst.setString(4, dto.getDrivinglicenseId());
				java.util.Date offenceDate = sourceSdf.parse(dto.getOffencedate());
				String OffenceDate = targetSdf.format(offenceDate);
				pst.setString(5, OffenceDate);
				pst.setString(6, dto.getTimeofInspection());
				pst.setString(7, dto.getInspectedby());
				pst.setString(8, dto.getInspectedType());
				pst.setString(9, dto.getTrafficbranch());
				pst.setString(10, dto.getPlaceofInspection());
				pst.setString(11, dto.getRegion());
				if("on".equals(dto.getLearnerlicense()))
					pst.setString(12, "Y");
				else
					pst.setString(12, "N");
				
				if("on".equals(dto.getBluebook()))
					pst.setString(13, "Y");
				else
					pst.setString(13, "N");
				pst.setString(14, dto.getTINno());
				pst.setString(15,dto.getRemarks());
				pst.setString(16, userVo.getActorName());
				pst.setString(17, userVo.getRegionId());
				pst.setString(18, userVo.getBaseId());
				if(dto.getCustomerId()!=null && !dto.getCustomerId().equalsIgnoreCase(""))
				{
					pst.setString(19, "Y");
				}
				else
				{
					pst.setString(19, "N");
				}
				pst.setString(20, dto.getCustomerId());
				int count = pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				while (rs.next()) {
					offenceId = rs.getString(1);
				}
				if (count > 0) 
				{
					for(OffenceDTO offence : dto.getOffenceList())
					{
						String[] offenceArray = offence.getOffenceId().split("_");
						pst = conn.prepareStatement(INSERT_INTO_OFFENCE_DETAILS);
						pst.setString(1, offenceId);
						pst.setString(2, offenceArray[0]);
						pst.executeUpdate();
					}
					
					if(null != dto.getOffence1() && dto.getOffence1().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence1().getFileData());
						documentVO.setName(dto.getOffence1().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					if(null != dto.getOffence2() && dto.getOffence2().getFileSize() > 0)
					{
						DocumentVO documentVO = new DocumentVO();
						documentVO.setServiceName("OFFENCE");
						documentVO.setFileContent(dto.getOffence2().getFileData());
						documentVO.setName(dto.getOffence2().getFileName());
						documentVO.setDocumentType("OFFENCE");
						CommonDAO.getInstance().fileUploader(documentVO, offenceId);
					}
					
					pst = conn.prepareStatement(GET_VEHICLE_REGISTRATION_TYPE);
					pst.setString(1, dto.getVehicleId());
					rs = pst.executeQuery();
					rs.first();
					if(rs.getString("Vehicle_Registration_Type").equals("P")){
						pst = conn.prepareStatement(GET_OWNER_PHONE_NUMBER);
					}
					else {
						pst = conn.prepareStatement(GET_ORGANISATION_CONTACT_DTLS);
					}
						pst.setString(1, dto.getVehicleId());
						rs = pst.executeQuery();
						rs.first();
						arrayList[0][0] = rs.getString("pname");
						arrayList[0][1] = rs.getString("currentDate");
						arrayList[0][2] = rs.getString("Present_Email");
						arrayList[0][3] = rs.getString("Present_Phone_No");
						arrayList[0][4] = dto.getTINno();
						
					if(dto.getDrivinglicenseId()!=null  && !dto.getDrivinglicenseId().equalsIgnoreCase("0"))
					{
						pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FROM_DRIVING_LICENSE);
						pst.setString(1, dto.getDrivinglicenseId());
						rs = pst.executeQuery();
						rs.first();
						arrayList[1][0] = rs.getString("pname");
						arrayList[1][1] = rs.getString("currentDate");
						arrayList[1][2] = rs.getString("Present_Email");
						arrayList[1][3] = rs.getString("Present_Phone_No");
						arrayList[1][4] = dto.getTINno();
					}
					else if(dto.getLearnerLicenseId()!=null && !dto.getLearnerLicenseId().equalsIgnoreCase("0"))
					{
						pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FROM_LEARNER_LICENSE);
						pst.setString(1, dto.getLearnerLicenseId());
						rs = pst.executeQuery();
						rs.first();
						
						arrayList[2][0] = rs.getString("pname");
						arrayList[2][1] = rs.getString("currentDate");
						arrayList[2][2] = rs.getString("Present_Email");
						arrayList[2][3] = rs.getString("Present_Phone_No");
						arrayList[2][4] = dto.getTINno();
					}
						arrayList[3][0] = "SUCCESS";
				}
			}*/
		}
		catch (Exception e) {
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_offence]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return arrayList;
	}

	public synchronized NotificationDTO license_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		notityDTO.setStatus("FAILURE");
		
		try {
			if (conn != null) 
			{
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_DTLS);
					pst.setString(1, userVo.getActorId());
					pst.setString(2, dto.getApplicationNo());
					int count = pst.executeUpdate();
					if (count > 0) 
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("101");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
						
						
			
					}
					String learnerlicenseNo=EralisCommonUtil.generateLearnerLicenseNoFormat(conn, dto.getRegionId());
					pst1=conn.prepareStatement(UPDATE_T_LEARNER_LICENSE_DTLS);
					pst1.setString(1,learnerlicenseNo);
					pst1.setString(2,dto.getApplicationNo());
					int count1 = pst1.executeUpdate();
					if (count1 > 0)
					{
						pst=conn.prepareStatement(UPDATE_LEARNER_LICENSE_TABLE);
						pst.setDate(1,date);
						java.util.Date expiry = sourceSdf.parse(dto.getExpiryDate());
						String expiryDate = targetSdf.format(expiry);
						pst.setString(2,expiryDate);
						pst.setString(3,userVo.getBaseId());
						pst.setString(4,userVo.getRegionId());
						pst.setString(5, dto.getApplicationNo());
						int count2=pst.executeUpdate();
						
						if (count2 > 0)
						{
							
							pst= conn.prepareStatement("UPDATE t_learner_license_application SET Learner_License_No = ? WHERE Application_Number=?");
							pst.setString(1, learnerlicenseNo);
							pst.setString(2, dto.getApplicationNo());
							pst.executeUpdate();
							
							pst= conn.prepareStatement(INSERT_LEARNER_LICENSE_DRIVETYPE);
							pst.setString(1, dto.getApplicationNo());
							pst.setString(2, dto.getApplicationNo());
							pst.executeUpdate();
							
							/*pst=conn.prepareStatement(INSERT_INTO_T_PRINT_DTLS);
							pst.setString(1,dto.getApplicationNo());
							pst.executeUpdate();*/
							
							pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION);
							pst.setString(1, dto.getApplicationNo());
							rs = pst.executeQuery();
							rs.first();
							
							notityDTO.setApplicationNo(dto.getApplicationNo());
							notityDTO.setCustomerName(rs.getString("pname"));
							notityDTO.setApprovalDate(rs.getString("currentDate"));
							notityDTO.setEmailAddress(rs.getString("Present_Email"));
							notityDTO.setMobileNumber(rs.getString("Present_Phone_No"));
							notityDTO.setLicenseNo(learnerlicenseNo);
							notityDTO.setStatus("SUCCESS");
							
						}
					}
				}
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_application_approval]:: "
					+ e);
		} 
		finally 
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notityDTO;
	}
	
	public synchronized NotificationDTO license_renewal_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notityDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		notityDTO.setStatus("FAILURE");
		
		try {
			
			if (conn != null)
			{
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_RENEWAL_DTLS);
					pst.setString(1, dto.getApplicationNo());
					int count = pst.executeUpdate();
			
					if (count > 0)
					{
						pst=conn.prepareStatement(UPDATE_LEARNER_LICENSE_RENEWAL_DTLS);
						
						pst.setDate(1, date);
						pst.setDate(2, date);
						java.util.Date nextexpiry = sourceSdf.parse(dto.getNextExpiry());
						String nextExpiry = targetSdf.format(nextexpiry);
						pst.setString(3, nextExpiry);
						pst.setString(4, userVo.getBaseId());
						pst.setString(5, userVo.getRegionId());
						pst.setString(6, dto.getApplicationNo());
						int count1=pst.executeUpdate();
						if (count1 > 0)
						{
							pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_RENEWAL);
							pst.setString(1, dto.getApplicationNo());
							rs = pst.executeQuery();
							rs.first();
							
							notityDTO.setApplicationNo(dto.getApplicationNo());
							notityDTO.setCustomerName(rs.getString("pname"));
							notityDTO.setApprovalDate(rs.getString("currentDate"));
							notityDTO.setEmailAddress(rs.getString("Present_Email"));
							notityDTO.setMobileNumber(rs.getString("Present_Phone_No"));
							notityDTO.setStatus("SUCCESS");
							
							WorkflowDetailsVO vo = new WorkflowDetailsVO();
							vo.setActorId(userVo.getActorId());
							vo.setActorName(userVo.getActorName());
							vo.setRoleId(userVo.getRoleId());
							vo.setRoleName(userVo.getRoleName());
							vo.setApplicationNo(dto.getApplicationNo());
							vo.setServiceId("102");
							vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
							vo.setJurisId(userVo.getJurisdictionId());
							vo.setJurisTypeId(userVo.getJurisdictionTypeId());
							new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
							new WorkflowManager(conn).logCompletedWorkflow(vo);
						}
					}
				}
			 }
		} catch (Exception e) {
			
			notityDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		return notityDTO;
	}
	
	public synchronized NotificationDTO non_commercial_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		notifyDTO.setStatus("FAILURE");
		String licenseId = null;
		
		
		try 
		{
			if (conn != null) 
			{	
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					int lastTwoDigits = Calendar.getInstance().get(Calendar.YEAR) % 100;
					pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_NONCOMMERCIAL_DTLS, PreparedStatement.RETURN_GENERATED_KEYS);
					pst.setString(1, dto.getApplicationNo());
					int count = pst.executeUpdate();
					rs = pst.getGeneratedKeys();
					while (rs.next())
					{
						licenseId = rs.getString(1);
					}
					if (count > 0) 
					{
						pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_DRIVE_TYPE);
						pst.setString(1, dto.getApplicationNo());
						count = pst.executeUpdate();
						if (count > 0)
						{
							pst = conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_DRIVE_TYPE);
							pst.setString(1, licenseId);
							pst.setString(2, dto.getApplicationNo());
							count = pst.executeUpdate();
							if (count > 0)
							{
								String noncommercialLicenseNo=EralisCommonUtil.generateDrivingLicenseNoFormat(conn, dto.getRegionId(),"t_driving_license_dtls");
	
								pst=conn.prepareStatement(UPDATE_T_NONCOMMERCIAL_LICENSE_DTLS);
								pst.setString(1, noncommercialLicenseNo);
								pst.setString(2, "A");
								//.setInt(3, lastTwoDigits); 
								java.util.Date expiry = sourceSdf.parse(dto.getExpiryDate());
								String Expiry = targetSdf.format(expiry);
								pst.setString(3, Expiry);
								pst.setString(4,userVo.getBaseId());
								pst.setString(5,userVo.getRegionId());
								pst.setString(6,dto.getApplicationNo());
								count = pst.executeUpdate();
								if (count > 0)
								{
									pst=conn.prepareStatement(UPDATE_T_NONCOMMERCIAL_LICENSE_TEST_MARKS_DTLS);
									pst.setString(1, dto.getLearnerlicenseNo());
									count = pst.executeUpdate();
									if (count > 0)
									{
										pst=conn.prepareStatement(INSERT_NON_COMMERCIAL_PRINT_DTLS);
										pst.setString(1, dto.getApplicationNo());
										count = pst.executeUpdate();
										if (count > 0)
										{
											pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_NON_COMMERCIAL);
											pst.setString(1, dto.getApplicationNo());
											rs = pst.executeQuery();
											rs.first();
											
											notifyDTO.setApplicationNo(dto.getApplicationNo());
											notifyDTO.setCustomerName(rs.getString("pname"));
											notifyDTO.setApprovalDate(rs.getString("currentDate"));
											notifyDTO.setEmailAddress(rs.getString("Present_Email"));
											notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
											notifyDTO.setLicenseNo(noncommercialLicenseNo);
											
											notifyDTO.setStatus("SUCCESS");
											
											WorkflowDetailsVO vo = new WorkflowDetailsVO();
											vo.setActorId(userVo.getActorId());
											vo.setActorName(userVo.getActorName());
											vo.setRoleId(userVo.getRoleId());
											vo.setRoleName(userVo.getRoleName());
											vo.setApplicationNo(dto.getApplicationNo());
											vo.setServiceId("104");
											vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
											vo.setJurisId(userVo.getJurisdictionId());
											vo.setJurisTypeId(userVo.getJurisdictionTypeId());
											//new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
											new WorkflowManager(conn).logDrivingLicenseApprovalCompletedWorkflow(vo);
											
										}	
									}
								}
							}
						}
					}
				}
			 }
		} catch (Exception e) {
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[non_commercial_application_approval]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
	}
	
	public synchronized NotificationDTO commercial_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String licenseId = null;
		notifyDTO.setStatus("FAILURE");
		
		
		try {
			if (conn != null) 
			{	
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					String applicationDriveType	=	"";
					pst = conn.prepareStatement(CHECK_DRIVE_TYPE_FROM_LICENSE_APPLICATION);
					pst.setString(1, dto.getApplicationNo());
					rs = pst.executeQuery();
					rs.first();
					applicationDriveType = rs.getString("Description");
					
					if(applicationDriveType.equalsIgnoreCase("TAXI") || applicationDriveType.equalsIgnoreCase("MEDIUM_BUS") || applicationDriveType.equalsIgnoreCase("HEAVY_BUS"))
					{
						//int lastTwoDigits = Calendar.getInstance().get(Calendar.YEAR) % 100;
						
						pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_COMMERCIAL_DTLS,PreparedStatement.RETURN_GENERATED_KEYS);
						pst.setString(1, dto.getApplicationNo());
						int count = pst.executeUpdate();
						
						rs = pst.getGeneratedKeys();
						while (rs.next()) 
						{
							licenseId = rs.getString(1);
						}
						if (count > 0) 
						{
							
							pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_DRIVE_TYPE);
							pst.setString(1, dto.getApplicationNo());
							count = pst.executeUpdate();
							if (count > 0) 
							{
								pst = conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_DRIVE_TYPE);
								pst.setString(1, licenseId);
								pst.setString(2, dto.getApplicationNo());
								count = pst.executeUpdate();
								if (count > 0) 
								{
									String commercialLicenseNo = EralisCommonUtil.generateCommercialLicenseNoFormat(conn, "t_driving_license_dtls");
									pst=conn.prepareStatement(UPDATE_NON_COMMERCIAL_DRIVING_LICENSE_STATUS);
									pst.setString(1,dto.getDrivinglicenseId());
									count = pst.executeUpdate();
									if (count > 0) 
									{
										pst=conn.prepareStatement(UPDATE_T_COMMERCIAL_DRIVING_LICENSE_DTLS);
										pst.setString(1,commercialLicenseNo);
										pst.setString(2,"A");
										//pst.setDate(3, date);
										java.util.Date expiry = sourceSdf.parse(dto.getExpiryDate());
										String Expiry = targetSdf.format(expiry);
										pst.setString(3, Expiry);
										//pst.setInt(4, lastTwoDigits);
										pst.setString(4,dto.getApplicationNo());
										count = pst.executeUpdate();
										if (count > 0) 
										{
											pst=conn.prepareStatement(INSERT_COMMERCIAL_PRINT_DTLS);
											pst.setString(1, dto.getApplicationNo());
											count = pst.executeUpdate();
											if (count > 0) 
											{
												pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_NON_COMMERCIAL);
												pst.setString(1, dto.getApplicationNo());
												rs = pst.executeQuery();
												rs.first();
												notifyDTO.setApplicationNo(dto.getApplicationNo());
												notifyDTO.setCustomerName(rs.getString("pname"));
												notifyDTO.setApprovalDate(rs.getString("currentDate"));
												notifyDTO.setEmailAddress(rs.getString("Present_Email"));
												notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
												notifyDTO.setLicenseNo(commercialLicenseNo);
												
												notifyDTO.setStatus("SUCCESS");
												
												WorkflowDetailsVO vo = new WorkflowDetailsVO();
												vo.setActorId(userVo.getActorId());
												vo.setActorName(userVo.getActorName());
												vo.setRoleId(userVo.getRoleId());
												vo.setRoleName(userVo.getRoleName());
												vo.setApplicationNo(dto.getApplicationNo());
												vo.setServiceId("105");
												vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
												vo.setJurisId(userVo.getJurisdictionId());
												vo.setJurisTypeId(userVo.getJurisdictionTypeId());
												new WorkflowManager(conn).logDrivingLicenseApprovalCompletedWorkflow(vo);
											}
										}
									}
								}
							}
						}
					}
					else
					{ 
						driving_license_endorsement_application_approval(dto, userVo, conn);
						notifyDTO.setStatus("SUCCESS");
					}
				 }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[commercial_application_approval]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
	}
	public synchronized NotificationDTO driving_license_renewal_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null,pst1 = null, pst2 = null, pst3 = null;
		ResultSet rs = null, rs1 = null, rs2 = null;
		notifyDTO.setStatus("FAILURE");
		
		try {
			if (conn != null) 
			{
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_RENEWAL_DTLS);
					pst.setString(1, userVo.getActorId());
					pst.setString(2, dto.getApplicationNo());
					int count = pst.executeUpdate();
					
					if (count > 0) 
					{
						pst = conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_RENEWAL_DTLS);
						java.util.Date expiry = sourceSdf.parse(dto.getNextExpiry());
						String Expiry = targetSdf.format(expiry);
						pst.setString(1, Expiry);
						pst.setDate(2,date);
						pst.setString(3, userVo.getBaseId());
						pst.setString(4, userVo.getRegionId());
						pst.setString(5, dto.getApplicationNo());
						int count1 = pst.executeUpdate();
						
						if (count1 > 0)
						{
							pst1 = conn.prepareStatement(GET_LICENSE_TYPE);
							pst1.setString(1, dto.getApplicationNo());
							rs1 = pst1.executeQuery();
							rs1.first();
							String customerId = rs1.getString("Customer_Id");
							String licenseId = rs1.getString("License_Id");
							String licenseType = rs1.getString("Driving_License_Type_Id");
							
							if(licenseType.equals("C"))
							{
								pst2 = conn.prepareStatement(GET_ORDINARY_LICENSE_ID);
								pst2.setString(1, customerId);
								pst2.setString(2, licenseId);
								rs2 = pst2.executeQuery();
								//rs2.first();
								while(rs2.next())
								{
									String ordinaryLicenseId = rs2.getString("Driving_License_Id");
									
									pst = conn.prepareStatement(GET_MAX_EXPIRY_FOR_LICENSE);
									pst.setString(1, ordinaryLicenseId);
									pst.setString(2, ordinaryLicenseId);
									pst.setString(3, ordinaryLicenseId);
									rs = pst.executeQuery();
									rs.first();
									java.util.Date ordinaryExpiryDate = rs.getDate("expiryDate");
									java.util.Date nextCommercialExpiryDate = targetSdf.parse(Expiry);
									int dayDiff = nextCommercialExpiryDate.compareTo(ordinaryExpiryDate);
									
									if(dayDiff == 1)
									{
										pst3 = conn.prepareStatement(AUTO_INSERT_ORDINARY_LICENSE_RENEWAL_DETAILS);
										pst3.setString(1, "SYSTEM_UPDATED");
										pst3.setString(2, customerId);
										pst3.setString(3, ordinaryLicenseId);
										pst3.setDate(4, date);
										pst3.setString(5, Expiry);
										pst3.setString(6, "AUTO INSERTED BY SYSTEM UPON COMMERCIAL LICENSE RENEWAL");
										pst3.setDate(7, date);
										pst3.setString(8, userVo.getActorId());
										pst3.setString(9, dto.getApplicationNo());
										pst3.setString(10, dto.getApplicationNo());
										count = pst3.executeUpdate();
									}
								}
							}
							//IF STATUS = Y THEN IT IS ENDORSEMENT AND RENEW CASE ELSE ONLY RENEW CASE 
							if(null==dto.getStatus())
							{
								pst = conn.prepareStatement(INSERT_DRIVING_RENEWAL_PRINT_DTLS);
								pst.setString(1,dto.getApplicationNo());
								count = pst.executeUpdate();
								if (count > 0) 
								{
									pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_DRIVING_LICENSE_RENEWAL);
									pst.setString(1, dto.getApplicationNo());
									rs = pst.executeQuery();
									rs.first();
									
									notifyDTO.setApplicationNo(dto.getApplicationNo());
									notifyDTO.setCustomerName(rs.getString("pname"));
									notifyDTO.setApprovalDate(rs.getString("currentDate"));
									notifyDTO.setEmailAddress(rs.getString("Present_Email"));
									notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
									notifyDTO.setStatus("SUCCESS");
								
									WorkflowDetailsVO vo = new WorkflowDetailsVO();
									vo.setActorId(userVo.getActorId());
									vo.setActorName(userVo.getActorName());
									vo.setRoleId(userVo.getRoleId());
									vo.setRoleName(userVo.getRoleName());
									vo.setApplicationNo(dto.getApplicationNo());
									vo.setServiceId("106");
									vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
									vo.setJurisId(userVo.getJurisdictionId());
									vo.setJurisTypeId(userVo.getJurisdictionTypeId());
									new WorkflowManager(conn).logDrivingLicenseApprovalCompletedWorkflow(vo);	
								}
							}
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[driving_license_renewal_application_approval]:: "+ e);
		} 
		finally 
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
	}
	
	public synchronized NotificationDTO driving_license_endorsement_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String endorsementId = null;
		notifyDTO.setStatus("FAILURE");
		try
		{
			if (conn != null) 
			{
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					int lastTwoDigits = Calendar.getInstance().get(Calendar.YEAR) % 100;
					
					pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_ENDORSEMENT_DTLS, PreparedStatement.RETURN_GENERATED_KEYS);
					pst.setString(1, dto.getApplicationNo());
					int count = pst.executeUpdate();
					rs = pst.getGeneratedKeys();
					while (rs.next()) {
						endorsementId = rs.getString(1);
					}
					
					if (count > 0) 
					{
						pst = conn.prepareStatement(GET_ENDORSEMENT_APPLICATION_DETAILS);
						pst.setString(1, dto.getApplicationNo());
						rs = pst.executeQuery();
						rs.first();
						
						String licenseId = rs.getString("License_Id");
						String customerId = rs.getString("Customer_Id");
						String driveType = rs.getString("Drive_Type_Id");
						String isTCBEndorsement = rs.getString("Is_TCB_Endorsement");
						
						if(isTCBEndorsement.equals("N"))
						{
							pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_DRIVE_FOR_ENDORSEMENT);
							pst.setString(1, dto.getApplicationNo());
							count = pst.executeUpdate();
						}
						else
						{
							String[] driveTypeList = driveType.split("~");
							for(int i=0; i<driveTypeList.length;i++)
							{
								pst = conn.prepareStatement(GET_DRIVE_TYPE_CATEGORY);
								pst.setString(1, driveTypeList[i]);
								rs = pst.executeQuery();
								rs.first();
								
								pst = conn.prepareStatement(INSERT_INTO_DRIVE_TYPE_FOR_TCB_ENDORSEMENT);
								pst.setString(1, dto.getApplicationNo());
								pst.setString(2, customerId);
								pst.setString(3, licenseId);
								pst.setString(4, driveTypeList[i]);
								pst.setString(5, rs.getString("Drive_Type_Category"));
								count = pst.executeUpdate();
							}
						}
						
						if (count > 0) 
						{
							pst = conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_DRIVE_TYPE_WITH_ENDORSEMENT_ID);
							pst.setString(1, endorsementId);
							pst.setString(2, dto.getApplicationNo());
							count = pst.executeUpdate();
							if (count > 0) 
							{
								pst = conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_ENDORSEMENT_DTLS);
								java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
								String receiptDateStr = targetSdf.format(receiptDate);
								pst.setString(1, receiptDateStr);
								pst.setInt(2,lastTwoDigits);
								pst.setString(3, userVo.getBaseId());
								pst.setString(4, userVo.getRegionId());
								pst.setString(5, dto.getApplicationNo());
								count = pst.executeUpdate();
								if (count > 0) 
								{
									pst=conn.prepareStatement(INSERT_ENDORSEMENT_PRINT_DTLS);
									pst.setString(1, dto.getApplicationNo());
									pst.executeUpdate();
									
									pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_DRIVING_LICENSE_ENDORSEMENT);
									pst.setString(1, dto.getApplicationNo());
									rs = pst.executeQuery();
									rs.first();
									
									notifyDTO.setApplicationNo(dto.getApplicationNo());
									notifyDTO.setCustomerName(rs.getString("pname"));
									notifyDTO.setApprovalDate(rs.getString("currentDate"));
									notifyDTO.setEmailAddress(rs.getString("Present_Email"));
									notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
									notifyDTO.setStatus("SUCCESS");
									
									if(null!=dto.getStatus() && dto.getStatus().equalsIgnoreCase("Y"))
									{
										driving_license_renewal_application_approval(dto,userVo,conn);
									}
									
									
									
									WorkflowDetailsVO vo = new WorkflowDetailsVO();
									vo.setActorId(userVo.getActorId());
									vo.setActorName(userVo.getActorName());
									vo.setRoleId(userVo.getRoleId());
									vo.setRoleName(userVo.getRoleName());
									vo.setApplicationNo(dto.getApplicationNo());
									vo.setServiceId("108");
									vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
									vo.setJurisId(userVo.getJurisdictionId());
									vo.setJurisTypeId(userVo.getJurisdictionTypeId());
									//new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
									new WorkflowManager(conn).logDrivingLicenseApprovalCompletedWorkflow(vo);
								}
							}
						}
					}
				}
			 }
			
		} catch (Exception e) {
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
		}
	
	public synchronized String license_application_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		try 
		{
			if (conn != null) 
			{	
				String serviceId	=	dto.getServiceId();
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNo());
				vo.setServiceId(serviceId);
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(userVo.getJurisdictionId());
				vo.setJurisTypeId(userVo.getJurisdictionTypeId());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				
				pst=conn.prepareStatement(UPDATE_REJECTION_REASON);
				pst.setString(1,dto.getReason());
				pst.setString(2,dto.getApplicationNo());
				int count1 = pst.executeUpdate();
				if (count1 > 0)
				{
					result = "SUCCESS";
				}
			}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_application_reject]:: "+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	
	public synchronized String commercial_application_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("105");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logRejectionWorkflow(vo);
						
						result = "SUCCESS";
					
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[non_commercial_application_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	
	public synchronized String driving_license_renewal_application_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("106");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logRejectionWorkflow(vo);
						
						result = "SUCCESS";
					
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[driving_license_renewal_application_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	
	public synchronized String driving_license_endorsement_application_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("108");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logRejectionWorkflow(vo);
						
						result = "SUCCESS";
					
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[driving_license_endorsement_application_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	
	public synchronized String license_duplicate_application_verify(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("103");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logVerificationWorkFlow(vo);
						
						result = "SUCCESS";
						
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_duplicate_application_verify]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	public synchronized NotificationDTO license_duplicate_application_approve(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		notifyDTO.setStatus("FAILURE");
		
		
		try {
			
			if (conn != null) 
			{
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_LEARNER_LICENSE_DUPLICATE_DTLS);
					pst.setString(1, dto.getApplicationNo());
					int count=pst.executeUpdate();
					if (count > 0)
					{
						pst=conn.prepareStatement(UPDATE_LEARNER_LICENSE_DUPLICATE_DTLS);
						pst.setDate(1, date);
						pst.setDate(2, date);
						pst.setString(3, userVo.getBaseId());
						pst.setString(4, userVo.getRegionId());
						pst.setString(5, dto.getApplicationNo());
						int count1=pst.executeUpdate();
						if (count1> 0)
						{	
						   //pst=conn.prepareStatement(INSERT_DUPLICATE_PRINT_DTLS);
						  // pst.setString(1,dto.getApplicationNo());
						   //pst.setString(2,dto.getApplicationNo());
						   //pst.setString(3,dto.getApplicationNo());
						  // pst.executeUpdate();
						   
						   	pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_LEARNER_DUPLICATE);
							pst.setString(1, dto.getApplicationNo());
							rs = pst.executeQuery();
							rs.first();
							
							notifyDTO.setApplicationNo(dto.getApplicationNo());
							notifyDTO.setCustomerName(rs.getString("pname"));
							notifyDTO.setApprovalDate(rs.getString("currentDate"));
							notifyDTO.setEmailAddress(rs.getString("Present_Email"));
							notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
							notifyDTO.setStatus("SUCCESS");
							
							WorkflowDetailsVO vo = new WorkflowDetailsVO();
							vo.setActorId(userVo.getActorId());
							vo.setActorName(userVo.getActorName());
							vo.setRoleId(userVo.getRoleId());
							vo.setRoleName(userVo.getRoleName());
							vo.setApplicationNo(dto.getApplicationNo());
							vo.setAssignedGroupId(userVo.getAssignedGroupId());
							vo.setAssignedUserId(userVo.getAssignedUserId());
							vo.setServiceId("103");
							vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
							vo.setJurisId(userVo.getJurisdictionId());
							vo.setJurisTypeId(userVo.getJurisdictionTypeId());
							new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
						}
					}
				}
			}
		} catch (Exception e) {
			
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_duplicate_application_verify]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
		}
	public synchronized String driving_license_verify(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId("107");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logVerificationWorkFlow(vo);
						
						result = "SUCCESS";
						
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_duplicate_application_verify]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	public synchronized NotificationDTO  driving_license_duplicate_approve(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		notifyDTO.setStatus("FAILURE");
		
		try {
			
			if (conn != null) 
			{		
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_DRIVING_LICENSE_DUPLICATE_DTLS);
					pst.setString(1, dto.getApplicationNo());
					int count = pst.executeUpdate();
					
					if (count>0)
					{
						pst=conn.prepareStatement(UPDATE_T_DRIVING_LICENSE_DUPLICATE_DTLS);
						pst.setDate(1,date);
						pst.setDate(2, date);
						pst.setString(3, userVo.getRegionId());
						pst.setString(4, userVo.getBaseId());
						pst.setString(5, dto.getApplicationNo());
						int count1 = pst.executeUpdate();
						if (count1 > 0)
						{
							pst=conn.prepareStatement(INSERT_DUPLICATE_DL_INTO_PRINT_LIST);
							pst.setString(1,dto.getApplicationNo());
							count1 =pst.executeUpdate();
							if (count1 > 0)
							{
								pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_DRIVING_LICENSE_DUPLICATE);
								pst.setString(1, dto.getApplicationNo());
								rs = pst.executeQuery();
								rs.first();
								
								notifyDTO.setApplicationNo(dto.getApplicationNo());
								notifyDTO.setCustomerName(rs.getString("pname"));
								notifyDTO.setApprovalDate(rs.getString("currentDate"));
								notifyDTO.setEmailAddress(rs.getString("Present_Email"));
								notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
								notifyDTO.setStatus("SUCCESS");
								
								WorkflowDetailsVO vo = new WorkflowDetailsVO();
								vo.setActorId(userVo.getActorId());
								vo.setActorName(userVo.getActorName());
								vo.setRoleId(userVo.getRoleId());
								vo.setRoleName(userVo.getRoleName());
								vo.setApplicationNo(dto.getApplicationNo());
								vo.setAssignedGroupId(userVo.getAssignedGroupId());
								vo.setAssignedUserId(userVo.getAssignedUserId());
								vo.setServiceId("107");
								vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
								vo.setJurisId(userVo.getJurisdictionId());
								vo.setJurisTypeId(userVo.getJurisdictionTypeId());
								//new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
								new WorkflowManager(conn).logDrivingLicenseApprovalCompletedWorkflow(vo);
							}	
						}
					}
				}
			}
				
		} catch (Exception e) {
			
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_duplicate_application_verify]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
		}
	public synchronized String license_duplicate_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("107");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logRejectionWorkflow(vo);
						
						result = "SUCCESS";
					
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_duplicate_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	public synchronized String learner_duplicate_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("103");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logRejectionWorkflow(vo);
						
						result = "SUCCESS";
					
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[learner_duplicate_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
		}
	 
	public synchronized String license_renewal_application_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		
		
			try {
				
				
				if (conn != null) 
				{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(dto.getApplicationNo());
						vo.setServiceId("101");
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logRejectionWorkflow(vo);
						
						result = "SUCCESS";
					
					}
				
		} catch (Exception e) {
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[license_duplicate_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public synchronized String top_issuance(LicenseDTO dto,UserDetailsVO userVo, Connection conn)throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		try {
			if (conn != null) 
			{
					applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_top_application", dto.getPageId(),dto.getBfsNo(), conn);
				    //applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_top_application", dto.getPageId(), conn);
					String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
				
					pst = conn.prepareStatement(INSERT_TOP_DETAILS);
			
					pst.setString(1, applicationNumber);
					pst.setString(2, "TOP");
					pst.setString(3, dto.getCustomerId());
					pst.setString(4, dto.getDrivinglicenseId());
					pst.setString(5, dto.getVehicleId());
					pst.setString(6, dto.getTopNo());
					pst.setString(7, dto.getRegion());
					pst.setString(8, dto.getDzongkhag());
					pst.setString(9, dto.getExactLocation());
					pst.setString(10,dto.getReceiptNo());
					java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
					String receiptDateStr = targetSdf.format(receiptDate);
					pst.setString(11,receiptDateStr);
					//pst.setDate(12,date);
					pst.setString(12,userVo.getActorId());
					pst.setString(13, dto.getAmount());
	
					int count=pst.executeUpdate();	
					if (count > 0)
					{
						WorkflowDetailsVO vo = new WorkflowDetailsVO();
						vo.setActorId(userVo.getActorId());
						vo.setActorName(userVo.getActorName());
						vo.setRoleId(userVo.getRoleId());
						vo.setRoleName(userVo.getRoleName());
						vo.setApplicationNo(applicationNumber);
						vo.setAssignedGroupId(userVo.getAssignedGroupId());
						vo.setAssignedUserId(userVo.getAssignedUserId());
						vo.setServiceId(serviceid);
						vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
						vo.setJurisId(userVo.getJurisdictionId());
						vo.setJurisTypeId(userVo.getJurisdictionTypeId());
						new WorkflowManager(conn).logSubmissionWorkflow(vo);
						
						result = "SUCCESS";
					}
			}
		}

		catch (Exception e) {e.printStackTrace();
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException(
					"###Error at licenseDAO[license_cancellation]:: " + e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber;

	}
	public synchronized NotificationDTO  top_application_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		notifyDTO.setStatus("FAILURE");
		
		try {
			
			if (conn != null) 
			{		
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_TOP_DTLS);
					pst.setString(1, dto.getApplicationNo());
					int count = pst.executeUpdate();
					
					if (count>0)
					{
					
						pst = conn.prepareStatement(INSERT_TOP_ISSUANCE_INTO_PRINT_LIST);
						pst.setString(1, dto.getApplicationNo());
						count = pst.executeUpdate();
						
						if(count > 0)
						{
						/*	pst = conn.prepareStatement(GET_REGION_AND_TOP_NUMBER_FROM_TOP_APPLICATION);
							pst.setString(1, dto.getApplicationNo());
							rs = pst.executeQuery();
							rs.first();
							
							String regionId = rs.getString("Region_Id");
							String topNumber = rs.getString("TOP_Number");
							
							if("Auto".equalsIgnoreCase(topNumber))
							{	
								topNumber = CommonDAO.getInstance().generateTOPNoFormat(regionId);
								
								pst = conn.prepareStatement(UPDATE_TOP_NUMBER_INTO_TOP_DTLS);
								pst.setString(1, topNumber);
								pst.setString(2, dto.getApplicationNo());
								count = pst.executeUpdate();
							}*/
							
							
							pst = conn.prepareStatement(GET_VEHICLE_ID);
							pst.setString(1, dto.getApplicationNo());
							rs = pst.executeQuery();
							rs.first();
							String vehicleId = rs.getString("Vehicle_Id");
							
							pst = conn.prepareStatement(GET_REGISTRATION_DATE_OF_VEHICLE);
							pst.setString(1, vehicleId);
							rs = pst.executeQuery();
							rs.first();
							String registrationDate = rs.getString("Registration_Date");
							
							DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
							DateTime registrationDate1 = format.parseDateTime(registrationDate);
							DateTime a = registrationDate1.plusYears(8);
							String p = a.toString();
							java.util.Date expiryDate = targetSdf.parse(p);
							String expiryDateStr = targetSdf.format(expiryDate);
							
							pst = conn.prepareStatement(UPDATE_TOP_DTLS_WITH_PROCESSED_REGION);
							pst.setString(1, userVo.getRegionId());
							pst.setString(2, userVo.getBaseId());
							pst.setString(3, expiryDateStr);
							pst.setString(4, dto.getApplicationNo());
							count = pst.executeUpdate();
							if(count > 0)
							{
								pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_TOP);
								pst.setString(1, dto.getApplicationNo());
								rs = pst.executeQuery();
								rs.first();
								
								notifyDTO.setApplicationNo(dto.getApplicationNo());
								notifyDTO.setCustomerName(rs.getString("pname"));
								notifyDTO.setApprovalDate(rs.getString("currentDate"));
								notifyDTO.setEmailAddress(rs.getString("Present_Email"));
								notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
								
								WorkflowDetailsVO vo = new WorkflowDetailsVO();
								vo.setActorId(userVo.getActorId());
								vo.setActorName(userVo.getActorName());
								vo.setRoleId(userVo.getRoleId());
								vo.setRoleName(userVo.getRoleName());
								vo.setApplicationNo(dto.getApplicationNo());
								vo.setAssignedGroupId(userVo.getAssignedGroupId());
								vo.setAssignedUserId(userVo.getAssignedUserId());
								vo.setServiceId("114");
								vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
								vo.setJurisId(userVo.getJurisdictionId());
								vo.setJurisTypeId(userVo.getJurisdictionTypeId());
								new WorkflowManager(conn).logApprovalCompletedWorkflow(vo);
								
								notifyDTO.setStatus("SUCCESS");
							}
						}
					}
				}
			}
				
		} 
		catch (Exception e)
		{
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[top_application_approval]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
	}
	
	public synchronized NotificationDTO top_replacement_approval(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		NotificationDTO notifyDTO = new NotificationDTO();
		PreparedStatement pst = null;
		ResultSet rs = null;
		notifyDTO.setStatus("FAILURE");
		
		try 
		{
			if (conn != null) 
			{		
				int countApprove = checkApplicationApproved(dto.getApplicationNo(),conn);
				if(countApprove==1)
				{
					pst = conn.prepareStatement(INSERT_INTO_T_TOP_REPLACEMENT_DTLS);
					pst.setString(1, dto.getApplicationNo());
					int count = pst.executeUpdate();
					
					if (count>0)
					{
						pst = conn.prepareStatement(INSERT_TOP_REPLACEMENT_INTO_PRINT_LIST);
						pst.setString(1, dto.getApplicationNo());
						count = pst.executeUpdate();
						
						if(count > 0)
						{
							/*pst = conn.prepareStatement(UPDATE_TOP_DTLS_WITH_PROCESSED_REGION);
							pst.setString(1, userVo.getRegionId());
							pst.setString(2, userVo.getBaseId());
							pst.setString(3, dto.getApplicationNo());
							count = pst.executeUpdate();
							if(count > 0)
							{*/
							
								pst = conn.prepareStatement(GET_CUSTOMER_INFORMATION_FOR_TOP);
								pst.setString(1, dto.getApplicationNo());
								rs = pst.executeQuery();
								rs.first();
								notifyDTO.setApplicationNo(dto.getApplicationNo());
								notifyDTO.setCustomerName(rs.getString("pname"));
								notifyDTO.setApprovalDate(rs.getString("currentDate"));
								notifyDTO.setEmailAddress(rs.getString("Present_Email"));
								notifyDTO.setMobileNumber(rs.getString("Present_Phone_No"));
								
								WorkflowDetailsVO vo = new WorkflowDetailsVO();
								vo.setActorId(userVo.getActorId());
								vo.setActorName(userVo.getActorName());
								vo.setRoleId(userVo.getRoleId());
								vo.setRoleName(userVo.getRoleName());
								vo.setApplicationNo(dto.getApplicationNo());
								vo.setAssignedGroupId(userVo.getAssignedGroupId());
								vo.setAssignedUserId(userVo.getAssignedUserId());
								vo.setServiceId("115");
								vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
								vo.setJurisId(userVo.getJurisdictionId());
								vo.setJurisTypeId(userVo.getJurisdictionTypeId());
								new WorkflowManager(conn).logDrivingLicenseApprovalCompletedWorkflow(vo);
								
								notifyDTO.setStatus("SUCCESS");
							//}
						}
					}
				}	
			}
		} 
		catch (Exception e) {
			notifyDTO.setStatus("FAILURE");
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[top_replacement_approval]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return notifyDTO;
	}
	
	public synchronized String top_application_reject(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
			
		try 
		{
			if (conn != null) 
			{
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(dto.getApplicationNo());
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId("114");
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(userVo.getJurisdictionId());
				vo.setJurisTypeId(userVo.getJurisdictionTypeId());
				new WorkflowManager(conn).logRejectionWorkflow(vo);
				
				result = "SUCCESS";
			}
		} 
		catch (Exception e)
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[top_application_reject]:: "
					+ e);
		} finally {
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public synchronized String learner_and_license_resubmit(String serviceCode,LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		String serviceId="";
		try 
		{
			if (conn != null)
			{	
				
			    if(dto.getFormType().equalsIgnoreCase("UPDATE_NEW_LEARNER_LICENSE_APPLICATION"))
			    {
				    update_new_learner_license_application(dto, userVo,conn);
				    delete_by_application_no("driveType",DELETE_DRIVE_TYPE_FROM_LEARNER_LICENSE_APPLICATION,dto, userVo,conn);
				    delete_by_application_no("file",DELETE_FILE_BY_APPLICATION_NO,dto, userVo,conn);
				    insert_drive_type(INSERT_INTO_T_LEARNER_LICENSE_DRIVE_TYPE_APPLICATION,dto, userVo,conn);
				    file_upload(dto.getFormType(),dto,conn);
				   
			    }
			    pst = conn.prepareStatement(GET_SERVICE_CODE_FROM_SHORT_DESC);
				pst.setString(1, serviceCode);
				rs = pst.executeQuery();
				rs.first();
				serviceId	=	rs.getString("Service_Id");
				
				
				WorkflowDetailsVO vo = new WorkflowDetailsVO();
				vo.setActorId(userVo.getActorId());
				vo.setActorName(userVo.getActorName());
				vo.setRoleId(userVo.getRoleId());
				vo.setRoleName(userVo.getRoleName());
				vo.setApplicationNo(applicationNumber);
				vo.setAssignedGroupId(userVo.getAssignedGroupId());
				vo.setAssignedUserId(userVo.getAssignedUserId());
				vo.setServiceId(serviceId);
				vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
				vo.setJurisId(dto.getRegion());
				new WorkflowManager(conn).logResubmissionWorkflow(vo);
				result="SUCCESS"; 
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "
					+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber;
	}
	public synchronized String update_new_learner_license_application(LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	int count=1;
				pst = conn.prepareStatement(UPDATE_NEW_LEARNER_LICENSE_APPLICATION);
		    	pst.setString(count++, dto.getCertifyingDoctor());
				pst.setString(count++, dto.getRemarks());
		    	pst.setString(count++, userVo.getActorId());
				pst.setDate(count++, date);
		    	pst.setString(count++, applicationNumber);
		    	pst.executeUpdate();
		    	result="SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "
					+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}
	public synchronized String delete_by_application_no(String deleteType,String query,LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	
				pst = conn.prepareStatement(query); 
		    	pst.setString(1, applicationNumber);
		    	pst.executeUpdate();
		    	if(deleteType.equalsIgnoreCase("file"))
		    	{
		    		pst = conn.prepareStatement(GET_FILE_PATH_FROM_DOC);
					pst.setString(1, dto.getApplicationNo());
					rs = pst.executeQuery();
					while (rs.next()) 
					{
						File filePath = new File(rs.getString("Document_Path"));
						if(filePath.exists())
							filePath.delete();
					}
		    	}
		    	result	=	"SUCCESS";
			}
		} 
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "
					+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result;
	}
	public synchronized String insert_drive_type(String query,LicenseDTO dto, UserDetailsVO userVo,Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	
				String[] driveTypeArray = dto.getDriveTypeList().split("#");
				for (int i = 0; i < driveTypeArray.length; i++) {
					if (!(driveTypeArray[i].equals("0"))) {
						pst = conn.prepareStatement(query);
						pst.setString(1, applicationNumber);
						pst.setString(2, driveTypeArray[i]);

						  pst.executeUpdate();
					}
				}
			}
		} 
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "
					+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber;
	}
	public synchronized String file_upload(String fileType,LicenseDTO dto, Connection conn) throws ERALISException, ERALISSystemException 
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber	=	dto.getApplicationNo();
		ResultSet rs = null;
		try 
		{
			if (conn != null)
			{	 
				if(fileType.equalsIgnoreCase("UPDATE_NEW_LEARNER_LICENSE_APPLICATION"))
				{ 
					DocumentVO documentVO = new DocumentVO();
					documentVO.setServiceName("LEARNER");
					documentVO.setFileContent(dto.getFileMC().getFileData());
					documentVO.setName(dto.getFileMC().getFileName());
					documentVO.setDocumentType("LL");
					CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
					
					documentVO.setServiceName("LEARNER");
					documentVO.setFileContent(dto.getFileApplForm().getFileData());
					documentVO.setName(dto.getFileApplForm().getFileName());
					documentVO.setDocumentType("LL");
					CommonDAO.getInstance().fileUploader(documentVO, applicationNumber);
				}
			}
		} 
		catch (Exception e) 
		{ 
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at licenseDAO[new_issue]:: "
					+ e);
		} 
		finally {
			ConnectionManager.close(null, null, null, pst);
		}

		return result+"#"+applicationNumber;
	}
	
	public LicenseDTO getTopDtls(String topNumber,String searchType) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		LicenseDTO dto = new LicenseDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				String query = GET_TOP_DETAILS;
				if(searchType.equalsIgnoreCase("TOP_HISTORY") ||  searchType.equalsIgnoreCase("REPLACEMENT") )
				{
					query = query + " a.`Customer_Id` = ?";
				}
				else
				{
					query = query + " a.`TOP_Number` = ?";
				}
				pst = conn.prepareStatement(query);
				pst.setString(1, topNumber);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto.setTopNo(rs.getString("TOP_Number"));
					dto.setName(rs.getString("pname"));
					dto.setRegion(rs.getString("region"));
					dto.setDzongkhag(rs.getString("dzongkhag"));
					dto.setExactLocation(rs.getString("Exact_Location"));
					dto.setCustomerId(rs.getString("Customer_Id"));
					dto.setPhoneNo(rs.getString("Present_Phone_No"));
					dto.setCID(rs.getString("CID_Number"));
					dto.setPermDzongkhag(rs.getString("permDzongkhag"));
					dto.setPermGewog(rs.getString("permGewog"));
					dto.setPermVillage(rs.getString("Permanant_Village_Id"));
					dto.setVehicleNo(rs.getString("Vehicle_Number"));
					dto.setVehicleCompany(rs.getString("vehicleCompany"));
					dto.setVehicleModel(rs.getString("vehicleModel"));
					dto.setVehicleColor(rs.getString("Colour"));
					dto.setEngineNumber(rs.getString("Engine_Number"));
					dto.setEngineCC(rs.getString("Engine_CC"));
					dto.setChassisNumber(rs.getString("Chassis_Number"));
					dto.setEngineType(rs.getString("engineType"));
					dto.setSeatingCapacity(rs.getString("Seat_Capacity"));
					dto.setRegionId(rs.getString("Region_Id"));
					dto.setLicenseNo(rs.getString("Driving_License_No"));
					
					pst = conn.prepareStatement(CHECK_PENDING_APPLICATION);
					pst.setString(1, rs.getString("Customer_Id"));
					rs = pst.executeQuery();
					rs.first();
					int rowCount1 = rs.getInt("rowCount");
					if(rowCount1>0)
					{
						dto.setStatus("PENDING_APPLICATION");
						dto.setReason(rs.getString("remarks"));
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public String top_cancellation(LicenseDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		
		try 
		{
			if(conn != null)
			{
				java.util.Date Cancellationdate = sourceSdf.parse(dto.getCancellationdate());
				String cancellationdate = targetSdf.format(Cancellationdate);
				
				pst = conn.prepareStatement(INSERT_INTO_T_TOP_CANCELLATION_DTLS);
				pst.setString(1, dto.getTopNo());
				pst.setString(2, dto.getCustomerId());
				pst.setString(3, cancellationdate);
				pst.setString(4, dto.getCancellationReason());
				pst.setString(5, userVo.getActorId());
				pst.setString(6, userVo.getRegionId());
				pst.setString(7, userVo.getBaseId());
				
				int count = pst.executeUpdate();
				
				if(count > 0)
					result = "SUCCESS";
			}
		} 
		catch (Exception e) 
		{
			
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String top_replacement(LicenseDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		String result = "FAILURE";
		String applicationNumber = null;
		
		try 
		{
			if(conn != null)
			{
				applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_top_application", dto.getPageId(),dto.getBfsNo(), conn);
				//applicationNumber = EralisCommonUtil.generateApplicationNumberFormat("t_top_application", dto.getPageId(), conn);
				String serviceid=EralisCommonUtil.getServiceIdAsString(dto.getPageId(), conn);
					
				pst = conn.prepareStatement(INSERT_TOP_DETAILS);
				pst.setString(1, applicationNumber);
				pst.setString(2, "TOPR");
				pst.setString(3, dto.getCustomerId());
				pst.setString(4, null);
				pst.setString(5, null);
				pst.setString(6, dto.getTopNo());
				pst.setString(7, dto.getRegion());
				pst.setString(8, null);
				pst.setString(9, null);
				pst.setString(10,dto.getReceiptNo());
				java.util.Date receiptDate = sourceSdf.parse(dto.getReceiptDate());
				String receiptDateStr = targetSdf.format(receiptDate);
				pst.setString(11,receiptDateStr);
				//pst.setDate(12,date);
				pst.setString(12,userVo.getActorId());
				pst.setString(13, dto.getAmount());
				int count=pst.executeUpdate();	
				
				if (count > 0)
				{
					WorkflowDetailsVO vo = new WorkflowDetailsVO();
					vo.setActorId(userVo.getActorId());
					vo.setActorName(userVo.getActorName());
					vo.setRoleId(userVo.getRoleId());
					vo.setRoleName(userVo.getRoleName());
					vo.setApplicationNo(applicationNumber);
					vo.setAssignedGroupId(userVo.getAssignedGroupId());
					vo.setAssignedUserId(userVo.getAssignedUserId());
					vo.setServiceId(serviceid);
					vo.setAssigned_Priv_Id(userVo.getAssignedPrivId());
					vo.setJurisId(userVo.getJurisdictionId());
					vo.setJurisTypeId(userVo.getJurisdictionTypeId());
					new WorkflowManager(conn).logSubmissionWorkflow(vo);
					
					result = "SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result+"#"+applicationNumber;
	}
	
	public int checkApplicationApproved(String applicationNo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		int result = 0;
		ResultSet rs = null;
		try 
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(CHECK_APPLICATION_APPROVED);
				pst.setString(1, applicationNo);
				rs = pst.executeQuery();
				rs.first();
				result = rs.getInt("rowCount");
			}
		} 
		catch (Exception e) 
		{
			
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public String request_reprint(LicenseDTO dto, UserDetailsVO userVo, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result = "FAILURE";
		String requestId = null;
		
		try
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(INSERT_INTO_T_REPRINT_TABLE, PreparedStatement.RETURN_GENERATED_KEYS);
				pst.setString(1, dto.getRemarks());
				pst.setInt(2, Integer.parseInt(userVo.getJurisdictionId()));
				pst.setInt(3, Integer.parseInt(userVo.getJurisdictionTypeId()));
				pst.setString(4, userVo.getActorId());
				pst.setDate(5, date);
				int count = pst.executeUpdate();
				
				rs = pst.getGeneratedKeys();
				while(rs.next())
				{
					requestId = rs.getString(1);
				}
				
				if(null != requestId && count > 0)
				{
					for(OffenceDTO offence : dto.getOffenceList())
					{
						pst = conn.prepareStatement(INSERT_INTO_T_REPRINT_DTLS);
						pst.setString(1, requestId);
						pst.setString(2, offence.getOffenceId());
						pst.executeUpdate();
					}
					
					result = "SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseDAO[request_reprint]:: "+e);
		}
		finally
		{
			ConnectionManager.close(null, null, null, pst);
		}
		
		return result;
	}
	
	public List<LicenseDTO> getReprintList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LicenseDTO> reprintList = new ArrayList<LicenseDTO>();
		LicenseDTO dto;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPRINT_REQUEST_LIST);
				pst.setInt(1, Integer.parseInt(jurisId));
				pst.setInt(2, Integer.parseInt(jurisTypeId));
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new LicenseDTO();
					dto.setDrivinglicenseId(rs.getString("Request_Id"));
					dto.setName(rs.getString("name"));
					dto.setRemarks(rs.getString("Request_Remarks"));
					dto.setAppsubmissiondate(rs.getString("requestedDate"));
					
					reprintList.add(dto);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return reprintList;
	}
	
	public LicenseDTO get_reprint_details(String requestId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		LicenseDTO dto = new LicenseDTO();
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPRINT_DETAILS);
				pst.setInt(1, Integer.parseInt(requestId));
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto.setDrivinglicenseId(rs.getString("Request_Id"));
					dto.setRemarks(rs.getString("Request_Remarks"));
					dto.setName(rs.getString("name"));
					dto.setAppsubmissiondate(rs.getString("requestedDate"));
					
					List<OffenceDTO> drivingLicenseList = getReprintLicenseList(requestId, conn);
					dto.setOffenceList(drivingLicenseList);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return dto;
	}
	
	public String approve_request_reprint(String requestId, String remarks, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null, pst1 = null, pst2 = null, pst3 = null;
		ResultSet rs = null, rs1 = null, rs2 = null, rs3 = null;
		String result = "FAILURE";
		int count = 0;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPRINT_LICENSE_LIST);
				pst.setString(1, requestId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					String licenseNo = rs.getString("Driving_License_No");
					
					pst1 = conn.prepareStatement(CHECK_IF_LICENSE_EXISTS_IN_PRINT_LIST);
					pst1.setString(1, licenseNo);
					rs1 = pst1.executeQuery();
					rs1.first();
					
					int rowCount = rs1.getInt("rowCount");
					
					if(rowCount > 0)
					{
						pst = conn.prepareStatement(UPDATE_IS_PRINTED_FLAG);
						pst.setString(1, "N");
						pst.setString(2, licenseNo);
						count = pst.executeUpdate();
					}
					else
					{
						pst2 = conn.prepareStatement(CHECK_IF_DOCUMENT_IS_LICENSE);
						pst2.setString(1, licenseNo);
						rs2 = pst2.executeQuery();
						rs2.first();
						int count1 = rs2.getInt("rowCount");
						
						pst3 = conn.prepareStatement(CHECK_IF_DOCUMENT_IS_TOP);
						pst3.setString(1, licenseNo);
						rs3 = pst3.executeQuery();
						rs3.first();
						int count2 = rs3.getInt("rowCount");
						
						if(count1 > 0)
						{
							pst = conn.prepareStatement(INSERT_INTO_T_PRINT_DTLS_FOR_REPRINT);
							pst.setString(1, licenseNo);
							pst.setString(2, licenseNo);
							pst.setString(3, requestId);
							pst.setString(4, requestId);
							count = pst.executeUpdate();
						}
						
						if(count2 > 0)
						{
							pst = conn.prepareStatement(INSERT_INTO_PRINT_DTLS_FOR_TOP);
							pst.setString(1, licenseNo);
							pst.setString(2, licenseNo);
							pst.setString(3, licenseNo);
							pst.setString(4, requestId);
							pst.setString(5, requestId);
							count = pst.executeUpdate();
						}
					}
				}
				
				if(count > 0)
				{
					pst = conn.prepareStatement(UPDATE_T_REPRINT);
					pst.setString(1, userVo.getActorId());
					pst.setDate(2, date);
					pst.setString(3, remarks);
					pst.setString(4, requestId);
					
					count  = pst.executeUpdate();
					
					if(count > 0)
						result = "SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, rs, pst);
		}
		
		return result;
	}
	
	private List<OffenceDTO> getReprintLicenseList(String requestId, Connection conn) throws ERALISException, ERALISSystemException
	{
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<OffenceDTO> drivingLicenseList = new ArrayList<OffenceDTO>();
		OffenceDTO dto;
		
		try 
		{
			if(conn != null)
			{
				pst = conn.prepareStatement(GET_REPRINT_DETAILS1);
				pst.setString(1, requestId);
				rs = pst.executeQuery();
				
				while(rs.next())
				{
					dto = new OffenceDTO();
					dto.setOffenceId(rs.getString("Driving_License_No"));
					
					drivingLicenseList.add(dto);
				}
			}
		}
		catch (Exception e) 
		{
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(null, null, rs, pst);
		}
		
		return drivingLicenseList;
	}
	 
	/*
	 * Queries
	 */
	
	private static final String UPDATE_T_REPRINT = "UPDATE `t_license_reprint` a SET a.`Status`='A',a.`Approved_By`=?,a.`Approved_Date`=?,a.`Approval_Remarks`=? WHERE a.`Request_Id`=?";
	
	private static final String UPDATE_IS_PRINTED_FLAG = "UPDATE t_print_dtls a SET a.`isPrinted`=? WHERE a.`Identity_Number`=?";
	
	private static final String INSERT_INTO_T_PRINT_DTLS_FOR_REPRINT = "INSERT INTO `t_print_dtls` ( "
		+ "  `Document_Type`, "
		+ "  `Service_Id`, "
		+ "  `Identity_Number`, "
		+ "  `Identity_Id`, "
		+ "  `isPrinted`, "
		+ "  `Jurisdiction_Id`, "
		+ "  `Jurisdiction_Type_Id`, "
		+ "  `Created_On` "
		+ ") "
		+ "VALUES "
		+ "  ( "
		+ "    'DL', "
		+ "    '106', "
		+ "    ?, "
		+ "    (SELECT "
		+ "      `Driving_License_Id` "
		+ "    FROM "
		+ "      `t_driving_license_dtls` "
		+ "    WHERE `Driving_License_No` = ?), "
		+ "    'N', "
		+ "    (SELECT "
		+ "      `Juris_Id` "
		+ "    FROM "
		+ "      `t_license_reprint` "
		+ "    WHERE `Request_Id` = ?), "
		+ "    (SELECT "
		+ "      `Juris_Type_Id` "
		+ "    FROM "
		+ "      `t_license_reprint` "
		+ "    WHERE `Request_Id` = ?), "
		+ "    CURRENT_TIMESTAMP "
		+ "  )";
	
	private static final String INSERT_INTO_PRINT_DTLS_FOR_TOP = "INSERT INTO `t_print_dtls` ( "
		+ "  `Application_Number`, "
		+ "  `Document_Type`, "
		+ "  `Service_Id`, "
		+ "  `Identity_Number`, "
		+ "  `Identity_Id`, "
		+ "  `isPrinted`, "
		+ "  `Jurisdiction_Id`, "
		+ "  `Jurisdiction_Type_Id`, "
		+ "  `Created_On` "
		+ ") "
		+ "VALUES "
		+ "  ( "
		+ "    (SELECT "
		+ "      Application_Number "
		+ "    FROM "
		+ "      `t_top_dtls` "
		+ "    WHERE `TOP_Number` = ?), "
		+ "    'TOP', "
		+ "    '114', "
		+ "    ?, "
		+ "    (SELECT "
		+ "      `Top_Issue_Id` "
		+ "    FROM "
		+ "      `t_top_dtls` "
		+ "    WHERE `TOP_Number` = ?), "
		+ "    'N', "
		+ "    (SELECT "
		+ "      `Juris_Id` "
		+ "    FROM "
		+ "      `t_license_reprint` "
		+ "    WHERE `Request_Id` = ?), "
		+ "    (SELECT "
		+ "      `Juris_Type_Id` "
		+ "    FROM "
		+ "      `t_license_reprint` "
		+ "    WHERE `Request_Id` = ?), "
		+ "    CURRENT_TIMESTAMP "
		+ "  )";
	
	private static final String CHECK_IF_DOCUMENT_IS_LICENSE = "SELECT COUNT(*) rowCount FROM t_driving_license_dtls a WHERE a.`Driving_License_No`=?";
	
	private static final String CHECK_IF_DOCUMENT_IS_TOP = "SELECT COUNT(*) rowCount FROM t_top_dtls a WHERE a.`TOP_Number`=?";
	
	private static final String CHECK_IF_LICENSE_EXISTS_IN_PRINT_LIST = "SELECT COUNT(a.`Document_Id`) rowCount FROM t_print_dtls a WHERE  a.`Identity_Number`=?";
	
	private static final String GET_REPRINT_DETAILS = "SELECT "
		+ "  a.`Request_Id`, "
		+ "  a.`Request_Remarks`, "
		+ "  b.`name`, "
		+ "  DATE_FORMAT(a.`Requested_On`, '%d/%m/%Y') requestedDate "
		+ "FROM "
		+ "  `t_license_reprint` a "
		+ "  LEFT JOIN t_user_master b "
		+ "    ON a.`Requested_By` = b.`login_id` "
		+ "WHERE a.`Request_Id` = ?";
	
	private static final String GET_REPRINT_LICENSE_LIST = "SELECT a.Driving_License_No FROM `t_license_reprint_dtls` a WHERE a.`Request_Id`=?";
	
	private static final String GET_REPRINT_DETAILS1 = "SELECT * FROM `t_license_reprint_dtls` a WHERE a.`Request_Id`=?";
	
	private static final String GET_REPRINT_REQUEST_LIST = "SELECT "
		+ "  a.`Request_Id`, "
		+ "  b.`name`, "
		+ "  a.`Request_Remarks`, "
		+ "  DATE_FORMAT(a.`Requested_On`, '%d/%m/%Y') requestedDate "
		+ "FROM "
		+ "  `t_license_reprint` a "
		+ "  LEFT JOIN t_user_master b "
		+ "    ON a.Requested_By = b.login_id "
		+ "WHERE a.`Juris_Id` = ? "
		+ "  AND a.`Juris_Type_Id` = ? "
		+ "  AND a.Status <> 'A'";
	
	private static final String INSERT_INTO_T_REPRINT_TABLE = "INSERT INTO `t_license_reprint` ( "
		+ "  `Request_Remarks`, "
		+ "  `Juris_Id`, "
		+ "  `Juris_Type_Id`, "
		+ "  `Requested_By`, "
		+ "  `Requested_On` "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?)";
	
	private static final String INSERT_INTO_T_REPRINT_DTLS = "INSERT INTO `t_license_reprint_dtls` ( "
		+ "  `Request_Id`, "
		+ "  `Driving_License_No` "
		+ ") "
		+ "VALUES "
		+ "  (?, ?)";
	
	private static final String INSERT_INTO_T_TOP_CANCELLATION_DTLS = "INSERT INTO `t_top_cancellation_dtls` ( "
		+ "  `TOP_Number`, "
		+ "  `Customer_Id`, "
		+ "  `Cancellation_Date`, "
		+ "  `Cancellation_Reason_Id`, "
		+ "  `Created_By`, "
		+ "  `Created_On`,Processed_Region_Id,Processed_Base_Id "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?)";
	
	private static final String GET_TOP_DETAILS = "SELECT "
		+ "  a.`TOP_Number`, "
		+ "  CONCAT( "
		+ "    c.`First_Name`, "
		+ "    ' ', "
		+ "    c.`Middle_Name`, "
		+ "    ' ', "
		+ "    c.`Last_Name` "
		+ "  ) pname, "
		+ "  (SELECT "
		+ "    `region_name` "
		+ "  FROM "
		+ "    `t_region_master` "
		+ "  WHERE `region_id` = a.`Region_Id`) region, "
		+ "  (SELECT "
		+ "    dzongkhag_name "
		+ "  FROM "
		+ "    t_dzongkhag_master "
		+ "  WHERE dzongkhag_id = a.`Dzongkhag_Id`) dzongkhag, "
		+ "  a.`Exact_Location`, "
		+ "  a.`Customer_Id`, "
		+ "  c.`Present_Phone_No`, "
		+ "  c.`CID_Number`, "
		+ "  (SELECT "
		+ "    dzongkhag_name "
		+ "  FROM "
		+ "    t_dzongkhag_master "
		+ "  WHERE dzongkhag_id = c.`Permanent_Dzongkhag_Id`) permDzongkhag, "
		+ "  (SELECT "
		+ "    gewog_name "
		+ "  FROM "
		+ "    t_gewog_master "
		+ "  WHERE gewog_id = c.`Permanent_Gewog_Id`) permGewog, "
		+ "  c.`Permanant_Village_Id`, "
		+ "  b.`Vehicle_Number`, "
		+ "  (SELECT "
		+ "    `Vehicle_Company_Name` "
		+ "  FROM "
		+ "    `t_vehicle_company_master` "
		+ "  WHERE `Vehicle_Company_Id` = b.`Vehicle_Company_Id`) vehicleCompany, "
		+ "  (SELECT "
		+ "    `Vehicle_Model_Name` "
		+ "  FROM "
		+ "    `t_vehicle_model_master` "
		+ "  WHERE `Vehicle_Model_Id` = b.`Vehicle_Model_Id`) vehicleModel, "
		+ "  b.`Colour`, "
		+ "  b.`Engine_Number`, "
		+ "  b.`Engine_CC`, "
		+ "  b.`Chassis_Number`, "
		+ "  (SELECT "
		+ "    Engine_Name "
		+ "  FROM "
		+ "    `t_engine_type_master` "
		+ "  WHERE `Engine_Type_Id` = b.`Engine_Type_Id`) engineType, "
		+ "  b.`Seat_Capacity`, "
		+ "  a.`Region_Id`, "
		+ "  d.`Driving_License_No` "
		+ "FROM "
		+ "  t_top_dtls a "
		+ "  LEFT JOIN t_vehicle_registration_dtls b "
		+ "    ON a.`Vehicle_Id` = b.`Vehicle_Reg_Dtls_Id` "
		+ "  LEFT JOIN t_personal_dtls c "
		+ "    ON a.`Customer_Id` = c.`Customer_Id` "
		+ "  LEFT JOIN `t_driving_license_dtls` d "
		+ "    ON a.`License_Id` = d.`Driving_License_Id` "
		+ "WHERE ";
	
	private static final String INSERT_INTO_T_LEARNER_LICENSE_APPLICATION = "INSERT INTO `t_learner_license_application` "
																			+ "            (`Application_Number`, "
																			+ "             `Application_Type`, "
																			+ "             `Customer_Id`, "
																			+ "             `Learner_License_No`, "
																			+ "             `Region_Id`, "
																			+ "             `Receipt_No`, "
																			+ "             `Receipt_Date`, "
																			+ "             `Certifying_Doctor`, "
																			+ "             `Remarks`, "
																			+ "             `App_Submission_Date`, "
																			+ "             `Created_By`, "
																			+ "             `Create_On`, "
																			+ "             `Updated_By`, "
																			+ "             `Updated_On`) "
																			+ "VALUES (?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        CURRENT_TIMESTAMP, "
																			+ "        ?, "
																			+ "        CURRENT_TIMESTAMP, "
																			+ "        ?, "
																			+ "        CURRENT_TIMESTAMP);";

	private static final String INSERT_INTO_T_LEARNER_LICENSE_DRIVE_TYPE_APPLICATION = "INSERT INTO `t_learner_licn_drive_type_application` ( "
																					+ "  `Application_Number`, "
																					+ "  `Drive_Type_Id` "
																					+ ") "
																					+ "VALUES " + "  ( " + "    ?, " + "    ? " + "  )";

	private static final String INSERT_INTO_T_LEARNER_LICENSE_RENEWAL_APPLICATION = "INSERT INTO `t_learner_license_application` "
																					+ "            (`Application_Number`, "
																					+ "             `Application_Type`, "
																					+ "             `Customer_Id`, "
																					+ "             `Learner_License_No`, "
																					+ "             `Region_Id`, "
																					+ "             `Receipt_No`, "
																					+ "             `Receipt_Date`, "
																					+ "             `Certifying_Doctor`, "
																					+ "             `Remarks`, "
																					+ "             `App_Submission_Date`, "
																					+ "             `Created_By`, "
																					+ "             `Create_On`, "
																					+ "             `Updated_By`, "
																					+ "             `Updated_On`,Learner_License_Id,Renewal_Duration) "
																					+ "VALUES (?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        ?, "
																					+ "        CURRENT_TIMESTAMP, "
																					+ "        ?, "
																					+ "        CURRENT_TIMESTAMP, "
																					+ "        ?, "
																					+ "        CURRENT_TIMESTAMP,?,?);";
	
	private static final String INSERT_INTO_T_LEARNER_LICENSE_DUPLICATION_APPLICATION = "INSERT INTO `t_learner_license_application` "
																						+ "            (`Application_Number`, "
																						+ "             `Application_Type`, "
																						+ "             `Customer_Id`, "
																						+ "             `Learner_License_No`, "
																						+ "             `Region_Id`, "
																						+ "             `Receipt_No`, "
																						+ "             `Receipt_Date`, "
																						+ "             `Certifying_Doctor`, "
																						+ "             `Remarks`, "
																						+ "             `App_Submission_Date`, "
																						+ "             `Created_By`, "
																						+ "             `Create_On`, "
																						+ "             `Updated_By`, "
																						+ "             `Updated_On`,Learner_License_Id) "
																						+ "VALUES (?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        ?, "
																						+ "        CURRENT_TIMESTAMP, "
																						+ "        ?, "
																						+ "        CURRENT_TIMESTAMP, "
																						+ "        ?, "
																						+ "        CURRENT_TIMESTAMP,?);";
	
	private static final String INSERT_COMMERCIAL_LICENSE_DETAILS = "INSERT INTO `t_driving_license_application` "
																	+ "            (`Application_Number`, "
																	+ "             `Application_Type`, "
																	+ "             `Driving_license_type`, "
																	+ "             `Issued_To_Id`, "
																	+ "             `Learner_License_No`, "
																	+ "             `Customer_Id`, "
																	+ "             `Driving_License_No`, "
																	+ "             `Region_Id`, "
																	+ "             `Issue_Date`, "
																	+ "             `Expiry_Date`, "
																	+ "             `Status`, "
																	+ "             `IID`, "
																	+ "             `Test_Marks`, "
																	+ "             `Register_Number`, "
																	+ "             `Drive_Type_Id`, "
																	+ "             `Is_Renewal_Upon_MC`, "
																	+ "             `Remarks`, "
																	+ "             `Receipt_Number`, "
																	+ "             `Receipt_Date`, "
																	+ "             `Endorsed_Dated`, "
																	+ "             `App_Submission_Date`, "
																	+ "             `Created_By`, "
																	+ "             `Created_On`, "
																	+ "             `Updated_By`, "
																	+ "             `Updated_On`,License_Id,Renewal_Duration) "
																	+ "VALUES (?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, ?,?);";

	private static final String INSERT_LICENSE_ENDORSEMENT_DETAILS ="INSERT INTO `t_driving_license_application` "
																	+ "            (`Application_Number`, "
																	+ "             `Application_Type`, "
																	+ "             `Driving_license_type`, "
																	+ "             `Issued_To_Id`, "
																	+ "             `Learner_License_No`, "
																	+ "             `Customer_Id`, "
																	+ "             `Driving_License_No`, "
																	+ "             `Region_Id`, "
																	+ "             `Issue_Date`, "
																	+ "             `Expiry_Date`, "
																	+ "             `Status`, "
																	+ "             `IID`, "
																	+ "             `Test_Marks`, "
																	+ "             `Register_Number`, "
																	+ "             `Drive_Type_Id`, "
																	+ "             `Is_Renewal_Upon_MC`, "
																	+ "             `Remarks`, "
																	+ "             `Receipt_Number`, "
																	+ "             `Receipt_Date`, "
																	+ "             `Endorsed_Dated`, "
																	+ "             `App_Submission_Date`, "
																	+ "             `Created_By`, "
																	+ "             `Created_On`, "
																	+ "             `Updated_By`, "
																	+ "             `Updated_On`,License_Id,`Is_TCB_Endorsement`,Is_Endorsement_Renewal,Renewal_Duration) "
																	+ "VALUES (?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?," +
																			"?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP,?,?,?,?);";

	private static final String INSERT_RENEWAL_LICENSE_DETAILS = "INSERT INTO `t_driving_license_application` "
																	+ "            (`Application_Number`, "
																	+ "             `Application_Type`, "
																	+ "             `Driving_license_type`, "
																	+ "             `Issued_To_Id`, "
																	+ "             `Learner_License_No`, "
																	+ "             `Customer_Id`, "
																	+ "             `Driving_License_No`, "
																	+ "             `License_Id`, "
																	+ "             `Region_Id`, "
																	+ "             `Issue_Date`, "
																	+ "             `Expiry_Date`, "
																	+ "             `Status`, "
																	+ "             `IID`, "
																	+ "             `Test_Marks`, "
																	+ "             `Register_Number`, "
																	+ "             `Drive_Type_Id`, "
																	+ "             `Is_Renewal_Upon_MC`, "
																	+ "             `Remarks`, "
																	+ "             `Receipt_Number`, "
																	+ "             `Receipt_Date`, "
																	+ "             `Endorsed_Dated`, "
																	+ "             `App_Submission_Date`, "
																	+ "             `Created_By`, "
																	+ "             `Created_On`, "
																	+ "             `Updated_By`, "
																	+ "             `Updated_On`,Renewal_Duration) "
																	+ "VALUES (?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP,?);";
	
	private static final String INSERT_INTO_T_PAYMENT_DTLS = "INSERT INTO `t_payment_dtls` ( "
		+ "  `Application_Number`, "
		+ "  `Application_Type`, "
		+ "  `Service_Id`, "
		+ "  `Amount_Paid`, "
		+ "  `Penalty_Paid`, "
		+ "   Receipt_No, "
		+ "   Receipt_Date, "
		+ "  `Created_By` "
		+ ") " + "VALUES " + "  (?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String INSERT_LICENSE_DUPLICATION_DETAILS = "INSERT INTO `t_driving_license_application` "
																	+ "            (`Application_Number`, "
																	+ "             `Application_Type`, "
																	+ "             `Driving_license_type`, "
																	+ "             `Issued_To_Id`, "
																	+ "             `Learner_License_No`, "
																	+ "             `Customer_Id`, "
																	+ "             `Driving_License_No`, "
																	+ "             `Region_Id`, "
																	+ "             `Issue_Date`, "
																	+ "             `Expiry_Date`, "
																	+ "             `Status`, "
																	+ "             `IID`, "
																	+ "             `Test_Marks`, "
																	+ "             `Register_Number`, "
																	+ "             `Drive_Type_Id`, "
																	+ "             `Is_Renewal_Upon_MC`, "
																	+ "             `Remarks`, "
																	+ "             `Receipt_Number`, "
																	+ "             `Receipt_Date`, "
																	+ "             `Endorsed_Dated`, "
																	+ "             `App_Submission_Date`, "
																	+ "             `Created_By`, "
																	+ "             `Created_On`, "
																	+ "             `Updated_By`, "
																	+ "             `Updated_On`,License_Id) "
																	+ "VALUES (?, ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?, "
																	+ "        CURRENT_TIMESTAMP, "
																	+ "        ?);";
	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_NON_COMMERCIAL_APPLICATION = "INSERT INTO `t_driving_license_application` "
																				+ "            (`Application_Number`, "
																				+ "             `Application_Type`, "
																				+ "             `Driving_license_type`, "
																				+ "             `Issued_To_Id`, "
																				+ "             `Learner_License_No`, "
																				+ "             `Customer_Id`, "
																				+ "             `Driving_License_No`, "
																				+ "             `Region_Id`, "
																				+ "             `Issue_Date`, "
																				+ "             `Expiry_Date`, "
																				+ "             `Status`, "
																				+ "             `IID`, "
																				+ "             `Test_Marks`, "
																				+ "             `Register_Number`, "
																				+ "             `Drive_Type_Id`, "
																				+ "             `Is_Renewal_Upon_MC`, "
																				+ "            `Remarks`,Renewal_Duration,  "
																				+ "             `Receipt_Number`, "
																				+ "             `Receipt_Date`, "
																				+ "             `Endorsed_Dated`, "
																				+ "             `App_Submission_Date`, "
																				+ "             `Created_By`, "
																				+ "             `Created_On`, "
																				+ "             `Updated_By`, "
																				+ "             `Updated_On`,"
																				+ "				`Issue_type`) "
																				+ "VALUES (?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        CURRENT_TIMESTAMP, "
																				+ "        ?, "
																				+ "        CURRENT_TIMESTAMP, "
																				+ "        ?, "
																				+ "        CURRENT_TIMESTAMP,?);";

	private static final String INSERT_DRIVING_LICENSE_CANCELLATION_DETAILS = "INSERT INTO `t_driving_license_cancelled` "
																			+ "            (`Customer_Id`, "
																			+ "             `License_Id`, "
																			+ "             `Cancellation_Date`, "
																			+ "             `Status`, "
																			+ "             `Cancellation_Reason`, "
																			+ "             `Created_On`, "
																			+ "             `Created_By`) "
																			+ "VALUES (?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        ?, "
																			+ "        CURRENT_TIMESTAMP, "
																			+ "        ?);";
	
	private static final String INSERT_LEARNER_LICENSE_CANCELLATION_DETAILS = "INSERT INTO `t_learner_license_cancelled` "
																			+ "            ( "
																			+ "             `Customer_Id`, "
																			+ "             `Learner_Id`, "
																			+ "             `Cancellation_Date`, "
																			+ "             `Status`, "
																			+ "             `Cancellation_Reason`, "
																			+ "             `Created_On`, "
																			+ "             `Created_By`) "
																			+ "VALUES (?,?,?,?,?,CURRENT_TIMESTAMP,?);";
	
	private static final String INSERT_DRIVING_LICENSE_WITHDRAW_CANCELLATION_DETAILS = "INSERT INTO `t_driving_license_withdraw_cancellation` "
		+ "            (`Customer_Id`, "
		+ "             `License_Id`, "
		+ "             `Withdrwan_Date`, " 
		+ "             `Withdrawn_Reason`, "
		+ "             `Created_On`, "
		+ "             `Created_By`, "
		+ "             `Updated_On`, "
		+ "             `Updated_By`) "
		+ "VALUES (?, "
		+ "        ?, "
		+ "        ?, "
		+ "        ?, " 
		+ "        CURRENT_TIMESTAMP, "
		+ "        ?, "
		+ "        CURRENT_TIMESTAMP, "
		+ "        ?);";
	
	
	
	private static final String UPDATE_T_DRIVING_LICENSE_DTLS="UPDATE t_driving_license_dtls "
																+ " SET "
																+ " `Status` = ? "
																+ " WHERE "
																+ " Driving_License_Id=?";
	
	private static final String UPDATE_CANCELLED_T_LEARNER_LICENSE_DTLS = "UPDATE `t_learner_license_dtls` a SET a.`Status`=? WHERE a.`Learner_License_Info_Id`=?";
	
	private static final String INSERT_LICENSE_SUSPENSION_DETAILS = "INSERT INTO `t_driving_license_suspension` "
																				+ "            ( `License_Id`, "
																				+ "             `Customer_Id`, "
																				+ "             `Start_Date`, "
																				+ "             `End_Date`, "
																				+ "             `Reason_For_Suspension`, "
																				+ "             `Created_On`, "
																				+ "             `Created_By`, "
																				+ "             `Updated_On`, "
																				+ "             `Updated_By`) "
																				+ "VALUES (?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?);";
	
	private static final String INSERT_LEARNER_LICENSE_SUSPENSION_DETAILS = "INSERT INTO `t_learner_license_suspension` "
																				+ "            ( `Learner_Id`, "
																				+ "             `Customer_Id`, "
																				+ "             `Start_Date`, "
																				+ "             `End_Date`, "
																				+ "             `Reason_For_Suspension`, "
																				+ "             `Created_On`, "
																				+ "             `Created_By`, "
																				+ "             `Updated_On`, "
																				+ "             `Updated_By`) "
																				+ "VALUES (?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?);";
	
	
	private static final String INSERT_DRIVE_TYPE_SUSPENSION_DETAILS = "INSERT INTO `t_drive_type_suspension` "
																				+ "            (`License_Id`, "
																				+ "             `Customer_Id`, "
																				+ "             `Drive_Type_Id`, "
																				+ "             `Start_Date`, "
																				+ "             `End_Date`, "
																				+ "             `Reason`, "
																				+ "             `Re_Issued_Date`, "
																				+ "             `Is_Reissued`, "
																				+ "             `Created_On`, "
																				+ "             `Created_By`, "
																				+ "             `Updated_On`, "
																				+ "             `Updated_By`) "
																				+ "VALUES (?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?, "
																				+ "        ?);";
	
	private static final String INSERT_OFFENCE_INFORMATION = "INSERT INTO `t_offence_information` ( "
															+ "  `Is_Foreign`, "
															+ "  `Vehicle_Id`, "
															+ "  `Learner_License_Id`, "
															+ "  `License_Id`, "
															+ "  `Offence_Date`, "
															+ "  `Time_Of_Inspection`, "
															+ "  `Inspected_By`, "
															+ "  `Inspection_Type`, "
															+ "  `Traffic_Branch`, "
															+ "  `Place_Of_Inspection`, "
															+ "  `Region_Id`, "
															+ "  `Is_License_Seized`, "
															+ "  `Is_Bluebook_Seized`, "
															+ "  `TIN_No`, "
															+ "  `Remarks`, "
															+ "  `Created_On`, "
															+ "  `Created_By`,Processed_Region_Id,Processed_Base_Id,`Is_Personal_New`,`Customer_Id` "
															+ ") "
															+ "VALUES "
															+ "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?, ?)";
	
	
	private static final String INSERT_OFFENCE_INFORMATION_FOREIGN="INSERT INTO t_offence_information ( "
																	+ "  Is_Foreign, "
																	+ "  Vehicle_Number, "
																	+ "  License_Number, "
																	+ "  Mobile_Number, "
																	+ "  Vehicle_Type, "
																	+ "  Owner_Name, "
																	+ "  Diver_Name, "
																	+ "  Offence_Date, "
																	+ "  Time_Of_Inspection, "
																	+ "  Inspected_By, "
																	+ "  Inspection_Type, "
																	+ "  Traffic_Branch, "
																	+ "  Place_Of_Inspection, "
																	+ "  Region_Id, "
																	+ "  Is_License_Seized, "
																	+ "  Is_Bluebook_Seized, "
																	+ "  TIN_No, "
																	+ "  Remarks, "
																	+ "  Created_On, "
																	+ "  Created_By,Processed_Region_Id,Processed_Base_Id,Address "
																	+ ") "
																	+ "VALUES "
																	+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?,?,?,?)";
	
	private static final String GET_OWNER_PHONE_NUMBER = "SELECT "
														+ "  b.`Present_Phone_No`, "
														+ "  b.`Present_Email`, "
														+ "  CONCAT( "
														+ "    b.`First_Name`, "
														+ "    ' ', "
														+ "    b.`Middle_Name`, "
														+ "    ' ', "
														+ "    b.`Last_Name` "
														+ "  ) pname, "
														+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
														+ "FROM "
														+ "  t_vehicle_registration_dtls a "
														+ "  LEFT JOIN `t_personal_dtls` b "
														+ "    ON a.`Customer_Id` = b.`Customer_Id` "
														+ "WHERE a.Vehicle_Reg_Dtls_Id = ?";
	
	private static final String GET_ORGANISATION_CONTACT_DTLS = "SELECT   a.`Vehicle_Number`,  b.`Organization_Name` pname,  b.`Email_Id` Present_Email," +
																	"  b.`Phone_Number` Present_Phone_No,  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate " +
																	"FROM  t_vehicle_registration_dtls a " +
																	"  LEFT JOIN t_organization_info b  " +
																	"   ON a.`Customer_Id` = b.`Customer_Id` WHERE a.`Vehicle_Reg_Dtls_Id` = ? ;";
	
	private static final String GET_VEHICLE_REGISTRATION_TYPE = "SELECT a.Vehicle_Registration_Type FROM t_vehicle_registration_dtls a WHERE a.Vehicle_Reg_Dtls_Id=?";
	
	private static final String INSERT_INTO_OFFENCE_DETAILS = "INSERT INTO `t_offence_information_dtls` (`Offence_Id`, `Offence_Type_Id`) VALUES (?, ?)";
	
	private static final String INSERT_INTO_T_LEARNER_LICENSE_DTLS ="INSERT INTO `t_learner_license_dtls` "
																		+ "            (`Application_Number`, "
																		+ "             `Customer_Id`, "
																		+ "             `Learner_License_No`, "
																		+ "             `Region_Id`, "
																		+ "             `Issue_Date`, "
																		+ "             `Receipt_No`, "
																		+ "             `Receipt_Date`, "
																		+ "             `Certifying_Doctor`, "
																		+ "             `Remarks`, "
																		+ "             `Delivered_On`, "
																		+ "             `Created_On`, "
																		+ "             `Created_By`, "
																		+ "             `Updated_On`, "
																		+ "             `Updated_By`) "
																		+ "SELECT "
																		+ "Application_Number, "
																		+ "`Customer_Id`, "
																		+ " `Learner_License_No`, "
																		+ "`Region_Id`, "
																		+ "`Receipt_Date`, "
																		+ "`Receipt_No`, "
																		+ "`Receipt_Date`, "
																		+ "`Certifying_Doctor`, "
																		+ "`Remarks`," 
																		+ "CURDATE(), "
																		+ "CURDATE(), "
																		+ "?, "
																		+ "`Updated_On`, "
																		+ "`Updated_By` "		 
																		+ "FROM " 
																		+ "t_learner_license_application " 
																		+" WHERE " 
																		+" Application_Number=?;";
	
	private static final String UPDATE_T_LEARNER_LICENSE_DTLS="UPDATE t_learner_license_dtls "
																+ " SET "
																+ " `Learner_License_No` = ? "
																+ " WHERE "
																+ " Application_Number=?";	
	
	private static final String UPDATE_LEARNER_LICENSE_TABLE="UPDATE t_learner_license_dtls "
																	+ " SET "
																	+ " `Delivered_On` = ? ,"
																	+ " `Expiry_Date` = ?," 
																	+ " Processed_Base_id=?," 
																	+ " Processed_Region_Id=? "
																	+ " WHERE "
																	+ " Application_Number=?";	
	
	
	private static final String GET_CUSTOMER_INFORMATION = "SELECT "
														+ "  CONCAT( "
														+ "    b.`First_Name`, "
														+ "    ' ', "
														+ "    b.`Middle_Name`, "
														+ "    ' ', "
														+ "    b.`Last_Name` "
														+ "  ) pname, "
														+ "  b.`Present_Email`, "
														+ "  b.`Present_Phone_No`, "
														+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
														+ "FROM "
														+ "  t_learner_license_dtls a "
														+ "  LEFT JOIN t_personal_dtls b "
														+ "    ON a.`Customer_Id` = b.`Customer_Id` "
														+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_CUSTOMER_INFORMATION_FOR_RENEWAL = "SELECT "
														+ "  CONCAT( "
														+ "    b.`First_Name`, "
														+ "    ' ', "
														+ "    b.`Middle_Name`, "
														+ "    ' ', "
														+ "    b.`Last_Name` "
														+ "  ) pname, "
														+ "  b.`Present_Email`, "
														+ "  b.`Present_Phone_No`, "
														+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
														+ "FROM "
														+ "  t_learner_license_renewal_dtls a "
														+ "  LEFT JOIN t_personal_dtls b "
														+ "    ON a.`Customer_Id` = b.`Customer_Id` "
														+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_CUSTOMER_INFORMATION_FOR_NON_COMMERCIAL = "SELECT "
																	+ "  CONCAT( "
																	+ "    b.`First_Name`, "
																	+ "    ' ', "
																	+ "    b.`Middle_Name`, "
																	+ "    ' ', "
																	+ "    b.`Last_Name` "
																	+ "  ) pname, "
																	+ "  b.`Present_Email`, "
																	+ "  b.`Present_Phone_No`, "
																	+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																	+ "FROM "
																	+ "  t_driving_license_dtls a "
																	+ "  LEFT JOIN t_personal_dtls b "
																	+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																	+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_CUSTOMER_INFORMATION_FROM_DRIVING_LICENSE = "SELECT "
																	+ "  CONCAT( "
																	+ "    b.`First_Name`, "
																	+ "    ' ', "
																	+ "    b.`Middle_Name`, "
																	+ "    ' ', "
																	+ "    b.`Last_Name` "
																	+ "  ) pname, "
																	+ "  b.`Present_Email`, "
																	+ "  b.`Present_Phone_No`, "
																	+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																	+ "FROM "
																	+ "  t_driving_license_dtls a "
																	+ "  LEFT JOIN t_personal_dtls b "
																	+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																	+ "WHERE a.`Driving_License_Id`=?";
	
	private static final String GET_CUSTOMER_INFORMATION_FOR_DRIVING_LICENSE_RENEWAL= "SELECT "
																		+ "  CONCAT( "
																		+ "    b.`First_Name`, "
																		+ "    ' ', "
																		+ "    b.`Middle_Name`, "
																		+ "    ' ', "
																		+ "    b.`Last_Name` "
																		+ "  ) pname, "
																		+ "  b.`Present_Email`, "
																		+ "  b.`Present_Phone_No`, "
																		+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																		+ "FROM "
																		+ "  t_driving_license_renewal_dtls a "
																		+ "  LEFT JOIN t_personal_dtls b "
																		+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																		+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_CUSTOMER_INFORMATION_FOR_DRIVING_LICENSE_ENDORSEMENT= "SELECT "
																				+ "  CONCAT( "
																				+ "    b.`First_Name`, "
																				+ "    ' ', "
																				+ "    b.`Middle_Name`, "
																				+ "    ' ', "
																				+ "    b.`Last_Name` "
																				+ "  ) pname, "
																				+ "  b.`Present_Email`, "
																				+ "  b.`Present_Phone_No`, "
																				+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																				+ "FROM "
																				+ "  t_driving_license_endorsement a "
																				+ "  LEFT JOIN t_personal_dtls b "
																				+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																				+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_CUSTOMER_INFORMATION_FOR_LEARNER_DUPLICATE= "SELECT "
																			+ "  CONCAT( "
																			+ "    b.`First_Name`, "
																			+ "    ' ', "
																			+ "    b.`Middle_Name`, "
																			+ "    ' ', "
																			+ "    b.`Last_Name` "
																			+ "  ) pname, "
																			+ "  b.`Present_Email`, "
																			+ "  b.`Present_Phone_No`, "
																			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																			+ "FROM "
																			+ "  t_learner_license_duplicate_dtls a "
																			+ "  LEFT JOIN t_personal_dtls b "
																			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																			+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_CUSTOMER_INFORMATION_FOR_DRIVING_LICENSE_DUPLICATE= "SELECT "
																			+ "  CONCAT( "
																			+ "    b.`First_Name`, "
																			+ "    ' ', "
																			+ "    b.`Middle_Name`, "
																			+ "    ' ', "
																			+ "    b.`Last_Name` "
																			+ "  ) pname, "
																			+ "  b.`Present_Email`, "
																			+ "  b.`Present_Phone_No`, "
																			+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																			+ "FROM "
																			+ "  t_driving_license_duplicate a "
																			+ "  LEFT JOIN t_personal_dtls b "
																			+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																			+ "WHERE a.`Application_Number` = ?";
	

	private static final String GET_CUSTOMER_INFORMATION_FOR_TOP= "SELECT "
		+ "  CONCAT( "
		+ "    b.`First_Name`, "
		+ "    ' ', "
		+ "    b.`Middle_Name`, "
		+ "    ' ', "
		+ "    b.`Last_Name` "
		+ "  ) pname, "
		+ "  b.`Present_Email`, "
		+ "  b.`Present_Phone_No`, "
		+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
		+ "FROM "
		+ "  t_top_application a "
		+ "  LEFT JOIN t_personal_dtls b "
		+ "    ON a.`Customer_Id` = b.`Customer_Id` "
		+ "WHERE a.`Application_Number` = ?";
																		
	private static final String INSERT_LEARNER_LICENSE_DRIVETYPE="INSERT INTO `t_learner_licn_drive_type` ( "
																	+ "  `Application_Number`, "
																	+ "  `Learner_License_Id`, "
																	+ "  `Drive_Type_Id`, "
																	+ "  `Created_By`, "
																	+ "  `Created_On` "
																	+ ") "
																	+ "(SELECT "
																	+ "  a.`Application_Number`, "
																	+ "  (SELECT "
																	+ "    `Learner_License_Info_Id` "
																	+ "  FROM "
																	+ "    `t_learner_license_dtls` "
																	+ "  WHERE `Application_Number` = ?), "
																	+ "  a.`Drive_Type_Id`, "
																	+ "  a.`Created_By`, "
																	+ "  a.`Created_On` "
																	+ "FROM "
																	+ "  `t_learner_licn_drive_type_application` a "
																	+ "WHERE a.Application_Number = ?)";
	
	
	private static final String INSERT_INTO_T_PRINT_DTLS="INSERT INTO t_print_dtls ( "
														+ "  Application_Number, "
														+ "  Document_Type, "
														+ "  Service_Id, "
														+ "  Jurisdiction_Id, "
														+ "  Identity_Number, "
														+ "  Identity_Id, "
														+ "  Jurisdiction_Type_Id "
														+ ") "
														+ " SELECT "
														+ "  a.Application_Number, "
														+ "  'LL', "
														+ "  (SELECT "
														+ "    Service_Id "
														+ "  FROM "
														+ "    t_workflow_dtls "
														+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
														+ "  (SELECT "
														+ "    Juris_Id "
														+ "  FROM "
														+ "    t_workflow_dtls "
														+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
														+ "   a.`Learner_License_No`," 
														+ "   a.`Learner_License_Info_Id`,"
														+ "  (SELECT "
														+ "    Juris_Type_Id "
														+ "  FROM "
														+ "    t_workflow_dtls "
														+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
														+ " FROM "
														+ "  t_learner_license_dtls a "
														+ " WHERE a.Application_Number = ? ;";
		/*"INSERT INTO `t_print_dtls` ( "
															+ "  `Application_Number`, "
															+ "  `Document_Type`, "
															+ "  `Service_Id`, "
															+ "  `Identity_Number`, "
															+ "  `Region_Id` "
															+ ") "
															+ "SELECT "
															+ "  a.`Application_Number`, "
															+ "  'LL', "
															+ "  (SELECT "
															+ "    Service_Id "
															+ "  FROM "
															+ "    `t_workflow_dtls` "
															+ "  WHERE Application_Number = a.Application_Number) serviceId, "
															+ "  a.Learner_License_No, "
															+ "  a.`Region_Id` "
															+ "FROM "
															+ "  `t_learner_license_dtls` a "
															+ "WHERE a.Application_Number = ?;";	
	*/
	private static final String INSERT_INTO_T_LEARNER_LICENSE_RENEWAL_DTLS ="INSERT INTO `t_learner_license_renewal_dtls` "
																		    + "            (Application_Number ,"
																			+ "             Customer_Id ,"
																			+ "             Learner_License_Id ,"
																			+ "             Receipt_No ,"
																			+ "             Receipt_Date, "
																			+ "             Remarks ,"
																			+ "             Created_On, "
																			+ "             Created_By ,"
																			+ "             Updated_On ,"
																			+ "             Updated_By) "
																		+ "SELECT "
																		+ "Application_Number, "
																		+ "`Customer_Id`, "
																		+ " `Learner_License_Id`, "
																		+ "`Receipt_No`, "
																		+ "`Receipt_Date`, "
																		+ "`Remarks`," 
																		+ "`Create_On`, "
																		+ "`Created_By`, "
																		+ "`Updated_On`, "
																		+ "`Updated_By` "		 
																		+ "FROM " 
																		+ "t_learner_license_application " 
																		+" WHERE " 
																		+" Application_Number=?;";

	
	private static final String UPDATE_LEARNER_LICENSE_RENEWAL_DTLS="UPDATE t_learner_license_renewal_dtls "
																	+ " SET "
																	+ " `Renewal_Date` = ? ,"
																	+ " `Delivered_On` = ? ,"
																	+ " `Expiry_Date` = ?," 
																	+ " Processed_Base_id=?," 
																	+ " Processed_Region_Id=? "
																	+ " WHERE "
																	+ " Application_Number=?";
	
	private static final String INSERT_LEARNER_RENEWAL_PRINT_DTLS ="INSERT INTO `t_print_dtls` ( "
																	+ "  Application_Number, "
																	+ "  Document_Type, "
																	+ "  Service_Id, "
																	+ "  Jurisdiction_Id, "
																	+ "  Identity_Number, "
																	+ "  Identity_Id, "
																	+ "  Jurisdiction_Type_Id "
																	+ ") "
																	+ "SELECT "
																	+ "  a.`Application_Number`, "
																	+ "  'LL', "
																	+ "  (SELECT "
																	+ "    Service_Id "
																	+ "  FROM "
																	+ "    t_workflow_dtls "
																	+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
																	+ "  (SELECT "
																	+ "    Juris_Id "
																	+ "  FROM "
																	+ "    t_workflow_dtls "
																	+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
																	+ "  b.Learner_License_No, "
																	+ "  a.`Learner_License_Id`, "
																	+ "  (SELECT "
																	+ "    Juris_Type_Id "
																	+ "  FROM "
																	+ "    t_workflow_dtls "
																	+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
																	+ "FROM "
																	+ "  `t_learner_license_renewal_dtls` a "
																	+ "  LEFT JOIN `t_learner_license_dtls` b "
																	+ "    ON b.`Learner_License_Info_Id` = a.`Learner_License_Id` "
																	+ "WHERE a.Application_Number = ? ;";

	private static final String INSERT_INTO_T_DRIVING_LICENSE_NONCOMMERCIAL_DTLS="INSERT INTO `t_driving_license_dtls` "
		+ "            (`Application_Number`, "
		+ "             `Driving_License_Type_Id`, "
		+ "             `Learner_License_No`, "
		+ "             `Customer_Id`, "
		+ "             `Driving_License_No`, "
		+ "             `Region_Id`, "
		+ "             `Receipt_Number`," 
		+"				`Issue_Date`, "
		+ "             `Receipt_Date`, "
		+ "             `Status`, "
		+ "             `Test_Marks`, "
		+ "             `Register_Number`, "
		+ "             `Remarks`, "
		+ "             `Created_On`, "
		+ "             `Created_By`, "
		+ "             `Updated_On`, "
		+ "             `Updated_By`,Issue_type,IID) "
		
		+ "SELECT "
		+ "Application_Number, "
		+ "`Driving_license_type`, "
		+ " `Learner_License_No`, "
		+ "`Customer_Id`, "
		+ " `Driving_License_No`, "
		+ "`Region_Id`, "
		+ "`Receipt_Number`, "
		+ "`Receipt_Date`, "
		+ "`Receipt_Date`, "
		+ "`Status`, "
		+ "`Test_Marks`, "
		+ "`Register_Number`, "
		+ "`Remarks`," 
		+ "`Created_On`, "
		+ "`Created_By`, "
		+ "`Updated_On`, "
		+ "`Updated_By`,Issue_type,RIGHT(DATE_FORMAT(`Receipt_Date`,'%Y'), 2) "		 
		+ "FROM " 
		+ "t_driving_license_application " 
		+" WHERE " 
		+" Application_Number=?;";

	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_DRIVE_TYPE = "INSERT INTO `t_driving_license_drive_type` ( "
																			+ "  `Application_Number`, "
																			+ "  `Customer_Id`, "
																			+ "  `Drive_Type_Id`, "
																			+ "  `Drive_Type_Category` "
																			+ ") "
																			+ "SELECT "
																			+ "  a.`Application_Number`, "
																			+ "  a.`Customer_Id`, "
																			+ "  a.`Drive_Type_Id`, "
																			+ "  (SELECT "
																			+ "    `Drive_Type_Category` "
																			+ "  FROM "
																			+ "    `t_drive_type_master` "
																			+ "  WHERE `Drive_Type_Id` = a.Drive_Type_Id) "
																			+ "FROM "
																			+ "  `t_driving_license_application` a "
																			+ "WHERE a.`Application_Number` = ?";
	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_DRIVE_FOR_ENDORSEMENT = "INSERT INTO `t_driving_license_drive_type` ( "
		+ "  `Application_Number`, "
		+ "  `Customer_Id`, "
		+ "  `License_Id`, "
		+ "  `Drive_Type_Id`, "
		+ "  `Drive_Type_Category` "
		+ ") "
		+ "SELECT "
		+ "  a.`Application_Number`, "
		+ "  a.`Customer_Id`, "
		+ "  a.License_Id, "
		+ "  a.`Drive_Type_Id`, "
		+ "  (SELECT "
		+ "    `Drive_Type_Category` "
		+ "  FROM "
		+ "    `t_drive_type_master` "
		+ "  WHERE `Drive_Type_Id` = a.Drive_Type_Id) "
		+ "FROM "
		+ "  `t_driving_license_application` a "
		+ "WHERE a.`Application_Number` = ?";
	
	private static final String GET_ENDORSEMENT_APPLICATION_DETAILS = "SELECT "
		+ "  a.`Application_Number`, "
		+ "  a.`Customer_Id`, "
		+ "  a.`License_Id`, "
		+ "  a.`Drive_Type_Id`, "
		+ "  a.`Is_TCB_Endorsement` "
		+ "FROM "
		+ "  t_driving_license_application a "
		+ "WHERE a.`Application_Number` = ?";
	
	private static final String INSERT_INTO_DRIVE_TYPE_FOR_TCB_ENDORSEMENT = "INSERT INTO `t_driving_license_drive_type` ( "
		+ "  `Application_Number`, "
		+ "  `Customer_Id`, "
		+ "  `License_Id`, "
		+ "  `Drive_Type_Id`, "
		+ "  `Drive_Type_Category` "
		+ ") "
		+ "VALUES "
		+ "  (?, ?, ?, ?, ?)";
	
	private static final String GET_DRIVE_TYPE_CATEGORY = "SELECT a.`Drive_Type_Category` FROM t_drive_type_master a WHERE a.`Drive_Type_Id`=?";
	
	private static final String UPDATE_T_DRIVING_LICENSE_DRIVE_TYPE = "UPDATE `t_driving_license_drive_type` a SET a.`License_Id`=? WHERE a.`Application_Number`=?";
	
	private static final String UPDATE_T_DRIVING_LICENSE_DRIVE_TYPE_WITH_ENDORSEMENT_ID = "UPDATE `t_driving_license_drive_type` a SET a.`Endorsement_Id`=? WHERE a.`Application_Number`=?";
	
	private static final String UPDATE_T_NONCOMMERCIAL_LICENSE_DTLS = "UPDATE t_driving_license_dtls "
																		+ " SET "
																		+ " `Driving_License_No` = ? ,"
																		+ " `Status` = ? ,"
																		//+ " `IID` = ? ,"
																		+ " `Expiry_Date` = ? ,"
																		+	"Processed_Base_Id=?," 
																		+	"Processed_Region_Id=?"
																		+ " WHERE "
																		+ " Application_Number=?";	
	
	
	private static final String UPDATE_T_NONCOMMERCIAL_LICENSE_TEST_MARKS_DTLS= "UPDATE "
																				+ "	   t_driving_license_dtls "
																				+ "	 SET "
																				+ "	   Test_Marks = "
																				+ "	   (SELECT "
																				+ "	     Overall_Marks_Obtained "
																				+ "	   FROM "
																				+ "	     t_etest_application "
																				+ "	   WHERE Theory_Test_Status = 'P' AND Practical_Test_Status= 'P' AND Learner_License_No=?)";
	
	private static final String INSERT_NON_COMMERCIAL_PRINT_DTLS ="INSERT INTO t_print_dtls ( Application_Number,Document_Type,Service_Id,Jurisdiction_Id," +
																"Identity_Number,Identity_Id, Jurisdiction_Type_Id)" +
																" SELECT a.Application_Number,  'DL',  (SELECT Service_Id FROM t_workflow_dtls  " +
																" WHERE Application_Number = a.Application_Number) AS Service_Id,  " +
																"(SELECT Juris_Id FROM t_workflow_dtls   " +
																"WHERE Application_Number = a.Application_Number) AS Juris_Id, " +
																" a.`Driving_License_No`,  a.`Driving_License_Id`, " +
																" (SELECT Juris_Type_Id  FROM   t_workflow_dtls   " +
																"WHERE Application_Number = a.Application_Number) AS Juris_Type_Id FROM " +
																" `t_driving_license_dtls` a " +
																" WHERE a.Application_Number = ? ";	
	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_COMMERCIAL_DTLS="INSERT INTO `t_driving_license_dtls` "
																						+ "            (`Application_Number`, "
																						+ "             `Driving_License_Type_Id`, "
																						+ "             `Learner_License_No`, "
																						+ "             `Customer_Id`, "
																						+ "             `Region_Id`, "
																						+ "             `Receipt_Number`," 
																						+ "				`Issue_Date`, "
																						+ "             `Receipt_Date`, "
																						+ "             `Status`, "
																						+ "             `Test_Marks`, "
																						+ "             `Register_Number`, "
																						+ "             `Remarks`, "
																						+ "             `Created_On`, "
																						+ "             `Created_By`, "
																						+ "             `Updated_On`, "
																						+ "             `Updated_By`,IID) "
																						+ "SELECT "
																						+ "Application_Number, "
																						+ "`Driving_license_type`, "
																						+ " `Learner_License_No`, "
																						+ "`Customer_Id`, "
																						+ "`Region_Id`, "
																						+ "`Receipt_Number`, "
																						+ "`Receipt_Date`, "
																						+ "`Receipt_Date`, "
																						+ "`Status`, "
																						+ "`Test_Marks`, "
																						+ "`Register_Number`, "
																						+ "`Remarks`," 
																						+ "`Created_On`, "
																						+ "`Created_By`, "
																						+ "`Updated_On`, "
																						+ "`Updated_By`,RIGHT(DATE_FORMAT(`Receipt_Date`,'%Y'), 2) "		 
																						+ "FROM " 
																						+ "t_driving_license_application " 
																						+" WHERE " 
																						+" Application_Number=?";
	
	private static final String UPDATE_T_COMMERCIAL_DRIVING_LICENSE_DTLS="UPDATE t_driving_license_dtls "
																		+ " SET "
																		+ " `Driving_License_No` = ?,"
																		+ " `Status` = ?,"
																	//	+ " `Issue_Date` = ?,"
																		+ " `Expiry_Date` = ?"
																	//	+ " `IID` = ?"
																		+ " WHERE "
																		+ " Application_Number=?";
	
	private static final String INSERT_COMMERCIAL_PRINT_DTLS ="INSERT INTO `t_print_dtls` ( "
															+ "  Application_Number, "
															+ "  Document_Type, "
															+ "  Service_Id, "
															+ "  Jurisdiction_Id, "
															+ "  Identity_Number, "
															+ "  Identity_Id, "
															+ "  Jurisdiction_Type_Id "
															+ ") "
															+ "SELECT "
															+ "  a.`Application_Number`, "
															+ "  'DL', "
															+ "   "
															+ "  (SELECT "
															+ "    Service_Id "
															+ "  FROM "
															+ "    t_workflow_dtls "
															+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
															+ "   "
															+ "  (SELECT "
															+ "    Juris_Id "
															+ "  FROM "
															+ "    t_workflow_dtls "
															+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
															+ "  a.`Driving_License_No`, "
															+ "  a.`Driving_License_Id`, "
															+ "   "
															+ "  (SELECT "
															+ "    Juris_Type_Id "
															+ "  FROM "
															+ "    t_workflow_dtls "
															+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
															+ "FROM "
															+ "  `t_driving_license_dtls` a "
															+ "WHERE a.Application_Number = ?";
	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_RENEWAL_DTLS="INSERT INTO `t_driving_license_renewal_dtls` "
																				+ "(`Application_Number`, "
																				+ "`Customer_Id`, "
																				+ "`License_Id`, "
																				+ "`Renewal_Upon_MC_Flag`, "
																				+ "`Receipt_No`,"
																				+ "`Receipt_Date`, "
																				+ "`Renewal_Date`, "
																				+ "`Remarks`, "
																				+ "`Created_On`, "
																				+ " `Created_By`, "
																				+ " `Updated_On`, "
																				+ " `Updated_By`) "
																				+ "SELECT  "
																				+ "Application_Number, "
																				+ "`Customer_Id`, "
																				+ "`License_Id`, "
																				+ "`Is_Renewal_Upon_MC`, "
																				+ "`Receipt_Number`, "
																				+ " `Receipt_Date`, "
																				+ " `Receipt_Date`, "
																				+ "`Remarks`,"
																				+ " 	CURDATE(), "
																				+ " ?, "
																				+ " `Updated_On`, "
																				+ " `Updated_By` "
																				+ "FROM " 
																				+ "t_driving_license_application " 
																				+" WHERE " 
																				+" Application_Number=?;";
	
	private static final String UPDATE_T_DRIVING_LICENSE_RENEWAL_DTLS="UPDATE t_driving_license_renewal_dtls "
																+ "SET "
																+ "`Expiry_Date` = ?, "
																+ "`Delivered_On`=?,"
																+"Processed_Base_Id=?," 
																+"Processed_Region_Id=?"
																+ " WHERE "
																+ "Application_Number=?";
	
	private static final String GET_LICENSE_TYPE = "SELECT "
													+ "  a.`Customer_Id`, "
													+ "  a.`License_Id`, "
													+ "  b.`Driving_License_Type_Id` "
													+ "FROM "
													+ "  `t_driving_license_renewal_dtls` a "
													+ "  LEFT JOIN `t_driving_license_dtls` b "
													+ "    ON a.`License_Id` = b.`Driving_License_Id` "
													+ "WHERE a.Application_Number = ?";
	
	private static final String GET_ORDINARY_LICENSE_ID = "SELECT "
															+ "  a.`Driving_License_Id` "
															+ "FROM "
															+ "  t_driving_license_dtls a "
															+ "WHERE a.`Customer_Id` = ? "
															+ "  AND a.`Driving_License_Id` <> ?";
	
	private static final String GET_MAX_EXPIRY_FOR_LICENSE = "SELECT "
		+ "  IF( "
		+ "    a.`Driving_License_Id` IN "
		+ "    (SELECT "
		+ "      `License_Id` "
		+ "    FROM "
		+ "      `t_driving_license_renewal_dtls` "
		+ "    WHERE `License_Id` = ?), "
		+ "    (SELECT "
		+ "      MAX(`Expiry_Date`) "
		+ "    FROM "
		+ "      `t_driving_license_renewal_dtls` "
		+ "    WHERE `License_Id` = ?), "
		+ "    a.`Expiry_Date` "
		+ "  ) expiryDate "
		+ "FROM "
		+ "  t_driving_license_dtls a "
		+ "WHERE a.`Driving_License_Id` = ?";
	
	private static final String AUTO_INSERT_ORDINARY_LICENSE_RENEWAL_DETAILS = "INSERT INTO `t_driving_license_renewal_dtls` ( "
		+ "  `Application_Number`, "
		+ "  `Customer_Id`, "
		+ "  `License_Id`, "
		+ "  `Renewal_Date`, "
		+ "  `Expiry_Date`, "
		+ "  `Remarks`, "
		+ "  `Created_On`, "
		+ "  `Created_By`, "
		+ "  `Receipt_No`, "
		+ "  `Receipt_Date` "
		+ ") "
		+ "VALUES "
		+ "  ( "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    ?, "
		+ "    (SELECT "
		+ "      Receipt_Number "
		+ "    FROM "
		+ "      t_driving_license_application "
		+ "    WHERE Application_Number = ?), "
		+ "    (SELECT "
		+ "      Receipt_Date "
		+ "    FROM "
		+ "      t_driving_license_application "
		+ "    WHERE Application_Number = ?) "
		+ "  )";
	
	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_ENDORSEMENT_DTLS="INSERT INTO `t_driving_license_endorsement` "
																				+ "(`Application_Number`, "
																				+ "`Customer_Id`, "
																				+ "`License_Id`, "
																				+ "`Endorsed_Date`, "
																				+ "`Receipt_No`, "
																				+ "`Receipt_Date`, "
																				+ "`Test_Marks`, "
																				+ "`IID`, "
																				+ "`Remarks`, "
																				+ "`Created_On`, "
																				+ " `Created_By`, "
																				+ " `Updated_On`, "
																				+ " `Updated_By`) "
																				+ "SELECT  "
																				+ "Application_Number, "
																				+ "`Customer_Id`, "
																				+ "`License_Id`, "
																				+ "`Receipt_Date`, "
																				+ "`Receipt_Number`, "
																				+ " `Receipt_Date`, "
																				+ "`Test_Marks`, "
																				+ "`IID`, "
																				+ "`Remarks`,"
																				+ " `Created_On`, "
																				+ " `Created_By`, "
																				+ " `Updated_On`, "
																				+ " `Updated_By` "
																				+ "FROM " 
																				+ "t_driving_license_application " 
																				+" WHERE " 
																				+" Application_Number=?;";
	
	private static final String UPDATE_T_DRIVING_LICENSE_ENDORSEMENT_DTLS="UPDATE t_driving_license_endorsement "
																	+ " SET "
																	+ "`Endorsed_Date` = ?, "
																	+ "`IID` = ?, "
																	+ "Processed_Base_Id=?,"
																	+ "Processed_Region_Id=?"
																	+ " WHERE "
																	+ " Application_Number=?";
	
	
	
	private static final String INSERT_INTO_T_DRIVING_LICENSE_DUPLICATE_DTLS="INSERT INTO `t_driving_license_duplicate` "
																					+ "(`Application_Number`, "
																					+ "`Customer_Id`, "
																					+ "`License_Id`, "
																					+ "`Receipt_No`, "
																					+ "`Receipt_Date`, "
																					+ "`Remarks`, "
																					+ "`Created_On`, "
																					+ " `Created_By`, "
																					+ " `Updated_On`, "
																					+ " `Updated_By`) "
																					+ "SELECT  "
																					+ "Application_Number, "
																					+ "`Customer_Id`, "
																					+ "`License_Id`, "
																					+ "`Receipt_Number`, "
																					+ " `Receipt_Date`, "
																					+ "`Remarks`,"
																					+ " `Created_On`, "
																					+ " `Created_By`, "
																					+ " `Updated_On`, "
																					+ " `Updated_By` "
																					+ "FROM " 
																					+ "t_driving_license_application " 
																					+" WHERE " 
																					+" Application_Number=?;";
	
		private static final String UPDATE_T_DRIVING_LICENSE_DUPLICATE_DTLS="UPDATE t_driving_license_duplicate "
																		+ " SET "
																		+ "`Duplication_Date` = ?, "
																		+ "`Delivered_On`=?," 
																		+"Processed_Region_Id=?,"
																		+"Processed_Base_Id=?" 
																		+ " WHERE "
																		+ " Application_Number=?";

			private static final String UPDATE_DUPLICATE_LICENSE_NO="UPDATE "
																	+ "  t_driving_license_duplicate "
																	+ "SET "
																	+ "  License_No = "
																	+ "  (SELECT "
																	+ "    Driving_License_No "
																	+ "  FROM "
																	+ "    t_driving_license_dtls "
																	+ "  WHERE Customer_Id = ?)";

	
		private static final String INSERT_INTO_T_LEARNER_LICENSE_DUPLICATE_DTLS="INSERT INTO `t_learner_license_duplicate_dtls` "
																	+ "(`Application_Number`, "
																	+ "`Learner_License_Id`, "
																	+ "`Receipt_No`, "
																	+ "`Receipt_Date`, "
																	+ "`Remarks`, "
																	+ "`Created_On`, "
																	+ " `Created_By`, "
																	+ " `Updated_On`, "
																	+ " `Updated_By`) "
																	+ "SELECT  "
																	+ "Application_Number, "
																	+ "`Learner_License_Id`, "
																	+ "`Receipt_No`, "
																	+ " `Receipt_Date`, "
																	+ "`Remarks`,"
																	+ " `Create_On`, "
																	+ " `Created_By`, "
																	+ " `Updated_On`, "
																	+ " `Updated_By` "
																	+ "FROM " 
																	+ "t_learner_license_application " 
																	+" WHERE " 
																	+" Application_Number=?;";
		
		private static final String UPDATE_LEARNER_LICENSE_DUPLICATE_DTLS="UPDATE t_learner_license_duplicate_dtls "
																			+ "SET "
																			+ "`Issue_Date` = ?, "
																			+ "`Delivered_On`=?," 
																			+"Processed_Base_Id=?," 
																			+"Processed_Region_Id=?"
																			+ " WHERE "
																			+ " Application_Number=?";
		
		
		private static final String INSERT_DUPLICATE_PRINT_DTLS ="INSERT INTO t_print_dtls ( "
																+ "  Application_Number, "
																+ "  Document_Type, "
																+ "  Service_Id, "
																+ "  Jurisdiction_Id, "
																+ "  Identity_Number, "
																+ "  Identity_Id, "
																+ "  Jurisdiction_Type_Id "
																+ ") "
																+ "SELECT "
																+ "  a.Application_Number, "
																+ "  'DLL', "
																+ "  (SELECT "
																+ "    Service_Id "
																+ "  FROM "
																+ "    t_workflow_dtls "
																+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
																+ "  (SELECT "
																+ "    Juris_Id "
																+ "  FROM "
																+ "    t_workflow_dtls "
																+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
																+ "  d.`Learner_License_No`, "
																+ "  a.Learner_License_Id, "
																+ "  (SELECT "
																+ "    Juris_Type_Id "
																+ "  FROM "
																+ "    t_workflow_dtls "
																+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
																+ "FROM "
																+ "  `t_learner_license_duplicate_dtls` a "
																+ "  LEFT JOIN `t_learner_license_dtls` d "
																+ "    ON d.`Learner_License_Info_Id` = a.`Learner_License_Id` "
																+ "WHERE a.Application_Number = ?";
		
		private static final String INSERT_DRIVING_RENEWAL_PRINT_DTLS ="INSERT INTO `t_print_dtls` ( "
			+ "  Application_Number, "
			+ "  Document_Type, "
			+ "  Service_Id, "
			+ "  Jurisdiction_Id, "
			+ "  Identity_Number, "
			+ "  Identity_Id, "
			+ "  Jurisdiction_Type_Id "
			+ ") "
			+ "SELECT "
			+ "  a.`Application_Number`, "
			+ "  'DL', "
			+ "  (SELECT "
			+ "    Service_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
			+ "  (SELECT "
			+ "    Juris_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
			+ "  (SELECT "
			+ "    `Driving_License_No` "
			+ "  FROM "
			+ "    `t_driving_license_dtls` "
			+ "  WHERE `Driving_License_Id` = a.`License_Id`) Driving_License_No, "
			+ "  a.`License_Id`, "
			+ "  (SELECT "
			+ "    Juris_Type_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
			+ "FROM "
			+ "  `t_driving_license_renewal_dtls` a "
			+ "WHERE a.Application_Number = ?";
		
		private static final String INSERT_ENDORSEMENT_PRINT_DTLS ="INSERT INTO t_print_dtls ("
			+ "  Application_Number,"
			+ "  Document_Type,"
			+ "												  Service_Id,"
			+ "												  Jurisdiction_Id,"
			+ "												  Identity_Number,"
			+ "												  Identity_Id,"
			+ "												  Jurisdiction_Type_Id"
			+ "												) "
			+ "												SELECT "
			+ "												  a.Application_Number,"
			+ "												  'DL',"
			+ "												  (SELECT "
			+ "												    Service_Id "
			+ "												  FROM"
			+ "												    t_workflow_dtls "
			+ "												  WHERE Application_Number = a.Application_Number) AS Service_Id,"
			+ "												  (SELECT "
			+ "												    Juris_Id "
			+ "												  FROM"
			+ "												    t_workflow_dtls "
			+ "												  WHERE Application_Number = a.Application_Number) AS Juris_Id,"
			+ "												  b.`Driving_License_No`,"
			+ "												  b.`Driving_License_Id`,"
			+ "												  (SELECT "
			+ "												    Juris_Type_Id "
			+ "												  FROM"
			+ "												    t_workflow_dtls "
			+ "												  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
			+ "												FROM"
			+ "												  `t_driving_license_endorsement` a "
			+ "												  LEFT JOIN `t_driving_license_dtls` b ON b.`Driving_License_Id`=a.`License_Id`"
			+ "												WHERE a.Application_Number = ? ;";
		
		
		private static final String INSERT_DUPLICATE_DL_INTO_PRINT_LIST = "INSERT INTO t_print_dtls ( "
			+ "  Application_Number, "
			+ "  Document_Type, "
			+ "  Service_Id, "
			+ "  Jurisdiction_Id, "
			+ "  Identity_Number, "
			+ "  Identity_Id, "
			+ "  Jurisdiction_Type_Id "
			+ ") "
			+ "SELECT "
			+ "  a.Application_Number, "
			+ "  'DL', "
			+ "  (SELECT "
			+ "    Service_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
			+ "  (SELECT "
			+ "    Juris_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
			+ "  b.`Driving_License_No`, "
			+ "  b.`Driving_License_Id`, "
			+ "  (SELECT "
			+ "    Juris_Type_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
			+ "FROM "
			+ "  `t_driving_license_duplicate` a "
			+ "  LEFT JOIN `t_driving_license_dtls` b "
			+ "    ON b.`Driving_License_Id` = a.`License_Id` "
			+ "WHERE a.Application_Number = ?";



		private static final String INSERT_TOP_DETAILS="INSERT INTO `t_top_application` "
														+ "            (`Application_Number`, "
														+ "             `Application_Type`, "
														+ "             `Customer_Id`, "
														+ "             `License_Id`, "
														+ "             `Vehicle_Id`, "
														+ "             `TOP_Number`, "
														+ "             `Region_Id`, "
														+ "             `Dzongkhag_Id`, "
														+ "             `Exact_Location`, "
														+ "             `Receipt_Number`, "
														+ "             `Receipt_Date`, "
														+ "             `App_Submission_Date`, "
														+ "             `Created_By`,Amount) "
														+ "VALUES (?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        ?, "
														+ "        CURRENT_TIMESTAMP, "
														+ "        ?,?);";
		private static final String INSERT_INTO_T_TOP_DTLS = "INSERT INTO `t_top_dtls` ( "
			+ "  `Application_Number`, "
			+ "  `Customer_Id`, "
			+ "  `License_Id`, "
			+ "  `Vehicle_Id`, "
			+ "  `TOP_Number`, "
			+ "  `Region_Id`, "
			+ "  `Dzongkhag_Id`, "
			+ "  `Exact_Location`, "
			+ "  `Receipt_Number`, "
			+ "  `Receipt_Date`, "
			+ "  `Amount`, "
			+ "  `Created_By`, "
			+ "  Issue_Date "
			+ ") "
			+ "SELECT "
			+ "  Application_Number, "
			+ "  `Customer_Id`, "
			+ "  `License_Id`, "
			+ "  `Vehicle_Id`, "
			+ "  `TOP_Number`, "
			+ "  `Region_Id`, "
			+ "  `Dzongkhag_Id`, "
			+ "  `Exact_Location`, "
			+ "  `Receipt_Number`, "
			+ "  `Receipt_Date`, "
			+ "  `Amount`, "
			+ "  `Created_By`, "
			+ "  NOW() "
			+ "FROM "
			+ "  t_top_application "
			+ "WHERE Application_Number = ?";
		
		private static final String INSERT_INTO_T_TOP_REPLACEMENT_DTLS = "INSERT INTO `t_top_replacement_dtls` ( "
			+ "  `Application_Number`, "
			+ "  `TOP_Number`, "
			+ "  `Customer_Id`, "
			+ "  `Replacement_Issue_Date`, "
			+ "  `Amount`, "
			+ "  `Receipt_No`, "
			+ "  `Receipt_Date`, "
			+ "  `Created_By`, "
			+ "  `Created_On` "
			+ ") "
			+ "SELECT "
			+ "  `Application_Number`, "
			+ "  `TOP_Number`, "
			+ "  `Customer_Id`, "
			+ "  CURRENT_TIMESTAMP, "
			+ "  `Amount`, "
			+ "  `Receipt_Number`, "
			+ "  `Receipt_Date`, "
			+ "  `Created_By`, "
			+ "  CURRENT_TIMESTAMP "
			+ "FROM "
			+ "  `t_top_application` "
			+ "WHERE Application_Number = ?";
		
		private static final String INSERT_TOP_REPLACEMENT_INTO_PRINT_LIST = "INSERT INTO t_print_dtls ( "
			+ "  Application_Number, "
			+ "  Document_Type, "
			+ "  Service_Id, "
			+ "  Jurisdiction_Id, "
			+ "  Identity_Number, "
			+ "  Identity_Id, "
			+ "  Jurisdiction_Type_Id "
			+ ") "
			+ "SELECT "
			+ "  a.Application_Number, "
			+ "  'TOP', "
			+ "  (SELECT "
			+ "    Service_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
			+ "  (SELECT "
			+ "    Juris_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
			+ "  a.TOP_Number, "
			+ "  d.Top_Issue_Id, "
			+ "  (SELECT "
			+ "    Juris_Type_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
			+ "FROM "
			+ "  t_top_replacement_dtls a "
			+ "  LEFT JOIN t_top_dtls d "
			+ "    ON d.TOP_Number = a.TOP_Number "
			+ "WHERE a.Application_Number = ?";
		
		private static final String INSERT_TOP_ISSUANCE_INTO_PRINT_LIST = "INSERT INTO t_print_dtls ( "
			+ "  Application_Number, "
			+ "  Document_Type, "
			+ "  Service_Id, "
			+ "  Jurisdiction_Id, "
			+ "  Identity_Number, "
			+ "  Identity_Id, "
			+ "  Jurisdiction_Type_Id "
			+ ") "
			+ "SELECT "
			+ "  a.Application_Number, "
			+ "  'TOP', "
			+ "  (SELECT "
			+ "    Service_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Service_Id, "
			+ "  (SELECT "
			+ "    Juris_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Id, "
			+ "  a.TOP_Number, "
			+ "  a.Top_Issue_Id, "
			+ "  (SELECT "
			+ "    Juris_Type_Id "
			+ "  FROM "
			+ "    t_workflow_dtls "
			+ "  WHERE Application_Number = a.Application_Number) AS Juris_Type_Id "
			+ "FROM "
			+ "  t_top_dtls a "
			+ "WHERE a.Application_Number = ?";
		
		private static final String UPDATE_TOP_DTLS_WITH_PROCESSED_REGION = "UPDATE t_top_dtls a SET a.`Processed_Region_Id`=?, a.`Processed_Base_Id`=?, a.Expiry_Date=? WHERE a.`Application_Number`=?";
		
		private static final String GET_VEHICLE_ID = "SELECT a.`Vehicle_Id` FROM t_top_application a WHERE a.`Application_Number`=?";
		
		private static final String GET_REGISTRATION_DATE_OF_VEHICLE = "SELECT a.`Registration_Date` FROM t_vehicle_registration_dtls a WHERE a.`Vehicle_Reg_Dtls_Id`=?";
		
		private static final String UPDATE_NON_COMMERCIAL_DRIVING_LICENSE_STATUS="UPDATE `t_driving_license_dtls` SET `Status` = 'I' WHERE `Driving_License_Id` = ?;";
		
		private static final String CHECK_DRIVE_TYPE_FROM_LICENSE_APPLICATION	=	"SELECT "
																					+ "  b.`Description` "
																					+ "FROM "
																					+ "  `t_driving_license_application` a "
																					+ "  LEFT JOIN `t_drive_type_master` b ON b.`Drive_Type_Id`=a.`Drive_Type_Id` "
																					+ "WHERE a.`Application_Number` =?";
		
		private static final String UPDATE_REJECTION_REASON	=	"UPDATE  `t_task_dtls` SET  `Task_Remark` = ? WHERE  `Application_Number`=?";	
		
		private static final String UPDATE_NEW_LEARNER_LICENSE_APPLICATION	=	"UPDATE `t_learner_license_application` SET  `Certifying_Doctor` = ?,`Remarks` = ?,`Updated_By` = ?, `Updated_On` = ? WHERE `Application_Number` = ?";
		
		private static final String GET_FILE_PATH_FROM_DOC	=	"SELECT a.`Document_Path` FROM t_document_dtls a WHERE a.`Application_Number`=?";
		
		private static final String DELETE_DRIVE_TYPE_FROM_LEARNER_LICENSE_APPLICATION	=	"DELETE FROM  `t_learner_licn_drive_type_application` WHERE `Application_Number` = ?";
		
		private static final String DELETE_FILE_BY_APPLICATION_NO	=	"DELETE FROM t_document_dtls WHERE `Application_Number`=?";
		
		private static final String GET_SERVICE_CODE_FROM_SHORT_DESC	=	"SELECT a.`Service_Id` FROM t_service_master a WHERE a.`Service_Short_Desc`=?";
		
		private static final String UPDATE_OFFENCE_PAYMENT	=	"UPDATE "
																+ "  `t_offence_information` "
																+ " SET "
																+ " Amount=?, `Is_paid`=?," 
																+ " `Receipt_Number`=?," 
																+ " `Receipt_Date`=?," 
																+ " Updated_On=CURRENT_TIMESTAMP," 
																+ " Updated_By=? "
																+ "  WHERE `TIN_No`=? AND Offence_Id=?";
		
		private static final String GET_REGION_AND_TOP_NUMBER_FROM_TOP_APPLICATION	=	"SELECT   a.`Region_Id`,a.`TOP_Number` FROM  `t_top_application` a WHERE a.`Application_Number` = ?";
		
		private static final String UPDATE_TOP_NUMBER_INTO_TOP_DTLS	=	"UPDATE `t_top_dtls` a SET a.`TOP_Number`=? WHERE a.`Application_Number`=?;";
		
		private static final String INSERT_INTO_T_DRIVING_LICENSE_CANCELLATION_AUDIT	=	"INSERT INTO `t_driving_license_cancelled_audit` ( "
																						+ "  Cancellation_Id,`Customer_Id`, "
																						+ "  `License_Id`, "
																						+ "  `Cancellation_Date`, "
																						+ "  `Status`, "
																						+ "  `Cancellation_Reason`, "
																						+ "  `Created_On`, "
																						+ "  `Created_By`, "
																						+ "  `Updated_On`, "
																						+ "  `Updated_By`, "
																						+ "  `Withdrawn_Reason`, "
																						+ "  `Withdrwan_Date` "
																						+ ") "
																						+ "(SELECT Driving_License_Cancelled_Id,"
																						+ "  `Customer_Id`, "
																						+ "  `License_Id`, "
																						+ "  `Cancellation_Date`, "
																						+ "  `Status`, "
																						+ "  `Cancellation_Reason`, "
																						+ "  `Created_On`, "
																						+ "  `Created_By`, "
																						+ "  `Updated_On`, "
																						+ "  `Updated_By` , "
																						+ "  ?, "
																						+ "  ? "
																						+ "FROM "
																						+ "  `t_driving_license_cancelled` "
																						+ "WHERE `License_Id` = ?)";


		private static final String INSERT_INTO_T_LEARNER_LICENSE_CANCELLATION_AUDIT	=	"INSERT INTO `t_learner_license_cancelled_audit` ( "
																				+ "  Cancellation_Id,`Customer_Id`, "
																				+ "  `Learner_Id`, "
																				+ "  `Cancellation_Date`, "
																				+ "  `Status`, "
																				+ "  `Cancellation_Reason`, "
																				+ "  `Created_On`, "
																				+ "  `Created_By`, "
																				+ "  `Updated_On`, "
																				+ "  `Updated_By`, "
																				+ "  `Withdrawn_Reason`, "
																				+ "  `Withdrwan_Date` "
																				+ ") "
																				+ "(SELECT Learner_License_Cancelled_Id,"
																				+ "  `Customer_Id`, "
																				+ "  `Learner_Id`, "
																				+ "  `Cancellation_Date`, "
																				+ "  `Status`, "
																				+ "  `Cancellation_Reason`, "
																				+ "  `Created_On`, "
																				+ "  `Created_By`, "
																				+ "  `Updated_On`, "
																				+ "  `Updated_By` , "
																				+ "  ?, "
																				+ "  ? "
																				+ "FROM "
																				+ "  `t_learner_license_cancelled` "
																				+ "WHERE `Learner_Id` = ?)";


		private static final String DELETE_DRIVING_LICENSE_CANCELLATION	=	"DELETE FROM `t_driving_license_cancelled` WHERE `License_Id`=?";
		
		private static final String DELETE_LEARNER_LICENSE_CANCELLATION	=	"DELETE FROM `t_learner_license_cancelled` WHERE `Learner_Id`=?";
		
		private static final String GET_CUSTOMER_INFORMATION_FROM_LEARNER_LICENSE = "SELECT "
																					+ "  CONCAT( "
																					+ "    b.`First_Name`, "
																					+ "    ' ', "
																					+ "    b.`Middle_Name`, "
																					+ "    ' ', "
																					+ "    b.`Last_Name` "
																					+ "  ) pname, "
																					+ "  b.`Present_Email`, "
																					+ "  b.`Present_Phone_No`, "
																					+ "  DATE_FORMAT(CURDATE(), '%d-%m-%Y') AS currentDate "
																					+ "FROM "
																					+ "  `t_learner_license_dtls` a "
																					+ "  LEFT JOIN t_personal_dtls b "
																					+ "    ON a.`Customer_Id` = b.`Customer_Id` "
																					+ "WHERE a.`Learner_License_Info_Id`=?";
		
		private static final String GET_VEHICLE_DL_LL_ID_FROM_OFFENCE ="SELECT COUNT(a.`Offence_Id`) rowCount,a.`Vehicle_Id`" +
																		",a.`License_Id`,a.`Learner_License_Id`," +
																		"a.`Is_Foreign" +
																		"` FROM `t_offence_information` a" +
																		" WHERE a.`Offence_Id`=? -- a.`Is_Foreign` = 'N' AND " ;

		private static final String CHECK_APPLICATION_APPROVED ="SELECT COUNT(a.`Status_Id`) rowCount FROM t_workflow_dtls a " +
																		" WHERE a.`Application_Number`=? AND a.`Status_Id` IN ('1','11','4')";

		private static final String CHECK_PENDING_APPLICATION = "SELECT COUNT(*)rowCount,GROUP_CONCAT(' Remarks : ',Remarks,', Updated By : ',(SELECT a.name FROM t_user_master a  WHERE a.`login_id`=Created_By),' From : ',(SELECT "
			+ "      IF( d.base_office_id IS NULL,(SELECT z.region_name FROM t_region_master z" +
					" WHERE z.region_id=d.region_id), "
			+ " (SELECT z.base_office_name FROM t_base_office_master z WHERE z.base_office_id=d.base_office_id))location "
			+ "    FROM "
			+ "       t_user_master a "
			+ "       LEFT JOIN t_user_role_mapping b "
			+ "         ON a.login_id = b.login_id "
			+ "       LEFT JOIN t_role_master c "
			+ "         ON c.role_id = b.role_id "
			+ "       LEFT JOIN t_user_jurisdiction_mapping d "
			+ "         ON a.login_id = d.login_id "
			+ "     WHERE  a.login_id = created_By LIMIT 1)) "
			+ " remarks FROM t_pending_task a WHERE a.Customer_Id=?";



}