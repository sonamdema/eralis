package bt.gov.rsta.eralis.dao.payment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import bt.gov.g2c.aggregator.business.InvokePaymentWS;
import bt.gov.g2c.aggregator.dto.RequestDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;


public class PaymentDAO {
	
	private static PaymentDAO paymentDAO;
	
	public static PaymentDAO getInstance()
	{
		if(paymentDAO == null){
			paymentDAO = new PaymentDAO();
		}
		
		return paymentDAO;
	}
	
	
	
	public String processPaymentDTLS(String application_no,String service_name,String amount) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result	=	null;
		try
		{
			conn = ConnectionManager.getConnection();
			ResultSet rs = null;
			ResultSet rs1 = null;
			if(conn != null)
			{
				RequestDTO dto = new RequestDTO();
				dto.setApplicationNo(application_no);
				dto.setAgencyCode("RSTA");
				dto.setServiceName("RSTA Offence");
				dto.setExpiryDate(null);//only for renewal services or services which involve penalty or late fees
				ArrayList<bt.gov.g2c.aggregator.dto.PaymentDTO> paymentList = new ArrayList<bt.gov.g2c.aggregator.dto.PaymentDTO>();
				
				/***offence amount****/
				bt.gov.g2c.aggregator.dto.PaymentDTO dto1 = new bt.gov.g2c.aggregator.dto.PaymentDTO();
				dto1.setServiceFee(amount);
				
				pst = conn.prepareStatement(GET_ACCOUNT_HEAD_ID);
				pst.setString(1, "FINES_AND_PENALTIES_ROAD");
				rs1 = pst.executeQuery();
				rs1.first();
				if(rs1.getInt("rowCount")>0)
				{
					dto1.setAccountHeadId(rs1.getString("account_head_code"));
				} 
				paymentList.add(dto1);
				
				dto.setPaymentList(paymentList.toArray(new bt.gov.g2c.aggregator.dto.PaymentDTO[paymentList.size()]));
				ResourceBundle bundle = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.wsEndPointURL_en_US");
				InvokePaymentWS invokews = new InvokePaymentWS(bundle.getString("getPaymentDetails.endPointURL"));
				String bfsOrderNo = invokews.insertPaymentDetailsOnSubmission(dto);
				result	=	bfsOrderNo;
			}
		}
		catch (Exception e) 
		{
			throw new ERALISException("###Error at PaymentDAO[processPaymentDTLS]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		return result;
	}
	
	public String processServicePayment(String[][] services,String applicationNo,String serviceName) throws ERALISException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		String result	=	null;
		try
		{
			conn = ConnectionManager.getConnection();
			ResultSet rs = null;
			ResultSet rs1 = null;
			if(conn != null)
			{ 
				String account_service_name = null;
				String service_fees = null;
				if(services==null)
				{
					account_service_name	=	null;
				}
				else
				{
					account_service_name	=	services[0][0];
					service_fees	=	services[1][0];	
				}
				
				
				RequestDTO dto = new RequestDTO();
				dto.setApplicationNo(applicationNo);
				dto.setAgencyCode("RSTA");
				dto.setServiceName(serviceName);
				dto.setExpiryDate(null);//only for renewal services or services which involve penalty or late fees
				ArrayList<bt.gov.g2c.aggregator.dto.PaymentDTO> paymentList = new ArrayList<bt.gov.g2c.aggregator.dto.PaymentDTO>();
				
				/***offence amount****/
				bt.gov.g2c.aggregator.dto.PaymentDTO dto1 = new bt.gov.g2c.aggregator.dto.PaymentDTO();
				
				dto1.setServiceFee(service_fees);
				
				pst = conn.prepareStatement(GET_ACCOUNT_HEAD_ID);
				pst.setString(1, account_service_name);
				rs1 = pst.executeQuery();
				rs1.first();
				if(rs1.getInt("rowCount")>0)
				{
					dto1.setAccountHeadId(rs1.getString("account_head_code"));
				} 
				paymentList.add(dto1);
				
				
				pst = conn.prepareStatement(GET_SERVICE_MAPPING);
				pst.setString(1, account_service_name);
				rs = pst.executeQuery();
				while (rs.next()) 
				{
					pst = conn.prepareStatement(GET_ACCOUNT_HEAD_ID);
					pst.setString(1, rs.getString("related_service_desc"));
					rs1 = pst.executeQuery();
					rs1.first();
					if(rs1.getInt("rowCount")>0)
					{
						bt.gov.g2c.aggregator.dto.PaymentDTO dto2 = new bt.gov.g2c.aggregator.dto.PaymentDTO();
						for(int i=0;i<services.length;i++)
						{
							if(services[0][i]!=null)
							{
								if(services[0][i].equalsIgnoreCase(rs.getString("related_service_desc")))
								{
									dto2.setServiceFee(services[1][i]);
								}
							}
						}
						
						dto2.setAccountHeadId(rs1.getString("account_head_code")); 
						paymentList.add(dto2);  
					}
				}  
				
				dto.setPaymentList(paymentList.toArray(new bt.gov.g2c.aggregator.dto.PaymentDTO[paymentList.size()]));
				ResourceBundle bundle = ResourceBundle.getBundle("bt.gov.rsta.framework.properties.wsEndPointURL_en_US");
				InvokePaymentWS invokews = new InvokePaymentWS(bundle.getString("getPaymentDetails.endPointURL"));
				String bfsOrderNo = invokews.insertPaymentDetailsOnSubmission(dto);
				result	=	bfsOrderNo;
			}
		}
		catch (Exception e) 
		{
			
			throw new ERALISException("###Error at PaymentDAO[processPaymentDTLS]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn, null, null, pst);
		}
		
		
		return result;
	}
	
	/***************SQL QUERIES************/
	
	private static final String GET_SERVICE_MAPPING	=	"SELECT a.`related_service_desc` FROM `t_service_mapping` a WHERE a.`service_desc`=?";
	
	private static final String GET_ACCOUNT_HEAD_ID	=	"SELECT a.`account_head_code`,COUNT(a.`account_head_code`) rowCount FROM `t_account_head_master` a WHERE a.`account_head_desc`=?";
}
