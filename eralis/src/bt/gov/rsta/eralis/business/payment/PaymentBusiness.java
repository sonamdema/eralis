package bt.gov.rsta.eralis.business.payment;

import bt.gov.rsta.eralis.dao.payment.PaymentDAO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class PaymentBusiness {
		
	private static PaymentBusiness paymentBusiness;
		
		public static PaymentBusiness getInstance()
		{
			if(paymentBusiness == null)
				paymentBusiness = new PaymentBusiness();
			return paymentBusiness;
		}
		
		public String processPaymentDTLS(String application_no,String service_name,String amount) throws ERALISException, ERALISSystemException
		{
			return PaymentDAO.getInstance().processPaymentDTLS(application_no,service_name,amount);
		}
		
		public String processServicePayment(String[][] services,String applicationNo, String serviceName) throws ERALISException 
		{
			return PaymentDAO.getInstance().processServicePayment(services,applicationNo,serviceName);
		}

}
