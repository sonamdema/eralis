package bt.gov.rsta.eralis.business.etest;

import java.sql.Connection;
import java.util.List;
import bt.gov.rsta.eralis.dao.etest.ETestDAO;
import bt.gov.rsta.eralis.dto.etest.ETestDTO;
import bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class ETestBusiness 
{
	private static ETestBusiness etestBusiness = null;
	
	public static ETestBusiness getInstance()
	{
		if(etestBusiness == null)
			etestBusiness = new ETestBusiness();
		
		return etestBusiness;
	}
	
	public ApplicationDataVO login(String learnerNo, String dob) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().login(learnerNo, dob);
	}
	
	public ETestDTO start_test(String learnerNo) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().start_test(learnerNo);
	}
	
	public ETestDTO get_question(String questionId, String identifier) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().get_question(questionId, identifier);
	}
	
	public List<ETestDTO> getETestConfigDtls(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().getETestConfigDtls(jurisId, jurisTypeId);
	}
	
	public ETestDTO getTestConfigDetails(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().getTestConfigDetails(jurisId, jurisTypeId);
	}
	
	public String edit_test_details(ETestDTO dto, String jurisId, String jurisTypeId, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().edit_test_details(dto, jurisId, jurisTypeId,userVo);
	}
	
	public String add_test_details(ETestDTO dto, String jurisId, String jurisTypeId, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "ADD_FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().add_test_details(dto, jurisId, jurisTypeId,userVo);
				
				if(result.equalsIgnoreCase("ADD_SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[add_test_details]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String add_questions(ETestDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "ADD_FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().add_questions(conn, dto);
				
				if(result.equalsIgnoreCase("ADD_SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[add_questions]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String edit_question(ETestDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "EDIT_FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().edit_question(conn, dto);
				
				if(result.equalsIgnoreCase("EDIT_SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "EDIT_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[add_questions]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String delete_question(String questionId) throws ERALISException, ERALISSystemException
	{
		String result = "DELETE_FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().delete_question(conn, questionId);
				
				if(result.equalsIgnoreCase("DELETE_SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "DELETE_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[delete_question]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public ETestDTO get_edit_dtls(String questionId) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().get_edit_dtls(questionId);
	}
	
	public ETestDTO process_result(String testResult, String name, String learnerNo, String applicationNo) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().process_result(testResult, name, learnerNo, applicationNo);
	}
	
	public List<PracticalCriteriaDTO> getPracticalCriteriaList() throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().getPracticalCriteriaList();
	}

	public String submit_marksheet(ETestDTO dto, UserDetailsVO vo) throws ERALISException, ERALISSystemException
	{
		String result = "MARKSHEET_FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().submit_marksheet(conn, dto, vo);
				
				if(result.equalsIgnoreCase("MARKSHEET_SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "MARKSHEET_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[submit_marksheet]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String CID_submit_marksheet(ETestDTO dto, UserDetailsVO vo) throws ERALISException, ERALISSystemException
	{
		String result = "MARKSHEET_FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().CID_submit_marksheet(conn, dto, vo);
				
				if(result.equalsIgnoreCase("MARKSHEET_SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "MARKSHEET_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[submit_marksheet]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String candidate_list(String applicationNo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		Connection conn = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ETestDAO.getInstance().candidate_list(conn, applicationNo);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("##Error at ETestBusiness[candidate_list]"+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public List<DropDownDTO> getTestDateList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().getTestDateList(jurisId, jurisTypeId);
	}
	public List<DropDownDTO> getCandidateTestDateList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().getCandidateTestDateList(jurisId, jurisTypeId);
	}
	
	public List<ETestDTO> candidate_list(String testDate, String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return ETestDAO.getInstance().candidate_list(testDate, jurisId, jurisTypeId);
	}
}
