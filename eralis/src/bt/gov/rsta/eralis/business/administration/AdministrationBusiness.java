package bt.gov.rsta.eralis.business.administration;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.business.vehicle.VehicleBusiness;
import bt.gov.rsta.eralis.dao.administration.AdministrationDAO;
import bt.gov.rsta.eralis.dto.administration.AdministrationDTO;
import bt.gov.rsta.eralis.dto.administration.PagePermissionDTO;
import bt.gov.rsta.eralis.dto.administration.PrivDTO;
import bt.gov.rsta.eralis.dto.administration.RoleDTO;
import bt.gov.rsta.eralis.dto.administration.UserDTO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.framework.dao.EmailDAO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.ldapconfig.InsertUsersIntoLDAP;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.Constants;
import bt.gov.rsta.framework.util.EmailUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.SMSUtil;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class AdministrationBusiness 
{
	Connection conn = null;
	private static AdministrationBusiness adminBusiness;
	
	public static AdministrationBusiness getInstance()
	{
		if(adminBusiness == null)
			adminBusiness = new AdministrationBusiness();
		
		return adminBusiness;
	}

	public List<RoleDTO> getRoleList() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getRoleList();
	}
	public List<AdministrationDTO> getPeningPaymentList() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getPeningPaymentList();
	}
	
	public List<UserDTO> getUserList() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getUserList();
	}
	
	public List<String> getTableList() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getTableList();
	}
	
	public String add_user(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "USER_ADD_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().add_user(dto, conn);
				
				if(result.equalsIgnoreCase("USER_ADD_SUCCESS"))
				{
					boolean insert = InsertUsersIntoLDAP.insertUserIntoLdap(dto);
					if(insert)
					{
						conn.commit();
						
						NotificationDTO notifyDTO = new NotificationDTO();
						notifyDTO.setLoginId(dto.getLoginId());
						notifyDTO.setPassword(Constants.ERALIS_PASSWORD_BUILDER+"@"+dto.getLoginId());
						notifyDTO.setEmailAddress(dto.getEmailId());
						notifyDTO.setMobileNumber(dto.getMobileNumber());
						notifyDTO.setUserName(dto.getUserName());
						
						if(dto.getEmailId() != null)
						{
							EmailModelVO emailVO = new EmailModelVO();
							emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_USER_CREATION);
							sendMailToUser(emailVO, notifyDTO);
						}
						
						if(dto.getMobileNumber() != null)
						{
							SMSModelVO smsVO = new SMSModelVO();
							smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_USER_CREATION);
							sendSMSToUser(smsVO, notifyDTO);
						}
					}
				}
			}
		}
		catch (Exception e) 
		{
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[add_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String add_role(String role, String isActive) throws ERALISException, ERALISSystemException
	{
		String result = "ROLE_ADD_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().add_role(role, isActive, conn);
				
				if(result.equalsIgnoreCase("ROLE_ADD_SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			result = "ROLE_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[add_role]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String activateDeactivateRole(String roleId, String typeFlag) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().activateDeactivateRole(roleId, typeFlag, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e)
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[activateDeactivateRole]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String user_management(String loginId, String typeFlag) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().user_management(loginId, typeFlag, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			throw new ERALISException("###Error at AdministrationBusiness[user_management]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String edit_user(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "USER_EDIT_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().edit_user(dto, conn);
				
				if(result.equalsIgnoreCase("USER_EDIT_SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			result = "USER_EDIT_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[edit_user]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public List<PagePermissionDTO> getPagePermissionList(String roleId) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getPagePermissionList(roleId);
	}
	
	public List<PagePermissionDTO> getServicePrivList(String roleId) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getServicePrivList(roleId);
	}
	
	public List<PrivDTO> getPreviousPrivList(String roleId) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getPreviousPrivList(roleId);
	}
	
	public List<String> getPageList() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getPageList();
	}
	
	public int getServiceCount() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getServiceCount();
	}
	
	public String update_permission_dtls(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "PERMISSION_UPDATE_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().update_permission_dtls(dto, conn);
				
				if(result.equalsIgnoreCase("PERMISSION_UPDATE_SUCCESS")) 
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			throw new ERALISException("###Error at AdministrationBusiness[update_permission_dtls]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String add_master_data(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().add_master_data(dto);
	}
	
	public String edit_master_data(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().edit_master_data(dto);
	}

	public String delete_master_data(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().delete_master_data(dto);
	}
	public List<EralisCommonDTO> getVehicleConstant() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getVehicleConstant();
	}
	public List<EralisCommonDTO> getLicenseConstant() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getLicenseConstant();
	}
	public List<EralisCommonDTO> getPenaltyConstant() throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getPenaltyConstant();
	}
	
	public String updateMailConfiguration(AdministrationDTO dto) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = "MAIL_UPDATE_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			result = EmailDAO.updateMailConfiguration(dto, conn);
			
			if(result.equalsIgnoreCase("MAIL_UPDATE_SUCCESS"))
				conn.commit();
		} 
		catch (Exception e) 
		{
			result = "MAIL_UPDATE_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public List<DropDownDTO> getColumnList(String tableName) throws ERALISException, ERALISSystemException
	{
		return AdministrationDAO.getInstance().getColumnList(tableName);
	}
	
	/**
	 * Send mail to a user
	 * 
	 * @param EmailModelVO
	 * @param NotificationDTO
	 * @throws ERALISException
	 */
	public void sendMailToUser(EmailModelVO inputMailVO,NotificationDTO dto) throws ERALISException 
	{
		Log.info("Inside AdministrationBusiness::sendMailToUser");
		List<String> recipientList = new ArrayList<String>();
		recipientList.add(dto.getEmailAddress());
		inputMailVO.setRecipentList(recipientList);
		inputMailVO.setUserName(dto.getUserName());
		inputMailVO.setLoginId(dto.getLoginId());
		inputMailVO.setPassword(dto.getPassword());
		
		Log.info("Mail Type ============> "+inputMailVO.getMailType());
		
		Log.info("Before calling EmailUtil.sendMail");
		EmailUtil.sendMail(inputMailVO);
		Log.info("After calling EmailUtil.sendMail");
	}
	
	/**
	 * @param smsObject the sms object
	 * @param NotificationDTO
	 * @throws ERALISException the system exception
	 */  
	public void sendSMSToUser(SMSModelVO smsObject, NotificationDTO dto)
			throws ERALISException {
		Log.info("Inside RegistrationBusiness::sendSMSToJobseeker");
		String mobileNo = dto.getMobileNumber();

		if (mobileNo != null && !mobileNo.trim().isEmpty()) {
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(mobileNo);
			smsObject.setRecipentList(recipientList);
			smsObject.setLoginId(dto.getLoginId());
			smsObject.setPassword(dto.getPassword());
			
			Log.info("Before calling SMSUtil.sendSMS");
			new SMSUtil().sendSMS(smsObject);
			Log.info("After calling SMSUtil.sendSMS");
		}

	}
	public String edit_vehicle_constant(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "USER_ADD_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().edit_vehicle_constant(dto, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[edit_vehicle_constant]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String edit_license_constant(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "USER_ADD_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().edit_license_constant(dto, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[edit_license_constant]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String edit_penalty_constant(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = "USER_ADD_FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = AdministrationDAO.getInstance().edit_penalty_constant(dto, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			result = "USER_ADD_FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at AdministrationBusiness[edit_license_constant]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
}
