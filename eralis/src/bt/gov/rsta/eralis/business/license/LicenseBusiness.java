package bt.gov.rsta.eralis.business.license;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.business.payment.PaymentBusiness;
import bt.gov.rsta.eralis.dao.license.LicenseDAO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EmailUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.SMSUtil;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class LicenseBusiness {
	Connection conn = null;
	private static LicenseBusiness licenseBusiness;
	
	public static LicenseBusiness getInstance()
	{
		if(licenseBusiness == null)
			licenseBusiness = new LicenseBusiness();
		
		return licenseBusiness;
	}
	
	public String learner_and_license_resubmit(String serviceCode,LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().learner_and_license_resubmit(serviceCode,dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String new_issue(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().new_issue(dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String request_reprint(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().request_reprint(dto, userVo, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[request_reprint]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public LicenseDTO get_reprint_details(String requestId) throws ERALISException, ERALISSystemException
	{
		return LicenseDAO.getInstance().get_reprint_details(requestId);
	}
	
	public String approve_request_reprint(String requestId, String remarks, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		return LicenseDAO.getInstance().approve_request_reprint(requestId, remarks, userVo);
	}
	
	public List<LicenseDTO> getReprintList(String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return LicenseDAO.getInstance().getReprintList(jurisId, jurisTypeId);
	}
	
	public String pay_offence(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				String[][] notifyArray = LicenseDAO.getInstance().pay_offence(dto, userVo, conn);
				result = notifyArray[3][0];
				if(notifyArray[3][0].equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					/*for(int i=0;i<3;i++)
					{
						if(notifyArray[i][2] != null)
						{
							NotificationDTO notifyDTO = new NotificationDTO();
							notifyDTO.setCustomerName(notifyArray[i][0]);
							notifyDTO.setApprovalDate(notifyArray[i][1]);
							notifyDTO.setEmailAddress(notifyArray[i][2]);
							notifyDTO.setMobileNumber(notifyArray[i][3]);
							notifyDTO.setApplicationNo(notifyArray[i][4]);
							
							EmailModelVO emailVO = new EmailModelVO();
							emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_OFFENCE_PAYMENT_NOTIFICATION);
							LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
						}
						
						if(notifyArray[i][3] != null)
						{
							NotificationDTO notifyDTO = new NotificationDTO();
							notifyDTO.setCustomerName(notifyArray[i][0]);
							notifyDTO.setApprovalDate(notifyArray[i][1]);
							notifyDTO.setEmailAddress(notifyArray[i][2]);
							notifyDTO.setMobileNumber(notifyArray[i][3]);
							notifyDTO.setApplicationNo(notifyArray[i][4]);
							SMSModelVO smsVO = new SMSModelVO();
							smsVO.setSmsType(NotificationConstants.SMS_TEMPLATE_OFFENCE_UPDATE_NOTIFICATION);
							LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
						}
					}*/
					
				}
				
				
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[pay_offence]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	

	public String license_renewal_learner(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			
			if(null!=dto.getBfsNo())
			{
				conn = ConnectionManager.getOnlineDbConnection();
			}
			else
			{
				conn = ConnectionManager.getConnection();
			}
			
		//	conn = ConnectionManager.getConnection();
			
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_renewal_learner(dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS")){
					conn.commit();
					if(null!=dto.getBfsNo())
					{
						String bfsOrderNo =	null;
						bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(dto.getService(),resultArray[1],dto.getServiceName());
						if(null==bfsOrderNo || bfsOrderNo.equalsIgnoreCase(""))
						{
							return "BFS_ORDER_NO_FAILURE"+"#"+null+"#"+null;
						}
						else 
						{
							result =resultArray[0]+"#"+resultArray[1]+"#"+bfsOrderNo;
						}
					}
					else
					{
						result =resultArray[0]+"#"+resultArray[1]+"#"+"";
					}
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_renewal_learner]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	
	
	public String license_offence(LicenseDTO dto,String offenceFormType,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				String[][] notifyArray = LicenseDAO.getInstance().license_offence(dto,offenceFormType, userVo, conn);
				result = notifyArray[3][0];
				if(notifyArray[3][0].equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					/*for(int i=0;i<3;i++)
					{
						System.out.println(notifyArray[i][2]+"------------notifyArray[i][2]------"+notifyArray[i][2]);
						if(notifyArray[i][2] != null)
						{
							NotificationDTO notifyDTO = new NotificationDTO();

							notifyDTO.setCustomerName(notifyArray[i][0]);
							notifyDTO.setApprovalDate(notifyArray[i][1]);
							notifyDTO.setEmailAddress(notifyArray[i][2]);
							notifyDTO.setMobileNumber(notifyArray[i][3]);
							notifyDTO.setApplicationNo(notifyArray[i][4]);
							
							EmailModelVO emailVO = new EmailModelVO();
							emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_OFFENCE_UPDATE_NOTIFICATION);
							LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
						}

						System.out.println(notifyArray[i][3]+"------------notifyArray[i][3]------"+notifyArray[i][3]);
						if(notifyArray[i][3] != null)
						{
							NotificationDTO notifyDTO = new NotificationDTO();
							notifyDTO.setCustomerName(notifyArray[i][0]);
							notifyDTO.setApprovalDate(notifyArray[i][1]);
							notifyDTO.setEmailAddress(notifyArray[i][2]);
							notifyDTO.setMobileNumber(notifyArray[i][3]);
							notifyDTO.setApplicationNo(notifyArray[i][4]);
							
							SMSModelVO smsVO = new SMSModelVO();
							smsVO.setSmsType(NotificationConstants.SMS_TEMPLATE_OFFENCE_UPDATE_NOTIFICATION);
							LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
						}
					}*/
					
				}
					
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_offence]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	

	public String license_duplication_learner(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			if(null!=dto.getBfsNo())
			{
				conn = ConnectionManager.getOnlineDbConnection();
			}
			else
			{
				conn = ConnectionManager.getConnection();
			}
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_duplication_learner(dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplication_learner]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	
	public String license_non_commerical(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_non_commerical(dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_non_commerical]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String license_commerical(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_commerical(dto, userVo, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_commerical]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String license_renewal(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		String bfsOrderNo = null;
		try 
		{
			if(null!=dto.getBfsNo())
			{
				conn = ConnectionManager.getOnlineDbConnection();
			}
			else
			{
				conn = ConnectionManager.getConnection();
			}
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_renewal(dto, userVo, conn);
				String[] resultArray = result.split("#");
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					if(null!=dto.getBfsNo())
					{
						bfsOrderNo = PaymentBusiness.getInstance().processServicePayment(dto.getService(),resultArray[1],dto.getServiceName());
						if(null==bfsOrderNo || bfsOrderNo.equalsIgnoreCase(""))
						{
							return "BFS_ORDER_NO_FAILURE"+"#"+null+"#"+null;
						}
						else 
						{
							result =resultArray[0]+"#"+resultArray[1]+"#"+bfsOrderNo;
						}
					}
					else
					{
						result =resultArray[0]+"#"+resultArray[1]+"#"+"";
					}
				}
					
					
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_renewal]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String license_duplication(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			if(null!=dto.getBfsNo())
			{
				conn = ConnectionManager.getOnlineDbConnection();
			}
			else
			{
				conn = ConnectionManager.getConnection();
			}
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_duplication(dto,userVo, conn);
				String[] resultArray = result.split("#");
				if(null != resultArray[0])
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplication]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	
	public String license_endorsement(LicenseDTO dto,UserDetailsVO userVo, String isRenewalProcess) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_endorsement(dto, userVo,isRenewalProcess, conn);
				
				String[] resultArray = result.split("#");
				
				if(resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_endorsement]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String license_cancellation(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_cancellation(dto,userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_cancellation]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String withdraw_license_cancellation(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().withdraw_license_cancellation(dto,userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_cancellation]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String license_drive_type_suspension(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_drive_type_suspension(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_drive_type_suspension]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String license_application_approval(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				NotificationDTO notifyDTO = LicenseDAO.getInstance().license_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_LL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_LL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					result = "SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	
	public String license_renewal_application_approval(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				NotificationDTO notifyDTO = LicenseDAO.getInstance().license_renewal_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_LL_RENEWAL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_LL_RENEWAL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					result = "SUCCESS";
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_renewal_application_approval]:: "+e);
		}
		finally
		{
			result = dto.getStatus();
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	
	public String non_commercial_application_approval(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				NotificationDTO notifyDTO = LicenseDAO.getInstance().non_commercial_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_DL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_DL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					
					result = notifyDTO.getStatus();
				}
			}
			
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	
	public String commercial_application_approval(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{	
				NotificationDTO notifyDTO =LicenseDAO.getInstance().commercial_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_DL_COMMWERCIAL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_DL_COMMWERCIAL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					
					result = notifyDTO.getStatus();
				}
				
			}
		}
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	
	public String driving_license_renewal_application_approval(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{	
				NotificationDTO notifyDTO =LicenseDAO.getInstance().driving_license_renewal_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_DL_RENEWAL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_DL_RENEWAL_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					
					result = notifyDTO.getStatus();
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[driving_license_renewal_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	public String driving_license_endorsement_application_approval(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{	
				NotificationDTO notifyDTO =LicenseDAO.getInstance().driving_license_endorsement_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_DL_ENDORSEMENT_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_DL_ENDORSEMENT_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					
					result = notifyDTO.getStatus();
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	public String license_application_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	public String commercial_application_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().commercial_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[new_issue]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	public String driving_license_renewal_application_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().driving_license_renewal_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[driving_license_renewal_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	
	public String driving_license_endorsement_application_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().driving_license_endorsement_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[driving_license_endorsement_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String license_duplicate_application_verify(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_duplicate_application_verify(dto, userVo, conn);
				
				//if(result.equalsIgnoreCase("SUCCESS"))
				//	conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_application_verify]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	public String license_duplicate_application_approve(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				NotificationDTO notifyDTO = LicenseDAO.getInstance().license_duplicate_application_approve(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_LL_DUPLICATE_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_LL_DUPLICATE_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					
					result = notifyDTO.getStatus();
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_application_verify]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}	
	public String driving_license_verify(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().driving_license_verify(dto, userVo, conn);
				
				//if(result.equalsIgnoreCase("SUCCESS"))
				//	conn.commit();
			}
			
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_application_verify]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String driving_license_duplicate_approve(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = "FAILURE";
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				NotificationDTO notifyDTO = LicenseDAO.getInstance().driving_license_duplicate_approve(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_DL_ENDORSEMENT_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_DL_ENDORSEMENT_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
					
					result = notifyDTO.getStatus();
				}
			}	
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_application_verify]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String license_duplicate_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_duplicate_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String learner_duplicate_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().learner_duplicate_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[learner_duplicate_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String license_suspension(LicenseDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().license_suspension(dto, conn);
				
				//if(result.equalsIgnoreCase("SUCCESS"))
				//	conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_license_suspension]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String top_issuance(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().top_issuance(dto,userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_cancellation]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String top_application_approve(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = LicenseDAO.getInstance().top_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_TOP_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_TOP_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
				}
			}	
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_application_verify]:: "+e);
		}
		finally
		{
			result = notifyDTO.getStatus();
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String top_replacement_approval (LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			//conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = LicenseDAO.getInstance().top_replacement_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					//conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_TOP_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_TOP_APPLICATION_APPROVAL_NOTIFICATION);
						LicenseBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
				}
			}	
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[top_replacement_approval]:: "+e);
		}
		finally
		{
			result = notifyDTO.getStatus();
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String top_application_reject(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().top_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[license_duplicate_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public LicenseDTO getTopDtls(String topNumber,String searchType) throws ERALISException, ERALISSystemException
	{
		return LicenseDAO.getInstance().getTopDtls(topNumber,searchType);
	}
	
	public String top_cancellation(LicenseDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = "FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			result = LicenseDAO.getInstance().top_cancellation(dto, userVo, conn);
			
			if(result.equals("SUCCESS"))
				conn.commit();
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String top_replacement(LicenseDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = LicenseDAO.getInstance().top_replacement(dto,userVo, conn);
				String[] resultArray = result.split("#");
				if(null != result && resultArray[0].equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at LicenseBusiness[top_replacement]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	/**
	 * Send mail to a user
	 * 
	 * @param EmailModelVO
	 * @param NotificationDTO
	 * @throws ERALISException
	 */
	public void sendMailToUser(EmailModelVO inputMailVO,NotificationDTO dto) throws ERALISException 
	{
		Log.info("Inside LicenseBusiness::sendMailToUser");
		List<String> recipientList = new ArrayList<String>();
		recipientList.add(dto.getEmailAddress());
		inputMailVO.setRecipentList(recipientList);
		inputMailVO.setCustomerName(dto.getCustomerName());
		inputMailVO.setApplicationNo(dto.getApplicationNo());
		inputMailVO.setApprovalDate(dto.getApprovalDate());
		inputMailVO.setLicenseNo(dto.getLicenseNo());
		
		Log.info("Mail Type ============> "+inputMailVO.getMailType());
		
		Log.info("Before calling EmailUtil.sendMail");
		EmailUtil.sendMail(inputMailVO);
		Log.info("After calling EmailUtil.sendMail");
	}
	
	/**
	 * @param smsObject the sms object
	 * @param NotificationDTO
	 * @throws ERALISException the system exception
	 */  
	public void sendSMSToUser(SMSModelVO smsObject, NotificationDTO dto)
			throws ERALISException {
		Log.info("Inside LicenseBusiness::sendSMSToJobseeker");
		String mobileNo = dto.getMobileNumber();

		if (mobileNo != null && !mobileNo.trim().isEmpty()) {
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(mobileNo);
			smsObject.setRecipentList(recipientList);
			smsObject.setCustomerName(dto.getCustomerName());
			smsObject.setApplicationNo(dto.getApplicationNo());
			smsObject.setApprovalDate(dto.getApprovalDate());
			smsObject.setLicenseNo(dto.getLicenseNo());
			smsObject.setTinNo(dto.getTinNo());
			smsObject.setTestDate(dto.getTestDate());
			smsObject.setTestLocation(dto.getTestLocation());
			smsObject.setDriveType(dto.getDriveType());
			
			Log.info("Before calling SMSUtil.sendSMS");
			new SMSUtil().sendSMS(smsObject);
			Log.info("After calling SMSUtil.sendSMS");
		}

	}
}
