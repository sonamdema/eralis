package bt.gov.rsta.eralis.business.tools;

import java.sql.Connection;
import java.util.List;

import bt.gov.rsta.eralis.dao.tools.ToolsDAO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.tools.ToolsDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.vo.ApplicationDataVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;



public class ToolsBusiness 
{
	Connection conn = null;
	private static ToolsBusiness toolsBusiness;
	
	public static ToolsBusiness getInstance()
	{
		if(toolsBusiness == null)
			toolsBusiness = new ToolsBusiness();
		
		return toolsBusiness;
	}
	
	public String replace_CID(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = ToolsDAO.getInstance().replace_CID(dto, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		}
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[replace_CID]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String replace_IID(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().replace_IID(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[replace_IID]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String delete_Drive_Type(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().delete_Drive_Type(dto, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[delete_Drive_Type]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String change_Driving_License_Status(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().change_Driving_License_Status(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[change_Driving_License_Status]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String vehicle_Purchase(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().vehicle_Purchase(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[vehicle_Purchase]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String road_Block_Entry(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().road_Block_Entry(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[road_Block_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String edit_Road_Block_Entry(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().edit_Road_Block_Entry(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[road_Block_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String hypothecated_To(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().hypothecated_To(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[hypothecated_To]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String license_Punch(ToolsDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().license_Punch(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String save_pending_task(ToolsDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().save_pending_task(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String delete_License_Punch(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().delete_License_Punch(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String withdraw_License_Punch(ToolsDTO dto,UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().withdraw_License_Punch(dto,userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[license_Punch]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String license_Print(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().license_Print(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[license_Print]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String refresh_Course_Entry(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().refresh_Course_Entry(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[refresh_Course_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		return result;
	}
	public List<ToolsDTO> license_holder_list(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		return ToolsDAO.getInstance().license_holder_list(dto);
	}
	public List<ToolsDTO> getEndorsementLicenseDetails(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		return ToolsDAO.getInstance().getEndorsementLicenseDetails(dto);
	}
	public List<ToolsDTO> getDriveType(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		return ToolsDAO.getInstance().getDriveType(dto);
	}
	public List<ToolsDTO> licensePunchList(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		return ToolsDAO.getInstance().licensePunchList(dto);
	}
	public List<ToolsDTO> getRoadBlockList(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		return ToolsDAO.getInstance().getRoadBlockList(dto);
	}
	public List<ToolsDTO> licensePunchDetails(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		return ToolsDAO.getInstance().licensePunchDetails(dto);
	}
	
	
	public String delete_Road_Block_Entry(ToolsDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = ToolsDAO.getInstance().delete_Road_Block_Entry(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at ToolsBusiness[delete_Road_Block_Entry]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	
	
}
