package bt.gov.rsta.eralis.business.eralis_common;

import java.sql.Connection;
import bt.gov.rsta.eralis.dao.eralis_common.EralisCommonDAO;
import bt.gov.rsta.eralis.dao.vehicle.VehicleDAO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.noc.NOCDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.eralis.web.actionform.eralis_common.EralisCommonActionForm;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class EralisCommonBusiness {
	Connection conn = null;
	private static EralisCommonBusiness EralisCommonBusiness;
	public static EralisCommonBusiness getInstance()
	{
		if(EralisCommonBusiness == null)
			EralisCommonBusiness = new EralisCommonBusiness();
		
		return EralisCommonBusiness;
	}
	
	public String noc_issuance(EralisCommonDTO dto, String jurisId, String regionId, String baseId) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = null;
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			result = EralisCommonDAO.getInstance().noc_issuance(dto, jurisId, regionId, baseId, conn);
			
			if(null != result)
				conn.commit();
		}
		catch (Exception e) 
		{
			result = null;
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public NOCDTO get_noc_dtls(String nocNo) throws ERALISException, ERALISSystemException
	{
		return EralisCommonDAO.getInstance().get_noc_dtls(nocNo);
	}
	public EralisCommonDTO  get_permit_dtls(String permitID) throws ERALISException, ERALISSystemException
	{
		return EralisCommonDAO.getInstance().get_permit_dtls(permitID);
	}
	public String personal_info(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().personal_info(dto, userVo, conn);
				
				String[] tempArray = result.split("#");
				String responseStr = tempArray[0];
				
				if(responseStr.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String edit_personal_info(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().edit_personal_info(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[edit_personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String edit_receipt_dtls(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().edit_receipt_dtls(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[edit_personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String delete_personal_info(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().delete_personal_info(dto, userVo, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[delete_personal_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String organization_info(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().organization_info(dto, conn, userVo);
				String[] tempArray = result.split("#");
				String responseStr = tempArray[0];
				
				if(responseStr.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[organization_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String delete_organization_info(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().delete_organization_info(dto, conn);
				String[] tempArray = result.split("#");
				String responseStr = tempArray[0];
				
				if(responseStr.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[delete_organization_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String edit_organization_info(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().edit_organization_info(dto, conn,userVo);
				String[] tempArray = result.split("#");
				String responseStr = tempArray[0];
				
				if(responseStr.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[edit_organization_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String accident_info(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
			//	result = EralisCommonDAO.getInstance().personal_info(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[accident_info]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String license_history(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().license_history(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[license_history]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String vehicle_history(EralisCommonDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
			//	result = EralisCommonDAO.getInstance().personal_info(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[vehicle_history]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String route_permit(EralisCommonDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().route_permit(dto, userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{	
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[route_permit]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String new_accident_entry(EralisCommonActionForm comonForm, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = EralisCommonDAO.getInstance().new_accident_entry(comonForm, userVo, conn);
				
				if(!result.equalsIgnoreCase("FAILURE"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{	
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at EralisCommonBusiness[route_permit]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	 
}
