package bt.gov.rsta.eralis.business.report;

import java.util.List;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.dao.administration.AdministrationDAO;
import bt.gov.rsta.eralis.dao.license.LicenseDAO;
import bt.gov.rsta.eralis.dao.report.ReportDAO;
import bt.gov.rsta.eralis.dao.vehicle.VehicleDAO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.report.ReportDTO;
import bt.gov.rsta.eralis.dto.report.ReportParameterDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dto.DropDownDTO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class ReportBusiness
{
	private static ReportBusiness reportBusiness = null;
	
	public static ReportBusiness getInstance()
	{
		if(reportBusiness == null)
			reportBusiness = new ReportBusiness();
		
		return reportBusiness;
	}
	
	public String getReportName(String reportId) throws ERALISException, ERALISSystemException
	{
		return ReportDAO.getInstance().getReportName(reportId);
	}
	

	public List<EralisCommonDTO> getStatisticDtlReport(String reportType,String jurisId, String jurisTypeId) throws ERALISException, ERALISSystemException
	{
		return ReportDAO.getInstance().getStatisticDtlReport(reportType,jurisId, jurisTypeId);
	}

	public List<ReportDTO> get_report_header(String reportId,ReportDTO dto) throws ERALISException, ERALISSystemException
	{
		return ReportDAO.getInstance().get_report_header(reportId,dto);
	}
	
	public String[][] get_report_details(String reportId,ReportDTO dto) throws ERALISException, ERALISSystemException
	{
		return ReportDAO.getInstance().get_report_details(reportId,dto);
	}
	public ReportDTO getReportParam(String reportId) throws ERALISException, ERALISSystemException
	{
		return ReportDAO.getInstance().getReportParam(reportId);
	}
}