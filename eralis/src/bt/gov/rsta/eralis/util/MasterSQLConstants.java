package bt.gov.rsta.eralis.util;

public class MasterSQLConstants 
{
	public static final String GET_REGION_DATA = "SELECT a.`region_id` AS HEADER_ID,a.`region_name` as HEADER_NAME,a.`region_desc` AS HEADER_DESC ,a.`region_number` AS NUMBER FROM t_region_master a";
	
	public static final String ADD_REGION = "INSERT INTO `t_region_master` ( "
												+ "  `region_name`, "
												+ "  `region_desc`, "
												+ "  `region_number` "
												+ ") "
												+ "VALUES "
												+ "  (?, ?, ?)";

	public static final String EDIT_REGION = "UPDATE "
												+ "  `t_region_master` "
												+ "SET "
												+ "  `region_name` = ?, "
												+ "  `region_desc` = ?, "
												+ "  `region_number` = ? "
												+ "WHERE `region_id` = ?";

	public static final String DELETE_REGION = "DELETE FROM `t_region_master` WHERE `region_id` = ?";
	
	public static final String GET_DZONGKHAG_DATA = "SELECT "
													+ "  a.dzongkhag_id AS HEADER_ID, "
													+ "  a.dzongkhag_name AS HEADER_NAME, "
													+ "  a.region_id AS OTHER_ID, "
													+ "  (SELECT "
													+ "    region_name "
													+ "  FROM "
													+ "    t_region_master "
													+ "  WHERE region_id = a.region_id) AS OTHER_NAME "
													+ "FROM "
													+ "  t_dzongkhag_master a "
													+ "ORDER BY a.`dzongkhag_name`";
	
	public static final String DELETE_DZONGKHAG = "DELETE FROM t_dzongkhag_master WHERE dzongkhag_id=?";
	
	public static final String EDIT_DZONGKHAG = "UPDATE `t_dzongkhag_master` SET `dzongkhag_name` = ?,`region_id` = ? WHERE `dzongkhag_id` = ?";
	
	public static final String ADD_DZONGKHAG = "INSERT INTO `t_dzongkhag_master`(`dzongkhag_name`,`region_id`) VALUES (?,?)";
	
	public static final String GET_GEWOG_DATA = "SELECT "
												+ "  a.Gewog_Id AS HEADER_ID, "
												+ "  a.Gewog_Name AS HEADER_NAME, "
												+ "  Dzongkhag_Id AS OTHER_ID, "
												+ "  (SELECT "
												+ "    `dzongkhag_name` "
												+ "  FROM "
												+ "    `t_dzongkhag_master` "
												+ "  WHERE `dzongkhag_id` = a.`Dzongkhag_Id`) AS OTHER_NAME "
												+ "FROM "
												+ "  t_gewog_master a "
												+ "ORDER BY a.Gewog_Name";
	
	public static final String EDIT_GEWOG = "UPDATE t_gewog_master a SET a.`Gewog_Name`=?, a.`Dzongkhag_Id`=? WHERE a.`Gewog_Id`=?";
	
	public static final String ADD_GEWOG = "INSERT INTO `t_gewog_master` (`Gewog_Name`,`Dzongkhag_Id`) VALUES (?,?)";
	
	public static final String DELETE_GEWOG = "DELETE FROM `t_gewog_master` WHERE `Gewog_Id` = ?";
	
	public static final String GET_MINISTRY_DATA = "SELECT Ministry_Id AS HEADER_ID, Ministry_Name AS HEADER_NAME, Description AS HEADER_DESC FROM t_ministry_master";
	
	public static final String ADD_MINISTRY = "INSERT INTO `t_ministry_master` (`Ministry_Name`, `Description`) VALUES (?, ?)";
	
	public static final String EDIT_MINISTRY = "UPDATE `t_ministry_master` SET `Ministry_Name` = ?,`Description` = ? WHERE `Ministry_Id` = ?";
	
	public static final String DELETE_MINISTRY = "DELETE FROM `eralis_db`.`t_ministry_master` WHERE `Ministry_Id` = ?";
	
	public static final String GET_DEPARTMENT_DATA = "SELECT "
														+ "  a.`Department_Id` AS HEADER_ID, "
														+ "  a.`Department_Name` AS HEADER_NAME, "
														+ "  a.`Description` AS HEADER_DESC, "
														+ "  a.`Ministry_Id` AS OTHER_ID, "
														+ "  (SELECT "
														+ "    `Ministry_Name` "
														+ "  FROM "
														+ "    t_ministry_master "
														+ "  WHERE `Ministry_Id` = a.`Ministry_Id`) AS OTHER_NAME "
														+ "FROM "
														+ "  `t_department_master` a";
	
	public static final String ADD_DEPARTMENT = "INSERT INTO `t_department_master` (`Department_Name`,`Ministry_Id`) VALUES (?, ?)";
	
	public static final String EDIT_DEPARTMENT = "UPDATE `t_department_master` SET `Department_Name` = ?,`Ministry_Id` = ? WHERE `Department_Id` = ?";
	
	public static final String DELETE_DEPARTMENT = "DELETE FROM `t_department_master` WHERE `Department_Id` = ?";
	
	public static final String GET_COUNTRY_DATA = "SELECT Country_Id AS HEADER_ID,Country_Name AS HEADER_NAME, Nationality AS HEADER_DESC FROM `t_country_master`";
	
	public static final String EDIT_COUNTRY = "UPDATE `t_country_master` SET `Country_Name` = ?,`Nationality` = ? WHERE `Country_Id` = ?";
	
	public static final String ADD_COUNTRY = "INSERT INTO `t_country_master` (`Country_Name`,`Nationality`) VALUES (?, ?)";
	
	public static final String DELETE_COUNTRY = "DELETE FROM `t_country_master` WHERE `Country_Id` = ?";
	
	public static final String GET_OFFENCE_TYPE_DATA = "SELECT `Offence_Type_Id` AS HEADER_ID,`Offence_Name` AS HEADER_NAME, `Amount` AS NUMBER FROM `t_offence_type_master`";
	
	public static final String EDIT_OFFENCE_DATA = "UPDATE `t_offence_type_master` SET `Offence_Name` = ?,`Amount` = ? WHERE `Offence_Type_Id` = ?";
	
	public static final String ADD_OFFENCE = "INSERT INTO `t_offence_type_master` (`Offence_Name`,`Amount`) VALUES (?, ?)";
	
	public static final String DELETE_OFFENCE = "DELETE FROM `t_offence_type_master` WHERE `Offence_Type_Id` = ?";
	
	public static final String GET_ACCIDENT_CAUSE_DATA = "SELECT `Accident_Cause_Id` AS HEADER_ID,`Accident_Name` AS HEADER_NAME, `Accident_Type` AS HEADER_DESC FROM `t_accident_cause_master`";
	
	public static final String DELETE_ACCIDENT = "DELETE FROM `t_accident_cause_master` WHERE `Accident_Cause_Id` = ?";
	
	public static final String EDIT_ACCIDENT = "UPDATE `t_accident_cause_master` SET `Accident_Name` = ?,`Accident_Type` = ? WHERE `Accident_Cause_Id` = ?";
	
	public static final String ADD_ACCIDENT = "INSERT INTO `t_accident_cause_master` (`Accident_Name`,`Accident_Type`) VALUES (?, ?)";
	
	public static final String GET_ACTION_TAKEN_DATA = "SELECT `Action_Taken_Id` AS HEADER_ID,`Action_Name` AS HEADER_NAME, `Description` AS HEADER_DESC FROM `t_action_taken_master`";
	
	public static final String EDIT_ACTION_TAKEN = "UPDATE `t_action_taken_master` SET `Action_Name` = ? WHERE `Action_Taken_Id` = ?";
	
	public static final String ADD_ACTION_TAKEN = "INSERT INTO `t_action_taken_master` (`Action_Name`,`Description`) VALUES (?, ?)";
	
	public static final String DELETE_ACTION_TAKEN = "DELETE FROM `t_action_taken_master` WHERE `Action_Taken_Id` = ?";
	
	public static final String GET_DESIGNATION_DATA = "SELECT `Designation_Id` AS HEADER_ID,`Designation_Name` AS HEADER_NAME FROM `t_designation_master`";
	
	public static final String ADD_DESIGNATION = "INSERT INTO `t_designation_master` (`Designation_Name`) VALUES (?)";
	
	public static final String EDIT_DESIGNATION = "UPDATE `t_designation_master` SET `Designation_Name`=? WHERE `Designation_Id` = ?";
	
	public static final String DELETE_DESIGNATION = "DELETE FROM `t_designation_master` WHERE `Designation_Id` = ?";
	
	public static final String GET_DRIVETYPE_DATA = "SELECT Drive_Type_Id AS HEADER_ID, Drive_Type_Name AS HEADER_NAME, Drive_Type_Category AS HEADER_DESC FROM `t_drive_type_master`";
	
	public static final String ADD_DRIVETYPE = "INSERT INTO `t_drive_type_master` (`Drive_Type_Name`,`Drive_Type_Category`) VALUES (?, ?)";
	
	public static final String EDIT_DRIVETYPE = "UPDATE `t_drive_type_master` SET `Drive_Type_Name` = ?,`Drive_Type_Category` = ? WHERE `Drive_Type_Id` = ?";
	
	public static final String DELETE_DRIVETYPE = "DELETE FROM `t_drive_type_master` WHERE `Drive_Type_Id` = ?";
	
	public static final String GET_ENGINETYPE_DATA = "SELECT `Engine_Type_Id` AS HEADER_ID, `Engine_Name` AS HEADER_NAME FROM `t_engine_type_master`";
	
	public static final String ADD_ENGINETYPE = "INSERT INTO `t_engine_type_master` (`Engine_Name`) VALUES (?)";
	
	public static final String EDIT_ENGINETYPE = "UPDATE `t_engine_type_master` SET `Engine_Name`=? WHERE `Engine_Type_Id` = ?";
	
	public static final String DELETE_ENGINETYPE = "DELETE FROM `t_engine_type_master` WHERE `Engine_Type_Id`=?";
	
	public static final String GET_HYPOTHECATED_DATA = "SELECT `Hypothecated_Id` AS HEADER_ID, `Hypothecated_Name` AS HEADER_NAME FROM `t_hypothecated_to_master`";
	
	public static final String ADD_HYPOTHECATED = "INSERT INTO `t_hypothecated_to_master` (`Hypothecated_Name`) VALUES (?)";
	
	public static final String EDIT_HYPOTHECATED = "UPDATE `t_hypothecated_to_master` SET `Hypothecated_Name`=? WHERE `Hypothecated_Id` = ?";
	
	public static final String DELETE_HYPOTHECATED = "DELETE FROM `t_hypothecated_to_master` WHERE `Hypothecated_Id`=?";
	
	public static final String GET_OCCUPATION_DATA = "SELECT `Occupation_Id` AS HEADER_ID, `Occupation_Name` AS HEADER_NAME FROM `t_occupation_master`";
	
	public static final String ADD_OCCUPATION = "INSERT INTO `t_occupation_master` (`Occupation_Name`) VALUES (?)";
	
	public static final String EDIT_OCCUPATION = "UPDATE `t_occupation_master` SET `Occupation_Name`=? WHERE `Occupation_Id` = ?";
	
	public static final String DELETE_OCCUPATION = "DELETE FROM `t_occupation_master` WHERE `Occupation_Id`=?";
	
	public static final String GET_OWNERTYPE_DATA = "SELECT `Owner_Type_Id` AS HEADER_ID, `Owner_Name` AS HEADER_NAME FROM `t_owner_master`";
	
	public static final String ADD_OWNERTYPE = "INSERT INTO `t_owner_master` (`Owner_Name`) VALUES (?)";
	
	public static final String EDIT_OWNERTYPE = "UPDATE `t_owner_master` SET `Owner_Name`=? WHERE `Owner_Type_Id` = ?";
	
	public static final String DELETE_OWNERTYPE = "DELETE FROM `t_owner_master` WHERE `Owner_Type_Id`=?";
	
	public static final String GET_COURTESY_DATA = "SELECT `Courtesy_Title_Id` AS HEADER_ID, `Courtesy_Name` AS HEADER_NAME FROM `t_courtesy_title_master`";
	
	public static final String ADD_COURTESY = "INSERT INTO `t_courtesy_title_master` (`Courtesy_Name`) VALUES (?)";
	
	public static final String EDIT_COURTESY = "UPDATE `t_courtesy_title_master` SET `Courtesy_Name`=? WHERE `Courtesy_Title_Id` = ?";
	
	public static final String DELETE_COURTESY = "DELETE FROM `t_courtesy_title_master` WHERE `Courtesy_Title_Id`=?";
	
	public static final String GET_VEHICLE_COMPANY_DATA = "SELECT `Vehicle_Company_Id` AS HEADER_ID, `Vehicle_Company_Name` AS HEADER_NAME FROM `t_vehicle_company_master`";
	
	public static final String ADD_VEHICLE_COMPANY = "INSERT INTO `t_vehicle_company_master` (`Vehicle_Company_Name`) VALUES (?)";
	
	public static final String EDIT_VEHICLE_COMPANY = "UPDATE `t_vehicle_company_master` SET `Vehicle_Company_Name`=? WHERE `Vehicle_Company_Id` = ?";
	
	public static final String DELETE_VEHICLE_COMPANY = "DELETE FROM `t_vehicle_company_master` WHERE `Vehicle_Company_Id`=?";
	
	public static final String GET_VEHICLE_MODEL_DATA = "SELECT "
														+ "  a.`Vehicle_Model_Id` AS HEADER_ID, "
														+ "  a.`Vehicle_Model_Name` AS HEADER_NAME, "
														+ "  a.`Vehicle_Company_Id` AS OTHER_ID, "
														+ "  (SELECT "
														+ "    `Vehicle_Company_Name` "
														+ "  FROM "
														+ "    `t_vehicle_company_master` "
														+ "  WHERE `Vehicle_Company_Id` = a.`Vehicle_Company_Id`) AS OTHER_NAME," 
														+ "a.`Engine_CC_Kilowatt_Horsepower`,a.`Seating_Capacity`,a.`Unladen_Weight` "
														+ "FROM "
														+ "  `t_vehicle_model_master` a";
	
	public static final String ADD_VEHICLE_MODEL = "INSERT INTO `t_vehicle_model_master` (`Vehicle_Model_Name`,`Vehicle_Company_Id`,`Engine_CC_Kilowatt_Horsepower`,`Seating_Capacity`,Unladen_Weight) VALUES (?,?,?,?,?)";
	
	public static final String EDIT_VEHICLE_MODEL = "UPDATE `t_vehicle_model_master` SET `Vehicle_Model_Name`=?, `Vehicle_Company_Id`=?," +
			"`Engine_CC_Kilowatt_Horsepower`=?,`Seating_Capacity`=?,Unladen_Weight=? WHERE `Vehicle_Model_Id` = ?";
	
	public static final String DELETE_VEHICLE_MODEL = "DELETE FROM `t_vehicle_model_master` WHERE `Vehicle_Model_Id`=?";
	
	public static final String GET_VEHICLE_TYPE_DATA = "SELECT `Vehicle_Type_Id` AS HEADER_ID, `Vehicle_Type_Name` AS HEADER_NAME, `Vehicle_Type_Number` AS NUMBER FROM `t_vehicle_type_master`";
	
	public static final String ADD_VEHICLE_TYPE = "INSERT INTO `t_vehicle_type_master` (`Vehicle_Type_Name`,`Vehicle_Type_Number`) VALUES (?,?)";
	
	public static final String EDIT_VEHICLE_TYPE = "UPDATE `t_vehicle_type_master` SET `Vehicle_Type_Name`=?, `Vehicle_Type_Number`=? WHERE `Vehicle_Type_Id` = ?";
	
	public static final String DELETE_VEHICLE_TYPE = "DELETE FROM `t_vehicle_type_master` WHERE `Vehicle_Type_Id`=?";
	
	public static final String GET_REGISTRATION_CODE_DATA = "SELECT `Vehicle_Registration_Id` AS HEADER_ID, `Vehicle_Reg_Code_Name` AS HEADER_NAME FROM `t_vehicle_registration_code_master`";
	
	public static final String ADD_REGISTRATION_CODE = "INSERT INTO `t_vehicle_registration_code_master` (`Vehicle_Reg_Code_Name`) VALUES (?)";
	
	public static final String EDIT_REGISTRATION_CODE = "UPDATE `t_vehicle_registration_code_master` SET `Vehicle_Reg_Code_Name`=? WHERE `Vehicle_Registration_Id` = ?";
	
	public static final String DELETE_REGISTRATION_CODE = "DELETE FROM `t_vehicle_registration_code_master` WHERE `Vehicle_Registration_Id`=?";
	
	public static final String GET_DEALER_DATA = "SELECT `Dealer_Id` AS HEADER_ID, `Dealer_Name` AS HEADER_NAME FROM `t_dealer_master`";
	
	public static final String ADD_DEALER = "INSERT INTO `t_dealer_master` (`Dealer_Name`) VALUES (?)";
	
	public static final String EDIT_DEALER = "UPDATE `t_dealer_master` SET `Dealer_Name`=? WHERE `Dealer_Id` = ?";
	
	public static final String DELETE_DEALER = "DELETE FROM `t_dealer_master` WHERE `Dealer_Id`=?";
	
	public static final String GET_REASON_DATA = "SELECT `Reason_Id` AS HEADER_ID, `Reason` AS HEADER_NAME FROM `t_reason_master`";
	
	public static final String ADD_REASON = "INSERT INTO `t_reason_master` (`Reason`) VALUES (?)";
	
	public static final String EDIT_REASON = "UPDATE `t_reason_master` SET `Reason`=? WHERE `Reason_Id` = ?";
	
	public static final String DELETE_REASON = "DELETE FROM `t_reason_master` WHERE `Reason_Id`=?";
	
	public static final String GET_BASE_OFFICE_DATA = "SELECT "
														+ "  a.`base_office_id` AS HEADER_ID, "
														+ "  a.`base_office_name` AS HEADER_NAME, "
														+ "  a.`region_id` AS OTHER_ID, "
														+ "  (SELECT "
														+ "    `region_name` "
														+ "  FROM "
														+ "    `t_region_master` "
														+ "  WHERE `region_id` = a.`region_id`) AS OTHER_NAME "
														+ "FROM "
														+ "  `t_base_office_master` a";
	
	public static final String ADD_BASE_OFFICE = "INSERT INTO `t_base_office_master` (`base_office_name`,`region_id`) VALUES (?,?)";
	
	public static final String EDIT_BASE_OFFICE = "UPDATE `t_base_office_master` SET `base_office_name`=?,`region_id`=? WHERE `base_office_id` = ?";
	
	public static final String DELETE_BASE_OFFICE = "DELETE FROM `t_base_office_master` WHERE `base_office_id`=?";
	
	public static final String GET_BLOOD_GROUP_DATA = "SELECT `blood_group_id` AS HEADER_ID, `blood_group_type` AS HEADER_NAME FROM `t_blood_group_master`";
	
	public static final String ADD_BLOOD_GROUP = "INSERT INTO `t_blood_group_master` (`blood_group_type`) VALUES (?) ;";
	
	public static final String EDIT_BLOOD_GROUP = "UPDATE `t_blood_group_master` SET `blood_group_type` = ? WHERE `blood_group_id` = ?";
	
	public static final String DELETE_BLOOD_GROUP = "DELETE FROM `t_blood_group_master` WHERE `blood_group_id` = ?";
	
	public static final String GET_PRACTICAL_CRITERIA_DATA = "SELECT `Practical_Criteria_Id` AS HEADER_ID, `Criteria_Name` AS HEADER_NAME, `Full_Point` AS NUMBER FROM `t_practical_test_criteria_master`";
	
	public static final String ADD_PRACTICAL_CRITERIA = "INSERT INTO `t_practical_test_criteria_master` (`Criteria_Name`,`Full_Point`) VALUES (?,?)";
	
	public static final String EDIT_PRACTICAL_CRITERIA = "UPDATE `t_practical_test_criteria_master` SET `Criteria_Name` = ?, `Full_Point` = ? WHERE `Practical_Criteria_Id` = ?";
	
	public static final String DELETE_PRACTICAL_CRITERIA = "DELETE FROM `t_practical_test_criteria_master` WHERE `Practical_Criteria_Id` = ?";
	
	public static final String GET_VILLAGE_DATA = "SELECT "
													+ "  a.`Village_Serial_No` AS HEADER_ID, "
													+ "  a.`Village_Name` AS HEADER_NAME, "
													+ "  a.`Gewog_Serial_No` AS OTHER_ID, "
													+ "  (SELECT "
													+ "    `Gewog_Name` "
													+ "  FROM "
													+ "    `t_gewog_master` "
													+ "  WHERE `Gewog_Id` = a.`Gewog_Serial_No`) AS OTHER_NAME "
													+ "FROM "
													+ "  `t_village_master` a";
	
	public static final String ADD_VILLAGE = "INSERT INTO `t_village_master` (`Village_Name`,`Gewog_Serial_No`) VALUES (?,?)";
	
	public static final String EDIT_VILLAGE = "UPDATE `t_village_master` SET `Village_Name` = ?, `Gewog_Serial_No` = ? WHERE `Village_Serial_No` = ?";

	public static final String EDIT_COLOUR = "UPDATE `t_colour_master` SET `Colour_Name` = ?, `Description` = ? WHERE `Colour_Id` = ?";

	public static final String DELETE_VILLAGE = "DELETE FROM `t_village_master` WHERE `Village_Serial_No` = ?";

	public static final String DELETE_COLOUR = "DELETE FROM `t_colour_master` WHERE `Colour_Id` = ?";
	
	public static final String GET_COLOUR_MASTER_DATA = "SELECT `Colour_Id` AS HEADER_ID, `Colour_Name` AS HEADER_NAME,`Description` AS HEADER_DESC FROM `t_colour_master`";

	public static final String ADD_COLOUR = "INSERT INTO `t_colour_master` (`Colour_Name`,`Description`) VALUES (?,?)";
	
	public static final String ADD_DIPLOMATS = "INSERT INTO `t_diplomat_master` "
												+ "            (`Name`, "
												+ "             `Diplomat_Code`) "
												+ "VALUES (?,?);";
	
	public static final String ADD_PRIVATE_COMPANY_MASTER = "INSERT INTO  `t_private_company_master` (`Private_Name`)VALUES (?);";
	
	public static final String ADD_ROUTE_MASTER = "INSERT INTO  `t_route_master` (`Route_Name`)VALUES (?);";
												
	

	public static final String DIPLOMAT_DATA = "SELECT a.`Diplomat_Master_Id` HEADER_ID,a.`Diplomat_Code` HEADER_NAME, a.Name HEADER_DESC "
	+ " FROM t_diplomat_master a ";

	public static final String GET_PRIVATE_COMPANY_MASTER = "SELECT a.`Private_Id` HEADER_ID,a.`Private_Name` HEADER_NAME FROM t_private_company_master a ";


	public static final String GET_ROUTE_MASTER = "SELECT a.`Route_Id` HEADER_ID,a.`Route_Name` HEADER_NAME FROM t_route_master a";
	
	

	public static final String DELETE_DIPLOMATS =  "DELETE FROM `t_diplomat_master` WHERE `Diplomat_Master_Id` = ?";

	public static final String EDIT_DIPLOMATS =  "UPDATE `t_diplomat_master` SET `Name` = ?,`Diplomat_Code` = ? WHERE `Diplomat_Master_Id` = ?;";

	public static final String EDIT_PRIVATE_COMPANY =  "UPDATE t_private_company_master SET `Private_Name`=? WHERE `Private_Id`=?"; 

	public static final String EDIT_ROUTE_MASTER =  "UPDATE t_route_master SET `Route_Name`=? WHERE `Route_Id`=?"; 
	
}
