package bt.gov.rsta.eralis.util;

public class PaymentSQLConstant 
{
	public static final String GET_LICENSE_PAYMENT_DETAILS = "SELECT Amount, License_Card_Cost FROM t_license_constant WHERE Request_Type=? AND Service_Type=? AND License_Type=?";   
	
	public static final String GET_LAST_LEARNER_EXPIRY_DATE = "SELECT "
															+ "  IF( "
															+ "    a.`Learner_License_Info_Id` IN "
															+ "    (SELECT "
															+ "      `Learner_License_Id` "
															+ "    FROM "
															+ "      `t_learner_license_renewal_dtls` "
															+ "    WHERE `Learner_License_Id` = ?), "
															+ "    (SELECT "
															+ "      MAX(`Expiry_Date`) "
															+ "    FROM "
															+ "      `t_learner_license_renewal_dtls` "
															+ "    WHERE `Learner_License_Id` = ?), "
															+ "    a.`Expiry_Date` "
															+ "  ) expiryDate "
															+ "FROM "
															+ "  t_learner_license_dtls a "
															+ "WHERE a.`Learner_License_Info_Id` = ?";
	
	public static final String GET_PENALTY_DETAILS = "SELECT Penalty_Per_Day, Max_Penalty FROM t_penalty_constant WHERE Request_Type=? AND Service_Type=?";
	
	public static final String GET_MAX_EXPIRY_FOR_LICENSE = "SELECT "
		+ "  IF( "
		+ "    a.`Driving_License_Id` IN "
		+ "    (SELECT "
		+ "      `License_Id` "
		+ "    FROM "
		+ "      `t_driving_license_renewal_dtls` "
		+ "    WHERE `License_Id` = ?), "
		+ "    (SELECT "
		+ "      MAX(Expiry_Date) "
		+ "    FROM "
		+ "      `t_driving_license_renewal_dtls` "
		+ "    WHERE `License_Id` = ?), "
		+ "    a.`Expiry_Date` "
		+ "  ) expiryDate "
		+ "FROM "
		+ "  `t_driving_license_dtls` a "
		+ "WHERE a.`Driving_License_Id` = ?";
	
	public static final String GET_CUSTOMER_ID = "SELECT a.Customer_Id FROM t_driving_license_dtls a WHERE a.`Driving_License_Id`=?";
	
	public static final String GET_ORDINARY_LICENSE_ID = "SELECT "
		+ "  a.`Driving_License_Id` "
		+ "FROM "
		+ "  t_driving_license_dtls a "
		+ "WHERE a.`Customer_Id` = ? "
		+ "  AND a.`Driving_License_Id` <> ?";
	
	public static final String GET_VEHICLE_TYPE_DESC = "SELECT Description FROM t_vehicle_type_master WHERE Vehicle_Type_Id=?";
	
	public static final String GET_VEHICLE_PAYMENT_DETAILS = "SELECT "
															+ "  * "
															+ "FROM "
															+ "  t_vehicle_constant a "
															+ "WHERE a.`Vehicle_Type_Id` = ?";
	
	public static final String GET_PERMIT_PAYMENT_DETAILS = "SELECT "
		+ "  * "
		+ "FROM "
		+ "  t_permit_constant a "
		+ "WHERE a.`Vehicle_Type_Id` = ?";
	
	public static final String GET_MAX_EXPIRY_FOR_VEHICLE = "SELECT "
		+ "  IF( "
		+ "    a.`Vehicle_Reg_Dtls_Id` IN "
		+ "    (SELECT "
		+ "      `Vehicle_Id` "
		+ "    FROM "
		+ "      `t_vehicle_renewal_dtls` WHERE Vehicle_Id=?), "
		+ "    (SELECT "
		+ "      MAX(Expiry_Date) "
		+ "    FROM "
		+ "      `t_vehicle_renewal_dtls` WHERE Vehicle_Id=?), "
		+ "    a.`Expiry_Date` "
		+ "  ) expiryDate "
		+ "FROM "
		+ "  `t_vehicle_registration_dtls` a "
		+ "WHERE a.`Vehicle_Reg_Dtls_Id`=?";
	
	public static final String GET_INITIAL_REGISTRATION_DETAILS = "SELECT a.`Registration_Date`, a.`Price` FROM t_vehicle_registration_dtls a WHERE a.`Vehicle_Reg_Dtls_Id`=?";
}
