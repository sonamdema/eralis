package bt.gov.rsta.eralis.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateConversionUtil 
{

	private static String EMPTY_STRING = "";
	/**
	 * DateConversionUtil.java
	 * Purpose: converts to java.util.Date from date in String format
	 *
	 * @author poojan
	 * @version 0.1 27/06/2011
	 * @param ipDate - Input Date in String format
	 * @param ipDateFormat - The format of the Input Date needs to be mentioned e.g. yyyy-MM-dd
	 * @param opDateFormat - The format of the Output Date needs to be mentioned e.g. dd/MM/yyyy
	 */

	public static java.util.Date convertToDateFromStringInUserSpecifiedFormat(String ipDate, String ipDateFormat, String opDateFormat){
        SimpleDateFormat opFormatter = new SimpleDateFormat(ipDateFormat);
        java.util.Date utilDate = null;
	    if(ipDate!=null){    
        	try {
	                utilDate = opFormatter.parse(ipDate);
	        } catch (ParseException e) {
	            e.printStackTrace();
	        	return null;
	        }
	        SimpleDateFormat outFormatter = new SimpleDateFormat(opDateFormat);
	        String dateString=outFormatter.format(utilDate);
	        try {
				utilDate=outFormatter.parse(dateString);
			} catch (ParseException e) {
	            e.printStackTrace();
				return null;
			}
	    }else{
	    	utilDate = null;
	    }
        return utilDate;
	}
	
	
	/*
	 * @method : getUIDateFromDBDate
	 * @purpose : this method provides UI end Date String from DataBase end Date string
	 * 			  like 31-12-2010 from 2010-12-31
	 */
	public static String getUIDateFromDBDate(String dbDate){
		String uiDate = EMPTY_STRING;
		
		if(dbDate!=null && !dbDate.equals(EMPTY_STRING)){
			uiDate=dbDate.substring(8)+"-"+dbDate.substring(5, 7)+"-"+dbDate.substring(0, 4);
		}
		
		return uiDate;
	}
	
	
	/*
	 * @method : workingDateCalculator
	 * @purpose : this method calculates Saturday and Sunday on the given working days and provide final
	 *            date on basis on given start date  
	 * @output : java.util.Date    
	 * @input : java.util.Date, int          
	 */
	public static Date workingDateCalculator(Date startDate, int workingDays, List<Date> holidayList) throws Exception{
		Date finalDate = null;
		int extraDays = 0;
		
		if(startDate!=null && workingDays>0){
			Calendar cal =Calendar.getInstance();
			cal.setTime(startDate);
			
			for(int i=0; i<workingDays; i++){
				if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
						|| (holidayList!=null && !holidayList.isEmpty() && holidayList.contains(cal.getTime())))
					extraDays++;
				
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}
			
			if(extraDays>0)
				cal.add(Calendar.DAY_OF_MONTH, extraDays);
			
			finalDate = cal.getTime();
		}
		
		return finalDate;
	}
	
	/*
	 * @method : getExpectedDate
	 * @purpose : this method will return expected date of completion of any service with respect to service id 
	 * @output : java.util.Date    
	 * @input : String, List<Date>          
	 */
	/*public Date getExpectedDate(String serviceId, List<Date> holidayList){
		Log.debug("######## Enter DateConversionUtil[getExpectedDate] with serviceId = "+serviceId+" and holidayList = "+holidayList);
		Date finalDate = new Date();
		int slaDuration = 0;
		try{
			if(serviceId!=null ){
				slaDuration = new CommonDataBaseTransactionUtil().getSLAduration(serviceId);
				finalDate = workingDateCalculator(finalDate, slaDuration, holidayList);
			}
		
			if(finalDate==null)
				finalDate = new Date();
		}
		catch(Exception e){
			Log.error("######## Error in DateConversionUtil[getExpectedDate] : " + e.toString());
		}
		
		Log.debug("######## Exit DateConversionUtil[getExpectedDate] with finalDate = " + finalDate);
		return finalDate;
	}*/
	
	public static String convertToAnotherPattern(String inputDate, String ipFormat, String opFormat){
		java.util.Date utilDate = null;
		SimpleDateFormat opFormatter = new SimpleDateFormat(ipFormat/*"yyyy-MM-dd'T'HH:mm:ss.SSSZ"*/);
		try {
                utilDate = opFormatter.parse(inputDate);
        } catch (ParseException e) {
                e.printStackTrace();
        }
		SimpleDateFormat outFormatter = new SimpleDateFormat(opFormat/*"MM/dd/yyyy"*/);
        String dateString=outFormatter.format(utilDate);
        return dateString;
	}
	
}
