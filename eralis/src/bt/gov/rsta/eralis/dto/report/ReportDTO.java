package bt.gov.rsta.eralis.dto.report;

import java.io.Serializable;

public class ReportDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String columnArray[][];
	private String headerCol;
	private int totalCol;
	private int totalRow;
	private String description;
	private String whereParam;
	private String paramVal;

	
	public String getParamVal() {
		return paramVal;
	}

	public void setParamVal(String paramVal) {
		this.paramVal = paramVal;
	}

	public String getWhereParam() {
		return whereParam;
	}

	public void setWhereParam(String whereParam) {
		this.whereParam = whereParam;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTotalRow() {
		return totalRow;
	}

	public void setTotalRow(int totalRow2) {
		this.totalRow = totalRow2;
	}

	public int getTotalCol() {
		return totalCol;
	}

	public void setTotalCol(int totalCol) {
		this.totalCol = totalCol;
	}

	public String getHeaderCol() {
		return headerCol;
	}

	public void setHeaderCol(String headerCol) {
		this.headerCol = headerCol;
	}

	public String[][] getColumnArray() {
		return columnArray;
	}

	public void setColumnArray(String[][] columnArray) {
		this.columnArray = columnArray;
	}
	 
	
}
