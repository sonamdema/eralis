package bt.gov.rsta.eralis.dto.administration;

import java.io.Serializable;

public class RoleDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String roleId;
	private String roleName;
	private String roleStatus;
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleStatus() {
		return roleStatus;
	}
	public void setRoleStatus(String roleStatus) {
		this.roleStatus = roleStatus;
	}
}
