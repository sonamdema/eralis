package bt.gov.rsta.eralis.dto.administration;

import java.io.Serializable;

public class UserDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String loginId;
	private String userName;
	private String region;
	private String dzongkhag;
	private String isActive;
	private String roleName;
	private String emailAddress;
	private String mobileNumber;
	private String regionId;
	private String dzongkhagId;
	private String roleId;
	private String userType;
	private String agencyCode;
	private String showTaskList;
	private String jurisType;
	private String jurisTypeDesc;
	private String jurisTypeId;
	private String baseOffice;
	private String baseOfficeId;
	
	public String getBaseOffice() {
		return baseOffice;
	}
	public void setBaseOffice(String baseOffice) {
		this.baseOffice = baseOffice;
	}
	public String getBaseOfficeId() {
		return baseOfficeId;
	}
	public void setBaseOfficeId(String baseOfficeId) {
		this.baseOfficeId = baseOfficeId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getAgencyCode() {
		return agencyCode;
	}
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}
	public String getShowTaskList() {
		return showTaskList;
	}
	public void setShowTaskList(String showTaskList) {
		this.showTaskList = showTaskList;
	}
	public String getJurisType() {
		return jurisType;
	}
	public void setJurisType(String jurisType) {
		this.jurisType = jurisType;
	}
	public String getJurisTypeDesc() {
		return jurisTypeDesc;
	}
	public void setJurisTypeDesc(String jurisTypeDesc) {
		this.jurisTypeDesc = jurisTypeDesc;
	}
	public String getJurisTypeId() {
		return jurisTypeId;
	}
	public void setJurisTypeId(String jurisTypeId) {
		this.jurisTypeId = jurisTypeId;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getDzongkhagId() {
		return dzongkhagId;
	}
	public void setDzongkhagId(String dzongkhagId) {
		this.dzongkhagId = dzongkhagId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
