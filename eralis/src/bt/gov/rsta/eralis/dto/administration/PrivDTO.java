package bt.gov.rsta.eralis.dto.administration;

import java.io.Serializable;

public class PrivDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String privId;
	private String privName;
	public String getPrivId() {
		return privId;
	}
	public void setPrivId(String privId) {
		this.privId = privId;
	}
	public String getPrivName() {
		return privName;
	}
	public void setPrivName(String privName) {
		this.privName = privName;
	}
}
