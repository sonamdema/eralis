package bt.gov.rsta.eralis.dto.online_service;

public class ApplicationStatusTrailDTO {

	private String actor = null;
	private String role = null;
	private String action = null;
	private String actionCode = null;
	private String date = null;
	private String serviceID=null;
	private String applicationNo=null;
	private String claimedBy=null;
	private String serviceName=null;

	private String cid=null;
	private String serviceTypeNo=null;
	private String submittedTo=null;
	
	
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getServiceTypeNo() {
		return serviceTypeNo;
	}
	public void setServiceTypeNo(String serviceTypeNo) {
		this.serviceTypeNo = serviceTypeNo;
	}
	public String getSubmittedTo() {
		return submittedTo;
	}
	public void setSubmittedTo(String submittedTo) {
		this.submittedTo = submittedTo;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getClaimedBy() {
		return claimedBy;
	}
	public void setClaimedBy(String claimedBy) {
		this.claimedBy = claimedBy;
	}
	public String getActor() {
		return actor;
	}
	public void setActor(String actor) {
		this.actor = actor;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the serviceID
	 */
	public String getServiceID() {
		return serviceID;
	}
	/**
	 * @param serviceID the serviceID to set
	 */
	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}
	
}
