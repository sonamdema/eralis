package bt.gov.rsta.eralis.dto.online_service;

import java.util.ArrayList;
import java.util.List;

public class ApplicationStatusDTO 
{
	private String role = null;
	private String latestStatus = null;
	private String latestStatusCode = null;
	private List<ApplicationStatusTrailDTO> appStatusTrailList = new ArrayList<ApplicationStatusTrailDTO>();
	private String appLinkEnable = null;
	private String viewStatusEnable = null;
	private String applicationNo = null;
	private String serviceCode = null;
	private String serviceName	=	null;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getLatestStatus() {
		return latestStatus;
	}
	public void setLatestStatus(String latestStatus) {
		this.latestStatus = latestStatus;
	}
	public String getLatestStatusCode() {
		return latestStatusCode;
	}
	public void setLatestStatusCode(String latestStatusCode) {
		this.latestStatusCode = latestStatusCode;
	}
	public List<ApplicationStatusTrailDTO> getAppStatusTrailList() {
		return appStatusTrailList;
	}
	public void setAppStatusTrailList(
			List<ApplicationStatusTrailDTO> appStatusTrailList) {
		this.appStatusTrailList = appStatusTrailList;
	}
	public String getAppLinkEnable() {
		return appLinkEnable;
	}
	public void setAppLinkEnable(String appLinkEnable) {
		this.appLinkEnable = appLinkEnable;
	}
	public String getViewStatusEnable() {
		return viewStatusEnable;
	}
	public void setViewStatusEnable(String viewStatusEnable) {
		this.viewStatusEnable = viewStatusEnable;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	
}
