package bt.gov.rsta.eralis.dto.vehicle;

import java.io.Serializable;
import java.util.List;

public class FitnessMainDTO implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private String itemId;
	private String itemName;
	private List<FitnessItemDTO> fitnessItemList;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public List<FitnessItemDTO> getFitnessItemList() {
		return fitnessItemList;
	}
	public void setFitnessItemList(List<FitnessItemDTO> fitnessItemList) {
		this.fitnessItemList = fitnessItemList;
	}
}
