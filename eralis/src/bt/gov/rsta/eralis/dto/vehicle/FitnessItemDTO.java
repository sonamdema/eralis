package bt.gov.rsta.eralis.dto.vehicle;

import java.io.Serializable;

public class FitnessItemDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String itemId;
	private String itemName;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
}
