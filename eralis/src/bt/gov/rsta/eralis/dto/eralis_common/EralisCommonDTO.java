package bt.gov.rsta.eralis.dto.eralis_common;

import java.io.Serializable;

import org.apache.struts.upload.FormFile;

public class EralisCommonDTO implements Serializable
{
	private static final long serialVersionUID = 1L;	
	
	private String organizationTypeId;
	private String dzongkhagId;
	private String learnerId;
	private String customerID;
	private String buyerName;
	private String buyerAddress;
	private String amount;
	private String manufactureYear;
	private String titleOfcourtesy;
	private String firstname;
	private String middlename;
	private String lastName;
	private String occupation;
	private String nationality;
	private String CID;
	private String DOB;
	private String bloodGroup;
	private String bloodGroupId;
	private String fathersName;
	private String gender;
	private String identificationMarks;
	private String remarks;
	private String drivetype;
	private String national;
	private String searchpermitID;
	private String dzongkhag;
	private String gewog;
	private String village;
	private String country;
	private String address;
	private String contactAddress;
	private String phone;
	private String email;
	private FormFile upload;
	private String code;
	private String type;
	private String ministry;
	private String department;
	private String region;
	private String name;
	private String conductorName;
	private String vehicleNo;
	private String LLNo;
	private String accidentDate;
	private String accidentCause;
	private String accidentSite;
	private String licenseNo;
	private String driverName;
	private String permitID;
	private String registrationNo;
	private String baseoffice;
	private String routeBetween;
	private String to;
	private String vehicleType;
	private String carryingCapacity;
	private String seatCapacity;
	private String receiptNo;
	private String dateOfissue;
	private String validUpto;
	private String licensetype;
	private String numberOfDeath;
	private String numberofInjured;
	private String numberofunInjured;
	private String registrationcertificate;
	private String issurance;
	private String certificateOfroadworthiness;
	private String emmissioncertificate;
	private String presentDzongkhag;
	private String manualFlag;
	private String age;
	private Integer learnerapplicationage;
	private String vehicleTypeName;
	private String vehicleTypeDesc;
	//TOOLS
	private String permanentAddress;
	private String permitIDNo;
	private String personalInfoId;
	private String permitDetailsId;
	private String learnerlicenseInfoId;
	private String ownerId;
	private String registrationDate;
	private String organizationInfoId;
	private String renewalInfoId;
	private String OwnerType;
	private String OwnerName;
	private String EngineNumber;
	private String ChasisNumber;
	private String expiryDate;
	private String company;
	private String model;
	private String engineType;
	private String loadCapacity;
	private String color;
	private String status;
	private String renewalDate;
	private String receiptDate;
	private String renewalHistoryId;
	private String drivinglicenseId;
	private String organisationInfoId;
	private String formType;
	private String transferorCustomerId;
	private String transfereeCustomerId;
	private String newVehicleNo;
	private String latestVehicleNumber;
	private String selectall;
	private String vehicleRegistrationType;
	private String applicationNo;
	private String personalOwnerName;
	private String testDate;
	private String applicantNo;
	private Integer licenseApplicationAge;
	private Integer commercialApplicationAge;
	private String caseNo;
	private String policeStation;
	private String occurrenceDate;
	private String occurrenceTime;
	private String reportDate;
	private String reportTime;
	private String occurencePlace;
	private String mobileNo;
	private String make;
	private String licenseStatus;
	private String duplicateIssueDate;
	private String transferDate;
	private String vehicleHorsePower;
	private String vehicleKiloWatt;
	private String engineCC;
	private String vPrefix;
	private String vehicleConstantId;
	private String flag;
	private String rcCost;
	private String fitnessAmount;
	private String ownershipTransferFees;
	private String regionalTransferFee;
	private String conversionFee;
	private String transferTax;
	private String licenseConstantId;
	private String requestType;
	private String serviceType;
	private String licenseCardCost;
	private String penaltyId;
	private String penaltyPerDay;
	private String maxPenalty;
	private String vehicleId;
	private String transfereeName;
	private String transferorName;
	private String learnerLicenseId;
	private String overallObtainedMark;
	private String CO;
	private String HSU;
	private String testResult;
	
	private String offenceDate;
	private String placeOfInspection;
	private String TIN;
	private String offence1;
	private String offence2;
	private String offence3;
	private String privateCompany;
	private String bloodgroup;
	private String driveTypeList;
	private String certifyingDoctor;
	private FormFile fileMC;
	private FormFile fileApplForm;
	private FormFile supportDoc;
	private FormFile rcCertificate;
	private String reason;
	private String isInternational;
	private String inspectedBy;
	private String trafficBranch;
	private String timeOfInspection;
	private String inspectionType;
	private String testDateId;
	private String regionId;
	private String saleDeedDate;
	private String saleDeedAmount;
	private String nocFees;
	private String penalty;
	private String imagePath;
	
	private String permitIssuedTo;
	private String licenseNumber;
	private String vehicleNumber;
	private String vehicleTypeBV;
	private String searchType;
	private String isBluebookSeized;
	private String isLicenseSeized;
	private String isPaid;
	private String offenceId;
	private String cancellationDate;
	private String licensePunchNo;
	private String totalDuration;
	private String dateDifference;
	private String conversionDate;
	private String convertedFrom;
	private String owner;
	private String ownerTypeDesc;
	private String pendingId;
	private String emissionId;
	private String journeyPurpose;
	private String cancellationdate;
	private String cancellationReason;
	private String withdrawnDate;
	private String withdrawnReason;
	private String dzongkhagName;
	private String gewogName;
	private String bloodTypeName;
	
	private String vehicleAge;
	private String buyerCID;
	private String busTypeId;
	private String busTypeDesc;
	private String ownershipType; 
	private String driverDtls;
	private String vehilceDtls;
	private String killedDtls;
	private String injuredDtls;
	private String accidentCauseDtls;
	private String dateOfOccurrence;
	private String timeOfOccurrence;
	private String dateOfReport;
	private String timeOfReport;
	private String placeOfOccurrence;
	private String accidentType;
	private String accidentNature;

	private String canDOCName;
	private String canDOCPath;
	private String canDOCUUID;
	private String withDOCName;
	private String withDOCPath;
	private String withDOCUUID;
	private String testType;
	private String instituteName;
	private String latestExpiryDate;
	private String transferExpiryDate;
	private String contactRadio;
	private String topNumber;
	private String exactLocation;
	private String unladenWeight;
	private String offenceLicenseType;
	private String dealerName;
	private String price;
	private String learnerLicenseNo;
	private String permitNo;
	private String lifeSpanExpiryDate;
	private String vehicleOrigin;
	
	private String isDrugAbuse;
	private String alcoholContent;
	private String injurydescription;
	private String passengerBusType;
	private String emergencyPhoneNo;
	private String colorName;
	private String guardianNo;
	private String initialAmount;
	private String initialReceiptDate;
	private String initialReceiptNo;
	private String fileName;
	private String fileUUID;
	private String theoryMarks;
	private String practicalMarks;
	private String issueType;
	private String mothersName;
	private String diplomatId;
	private String outstandingVehicleNo;
	private String outstandingVehicleType;
	private String chasisStatus;
	private String engineNoStatus;
	private String id;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getChasisStatus() {
		return chasisStatus;
	}
	public void setChasisStatus(String chasisStatus) {
		this.chasisStatus = chasisStatus;
	}
	public String getEngineNoStatus() {
		return engineNoStatus;
	}
	public void setEngineNoStatus(String engineNoStatus) {
		this.engineNoStatus = engineNoStatus;
	}
	public String getOutstandingVehicleNo() {
		return outstandingVehicleNo;
	}
	public void setOutstandingVehicleNo(String outstandingVehicleNo) {
		this.outstandingVehicleNo = outstandingVehicleNo;
	}
	public String getOutstandingVehicleType() {
		return outstandingVehicleType;
	}
	public void setOutstandingVehicleType(String outstandingVehicleType) {
		this.outstandingVehicleType = outstandingVehicleType;
	}
	public String getDiplomatId() {
		return diplomatId;
	}
	public void setDiplomatId(String diplomatId) {
		this.diplomatId = diplomatId;
	}
	
	public String getMothersName() {
		return mothersName;
	}
	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public String getTheoryMarks() {
		return theoryMarks;
	}
	public void setTheoryMarks(String theoryMarks) {
		this.theoryMarks = theoryMarks;
	}
	public String getPracticalMarks() {
		return practicalMarks;
	}
	public void setPracticalMarks(String practicalMarks) {
		this.practicalMarks = practicalMarks;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileUUID() {
		return fileUUID;
	}
	public void setFileUUID(String fileUUID) {
		this.fileUUID = fileUUID;
	}
	public String getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(String initialAmount) {
		this.initialAmount = initialAmount;
	}
	public String getInitialReceiptDate() {
		return initialReceiptDate;
	}
	public void setInitialReceiptDate(String initialReceiptDate) {
		this.initialReceiptDate = initialReceiptDate;
	}
	public String getInitialReceiptNo() {
		return initialReceiptNo;
	}
	public void setInitialReceiptNo(String initialReceiptNo) {
		this.initialReceiptNo = initialReceiptNo;
	}
	public String getGuardianNo() {
		return guardianNo;
	}
	public void setGuardianNo(String guardianNo) {
		this.guardianNo = guardianNo;
	}
	public String getColorName() {
		return colorName;
	}
	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	public String getEmergencyPhoneNo() {
		return emergencyPhoneNo;
	}
	public void setEmergencyPhoneNo(String emergencyPhoneNo) {
		this.emergencyPhoneNo = emergencyPhoneNo;
	}
	public String getPassengerBusType() {
		return passengerBusType;
	}
	public void setPassengerBusType(String passengerBusType) {
		this.passengerBusType = passengerBusType;
	}
	public String getIsDrugAbuse() {
		return isDrugAbuse;
	}
	public void setIsDrugAbuse(String isDrugAbuse) {
		this.isDrugAbuse = isDrugAbuse;
	}
	public String getAlcoholContent() {
		return alcoholContent;
	}
	public void setAlcoholContent(String alcoholContent) {
		this.alcoholContent = alcoholContent;
	}
	public String getInjurydescription() {
		return injurydescription;
	}
	public void setInjurydescription(String injurydescription) {
		this.injurydescription = injurydescription;
	}
	public String getVehicleOrigin() {
		return vehicleOrigin;
	}
	public void setVehicleOrigin(String vehicleOrigin) {
		this.vehicleOrigin = vehicleOrigin;
	}
	public String getLifeSpanExpiryDate() {
		return lifeSpanExpiryDate;
	}
	public void setLifeSpanExpiryDate(String lifeSpanExpiryDate) {
		this.lifeSpanExpiryDate = lifeSpanExpiryDate;
	}
	public String getLearnerLicenseNo() {
		return learnerLicenseNo;
	}
	public void setLearnerLicenseNo(String learnerLicenseNo) {
		this.learnerLicenseNo = learnerLicenseNo;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getPermitNo() {
		return permitNo;
	}
	public void setPermitNo(String permitNo) {
		this.permitNo = permitNo;
	}
	public String getUnladenWeight() {
		return unladenWeight;
	}
	public void setUnladenWeight(String unladenWeight) {
		this.unladenWeight = unladenWeight;
	}
	
	public String getOffenceLicenseType() {
		return offenceLicenseType;
	}
	public void setOffenceLicenseType(String offenceLicenseType) {
		this.offenceLicenseType = offenceLicenseType;
	}
	public String getExactLocation() {
		return exactLocation;
	}
	public void setExactLocation(String exactLocation) {
		this.exactLocation = exactLocation;
	}
	public String getTopNumber() {
		return topNumber;
	}
	public void setTopNumber(String topNumber) {
		this.topNumber = topNumber;
	}
	public String getContactRadio() {
		return contactRadio;
	}
	public void setContactRadio(String contactRadio) {
		this.contactRadio = contactRadio;
	}
	public String getTransferExpiryDate() {
		return transferExpiryDate;
	}
	public void setTransferExpiryDate(String transferExpiryDate) {
		this.transferExpiryDate = transferExpiryDate;
	}
	public String getLatestExpiryDate() {
		return latestExpiryDate;
	}
	public void setLatestExpiryDate(String latestExpiryDate) {
		this.latestExpiryDate = latestExpiryDate;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	public String getCanDOCName() {
		return canDOCName;
	}
	public void setCanDOCName(String canDOCName) {
		this.canDOCName = canDOCName;
	}
	public String getCanDOCPath() {
		return canDOCPath;
	}
	public void setCanDOCPath(String canDOCPath) {
		this.canDOCPath = canDOCPath;
	}
	public String getCanDOCUUID() {
		return canDOCUUID;
	}
	public void setCanDOCUUID(String canDOCUUID) {
		this.canDOCUUID = canDOCUUID;
	}
	public String getWithDOCName() {
		return withDOCName;
	}
	public void setWithDOCName(String withDOCName) {
		this.withDOCName = withDOCName;
	}
	public String getWithDOCPath() {
		return withDOCPath;
	}
	public void setWithDOCPath(String withDOCPath) {
		this.withDOCPath = withDOCPath;
	}
	public String getWithDOCUUID() {
		return withDOCUUID;
	}
	public void setWithDOCUUID(String withDOCUUID) {
		this.withDOCUUID = withDOCUUID;
	}
	public String getOwnershipType() {
		return ownershipType;
	}
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}
	public String getDriverDtls() {
		return driverDtls;
	}
	public void setDriverDtls(String driverDtls) {
		this.driverDtls = driverDtls;
	}
	public String getVehilceDtls() {
		return vehilceDtls;
	}
	public void setVehilceDtls(String vehilceDtls) {
		this.vehilceDtls = vehilceDtls;
	}
	public String getKilledDtls() {
		return killedDtls;
	}
	public void setKilledDtls(String killedDtls) {
		this.killedDtls = killedDtls;
	}
	public String getInjuredDtls() {
		return injuredDtls;
	}
	public void setInjuredDtls(String injuredDtls) {
		this.injuredDtls = injuredDtls;
	}
	public String getAccidentCauseDtls() {
		return accidentCauseDtls;
	}
	public void setAccidentCauseDtls(String accidentCauseDtls) {
		this.accidentCauseDtls = accidentCauseDtls;
	}
	public String getDateOfOccurrence() {
		return dateOfOccurrence;
	}
	public void setDateOfOccurrence(String dateOfOccurrence) {
		this.dateOfOccurrence = dateOfOccurrence;
	}
	public String getTimeOfOccurrence() {
		return timeOfOccurrence;
	}
	public void setTimeOfOccurrence(String timeOfOccurrence) {
		this.timeOfOccurrence = timeOfOccurrence;
	}
	public String getDateOfReport() {
		return dateOfReport;
	}
	public void setDateOfReport(String dateOfReport) {
		this.dateOfReport = dateOfReport;
	}
	public String getTimeOfReport() {
		return timeOfReport;
	}
	public void setTimeOfReport(String timeOfReport) {
		this.timeOfReport = timeOfReport;
	}
	public String getPlaceOfOccurrence() {
		return placeOfOccurrence;
	}
	public void setPlaceOfOccurrence(String placeOfOccurrence) {
		this.placeOfOccurrence = placeOfOccurrence;
	}
	public String getAccidentType() {
		return accidentType;
	}
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	public String getAccidentNature() {
		return accidentNature;
	}
	public void setAccidentNature(String accidentNature) {
		this.accidentNature = accidentNature;
	}
	public String getBuyerCID() {
		return buyerCID;
	}
	public void setBuyerCID(String buyerCID) {
		this.buyerCID = buyerCID;
	}
	
	public String getVehicleAge() {
		return vehicleAge;
	}
	public void setVehicleAge(String vehicleAge) {
		this.vehicleAge = vehicleAge;
	}
	public String getDzongkhagName() {
		return dzongkhagName;
	}
	public void setDzongkhagName(String dzongkhagName) {
		this.dzongkhagName = dzongkhagName;
	}
	public String getGewogName() {
		return gewogName;
	}
	public void setGewogName(String gewogName) {
		this.gewogName = gewogName;
	}
	public String getBloodTypeName() {
		return bloodTypeName;
	}
	public void setBloodTypeName(String bloodTypeName) {
		this.bloodTypeName = bloodTypeName;
	}
	public String getBusTypeId() {
		return busTypeId;
	}
	public void setBusTypeId(String busTypeId) {
		this.busTypeId = busTypeId;
	}
	public String getBusTypeDesc() {
		return busTypeDesc;
	}
	public void setBusTypeDesc(String busTypeDesc) {
		this.busTypeDesc = busTypeDesc;
	}
	public String getCancellationdate() {
		return cancellationdate;
	}
	public void setCancellationdate(String cancellationdate) {
		this.cancellationdate = cancellationdate;
	}
	public String getCancellationReason() {
		return cancellationReason;
	}
	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	public String getWithdrawnDate() {
		return withdrawnDate;
	}
	public void setWithdrawnDate(String withdrawnDate) {
		this.withdrawnDate = withdrawnDate;
	}
	public String getWithdrawnReason() {
		return withdrawnReason;
	}
	public void setWithdrawnReason(String withdrawnReason) {
		this.withdrawnReason = withdrawnReason;
	}
	public String getJourneyPurpose() {
		return journeyPurpose;
	}
	public void setJourneyPurpose(String journeyPurpose) {
		this.journeyPurpose = journeyPurpose;
	}
	public String getEmissionId() {
		return emissionId;
	}
	public void setEmissionId(String emissionId) {
		this.emissionId = emissionId;
	}
	public String getPendingId() {
		return pendingId;
	}
	public void setPendingId(String pendingId) {
		this.pendingId = pendingId;
	}
	public String getOwnerTypeDesc() {
		return ownerTypeDesc;
	}
	public void setOwnerTypeDesc(String ownerTypeDesc) {
		this.ownerTypeDesc = ownerTypeDesc;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getConversionDate() {
		return conversionDate;
	}
	public void setConversionDate(String conversionDate) {
		this.conversionDate = conversionDate;
	}
	public String getConvertedFrom() {
		return convertedFrom;
	}
	public void setConvertedFrom(String convertedFrom) {
		this.convertedFrom = convertedFrom;
	}
	public String getTotalDuration() {
		return totalDuration;
	}
	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}
	public String getDateDifference() {
		return dateDifference;
	}
	public void setDateDifference(String dateDifference) {
		this.dateDifference = dateDifference;
	}
	public String getLicensePunchNo() {
		return licensePunchNo;
	}
	public void setLicensePunchNo(String licensePunchNo) {
		this.licensePunchNo = licensePunchNo;
	}
	public String getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public String getOffenceId() {
		return offenceId;
	}
	public void setOffenceId(String offenceId) {
		this.offenceId = offenceId;
	}
	public String getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(String isPaid) {
		this.isPaid = isPaid;
	}
	public String getIsBluebookSeized() {
		return isBluebookSeized;
	}
	public void setIsBluebookSeized(String isBluebookSeized) {
		this.isBluebookSeized = isBluebookSeized;
	}
	public String getIsLicenseSeized() {
		return isLicenseSeized;
	}
	public void setIsLicenseSeized(String isLicenseSeized) {
		this.isLicenseSeized = isLicenseSeized;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getPermitIssuedTo() {
		return permitIssuedTo;
	}
	public void setPermitIssuedTo(String permitIssuedTo) {
		this.permitIssuedTo = permitIssuedTo;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehicleTypeBV() {
		return vehicleTypeBV;
	}
	public void setVehicleTypeBV(String vehicleTypeBV) {
		this.vehicleTypeBV = vehicleTypeBV;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getDzongkhagId() {
		return dzongkhagId;
	}
	public void setDzongkhagId(String dzongkhagId) {
		this.dzongkhagId = dzongkhagId;
	}
	public String getOrganizationTypeId() {
		return organizationTypeId;
	}
	public void setOrganizationTypeId(String organizationTypeId) {
		this.organizationTypeId = organizationTypeId;
	}
	public String getSaleDeedDate() {
		return saleDeedDate;
	}
	public void setSaleDeedDate(String saleDeedDate) {
		this.saleDeedDate = saleDeedDate;
	}
	public String getSaleDeedAmount() {
		return saleDeedAmount;
	}
	public void setSaleDeedAmount(String saleDeedAmount) {
		this.saleDeedAmount = saleDeedAmount;
	}
	public String getNocFees() {
		return nocFees;
	}
	public void setNocFees(String nocFees) {
		this.nocFees = nocFees;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getTestDateId() {
		return testDateId;
	}
	public void setTestDateId(String testDateId) {
		this.testDateId = testDateId;
	}
	public String getInspectionType() {
		return inspectionType;
	}
	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}
	public String getInspectedBy() {
		return inspectedBy;
	}
	public void setInspectedBy(String inspectedBy) {
		this.inspectedBy = inspectedBy;
	}
	public String getTrafficBranch() {
		return trafficBranch;
	}
	public void setTrafficBranch(String trafficBranch) {
		this.trafficBranch = trafficBranch;
	}
	public String getTimeOfInspection() {
		return timeOfInspection;
	}
	public void setTimeOfInspection(String timeOfInspection) {
		this.timeOfInspection = timeOfInspection;
	}
	public String getIsInternational() {
		return isInternational;
	}
	public void setIsInternational(String isInternational) {
		this.isInternational = isInternational;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public FormFile getFileMC() {
		return fileMC;
	}
	public void setFileMC(FormFile fileMC) {
		this.fileMC = fileMC;
	}
	public FormFile getFileApplForm() {
		return fileApplForm;
	}
	public void setFileApplForm(FormFile fileApplForm) {
		this.fileApplForm = fileApplForm;
	}
	public FormFile getSupportDoc() {
		return supportDoc;
	}
	public void setSupportDoc(FormFile supportDoc) {
		this.supportDoc = supportDoc;
	}
	public FormFile getRcCertificate() {
		return rcCertificate;
	}
	public void setRcCertificate(FormFile rcCertificate) {
		this.rcCertificate = rcCertificate;
	}
	public String getCertifyingDoctor() {
		return certifyingDoctor;
	}
	public void setCertifyingDoctor(String certifyingDoctor) {
		this.certifyingDoctor = certifyingDoctor;
	}
	public String getDriveTypeList() {
		return driveTypeList;
	}
	public void setDriveTypeList(String driveTypeList) {
		this.driveTypeList = driveTypeList;
	}
	public String getBloodgroup() {
		return bloodgroup;
	}
	public void setBloodgroup(String bloodgroup) {
		this.bloodgroup = bloodgroup;
	}
	public String getVehicleTypeDesc() {
		return vehicleTypeDesc;
	}
	public void setVehicleTypeDesc(String vehicleTypeDesc) {
		this.vehicleTypeDesc = vehicleTypeDesc;
	}
	public String getPrivateCompany() {
		return privateCompany;
	}
	public void setPrivateCompany(String privateCompany) {
		this.privateCompany = privateCompany;
	}
	public String getOffenceDate() {
		return offenceDate;
	}
	public void setOffenceDate(String offenceDate) {
		this.offenceDate = offenceDate;
	}
	public String getPlaceOfInspection() {
		return placeOfInspection;
	}
	public void setPlaceOfInspection(String placeOfInspection) {
		this.placeOfInspection = placeOfInspection;
	}
	public String getTIN() {
		return TIN;
	}
	public void setTIN(String tIN) {
		TIN = tIN;
	}
	public String getOffence1() {
		return offence1;
	}
	public void setOffence1(String offence1) {
		this.offence1 = offence1;
	}
	public String getOffence2() {
		return offence2;
	}
	public void setOffence2(String offence2) {
		this.offence2 = offence2;
	}
	public String getOffence3() {
		return offence3;
	}
	public void setOffence3(String offence3) {
		this.offence3 = offence3;
	}
	public String getCO() {
		return CO;
	}
	public void setCO(String cO) {
		CO = cO;
	}
	public String getHSU() {
		return HSU;
	}
	public void setHSU(String hSU) {
		HSU = hSU;
	}
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	public String getOverallObtainedMark() {
		if(null==(overallObtainedMark))
			overallObtainedMark="";
		return overallObtainedMark;
	}
	public void setOverallObtainedMark(String overallObtainedMark) {
		this.overallObtainedMark = overallObtainedMark;
	}
	public String getLearnerLicenseId() {
		return learnerLicenseId;
	}
	public void setLearnerLicenseId(String learnerLicenseId) {
		this.learnerLicenseId = learnerLicenseId;
	}
	public String getLearnerId() {
		return learnerId;
	}
	public void setLearnerId(String learnerId) {
		this.learnerId = learnerId;
	}
	public String getTransfereeName() {
		return transfereeName;
	}
	public void setTransfereeName(String transfereeName) {
		this.transfereeName = transfereeName;
	}
	public String getTransferorName() {
		return transferorName;
	}
	public void setTransferorName(String transferorName) {
		this.transferorName = transferorName;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getPenaltyId() {
		return penaltyId;
	}
	public void setPenaltyId(String penaltyId) {
		this.penaltyId = penaltyId;
	}
	public String getPenaltyPerDay() {
		return penaltyPerDay;
	}
	public void setPenaltyPerDay(String penaltyPerDay) {
		this.penaltyPerDay = penaltyPerDay;
	}
	public String getMaxPenalty() {
		return maxPenalty;
	}
	public void setMaxPenalty(String maxPenalty) {
		this.maxPenalty = maxPenalty;
	}
	public String getLicenseCardCost() {
		return licenseCardCost;
	}
	public void setLicenseCardCost(String licenseCardCost) {
		this.licenseCardCost = licenseCardCost;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getLicenseConstantId() {
		return licenseConstantId;
	}
	public void setLicenseConstantId(String licenseConstantId) {
		this.licenseConstantId = licenseConstantId;
	}
	public String getVehicleConstantId() {
		return vehicleConstantId;
	}
	public void setVehicleConstantId(String vehicleConstantId) {
		this.vehicleConstantId = vehicleConstantId;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getRcCost() {
		return rcCost;
	}
	public void setRcCost(String rcCost) {
		this.rcCost = rcCost;
	}
	public String getFitnessAmount() {
		return fitnessAmount;
	}
	public void setFitnessAmount(String fitnessAmount) {
		this.fitnessAmount = fitnessAmount;
	}
	public String getOwnershipTransferFees() {
		return ownershipTransferFees;
	}
	public void setOwnershipTransferFees(String ownershipTransferFees) {
		this.ownershipTransferFees = ownershipTransferFees;
	}
	public String getRegionalTransferFee() {
		return regionalTransferFee;
	}
	public void setRegionalTransferFee(String regionalTransferFee) {
		this.regionalTransferFee = regionalTransferFee;
	}
	public String getConversionFee() {
		return conversionFee;
	}
	public void setConversionFee(String conversionFee) {
		this.conversionFee = conversionFee;
	}
	public String getTransferTax() {
		return transferTax;
	}
	public void setTransferTax(String transferTax) {
		this.transferTax = transferTax;
	}
	public String getVehicleTypeName() {
		return vehicleTypeName;
	}
	public void setVehicleTypeName(String vehicleTypeName) {
		this.vehicleTypeName = vehicleTypeName;
	}
	public String getvPrefix() {
		return vPrefix;
	}
	public void setvPrefix(String vPrefix) {
		this.vPrefix = vPrefix;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getVehicleHorsePower() {
		return vehicleHorsePower;
	}
	public void setVehicleHorsePower(String vehicleHorsePower) {
		this.vehicleHorsePower = vehicleHorsePower;
	}
	public String getVehicleKiloWatt() {
		return vehicleKiloWatt;
	}
	public void setVehicleKiloWatt(String vehicleKiloWatt) {
		this.vehicleKiloWatt = vehicleKiloWatt;
	}
	
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	public String getDuplicateIssueDate() {
		return duplicateIssueDate;
	}
	public void setDuplicateIssueDate(String duplicateIssueDate) {
		this.duplicateIssueDate = duplicateIssueDate;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCaseNo() {
		return caseNo;
	}
	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}
	public String getPoliceStation() {
		return policeStation;
	}
	public void setPoliceStation(String policeStation) {
		this.policeStation = policeStation;
	}
	public String getOccurrenceDate() {
		return occurrenceDate;
	}
	public void setOccurrenceDate(String occurrenceDate) {
		this.occurrenceDate = occurrenceDate;
	}
	public String getOccurrenceTime() {
		return occurrenceTime;
	}
	public void setOccurrenceTime(String occurrenceTime) {
		this.occurrenceTime = occurrenceTime;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}
	public String getOccurencePlace() {
		return occurencePlace;
	}
	public void setOccurencePlace(String occurencePlace) {
		this.occurencePlace = occurencePlace;
	}
	public Integer getCommercialApplicationAge() {
		return commercialApplicationAge;
	}
	public void setCommercialApplicationAge(Integer commercialApplicationAge) {
		this.commercialApplicationAge = commercialApplicationAge;
	}
	public Integer getLicenseApplicationAge() {
		return licenseApplicationAge;
	}
	public void setLicenseApplicationAge(Integer licenseApplicationAge) {
		this.licenseApplicationAge = licenseApplicationAge;
	}
	public Integer getLearnerapplicationage() {
		return learnerapplicationage;
	}
	public void setLearnerapplicationage(Integer learnerapplicationage) {
		this.learnerapplicationage = learnerapplicationage;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPersonalOwnerName() {
		return personalOwnerName;
	}
	public void setPersonalOwnerName(String personalOwnerName) {
		this.personalOwnerName = personalOwnerName;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	
	public String getApplicantNo() {
		return applicantNo;
	}
	public void setApplicantNo(String applicantNo) {
		this.applicantNo = applicantNo;
	}
	public String getTestDate() {
		return testDate;
	}
	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}
	public String getManufactureYear() {
		return manufactureYear;
	}
	public void setManufactureYear(String manufactureYear) {
		this.manufactureYear = manufactureYear;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getBuyerAddress() {
		return buyerAddress;
	}
	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getVehicleRegistrationType() {
		return vehicleRegistrationType;
	}
	public void setVehicleRegistrationType(String vehicleRegistrationType) {
		this.vehicleRegistrationType = vehicleRegistrationType;
	}
	public String getSelectall() {
		return selectall;
	}
	public void setSelectall(String selectall) {
		this.selectall = selectall;
	}
	public String getLatestVehicleNumber() {
		return latestVehicleNumber;
	}
	public void setLatestVehicleNumber(String latestVehicleNumber) {
		this.latestVehicleNumber = latestVehicleNumber;
	}
	public String getNewVehicleNo() {
		return newVehicleNo;
	}
	public void setNewVehicleNo(String newVehicleNo) {
		this.newVehicleNo = newVehicleNo;
	}
	public String getTransferorCustomerId() {
		return transferorCustomerId;
	}
	public void setTransferorCustomerId(String transferorCustomerId) {
		this.transferorCustomerId = transferorCustomerId;
	}
	public String getTransfereeCustomerId() {
		return transfereeCustomerId;
	}
	public void setTransfereeCustomerId(String transfereeCustomerId) {
		this.transfereeCustomerId = transfereeCustomerId;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getOrganisationInfoId() {
		return organisationInfoId;
	}
	public void setOrganisationInfoId(String organisationInfoId) {
		this.organisationInfoId = organisationInfoId;
	}
	public String getManualFlag() {
		return manualFlag;
	}
	public void setManualFlag(String manualFlag) {
		this.manualFlag = manualFlag;
	}
	public String getPresentDzongkhag() {
		return presentDzongkhag;
	}
	public void setPresentDzongkhag(String presentDzongkhag) {
		this.presentDzongkhag = presentDzongkhag;
	}

	public String getIssurance() {
		return issurance;
	}
	public void setIssurance(String issurance) {
		this.issurance = issurance;
	}
	public String getNumberOfDeath() {
		return numberOfDeath;
	}
	public void setNumberOfDeath(String numberOfDeath) {
		this.numberOfDeath = numberOfDeath;
	}
	public String getNumberofInjured() {
		return numberofInjured;
	}
	public void setNumberofInjured(String numberofInjured) {
		this.numberofInjured = numberofInjured;
	}
	public String getNumberofunInjured() {
		return numberofunInjured;
	}
	public void setNumberofunInjured(String numberofunInjured) {
		this.numberofunInjured = numberofunInjured;
	}
	public String getRegistrationcertificate() {
		return registrationcertificate;
	}
	public void setRegistrationcertificate(String registrationcertificate) {
		this.registrationcertificate = registrationcertificate;
	}
	
	public String getCertificateOfroadworthiness() {
		return certificateOfroadworthiness;
	}
	public void setCertificateOfroadworthiness(String certificateOfroadworthiness) {
		this.certificateOfroadworthiness = certificateOfroadworthiness;
	}
	public String getEmmissioncertificate() {
		return emmissioncertificate;
	}
	public void setEmmissioncertificate(String emmissioncertificate) {
		this.emmissioncertificate = emmissioncertificate;
	}
	public String getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(String licensetype) {
		this.licensetype = licensetype;
	}

	public String getRenewalHistoryId() {
		return renewalHistoryId;
	}
	public void setRenewalHistoryId(String renewalHistoryId) {
		this.renewalHistoryId = renewalHistoryId;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getRenewalDate() {
		return renewalDate;
	}
	public void setRenewalDate(String renewalDate) {
		this.renewalDate = renewalDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getLoadCapacity() {
		return loadCapacity;
	}
	public void setLoadCapacity(String loadCapacity) {
		this.loadCapacity = loadCapacity;
	}
	public String getEngineType() {
		return engineType;
	}
	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getDrivinglicenseId() {
		return drivinglicenseId;
	}
	public void setDrivinglicenseId(String drivinglicenseId) {
		this.drivinglicenseId = drivinglicenseId;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getPermitIDNo() {
		return permitIDNo;
	}
	public void setPermitIDNo(String permitIDNo) {
		this.permitIDNo = permitIDNo;
	}
	public String getLearnerlicenseInfoId() {
		return learnerlicenseInfoId;
	}
	public void setLearnerlicenseInfoId(String learnerlicenseInfoId) {
		this.learnerlicenseInfoId = learnerlicenseInfoId;
	}
	public String getPermitDetailsId() {
		return permitDetailsId;
	}
	public void setPermitDetailsId(String permitDetailsId) {
		this.permitDetailsId = permitDetailsId;
	}
	
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getBloodGroupId() {
		return bloodGroupId;
	}
	public void setBloodGroupId(String bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}
	public String getDrivetype() {
		return drivetype;
	}
	public void setDrivetype(String drivetype) {
		this.drivetype = drivetype;
	}
	
	public String getRenewalInfoId() {
		return renewalInfoId;
	}
	public void setRenewalInfoId(String renewalInfoId) {
		this.renewalInfoId = renewalInfoId;
	}
	public String getOwnerType() {
		return OwnerType;
	}
	public void setOwnerType(String ownerType) {
		OwnerType = ownerType;
	}
	public String getOwnerName() {
		return OwnerName;
	}
	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}
	public String getEngineNumber() {
		return EngineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		EngineNumber = engineNumber;
	}
	public String getChasisNumber() {
		return ChasisNumber;
	}
	public void setChasisNumber(String chasisNumber) {
		ChasisNumber = chasisNumber;
	}
	public String getOrganizationInfoId() {
		return organizationInfoId;
	}
	public void setOrganizationInfoId(String organizationInfoId) {
		this.organizationInfoId = organizationInfoId;
	}
	public String getPersonalInfoId() {
		return personalInfoId;
	}
	public void setPersonalInfoId(String personalInfoId) {
		this.personalInfoId = personalInfoId;
	}

	public String getSearchpermitID() {
		return searchpermitID;
	}
	public void setSearchpermitID(String searchpermitID) {
		this.searchpermitID = searchpermitID;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	

	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getPermitID() {
		return permitID;
	}
	public void setPermitID(String permitID) {
		this.permitID = permitID;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public String getBaseoffice() {
		return baseoffice;
	}
	public void setBaseoffice(String baseoffice) {
		this.baseoffice = baseoffice;
	}
	public String getRouteBetween() {
		return routeBetween;
	}
	public void setRouteBetween(String routeBetween) {
		this.routeBetween = routeBetween;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getCarryingCapacity() {
		return carryingCapacity;
	}
	public void setCarryingCapacity(String carryingCapacity) {
		this.carryingCapacity = carryingCapacity;
	}
	public String getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(String seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getDateOfissue() {
		return dateOfissue;
	}
	public void setDateOfissue(String dateOfissue) {
		this.dateOfissue = dateOfissue;
	}
	public String getValidUpto() {
		return validUpto;
	}
	public void setValidUpto(String validUpto) {
		this.validUpto = validUpto;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getConductorName() {
		return conductorName;
	}
	public void setConductorName(String conductorName) {
		this.conductorName = conductorName;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getLLNo() {
		return LLNo;
	}
	public void setLLNo(String lLNo) {
		LLNo = lLNo;
	}
	public String getAccidentDate() {
		return accidentDate;
	}
	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}
	public String getAccidentCause() {
		return accidentCause;
	}
	public void setAccidentCause(String accidentCause) {
		this.accidentCause = accidentCause;
	}
	public String getAccidentSite() {
		return accidentSite;
	}
	public void setAccidentSite(String accidentSite) {
		this.accidentSite = accidentSite;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getTitleOfcourtesy() {
		return titleOfcourtesy;
	}
	public void setTitleOfcourtesy(String titleOfcourtesy) {
		this.titleOfcourtesy = titleOfcourtesy;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCID() {
		return CID;
	}
	public void setCID(String cID) {
		CID = cID;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIdentificationMarks() {
		return identificationMarks;
	}
	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getNational() {
		return national;
	}
	public void setNational(String national) {
		this.national = national;
	}
	
	public String getDzongkhag() {
		return dzongkhag;
	}
	public void setDzongkhag(String dzongkhag) {
		this.dzongkhag = dzongkhag;
	}
	public String getGewog() {
		return gewog;
	}
	public void setGewog(String gewog) {
		this.gewog = gewog;
	}
	public String getVillage() {
		if(null==(village))
			village="";
		
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public FormFile getUpload() {
		return upload;
	}
	public void setUpload(FormFile upload) {
		this.upload = upload;
	}
	//TOOLS GETTER AND SETTER
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
		
	}