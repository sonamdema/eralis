package bt.gov.rsta.eralis.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TaskListServiceDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String serviceCode = null;
	private String serviceName = null;
	private List<StatusDTO> statusList = new ArrayList<StatusDTO>();
	
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public List<StatusDTO> getStatusList() {
		return statusList;
	}
	public void setStatusList(List<StatusDTO> statusList) {
		this.statusList = statusList;
	}
}
