package bt.gov.rsta.eralis.dto.license;

import java.io.Serializable;

public class OffenceDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String offenceId;
	
	public String getOffenceId() {
		return offenceId;
	}
	public void setOffenceId(String offenceId) {
		this.offenceId = offenceId;
	}
}
