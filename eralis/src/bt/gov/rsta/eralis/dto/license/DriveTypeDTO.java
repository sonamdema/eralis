package bt.gov.rsta.eralis.dto.license;

import java.io.Serializable;

public class DriveTypeDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String driveTypeId;
	private String driveType;
	private String isChecked;
	public String getDriveTypeId() {
		return driveTypeId;
	}
	public void setDriveTypeId(String driveTypeId) {
		this.driveTypeId = driveTypeId;
	}
	public String getDriveType() {
		return driveType;
	}
	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}
	public String getIsChecked() {
		return isChecked;
	}
	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}
	
}
