package bt.gov.rsta.eralis.dto.etest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

public class ETestDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String testDuration;
	private String testDate;
	private String maxApplicants;
	private String testLocation;
	private String testLocationId;
	private String isActive;
	private String slNo;
	private String addTestDate;
	private String question;
	private String[] options;
	private String isAnswer;
	private List<OptionsDTO> optionsList;
	private String questionId;
	private String lastQuestionId;
	private String name;
	private String learnerNo;
	private String questionsAttempted;
	private String marksObtained;
	private String testStatus;
	private String maxQuestions;
	private ArrayList<Integer> questionIdList;
	
	private String theoryPassMarks;
	private String theoryFullMarks;
	private String practicalPassMarks;
	private String practicalFullMarks;
	private String overAllPassPercent;
	
	private String practicalMarksObtained;
	private String overAllMarksObtained;
	private String appNo;
	private String practicalTestStatus;
	private String isViva;
	
	private String phoneNo;
	private String emailId;
	private String contactAddres;
	private String testType;
	private String driveType;
	private String theoryMarksObtained;
	private String instituteName;
	private String CID;
	private String issueType;
	private String theoryTestStatus;
	private FormFile upload;
	private String path;
	private String rstaMaxApplicant;
	private String message;
	
	public FormFile getUpload() {
		return upload;
	}
	public void setUpload(FormFile upload) {
		this.upload = upload;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getRstaMaxApplicant() {
		return rstaMaxApplicant;
	}
	public void setRstaMaxApplicant(String rstaMaxApplicant) {
		this.rstaMaxApplicant = rstaMaxApplicant;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTheoryTestStatus() {
		return theoryTestStatus;
	}
	public void setTheoryTestStatus(String theoryTestStatus) {
		this.theoryTestStatus = theoryTestStatus;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public String getCID() {
		return CID;
	}
	public void setCID(String cID) {
		CID = cID;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getTheoryMarksObtained() {
		return theoryMarksObtained;
	}
	public void setTheoryMarksObtained(String theoryMarksObtained) {
		this.theoryMarksObtained = theoryMarksObtained;
	}
	public String getDriveType() {
		return driveType;
	}
	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContactAddres() {
		return contactAddres;
	}
	public void setContactAddres(String contactAddres) {
		this.contactAddres = contactAddres;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	public String getIsViva() {
		return isViva;
	}
	public void setIsViva(String isViva) {
		this.isViva = isViva;
	}
	public String getPracticalMarksObtained() {
		return practicalMarksObtained;
	}
	public void setPracticalMarksObtained(String practicalMarksObtained) {
		this.practicalMarksObtained = practicalMarksObtained;
	}
	public String getOverAllMarksObtained() {
		return overAllMarksObtained;
	}
	public void setOverAllMarksObtained(String overAllMarksObtained) {
		this.overAllMarksObtained = overAllMarksObtained;
	}
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getPracticalTestStatus() {
		return practicalTestStatus;
	}
	public void setPracticalTestStatus(String practicalTestStatus) {
		this.practicalTestStatus = practicalTestStatus;
	}
	public String getTheoryPassMarks() {
		return theoryPassMarks;
	}
	public void setTheoryPassMarks(String theoryPassMarks) {
		this.theoryPassMarks = theoryPassMarks;
	}
	public String getPracticalPassMarks() {
		return practicalPassMarks;
	}
	public void setPracticalPassMarks(String practicalPassMarks) {
		this.practicalPassMarks = practicalPassMarks;
	}
	public String getPracticalFullMarks() {
		return practicalFullMarks;
	}
	public void setPracticalFullMarks(String practicalFullMarks) {
		this.practicalFullMarks = practicalFullMarks;
	}
	public String getOverAllPassPercent() {
		return overAllPassPercent;
	}
	public void setOverAllPassPercent(String overAllPassPercent) {
		this.overAllPassPercent = overAllPassPercent;
	}
	public String getTheoryFullMarks() {
		return theoryFullMarks;
	}
	public void setTheoryFullMarks(String theoryFullMarks) {
		this.theoryFullMarks = theoryFullMarks;
	}
	public ArrayList<Integer> getQuestionIdList() {
		return questionIdList;
	}
	public void setQuestionIdList(ArrayList<Integer> questionIdList) {
		this.questionIdList = questionIdList;
	}
	public String getMaxQuestions() {
		return maxQuestions;
	}
	public void setMaxQuestions(String maxQuestions) {
		this.maxQuestions = maxQuestions;
	}
	public String getTestStatus() {
		return testStatus;
	}
	public void setTestStatus(String testStatus) {
		this.testStatus = testStatus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLearnerNo() {
		return learnerNo;
	}
	public void setLearnerNo(String learnerNo) {
		this.learnerNo = learnerNo;
	}
	public String getQuestionsAttempted() {
		return questionsAttempted;
	}
	public void setQuestionsAttempted(String questionsAttempted) {
		this.questionsAttempted = questionsAttempted;
	}
	public String getMarksObtained() {
		return marksObtained;
	}
	public void setMarksObtained(String marksObtained) {
		this.marksObtained = marksObtained;
	}
	public String getLastQuestionId() {
		return lastQuestionId;
	}
	public void setLastQuestionId(String lastQuestionId) {
		this.lastQuestionId = lastQuestionId;
	}
	public String getTestDuration() {
		return testDuration;
	}
	public void setTestDuration(String testDuration) {
		this.testDuration = testDuration;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public List<OptionsDTO> getOptionsList() {
		return optionsList;
	}
	public void setOptionsList(List<OptionsDTO> optionsList) {
		this.optionsList = optionsList;
	}
	public String[] getOptions() {
		return options;
	}
	public void setOptions(String[] options) {
		this.options = options;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getIsAnswer() {
		return isAnswer;
	}
	public void setIsAnswer(String isAnswer) {
		this.isAnswer = isAnswer;
	}
	public String getAddTestDate() {
		return addTestDate;
	}
	public void setAddTestDate(String addTestDate) {
		this.addTestDate = addTestDate;
	}
	
	public String getSlNo() {
		return slNo;
	}
	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}
	public String getTestDate() {
		return testDate;
	}
	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}
	public String getMaxApplicants() {
		return maxApplicants;
	}
	public void setMaxApplicants(String maxApplicants) {
		this.maxApplicants = maxApplicants;
	}
	public String getTestLocation() {
		return testLocation;
	}
	public void setTestLocation(String testLocation) {
		this.testLocation = testLocation;
	}
	public String getTestLocationId() {
		return testLocationId;
	}
	public void setTestLocationId(String testLocationId) {
		this.testLocationId = testLocationId;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
}
