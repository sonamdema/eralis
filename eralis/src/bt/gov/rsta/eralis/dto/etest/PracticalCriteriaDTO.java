package bt.gov.rsta.eralis.dto.etest;

import java.io.Serializable;

public class PracticalCriteriaDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String criteriaId;
	private String criteriaName;
	private String criteriaPoint;
	
	public String getCriteriaId() {
		return criteriaId;
	}
	public void setCriteriaId(String criteriaId) {
		this.criteriaId = criteriaId;
	}
	public String getCriteriaName() {
		return criteriaName;
	}
	public void setCriteriaName(String criteriaName) {
		this.criteriaName = criteriaName;
	}
	public String getCriteriaPoint() {
		return criteriaPoint;
	}
	public void setCriteriaPoint(String criteriaPoint) {
		this.criteriaPoint = criteriaPoint;
	}
}
