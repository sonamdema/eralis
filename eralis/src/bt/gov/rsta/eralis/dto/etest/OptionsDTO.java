package bt.gov.rsta.eralis.dto.etest;

import java.io.Serializable;

public class OptionsDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String optionId;
	private String optionName;
	
	public String getOptionId() {
		return optionId;
	}
	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
}
