package bt.gov.rsta.eralis.dto.payment;

import java.io.Serializable;

public class PaymentDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String amount;
	private String penalty;
	private String totalAmount;
	private String status;
	private String penaltyFlag;
	private String requestType;
	private String serviceType;
	private String identityNo;
	private String identityTypeId;
	private String loadingCapacity;
	private String seatingCapacity;
	private String vehicleHP;
	private String vehicleKilowatt;
	private String vehicleEngineCC;
	private String purchaseDate;
	private String saleDeedAmount;
	private String saleDeedDate;
	private String renewalDuration;
	private String registrationCode;
	
	private String applicationNo;
	private String applicationType;
	private String serviceId;
	private String createdBy;
	private String receiptNo;
	private String receiptDate;
	
	private String marketValue;
	private String valueForTT;
	private String taxedAmount;
	private String transferFees;
	private String initialPurchasePrice;
	private String registrationDate;
	private String serviceFee;
	private String AccountHeadId;
	private double cardCost;
	private double ODLRenewalCost;
	private double PDRenewalCost;
	
	public double getPDRenewalCost() {
		return PDRenewalCost;
	}
	public void setPDRenewalCost(double pDRenewalCost) {
		PDRenewalCost = pDRenewalCost;
	}
	public double getODLRenewalCost() {
		return ODLRenewalCost;
	}
	public void setODLRenewalCost(double oDLRenewalCost) {
		ODLRenewalCost = oDLRenewalCost;
	}
	public double getCardCost() {
		return cardCost;
	}
	public void setCardCost(double cardCost) {
		this.cardCost = cardCost;
	}
	public String getServiceFee() {
		return serviceFee;
	}
	public void setServiceFee(String serviceFee) {
		this.serviceFee = serviceFee;
	}
	public String getAccountHeadId() {
		return AccountHeadId;
	}
	public void setAccountHeadId(String accountHeadId) {
		AccountHeadId = accountHeadId;
	}
	public String getInitialPurchasePrice() {
		return initialPurchasePrice;
	}
	public void setInitialPurchasePrice(String initialPurchasePrice) {
		this.initialPurchasePrice = initialPurchasePrice;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getRegistrationCode() {
		return registrationCode;
	}
	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}
	public String getTransferFees() {
		return transferFees;
	}
	public void setTransferFees(String transferFees) {
		this.transferFees = transferFees;
	}
	public String getMarketValue() {
		return marketValue;
	}
	public void setMarketValue(String marketValue) {
		this.marketValue = marketValue;
	}
	public String getValueForTT() {
		return valueForTT;
	}
	public void setValueForTT(String valueForTT) {
		this.valueForTT = valueForTT;
	}
	public String getTaxedAmount() {
		return taxedAmount;
	}
	public void setTaxedAmount(String taxedAmount) {
		this.taxedAmount = taxedAmount;
	}
	public String getRenewalDuration() {
		return renewalDuration;
	}
	public void setRenewalDuration(String renewalDuration) {
		this.renewalDuration = renewalDuration;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getSaleDeedAmount() {
		return saleDeedAmount;
	}
	public void setSaleDeedAmount(String saleDeedAmount) {
		this.saleDeedAmount = saleDeedAmount;
	}
	public String getSaleDeedDate() {
		return saleDeedDate;
	}
	public void setSaleDeedDate(String saleDeedDate) {
		this.saleDeedDate = saleDeedDate;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getPenaltyFlag() {
		return penaltyFlag;
	}
	public void setPenaltyFlag(String penaltyFlag) {
		this.penaltyFlag = penaltyFlag;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getIdentityTypeId() {
		return identityTypeId;
	}
	public void setIdentityTypeId(String identityTypeId) {
		this.identityTypeId = identityTypeId;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getLoadingCapacity() {
		return loadingCapacity;
	}
	public void setLoadingCapacity(String loadingCapacity) {
		this.loadingCapacity = loadingCapacity;
	}
	public String getSeatingCapacity() {
		return seatingCapacity;
	}
	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}
	public String getVehicleHP() {
		return vehicleHP;
	}
	public void setVehicleHP(String vehicleHP) {
		this.vehicleHP = vehicleHP;
	}
	public String getVehicleKilowatt() {
		return vehicleKilowatt;
	}
	public void setVehicleKilowatt(String vehicleKilowatt) {
		this.vehicleKilowatt = vehicleKilowatt;
	}
	public String getVehicleEngineCC() {
		return vehicleEngineCC;
	}
	public void setVehicleEngineCC(String vehicleEngineCC) {
		this.vehicleEngineCC = vehicleEngineCC;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
}
