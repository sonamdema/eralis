package bt.gov.rsta.eralis.dto.common;

import java.io.Serializable;

public class DailyTaskUserDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String employeeName;
	private String userId;
	private String regionName;
	private String jurisId;
	private String jurisTypeId;
	
	public String getJurisId() {
		return jurisId;
	}
	public void setJurisId(String jurisId) {
		this.jurisId = jurisId;
	}
	public String getJurisTypeId() {
		return jurisTypeId;
	}
	public void setJurisTypeId(String jurisTypeId) {
		this.jurisTypeId = jurisTypeId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
}
