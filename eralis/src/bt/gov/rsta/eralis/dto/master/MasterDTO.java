package bt.gov.rsta.eralis.dto.master;

import java.io.Serializable;

public class MasterDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String desc;
	private String number;
	private String otherId;
	private String otherName;
	private String engineCC;
	private String seatCapacity;
	private String unladenWeight;
	
	public String getUnladenWeight() {
		return unladenWeight;
	}
	public void setUnladenWeight(String unladenWeight) {
		this.unladenWeight = unladenWeight;
	}
	public String getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(String engineCC) {
		this.engineCC = engineCC;
	}
	public String getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(String seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getOtherId() {
		return otherId;
	}
	public void setOtherId(String otherId) {
		this.otherId = otherId;
	}
	public String getOtherName() {
		return otherName;
	}
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}
}
