package bt.gov.rsta.eralis.dto.faq;

import java.io.Serializable;

public class FaqDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String faqTitle;
	private String faqDesc;
	private String faqType;
	
	public String getFaqTitle() {
		return faqTitle;
	}
	public void setFaqTitle(String faqTitle) {
		this.faqTitle = faqTitle;
	}
	public String getFaqDesc() {
		return faqDesc;
	}
	public void setFaqDesc(String faqDesc) {
		this.faqDesc = faqDesc;
	}
	public String getFaqType() {
		return faqType;
	}
	public void setFaqType(String faqType) {
		this.faqType = faqType;
	}
}
