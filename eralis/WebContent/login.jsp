<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
    <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
	<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login - eRaLIS</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- favicon -->
		<link rel="shortcut icon" href="<%=request.getContextPath() %>/images/favicon.ico" type="image/x-icon" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/fonts/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-ie.min.css" />
		<![endif]-->

		<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="<%=request.getContextPath() %>/js/html5shiv.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/respond.min.js"></script>
		<![endif]-->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-0WQ7MH1BGV"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'G-0WQ7MH1BGV');
		</script>
		
</head>
<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<img src="<%=request.getContextPath() %>/images/eRaLIS-Logo.png" width="70px" height="80px"/>
									<span class="green"><b>e-RaLIS</b></span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; Road Safety and Transport Authority</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-lock green"></i>
												Please Enter Your Credentials
											</h4>
											<div class="space-6"></div>
											<form action="<%=request.getContextPath() %>/Login" method="POST" onsubmit="return validateForm()">
												<div class="alert alert-danger" id="errorMsg" style="display:none">
												</div>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Username" autocomplete="off" id="username" name="username"/>
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Password" id="password" name="password"/>
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="#" data-target="#forgot-box" class="forgot-password-link">
													<i class="ace-icon fa fa-arrow-left"></i>
													I forgot my password
												</a>
											</div>
											 <div>
												<a href="<%=request.getContextPath() %>/public/serviceredirection.html?q=etest-login" class="user-signup-link">
													e-Test Login
													<i class="ace-icon fa fa-arrow-right"></i>
												</a>
											</div>  

										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<div id="forgot-box" class="forgot-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Retrieve Password
											</h4>

											<div class="space-6"></div>
											<p>
												Fill up the form to reset your password
											</p>
											<form action="<%=request.getContextPath() %>/ResetPasswordServlet" method="POST" id="forgotPasswordForm">
												<div id="forgotPasswordMsg" style="display:none"></div>
												<fieldset>
													<label class="block clearfix">
														Username
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Username" id="loginId" name="loginId"/>
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>
													<label class="block clearfix">
														Security Question
														<span class="block input-icon input-icon-right">
															<select class="form-control" id="questionId" name="questionId">
																<option value="-1">--SELECT--</option>
																<logic:iterate id="security" name="SECURITY_QUESTION_LIST">
										     						<option value='<bean:write name="security" property="headerId"/>'><bean:write name="security" property="headerName"/></option>
										     					</logic:iterate>
															</select>
														</span>
													</label>
													<label class="block clearfix">
														Answer
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Answer" id="answer" name="answer"/>
															<i class="ace-icon fa fa-retweet"></i>
														</span>
													</label>
													<div class="clearfix">
														<button type="button" class="width-35 pull-right btn btn-sm btn-danger" onclick="validateForgotForm()">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Send Me!</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar center">
											<a href="#" data-target="#login-box" class="back-to-login-link">
												Back to login
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
			
			<div class="col-md-3 mag-inner-right" style="background: #F1EFEF;">
					<div class="connect">
					    <h4 class="side text-center" style="margin-bottom: 0px;margin-bottom: 6px;"><i class="fa fa-globe"></i> Online Services</h4>
					    <table id="conversion-history-table" class="table table-striped" style="background: white;">
							<tbody>
								<tr>
									<td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=view_application_status">View Your Application Status</a><br>
									</td></tr>
								<tr>
									<td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=acknowledgement_receipt">Re-print Acknowledgement Receipt</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=passenger_bus_route_permit">Passenger Bus Route Approval</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=change_personal_details">Change your contact address/mobile number</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=search_learner_dtls">Book a driving Test</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=payfine">Pay Fine(Offence-TIN)</a><br> 
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=vehicle_renewal_service">Vehicle Registration Certificate Renewal</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=vehicle_search_form&form_type=vehicle_duplication">Vehicle Registration Certificate Replacement</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=vehicle_search_form&form_type=vehicle_fitness">Vehicle Fitness Test</a><br>
										
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=get_license_information&form_type=renew_driving_license">Driving License Renewal</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=get_license_information&form_type=driving_license_duplication">Driving License Replacement </a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=renew_learner_search&form_type=renew_learner_search">Learner License Renewal</a><br>
									</td></tr><tr><td>
										<a href="<%=request.getContextPath()%>/public/serviceredirection.html?q=renew_learner_search&form_type=learner_license_duplicate">Learner License Replacement</a><br>
									</td>
								</tr>
							</tbody>
						</table>
                     		
					</div>
				<div>
					
			</div>
		
		<!-- /.main-content 		-->
		<!-- /.main-container -->
        </div>
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script src="<%=request.getContextPath() %>/js/jquery.1.11.1.min.js"></script>
		<![endif]-->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath() %>/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		
		<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>
		<script src="<%=request.getContextPath() %>/js/jqueryAjaxFormSubmit.js"></script>
		<script src="<%=request.getContextPath() %>/js/jquery.form.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
			
			
			$('document').ready(function()
			{
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
			});
			
			jQuery(function($) {
			 $('#btn-login').on('click', function(e) {
				$(location).attr('href', '<%=request.getContextPath()%>/pages/common/main.jsp');

				e.preventDefault();
			 });
			});

		</script>
		
		<script>

			<%
				String msg = null;
				if(request.getAttribute("message") != null)
					msg = (String)request.getAttribute("message");
			%>

			var msg = "<%=msg%>";
			$(document).ready(function()
			{
				if(msg == "FAILURE")
				{
			    	 $('#errorMsg').html("Invalid login credentials");
			    	 $('#errorMsg').show();
			    	 setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "LOGIN_NOT_ACTIVE")
				{
					$('#errorMsg').html("Account inactive, please contact the administrator");
			    	$('#errorMsg').show();
			    	setTimeout('hideStatus("errorMsg")',5000);
				}
			});

			function validateForm()
			{
				var username = $('#username').val();
				var password = $('#password').val();

				if(username == "")
				{
					$('#username').focus();
					return false;
				}
				if(password == "")
				{
					$('#password').focus();
					return false;
				}
				else
					return;
			}

			function validateForgotForm()
			{
				var userName = $('#loginId').val();
				var questionId = $('#questionId').val();
				var answer = $('#answer').val();
				
				if(userName == "")
				{
					$('#forgotPasswordMsg').html("<div class='alert alert-danger'>please enter your login id</div>");
					$('#forgotPasswordMsg').show();
					$('#loginId').focus();
					setTimeout('hideStatus("forgotPasswordMsg")', 4000);
					return false;
				}
				if(questionId == -1)
				{
					$('#forgotPasswordMsg').html("<div class='alert alert-danger'>please select the security question</div>");
					$('#forgotPasswordMsg').show();
					$('#questionId').focus();
					setTimeout('hideStatus("forgotPasswordMsg")', 4000);
					return false;
				}
				if(answer == "")
				{
					$('#forgotPasswordMsg').html("<div class='alert alert-danger'>please enter your answer for the question</div>");
					$('#forgotPasswordMsg').show();
					$('#answer').focus();
					setTimeout('hideStatus("forgotPasswordMsg")', 4000);
					return false;
				}
				else 
				{
					var options = {target:'#forgotPasswordMsg',url:'<%=request.getContextPath() %>/common.html?method=resetPassword',type:'POST',data: $("#forgotPasswordForm").serialize()}; 
					$("#forgotPasswordForm").ajaxSubmit(options);
				}
			}

		</script>
	</body>
</html>