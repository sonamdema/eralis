<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Permit Details
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Route Permit Details
		</small>
	</h1>
</div>
<!-- /.page-header -->
<div class="row">
	<div class="col-lg-12">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Search Details</h5>
			</div>
			 <div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label"> Vehicle No  :</label>
								</div>
									<div class="col-lg-3">
										<div class="input-group">
											<input type="text"  class="form-control" id="vehicleNo" readonly="readonly">
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('FORlicenseModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>
										</div>
									</div>
							  </div>		
						   </div>
					    </div>
				   </div>
			  </div>
		 </div>
	</div>
</div>		
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Driver Details</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group col-lg-12">
							<div class="col-lg-2">
								<label class="control-label"> License No  :</label>
							</div>
							<div class="col-lg-3" id="licenseNo"></div>
							<div class="col-lg-2">
								<label class="control-label"> Driver Name :</label>
							</div>
							<div class="col-lg-3" id="driverName"></div>
						</div>
						<div class="form-group col-lg-12">
							<div class="col-lg-2">
								<label class="control-label"> Address :</label>
							</div>
							<div class="col-lg-3" id="address1"></div>
							<div class="col-lg-2">
								<label class="control-label"> Phone No :</label>
							</div>
							<div class="col-lg-3" id="license_mobile"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Vehicle Details</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group col-lg-12">
							<div class="col-lg-2">
								<label class="control-label">Seat Capacity  :</label>
							</div>
							<div class="col-lg-3" id="seatCapacity"></div>
							<div class="col-lg-2">
								<label class="control-label">Vehicle Type  :</label>
							</div>
							<div class="col-lg-3" id="FVVehicleType" ></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	
	<div class="widget-box">
			<div class="widget-body">
				<div class="widget-main">
					<div id="accordion" class="accordion-style1 panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Route Permit Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse1">
								<div class="panel-body" id="route-permit_dtls">
									
								</div>
								<div id="routePermitDtls" class="panel-body"  ></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<div id="FORlicenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Name:</label>
								<span class="pull-right">
									<input type="text" id="namePersonalModalFOR" placeholder="Name"  style="width: 200px" />
								</span>
							</div>
							<div class="form-group">
								<label>Permit No:</label>
								<span class="pull-right">
									<input type="text" id="permitNoFOR" placeholder="PermitNo" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>License No:</label>
								<span class="pull-right">
									<input type="text" id="licenseNoModalFOR" placeholder="LicenseNo" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>vehicle Number:</label>
								<span class="pull-right">
									<span class="pull-right">
									<input type="text" id="VehicleNoModalFOR" placeholder="VehicleNo" style="width: 200px"  />
								</span>
								</span>
							</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchFORPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="permitDetailList">
			</div>
			
		</div>
	</div>
</div>
			



<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
		
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
			
	function searchCommonInfo()
	{
		var driverName = $('#namepermitModal').val();
		var permitNo = $('#permitNopermitModal').val();
		var regionId = $('#regionpermitModal').val();

		if(driverName == "")
			driverName = "NA";
		if(permitNo == "")
			permitNo = "NA";
		if(regionId == "")
			regionId = "NA";
		
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPermitInfoList&driverName="+driverName+"&permitNo="+permitNo+"&region="+regionId,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#permitListTable").html(responseText);
				$("#permitListTable").show();
			}
		});
	}

	function searchRenewalInfo()
	{ 
		var ownerType = $('#ownerTypeRenewalModal').val();
		var vehicleType = $('#vehicleTypeRenewalModal').val();
		var vehicleNumber = $('#vehicleNumberRenewalModal').val();
		var ownerName = $('#ownerNameRenewalModal').val();
		var engineNumber = $('#engineNumberRenewalModal').val();
		var chasisNumber = $('#chasisNumberRenewalModal').val();
		var cidNumber = $('#citizenIdRenewalModal').val();

		if(ownerType == "")
			ownerType = "NA";
		if(vehicleType == "")
			vehicleType = "NA";
		if(vehicleNumber == "")
			vehicleNumber = "NA";
		if(ownerName == "")
			ownerName = "NA";
		if(engineNumber == "")
			engineNumber = "NA";
		if(chasisNumber == "")
			chasisNumber = "NA";
		if(cidNumber == "")
			cidNumber = "NA";

		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&ownerType="+ownerType+"&vehicleType="+vehicleType+"&vehicleNumber="+vehicleNumber+"&ownerName="+ownerName+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#renewalListTable").html(responseText);
				$("#renewalListTable").show();
			}
		});
	}

	function searchVehicleNumber()
	{
		$('#licenseNo').val();
	}

	function searchPersonalInfo()
	{
		var name 		= $('#namePersonalModal').val();
		var cidNumber 	= $('#cidPersonalModal').val();
		var regionId 	= $('#regionPersonalModal').val();
		var licenseNo	= $('#licenseNoModal').val();
		var licenseType	= $('#licenseTypeModal').val();
		
		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(regionId == "")
			regionId = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		if(licenseType == "")
			licenseType = "NA";
	 
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&name="+name+"&cid="+cidNumber+"&region="+regionId+"&licenseNo="+licenseNo+"&licenseType="+licenseType+"&searchType=LICENSE_RENEWAL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#licenseDetailList").html(responseText);
				$("#licenseDetailList").show();
			}
		});
	} 

	function searchFORPersonalInfo()
	{
		var name = $('#namePersonalModalFOR').val();
		var permitNo 	= $('#permitNoFOR').val();
		var licenseNo	= $('#licenseNoModalFOR').val();
		var vehicleNo	= $('#VehicleNoModalFOR').val();
		
		if(name == "")
			name = "NA";
		if(permitNo == "")
			permitNo = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		if(vehicleNo == "")
			vehicleNo = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=searchVehicleRoutePermitDtls&name="+name+"&permitNo="+permitNo+"&licenseNo="+licenseNo+"&vehicleNo="+vehicleNo+"&searchType=FOREIGN_PERMIT_RENEWAL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#permitDetailList").html(responseText);
				$("#permitDetailList").show();
			}
		});
	} 

	function getRoutePermitDtls(vehicleNo)
	{
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getRoutePermitDtls&vehicleNo="+vehicleNo,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#routePermitDtls").html(responseText);
				$("#routePermitDtls").show();
			}
		});
		
	}
	
</script>
<style>
	#permitForm .error { color: red; }
</style>