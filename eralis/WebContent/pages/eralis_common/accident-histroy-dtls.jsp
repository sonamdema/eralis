<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%
	String userName = null;
	EralisUserRolePriviledge userRolePriv = null;
	Role[] roles = null;
	String roleCode = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		userName = userRolePriv.getUserName();
		roles = userRolePriv.getRoles();
		
		Role currentRole = userRolePriv.getCurrentRole();
		roleCode = currentRole.getRoleCode();
	}
%>
 
<div class="row" id="cancelledStatus" style="display:none;">
	<div class="widget-body">
		<div class="widget-main">
			<div class="col-lg-12">
				<div class="form-group">
					<div id="msgDIV" class="col-lg-12 alert alert-danger">
						<div id="cancelReason"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="Msg" class="col-lg-12"></div>

<div id="license_history">

<div class="row">
	<div class="col-lg-12">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Vehicle Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Vehicle Company</label>
								</div>
								<div class="col-lg-3" id="displayCompany">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Vehicle Model</label>
								</div>
								<div class="col-lg-3" id="displayModel">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Origin</label>
								</div>
								<div class="col-lg-3" id="displayOrigin">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Vehicle Type</label>
								</div>
								<div class="col-lg-3" id="displayVehicleType"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Owner Type</label>
								</div>
								<div class="col-lg-3" id="displayOwnerType">
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%
	if(!roleCode.equalsIgnoreCase("OTHERS"))
	{
%>
<div class="row">
	<div class="col-lg-12">
		<div id="accordion" class="accordion-style1 panel-group">
		
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							&nbsp;Accident Detail History
						</a>
					</h4>
				</div>
				<div class="panel-collapse collapse" id="collapse1">
					<div class="panel-body">
						<div id="accidentDetailHistory"></div>
					</div>
				</div>
			</div>
		 </div>
	  </div>
   </div>
</div>
<%
	}
%>
<script>

	function printReport()
	{
	 	var divToPrint=document.getElementById('license_history');
	  	var newWin=window.open('','Print-Window');
	  	newWin.document.open();
	  	newWin.document.write(' '+divToPrint.innerHTML+'');
	  	newWin.document.close();
	}

	$(document).ready(function() 
	{
	    $('#renewal-history-table').DataTable({
	         responsive: true
	    });

	    $('#duplication-history-table').DataTable({
	         responsive: true
	    });

	    $('#transfer-history-table').DataTable({
	         responsive: true
	    });

	    $('#fitness-history-table').DataTable({
	         responsive: true
	    });
	});

</script>