<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="organization-search-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Customer Id</th>
			<th>Name</th>
			<th>Region</th>
			<th>Dzongkhag</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="ORGANISATION_LIST">
			<logic:iterate id="organisation" name="ORGANISATION_LIST">
				<tr>
					<td>
						<!-- <input type="radio" name="organisationListRadio" id='<bean:write name="organisation" property="organizationInfoId"/>' onclick="addSelected(this.id)"/> -->
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="organisation" property="organizationInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
					</td>
					<td><bean:write name="organisation" property="customerID"/></td>
					<td><bean:write name="organisation" property="name"/></td>
					<td><bean:write name="organisation" property="region"/></td>
					<td><bean:write name="organisation" property="dzongkhag"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>
	$(document).ready(function() 
	{
   		$('#organization-search-table').DataTable({
            responsive: true
    	});
	});
		

	//var globalId = "";
	//function addSelected(id)
	//{
	//	globalId = id;
	//}

	function getSelected(globalId)
	{
		$("#editBtn").show();
		$("#submitBtn").hide();
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOrganisationDtls&organisationInfoId='+globalId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					
					var name = $(this).find('name').text();
					var address = $(this).find('address').text();
					var phone = $(this).find('phone').text();
					var email=$(this).find('email').text();
					var remarks=$(this).find('remarks').text();
					var customerId = $(this).find('customerId').text();
					var organisationInfoId = $(this).find('organisationInfoId').text();
					var organizationType = $(this).find('organization-type').text();
					var organizationTypeId = $(this).find('organization-type-id').text();
					var regionId = $(this).find('region-id').text();
					var dzongkhagId = $(this).find('dzongkhag-id').text();
					var ministryId = $(this).find('ministry-id').text();
					var departmentId = $(this).find('department-id').text();
					var privateId = $(this).find('private-id').text();
					var diplomatId = $(this).find('diplomat-id').text();

					
					$('#customerID').val(customerId);
					$('#typedropdown').val(organizationTypeId);
					if(organizationType == "GOVERNMENT")
					{
						$('#privateDIV').hide();
						$('#privateCompany').val("0");
						$('#nameDIV').hide();
						$('#name').val("");
						
						$('#ministryDIV').show();
						$('#departmentDIV').show();
						$('#regionDIV').show();

						$('#ministry').val(ministryId);
						populateDependentDropDown(ministryId, 'department', '', 'DEPARTMENT_LIST', 'N');
						$('#department').val(departmentId);
						$('#region').val(regionId);
					}
					else if(organizationType == "PRIVATE")
					{
						$('#ministryDIV').hide();
						$('#departmentDIV').hide();
						$('#regionDIV').hide();
						$('#ministry').val("0");
						$('#department').val("0");
						$('#region').val("0");
						$('#nameDIV').hide();
						$('#name').val("");
						
						$('#privateDIV').show();
						$('#privateCompany').val(privateId);
					}
					else if(organizationType == "DIPLOMATS")
					{
					   $('#ministry').attr('disabled', true);
					   $('#department').attr('disabled', true);
					   $('#nameDIV').hide();
					   $('#diplomatDiv').show();
					   $('#privateDIV').hide();
					   $('#regionDIV').hide();
					   $('#ministryDIV').hide();
					    $('#departmentDIV').hide();
						$('#diplomatId').val(diplomatId);
					}
					else
					{
						$('#ministryDIV').hide();
						$('#departmentDIV').hide();
						$('#regionDIV').hide();
						$('#ministry').val("0");
						$('#department').val("0");
						$('#region').val("0");
						$('#privateDIV').hide();
						$('#privateCompany').val("0");
						
						$('#nameDIV').show();
						$('#name').val(name);
					}

					$('#organisationInfoId').val(organisationInfoId);
					$('#address').val(address);
					$('#phone').val(phone);
					$('#email').val(email);
					$('#remarks').val(remarks);
					
					$('select').select2().trigger('change');
					populateDependentDropDown('', 'dzongkhag', '', 'DZONGKHAG_LIST_ALL', 'N');
					$("#dzongkhag").select2("val", dzongkhagId);
				});
			}
		});
	}

</script>