<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Permit Details
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Route Permit Details
		</small>
	</h1>
</div>
<!-- /.page-header -->
<div class="row">
	<div class="col-lg-12">
		<html:form styleClass="form-horizontal" action="/eralis_common.html?method=route_permit" styleId="permitForm">
		<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Search Details</h5>
			</div>
			 <div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label"> Permit No <span style="color: #ff0000">*</span> :</label>
								</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="permitNo" styleClass="form-control" styleId="permitNo" readonly="true"></html:text>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('FORlicenseModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>
										</div>
									</div>
							  </div>		
						   </div>
					    </div>
				   </div>
			  </div>
		 </div>
								</div>
<!--								<div id="BVLicenseNo" style="display: none;">-->
<!--									<div class="col-lg-3">-->
<!--										<div class="input-group">-->
<!--											<html:text property="permitNo" styleClass="form-control" styleId="permitNo" readonly="true"></html:text>-->
<!--											<span class="input-group-btn">-->
<!--												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('FORlicenseModal')">-->
<!--													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>-->
<!--												</button>-->
<!--											</span>-->
<!--										</div>-->
<!--									</div>-->
<!--								</div>-->
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driver Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group col-lg-12">
								<div class="col-lg-2">
									<label class="control-label"> License No <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="licenseNo" styleId="licenseNo" styleClass="form-control" readonly="true"></html:text>
								</div>
								<div class="col-lg-2">
									<label class="control-label"> Driver Name <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="driverName" styleId="driverName" styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
							<div class="form-group col-lg-12">
								<div class="col-lg-2">
									<label class="control-label"> Address <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="address" styleId="address1" styleClass="form-control" readonly="true"></html:text>
								</div>
								<div class="col-lg-2">
									<label class="control-label"> Phone No <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="phone" styleId="license_mobile" styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Vehicle Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group col-lg-12">
								<div class="col-lg-2">
									<label class="control-label"> Registration No <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="vehicleNo" styleId="registrationNo" styleClass="form-control" readonly="true" ></html:text>
								</div>
								<div class="col-lg-2">
									<label class="control-label">Vehicle Type <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3" id="FVVehicleType" >
									
									<%-- <html:select property="vehicleType" styleClass="form-control" styleId="vehicleTypeFV">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId" />
									</html:select> --%>
									
									<select class="form-control" id="vehicleTypeFV" readonly="readonly">
										<option>Vehicle Type</option>
										<logic:iterate id="listId" name="vehicleTypeList">
											<option value='<bean:write name="listId" property="headerId"/>'><bean:write name="listId" property="headerName"/></option>
										</logic:iterate>
									</select>
									
									
										
								</div>
							</div>
							<div class="form-group col-lg-12">
								<div style="display:none;">
									<div class="col-lg-2">
										<label class="control-label">Engine CC <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<html:text property="engineCC" styleId="engineCC" styleClass="form-control" value="0" readonly="true"></html:text>
									</div>
								</div>
								<div class="col-lg-2">
									<label class="control-label">Seat Capacity <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="seatCapacity" styleId="seatCapacity" styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Permit Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group col-lg-12">
								<div class="col-lg-3">
									<label class="control-label">Purpose of Journey<span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<select class="form-control" id="journeyPurpose" readonly="readonly">
										<option value="">Purpose of Journey</option>
										<option value="OFFICIAL">Official</option>
										<option value="TOURIST">Tourist</option>
										<option value="BUSINESS_TRIP">Business Trip</option>
									</select>
									 
								</div>
							</div>
							<div class="form-group col-lg-12">
								<div class="col-lg-3">
									<label class="control-label">Region <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<select class="form-control" id="region1" readonly="readonly" onchange="populateDependentDropDown(this.value, 'baseoffice', '', 'BASE_OFFICE_LIST', 'N')">
										<option>Region</option>
										<logic:iterate id="listId" name="regionList">
											<option value='<bean:write name="listId" property="headerId"/>'><bean:write name="listId" property="headerName"/></option>
										</logic:iterate>
									</select>
									
									<%-- <html:select property="region" styleClass="form-control" styleId="region1" onchange="populateDependentDropDown(this.value, 'baseoffice', '', 'BASE_OFFICE_LIST', 'N')">
										<html:option value="">--SELECT--</html:option>
								   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
									</html:select> --%>
								</div>
								
								<div class="col-lg-3">
									<label class="control-label"> Base Office <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
                                       <html:text property="baseoffice" styleId="baseoffice" styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
														
							<div class="form-group col-lg-12">
								<div class="col-lg-3">
									<label class="control-label">Route From <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									 <html:text property="routeBetween" styleId="routeBetween" styleClass="form-control" readonly="true"></html:text>
								</div>
								
								<div class="col-lg-3">
									<label class="control-label">Route To <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									 <html:text property="to" styleId="to" styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
							<div class="form-group col-lg-12">
							
								<div class="col-lg-3">
									<label class="control-label">Date Of Issue <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<input type="text" value="" class="form-control" id="dateOfissue" readonly="readonly">
                   						<%-- <html:text property="dateOfissue" styleClass="form-control" styleId="dateOfissue" readonly="true"></html:text> --%>
										<span class="input-group-addon" style="cursor:pointer;">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								<div class="col-lg-3 col-lg-12">
									<label class="control-label"> Valid Upto <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="validUpto" styleClass="form-control" styleId="validity" readonly="true"></html:text>																	
										<span class="input-group-addon" style="cursor:pointer;">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group col-lg-12">
								<div class="col-lg-3">
									<label class="control-label">Capacity/No. of Passengers<span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									  <html:text property="carryingCapacity" styleId="carryingCapacity" styleClass="form-control" readonly="true"></html:text>
								</div>
								<div class="col-lg-3">
									<label class="control-label">Remarks :</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks1" styleClass="form-control" readonly="true"></html:textarea>
								</div>
							</div>
						   </div>
					     </div>
				       </div>
			       </div>
		       </div>	
		       <div class="widget-box">
                  <div class="widget-header widget-header-small">
				  <h5 class="widget-title lighter">Permit Renewal Details</h5>
			     </div>
			  <div class="widget-body">
				<div class="widget-main">
					<div class="row">
						 <div class="form-group col-lg-12">
							<div class="col-lg-3">
								<label class="control-label">Region <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								<html:select property="region" styleClass="form-control" styleId="region1" onchange="populateDependentDropDown(this.value, 'baseofficeRenew', '', 'BASE_OFFICE_LIST', 'N')">
									<html:option value="">--SELECT--</html:option>
							   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
								</html:select>
							</div>
							
							<div class="col-lg-3">
								<label class="control-label"> Base Office <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								 <html:select property="baseoffice" styleClass="form-control" styleId="baseofficeRenew">
									<html:option value="">--SELECT--</html:option>
								</html:select>
							</div>
						</div>
													
						<div class="form-group col-lg-12">
							<div class="col-lg-3">
								<label class="control-label">Route From <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								 <html:text property="routeBetween" styleId="routeBetween" styleClass="form-control"></html:text>
							</div>
							
							<div class="col-lg-3">
								<label class="control-label">Route To <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								 <html:text property="to" styleId="to" styleClass="form-control"></html:text>
							</div>
						</div>
						<div class="form-group col-lg-12">
							<div class="col-lg-3">
								<label class="control-label">Capacity/No. of Passengers<span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								  <html:text property="carryingCapacity" styleId="carryingCapacity" styleClass="form-control"></html:text>
							</div>
							<div class="col-lg-3">
								<label class="control-label">Remarks :</label>
							</div>
							<div class="col-lg-3">
								<html:textarea property="remarks" styleId="remarks1" styleClass="form-control"></html:textarea>
							</div>
						</div>
						<div class="form-group col-lg-12">
							<div class="col-lg-3">
								<label class="control-label">Renewal Upto<span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								<div class="input-group">
                  						<html:text property="dateOfissue" styleClass="form-control date-picker" styleId="RenewalUpto" readonly="true"></html:text>
									<span class="input-group-addon" style="cursor:pointer;">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
							<div class="col-lg-3">
								<label class="control-label">Number of Days <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								<div class="input-group">
									 <html:text property="numberOfDays" styleId="numberOfDays" styleClass="form-control"></html:text>
								</div>
							</div>
						 </div>
		              </div>
		           </div>
		        </div>
		    </div>	
		    <div id="PERMITDIV"></div>	
		 <div class="pull-right">
			<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
		</div>
			
	</html:form>
	</div>
</div>

<div id="permitModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Driver Name:</label>
								<span class="pull-right">
									<input type="text" id="namepermitModal" placeholder="Name"  style="width: 200px" />
								</span>
							</div>
							<div class="form-group">
								<label>Permit No:</label>
								<span class="pull-right">
									<input type="text" id="permitNopermitModal" placeholder="permitNo" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>Region:</label>
								<span class="pull-right">
									<select id="regionpermitModal" style="width: 200px" onchange="populateDependentDropDown(this.value, 'dzongkhagPermitModal', '', 'DZONGKHAG_LIST', 'N')">
										<option value="">SELECT</option>
										<logic:iterate id="region" name="regionList">
											<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
										</logic:iterate>
								</select> 
								</span>
							</div>
							<div class="form-group">
								<label>Dzongkhag:</label>
								<span class="pull-right">
								
									 <select id="dzongkhagPermitModal" style="width: 200px">
                                  		<option value="">SELECT</option>
                                  		 <logic:iterate id="dzongkhagPermitModal" name="dzongkhagList">
								        <option value='<bean:write name="dzongkhagPermitModal" property="headerId"/>'><bean:write name="dzongkhagPermitModal" property="headerName"/></option>
							            </logic:iterate>
	                                </select>
								</span>
							</div>
				  	   </div>
				   </div>
			   </div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="permitListTable">
			</div>
		</div>
	</div>
</div>
<div id="BTlicenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Name:</label>
								<span class="pull-right">
									<input type="text" id="namePersonalModal" placeholder="Name"  style="width: 200px" />
								</span>
							</div>
							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>License No:</label>
								<span class="pull-right">
									<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>Region:</label>
								<span class="pull-right">
									<select id="regionPersonalModal" style="width: 200px" >
									<option value="">SELECT</option>
									<logic:iterate id="region" name="regionList">
										<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
									</logic:iterate>
								</select> 
								</span>
							</div>
							<div class="form-group">
								<label>License Type:</label>
								<span class="pull-right">
									 <select id="licenseTypeModal" style="width: 200px">
                               			<option value="">SELECT</option>
                               			<option value="C">commercial</option>
                               			<option value="N">non-commercial</option>
                               		</select>
								</span>
							</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>

<div id="FORlicenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Name:</label>
								<span class="pull-right">
									<input type="text" id="namePersonalModalFOR" placeholder="Name"  style="width: 200px" />
								</span>
							</div>
							<div class="form-group">
								<label>Permit No:</label>
								<span class="pull-right">
									<input type="text" id="permitNoFOR" placeholder="PermitNo" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>License No:</label>
								<span class="pull-right">
									<input type="text" id="licenseNoModalFOR" placeholder="LicenseNo" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>vehicle Number:</label>
								<span class="pull-right">
									<span class="pull-right">
									<input type="text" id="VehicleNoModalFOR" placeholder="VehicleNo" style="width: 200px"  />
								</span>
								</span>
							</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchFORPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="permitDetailList">
			</div>
			
		</div>
	</div>
</div>

<div id="renewal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Owner Type"> Owner Type : </label>
	
                            <div class="col-sm-4">
                                <select class="col-sm-12 modal_input" id="ownerTypeRenewalModal">
									<option value="">SELECT</option>
									<option value="P">Personal</option>
									<option value="O">Organization</option>
								</select> 
                             </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Type"> Vehicle Type : </label>
	                              <div class="col-lg-4">
			                        <select class="col-sm-12 modal_input" id="vehicleTypeRenewalModal">
											<option value="">SELECT</option>
											<logic:iterate id="vehicleType" name="vehicleTypeList">
												<option value='<bean:write name="vehicleType" property="headerId"/>'><bean:write name="vehicleType" property="headerName"/></option>
											</logic:iterate>
										</select> 
                                 </div> 
						 </div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Owner's Name"> Owner's Name : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="ownerNameRenewalModal" placeholder="Owner's Name"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
						   </div>
						</form>
					</div>
				</div>
			 </div>
				<div class="modal-footer">
					<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
						<i class="ace-icon fa fa-search"></i>
						Search
					</button>
	
					<button class="btn btn-sm btn-primary">
						<i class="ace-icon fa fa-check"></i>
						Reset
					</button>
					
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>
				  </div>
				 <div id="renewalListTable">
			   </div>
			 </div>
		  </div>
	   </div><!-- PAGE CONTENT ENDS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
		
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
			
	function searchCommonInfo()
	{
		var driverName = $('#namepermitModal').val();
		var permitNo = $('#permitNopermitModal').val();
		var regionId = $('#regionpermitModal').val();

		if(driverName == "")
			driverName = "NA";
		if(permitNo == "")
			permitNo = "NA";
		if(regionId == "")
			regionId = "NA";
		
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPermitInfoList&driverName="+driverName+"&permitNo="+permitNo+"&region="+regionId,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#permitListTable").html(responseText);
				$("#permitListTable").show();
			}
		});
	}

	function searchRenewalInfo()
	{ 
		var ownerType = $('#ownerTypeRenewalModal').val();
		var vehicleType = $('#vehicleTypeRenewalModal').val();
		var vehicleNumber = $('#vehicleNumberRenewalModal').val();
		var ownerName = $('#ownerNameRenewalModal').val();
		var engineNumber = $('#engineNumberRenewalModal').val();
		var chasisNumber = $('#chasisNumberRenewalModal').val();
		var cidNumber = $('#citizenIdRenewalModal').val();

		if(ownerType == "")
			ownerType = "NA";
		if(vehicleType == "")
			vehicleType = "NA";
		if(vehicleNumber == "")
			vehicleNumber = "NA";
		if(ownerName == "")
			ownerName = "NA";
		if(engineNumber == "")
			engineNumber = "NA";
		if(chasisNumber == "")
			chasisNumber = "NA";
		if(cidNumber == "")
			cidNumber = "NA";

		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&ownerType="+ownerType+"&vehicleType="+vehicleType+"&vehicleNumber="+vehicleNumber+"&ownerName="+ownerName+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#renewalListTable").html(responseText);
				$("#renewalListTable").show();
			}
		});
	}

	function searchVehicleNumber()
	{
		$('#licenseNo').val();
	}

	function searchPersonalInfo()
	{
		var name 		= $('#namePersonalModal').val();
		var cidNumber 	= $('#cidPersonalModal').val();
		var regionId 	= $('#regionPersonalModal').val();
		var licenseNo	= $('#licenseNoModal').val();
		var licenseType	= $('#licenseTypeModal').val();
		
		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(regionId == "")
			regionId = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		if(licenseType == "")
			licenseType = "NA";
	 
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&name="+name+"&cid="+cidNumber+"&region="+regionId+"&licenseNo="+licenseNo+"&licenseType="+licenseType+"&searchType=LICENSE_RENEWAL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#licenseDetailList").html(responseText);
				$("#licenseDetailList").show();
			}
		});
	} 

	function searchFORPersonalInfo()
	{
		var name = $('#namePersonalModalFOR').val();
		var permitNo 	= $('#permitNoFOR').val();
		var licenseNo	= $('#licenseNoModalFOR').val();
		var vehicleNo	= $('#VehicleNoModalFOR').val();
		
		if(name == "")
			name = "NA";
		if(permitNo == "")
			permitNo = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		if(vehicleNo == "")
			vehicleNo = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLicesneFORInfo&name="+name+"&permitNo="+permitNo+"&licenseNo="+licenseNo+"&vehicleNo="+vehicleNo+"&searchType=FOREIGN_PERMIT_RENEWAL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#permitDetailList").html(responseText);
				$("#permitDetailList").show();
			}
		});
	} 

	


	$(document).ready(function()
	{
		$('#FV').attr('checked', true);
		$('#FVRegNo').show();
		$('#FORLicenseNo').show();
		
		$("#permitForm").validate
		({
	        invalidHandler: function(form, validator) 
	    	{
	             var errors = validator.numberOfInvalids();
	             if (errors) 
		         {                    
	                 var firstInvalidElement = $(validator.errorList[0].element);
	                 $('html,body').scrollTop(firstInvalidElement.offset().top);
	                 firstInvalidElement.focus();
	             }
         	},
			rules:
			{	
				licenseNo:{
					required: {
						depends: function() {
							if($('#FV').is(':checked')){
								return true;
							}
							else {
								return false;
							}
						}
					}
				},
				licenseNO:{
					required: {
						depends: function() {
							if($('#BV').is(':checked')){
								return true;
							}
							else {
								return false;
							}
						}
					}
				},
				driverName:{
					required: true
				},
				address:{
					required: true
				},
				phone:{
					required: true,
					number:true,
					minlength:8,
					maxlength:10
				},
				registrationNo:{
					required: {
						depends: function() {
							if($('#FV').is(':checked')){
								return true;
							}
							else {
								return false;
							}
						}
					}
				},
				displayVehicleNumber:{
					required: {
						depends: function() {
							if($('#BV').is(':checked')){
								return true;
							}
							else {
								return false;
							}
						}
					}
				},
				vehicleTypeFV:{
					required: {
						depends: function() {
							if($('#FV').is(':checked')){
								return true;
							}
							else {
								return false;
							}
						}
					}
				},
				vehicleType:{
					required: {
						depends: function() {
							if($('#BV').is(':checked')){
								return true;
							}
							else {
								return false;
							}
						}
					}
				},
				engineCC:{
					required:true,
					number:true
				},
				seatCapacity:{
					required:true,
					number:true
				},
				region:{
					required:true
				},
				baseoffice:{
					required:true
				},
				routeBetween:{
					required:true
				},
				to:{
					required:true
				},
				carryingCapacity:{
					required:true
				},
				dateOfissue:{
					required:true
				},
				validity:{
					required:true
				},
				journeyPurpose:{
					required:true
				}
			},    
			messages:
			{
				licenseNo:{required: "Please enter license number"},
				licenseNO:{required: "Please enter license number"},
				driverName:{required: "Please enter driver name"},
				address:{required: "Please enter driver name"},
				phone:{required: "Please enter phone number"},
				registrationNo:{required: "Please enter vehicle number"},
				displayVehicleNumber:{required: "Please enter vehicle number"},
				vehicleTypeFV:{required: "Please select vehicle type"},
				vehicleType:{required: "Please enter vehicle type"},
				engineCC:{required: "Please enter engine cc"},
				seatCapacity:{required: "Please enter seating capacity"},
				region:{required: "Please select region"},
				baseoffice:{required: "Please select base office"},
				routeBetween:{required: "Please enter route from"},
				to:{required: "Please enter route to"},
				carryingCapacity:{required: "Please enter route carrying capacity"},
				dateOfissue:{required: "Please enter date of issue"},
				validity:{required: "Please enter valid upto date"},
				journeyPurpose:{required: "Please enter the Purpose of Journey"}
			}
		});

		$('#submitBtn').click(function()
		{
			if($('#permitForm').valid()) 
			{
				requestType = "COMMON";
				serviceType = "ROUTE_PERMIT";
				var identityNo = "";
				var identityTypeId = "";
				identityNo = "FV";
				identityTypeId = $('#vehicleTypeFV').val(); 
				var loadingCapacity = "";
				var seatingCapacity = "";
				var vehicleHP = "";
				var kilowatts = "";
				var engineCC = $('#engineCC').val();
				var purchaseDate = $('#validity').val();
				var saleDeedAmount = "";
				var saleDeedDate = $('#RenewalUpto').val();
				var renewalDuration = "";
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
			}
		});
		
	});

	function formSubmit()
	{
		
		var validity = $("#validity").val();
		var RenewalUpto = $("#RenewalUpto").val();
		var options = {target:'#PERMITDIV',url:'<%=request.getContextPath()%>/eralis_common.html?method=route_permit&RenewalUpto='+RenewalUpto+'&validity='+validity,type:'POST',data: $("#permitForm").serialize()}; 
	    $("#permitForm").ajaxSubmit(options);
        $('#PERMITDIV').show();
	}
	
</script>
<style>
	#permitForm .error { color: red; }
</style>