<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Vehicle history
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			vehicle history details
		</small>
	</h1>
</div><!-- /.page-header -->
<div style="display:none;"  id="editing_form">
<jsp:include page="/pages/common/edit_personal_dtls.jsp"></jsp:include>
<jsp:include page="/pages/common/edit_vehicle_details.jsp"></jsp:include>
</div>
<div class="row">
	<div class="col-lg-12">
		<html:form styleClass="form-horizontal" action="/eralis_common.html?method=vehicle_history">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label class="col-lg-2 control-label"> Vehicle Number :</label>
						<div class="col-lg-3 no-padding">
							<html:text property="vehicleNo" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
							<input type="hidden" name="vehicleId" class="form-control" id="vehicleId"/>
						</div>
						<div>
							<span class="input-group-btn">
								<button type="button" class="btn btn-purple btn-sm" onclick="openModal('renewal')">
									<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="historyList" style="display: none;">
				<jsp:include page="/pages/eralis_common/vehicle-history-details.jsp"></jsp:include>
			</div>
			<div class="pull-center" style="display:none;">
				<button class="btn btn-primary btn-sm">View Personal Information</button>
			</div>
		</html:form>
	</div>
</div>

<div id="renewal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                    	<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
		                        <div class="col-sm-4">
		                       		<input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
		                        </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	                        	<div class="col-sm-4">
	                        		<input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
	                        	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Organization Name: </label>
	                        	<div class="col-sm-4">
	                        		<input type="text" id="organizationName" placeholder="Orqanization Name"  />
	                        	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	                       		<div class="col-sm-4">
	                        		<input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
	                        	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	                       		<div class="col-sm-4">
	                        		<input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
	                        	</div>
							</div>
						</form>
					</div>
				</div>
			 </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
						<i class="ace-icon fa fa-search"></i>
						Search
					</button>
	
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>
				  </div>
				 <div id="renewalListTable">
			   </div>
			 </div>
		  </div>
	   </div><!-- PAGE CONTENT ENDS -->
<script>
	function enable(identifier)
	{
		if("National" == identifier)
		{
			$('#address').attr('disabled', true);
			$('#dzongkhag').attr('disabled', false);
			$('#gewog').attr('disabled', false);
			$('#village').attr('disabled', false);
			
			$('#address_div').hide();
			$('#dzongkhag_div').show();
			$('#gewog_div').show();
			$('#village_div').show();
		}
		else if("Official" == identifier)
		{
			$('#ministryDIV').show();
		    $('#departmentDIV').show();
			$('#ministry').attr('disabled', false);
			$('#department').attr('disabled', false);
		   
		}
		else if("Private" == identifier)
		{
		    $('#ministryDIV').hide();
		    $('#departmentDIV').hide();
		}
		else
		{
			$('#country').attr('disabled', false);
			$('#address').attr('disabled', false);
			$('#dzongkhag').attr('disabled', true);
			$('#gewog').attr('disabled', true);
			$('#village').attr('disabled', true); 
			
			$('#country_div').show();
			$('#address_div').show();
			$('#dzongkhag_div').hide();
			$('#gewog_div').hide();
			$('#village_div').hide();
		}
	}

function searchRenewalInfo()
{ 
	var vehicleNumber = $('#vehicleNumberRenewalModal').val();
	var organizationName = $('#organizationName').val();
	var engineNumber = $('#engineNumberRenewalModal').val();
	var chasisNumber = $('#chasisNumberRenewalModal').val();
	var cidNumber = $('#citizenIdRenewalModal').val();

	if(vehicleNumber == "")
		vehicleNumber = "NA";
	//if(ownerName == "")
	//	ownerName = "NA";
	if(engineNumber == "")
		engineNumber = "NA";
	if(chasisNumber == "")
		chasisNumber = "NA";
	if(cidNumber == "")
		cidNumber = "NA";
	if(organizationName == "")
		organizationName = "NA";
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=HISTORY&searchType=OFFENCE&organizationName="+organizationName,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#renewalListTable").html(responseText);
			$("#renewalListTable").show();
			$('#historyList').show();
			$('#editing_form').show();
		}
	});
}

function searchVehicleNumber()
{ 
	fetchRenewalHistory();
	fetchDuplicationHistory();
	fetchTransferHistory();
	fetchConversionHistory();
	fetchFitnessHistory();
	fetchEmissionHistory();
	fetchOffenceHistory();
	fetchVehCancellationHistroy();
	fetchTopDetailsHistory();
	fetchTopCancellationHistory();
}

function fetchRenewalHistory()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getRenewalHistoryList&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#renewalHistoryTable").html(responseText);
			$("#renewalHistoryTable").show();
		}
	  });
}
function fetchVehCancellationHistroy()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=fetchVehCancellationHistroy&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#cancellationDtls").html(responseText);
			$("#cancellationDtls").show();
		}
	  });
}

function fetchTopDetailsHistory()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=fetchTopDetailsHistory&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#topDtlsHistory").html(responseText);
			$("#topDtlsHistory").show();
		}
	  });
}

function fetchTopCancellationHistory()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=fetchTopCancellationHistory&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#topCancellationDtlsHistory").html(responseText);
			$("#topCancellationDtlsHistory").show();
		}
	  });
}
function fetchDuplicationHistory()
{
	var vehicleId = $('#vehicleId').val();
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getDuplicationHistoryList&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#duplicationHistoryTable").html(responseText);
			$("#duplicationHistoryTable").show();
		}
	});
}

function fetchTransferHistory()
{
	var vehicleId = $('#vehicleId').val();
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getTransferHistoryList&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#transferHistoryTable").html(responseText);
			$("#transferHistoryTable").show();
		}
	});
}

function fetchConversionHistory()
{
	var vehicleId = $('#vehicleId').val();
	if(vehicleId == "")
		vehicleId = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getConversionHistoryList&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#conversionHistoryTable").html(responseText);
			$("#conversionHistoryTable").show();
		}
	});
}

function fetchFitnessHistory()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";

	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getFitnessHistoryList&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#fitnessHistoryTable").html(responseText);
			$("#fitnessHistoryTable").show();
		}
	});
}

function fetchEmissionHistory()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";

	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getEmissionHistoryList&vehicleId="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#emissionHistoryTable").html(responseText);
			$("#emissionHistoryTable").show();
		}
	});
}

function fetchOffenceHistory()
{
	var vehicleId = $('#vehicleId').val();
	 
	if(vehicleId == "")
		vehicleId = "NA";

	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getOffenceHistoryList&type=VEHICLE&id="+vehicleId,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#offenceHistoryTable").html(responseText);
			$("#offenceHistoryTable").show();
		}
	});
}
function get_vehicle_edit_dtls(vehicleNo,vehicleType)
{
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/vehicle.html?method=get_vehicle_details&vehicleNo="+vehicleNo+"&vehicleType="+vehicleType,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#vehicle_edit_form").html(responseText);
			$("#vehicle_edit_form").show();
		}
	});
}

	function getVehicleRegSupportingDoc(applicationNo)
	{
		if(vehicleId == "")
			vehicleId = "NA";
	
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getVehicleRegSupportingDoc&applicationNo="+applicationNo,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#vehRegSupportingDoc").html(responseText);
				$("#vehRegSupportingDoc").show();
			}
		});
	}

</script>

