<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Accident history
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			vehicle Accident history details
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12">
		<html:form styleClass="form-horizontal" action="/eralis_common.html?method=vehicle_history">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label class="col-lg-2 control-label"> Vehicle Number :</label>
						<div class="col-lg-3 no-padding">
							<html:text property="vehicleNo" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
							<input type="hidden" name="vehicleId" class="form-control" id="vehicleId"/>
						</div>
						<div>
							<span class="input-group-btn">
								<button type="button" class="btn btn-purple btn-sm" onclick="openModal('renewal')">
									<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="historyList" style="display: none;">
				<jsp:include page="/pages/eralis_common/accident-histroy-dtls.jsp"></jsp:include>
			</div>
			<div class="pull-center" style="display:none;">
				<button class="btn btn-primary btn-sm">View Personal Information</button>
			</div>
		</html:form>
	</div>
</div>

<div id="renewal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                    	<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
		                        <div class="col-sm-4">
		                       		<input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
		                        </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	                        	<div class="col-sm-4">
	                        		<input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
	                        	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Organization Name: </label>
	                        	<div class="col-sm-4">
	                        		<input type="text" id="organizationName" placeholder="Orqanization Name"  />
	                        	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	                       		<div class="col-sm-4">
	                        		<input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
	                        	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	                       		<div class="col-sm-4">
	                        		<input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
	                        	</div>
							</div>
						</form>
					</div>
				</div>
			 </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm" name="search" onclick="searchVehicle()">
						<i class="ace-icon fa fa-search"></i>
						Search
					</button>
	
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>
				  </div>
				 <div id="renewalListTable">
			   </div>
			 </div>
		  </div>
	   </div><!-- PAGE CONTENT ENDS -->
<script>
	function searchVehicle()
	{ 
		var vehicleNumber = $('#vehicleNumberRenewalModal').val();
		var organizationName = $('#organizationName').val();
		var engineNumber = $('#engineNumberRenewalModal').val();
		var chasisNumber = $('#chasisNumberRenewalModal').val();
		var cidNumber = $('#citizenIdRenewalModal').val();
	
		if(vehicleNumber == "")
			vehicleNumber = "NA";
		if(engineNumber == "")
			engineNumber = "NA";
		if(chasisNumber == "")
			chasisNumber = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(organizationName == "")
			organizationName = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=searchAccidentVehicle&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=HISTORY&searchType=OFFENCE&organizationName="+organizationName,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#renewalListTable").html(responseText);
				$("#renewalListTable").show();
				$('#historyList').show();
				$('#editing_form').show();
			}
		});
	}
	
	 
	function fetchAccidentRecord(vehicleNumber)
	{
		$('#displayVehicleNumber').val(vehicleNumber);
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getVehicleAccidentDtls&vehicleNumber='+vehicleNumber,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{	
					
					var Vehicle_Type_Name = $(this).find('Vehicle_Type_Name').text();
					var Owner_Type = $(this).find('Owner_Type').text();
					var Vehicle_Model_Name = $(this).find('Vehicle_Model_Name').text();
					var Vehicle_Company_Name = $(this).find('Vehicle_Company_Name').text();
					var Country_Name = $(this).find('Country_Name').text();
					$("#displayCompany").html(Vehicle_Company_Name);
					$("#displayModel").html(Vehicle_Model_Name);
					$("#displayOrigin").html(Country_Name);
					$("#displayVehicleType").html(Vehicle_Type_Name);
					$("#displayOwnerType").html(Owner_Type);
					
					
				});
			}
		});
		
				
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getAccidentRecord&vehicleNumber="+vehicleNumber,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#accidentDetailHistory").html(responseText);
				$("#accidentDetailHistory").show();
			}
		  });
	}
</script>

