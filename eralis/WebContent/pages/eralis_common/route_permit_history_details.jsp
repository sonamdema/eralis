<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%
	String userName = null;
	EralisUserRolePriviledge userRolePriv = null;
	Role[] roles = null;
	String roleCode = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		userName = userRolePriv.getUserName();
		roles = userRolePriv.getRoles();
		
		Role currentRole = userRolePriv.getCurrentRole();
		roleCode = currentRole.getRoleCode();
	}
%>
 
<div class="row" id="cancelledStatus" style="display:none;">
	<div class="widget-body">
		<div class="widget-main">
			<div class="col-lg-12">
				<div class="form-group">
					<div id="msgDIV" class="col-lg-12 alert alert-danger">
						<div id="cancelReason"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="Msg" class="col-lg-12"></div>

<div id="license_history">
<div class="row">
	<div class="col-lg-12">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Owner Information</h5>
				<%
					if(!roleCode.equalsIgnoreCase("OTHERS"))
					{
				%>
					<button type="button" class="pull-right btn btn-purple btn-sm" onclick="openModal('edit_PIS')" id="viewPersonalDtlsButton">
						View Personal Details
					</button>
				<%
					}
				%>
			</div>
			<div class="widget-body">
				<div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Vehicle Details</h5>
				<!--<button type="button" class="pull-right btn btn-purple btn-sm" onclick="openModal('edit_vehicle_history')">
					Edit Vehicle Details
				</button>
			--></div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Make</label>
								</div>
								<div class="col-lg-3" id="displayCompany">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Model</label>
								</div>
								<div class="col-lg-3" id="displayModel">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Engine Type</label>
								</div>
								<div class="col-lg-3" id="displayEngineType">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Initial Price</label>
								</div>
								<div class="col-lg-3" id="displayInitialPrice"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Initial Registration Date</label>
								</div>
								<div class="col-lg-3" id="displayLastRegistrationDate">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Region</label>
								</div>
								<div class="col-lg-3" id="displayRegion">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Chassis Number</label>
								</div>
								<div class="col-lg-3" id="displayChasisNumber">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Engine Number</label>
								</div>
								<div class="col-lg-3" id="displayEngineNumber">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Seat Capacity</label>
								</div>
								<div class="col-lg-3" id="displaySeatCapacity">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Loading Capacity</label>
								</div>
								<div class="col-lg-3" id="displayLoadCapacity">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">Colour</label>
								</div>
								<div class="col-lg-3" id="displayColor">
								</div>
								<div class="col-lg-3">
									<label class="control-label">Engine CC</label>
								</div>
								<div class="col-lg-3" id="displayEngineCC">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%
	if(!roleCode.equalsIgnoreCase("OTHERS"))
	{
%>
<div class="row">
	<div class="col-lg-12">
		<div id="accordion" class="accordion-style1 panel-group">
		
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							&nbsp;Route Permit Details
						</a>
					</h4>
				</div>
				<div class="panel-collapse collapse" id="collapse1">
					<div class="panel-body">
						<div id="renewalHistoryTable"></div>
					</div>
				</div>
			</div>
		 </div>
	  </div>
   </div>
</div>
<%
	}
%>
<script>

	function printReport()
	{
	 	var divToPrint=document.getElementById('license_history');
	  	var newWin=window.open('','Print-Window');
	  	newWin.document.open();
	  	newWin.document.write(' '+divToPrint.innerHTML+'');
	  	newWin.document.close();
	}

	$(document).ready(function() 
	{
	    $('#renewal-history-table').DataTable({
	         responsive: true
	    });

	    $('#duplication-history-table').DataTable({
	         responsive: true
	    });

	    $('#transfer-history-table').DataTable({
	         responsive: true
	    });

	    $('#fitness-history-table').DataTable({
	         responsive: true
	    });
	});

</script>