<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Charge Calculator
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Calculate payable amount for any transaction
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row" id="messageDiv" style="display:none"></div>

<html:form styleClass="form-horizontal" action="/eralis_common.html?method=charge_calculator">

<div class="row">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title lighter">General Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					
					<div class="row">
						<div class="col-lg-12">
						
							<div class="form-group">
								<div class="col-lg-2">
									<label>Request Type</label>
								</div>
								<div class="col-lg-3">
									<select class="form-control" id="requestType" name="requestType" onchange="populateServiceTypeList(this.value)">
										<option value="">--SELECT--</option>
										<option value="VEHICLE">Vehicle</option>
										<option value="LICENSE">Driving License</option>
										<option value="LEARNER">Learner License</option>
									</select>
								</div>
								<div class="col-lg-2">
									<label>Service Type</label>
								</div>
								<div class="col-lg-3">
									<select class="form-control" id="serviceType" name="serviceType" onchange="showFields(this.value)">
										<option value="">--SELECT--</option>
									</select>
								</div>
							</div>
						
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="otherType"/>

<div class="row" id="REGISTRATION" style="display:none;">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title lighter">Vehicle Registration</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					
					<div class="row">
						<div class="col-lg-12">
						
							<div class="form-group ">
								<div class="col-lg-2">
								   <label>Vehicle Type<span style="color: #ff0000">*</span>:</label>
							    </div>	
		                       <div class="col-lg-2">
			                        <select name="vehicleType" class="form-control" id="vehicleTypeReg" onchange="onChangeVehicleType()">
									   <option value="">--SELECT--</option>
									   <logic:iterate id="vehicleType" name="vehicleTypeList">
									   		 <option value='<bean:write name="vehicleType" property="headerId"/>'>
									   		 	<bean:write name="vehicleType" property="headerName"/>
									   		 </option>
									   </logic:iterate>
								   </select>
                               </div> 
                               <div class="col-lg-2">
								   <label>Engine Type<span style="color: #ff0000">*</span>:</label>
							    </div>	
		                       <div class="col-lg-2">
			                        <select name="engineType" class="form-control" id="engineTypeReg" onchange="onChangeVehicleType()">
									   <option value="">--SELECT--</option>
									   <logic:iterate id="engineType" name="engineTypeList">
									   		 <option value='<bean:write name="engineType" property="headerId"/>'>
									   		 	<bean:write name="engineType" property="headerName"/>
									   		 </option>
									   </logic:iterate>
								   </select>
                               </div> 
                               <div class="col-lg-2">
                                   	<label>Purchase Date<span style="color: #ff0000">*</span>:</label>
                               	</div>	
								<div class="col-lg-2">
									<div class="input-group">
										<input type="text" name="purchaseDate" class="form-control date-picker" id="purchaseDateReg"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
							 	</div>
		                 	</div>
		                 	<div class="form-group">
		                 		<div id="loadCapacityDiv" style="display:none;">
								 	<div class="col-lg-2">
	                                   	<label>Load Capacity<span style="color: #ff0000">*</span>:</label>
	                               	</div>	
									<div class="col-lg-2">
										<div class="input-group">
											<input type="number" name="loadingCapacity" class="form-control" id="loadingCapacityReg"/>
										</div>
								 	</div>
							 	</div>
							 	<div id="seatCapacityDiv" style="display:none;">
								 	<div class="col-lg-2">
	                                   	<label>Seating Capacity<span style="color: #ff0000">*</span>:</label>
	                               	</div>	
									<div class="col-lg-2">
										<div class="input-group">
											<input type="number" name="seatingCapacity" class="form-control" id="seatingCapacityReg"/>
										</div>
								 	</div>
							 	</div>
							 	<div id="vehicleHorsePowerDiv" style="display:none;">
								 	<div class="col-lg-2">
	                                   	<label>Horse Power<span style="color: #ff0000">*</span>:</label>
	                               	</div>	
									<div class="col-lg-2">
										<div class="input-group">
											<input type="number" name="horsePower" class="form-control" id="horsePowerReg"/>
										</div>
								 	</div>
							 	</div>
		                 	</div>
							<div class="form-group">
								<div id="vehicleKiloWattDiv" style="display:none;">
								 	<div class="col-lg-2">
	                                   	<label>Kilowatts<span style="color: #ff0000">*</span>:</label>
	                               	</div>	
									<div class="col-lg-2">
										<div class="input-group">
											<input type="number" name="kilowatt" class="form-control" id="kilowattReg"/>
										</div>
								 	</div>
							 	</div>
							 	<div id="engineCCDiv" style="display:none;">
								 	<div class="col-lg-2">
	                                   	<label>Engine CC<span style="color: #ff0000">*</span>:</label>
	                               	</div>	
									<div class="col-lg-2">
										<div class="input-group">
											<input type="number" name="engineCC" class="form-control" id="engineCCReg"/>
										</div>
								 	</div>
							 	</div>
		                 	</div>
		                 	<div class="form-group">
							   <input type="hidden" id="renewalDuration" value="12"/>
		                 		<div class="col-lg-12 pull-right">
		                 			<button type="button" class="btn btn-primary btn-sm" onclick="calculatePayment()">
		                 				Calculate
		                 			</button>
		                 		</div>
		                 	</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row" id="VEHICLE_OTHER" style="display:none">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title lighter"></h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
							 	<div class="col-lg-2">
									<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
							 	</div>
								<div class="col-lg-3">
									<div class="input-group">
										<input type="text" name="vehicleNumber" class="form-control" id="displayVehicleNumber"/>
										  <span class="input-group-btn">
									      	 <a href="#" onclick="openModal('renewal')" class="btn btn-purple btn-sm">
										  		<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
											</a>
										  </span>
									 </div>
								 </div>
							 </div>
							  <div class="form-group" id="RENEWAL" style="display:none">
								<div class="col-lg-2">
									<label>Renewal Duration :<span style="color: #ff0000">*</span></label>
								</div>
			                     <div class="col-lg-3">
	                                 <div class="input-group">
										<select id="renewalDuration">
										</select>
									</div>
	                             </div>
			                 </div> 
							 <div id="TRANSFER" style="display:none">
							 	 <div class="form-group">
									<div class="col-lg-2">
										<label>Transfer Type :<span style="color: #ff0000">*</span></label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<select class="form-control" id="transferType" onchange="populateRemarks(this.id)">
												<option value="NORMAL">Normal</option>
												<option value="PARENT_TO_CHILD">Parents to child</option>
												<option value="WITHIN_SPOUSE">Within Spouse</option>
												<option value="AUCTIONED">Auctioned</option>
											</select>
										</div>
									</div>
									<div class="col-lg-2">
										<label>Sale Deed Date :<span style="color: #ff0000">*</span></label>
									</div>
				                     <div class="col-lg-3">
		                                 <div class="input-group">
											<input type="text" name="saleDeedDate" class="form-control date-picker" id="saleDeedDate"/>
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
										</div>
		                             </div>
		                     	</div>
		                     	 <div class="form-group">
									<div class="col-lg-2">
										<label>Sale Deed Amount :<span style="color: #ff0000">*</span></label>
									</div>
				                     <div class="col-lg-3">
		                                 <input type="text" name="saleDeedAmount" class="form-control" id="saleDeedAmount"/>
		                             </div>
		                         </div>
			                 </div>  
							 <div class="form-group">
							 	<input type="hidden" name="vehicleId" class="form-control" id="vehicleId"/>
							 	<input type="hidden" name="engineType" id="engineType">
								<input type="hidden" name="vehicleType" id="vehicleType">
								<input type="hidden" name="loadCapacity" id="loadCapacity">
								<input type="hidden" name="seatCapacity" id="seatCapacity">
								<input type="hidden" name="vehicleHorsePower" id="vehicleHorsePower">
								<input type="hidden" name="vehicleKiloWatt" id="vehicleKiloWatt">
								<input type="hidden" name="engineCC" id="engineCC">
								<input type="hidden" name="vehicleTypeDesc" id="vehicleTypeDesc">
							 	<div class="col-lg-12 pull-right">
		                 			<button type="button" class="btn btn-primary btn-sm" onclick="calculatePayment()">
		                 				Calculate
		                 			</button>
		                 		</div>
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="renewal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
						   </div>
						</form>
					</div>
				</div>
			 </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm" onclick="searchRenewalInfo()">
						<i class="ace-icon fa fa-search"></i>
						Search
					</button>
	
					<button class="btn btn-sm btn-primary">
						<i class="ace-icon fa fa-check"></i>
						Reset
					</button>
					
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>
				  </div>
				 <div id="renewalListTable">
			   </div>
			 </div>
		  </div>
	   </div><!-- PAGE CONTENT ENDS -->
</div>

<div class="row" id="NEW" style="display:none">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title lighter">New License Issuance</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
							 	<div class="col-lg-2">
									<label>License Type:<span style="color: #ff0000">*</span></label>
							 	</div>
								<div class="col-lg-3">
									<select id="licenseType" class="form-control">
										<option value="">--SELECT--</option>
										<option value="C">Commercial</option>
										<option value="N">Non-commercial</option>
									</select>
								 </div>
							 </div>
							 <div class="form-group">
							 	<div class="col-lg-12 pull-right">
							 		<input type="hidden" id="renewalDuration" value="120"/>
		                 			<button type="button" class="btn btn-primary btn-sm" onclick="calculatePayment()">
		                 				Calculate
		                 			</button>
		                 		</div>
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row" id="LICENSE_RENEWAL" style="display:none">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title lighter">License Renewal</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-3">
									<label class="control-label">License No:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO"></html:text>
										<span class="input-group-btn">
											<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
												<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
											</button>
										</span>	
									</div>
								</div>
								<div id="RENEWAL_LICENSE" style="display:none">
									<div class="col-lg-3">
										<label class="control-label">Renewal Duration:</label>
									</div>
									<div class="col-lg-3">
										<select id="licenseRenewalDuration">
										</select>
									</div>
								</div>
							 </div>
							 <div class="form-group">
							 	<div class="col-lg-12 pull-right">
							 		<input type="hidden" id="licensetype1"/>
							 		<input type="hidden" id="drivinglicenseId"/>
		                 			<button type="button" class="btn btn-primary btn-sm" onclick="calculatePayment()">
		                 				Calculate
		                 			</button>
		                 		</div>
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									
									<div class="form-group">
										<label>Name:</label>
										<span class="pull-right">
											<input type="text" id="namePersonalModal" placeholder="Name"  style="width: 200px" />
										</span>
									</div>
									
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									
									
									<div class="form-group">
										<label>License No:</label>
										<span class="pull-right">
											<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<select id="regionPersonalModal" style="width: 200px" >
											<option value="">SELECT</option>
											<logic:iterate id="region" name="regionList">
												<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
											</logic:iterate>
										</select> 
										</span>
									</div>
									<div class="form-group">
										<label>License Type:</label>
										<span class="pull-right">
											 <select id="licenseTypeModal" style="width: 200px">
                                      			<option value="">SELECT</option>
                                      			<option value="C">commercial</option>
                                      			<option value="N">non-commercial</option>
                                      		</select>
										</span>
									</div>
									
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
</div>

<div class="row" id="LEARNER_RENEWAL" style="display:none;">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title lighter">Learner License Renewal</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">License No:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<input type="text" name="licenseNo" class="form-control" id="licenseNo" readonly="readonly"/>
											<input type="hidden" name="learnerLicenseId" class="form-control" id="learnerLicenseId"/>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('duplicationModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
								</div>
							</div>
							 <div class="form-group">
							 	<div class="col-lg-12 pull-right">
		                 			<button type="button" class="btn btn-primary btn-sm" onclick="calculatePayment()">
		                 				Calculate
		                 			</button>
		                 		</div>
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="duplicationModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Name:</label>
							<span class="pull-right">
								<input type="text" id="nameduplicationModal" placeholder="Name"  style="width: 200px" />
							</span>
						</div>
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidduplicationModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>Learner License No:</label>
							<span class="pull-right">
								<input type="text" id="learnerNoduplicationModal" placeholder="Learner License No" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>Region:</label>
							<span class="pull-right">
								<select id="regionduplicationModal" style="width: 200px">
									<option value="NA">SELECT</option>
									    <logic:iterate id="region" name="regionList">
												   <option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
									   </logic:iterate>
								</select> 
							</span>
						</div>	
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" onclick="searchCommonInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="learnerDuplicateListTable">
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
</div>

<jsp:include page="/pages/eralis_common/payment-modal.jsp"></jsp:include>
</html:form>

<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">

		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		function calculatePayment()
		{
			var requestType = $('#requestType').val();
			var type = $('#serviceType').val();
			
			var identityNo = "", identityTypeId = "", loadingCapacity = "", seatingCapacity = "", vehicleHP = "", kilowatts = "", engineCC = "", purchaseDate = "", saleDeedAmount = "", saleDeedDate = "", renewalDuration = "";

			if(requestType == "VEHICLE" && type == "REGISTRATION")
			{
				requestType = $('#requestType').val();
				serviceType = $('#serviceType').val();
				if( $("#engineType").val() == "Electric")
					identityTypeId = '0'; 
				else
					identityTypeId = $('#vehicleTypeReg').val();
				
				loadingCapacity = $('#loadingCapacityReg').val();
				seatingCapacity = $('#seatingCapacityReg').val();
				vehicleHP = $('#horsePowerReg').val(); 
				kilowatts = $('#kilowattReg').val();
				engineCC = $('#engineCCReg').val();
				purchaseDate = $('#purchaseDateReg').val();
				renewalDuration = $('#renewalDuration').val();
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
			}
			else if(requestType == "VEHICLE" && (type == "RENEWAL" ||
					type == "TRANSFER" ||
					type == "CONVERSION" ||
					type == "DUPLICATION"))
			{
				requestType = $('#requestType').val();
				serviceType = $('#serviceType').val();
				var identityNo = $('#vehicleId').val();
				if( $("#engineType").val() == "Electric")
					identityTypeId = '0'; 
				else
					identityTypeId = $('#vehicleType').val();
				var loadingCapacity = $('#loadCapacity').val();
				var seatingCapacity = $('#seatCapacity').val();
				var vehicleHP = $('#vehicleHorsePower').val();
				var kilowatts = $('#vehicleKiloWatt').val();
				var engineCC = $('#engineCC').val();
				var transferType = $('#transferType').val();

				if(type == "TRANSFER")
				{
					saleDeedAmount = $('#saleDeedAmount').val();
					saleDeedDate = $('#saleDeedDate').val();
					
				}
				else if(type == "RENEWAL")
					renewalDuration = $('#renewalDuration').val();
				
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration, transferType);
			}
			else if(requestType == "LICENSE" && type == "NEW")
			{
				requestType = $('#requestType').val();
				serviceType = $('#serviceType').val();
				identityTypeId = $('#licenseType').val();
				identityNo = "";
				renewalDuration = $('#renewalDuration').val();
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
			}
			else if(requestType == "LICENSE" && (type == "RENEWAL" ||
					type == "DUPLICATE" ||
					type == "ENDORSEMENT"))
			{
				requestType = $('#requestType').val();
				serviceType = $('#serviceType').val();
				identityTypeId = $('#licensetype1').val();
				identityNo = $('#drivinglicenseId').val();

				if(type == "RENEWAL")
					renewalDuration = $('#licenseRenewalDuration').val();

				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
			}
			else if(requestType == "LEARNER" && (type == "NEW" ||
					type == "RENEWAL" ||
					type == "DUPLICATE"))
			{
				requestType = $('#requestType').val();
				serviceType = $('#serviceType').val();
				identityTypeId = "L";

				if(type == "RENEWAL")
					identityNo = $('#learnerLicenseId').val();
				else
					identityNo = "";
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate);
			}
		}

		function populateServiceTypeList(requestType)
		{
			if(requestType == "VEHICLE")
			{
				$('#serviceType').empty();
				$('#serviceType').append("<option value=''>--SELECT--</option>");
				$('#serviceType').append("<option value='REGISTRATION'>Registration</option>");
				$('#serviceType').append("<option value='RENEWAL'>Renewal</option>");
				$('#serviceType').append("<option value='TRANSFER'>Transfer</option>");
				$('#serviceType').append("<option value='CONVERSION'>Conversion</option>");
				$('#serviceType').append("<option value='DUPLICATION'>Duplication</option>");
			}
			else if(requestType == "LICENSE")
			{
				$('#serviceType').empty();
				$('#serviceType').append("<option value=''>--SELECT--</option>");
				$('#serviceType').append("<option value='NEW'>New Issuance</option>");
				$('#serviceType').append("<option value='RENEWAL'>Renewal</option>");
				$('#serviceType').append("<option value='DUPLICATE'>Duplication</option>");
				$('#serviceType').append("<option value='ENDORSEMENT'>Endorsement</option>");
			}
			else if(requestType == "LEARNER")
			{
				$('#serviceType').empty();
				$('#serviceType').append("<option value=''>--SELECT--</option>");
				$('#serviceType').append("<option value='NEW'>New Issuance</option>");
				$('#serviceType').append("<option value='RENEWAL'>Renewal</option>");
				$('#serviceType').append("<option value='DUPLICATE'>Duplication</option>");
			}
		}

		function showFields(id)
		{
			var requestType = $('#requestType').val();

			$('#REGISTRATION').hide();
			$('#TRANSFER').hide();
			$('#RENEWAL').hide();
			$('#NEW').hide();
			$('#LICENSE_RENEWAL').hide();
			$('#RENEWAL_LICENSE').hide();
			$('#LEARNER_RENEWAL').hide();
			
			if(requestType == "VEHICLE" && id == "REGISTRATION")
			{
				$('#'+id).show();
			}
			else if(requestType == "VEHICLE" && (id == "RENEWAL" || id == "TRANSFER" || id == "CONVERSION" || id == "DUPLICATION"))
			{
				$('#VEHICLE_OTHER').show();

				if(id == "TRANSFER")
					$('#TRANSFER').show();
				else if(id == "RENEWAL")
					$('#RENEWAL').show();
			}
			else if(requestType == "LICENSE" && id == "NEW")
			{
				$('#'+id).show();
			}
			else if(requestType == "LICENSE" && (id == "RENEWAL" || id == "DUPLICATE" || id == "ENDORSEMENT"))
			{
				$('#LICENSE_RENEWAL').show();

				if(id == "RENEWAL")
					$('#RENEWAL_LICENSE').show();
			}
			else if(requestType == "LEARNER" && (id == "NEW" || id == "DUPLICATE"))
			{
				calculatePayment();
			}
			else if(requestType == "LEARNER" && id == "RENEWAL")
			{
				$('#LEARNER_RENEWAL').show();
			}
		}

		function onChangeVehicleType()
		{
			var vehicleType	=	$("#vehicleTypeReg :selected").text();
			var engineType	=	$("#engineTypeReg :selected").text();

			vehicleType = vehicleType.trim();
			engineType = engineType.trim();

			$('#loadingCapacityReg').val('0');
			$('#loadCapacityDiv').hide();
			$('#seatingCapacityReeg').val('0');
			$('#seatCapacityDiv').hide();
			$('#horsePowerReg').val('0');
			$('#vehicleHorsePowerDiv').hide();
			$('#kilowattReg').val('0');
			$('#vehicleKiloWattDiv').hide();
			$('#engineCCReg').val('0');
			$('#engineCCDiv').hide();
			 
			if(vehicleType=='Heavy Bus' || vehicleType=='Medium Bus' || vehicleType=='Taxi')
			{
				$('#seatCapacityDiv').show();
				$('#seatingCapacityReg').val('');
			}
			else if(vehicleType=='Heavy Vehicle')
			{
				$('#loadCapacityDiv').show();
				$('#loadingCapacityReg').val('');
			}
			else if(vehicleType=='Tractor')
			{
				$('#vehicleHorsePowerDiv').show();
				$('#horsePowerReg').val('');
			}
			
			if(engineType=='Diesel')
			{
				$('#engineCCDiv').show();
				$('#engineCCReg').val('');
			}
			else if(engineType=='Electric')
			{
				$('#vehicleKiloWattDiv').show();
				$('#kilowattReg').val('');
			}
			else if(engineType=='Hybrid')
			{
				$('#engineCCDiv').show();
				$('#engineCCReg').val('');
			}
			else if(engineType=='Petrol')
			{
				$('#engineCCDiv').show();
				$('#engineCCReg').val('');
			}
		}

		function searchRenewalInfo()
		{ 
			var ownerType = $('#ownerTypeRenewalModal').val();
			var vehicleType = $('#vehicleTypeRenewalModal').val();
			var vehicleNumber = $('#vehicleNumberRenewalModal').val();
			var ownerName = $('#ownerNameRenewalModal').val();
			var engineNumber = $('#engineNumberRenewalModal').val();
			var chasisNumber = $('#chasisNumberRenewalModal').val();
			var cidNumber = $('#citizenIdRenewalModal').val();

			if(ownerType == "")
				ownerType = "NA";
			if(vehicleType == "")
				vehicleType = "NA";
			if(vehicleNumber == "")
				vehicleNumber = "NA";
			if(ownerName == "")
				ownerName = "NA";
			if(engineNumber == "")
				engineNumber = "NA";
			if(chasisNumber == "")
				chasisNumber = "NA";
			if(cidNumber == "")
				cidNumber = "NA";

			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&ownerType="+ownerType+"&vehicleType="+vehicleType+"&vehicleNumber="+vehicleNumber+"&ownerName="+ownerName+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#renewalListTable").html(responseText);
					$("#renewalListTable").show();
				}
			});
		}

		function searchVehicleNumber()
		{
			$('#renewalDuration').empty();
			var vehicleTypeDesc = $('#vehicleTypeDesc').val();

			if(vehicleTypeDesc == "TWO_WHEELER" || vehicleTypeDesc == "LIGHT_VEHICLE")
			{
				$('#renewalDuration').append("<option value='12'>1 Year</option>");
				$('#renewalDuration').append("<option value='24'>2 Years</option>");
				$('#renewalDuration').append("<option value='36'>3 Years</option>");
				$('#renewalDuration').append("<option value='48'>4 Years</option>");
				$('#renewalDuration').append("<option value='60'>5 Years</option>");
			}
			else
			{
				$('#renewalDuration').append("<option value='6'>6 Months</option>");
				$('#renewalDuration').append("<option value='12'>1 Year</option>");
			}
		}

		function searchPersonalInfo()
		{
			var name 		= $('#namePersonalModal').val();
			var cidNumber 	= $('#cidPersonalModal').val();
			var regionId 	= $('#regionPersonalModal').val();
			var licenseNo	= $('#licenseNoModal').val();
			var licenseType	= $('#licenseTypeModal').val();
			
			if(name == "")
				name = "NA";
			if(cidNumber == "")
				cidNumber = "NA";
			if(regionId == "")
				regionId = "NA";
			if(licenseNo == "")
				licenseNo = "NA";
			if(licenseType == "")
				licenseType = "NA";
		 
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&name="+name+"&cid="+cidNumber+"&region="+regionId+"&licenseNo="+licenseNo+"&licenseType="+licenseType+"&searchType=LICENSE_COMMERCIAL",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#licenseDetailList").html(responseText);
					$("#licenseDetailList").show();
				}
			});
		} 

		function searchsuspensionlist()
		{
			$('#licenseRenewalDuration').empty();
			var licenseType = $('#licensetype1').val();

			if(licenseType == "N")
			{
				$('#licenseRenewalDuration').append("<option value='120'>10 Years</option>");
				$('#licenseRenewalDuration').append("<option value='60'>5 Years</option>");
			}
			else
			{
				$('#licenseRenewalDuration').append("<option value='36'>3 Years</option>");
			}
		}

		function searchCommonInfo()
		{
			var name = $('#nameduplicationModal').val();
			var cid = $('#cidduplicationModal').val();
			var learnerno = $('#learnerNoduplicationModal').val();
			var regionId = $('#regionduplicationModal').val();
			
			if(name == "")
				name = "NA";
			if(cid == "")
				cid = "NA";
			if(learnerno == "")
				learnerno = "NA";
			if(regionId == "")
				regionId = "NA";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&name="+name+"&cid="+cid+"&learnerno="+learnerno+"&region="+regionId,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#learnerDuplicateListTable").html(responseText);
					$("#learnerDuplicateListTable").show();
				}
			});
		}
		function populateRemarks(id)
		{
			$('#remarks').val("");
			$('#saleDeedAmount').attr('readonly',false);
			
			var selectedOption = $("#transferType option:selected").text();
			$('#remarks').val(selectedOption);

			if($('#transferType').val() == "NORMAL" ||
					$('#transferType').val() == "AUCTIONED"){
				$('#saleDeedAmount').attr('readonly',false);
				$('#saleDeedAmount').val('');
			}
			if($('#transferType').val() == "PARENT_TO_CHILD" ||
					$('#transferType').val() == "WITHIN_SPOUSE"){
				$('#saleDeedAmount').attr('readonly',true);
				$('#saleDeedAmount').val('0');
			}
			
		}
</script>
			
          

