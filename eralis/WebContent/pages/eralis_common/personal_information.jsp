<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i> Personal Information 
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i> personal
			information details 
		</small>
	</h1>
</div>
<!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/eralis_common.html" styleId="personalForm">

		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
				<logic:equal value="Y" name="priviledge" property="isEdit">
					<span class="pull-right">
						<button type="button" class="btn btn-purple btn-sm" onclick="openModal('personalModal')">
							Search Existing PIS
							<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
						</button>
					</span>
				</logic:equal>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="pull-right">
								<div id="pic" style="display:none;"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<html:hidden property="customerID" styleClass="form-control" styleId="customerID"></html:hidden>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Citizen ID <i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="CID" styleClass="form-control" styleId="cid"></html:text>
										<span class="input-group-btn">
											<button type="button" class="btn btn-purple btn-sm" onclick="getCidDetails()">
												Go
											</button>
										</span>
									</div>
								</div>
								<div class="col-lg-7">
									<div id="msgDiv">
										<div class="alert alert-info">
											Pull new PIS record from Census
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Title of Courtesy<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3" >
									<html:select property="titleOfcourtesy"
										styleClass="form-control" styleId="titleOfcourtesy">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="titleOfcourtesyList"
											label="headerName" value="headerId"  />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>First Name<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-2" >
									<html:text property="firstname" styleId="firstname"
										styleClass="form-control" readonly="true"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Middle Name:</label>
								</div>
								<div class="col-lg-2" >
									<html:text property="middlename" styleId="middlename"
										styleClass="form-control" readonly="true"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Last Name:</label>
								</div>
								<div class="col-lg-2" >
									<html:text property="lastName" styleId="lastName"
										styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2" >
									<label>Occupation<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:select property="occupation" styleClass="form-control" styleId="occupation">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="occupationList" label="headerName" value="headerId" />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Date Of Birth<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="DOB"
											styleClass="form-control date-picker"
											styleId="displayDob" readonly="true"></html:text>
										<span class="input-group-addon"> <i
											class="fa fa-calendar bigger-110"></i> </span>
									</div>
								</div>
								<div class="col-lg-2">
									<label>Blood Group<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:select property="bloodGroup" styleClass="form-control" styleId="bloodGroup">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="bloodgroupList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Father's Name :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="fathersName" styleId="fathersName"
										styleClass="form-control" readonly="true"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Mother's Name :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="mothersName" styleId="mothersName" styleClass="form-control" ></html:text>
								</div>
								
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Gender<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:radio property="gender" value="M" styleId="maleRadio" styleClass="ace">
										<span class="lbl"> Male</span>
									</html:radio>
									<html:radio property="gender" value="F" styleId="femaleRadio" styleClass="ace">
										<span class="lbl"> Female</span>
									</html:radio>
								</div>
							</div>
							<div class="form-group">
								<div style="display: none;">
									<div class="col-lg-2">
										<label>Identification Marks:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="identificationMarks"
											styleId="identificationMarks" styleClass="form-control" value="NA"></html:text>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Permanent Address</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="radio">
									<label class="col-sm-2 no-padding-right">
										<html:radio property="national" styleId="nationalRadio" onclick="enable('National')" styleClass="ace"
											value="N"></html:radio> <span class="lbl"> Bhutanese</span> </label>
									<label class="col-sm-2 no-padding-right">
										<html:radio property="national" styleId="internationalRadio" onclick="enable('International')" styleClass="ace"
											value="I"></html:radio> <span class="lbl"> Foreign National</span> </label>
								</div>
							</div>
							<div class="form-group" id="dzongkhag_div">
								<div class="col-lg-2">
									<label>Dzongkhag<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<select id="dzongkhag" class="form-control" onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N')" >
										<option value="">--SELECT--</option>
										<logic:iterate id="dzongkhag" name="dzongkhagList">
											<option value='<bean:write name="dzongkhag" property="headerId"/>'><bean:write name="dzongkhag" property="headerName"/></option>
										</logic:iterate>
									</select>
								</div>
								<div id="requiredDzongkhag" style="display:none;" class="error">Please select Dzongkhag</div>
							</div>
							<div class="form-group" id="gewog_div">
								<div class="col-lg-2">
									<label>Gewog<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<select id="gewog" class="form-control">
										<option value="">--SELECT--</option>
									</select>
								</div>
								<div  id="requiredGewog" style="display:none;" class="error">Please select Gewog</div>
							</div>
							<div class="form-group" id="village_div">
								<div class="col-lg-2">
									<label>Village<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="village" styleId="village"
										styleClass="form-control"></html:text>
								</div>
								<div   class="error" id="requiredVillage" style="display:none;" >Please select Village</div>
								 
							</div>
							<div class="form-group" id="country_div">
								<div class="col-lg-2">
									<label>Country:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="country" styleClass="form-control" styleId="country" onchange="populateDependentDropDown(this.value, 'nationality', '', 'GET_NATIONALITY_LIST', 'N')">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="countryList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								<div id="requiredCountry" style="display:none;" class="error">Please select Country</div>
							</div>
							<div class="form-group" id="nationality_div">
								<div class="col-lg-2">
									<label>Nationality<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:select property="nationality" styleClass="form-control" styleId="nationality">
										<html:option value="-1">--SELECT--</html:option>
										<html:optionsCollection name="nationalityList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
							</div>
								
							<div class="form-group" id="address_div">
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="address" styleId="address"
										styleClass="form-control"></html:text>
								</div>
								<div id="requiredAddress" style="display:none;"  class="error">Please enter Address</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Contact Address</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<div class="radio">
									<label class="col-sm-2 no-padding-right">
										   <html:radio property="contactRadio" styleId="officialRadio" onclick="enable('Official')" styleClass="ace"
											value="Official"></html:radio> <span class="lbl"> Official</span> </label>
									<label class="col-sm-2 no-padding-right">
										   <html:radio property="contactRadio" styleId="privateRadio" onclick="enable('Private')" styleClass="ace"
											value="Private"></html:radio> <span class="lbl">
										    Business/Personal</span> </label>
								     </div>
							 </div>
							<div class="form-group" id="ministryDIV" style="display:none">
								<div class="col-lg-2">
									<label>Ministry:</label>
								</div>
								<div class="col-lg-3">
								<html:select property="ministry" styleClass="form-control" styleId="ministry" onchange="populateDependentDropDown(this.value, 'department', '', 'DEPARTMENT_LIST', 'N')">
										<html:option value="0">--SELECT--</html:option>
						   				<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
								</html:select>
								</div>
								<div id="requiredMinistry" style="display:none;" class="error">Please select Ministry</div>
							</div>
							<div class="form-group" id="departmentDIV" style="display:none">
								<div class="col-lg-2">
									<label>Department:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="department" styleClass="form-control" styleId="department">
										<html:option value="0">--SELECT--</html:option>
									</html:select>
								</div>
								<div id="requiredDepartment" style="display:none;" class="error">Please select Department</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Dzongkhag<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:select property="presentDzongkhag" styleClass="form-control" styleId="presentDzongkhag">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="dzongkhagList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								<div id="requiredContactDzongkhag" style="display:none;" class="error">Please select Dzongkhag</div>
							</div>
                            <div class="form-group">
								<div class="col-lg-2">
									<label>Contact Address:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="contactAddress" styleId="contactAddress"
										styleClass="form-control"></html:textarea>
								</div>
						    </div> 
							<div class="form-group">
								<div class="col-lg-2">
									<label>Phone<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="phone" styleId="phone"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Guardian Contact No :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="guardianNo" styleId="guardianNo" styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Email:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="email" styleId="email"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Upload Photo:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="upload" styleClass="form-control" styleId="id-input-file-2"></html:file>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<div id="messageDiv"></div>
		
		<div class="pull-right">
			<html:hidden property="dzongkhag" styleId="dzongkhagHidden"/>
			<html:hidden property="personalInfoId" styleId="personalInfoId"/>
			<html:hidden property="gewog" styleId="gewogHidden"/>
			<html:hidden property="manualFlag" styleId="manualFlag" value="N"/>

			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" onclick="formSubmit()" id="submitBtn">Save</button>
			</logic:equal>
			<logic:equal value="Y" name="priviledge" property="isEdit">
				<button type="button" class="btn btn-primary btn-sm" onclick="editPersonalInfo()" id="updateBtn">Update</button>
			</logic:equal>
<!--			<logic:equal value="Y" name="priviledge" property="isDelete">-->
<!--				<button type="button" class="btn btn-primary btn-sm" onclick="openModal('delete-modal')">Delete</button>-->
<!--			</logic:equal>-->
		</div>
		
	</html:form>
</div>
<div id="personalModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			  <div class="modal-body">
				 <div class="row">
				    <div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
								</span>
						</div>
						<div class="form-group">
							<label>Customer ID:</label>
							<span class="pull-right">
								<input type="text" id="customerIdPersonalModal" placeholder="CustomerID" style="width: 200px"  />
							</span>
						</div>
					</div>
				  </div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm" type="button"  onclick="searchCommonInfo()" name="search" >
						<i class="ace-icon fa fa-search" ></i> Search
					</button>
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i> Cancel
					</button>
				</div>
			  <div id="commonListTable">
			</div>
		</div>
	</div>
</div>
<div id="delete-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Confirmation</h4>
			</div>

			<div class="modal-body">
				Are you sure you want to delete the selected record?
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" onclick="deletePersonalInfo()" >
					<i class="ace-icon fa fa-check-square-o" ></i> Yes
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
		</div>
	</div>
</div>
 
<!-- PAGE CONTENT ENDS -->
<!-- PAGE CONTENT ENDS -->

<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>

<script type="text/javascript">
		$(document).ready(function()
        {
	        $("#personalForm").validate
	        ({
	        	invalidHandler: function(form, validator) {
		             var errors = validator.numberOfInvalids();
		             if (errors) {                    
		                 var firstInvalidElement = $(validator.errorList[0].element);
		                 $('html,body').scrollTop(firstInvalidElement.offset().top);
		                 firstInvalidElement.focus();
		             }
		        },
				rules:
				{
					titleOfcourtesy:
					{
						required:true,
					},
					firstname:
					{
						required: true,
					},
					occupation:
					{
						required: true,
					},
					nationality:
					{
						required: true,
					},
					displayDob:
					{
						required: true,
					},
					/* fathersName:
					{
						required: true,
					}, */
					bloodGroup:
					{
						required: true,
					},
					presentDzongkhag:
					{
						required: true,
					},
					
					contactAddress:
					{
						required: true,
					},
					phone:
					{
						required: true,
						number: true,
					}
				},    
				messages:
				{
					titleOfcourtesy:{required: "Please Select Title of Courtesy"},
					firstname:{required: "Please Enter First Name"},
					occupation:{required: "Please Select Occupation"},
					nationality:{required: "Please Select Nationality"},
					displayDob:{required: "Please Enter Date of Birth"},
					/* fathersName:{required: "Please Enter Fathers name"}, */
					bloodGroup:{required: "Please Select Blood Group"},
					presentDzongkhag:{required: "Please Select present dzongkhag"},
					contactAddress:{required: "Please enter contact address"},
					phone:{required: "Please enter phone number"}
				}
			});
        });

	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	//radio button
	$(document).ready(function() {
		$('#nationalRadio').attr('checked', true);
		$('#officialRadio').attr('checked', true);
		//$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);
		$('#ministryDIV').show();
	    $('#departmentDIV').show();
	    
	    
	    
	    $('#address_div').hide();
		$('#dzongkhag_div').show();
		$('#gewog_div').show();
		$('#village_div').show();
		
	});

	//radio button
	function enable(identifier)
	{
		if("National" == identifier)
		{
			//$('#country').attr('disabled', true);
			$('#address').attr('disabled', true);
			$('#dzongkhag').attr('disabled', false);
			$('#gewog').attr('disabled', false);
			$('#village').attr('disabled', false);
			
			$('#address_div').hide();
			$('#dzongkhag_div').show();
			$('#gewog_div').show();
			$('#village_div').show();

			$("#personalForm").validate
	        ({
				rules:
				{ 
					dzongkhag:
					{
						required: true,
						number: true,
					},
					gewog:
					{
						required: true,
						number: true,
					},
					village:
					{
						required: true,
						number: true,
					}
				},    
				messages:
				{
					 
					phone:{required: "Please enter phone number"},
					dzongkhag:{required: "Please enter Permanent Dzongkhag"},
					gewog:{required: "Please enter Gewog"},
					village:{required: "Please enter Village"}
				}
			});
		}
		else if("Official" == identifier)
		{
			$('#ministryDIV').show();
		    $('#departmentDIV').show();
			$('#ministry').attr('disabled', false);
			$('#department').attr('disabled', false);
		    $("#personalForm").validate
	        ({
				rules:
				{ 
					ministry:
					{
						required: true,
						number: true,
					},
					department:
					{
						required: true,
						number: true,
					}
					
				},    
				messages:
				{
					 
					ministry:{required: "Please enter phone number"},
					department:{required: "Please enter Permanent Dzongkhag"},
				}
			});
		}
		else if("Private" == identifier)
		{
			$('#ministryDIV').hide();
		    $('#departmentDIV').hide();
		}
		else
		{
			$('#country').attr('disabled', false);
			$('#address').attr('disabled', false);
			$('#dzongkhag').attr('disabled', true);
			$('#gewog').attr('disabled', true);
			$('#village').attr('disabled', true); 
			
			$('#country_div').show();
			$('#address_div').show();
			$('#dzongkhag_div').hide();
			$('#gewog_div').hide();
			$('#village_div').hide();
		}
	}

	function searchCommonInfo()
	{
		$('#submitBtn').hide();
		$('#updateBtn').show();
		var cidNumber = $('#cidPersonalModal').val();
		var customerId = $('#customerIdPersonalModal').val();
		if(cidNumber == "")
			cidNumber = "NA";
		if(customerId == "")
			customerId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoForEdit&cid="+cidNumber+"&customerId="+customerId+"&searchType=VEHICLE_REGISTRATION",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#commonListTable").html(responseText);
				$("#commonListTable").show();
			}
		});
	}

	function formSubmit()
	{	
		var country	=	$("#country").val();
		var address	=	$("#address").val();
		var dzongkhag	=	$("#dzongkhag").val();
		var gewog	=	$("#gewog").val();
		var village	=	$("#village").val();
		var ministry	=	$("#ministry").val();
		var department	=	$("#department").val();
		//var presentDzongkhag =$("#presentDzongkhag").val();

		$("#requiredDzongkhag").hide();
		$("#requiredGewog").hide();
		$("#requiredVillage").hide();
		$("#requiredCountry").hide();
		$("#requiredAddress").hide();
		$("#requiredMinistry").hide();
		$("#requiredDepartment").hide();
		//$("#requiredContactDzongkhag").hide();
		if($("#nationalRadio").prop("checked"))
		{
			if(dzongkhag=='')
			{
				$("#requiredDzongkhag").show();
			}
			if(gewog=='')
			{
				$("#requiredGewog").show();
			}
			if(village=='')
			{
				$("#requiredVillage").show();
			}
		}
		else if($("#internationalRadio").prop("checked"))
		{
			if(country=='')
			{
				$("#requiredCountry").show();
			}
			if(address=='')
			{
				$("#requiredAddress").show();
			}
		}
		if($("#officialRadio").prop("checked"))
		{
			if(ministry=='0')
			{
				$("#requiredMinistry").show();
			}
			if(department=='0')
			{
				$("#requiredDepartment").show();
			}
		}
		if($('#personalForm').valid()) 
		{
			if($("#nationalRadio").prop("checked"))
			{
				if(dzongkhag==''|| gewog=='' || village=='')
				{
					return false;
				}
			}
			else  if($("#internationalRadio").prop("checked"))
			{
				if(country==''|| address=='')
				{
					return false;
				}
			}
			else  if($("#officialRadio").prop("checked"))
			{
				if(ministry==''|| department=='')
				{
					return false;
				}
			}

			$('#submitBtn').attr('disabled',true);
			
			if($('#manualFlag').val() == "Y")
			{
				$('#dzongkhagHidden').val($('#dzongkhag').val());
				$('#gewogHidden').val($('#gewog').val());
			}
			
			var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=personal_info',type:'POST',data: $("#personalForm").serialize()}; 
		    $("#personalForm").ajaxSubmit(options);
	        $('#messageDiv').show();
	        $('#messageDiv').get(0).scrollIntoView();
	        //setTimeout('hideStatus("messageDiv")',5000);
	       // setTimeout('reloadPage()',5000);
		}
		
	}

	function editPersonalInfo()
	{
		$('#dzongkhagHidden').val($('#dzongkhag').val());
		$('#gewogHidden').val($('#gewog').val());
		
		var ministry	=	$("#ministry").val();
		var department	=	$("#department").val();
		$("#requiredMinistry").hide();
		$("#requiredDepartment").hide();
		
		//alert($("#officialRadio").prop("checked"));
		if($("#officialRadio").prop("checked"))
		{
			if(ministry=='0')
				
			{
				$("#requiredMinistry").show();
			}
			if(department=='0')
			{
				
				$("#requiredDepartment").show();
			}
		}
		if($('#personalForm').valid()) 
		{
		 if($("#officialRadio").prop("checked"))
			{
				if(ministry=='0'|| department=='0')
				{
					return false;
				}
			}
		
	   $('#updateBtn').attr('disabled',true);
		
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=edit_personal_info',type:'POST',data: $("#personalForm").serialize()}; 
	    $("#personalForm").ajaxSubmit(options);
        $('#messageDiv').show();
        $('#messageDiv').get(0).scrollIntoView();
        setTimeout('hideStatus("messageDiv")',10000);
        setTimeout('reloadPage()',10000);
	  }
	}

	function deletePersonalInfo()
	{
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=delete_personal_info',type:'POST',data: $("#personalForm").serialize()}; 
	    $("#personalForm").ajaxSubmit(options);
        $('#messageDiv').show();
        $('#messageDiv').get(0).scrollIntoView();
        setTimeout('hideStatus("messageDiv")',10000);
        setTimeout('reloadPage()',10000);
        $('#delete-modal').modal('hide');
	}

	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>

	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";
</script>

<style>
	#personalForm .error { color: red; }
</style>