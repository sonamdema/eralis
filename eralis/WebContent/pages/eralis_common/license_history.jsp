<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		License History
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			license history detailslicense_cancellation_list
		</small>
	</h1>
</div><!-- /.page-header -->
<jsp:include page="/pages/common/edit_personal_dtls.jsp"></jsp:include>
<div class="row">
	<div class="col-lg-12">
	
		<html:form styleClass="form-horizontal" action="/eralis_common.html?method=license_history">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label>License Number:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="licenseNo" styleId="licenseNO" styleClass="form-control" readonly="true"></html:text>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<div class="col-lg-2">
										<label>Learner Number:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="learnerNo" styleId="learnerNo" styleClass="form-control" readonly="true"></html:text>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('learnerModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<input type="hidden" name="drivinglicenseId" class="form-control" id="drivinglicenseId"/>
									<input type="hidden" name="learnerLicenseId" class="form-control" id="learnerLicenseId"/>
									<input type="hidden" name="learnerCustomerId" id="learnerCustomerId"/>
								</div>
							</div>
						</div>
						<div class="row" id="Msg">
						</div>
						<div class="row" id="historyList">
							<jsp:include page="/pages/eralis_common/license-history-details.jsp"></jsp:include>
						</div>
			 </div>
		</html:form>
	</div>
</div>
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			
			<div class="modal-body">
			<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm"  name="search"  onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="licenseDetailList"></div>
		</div>
	</div>
</div>
<div id="learnerModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidlicenseModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>Learner License No:</label>
								<span class="pull-right">
									<input type="text" id="learnerNolicenseModal" placeholder="Learner License No" style="width: 200px"  />
								</span>
							</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="learnerInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="learnerInfoList">
			</div>
			
		</div>
	</div>
</div>
<script>

	function learnerInfo()
	{
		var name = $('#namelicenseModal').val();
		var cid = $('#cidlicenseModal').val();
		var learnerno = $('#learnerNolicenseModal').val();
		var regionId = $('#regionlicenseModal').val();
		
		if(name == "")
			name = "NA";
		if(cid == "")
			cid = "NA";
		if(learnerno == "")
			learnerno = "NA";
		if(regionId == "")
			regionId = "NA";
		
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&name="+name+"&cid="+cid+"&learnerno="+learnerno+"&region="+regionId+"&searchType=LEARNER_HISTORY",
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				
				$("#learnerInfoList").html(responseText);
				$("#learnerInfoList").show();
			}
		});
		
	}
	function searchPersonalInfo()
	{
		var name 		= $('#namePersonalModal').val();
		var cidNumber 	= $('#cidPersonalModal').val();
		var licenseNo	= $('#licenseNoModal').val();
		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&name="+name+"&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=LICENSE_HISTORY",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			async: true,
			success : function(responseText) 
			{
				$("#licenseDetailList").html(responseText);
				$("#licenseDetailList").show();
			}
		});
	} 
	function getLicenseHistory()
	{
		var drivinglicenseId	=	$("#drivinglicenseId").val();
		fetchRenewalHistory();
		fetchDuplicationHistory();
		fetchEndorsementHistory();
		fetchSuspensionHistory();
		fetchDriveTypeSuspensionHistory();
		fetchDrivingLicenseCancellationHistroy();
		fetchLearnerDuplication();
		fetchLeanerRenewalHistory();
		learnerLicenseDetails();
		licenseOffense('LICENSE',drivinglicenseId);
		searchsuspensionlist();
		getToolsDetails();
		
		
	} 
	function getLearnerHistory()
	{
		var learnerLicenseId	=	$("#learnerLicenseId").val();
		fetchLearnerDuplication();
		fetchLeanerRenewalHistory();
		learnerLicenseDetails();
		licenseOffense('LEARNER',learnerLicenseId);
	} 
	function fetchRenewalHistory()
	{
		var licenseNo = $('#licenseNo').val();
		var drivingLicenseId = $('#drivinglicenseId').val();
		if(drivingLicenseId == "")
			drivingLicenseId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getlicenserenewal&drivingLicenseId="+drivingLicenseId+"&licenseNo"+licenseNo+"&formType=SEARCH_FORM",
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			async: false,
			success : function(responseText) 
			{
				$("#renewal_history_list").html(responseText);
				$("#renewal_history_table").hide();
				$("#renewal_history_list").show();
			}
		});
	}
	
	function fetchDuplicationHistory()
	{
		var drivinglicenseId = $('#drivinglicenseId').val();
		if(drivinglicenseId == "")
			drivinglicenseId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getlicenseduplication&drivinglicenseId="+drivinglicenseId,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			async: false,
			success : function(responseText) 
			{ 
				$("#license_duplication_list").hide();
				$("#license_duplication_table").html(responseText);
				$("#license_duplication_list").show();
			}
		});
	}
	
	function fetchEndorsementHistory()
	{
		var drivinglicenseId = $('#drivinglicenseId').val();
		
		if(drivinglicenseId == "")
			drivinglicenseId = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getlicenseendorsementlists&drivinglicenseId="+drivinglicenseId,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			async: false,
			success : function(responseText) 
			{
				$("#license_endorse_table").hide();
				$("#license_endorse_list").html(responseText);
				$("#license_endorse_list").show();
				
				getdrivetype();
			}
		});
	}
	function fetchSuspensionHistory()
	{
		
			var drivinglicenseId = $('#drivinglicenseId').val();
			
			if(drivinglicenseId == "")
				drivinglicenseId = "NA";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getsuspensionlist&FORM_TYPE=DL&drivinglicenseId="+drivinglicenseId,
				async: true,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				async: false,
				success : function(responseText) 
				{
					$("#license_suspension_table").html(responseText)
					$("#license_suspension_list").show();;
					$("#license_suspension_list").show();

				}
			});
	}

	function fetchDriveTypeSuspensionHistory()
	{
			var drivinglicenseId = $('#drivinglicenseId').val();
			
			if(drivinglicenseId == "")
				drivinglicenseId = "NA";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getsearchdrivetypesuspensionlist&drivinglicenseId="+drivinglicenseId,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				async: false,
				success : function(responseText) 
				{
					$("#license_drivetype_suspension_table").hide();
					$("#license_drivetype_suspension_list").html(responseText);
					$("#license_drivetype_suspension_list").show();

				}
			});
	}
	function fetchDrivingLicenseCancellationHistroy()
	{
			var drivinglicenseId = $('#drivinglicenseId').val();
			
			if(drivinglicenseId == "")
				drivinglicenseId = "NA";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getLicenseCancellationHistory&drivinglicenseId="+drivinglicenseId,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				async: false,
				success : function(responseText) 
				{
					$("#license_cancellation_dtls").hide();
					$("#license_cancellation_list").html(responseText);
					$("#license_cancellation_list").show();

				}
			});
	}
	function fetchLeanerRenewalHistory()
	{
			var learnerLicenseId	=	$("#learnerLicenseId").val();
			if(learnerLicenseId == "")
				learnerLicenseId = "NA";
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getlearnerrenewallist&learnerLicenseId="+learnerLicenseId,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				async: false,
				success : function(responseText) 
				{
					$("#learner_renew_table").hide();
					$("#learner_renew_list").html(responseText);
					$("#learner_renew_list").show();
				}
			});
	}
	function fetchLearnerDuplication()
	{		
			var learnerLicenseId	=	$("#learnerLicenseId").val();
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getlearnerduplicationlist&learnerLicenseId="+learnerLicenseId,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				async: false,
				success : function(responseText) 
				{
					$("#learner_duplication_table").hide();
					$("#learner_duplication_list").html(responseText);
					$("#learner_duplication_list").show();
				}
			});
	}
	function learnerLicenseDetails()
	{		
			var learnerLicenseId	=	$("#learnerLicenseId").val();
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseDtls&learnerLicenseId="+learnerLicenseId,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				async: false,
				success : function(responseText) 
				{
					$("#learner_dtls_table").hide();
					$("#learner_dtls_list").html(responseText);
					$("#learner_dtls_list").show();
				}
			});
	}
	function licenseOffense(type,id)
	{	
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getOffenceHistoryList&type="+type+"&id="+id,
			data : $('form').serialize(),
			cache : false,
			async: false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#offence_dtls_table").hide();
				$("#offence_dtls_list").html(responseText);
				$("#offence_dtls_list").show();
			}
		});
	}

	function searchsuspensionlist()
	{
		var drivinglicenseId = $('#drivinglicenseId').val();
		
		if(drivinglicenseId == "")
			drivinglicenseId = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getsuspensionlist&FORM_TYPE=DL&drivinglicenseId="+drivinglicenseId,
			async: false,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#suspensionlistTable").html(responseText);
				$("#suspensionlistTable").show();

			}
		});
	}
	
	function getToolsDetails(customerId)
	{ 
		var customerId	=	$("#learnerCustomerId").val();
		$.ajax
		({
			type: 'POST',
			cache : false,
			dataType : "html",
			async: false,
			url: '<%=request.getContextPath()%>/tools.html?method=licensePunchList&customerId='+customerId,
			success: function(xml)
			{	 
				$("#LincePunchList").html(xml); 
				
			}
		});
	}
	
	function getdrivetype()
	{
		var customerId	=	$("#learnerCustomerId").val();

		$.ajax
		({
			
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getdrivinglicensedrivetype&customerId="+customerId,
			
			data : $('form').serialize(),
			cache : false,
			async: true,
			dataType : "html",
			success : function(responseText) 
			{
				
				$("#drivetypelist").html("<label class='control-label'>"+responseText+"</label>");
				$("#drivetypelist").show();
			}
		});
	}
	function searchPersonalDtls()
	{
		
		var cid	=	"NA";
		var name="NA";
		var customerId	=	$("#learnerCustomerId").val();
		var regionId	=	"NA";
		var dzongkhagId	=	"NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoForEdit&name="+name+"&cid="+cid+"&customerId="+customerId+"&region="+regionId+"&dzongkhag="+dzongkhagId+"&searchType=VEHICLE_REGISTRATION",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#commonListTable").html(responseText);
				$("#commonListTable").show();
			}
		});
	}
</script>

<script type="text/javascript">

		$(document).ready(function()
        {
	        $("#personalForm").validate
	        ({
				rules:
				{
					titleOfcourtesy:
					{
						required:true,
					},
					firstname:
					{
						required: true,
					},
					occupation:
					{
						required: true,
					},
					nationality:
					{
						required: true,
					},
					displayDob:
					{
						required: true,
					},
					fathersName:
					{
						required: true,
					},
					bloodGroup:
					{
						required: true,
					},
					presentDzongkhag:
					{
						required: true,
					},
					contactAddress:
					{
						required: true,
					},
					phone:
					{
						required: true,
						number: true,
					}
				},    
				messages:
				{
					titleOfcourtesy:{required: "Please Select Title of Courtesy"},
					firstname:{required: "Please Enter First Name"},
					occupation:{required: "Please Select Occupation"},
					nationality:{required: "Please Select Nationality"},
					displayDob:{required: "Please Enter Date of Birth"},
					fathersName:{required: "Please Enter Fathers name"},
					bloodGroup:{required: "Please Select Blood Group"},
					presentDzongkhag:{required: "Please Select present dzongkhag"},
					contactAddress:{required: "Please enter contact address"},
					phone:{required: "Please enter phone number"}
				}
			});
        });

	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	//radio button
	$(document).ready(function() {
		$('#nationalRadio').attr('checked', true);
		$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);
	});

	//radio button
	function enable(identifier)
	{
		if("National" == identifier)
		{
			$('#address').attr('disabled', true);
			$('#dzongkhag').attr('disabled', false);
			$('#gewog').attr('disabled', false);
			$('#village').attr('disabled', false);
			
			$('#address_div').hide();
			$('#dzongkhag_div').show();
			$('#gewog_div').show();
			$('#village_div').show();
		}
		else if("Official" == identifier)
		{
			$('#ministryDIV').show();
		    $('#departmentDIV').show();
			$('#ministry').attr('disabled', false);
			$('#department').attr('disabled', false);
		   
		}
		else if("Private" == identifier)
		{
		    $('#ministryDIV').hide();
		    $('#departmentDIV').hide();
		}
		else
		{
			$('#country').attr('disabled', false);
			$('#address').attr('disabled', false);
			$('#dzongkhag').attr('disabled', true);
			$('#gewog').attr('disabled', true);
			$('#village').attr('disabled', true); 
			
			$('#country_div').show();
			$('#address_div').show();
			$('#dzongkhag_div').hide();
			$('#gewog_div').hide();
			$('#village_div').hide();
		}
	}
</script>