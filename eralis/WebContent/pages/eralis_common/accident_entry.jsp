<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-timepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Motor Vehicle Accident Entry Form
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			report motor vehicle accident 
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row" id="messageDiv" style="display:none"></div>
<html:form styleClass="form-horizontal" styleId="accidentForm" action="/eralis_common.html?method=new_accident_entry">
	<div class="row">
		<div class="col-xs-12">
			<!-- GENERAL INFORMATION -->
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">General Information</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
							
								<div class="form-group">
									<div class="col-lg-3">
										<label>Name of Police Division <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="policeDivision" styleId="policeDivision" styleClass="form-control" onchange="populateDependentDropDown(this.value, 'policeStation', '', 'GET_POLICE_STATION_LIST_AGAINST_DIV', 'N')">
										      <html:option value="">--SELECT--</html:option>
								   		      <html:optionsCollection name="policeDivisionList" label="headerName" value="headerId"/>
								    	</html:select>
								    	<span class="text-danger required" id="policeDivisionValidation">Please Enter the Data</span>
									</div>
									<div class="col-lg-3">
										<label>Name of Police Station <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="policeStation" styleId="policeStation" styleClass="form-control" >
										      <html:option value="">--SELECT--</html:option>
								    	</html:select>
								    	<span class="text-danger required" id="policeStationValidation">Please Enter the Data</span>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Date of Accident <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="dateOfOccurrence" styleId="dateOfOccurrence" styleClass="form-control date-picker" ></html:text>
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
										</div>
										<span class="text-danger required" id="dateOfOccurrenceValidation">Please Enter the Data</span>
									</div>
									<div class="col-lg-3">
										<label>Time of Accident :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group bootstrap-timepicker">
											<html:text property="timeOfOccurrence" styleClass="form-control timepicker" styleId="timeOfOccurrence"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
										<%-- <html:text property="timeOfOccurrence"  styleClass="form-control"></html:text> --%>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Date of Report <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="dateOfReport" styleId="dateOfReport" styleClass="form-control date-picker"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
										</div>
										<span class="text-danger required" id="dateOfReportValidation">Please Enter the Data</span>
									</div>
									<div class="col-lg-3">
										<label>Time of Report :</label>
									</div>
									<div class="col-lg-3">
										
										<div class="input-group bootstrap-timepicker">
											<html:text property="timeOfReport" styleClass="form-control timepicker" styleId="timeOfReport"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
										<%-- <html:text property="timeOfReport" styleClass="form-control"></html:text> --%>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Place of Accident <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="placeOfOccurrence" styleId="placeOfOccurrence"  styleClass="form-control"></html:text>
										<span class="text-danger required" id="placeOfOccurrenceValidation">Please Enter the Data</span>
									</div>
									<div class="col-lg-3">
										<label>Dzongkhag <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="dzongkhag" styleId="dzongkhag" styleClass="form-control" onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N')">
										      <html:option value="">--SELECT--</html:option>
								   		      <html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
								    	</html:select>
								    	<span class="text-danger required" id="dzongkhagValidation">Please Enter the Data</span>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Gewog <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="gewog" styleId="gewog" styleClass="form-control" >
										      <html:option value="">--SELECT--</html:option>
								    	</html:select>
								    	<span class="text-danger required" id="gewogValidation">Please Enter the Data</span>
									</div>
									<div class="col-lg-3">
										<label>Village :</label>
									</div>
									<div class="col-lg-3">
										<html:text property="village" styleClass="form-control"></html:text>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Informer Type <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="informerType" styleId="informerType" styleClass="form-control" >
										      <html:option value="">--SELECT--</html:option>
										      <html:optionsCollection name="informerList" label="headerName" value="headerId"/>
								    	</html:select>
								    	<span class="text-danger required" id="informerTypeValidation">Please Enter the Data</span>
									</div>
									<div class="col-lg-3">
										<label>Name of Informer :</label>
									</div>
									<div class="col-lg-3">
										<html:text property="informerName" styleClass="form-control"></html:text>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label>Distance between Place of Accident and Police Station :</label>
									</div>
									<div class="col-lg-3">
										<html:text property="distance" styleClass="form-control"></html:text>
									</div>
									<div class="col-lg-3">
										<label>Arrival Date at Place of Accident :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="arrivalDate" styleClass="form-control date-picker"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Arrival Time at Place of Accident :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group bootstrap-timepicker">
											<html:text property="arrivalTime" styleClass="form-control timepicker" styleId="arrivalTime"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
										<%-- <html:text property="arrivalTime" styleClass="form-control"></html:text> --%>
									</div>
									<div class="col-lg-3">
										<label>Remarks :</label>
									</div>
									<div class="col-lg-3">
										<html:textarea property="remarks" styleClass="form-control"></html:textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- DRIVER AND VEHILCE DTLS-->
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Driver and Vehicle Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							
							<div class="col-lg-12 margin-bottom-10">
								<div class="form-group">
									<div align="center" id="driverVehicleDtls">
										<div id="driverVehicleTable">
											<table id="driver_dtls" class="table table-striped table-bordered table-hover">
												<thead>
											        <tr>
											            <th colspan="4">Driver and Vehicle Details</th>
											        </tr>
											    </thead>
												<nested:iterate id="accidentDriver" property="accidentDriverList" name="commonForm"  indexId="index">
													<nested:nest property="accidentDriver">
														<tr>
															<td class="col-lg-2">Driver's Nationailty <span class="text-danger">*</span></td>
															<td>
																<nested:select property="nationality" name="accidentDriver" onchange="getLicenseDtls(this.id)" styleId="nationality_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:optionsCollection name="countryList" label="headerName" value="headerId"/>
																</nested:select>
																<span class="text-danger required" id="nationalityValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">License/Learner No </td>
															<td><nested:text property="licenseNo" onchange="getLicenseDtls(this.id)"  styleId="licenseNo_0" name="accidentDriver" styleClass="form-control" indexed="true" ></nested:text></td>
														</tr>
														<tr>
															<td class="col-lg-2">Driver's Name <span class="text-danger">*</span></td>
															<td><nested:text property="name"  styleId="name_0" name="accidentDriver" styleClass="form-control" indexed="true" ></nested:text>
																<span class="text-danger required" id="nameValidation_0">Please Enter the Data</span>
															</td>
															
															<td class="col-lg-2">CID/Passport </td>
															<td><nested:text property="CID"  styleId="CID_0" name="accidentDriver" styleClass="form-control" indexed="true" ></nested:text></td>
															</tr>
														
														<tr>
															<td class="col-lg-2">DOB </td>
															<td>
																<div class="input-group">
																	<nested:text property="DOB"  styleId="dob_0" name="accidentDriver" styleClass="form-control date-picker" indexed="true" ></nested:text>
																	<span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>
																</div>
															
															</td>
															<td class="col-lg-2">Mobile No</td>
															<td><nested:text property="mobileNo"  styleId="mobileNo_0" name="accidentDriver" styleClass="form-control" indexed="true" ></nested:text></td>
														</tr>
														<tr>
															<td class="col-lg-2">Sex <span class="text-danger">*</span></td>
															<td>
																<nested:select property="gender" name="accidentDriver" styleId="gender_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:option value="M">Male</html:option>
																	<html:option value="F">Female</html:option>
																</nested:select>
																<span class="text-danger required" id="genderValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Alcohol Content(If Drunk)</td>
															<td>
																<nested:text property="alcoholContent"  name="accidentDriver" styleClass="form-control" indexed="true" ></nested:text>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Is Drug Abuse <span class="text-danger">*</span></td>
															<td>
																<nested:select property="isDrugAbuse" name="accidentDriver" styleClass="form-control" indexed="true">
																	<html:option value="N">No</html:option>
																	<html:option value="Y">Yes</html:option>
																</nested:select>
															</td>
															
														</tr>
														<tr>
															<td class="col-lg-2">Vehicle Origin <span class="text-danger">*</span></td>
															<td>
																<nested:select property="vehicleOrigin" name="accidentDriver"  onchange="getVehicleDtls(this.id)"  styleId="vehicleOrigin_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:optionsCollection name="countryList" label="headerName" value="headerId"/>
																</nested:select>
																<span class="text-danger required" id="vehicleOriginValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Vehicle No <span class="text-danger">*</span></td>
															<td><nested:text property="vehicleNo"  styleId="vehicleNo_0" name="accidentDriver" onchange="getVehicleDtls(this.id)" styleClass="form-control" indexed="true" ></nested:text>
																<span class="text-danger required" id="vehicleNoValidation_0">Please Enter the Data</span>
															</td>
														</tr>
														
														<tr>
															<td class="col-lg-2">Vehicle Type <span class="text-danger">*</span></td>
															<td>
																<nested:select property="vehicleType" name="accidentDriver" onchange="getVehicleDtls(this.id)" styleId="vehicleType_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
																</nested:select>
																<span class="text-danger required" id="vehicleTypeValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Vehicle Company <span class="text-danger">*</span></td>
															<td>
																<nested:select property="company" name="accidentDriver" styleId="company_0" styleClass="form-control" indexed="true" onchange="populateDependentDropDown(this.value, 'model_0', '', 'VECHILE_MODEL_LIST', 'N')">
																	<html:option value="">SELECT</html:option>
																	<html:optionsCollection name="vehicleCompanyList" label="headerName" value="headerId"/>
																</nested:select>
																<span class="text-danger required" id="companyValidation_0">Please Enter the Data</span>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Vehicle Model <span class="text-danger">*</span></td>
															<td>
																<nested:select property="model" name="accidentDriver" styleId="model_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																</nested:select>
																<span class="text-danger required" id="modelValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Owner Type <span class="text-danger">*</span></td>
															<td>
																<nested:select property="owner" name="accidentDriver" styleId="owner_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:option value="GOV">Government</html:option>
																	<html:option value="PVT">Private</html:option>
																	<html:option value="TAXI">Taxi</html:option>
																	<html:option value="AF">Armed Force</html:option>
																	<html:option value="OTHERS">Others</html:option>
																</nested:select>
																<span class="text-danger required" id="ownerValidation_0">Please Enter the Data</span>
															</td>
														</tr>
														
													</nested:nest>
												</nested:iterate>
											</table>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<button type="button" class="btn btn-success btn-xs" onclick="addMoreVehDriverDtls()">
										<i class="ace-icon fa fa-plus"></i>Add More Dtls
									</button>
									<button type="button" class="btn btn-danger btn-xs" onclick="deleteRow()">
										<i class="ace-icon fa fa-minus"></i>Delete
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ACCIDENT TYPE-->
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Accident Type</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-3">
										<input type="radio" id='accidentType_1' value="MVA-Self" onClick="checkAccidentType('1')">
										<label>(a) MVA-Self</label>
									</div>
									<div class="col-lg-3">
										<input type="radio" id='accidentType_2' value="MVA-Collision" onClick="checkAccidentType('2')">
										<label>(b) MVA-Collision</label>
									</div>
									<div class="col-lg-3">
										<input type="radio" id='accidentType_3' value="MVA-Pedestrian/Cyclist" onClick="checkAccidentType('3')">
										<label>(c) MVA-Pedestrian/Cyclist</label>
									</div>
									<div class="col-lg-3">
										<input type="radio" id='accidentType_4' value="MVA-Hit and Run" onClick="checkAccidentType('4')">
										<label>(d) MVA-Hit and Run</label>
									</div>
									<div class="alert col-lg-12">
										<h5 class="text-danger required" id="accidentTypeValidation">Please Select accident type</h5>
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="box box-solid">
								    	<div class="box-header with-border">
								       		<h3 class="box-title text-danger" style=" font-weight:  bold; font-size: 19px;">Note</h3>
								     	</div>
								     	<div class="box-body">
								       		<blockquote style=" font-size: 13px;">
												<ul>
								         			<li><b>MVA-Self:</b> Going off road, collision with objects, collision with stationary/parked vehicle and collision with animals.</li>
													<li><b>MVA-Collision:</b> Collision with another moving vehicle.</li>
													<li><b>MVA-Pedestrian/Cyclist:</b> Collision involving pedestrian/Cyclist.</li>
													<li><b>MVA-Hit and Run:</b> Any collision that involve hit and run offence.</li>
								         			 
								         		</ul>
								       		</blockquote>
								     	</div>
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- NATURE OF ACCIDENT-->
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Nature of Accident</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-3">
										<input type="radio" value="Fatal Accident" id='accidentNature_1' onClick="checkAccidentNature(1)">
										<label>(a) Fatal Accident (Killed)</label>
									</div>
									<div class="col-lg-3">
										<input type="radio"  value="Non-Fatal Accident" id='accidentNature_2' onClick="checkAccidentNature(2)">
										<label>(b) Non-Fatal Accident (Injured)</label>
									</div>
									<div class="col-lg-3">
										<input type="radio"  value="Both A and B" id='accidentNature_4' onClick="checkAccidentNature(4)">
										<label>(c) Both A and B</label>
									</div>
									<div class="col-lg-3">
										<input type="radio" value="None" id='accidentNature_3' onClick="checkAccidentNature(3)">
										<label>(d) None</label>
									</div>
									<div class="alert col-lg-12">
										<h5 class="text-danger required" id="accidentNatureValidation">Please Select nature of accident</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- CAUSE OF ACCIDENT-->
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Probable Causes of Accident</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-3">
										<label>Driver Error</label>
									</div>
									<div class="col-lg-3">
										<label>Road Condition</label>
									</div>
									<div class="col-lg-3">
										<label>Weather Condition</label>
									</div>
									<div class="col-lg-3">
										<label>Mechanical Filure</label>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<% int count=0; %>
										<logic:notEmpty name="ACCIDENT_CAUSE_DE">
											<logic:iterate id="accident_de" name="ACCIDENT_CAUSE_DE">
												<div>
													<input type='checkbox' id='count_accident_cause_<%=count++ %>' value='<bean:write name="accident_de" property="headerId"/>'><bean:write name="accident_de" property="headerName"/>
												</div>
											</logic:iterate>
										</logic:notEmpty> 
									</div>
									<div class="col-lg-3">
										<logic:notEmpty name="ACCIDENT_CAUSE_RC">
											<logic:iterate id="accident_rc" name="ACCIDENT_CAUSE_RC">
												<div>
													<input type='checkbox' id='count_accident_cause_<%=count++ %>' value='<bean:write name="accident_rc" property="headerId"/>'><bean:write name="accident_rc" property="headerName"/>
												</div>
											</logic:iterate>
										</logic:notEmpty>
									</div>
									<div class="col-lg-3">
										<logic:notEmpty name="ACCIDENT_CAUSE_WC">
											<logic:iterate id="accident_wc" name="ACCIDENT_CAUSE_WC">
												<div>
													<input type='checkbox' id="count_accident_cause_<%=count++ %>" value='<bean:write name="accident_wc" property="headerId"/>'><bean:write name="accident_wc" property="headerName"/>
												</div>
											</logic:iterate>
										</logic:notEmpty>
									</div>
									<div class="col-lg-3">	
										<logic:notEmpty name="ACCIDENT_CAUSE_MF">
											<logic:iterate id="accident_mf" name="ACCIDENT_CAUSE_MF">
												<div>
													<input type='checkbox' id="count_accident_cause_<%=count++ %>" value='<bean:write name="accident_mf" property="headerId"/>'><bean:write name="accident_mf" property="headerName"/>
												</div>
											</logic:iterate>
										</logic:notEmpty>
										<input type='hidden' id='count_accident_cause' value="<%=count%>">
									</div>
								</div>
								<div class="alert col-lg-12">
									<h5 class="text-danger required" id="accidentCauseValidation">Please Select cause of accident</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- DETAILS OF PERSON INJURED-->
			<div class="widget-box" id="injuredDtlsForm">
				<div class="widget-header">
					<h5 class="widget-title lighter">Details of Person Injured in Accident</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="col-lg-12 form-group">
									<div id="injureDtls">
										<div id="injuredTable">
											<table class="table table-striped table-bordered table-hover">
												<thead>
											        <tr>
											            <th colspan="4">Injured Person Details</th>
											        </tr>
											    </thead>
												<nested:iterate id="injuredPerson" property="injuredPersonList" name="commonForm"  indexId="index">
													<nested:nest property="injuredPerson">
														<tr>
															<td class="col-lg-2">Nationailty <span class="text-danger">*</span></td>
															<td>
																<nested:select property="nationality" name="injuredPerson" styleId="injureNationality_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:optionsCollection name="countryList" label="headerName" value="headerId"/>
																</nested:select>
																<span class="text-danger required" id="injuredNationalityValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Name <span class="text-danger">*</span></td>
															<td><nested:text property="name"  styleId="injuredName_0" name="injuredPerson" styleClass="form-control" indexed="true" ></nested:text>
																<span class="text-danger required" id="injuredNameValidation_0">Please Enter the Data</span>
															
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">CID/Passport</td>
															<td><nested:text property="CID"  styleId="CID_0" name="injuredPerson" styleClass="form-control" indexed="true" ></nested:text></td>
															<td class="col-lg-2">Age</td>
															<td>
																<nested:text property="age"  styleId="age_0" name="injuredPerson"
																 styleClass="form-control" indexed="true" ></nested:text>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Mobile No</td>
															<td><nested:text property="mobileNo"  styleId="mobileNo_0" name="injuredPerson" styleClass="form-control" indexed="true" ></nested:text></td>
															<td class="col-lg-2">Sex <span class="text-danger">*</span></td>
															<td>
																<nested:select property="gender" name="injuredPerson"   styleId="injuredGender_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:option value="M">Male</html:option>
																	<html:option value="F">Female</html:option>
																</nested:select>
																<span class="text-danger required" id="injuredSexValidation_0">Please Enter the Data</span>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Address <span class="text-danger">*</span></td>
															<td><nested:text property="address"  styleId="injuredAddress_0" name="injuredPerson" styleClass="form-control" indexed="true" ></nested:text>
																<span class="text-danger required" id="injuredaddressValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Occupation</td>
															<td>
																<nested:select property="occupation" name="injuredPerson" styleClass="form-control" indexed="true">
																	<html:option value="0">SELECT</html:option>
																	<html:optionsCollection name="occupationList" label="headerName" value="headerId"/>
																</nested:select>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Injury Description </td>
															<td><nested:textarea property="injurydescription"   name="injuredPerson" styleClass="form-control" indexed="true" ></nested:textarea>
															</td>
														</tr>
													</nested:nest>
												</nested:iterate>
											</table>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<button type="button" class="btn btn-success btn-xs" onclick="addInjuredDtls()">
										<i class="ace-icon fa fa-plus"></i>Add More Dtls
									</button>
									<button type="button" class="btn btn-danger btn-xs" onclick="deleteRow('injured_dtls')">
										<i class="ace-icon fa fa-minus"></i>Delete
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- DETAILS OF PERSON KILLED-->
			<div class="widget-box" id="killedDtlsForm">
				<div class="widget-header">
					<h5 class="widget-title lighter">Details of Person Killed in Accident</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="col-lg-12 form-group">
									<div id="killedDtls">
										<div id="killedTable">
											<table class="table table-striped table-bordered table-hover">
												<thead>
											        <tr>
											            <th colspan="4">Person Details</th>
											        </tr>
											    </thead>
												<nested:iterate id="killedPerson" property="killedPersonList" name="commonForm"  indexId="index">
													<nested:nest property="killedPerson">
														<tr>
															<td class="col-lg-2">Nationailty <span class="text-danger">*</span></td>
															<td>
																<nested:select property="nationality" name="killedPerson" styleId="killedNationality_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:optionsCollection name="countryList" label="headerName" value="headerId"/>
																</nested:select>
																<span class="text-danger required" id="killedNationalityValidation_0">Please Enter the Data</span>
															</td>
															<td class="col-lg-2">Name <span class="text-danger">*</span></td>
															<td><nested:text property="name"  styleId="killedName_0" name="killedPerson" styleClass="form-control" indexed="true" ></nested:text>
																<span class="text-danger required" id="killedNameValidation_0">Please Enter the Data</span>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">CID/Passport</td>
															<td><nested:text property="CID"  styleId="CID_0" name="killedPerson" styleClass="form-control" indexed="true" ></nested:text></td>
															<td class="col-lg-2">Age</td>
															<td>
																<nested:text property="age"  styleId="dob_0" name="killedPerson" styleClass="form-control" indexed="true" ></nested:text>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Mobile No</td>
															<td><nested:text property="mobileNo"  styleId="mobileNo_0" name="killedPerson" styleClass="form-control" indexed="true" ></nested:text></td>
															<td class="col-lg-2">Sex <span class="text-danger">*</span></td>
															<td>
																<nested:select property="gender" name="killedPerson"   styleId="killedGender_0" styleClass="form-control" indexed="true">
																	<html:option value="">SELECT</html:option>
																	<html:option value="M">Male</html:option>
																	<html:option value="F">Female</html:option>
																</nested:select>
																<span class="text-danger required" id="killedGenderValidation_0">Please Enter the Data</span>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Address <span class="text-danger">*</span></td>
															<td><nested:text property="address"  styleId="killedAddress_0" name="killedPerson" styleClass="form-control" indexed="true" ></nested:text>
																<span class="text-danger required" id="killedAddressValidation_0">Please Enter the Data</span>
															
															</td>
															<td class="col-lg-2">Occupation</td>
															<td>
																<nested:select property="occupation" name="killedPerson"  styleClass="form-control" indexed="true">
																	<html:option value="0">SELECT</html:option>
																	<html:optionsCollection name="occupationList" label="headerName" value="headerId"/>
																</nested:select>
															</td>
														</tr>
														<tr>
															<td class="col-lg-2">Injury Description </td>
															<td><nested:textarea property="injurydescription" name="killedPerson" styleClass="form-control" indexed="true" ></nested:textarea>
															</td>
														</tr>
													</nested:nest>
												</nested:iterate>
											</table>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<button type="button" class="btn btn-success btn-xs" onclick="addKilledDtls()">
										<i class="ace-icon fa fa-plus"></i>Add More Dtls
									</button>
									<button type="button" class="btn btn-danger btn-xs" onclick="deleteRow('injured_dtls')">
										<i class="ace-icon fa fa-minus"></i>Delete
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- NOTE-->
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Document:</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12"> 	
								<div class="form-group">
									<div class="col-lg-2">
										<label> Accident Photo 1 :</label>
									</div>
									<div class="col-lg-3">
										<html:file property="photo1" styleId="photo1" styleClass="form-control fileupload" onchange="validation('vehicleForm','invoiceTypeValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
										<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
									</div>
									
									<div class="col-lg-2">
										<label> Accident Photo 2 :</label>
									</div>
									<div class="col-lg-3">
										<html:file property="photo2" styleId="photo2" styleClass="form-control fileupload" onchange="validation('vehicleForm','invoiceTypeValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
										<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-2">
										<label> Accident Photo 3 :</label>
									</div>
									<div class="col-lg-3">
										<html:file property="photo3" styleId="photo3" styleClass="form-control fileupload" onchange="validation('vehicleForm','invoiceTypeValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
										<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
									</div>
									
									<div class="col-lg-2">
										<label> Accident Photo 4 :</label>
									</div>
									<div class="col-lg-3">
										<html:file property="photo4" styleId="photo4" styleClass="form-control fileupload" onchange="validation('vehicleForm','invoiceTypeValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
										<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="result"></div>
	<html:hidden property="driverDtls" styleId="driver_dtls_hidden"></html:hidden>
	<html:hidden property="vehilceDtls" styleId="vehilce_dtls_hidden"></html:hidden>
	<html:hidden property="injuredDtls" styleId="injured_dtls_hidden"></html:hidden>
	<html:hidden property="killedDtls" styleId="killed_dtls_hidden"></html:hidden>
	<html:hidden property="accidentCauseDtls" styleId="accident_cause_dtls_hidden"></html:hidden>
	<html:hidden property="accidentType" styleId="accident_type_hidden"></html:hidden>
	<html:hidden property="accidentNature" styleId="accident_nature_hidden"></html:hidden>
	<div class="alert alert-danger" id="missingDataMessage" style="display:none">Enter the above highlited missing data</div>
	<button type="button" class="btn btn-primary btn-sm" id="saveButton" onClick='formSubmit()'>Save Accident Record</button>
	
</html:form>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
 
<script>
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	});
	
	$('#timeOfOccurrence').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#timeOfOccurrence').timepicker('showWidget');
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	
	$('#timeOfReport').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#timeOfReport').timepicker('showWidget');
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	
	$('#arrivalTime').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#arrivalTime').timepicker('showWidget');
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});


	function checkAccidentNature(natureId)
	{
		for(var i=1;i<=4;i++)
		{
			$("#accidentNature_"+i).prop("checked", false);
		}
		$("#accidentNature_"+natureId).prop("checked", true);
		$("#accident_nature_hidden").val($("#accidentNature_"+natureId).val());
		
		if(natureId=='1')
		{
			$("#injuredDtlsForm").hide();
			$("#killedDtlsForm").show();
		}
		else if(natureId=='2')
		{
			$("#injuredDtlsForm").show();
			$("#killedDtlsForm").hide();
		}
		else if(natureId=='4')
		{
			$("#injuredDtlsForm").show();
			$("#killedDtlsForm").show();
		}
		else if(natureId=='3')
		{
			$("#injuredDtlsForm").hide();
			$("#killedDtlsForm").hide();
		}
	}
	
	var countVehDriverDtls = 0;
	function addMoreVehDriverDtls()
	{
		var driverVehicleTable = $("#driverVehicleTable").html();
		for(var i= 0;i<25;i++)
		{
			driverVehicleTable = driverVehicleTable.replace("_0", "_"+(countVehDriverDtls+1));
			driverVehicleTable = driverVehicleTable.replace("[0]", "["+(countVehDriverDtls+1)+"]");
		}
		$("#driverVehicleDtls").append(driverVehicleTable);
		countVehDriverDtls++;
		
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});
	}

	var countinjuredDtls = 0;
	function addInjuredDtls()
	{
		var injuredTable = $("#injuredTable").html();
		for(var i= 0;i<22;i++)
		{
			injuredTable = injuredTable.replace("_0", "_"+(countinjuredDtls+1));
			injuredTable = injuredTable.replace("[0]", "["+(countinjuredDtls+1)+"]");
		}
		$("#injureDtls").append(injuredTable);
		countinjuredDtls++;
		
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});
	}

	var countKilledDtls = 0;
	function addKilledDtls()
	{
		var killedTable = $("#killedTable").html();
		for(var i= 0;i<22;i++)
		{
			killedTable = killedTable.replace("_0", "_"+(countKilledDtls+1));
			killedTable = killedTable.replace("[0]", "["+(countKilledDtls+1)+"]");
		}
		$("#killedDtls").append(killedTable);
		countKilledDtls++;

		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});
	}
	
	function getLicenseDtls(licensColId)
	{
		var dataArray = licensColId.split("_");
		var colId = dataArray[1];
		var countryName = $("#nationality_"+colId).find('option:selected').text();
		var licenseNo = $("#licenseNo_"+colId).val();
		

		$("#name_"+colId).attr('readonly', false);
		$("#CID_"+colId).attr('readonly', false);
		$("#dob_"+colId).attr('readonly', false);
		$("#mobileNo_"+colId).attr('readonly', false);
		$("#gender_"+colId).attr('readonly', false);
		
		if(countryName == 'Bhutan')
		{
			$.ajax
			({
				async: false,
				type: 'POST',
				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getLicensAndLearnerDtls&licenseNo='+licenseNo,
				success: function(xml)
				{
					$(xml).find('xml-response').each(function()
					{
						
						var cid = $(this).find('cid').text();
						var dob = $(this).find('dob').text();
						var mobileNo = $(this).find('mobileNo').text();
						var gender = $(this).find('gender').text();
						var name = $(this).find('name').text();

						$("#name_"+colId).val(name);
						$("#CID_"+colId).val(cid);
						$("#dob_"+colId).val(dob);
						$("#mobileNo_"+colId).val(mobileNo);
						$('#gender_'+colId).val(gender);

						if(cid!='null')
							$("#CID_"+colId).attr('readonly', true);
						if(name!='null')
							$("#name_"+colId).attr('readonly', true);
						if(dob!='null')
							$("#dob_"+colId).attr('readonly', true);
						if(mobileNo!='null')
							$("#mobileNo_"+colId).attr('readonly', true);
						if(gender!='null')
							$("#gender_"+colId).attr('readonly', true);
						
					});
				}
			});
			
		}
		
	}
	
	function getVehicleDtls(vehicleColId)
	{
		var dataArray = vehicleColId.split("_");
		var colId = dataArray[1];
		var vehicleNo = $("#vehicleNo_"+colId).val();
		var vehicleType = $("#vehicleType_"+colId).val();
		var vehicleOrigin = $("#vehicleOrigin_"+colId).find('option:selected').text();
		
		$("#model_"+colId).show();
		$("#company_"+colId).show();
	
		$("#vehicleCompany_"+colId).hide();
		$("#vehicleModel_"+colId).hide();
		
		if(vehicleOrigin=="Bhutan")
		{
			$.ajax
			({
				async: false,
				type: 'POST',
				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getVehicleDtls&vehicleNo='+vehicleNo+'&vehicleType='+vehicleType,
				success: function(xml)
				{
					$(xml).find('xml-response').each(function()
					{
						
						var model = $(this).find('model').text();
						var company = $(this).find('company').text();
	
						$("#company_"+colId).val(company);
						populateDependentDropDown(company, 'model_'+colId, '', 'VECHILE_MODEL_LIST', 'N');
						$("#model_"+colId).val(model);
					});
				}
			});
			
		}
	}
</script>
<script>
	
	
	$( document ).ready(function() {
		$(".required").hide();
	});
	
	
	function checkAccidentType(typeId)
	{
		for(var i=1;i<=4;i++)
		{
			$("#accidentType_"+i).prop("checked", false);
		}
		$("#accidentType_"+typeId).prop("checked", true);
		$("#accident_type_hidden").val($("#accidentType_"+typeId).val());

	}

	
	
	function formSubmit()
	{		
		var accident_cause	=	"0";
		var count_de	=	$("#count_accident_cause").val();
		for(var i=0;i<count_de;i++)
		{
			if($("#count_accident_cause_"+i).prop('checked')==true)
			{
				accident_cause	=	accident_cause+"#"+$("#count_accident_cause_"+i).val();
			}
		}
		$("#accident_cause_dtls_hidden").val(accident_cause);
		
	
		var policeDivision = $("#policeDivision").val();
		var policeStation = $("#policeStation").val();
		var dateOfOccurrence = $("#dateOfOccurrence").val();
		var dateOfReport = $("#dateOfReport").val();
		var placeOfOccurrence = $("#placeOfOccurrence").val();
		var dzongkhag = $("#dzongkhag").val();
		var gewog = $("#gewog").val();
		var accident_type_hidden = $("#accident_type_hidden").val();
		var accident_nature_hidden = $("#accident_nature_hidden").val();
		var accident_cause_dtls_hidden = $("#accident_cause_dtls_hidden").val();
		var informerType = $("#informerType").val();
		
		$(".required").hide();

		var isValid = 1;

		
		
		if(informerType=="")
		{
			$("#informerTypeValidation").show();
			isValid = 0;
		}
		if(placeOfOccurrence=="")
		{
			$("#placeOfOccurrenceValidation").show();
			isValid = 0;
		}
		if(accident_type_hidden=="")
		{
			$("#accidentTypeValidation").show();
			isValid = 0;
		}
		if(accident_nature_hidden=="")
		{
			$("#accidentNatureValidation").show();
			isValid = 0;
		}
		if(accident_cause_dtls_hidden=="0")
		{
			$("#accidentCauseValidation").show();
			isValid = 0;
		}
		
		
		
		if(policeDivision=="")
		{
			$("#policeDivisionValidation").show();
			isValid = 0;
		}
		if(policeStation=="")
		{
			$("#policeStationValidation").show();
			isValid = 0;
		}
		if(dateOfOccurrence=="")
		{
			$("#dateOfOccurrenceValidation").show();
			isValid = 0;
		}
		if(dateOfReport=="")
		{
			$("#dateOfReportValidation").show();
			isValid = 0;
		}
		if(dzongkhag=="")
		{
			$("#dzongkhagValidation").show();
			isValid = 0;
		}
		if(gewog=="")
		{
			$("#gewogValidation").show();
			isValid = 0;
		}
		
		for(var i=0;i<=countVehDriverDtls;i++)
		{
			var nationalityValidation = $("#nationality_"+i).val();
			var nameValidation = $("#name_"+i).val();
			var genderValidation = $("#gender_"+i).val();
			var vehicleOriginValidation = $("#vehicleOrigin_"+i).val();
			var vehicleNoValidation = $("#vehicleNo_"+i).val();
			var companyValidation = $("#company_"+i).val();
			var vehicleTypeValidation = $("#vehicleType_"+i).val();
			var modelValidation = $("#model_"+i).val();
			var ownerValidation = $("#owner_"+i).val();

			if(modelValidation=="")
			{
				$("#modelValidation_"+i).show();
				isValid = 0;
			}
			if(ownerValidation=="")
			{
				$("#ownerValidation_"+i).show();
				isValid = 0;
			}
			if(nationalityValidation=="")
			{
				$("#nationalityValidation_"+i).show();
				isValid = 0;
			}
			if(nameValidation=="")
			{
				$("#nameValidation_"+i).show();
				isValid = 0;
			}
			if(genderValidation=="")
			{
				$("#genderValidation_"+i).show();
				isValid = 0;
			}
			if(vehicleOriginValidation=="")
			{
				$("#vehicleOriginValidation_"+i).show();
				isValid = 0;
			}
			if(vehicleNoValidation=="")
			{
				$("#vehicleNoValidation_"+i).show();
				isValid = 0;
			}
			if(companyValidation=="")
			{
				$("#companyValidation_"+i).show();
				isValid = 0;
			}
			if(vehicleTypeValidation=="")
			{
				$("#vehicleTypeValidation_"+i).show();
				isValid = 0;
			}
		}
		//countinjuredDtls
		for(var i=0;i<=countinjuredDtls;i++)
		{
			var nationalityValidation = $("#injureNationality_"+i).val();
			var nameValidation = $("#injuredName_"+i).val();
			var genderValidation = $("#injuredGender_"+i).val();
			var addressValidation = $("#injuredAddress_"+i).val();

			if(nationalityValidation!="")
			{
				if(nameValidation=="")
				{
					$("#injuredNameValidation_0"+i).show();
					isValid = 0;
				}
				if(genderValidation=="")
				{
					$("#injuredSexValidation_"+i).show();
					isValid = 0;
				}
				if(addressValidation=="")
				{
					$("#injuredaddressValidation_"+i).show();
					isValid = 0;
				}
			}
			
		}

		//countKilledDtls
		for(var i=0;i<=countKilledDtls;i++)
		{
			var nationalityValidation = $("#killedNationality_"+i).val();
			var nameValidation = $("#killedName_"+i).val();
			var genderValidation = $("#killedGender_"+i).val();
			var addressValidation = $("#killedAddress_"+i).val();

			if(nationalityValidation!="")
			{
				if(nameValidation=="")
				{
					$("#killedNameValidation_"+i).show();
					isValid = 0;
				}
				if(genderValidation=="")
				{
					$("#killedGenderValidation_"+i).show();
					isValid = 0;
				}
				if(addressValidation=="")
				{
					$("#killedAddressValidation_"+i).show();
					isValid = 0;
				}
			}
		}
		
		
		if(isValid==1)
		{
			$("#missingDataMessage").hide();
			$("#saveButton").hide();
			var options = {target:'#result',usrl:context+'/eralis_common.html?method=new_accident_entry',type:'POST',data: $("#accidentForm").serialize()}; 
		    $("#accidentForm").ajaxSubmit(options);
	       	$('#result').show();
	       // setTimeout('hideStatus("result")',10000);
		}
		else
		{
			$("#missingDataMessage").show();
		}
		
	}
</script>
			
          

