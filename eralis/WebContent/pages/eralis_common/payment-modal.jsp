<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<style>
	.datepicker{z-index:1151 !important;}
</style>
<div id="payment-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-large">
								<h3 class="widget-title grey lighter">
									<i class="ace-icon fa fa-money orange"></i>
										Payment Details
								</h3>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-24">
									<div>
										<table class="table table-striped table-bordered" id="normal" style="display: none;">
											<tbody>
												<tr>
													<td>Amount:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" name="amount" class="form-control input-mask-phone" id="amount" disabled="disabled"/>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<table class="table table-striped table-bordered" id="transfer" style="display: none;">
											<tbody>
												<tr>
													<td style="width: 200px">Market Value:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" class="form-control input-mask-phone" id="marketValue" disabled="disabled"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Sale Deed Value:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" class="form-control input-mask-phone" id="saleDeedValue" disabled="disabled"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Value for Transfer Tax:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" class="form-control input-mask-phone" id="valueForTT" disabled="disabled"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Ownership Transfer Tax:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" class="form-control input-mask-phone" id="taxedAmount" disabled="disabled"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Transfer Fee & RC Cost:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" class="form-control input-mask-phone" id="transferFees" disabled="disabled"/>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<table class="table table-striped table-bordered" id="penaltyTR">
											<tbody>
												<tr>
													<td>Penalty:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" name="penalty" class="form-control input-mask-phone" id="penalty" disabled="disabled"/>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="hr hr8 hr-double hr-dotted"></div>
									<div class="row">
										<div class="col-sm-5 pull-left">
											<h4 class="pull-left">
												Total amount :
											</h4>
										</div>
										<div class="col-sm-5 pull-right">
											<div class="input-group">
												<span class="input-group-addon">
												   Nu.
												</span>
												<input class="form-control input-mask-phone" type="text" id="totalAmount" readonly="readonly">
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-tick"></i>
					Done
				</button>
			</div>
			
		</div>
	</div>
</div>