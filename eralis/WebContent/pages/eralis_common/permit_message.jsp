<%
	String message = (String)request.getAttribute("MESSAGE");
	String permitId = (String)request.getAttribute("PERMIT_ID");

	
	if(message.equalsIgnoreCase("SUCCESS"))
	{
%>
	<input type="hidden" id="permitId" value="<%=permitId%>"/>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 Route Permit details successfully saved.
		 <a href="#" class="btn btn-primary btn-sm" onclick="printReport()">
		 	<i class="ace-icon fa fa-print bigger-160"></i>
		 	Print Certificate
		 </a>
	</div>
<%
	}
	else if(message.equalsIgnoreCase("FAILURE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 Route Permit details couldnot be saved.
	</div>
<%
	}
%>

<script>

	function printReport()
	{
		var permitId = $('#permitId').val();

		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/eralis_common.html?method=print_permit_report&permitId="+permitId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				var mywindow = window.open('', 'my div', 'height=400,width=600');
				mywindow.document.write(responseText);

				setTimeout(function() {
					mywindow.print();
					mywindow.close();
				}, 400);
			}
		});
	}

</script>
