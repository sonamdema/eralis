<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="table-responsive">
	<table id="personal-search-table1" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<th>Customer Code</th>  
				<th>CID</th>
				<th>Name</th>
				<th>Blood Group</th>
				<th>Region</th>
				<th>Permanent Dzongkhag</th>
				<th>Permanent Gewog</th> 
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="PERSONAL_LIST">
				<logic:iterate id="personal" name="PERSONAL_LIST">
					<tr>
						<td>
							<!-- <input type="radio" name="personalListRadio" id='<bean:write name="personal" property="personalInfoId"/>' onclick="addSelected(this.id)"/> -->
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="personal" property="personalInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="personal" property="customerID"/></td>
						<td><bean:write name="personal" property="CID"/></td>
						<td><bean:write name="personal" property="name"/></td>
						<td><bean:write name="personal" property="bloodGroup"/></td>
						<td><bean:write name="personal" property="region"/></td>
						<td><bean:write name="personal" property="dzongkhag"/></td>
						<td><bean:write name="personal" property="gewog"/></td> 
					</tr>
				</logic:iterate>
			</logic:notEmpty>
			<logic:empty name="PERSONAL_LIST">
				<tr>
					<td colspan="8" align="center">
						<font color='red'>NO RECORD FOUND</font>
					</td>
				</tr>
			</logic:empty>
		</tbody>
	</table>
</div>
<!-- <div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div> -->

<script>
	$(document).ready(function() 
	{
   		$('#personal-search-table').DataTable({
            responsive: true
    	});
	});
		

	<%
		String searchType = (String)request.getAttribute("SEARCH_TYPE");
	%>
	var searchType = "<%=searchType%>";
	function getSelected(globalId)
	{	
		$("#originalCID").val('');
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtlsForEdit&personalInfoId='+globalId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					
				 	var guardianContactNo = $(this).find('guardianContactNo').text();
				 	var customerId = $(this).find('customerId').text();
				 	var cid = $(this).find('cid').text();
				 	var titleCourtesy = $(this).find('title-courtesy').text();
				 	var firstName = $(this).find('firstname').text();
				 	var middleName = $(this).find('middlename').text();
				 	var lastName = $(this).find('lastname').text();
				 	var occupation = $(this).find('occupation').text();
				 	var nationality = $(this).find('nationality').text();
				 	var dob = $(this).find('dob').text();
				 	var fatherName = $(this).find('father-name').text();
				 	var bloodGroup = $(this).find('blood-group').text();
				 	var gender = $(this).find('gender').text();
				 	var remarks = $(this).find('remarks').text();
				 	var isInternational = $(this).find('is-international').text();
				 	var dzongkhag = $(this).find('dzongkhag').text();
				 	var gewog = $(this).find('gewog').text();
				 	var village = $(this).find('village').text();
				 	var country = $(this).find('country').text();
				 	var address = $(this).find('address').text();
				 	var presentDzongkhag = $(this).find('present-dzongkhag').text();
				 	var contactAddress = $(this).find('present-address').text();
				 	var phone = $(this).find('phone').text();
				 	var email = $(this).find('email').text();
				 	var personalInfoId = $(this).find('personal-info-id').text();
				 	var imagePath = $(this).find('image-path').text();
				 	var contactRadio = $(this).find('contactRadio').text();
				 	var ministry = $(this).find('ministry').text();
				 	var department = $(this).find('department').text();
				 	var motherName = $(this).find('mother-name').text();
				 	
				 	populateDependentDropDown(dzongkhag, 'gewog', '', 'GEWOG_LIST', 'N');
				 	populateDependentDropDown(ministry, 'department', '', 'DEPARTMENT_LIST', 'N');
				 	$('#customerID').val(customerId);
				 	$('#guardianNo').val(guardianContactNo);
				 	
				 	//for Personal information cid
				 	$('#cid').val(cid);
				 	$('#newCustomerID').val(customerId);
				 	
					//for offence cidNo
					if(searchType=='searchPisForUpdateOffence')
					{
						$('#updateOffenceCID').val(cid);
						$('#updateOffenceCustomerID').val(customerId);
						$('#updateOffenceDriverName').text(firstName+" "+middleName+" "+" "+lastName);
						searchOffence("",customerId,"NON_DOCUMENT");
						
					}
					else if(searchType=='searchPisForOffence')
					{
						$('#offenceCID').val(cid);
					 	$('#offenceDriverName').text(firstName+" "+middleName+" "+" "+lastName);
					}
				 	
				 	$('#titleOfcourtesy').val(titleCourtesy);
				 	$('#firstname').val(firstName);
				 	$('#middlename').val(middleName);
				 	$('#lastName').val(lastName);
				 	$('#occupation').val(occupation);
				 	$('#nationality').val(nationality);
				 	$('#displayDob').val(dob);
				 	$('#fathersName').val(fatherName);
				 	$('#mothersName').val(motherName);
				 	$('#bloodGroup').val(bloodGroup);

				 	if(gender == "M")
					 	$('#maleRadio').attr('checked', true);
				 	else
				 		$('#femaleRadio').attr('checked', true);

			 		$('#remarks').val(remarks);

			 		if(isInternational == "N")
		 			{
			 			$('#nationalRadio').attr('checked', true);
			 			enable('National');
			 		}
			 		else
			 		{
			 			$('#internationalRadio').attr('checked', true);
			 			enable('International');
			 		}

		 			$('#dzongkhag').val(dzongkhag);
		 			$('#gewog').val(gewog);
		 			$('#village').val(village);
		 			$('#country').val(country);

		 			if(address == "" || address == "null")
		 				$('#address').val("");
		 			else
		 				$('#address').val(address);
	 				
		 			$('#presentDzongkhag').val(presentDzongkhag);
		 			$('#contactAddress').val(contactAddress);
		 			$('#phone').val(phone);
		 			$('#email').val(email);

		 			if(contactRadio == "Official")
			 		{
				 		$('#officialRadio').attr('checked', true);
				 		$('#ministry').val(ministry);
			 			$('#department').val(department);
			 			enable('Official');
			 		}
			 		else
			 		{
			 			$('#privateRadio').attr('checked', true);
			 			enable('Private');
			 		}
			 			
		 			$('#personalInfoId').val(personalInfoId);
		 			$('#msgDiv').html('');

		 			if(imagePath != "null")
			 		{
		 				$('#pic').html('<span class="profile-picture"><img class="editable img-responsive" id="" style="width:150px; height: 150px;" src="<%=request.getContextPath()%>/ImageServlet?url='+imagePath+'"/></span>');
						$('#pic').show();
			 		}
		 			else
			 		{
		 				$('#pic').html('');
						$('#pic').hide();
				 	}

		 			$('#firstname').attr('readonly', false);
		 			$('#middlename').attr('readonly', false);
		 			$('#lastName').attr('readonly', false);
		 			$('#fathersName').attr('readonly', false);
		 			$('#village').attr('readonly', false);
		 			$('#country').attr('disabled', false);
		 			$('#address').attr('disabled', false);

		 			$('#labelCustomerName').html("<label class='control-label'>"+firstName+" "+middleName+" "+lastName+"</label>");
		 			$('#labelCustomerId').html("<label class='control-label'>"+customerId+"</label>");
		 			$('#labelCID').html("<label class='control-label'>"+cid+"</label>");
		 			$('#labelDOB').html("<label class='control-label'>"+dob+"</label>");
		 			$('#labelContactNo').html("<label class='control-label'>"+phone+"</label>");
		 			$('#labelGender').html("<label class='control-label'>"+gender+"</label>");
		 			pendingList(customerId);
				});
			}
		});
	}
</script>
