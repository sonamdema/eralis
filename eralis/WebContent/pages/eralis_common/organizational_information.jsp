<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/select2.min.css">
 

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Organizational Information
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			organizational information details
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
	<html:form styleClass="form-horizontal" action="/eralis_common.html" styleId="organizationInfo">
					<div class="widget-box">
						<div class="widget-header widget-header-small">
							<h5 class="widget-title lighter">Organizational Information</h5>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="col-lg-12">
									<logic:equal value="Y" name="priviledge" property="isEdit">
										<div class="form-group">
											<div class="col-lg-2">
												<label>Customer ID:</label>
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<html:text property="customerID" styleClass="form-control" styleId="customerID" readonly="true"></html:text>
													<span class="input-group-btn">
														<a href="#" title="Find/Filter Customer" class="btn btn-purple btn-sm" onclick="openModal('organizationModal')">
															<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
														</a>
													</span>
												</div>
											</div>
										</div>
									</logic:equal>
										<div class="form-group">
											<div class="col-lg-2">
												<label>Type<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select  property="type" styleId="typedropdown" onchange="changetextbox()"  style="width:100%;">
													<html:option value="">--SELECT--</html:option>
													<html:optionsCollection name="ownerTypeList"
														label="headerName" value="headerId" />
												</html:select>
											</div>
										</div>
										<div class="form-group" id="ministryDIV" style="display:none">
											<div class="col-lg-2">
												<label>Ministry:</label>
											</div>
											<div class="col-lg-3">
											<html:select  style="width:100%;"property="ministry" styleClass="" styleId="ministry" onchange="populateDependentDropDown(this.value, 'department', '', 'DEPARTMENT_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
											</html:select>
												
											</div>
										</div>
										<div class="form-group" id="departmentDIV" style="display:none">
											<div class="col-lg-2">
												<label>Department:</label>
											</div>
											<div class="col-lg-3">
												<html:select  style="width:100%;"property="department" styleClass="" styleId="department">
													<html:option value="0">--SELECT--</html:option>
												</html:select>
											</div>
										</div>
										<div class="form-group" id="privateDIV" style="display:none;">
											<div class="col-lg-2">
												<label>Private Company:</label>
											</div>
											<div class="col-lg-3">
												<html:select  style="width:100%;"property="privateCompany" styleClass="" styleId="privateCompany">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="privateCompanyList" label="headerName" value="headerId"/>
												</html:select>
											</div>
										</div>
										<div class="form-group" id="diplomatDiv" style="display:none;">
											<div class="col-lg-2">
												<label>Diplomat <span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select  style="width:100%;"property="diplomatId" styleClass="" styleId="diplomatId">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="diplomatList" label="headerName" value="headerId"/>
												</html:select>
												<labe class="error diplomatValitaionMessage" style="display:none">Please SELECT Diplomat</label>
											</div>
										</div>
										
										<div class="form-group" id="nameDIV" style="display:none;">
											<div class="col-lg-2">
												<label>Name<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:text property="name" styleId="name"
													styleClass="form-control" value=" "></html:text>
											</div>
										</div>
                                        
                                        <div class="form-group" id="regionDIV" style="display:none;">
											<div class="col-lg-2">
												<label>Region<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select  style="width:100%;"property="region" styleClass="" styleId="region" onchange="populateDependentDropDown(this.value, 'dzongkhag', '', 'DZONGKHAG_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
											</div>
										</div>
										  
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Dzongkhag<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select  style="width:100%;"property="dzongkhag" styleClass="" styleId="dzongkhag">
													<html:option value="">--SELECT--</html:option>
									   				<html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
												</html:select>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Address:</label>
											</div>
											<div class="col-lg-3">
												<html:textarea property="address" styleId="address"
													styleClass="form-cont rol"></html:textarea>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Phone<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:text property="phone" styleId="phone"
													styleClass="form-control"></html:text>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Email:</label>
											</div>
											<div class="col-lg-3">
												<html:text property="email" styleId="email"
													styleClass="form-control"></html:text>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Remarks:</label>
											</div>
											<div class="col-lg-3">
												<html:textarea property="remarks" styleId="remarks"
													styleClass="form-control"></html:textarea>
											</div>
										</div> 
									</div>
								</div>
								
							</div>
						</div>
					</div>	
					
				<div id="messageDiv"></div>
			
			<div class="pull-right">
				<html:hidden property="organisationInfoId" styleId="organisationInfoId"/>
				<logic:equal value="Y" name="priviledge" property="isNew">
					<button type="button" class="btn btn-sm btn-primary" id="submitBtn">Save</button>
				</logic:equal>
				<logic:equal value="Y" name="priviledge" property="isEdit">
					<button type="button" onclick="editOrganizationInfo()" id="editBtn" style="display: none;" class="btn btn-sm btn-primary">Update</button>
				</logic:equal>
			</div>
			
		</html:form>
	</div>
</div>

<div id="organizationModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
							
									<div class="form-group">
										<label>Code:</label>
										<span class="pull-right">
											<input type="text" id="codeOrganisationModal" placeholder="Code" style="width: 200px"  />
										</span>
									</div>
									
									<div class="form-group">
										<label>Name:</label>
										<span class="pull-right">
											<input type="text" id="nameOrganisationModal" placeholder="Name"  style="width: 200px" />
										</span>
									</div>
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchOrganisationInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="organisationListTable">
			</div>
		</div>
	</div>
</div>
<div id="delete-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Confirmation</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete the selected record?
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" onclick="deleteOrganizationInfo()" >
					<i class="ace-icon fa fa-check-square-o" ></i> Yes
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>	
<script type="text/javascript" src="<%=request.getContextPath() %>/js/select2.full.min.js"></script>
	<script type="text/javascript">

		$(function () {
		   $('select').select2();
		   $('select').width(100 + '%');
		});


	function changetextbox()
	{
	   	var selectedText = $('#typedropdown option:selected').text();
	   	if(selectedText == "Government")
		{
			$('#ministry').attr('disabled', false);
			$('#department').attr('disabled', false);
			$('#regionDIV').show();
			$('#nameDIV').hide();
		    $('#privateDIV').hide();
		    $('#ministryDIV').show();
		    $('#departmentDIV').show();
			$('#diplomatDiv').hide();
		}
	   	else if(selectedText == "Royal Family")
		{
		   $('#ministry').attr('disabled', true);
		   $('#department').attr('disabled', true);
		   $('#nameDIV').show();
		   $('#privateDIV').hide();
		   $('#regionDIV').hide();
		   $('#ministryDIV').hide();
		    $('#departmentDIV').hide();
			$('#diplomatDiv').hide();
		}
	   	else if(selectedText == "Private")
		{
			$('#ministry').attr('disabled', true);
			$('#department').attr('disabled', true);
			$('#nameDIV').hide();
			$('#privateDIV').show();
			$('#regionDIV').hide();
			$('#ministryDIV').hide();
	    	$('#departmentDIV').hide();
			$('#diplomatDiv').hide();
		}
	   	else if(selectedText == "Diplomats")
		{
		   $('#ministry').attr('disabled', true);
		   $('#department').attr('disabled', true);
		   $('#nameDIV').hide();
		   $('#diplomatDiv').show();
		   $('#privateDIV').hide();
		   $('#regionDIV').hide();
		   $('#ministryDIV').hide();
		    $('#departmentDIV').hide();
		}
	   
	}

	var context = "<%=request.getContextPath()%>";
	function searchOrganisationInfo()
	{  
		var code = $('#codeOrganisationModal').val();
		var name = $('#nameOrganisationModal').val();
	//	var type = $('#typeOrganisationModal').val();
	//	var regionId = $('#regionOrganisationModal').val();
	//	var dzongkhagId = $('#dzongkhagOrganisationModal').val();	
		
		if(code == "")
			code = "NA";
		if(name == "")
			name = "NA";
	//	if(type == "")
	//		type = "NA";
	//	if(regionId == "")
	//		regionId = "NA";
	//	if(dzongkhagId == "")
	//		dzongkhagId = "NA";
	
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getOrganisationInfoForEdit&code="+code+"&name="+name,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#organisationListTable").html(responseText);
				$("#organisationListTable").show();
			}
		});
	}
	
	$(document).ready(function()
	{
		$("#organizationInfo").validate({
		rules:
		{
			typedropdown:{
				required:true
			},
			dzongkhag:{
				required:true
			},
			address:{
				required:true
			},
			phone:{
				required:true
			},
			
			
			
		},    
		messages:
		{
			typedropdown:{required: "Please Select Type"},
			dzongkhag:{required: "Please Select Dzongkhag"},
			address:{required: "Please Enter Address"},
			phone:{required: "Please Enter Phone No."},
			
		}
	});

	$('#submitBtn').click(function()
	{
		var isAllFielValid = "YES";
		var organistaionType = $('#typedropdown option:selected').text();
	   	if(organistaionType == "Diplomats")
   		{
	   		$(".diplomatValitaionMessage").hide();
	   		if($("#diplomatId").val()=="0")
   			{
   				$(".diplomatValitaionMessage").show();
   				isAllFielValid = "No";
   			}
   		}
	
		if($('#organizationInfo').valid() && isAllFielValid=="YES") 
		{
			$('#submitBtn').attr('disabled',true);
			var options = {target:'#messageDiv',url:context+'/eralis_common.html?method=organization_info',type:'POST',data: $("#organizationInfo").serialize()}; 
		    $("#organizationInfo").ajaxSubmit(options);
	        $('#messageDiv').show();
	        setTimeout('hideStatus("messageDiv")',20000);
	        setTimeout('reloadPage()',20000);
		}
		else 
		{
			return false;
		}
	});
});

	function deleteOrganizationInfo()
	{
		var options = {target:'#messageDiv',url:context+'/eralis_common.html?method=delete_organization_info',type:'POST',data: $("#organizationInfo").serialize()}; 
	    $("#organizationInfo").ajaxSubmit(options);
        $('#messageDiv').show();
        setTimeout('hideStatus("messageDiv")',5000);
        setTimeout('reloadPage()',5000);
        $('#delete-modal').modal('hide');
	}

	function editOrganizationInfo()
	{
		var isAllFielValid = "YES";
		var organistaionType = $('#typedropdown option:selected').text();
	   	if(organistaionType == "Diplomats")
   		{
	   		$(".diplomatValitaionMessage").hide();
	   		if($("#diplomatId").val()=="0")
   			{
   				$(".diplomatValitaionMessage").show();
   				isAllFielValid = "No";
   			}
   		}
 	
		if($('#organizationInfo').valid() && isAllFielValid=="YES") 
		{
			var options = {target:'#messageDiv',url:context+'/eralis_common.html?method=edit_organization_info',type:'POST',data: $("#organizationInfo").serialize()}; 
		    $("#organizationInfo").ajaxSubmit(options);
	        $('#messageDiv').show();
	        setTimeout('hideStatus("messageDiv")',5000);
	        setTimeout('reloadPage()',5000);
		}
		else 
		{
			return false;
		}
	}

	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>
	
	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";

	
</script>
<style>
	#organizationInfo .error { color: red; }
</style>