<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

					
<div class="row" id="displayStatus" style="display:none;">
	<div class="widget-body">
		<div class="widget-main">
			<div class="col-lg-12">
				<div class="form-group">
					<div id="msgDIV" class="col-lg-12 alert alert-danger">
						<div id="msg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div id="license_history" class="col-lg-12">
	<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
				<button type="button" class="pull-right btn btn-purple btn-sm" onclick="openModal('edit_PIS')">
					View Personal Details
				</button>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label class="control-label">Customer ID:</label>
								</div>
								<div class="col-lg-4" id="displayCustomerID"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="displayCID"></div>
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDateOfBirth"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Phone Number:</label>
								</div>
								<div class="col-lg-4" id="displayPhoneNo"></div>
								<div class="col-lg-2">
									<label>Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"></div>
							</div>
								
							<div class="form-group">
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
							</div>
							<div id="nationalDIV">
								<h4>Permanent Address(National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Dzongkhag:</label>
									</div>
									<div class="col-lg-4" id="displayDzongkhag"></div>
									<div class="col-lg-2">
										<label class="control-label">Gewog:</label>
									</div>
									<div class="col-lg-4" id="displayGewog"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Village:</label>
									</div>
									<div class="col-lg-4" id="displayVillage"></div>
								</div>
							</div>
							<br>
							<div>
								<h4>Driving license Details</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Issue Date:</label>
									</div>
									<div class="col-lg-4" id="displayissue"></div>
									<div class="col-lg-2">
										<label class="control-label">Expiry Date:</label>
									</div>
									<div class="col-lg-4" id="displayexpiry"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Customer Drive Types:</label>
									</div>
									<div class="col-lg-4" id="drivetypelist"></div>
									<div class="col-lg-2">
										<label class="control-label">Amount:</label>
									</div>
									<div class="col-lg-4" id="initialAmount"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Receipt No:</label>
									</div>
									<div class="col-lg-4" id="initialReceiptNo"></div>
									<div class="col-lg-2">
										<label class="control-label">Receipt Date:</label>
									</div>
									<div class="col-lg-4" id="initialReceiptDate"></div>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-body">
				<div class="widget-main">
					<div id="accordion" class="accordion-style1 panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Learner License Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse1">
								<div class="panel-body" id="learner_dtls_table">
									<table id="learnerTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>License No</td>
												<td>Issue Date</td>
												<td>Expiry Date</td>
												<td>Drive Type</td>
												<td>Region</td>
											</tr>
										</thead>
									</table>
								</div>
								<div id="learner_dtls_list" class="panel-body"  ></div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp; Learner License Renewal Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse2">
								<div class="panel-body" id="learner_renew_table">
									<table id="learnerRenewalTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>License No</td>
												<td>Issue Date</td>
												<td>Expiry Date</td>
												<td>Region</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="learner_renew_list"></div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Learner License Replacement Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse3">
								<div class="panel-body" id="learner_duplication_table">
									<table id="learnerDuplicationTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>License No</td>
												<td>Issue Date</td>
												<td>Region</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="learner_duplication_list"></div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Driving License Renewal Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse5">
								<div id="renewal_history_table"  class="panel-body">
									<table id="noncommercialRenewalTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>License No</td>
												<td>Issue Date</td>
												<td>Expiry Date</td>
												<td>Region</td>
											</tr>
										</thead>
									</table>
								</div>
								<div id="renewal_history_list" class="panel-body">
								</div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Driving License Replacement Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse6">
								<div class="panel-body" id="license_duplication_table">
									<table id="noncommercialDuplicationTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>License No</td>
												<td>Issue Date</td>
												<td>Region</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="license_duplication_list"></div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Endorsement Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse7">
								<div class="panel-body" id="license_endorse_table">
									<table id="noncommercialDuplicationTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>License No</td>
												<td>Drive Type</td>
												<td>Issue Date</td>
												<td>Region</td>
											</tr>
										</thead>
									</table>
								</div>
								<div id="license_endorse_list"></div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse8">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Offence Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse8">
								<div class="panel-body" id="offence_dtls_table">
									<table  class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>Offence Date</td>
												<td>Offence1</td>
												<td>Offence2</td>
												<td>Offence3</td>
												<td>TIN No</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="offence_dtls_list"></div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse9">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Driving License Suspension Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse9">
								<div class="panel-body" id="license_suspension_table">
									<table id="noncommercialDuplicationTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>Offence Date</td>
												<td>Offence1</td>
												<td>Offence2</td>
												<td>Offence3</td>
												<td>TIN No</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="license_suspension_list"></div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse10">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Drive Type Suspension Details
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse10">
								<div class="panel-body" id="license_drivetype_suspension_table">
									<table id="noncommercialDuplicationTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>Offence Date</td>
												<td>Offence1</td>
												<td>Offence2</td>
												<td>Offence3</td>
												<td>TIN No</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="license_drivetype_suspension_list"></div>
								
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse11">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Drive License Cancellation History
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse11">
								<div class="panel-body" id="license_cancellation_dtls">
									<table id="noncommercialDuplicationTable" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Sl No</td>
												<td>Cancellation Date</td>
												<td>Cancellation Reason</td>
												<td>Status</td>
												<td>Withdrawn Date</td>
												<td>Withdrawn Reason</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="panel-body" id="license_cancellation_list"></div>
								
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse12">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp; License Suspension History
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse12">
								<div class="panel-body" id="suspensionlistTable"> 
     							</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse13">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Drive License Punch History
									</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="collapse13">
								<div class="panel-body" id="LincePunchList"> 
     							</div>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		$(document).ready(function() 
		{
		    $('.table').DataTable({
		         responsive: true
		    });
		});
</script>