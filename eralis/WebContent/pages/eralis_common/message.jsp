<%
	String message = (String)request.getAttribute("MESSAGE");
	
	if(message.equalsIgnoreCase("SUCCESS"))
	{
%>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 Action successfully performed. 
<% 
	 if(request.getAttribute("CUSTOMER_CODE") != null)
	{
		String customerCode = (String)request.getAttribute("CUSTOMER_CODE");
%>
		New organization code is <%=customerCode %>
<%
	}
%>
	</div>
<%
	}
	else if(message.equalsIgnoreCase("FAILURE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 Action couldn't be performed, please try again
	</div>
<%
	}
	if(message.equalsIgnoreCase("ACCIDENT_ENTRY_SUCCESS"))
	{
		
		String caseNo = (String)request.getAttribute("CASE_NO");
%>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 Accident Record has been successfully saved and Case No is : <b><%=caseNo %></b>
		 <a class="btn btn-primary btn-sm" href="#" onclick="loadPageDetails('redirect.html?q=common_accident_entry&amp;page_id=37')"><i class="menu-icon fa fa-caret-right"></i>Accident Entry</a>
	</div>
<%
	}
	if(message.equalsIgnoreCase("NOC_SUCCESS"))
	{
%>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 NOC has been issued successfully
	</div>
<%
	}
	else if(message.equalsIgnoreCase("NOC_FAILURE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		NOC couldn't be issued, please try again later
	</div>
<%
	}
	else if(message.equalsIgnoreCase("ALREADY_USED"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		This organization information has already been used to register a vehicle, thus cannot be deleted
	</div>
<%
	}
	else if(message.equalsIgnoreCase("ALREADY_USED_FOR_LEARNER"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		This personal information has already been used to issue a learner license, thus cannot be deleted
	</div>
<%
	}
	else if(message.equalsIgnoreCase("ALREADY_USED_FOR_DL"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		This personal information has already been used to issue a driving license, thus cannot be deleted
	</div>
<%
	}
	else if(message.equalsIgnoreCase("ALREADY_USED_FOR_VEHICLE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		This personal information has already been used to register a vehicle, thus cannot be deleted
	</div>
<%
	}
%>
<script>
	var newCID	=	'<%=request.getAttribute("duplicateCID")%>';
	var message	=	'<%=request.getAttribute("MESSAGE")%>';
	var formType	=	'<%=request.getAttribute("formType")%>';
	if(message=='SUCCESS' )
	{
		setTimeout('hideStatus("messageDiv")',10000);
        setTimeout('reloadPage()',10000);
	}
	 
	$('#cid').html("<label class='control-label'>"+newCID+"</label>");
	$('#duplicateCID').val(newCID);
	$('#originalCID').val('');
	$('#old_IID').val('');
	$('#newIID').val('');
	$('#endorse_date').val('');
</script>