<%
	String message = (String)request.getAttribute("MESSAGE");

	if(message.equalsIgnoreCase("SUCCESS"))
	{
%>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 Action successfully performed
	</div>
<%
	}
	else if(message.equalsIgnoreCase("FAILURE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 Action couldn't be performed, please try again
	</div>
<%
	}
%>