	 
	 
	
	<!-- basic scripts -->
	
	<!--[if !IE]> -->
	<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>
	<!-- <![endif]-->

	<!--[if IE]>
	<script src="<%=request.getContextPath() %>/js/jquery.1.11.1.min.js"></script>
	<![endif]-->

	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery.min.js'>"+"<"+"/script>");
	</script>
	<!-- <![endif]-->

 
	
	<script type="text/javascript">
		if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath() %>/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>
	
	<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>
	<script src="<%=request.getContextPath() %>/js/jqueryAjaxFormSubmit.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.form.js"></script>
	
	<!-- page specific plugin scripts -->
	<script src="<%=request.getContextPath() %>/js/jquery.easypiechart.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.blockUI.js"></script>
	
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"></script>
  	<script type="text/javascript" src="<%=request.getContextPath()%>/js/dataTables.bootstrap.min.js"></script>
	
	<!-- ace scripts -->
	<script src="<%=request.getContextPath() %>/js/ace-elements.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/ace.min.js"></script>

	<!-- inline scripts related to this page -->
	<script>
		jQuery(function($) {
			$('.easy-pie-chart.percentage').each(function(){
				var $box = $(this).closest('.infobox');
				var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
				var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
				var size = parseInt($(this).data('size')) || 50;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
			});
		});

	</script> 