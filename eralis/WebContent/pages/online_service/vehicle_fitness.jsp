<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
 <%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
	
	EralisCommonDTO dto = (EralisCommonDTO) request.getAttribute("EDTO");
	String fitnessId = (String) request.getAttribute("FITNESS_ID");
%> 

 <div class="row">
  <div class="col-lg-12">
   <html:form styleClass="form-horizontal" action="/service.html" styleId="serviceForm">
	<div class="row">
		<div id="renewalValidationMsg"></div>
		<div id="incompleteValidationMsg"></div>
		<div id="Msg"></div>
	</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owners Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-sm-2">
						 <label class=control-label>Owner Type:</label>
					</div>
                    <div class="col-sm-4"><label class=control-label><%=dto.getOwnerType()%></label></div>
				     <%-- <%
                    	if(dto.getOwnerType().equalsIgnoreCase("Personal")){
                    %>
				    <div class="col-sm-2">
						<label class=control-label>Citizen ID :</label>
					</div>
                    <div class="col-sm-3"><label class=control-label><%=dto.getCID()%></label></div> 
                    <%
                    	} else {
                    %>
                    <div class="col-sm-2">
						<label class=control-label>Owner ID :</label>
					</div>
                    <div class="col-sm-3"><label class=control-label><%=dto.getOwnerId()%></label></div> 
                    <%
                    	}
                    %> --%>
                    <div class="col-sm-2">
						<label class=control-label>Owner Name:</label>
					</div>
					<%
						if(dto.getOwnerType().equalsIgnoreCase("Personal")){
					%>
	               	<div class="col-sm-4"><label class=control-label><%=dto.getName()%></label></div>
	               	<%
						} else {
							if(dto.getOwnerTypeDesc().equalsIgnoreCase("GOVERNMENT")){
	               	%>
	               			<div class="col-sm-4"><label class=control-label><%=dto.getMinistry() %>,&nbsp;<%=dto.getDepartment() %></label></div>
	               	<%
							} else if(dto.getOwnerTypeDesc().equalsIgnoreCase("PRIVATE")){
					%>
							<div class="col-sm-4"><label class=control-label><%=dto.getPrivateCompany() %></label></div>
					<%
							} else {
			        %>					
							<div class="col-sm-4"><label class=control-label><%=dto.getOwner() %></label></div>
					<%
							}
						}
	               	%>
               </div> 
               <%-- <div class="form-group">
					
					<div class="col-sm-2">
						<label class=control-label>Phone:</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getPhone()%></label></div>
	             </div> --%>   
       		</div>
       	</div>
     </div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-left"  style="margin-bottom:50px">
			<html:hidden property="engineType" styleId="engineType" value="<%=dto.getEngineType() %>"></html:hidden>
			<html:hidden property="vehicleType" styleId="vehicleType" value="<%=dto.getVehicleType() %>"></html:hidden>
			<input type="hidden" id="vehicleTypeDesc" name="vehicleTypeDesc" value="<%=dto.getVehicleTypeDesc()%>"/>
			<input type="hidden"  id="vehicleRegistrationCode"  value="<%=dto.getVehicleRegistrationType()%>"/>
			<html:hidden property="loadCapacity" styleId="loadCapacity" value="<%=dto.getLoadCapacity() %>"></html:hidden>
			<html:hidden property="seatCapacity" styleId="seatCapacity" value="<%=dto.getSeatCapacity() %>"></html:hidden>
			<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower" value="<%=dto.getVehicleHorsePower() %>"></html:hidden>
			<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt" value="<%=dto.getVehicleKiloWatt() %>"></html:hidden>
			<html:hidden property="engineCC" styleId="engineCC" value="<%=dto.getEngineCC() %>"></html:hidden>
			<html:hidden property="customerId" styleId="customerID" value="<%=dto.getCustomerID() %>"></html:hidden>
			<html:hidden property="vehicleRegistrationType" styleId="ownerType" value=""></html:hidden>
			<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
			<html:hidden property="vehicleId" styleClass="form-control" value="<%=dto.getVehicleId() %>" styleId="vehicleId" ></html:hidden>
			<input type="hidden" id="fitnessId" name="fitnessId" value="<%=fitnessId%>"/>
		</div>
		</html:form>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});

	var ownerType	=	'<%=dto.getOwnerType() %>';
	if(ownerType=='Personal')
	{
		$('#ownerType').val("P");
	}
	else if(ownerType=='Organization')
    {
	    $('#ownerType').val("O");
	}

	function showAttachment(val)
	{
		if(val == "E")
			$('#attachmentDIV').show();
		else
			$('#attachmentDIV').hide();
	}

	function validation()
	{
		var vehicleNumber = $('#displayVehicleNumber').val();
		var vehicleRegionId	=	$("#vehicleRegionId").val();
		
		$("#regionValidation").hide(); 
		$("#attchValidation").hide();
		$("#vehicleNumberValidation").hide();
		
		if(vehicleNumber == "")
		{
			$('#vehicleNumberValidation').show();
			$('#vehicleNumberValidation').get(0).scrollIntoView();
			return "0";
		} 
		if(vehicleRegionId=="")
		{
 			$("#regionValidation").show(); 
 			$('#regionValidation').get(0).scrollIntoView();
 			return "0";
		}
	}
	
   var pageIdentifier = "<%=pageIdentifier%>";
   var pageId = "<%=pageId %>";
  		
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	});

   $('.fileupload').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
					
	function formSubmit()
	{
		var pageId = '68';
		var vehicleNo = $('#vehicleNo').val();
		var amount = $('#amount').val();
		var penalty = $('#penalty').val();
		var options = {target:'#online_receipt',url:context+'/service.html?method=vehicleOnlinePayment&formType=VEH_FITNESS&amount='+amount+'&penalty='+penalty+'&pageId='+pageId+'&vehicleNo='+vehicleNo,type:'POST',data: $("#serviceForm").serialize()}; 
	    $("#serviceForm").ajaxSubmit(options);
	}
				
	function paymentDtls()
	{
		var identityTypeId;
		var requestType = "VEHICLE";
		var serviceType = "RWC";
		var identityNo = $('#vehicleId').val();
		if( $("#engineType").val() == "Electric")
			identityTypeId = '0'; 
		else
			identityTypeId = $('#vehicleType').val();
		var loadingCapacity = $('#loadCapacity').val();
		var seatingCapacity = $('#seatCapacity').val();
		var vehicleHP = $('#vehicleHorsePower').val();
		var kilowatts = $('#vehicleKiloWatt').val();
		var engineCC = $('#engineCC').val();
		var purchaseDate = "";
		var saleDeedAmount = "";
		var saleDeedDate = "";
		getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate);
	}	
						
</script>