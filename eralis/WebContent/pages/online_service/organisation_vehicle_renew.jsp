<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>	
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<html:form styleClass="form-horizontal" action="/service.html" styleId="vehicleForm">
	<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Owner Details</h5>
			</div>						
		<div class="widget-body">
			<div class="widget-main">
         		<div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
		            <div class="col-lg-3" id="orgname">
                          	</div>
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
			        <div class="col-lg-3" id="displayPhone">
	                </div>
	         	</div>    
               	<h4>Address</h4>
                   <div class="form-group">
	            	<div class="col-lg-2">
						<label class=control-label>Dzongkhag:</label>
					</div>
			        <div class="col-lg-3" id="orgdzongkhagId">
                       </div>
                 			 
			    	 
                    		</div>
                 		<div class="form-group">
						<div class="col-lg-2">
						<label class=control-label>Village:</label>
						</div>
		  					<div class="col-lg-3" id="orgvillage">
                     		</div>  
                     	 <div class="col-lg-2">
							<label class=control-label>Address:</label>
						</div>
	               		<div class="col-lg-3" id="orgaddress">
	                    </div>
                 		</div>  
         			 
         				                    
		 	</div>
		</div>
  			</div>
    <div class="widget-box">
    	<div class="widget-header widget-header-small">
	    	<h5 class="widget-title lighter">Renewal Lists</h5>
       	</div>						
        <div class="widget-body">
	    	<div class="widget-main">
	            <table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Renewal Date</th>
							<th>Expiry Date</th> 
							<th>Receipt NO</th> 
							<th>Receipt Date</th> 
							
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="ORGANISATION_RENEWAL_HISTORY">
							<logic:iterate id="renewal" name="ORGANISATION_RENEWAL_HISTORY">
								<tr>
									<td><bean:write name="renewal" property="renewalDate"/></td>
									<td><bean:write name="renewal" property="expiryDate"/></td>
									
									<td><bean:write name="renewal" property="receiptNo"/></td> 
									<td><bean:write name="renewal" property="receiptDate"/></td> 
									
									
								</tr>
							</logic:iterate>
						</logic:notEmpty>
						<logic:empty name="ORGANISATION_RENEWAL_HISTORY">
							<tr>
								<td colspan="7" align="center">
									<font color='red'>NO RECORD FOUND</font>
								</td>
							</tr>
						</logic:empty>
					</tbody>
				</table>
           	</div>
		</div>
     </div>
     <div class="widget-box">
        <div class="widget-header widget-header-small">
	       <h5 class="widget-title lighter">Renewal Details</h5>
      	</div>						
       		<div class="widget-body">
     			<div class="widget-main">
                	<div class="form-group"> 
                    	<div class="col-lg-2">
							<label>Company Name :<span style="color: #ff0000">*</span> </label>
						</div>
                   		<div class="col-sm-3">
	                    	<div class="input-group">
								<html:text property="renewalDate"  styleId="orgVehicle_Company_Name"></html:text>
								 
							</div>
                  		</div>
	                    <div class="col-lg-2">
							<label>Vehicle Model :<span style="color: #ff0000">*</span> </label>
						</div>
	                    <div class="col-sm-3">
	                    	<div class="input-group">
								<html:text property="expiryDate"   styleId="orgVehicle_Model_Id"></html:text>
								 
							</div>
	                 	</div>
               		</div>
               		<div class="form-group"> 
                    	
                    	<div class="col-lg-2">
							<label>Engine CC :<span style="color: #ff0000">*</span> </label>
						</div>
                   		<div class="col-sm-3">
	                    	<div class="input-group">
								<html:text property="renewalDate"  styleId="orgEngine_CC"></html:text>
								 
							</div>
                  		</div>
                  		<div class="col-lg-2">
							<label>New Renewal Date :<span style="color: #ff0000">*</span> </label>
						</div>
                   		<div class="col-sm-3">
	                    	<div class="input-group">
								<html:text property="renewalDate" styleClass="form-control date-picker" styleId="orgRenewal_Date"></html:text>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
                  		</div>
	                    
               		</div>
                <div class="form-group"> 
                	<div class="col-lg-2">
							<label>Expiry Date :<span style="color: #ff0000">*</span> </label>
						</div>
	                    <div class="col-sm-3">
	                    	<div class="input-group">
								<html:text property="expiryDate" styleId="orgExpiry_Date"></html:text>
								 
							</div>
	                 	</div>
                	<div class="col-lg-2">
						<label>Amount :<span style="color: #ff0000">*</span> </label>
					</div>
                  	<div class="col-sm-3">
                       	<html:text property="renewalAmount" styleClass="form-control" styleId="orgdisplayAmount"></html:text>
                    </div>
                    
				</div>
                <div class="form-group"> 
                	<div class="col-lg-2">
						<label>Receipt No :<span style="color: #ff0000">*</span> </label>
					</div>
                    <div class="col-sm-3">
                    	<html:text property="receiptNo" styleClass="form-control" styleId="orgdisplayReceiptNo"></html:text>
                    </div>
                    <div class="col-lg-2">
						<label>Receipt Date :<span style="color: #ff0000">*</span> </label>
					</div>
                    <div class="col-sm-3">
                      	<div class="input-group">
							<html:text property="receiptDate" styleClass="form-control date-picker" styleId="orgdisplayReceiptDate"></html:text>
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>
                     </div>
                 	</div>
                 	<div class="form-group"> 
                   		<div class="col-lg-2">
							<label>Remarks :<span style="color: #ff0000">*</span> </label>
						</div>
                 	<div class="col-sm-3">
                       	<html:textarea property="remarks" styleClass="form-control" styleId="orgdatespan"></html:textarea>
                    </div>
                </div>    
             </div>
      	</div>
	</div>
	<div class="pull-left">
		<html:text property="customerId" styleId="customerID"></html:text>
		<html:text property="vehicleRegistrationType" styleId="ownerType"></html:text>
		
		<input type="text" value="<%=request.getAttribute("page_id")%>" id="pageId">	
		<button type="button" class="btn btn-primary btn-sm" onclick="formSubmit()">Save</button>
		
		<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
		<html:button property="delete" styleClass="btn btn-primary btn-sm">Delete</html:button>
		<html:button property="New" styleClass="btn btn-primary btn-sm">New</html:button>
		<html:button property="close" styleClass="btn btn-primary btn-sm">Close</html:button>
	</div>
</html:form>
<script>
$(' #id-input-file-2').ace_file_input({
	no_file : 'No File ...',
	btn_choose : 'Choose',
	btn_change : 'Change',
	droppable : false,
	onchange : null,
	thumbnail : false,
	whitelist:'png|jpg|jpeg',
	blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
});

//datepicker plugin
$('.date-picker').datepicker({
	autoclose : true,
	todayHighlight : true
})

//show datepicker when clicking on the icon
.next().on(ace.click_event, function() {
	$(this).prev().focus();
});


var firstname	=	"<%=request.getAttribute("firstName")%>";

var middleName	=	'<%=request.getAttribute("middleName")%>';
var lastName	=	'<%=request.getAttribute("lastName")%>';
var dzongkhagId	=	'<%=request.getAttribute("dzongkhagId")%>';
var address		=	'<%=request.getAttribute("address")%>';
var village		=	'<%=request.getAttribute("village")%>';
var Expiry_Date	=	'<%=request.getAttribute("Expiry_Date")%>';
var Vehicle_Company_Name=	'<%=request.getAttribute("Vehicle_Company_Name")%>';
var Vehicle_Model_Id	=	'<%=request.getAttribute("Vehicle_Model_Id")%>';
var Engine_CC	=	'<%=request.getAttribute("Engine_CC")%>';
var Renewal_Date	=	'<%=request.getAttribute("Renewal_Date")%>';

if(firstname=='null')
	firstname='';
if(middleName=='null')
	middleName='';
if(lastName=='null')
	lastName='';
if(dzongkhagId=='null')
	dzongkhagId='';
if(address=='null')
	address='';
if(village=='null')
	village='';
if(address=='null')
	address='';
if(Expiry_Date=='null')
	Expiry_Date='';
if(Vehicle_Company_Name=='null')
	Vehicle_Company_Name='';
if(Vehicle_Model_Id=='null')
	Vehicle_Model_Id='';
if(Engine_CC=='null')
	Engine_CC='';
var name	=	firstname+middleName+lastName;


$("#orgRenewal_Date").val(Renewal_Date);
$("#orgExpiry_Date").val(Expiry_Date);
$("#orgVehicle_Company_Name").val(Vehicle_Company_Name);
$("#orgVehicle_Model_Id").val(Vehicle_Model_Id);
$("#orgEngine_CC").val(Engine_CC);

$('#orgname').html("<label class='control-label'>"+name+"</label>"); 
$('#orgdzongkhagId').html("<label class='control-label'>"+dzongkhagId+"</label>"); 
$('#orgaddress').html("<label class='control-label'>"+address+"</label>"); 
$('#orgvillage').html("<label class='control-label'>"+village+"</label>"); 
</script>

		
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>