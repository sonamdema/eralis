<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
</head>
<body class="no-skin">
		<!-- header include -->
		<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
		<!-- ./header include -->
		<div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							 <div class="col-lg-8 row">
									<html:form styleId="searchForm" styleClass="form-horizontal" action="/service.html?method=getPersonalInfo">									<div class="widget-box">
										<div class="widget-header widget-header-small">
											<h5 class="widget-title lighter">Search Personal Information</h5>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group" > 
															<div class="col-lg-4">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID"><b> CID :  </b></label>
															</div> 
															<div class="col-lg-4">
							                                	<html:text property="cid" styleClass="form-control" styleId="cid" ></html:text>
															</div>
														</div>
														<div class="form-group" > 
															<div class="col-lg-4">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID"><b> Date of Birth :  </b></label>
															</div> 
															<div class="col-lg-4">
															
																<div class="input-group">
																	<html:text property="dob" styleId="dob" styleClass="form-control date-picker" ></html:text>
																	<span class="input-group-addon"> <i
																		class="fa fa-calendar bigger-110"></i> </span>
																</div>
															</div>  
														</div>
														<div class="form-group" > 
															<div class="col-lg-12">
																<button type="button" class="btn btn-sm  btn-primary" onclick="getPersonalInfo()">
																	<i class="ace-icon fa fa-search"></i>
																	Search
																</button>
															</div>  		
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</html:form>
							</div>
							<div id="personalDetailsDiv" class="col-lg-8 row">
									 
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<!-- footer include -->
			<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
			<!-- /.footer include -->
			
		</div><!-- /.main-container -->
		
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript">
<!--
//-->
</script>
<script type="text/javascript">
	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

 
		 
</script>
<script><!--
function getPersonalInfo()
{	
	var cid	=	$("#cid").val();
	var dob=	$("#dob").val();
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/service.html?method=getPersonalInfo&cid="+cid+"&dob="+dob+"&pageType=editPersonalDetails",
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#personalDetailsDiv").html(responseText);
			$("#personalDetailsDiv").show();
			
		}
	});
} 
function editPersonalInfo()
{	 
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/service.html?method=editPersonalInfo",
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			
			$("#personalDetailsDiv").html(responseText);
			$("#personalDetailsDiv").show();
			
		}
	});
	getPersonalInfo();
}
</script>
	</body>
</html>