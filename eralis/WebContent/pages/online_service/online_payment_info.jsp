<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="col-sm-10 col-sm-offset-1">
	<div class="widget-box transparent">
		<div class="widget-header widget-header-large">
			<h3 class="widget-title grey lighter">
				<i class="ace-icon fa fa-money orange"></i>
					Payment Details
			</h3>
		</div>
		<div class="widget-body">
			<div class="widget-main padding-24">
				<div style="overflow: auto;">
					<table class="table table-striped table-bordered" id="normal" style="min-width:300px" >
						<tbody>
							<tr>
								<td>Amount:</td>
								<td>
									<div class="input-group">
										<span class="input-group-addon">
										   Nu.
										</span>
										<input type="text" name="amount" readonly="readonly"  class="form-control input-mask-phone" id="amount"/>
									</div>
								</td>
							</tr>
							<tr id="penaltyForm">
								<td>Penalty:</td>
								<td>
									<div class="input-group">
										<span class="input-group-addon">
										   Nu.
										</span>
										<input type="text" name="penalty" readonly="readonly"  class="form-control input-mask-phone" id="penalty"/>
									</div>
								</td>
							</tr>
							<tr>
								<td>Total Amount:</td>
								<td>
									<div class="input-group">
										<span class="input-group-addon">
										   Nu.
										</span>
										<input type="text" readonly="readonly" name="totalAmount" class="form-control input-mask-phone" id="totalAmount"/>
										<input type="hidden" name="ODLRenewalCost" class="form-control input-mask-phone" id="ODLRenewalCost"/>
										<input type="hidden" name="PDRenewalCost" class="form-control input-mask-phone" id="PDRenewalCost"/>
										<input type="hidden" name="cardCost" class="form-control input-mask-phone" id="cardCost"/>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--<div class="col-lg-12 alert alert-info" id="perviousPaymentDiv" style="display:none"></div>-->
				</div>
		</div>
	</div>
</div>