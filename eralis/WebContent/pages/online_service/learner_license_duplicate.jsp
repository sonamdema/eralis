<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<script>
	var context = "<%=request.getContextPath()%>";
	
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/service.html" styleId="serviceForm">
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-sm-2">
									<label class="control-label">Learner Number:</label>
								</div>
								<div class="col-sm-4" id="displaylearner"></div>
								<div class="col-sm-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-sm-4" id="displayName"></div>
							</div>
							<!-- <div class="form-group">
								<div class="col-sm-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-sm-4" id="dob"></div>
								<div class="col-sm-2">
									<label class="control-label">Address:</label>
								</div>
								<div class="col-sm-4" id="displayAddress"></div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Replacement Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							 <div class="form-group"> 
			                    <div class="col-sm-3 control-label">
									<label>Submit to  <span style="color: #ff0000">*</span>:</label>
							    </div>	
						        <div class="col-sm-4" >
						        	<select class="form-control" id="locationId">
						        		<option value="">Select Submit To</option>
						        		<logic:iterate id="region" name="locationList">
											<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
										</logic:iterate>
						        	</select>
						        	<html:hidden property="jurisId" styleId="submittedLocationId"/>
									<html:hidden property="jurisTypeId" styleId="submittedLocationType"/>
									<html:hidden property="customerId" styleId="learnerCustomerId"/>
									<html:hidden property="learnerLicenseId" styleClass="form-control" styleId="learnerLicenseId"></html:hidden>
									<html:hidden property="licenseNo" styleClass="form-control" styleId="learnerLicenseNo"></html:hidden>
									
								   <label style="display:none;color: #ff0000" id="locationValidation">Please select Location</label>
				                </div>
				                <div class="col-sm-5">
				                	<a href="#" data-toggle="tooltip" title="Select Region and Office where you want to collect the document from"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
				                </div>
			                </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</html:form>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
		
		
		<script type="text/javascript">

		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
					
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
	
			</script>
			<script>
			var name 		= '<%=request.getAttribute("name")%>';
			var bloodgroup	= '<%=request.getAttribute("bloodgroup")%>';
			var dob			= '<%=request.getAttribute("dob")%>';
			var dzongkhag 	= '<%=request.getAttribute("dzongkhag")%>';
			var gewog 		= '<%=request.getAttribute("gewog")%>';
			var address 	= '<%=request.getAttribute("address")%>';
			var country 	= '<%=request.getAttribute("country")%>';
			var village		= '<%=request.getAttribute("village")%>';
			var learnerlicenseNo = '<%=request.getAttribute("learnerlicenseNo")%>';
			var customerId	= '<%=request.getAttribute("customerId")%>';
			var region		= '<%=request.getAttribute("region")%>';
			var expirydate	= '<%=request.getAttribute("expirydate")%>';
			var issuedate	= '<%=request.getAttribute("issuedate")%>';
			var learnerLicenseId	= '<%=request.getAttribute("learnerLicenseId")%>';
			
			$('#displayName').html("<label class='control-label'>"+name+"</label>");
			$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
			$('#dob').html("<label class='control-label'>"+dob+"</label>");
			$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
			$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
			$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
			$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
			$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
			$('#licenseNo').val(learnerlicenseNo);
			$('#learnerCustomerId').val(customerId);
			
			$('#learnerLicenseId').val(learnerLicenseId);
			$('#learnerLicenseNo').val(learnerlicenseNo);
			
			$('#region').val(region);
			$('#displayregion').html("<label class='control-label'>"+region+"</label>");
			$('#displaylearner').html("<label class='control-label'>"+learnerlicenseNo+"</label>");
			$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
			$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");

			function getPaymentInfo()
			{
				var requestType = "LEARNER";
				var serviceType = "DUPLICATE";
				var identityNo = $('#learnerLicenseId').val();
				var identityTypeId = "L";
				$("#penaltyForm").hide();
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '');
			}
			
			function formSubmit()
			{
				var amount = $('#amount').val();
				var options = {target:'#online_receipt',url:context+'/service.html?method=learnerLicenseOnlinePayment&formType=LL_DUPLICATE&pageId=47&amount='+amount,type:'POST',data: $("#serviceForm").serialize()}; 
			    $("#serviceForm").ajaxSubmit(options);
			}
			
			</script>
			 