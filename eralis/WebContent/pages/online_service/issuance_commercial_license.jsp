<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%
	ServiceDTO dto = (ServiceDTO)request.getAttribute("serviceDTO");
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="row">
	<div class="col-xs-12">
	 <html:form styleClass="form-horizontal" action="/service.html" styleId="learnerForm">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" ><%=dto.getName()%></div>
								<div class="col-lg-2">
									<label class="control-label">Customer ID:</label>
								</div>
								<div class="col-lg-4" id="customerID"><%=dto.getCustomerID()%></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Occupation:</label>
								</div>
								<div class="col-lg-4" id="occupation"><%=dto.getOccupation()%></div>
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"><%=dto.getDOB()%></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Identification Marks:</label>
								</div>
								<div class="col-lg-4" id="identificationMarks"><%=dto.getIdentificationMarks()%></div>
								<div class="col-lg-2">
									<label class="control-label">Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"><%=dto.getGender()%></div>
							</div>
								
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Nationality:</label>
								</div>
								<div class="col-lg-4" id="nationality"><%=dto.getNationality()%></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"><%=dto.getBloodGroup()%></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"><%=dto.getFatherName()%></div>
								
							</div>
							<h4>Permanent Address(National)</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"><%=dto.getDzongkhag()%></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"><%=dto.getGewog()%></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-3"><%=dto.getVillage()%></div>
								
							</div>
							<h4>Permanent Address(Non-National)</h4>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-4" id="displayCountry"><%=dto.getCountry()%></div>
								<div class="col-lg-2">
									<label class="control-label">Address:</label>
								</div>
								<div class="col-lg-4" id="displayAddress"><%=dto.getAddress()%></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driving License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Region<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="region"
										styleClass="form-control" styleId="region">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="regionList"
											label="headerName" value="headerId"  />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-lg-2">
									<label>Receipt Number<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="receiptNo" styleId="receiptNo"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Receipt Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="receiptDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drive Types:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="drivetype"
										styleClass="form-control" styleId="drivetype">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="DRIVE_TYPE_LIST"
											label="headerName" value="headerId"  /> 
									</html:select>
									
								</div>
								<div class="col-lg-2">
									<label>Status:</label>
								</div>
								<div class="col-lg-3">
									
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="displayMsgDiv">&nbsp;</div>
		<div class="pull-right">
			<html:hidden property="licenseNo" styleId="licenseNo" /> 
			<html:hidden property="pageId" styleId="pageId" /> 
			<html:hidden property="customerId" styleId="learnerCustomerId" value="<%=dto.getCustomerID()%>"/>
			<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Save</button>
			<button type="reset" class="btn btn-primary btn-sm">Refresh</button>
			<button class="btn btn-primary btn-sm">Delete</button>
			<button class="btn btn-primary btn-sm">New</button>
			<button class="btn btn-primary btn-sm">Close</button>
			<button class="btn btn-primary btn-sm">Find</button>
		</div>
		</html:form>
	</div>
</div>

 <script>
 $(document).ready(function()
			{
				$('#submitBtn').click(function(){
					if($('#learnerForm').valid()) 
					{
						var options = {target:'#displayMsgDiv',url:context+'/service.html?method=issue_commercial_driving_license',type:'POST',data: $("#learnerForm").serialize()}; 
					    $("#learnerForm").ajaxSubmit(options);
				        $('#displayMsgDiv').show();
				        setTimeout('hideStatus("displayMsgDiv")',3000);
				        setTimeout('reloadPage()',2000);
					}
					else 
					{
						return false;
					}
				});
		});
 </script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>			 