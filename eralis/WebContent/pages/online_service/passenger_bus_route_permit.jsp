<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Home - eRaLIS</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" /> 
		<script> var nextID	=	1;</script>
	</head>
	<body class="no-skin">
		<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			 <html:form styleClass="form-horizontal container" styleId="passengerBusPermit" action="/service.html">									
				<div class="widget-box">
					<div class="widget-header widget-header-small">
						<h4 class="widget-title lighter">Passenger Bus Route Permit</h4>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<div class="row">
								<div class="col-lg-12">
									<div class="col-lg-12 form-group">
										<label class="col-lg-3 col-sm-5 col-xs-12 control-label">Citizen Identity Card No
										 <span class="text-danger">*</span></label>
										<div class="col-lg-3 no-padding">
											<html:text property="cid" styleClass="col-xs-12" styleId="cid" ></html:text>
											<html:hidden property="customerId" styleId="customerId"></html:hidden>
											<label class="no-padding-right red" style="display:none;" id="cidValidation">Please enter CID No. </label>
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<label class="col-lg-3 col-sm-5 col-xs-12 control-label">Date of Birth <span class="text-danger">*</span></label>
										<div class="col-lg-3 no-padding">
											<html:text property="dob" styleClass="col-xs-12 date-picker" styleId="dob" ></html:text>
											<label class="no-padding-right red" style="display:none;" id="dobValidation">Please enter Date of Birth</label>
										</div>
									</div>
								</div>
								<div class="col-lg-12 form-group">
									<div class="center col-lg-8"> 
										<span class="input-group-btn">
											<button type="button" class="btn btn-purple btn-sm" onclick="getPersonalDtls()">
												<i class="ace-icon fa fa-search icon-on-right bigger-110"></i> <b>Search Applicant</b>
											</button>
										</span>
									</div>
									<div class="alert alert-danger col-lg-12 text-danger" style="margin-top: 12px;display:none" id="invalidCIDMessage"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="widget-box" id="permitDetiailsEntryForm" style="display:none">
					<div class="widget-body">
						<div class="widget-main">
							<div class="row">
								<div class="col-lg-12">	
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Applicant Name <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:text property="name" styleClass="col-xs-12" styleId="name" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="nameValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Gender <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:select property="gender" styleClass="col-xs-12" styleId="gender">
												   <html:option value="">Select Gender</html:option>
												   <html:option value="M">Male</html:option>
												   <html:option value="F">Female</html:option>
											   </html:select>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="genderValidation">This field is required</label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Permanent Dzongkhag <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:select property="dzongkhag" styleClass="col-xs-12" styleId="dzongkhag" onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N');">
												   <html:option value="">Select Dzongkhag</html:option>
												   <html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
											   </html:select>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="dzongkhagValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Permanent Gewog <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:select property="gewog" styleClass="col-xs-12" styleId="gewog">
												   <html:option value="">Select Gewog</html:option>
											   </html:select>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="gewogValidation">This field is required</label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Permanent Village : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:text property="village" styleClass="col-xs-12" styleId="village"></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="villageValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" for="Mobile">Present Mobile No <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:text property="phone" styleClass="col-xs-12" styleId="phone" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="phoneValidation">This field is required</label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Present Email : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:text property="email" styleClass="col-xs-12" styleId="email" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="emailValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Present Address <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:textarea property="address" styleClass="col-xs-12" styleId="address" ></html:textarea>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="addressValidation">This field is required</label>
										</div>
									</div>
									
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Route Type <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:select property="type" styleClass="col-xs-12" styleId="type">
												   <html:option value="">Select Route Type</html:option>
												   <html:option value="LOCAL_DISTANCE">Local Distance</html:option>
												   <html:option value="LONG_DISTANCE">Long Distance</html:option>
											   </html:select>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="typeValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Bus Category <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:select property="busCategory" styleClass="col-xs-12" styleId="busCategory">
												   <html:option value="">Select Bus Category</html:option>
												   <html:optionsCollection name="passengerBusCategoryList" label="headerName" value="headerId"/>
											   </html:select>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="busCategoryValidation">This field is required</label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Route From <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:text property="routeFrom" styleClass="col-xs-12" styleId="routeFrom" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="routeFromValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Route To <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:text property="routeTo" styleClass="col-xs-12" styleId="routeTo" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="routeToValidation">This field is required</label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Timing <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:text property="timing" styleClass="col-xs-12" styleId="timing" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="timingValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Transport Name <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               	<html:text property="transport" styleClass="col-xs-12" styleId="transport" ></html:text>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="transportValidation">This field is required</label>
										</div>
									</div>
									
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-2 no-padding-right" >Submit to <span class="text-danger">*</span> : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <html:select property="location" styleClass="col-xs-12" styleId="location">
												   <html:option value="">Select RSTA Office</html:option>
												   <html:optionsCollection name="locationList" label="headerName" value="headerId"/>
											   </html:select>
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="locationValidation">This field is required</label>
										</div>
										<label class="control-label col-lg-2 no-padding-right" >Support Document : </label>
										<div class="col-lg-3">
											<div class="clearfix">
				                               <input type="file">
											</div>
											<label class="no-padding-right red validationMessage" style="display:none;" id="docValidation">This field is required</label>
										</div>
									</div>
									<div class="form-group" id="submitButtonDiv" > 
										<div class="col-lg-12 center">
											<button type="button" onclick="formSubmit()" class="btn btn-primary btn-sm">Submit</button>
										</div> 
									</div>
									<div id="messageDiv" class="col-lg-12"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</html:form>
			<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
		</div><!-- /.main-container -->
	<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
	<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/ace.min.js"></script>			
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
	<script type="text/javascript">
		

	var context = "<%=request.getContextPath()%>";
	
		//datepicker plugin
		$('.date-picker').datepicker({
			autoclose : true,
			todayHighlight : true
		});
		
		function getPersonalDtls()
		{
			$("#invalidCIDMessage").hide();
			
			var cid = $("#cid").val();
			var dob = $("#dob").val();
			var valid = 1;
			if(cid=="")
			{
				valid = 0;
				$("#cidValidation").show();
			}
			if(dob=='')
			{
				valid = 0;
				$("#dobValidation").show();
			}
			if(valid==1)
			{

				$("#cidValidation").hide();
				$("#dobValidation").hide();
				$.ajax
				({
					async: true,
					type: 'POST',
					url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtlsByCid&cid='+cid+'&dob='+dob,
					success: function(xml)
					{
						$(xml).find('xml-response').each(function()
						{
							
							var name = $(this).find('name').text();
							var cid = $(this).find('cid').text();
							var customerId = $(this).find('customerId').text();
							var gender = $(this).find('gender').text();
							var dzongkhag = $(this).find('dzongkhag').text();
							var gewog = $(this).find('gewog').text();
							var village = $(this).find('village').text();
							var phone = $(this).find('phone').text();
							var address = $(this).find('address').text();
							var email = $(this).find('email').text();
							var status = $(this).find('status').text();
							
							
							if(status=='VALID_CID')
							{
								populateDependentDropDown(dzongkhag, 'gewog', '', 'GEWOG_LIST', 'N');

								$('#name').attr('readonly', true);
								$('#gender').attr('readonly',true);
								$('#dzongkhag').attr('readonly',true);
								$('#gewog').attr('readonly',true);
								$('#village').attr('readonly', true);
								$('#phone').attr('readonly', true);
								$('#email').attr('readonly', true);
								$('#address').attr('readonly', true);
								
								$("#name").val(name);
								$("#customerId").val(customerId);
								$("#gender").val(gender);
								$("#dzongkhag").val(dzongkhag);
								$("#gewog").val(gewog);
								$("#village").val(village);
								$("#phone").val(phone);
								$("#email").val(email);
								$("#address").val(address);
								$("#permitDetiailsEntryForm").show();
								
							}
							else if(status=='INVALID_CID')
							{
								$("#invalidCIDMessage").show();
								$("#invalidCIDMessage").html('<i class="fa fa-2x fa-exclamation-triangle"></i> Sorry! This CID is not recorded in our system, Please enter all field');
								$("#permitDetiailsEntryForm").show();
								
								$('#name').attr('readonly', false);
								$('#gender').attr('readonly',false);
								$('#dzongkhag').attr('readonly',false);
								$('#gewog').attr('readonly',false);
								$('#village').attr('readonly', false);
								$('#phone').attr('readonly', false);
								$('#email').attr('readonly', false);
								$('#address').attr('readonly', false);
								
							}
							else if(status=='NOT_MATCHED')
							{
								$("#invalidCIDMessage").show();
								$("#invalidCIDMessage").html('<i class="fa fa-2x fa-exclamation-triangle"></i> Sorry! CID and Date of Birth does not match, Please enter valid data');
								$("#permitDetiailsEntryForm").hide();
							}
						 
						});
					}
				});
			}
		}
		
		
		
		
		
		function populateDependentDropDown(parentId, targetFieldId, nextTargetFieldId, fieldCons, isOther)
		{
			
			var childFieldId = '#'+targetFieldId;
			$(childFieldId).empty();
			$(childFieldId).append("<option value='0'>--SELECT--</option>");
			
			var url = context+'/jsondataloader.data?fieldCons='+fieldCons+'&parentId='+parentId;
			$.ajax({	
				type: "GET",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data) 
				{
					
						for (var i = 0; i < data.length; i++) {
							$(childFieldId).append("<option value=" + data[i].headerId + ">"+ data[i].headerName + "</option>");
					    }
						
						if("Y" == isOther){
							$(childFieldId).append("<option value='ALL'>ALL</option>");
						}	
				},
				error : function(jqXHR, textStatus, errorThrown) {		
					//alert(textStatus);
				}
			});
		}
		
		
		
		function formSubmit()
		{

			var valid = 1;
			var name = $("#name").val();
			var gender = $("#gender").val();
			var dzongkhag = $("#dzongkhag").val();
			var gewog = $("#gewog").val();
			var village = $("#village").val();
			var phone = $("#phone").val();
			var email = $("#email").val();
			var address = $("#address").val();
			var type = $("#type").val();
			var busCategory = $("#busCategory").val();
			var routeFrom = $("#routeFrom").val();
			var routeTo = $("#routeTo").val();
			var timing = $("#timing").val();
			var transport = $("#transport").val();
			var location = $("#location").val();
			
			
			$(".validationMessage").hide();

			if(name=="")
			{
				valid = 0;
				$("#nameValidation").show();
			}
			if(gender=="")
			{
				valid = 0;
				$("#genderValidation").show();
			}
			if(dzongkhag=="")
			{
				valid = 0;
				$("#dzongkhagValidation").show();
			}
			if(gewog=="")
			{
				valid = 0;
				$("#gewogValidation").show();
			}
			/* if(village=="")
			{
				valid = 0;
				$("#villageValidation").show();
			} */
			if(phone=="")
			{
				valid = 0;
				$("#phoneValidation").show();
			}
			if(email=="")
			{
				valid = 0;
				$("#emailValidation").show();
			}
			if(address=="")
			{
				valid = 0;
				$("#addressValidation").show();
			}
			if(type=="")
			{
				valid = 0;
				$("#typeValidation").show();
			}
			if(busCategory=="")
			{
				valid = 0;
				$("#busCategoryValidation").show();
			}
			if(routeFrom=="")
			{
				valid = 0;
				$("#routeFromValidation").show();
			}
			if(routeTo=="")
			{
				valid = 0;
				$("#routeToValidation").show();
			}
			if(timing=="")
			{
				valid = 0;
				$("#timingValidation").show();
			}
			if(transport=="")
			{
				valid = 0;
				$("#transportValidation").show();
			}
			if(location=="")
			{
				valid = 0;
				$("#locationValidation").show();
			}
			
			
			if(valid==1)
			{
				var renewalType = "N";
				var totalAmount = $('#totalAmount').val();
				var amount = $('#amount').val();
				var penalty = $('#penalty').val();
				var options = {target:'#messageDiv',url:context+'/service.html?method=passengerBusPermit',type:'POST',data: $("#passengerBusPermit").serialize()}; 
			    $("#passengerBusPermit").ajaxSubmit(options);
		        $('#messageDiv').show();
		        $("#submitButtonDiv").hide();
			}
		}	
	</script>
	
	</body>
</html>