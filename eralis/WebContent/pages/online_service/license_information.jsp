<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%
	ServiceDTO dto = (ServiceDTO)request.getAttribute("serviceDTO");
%>
<html:form styleClass="form-horizontal" action="/service.html" styleId="learnerForm">
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Information</h5>
		</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
					
					
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Name:</label>
						</div>
						<div class="col-lg-4" id="name"><%=dto.getName()%></div>
						
						<div class="col-lg-2">
							<label class="control-label">Blood Group:</label>
						</div>
						<div class="col-lg-4" id="bloodGroup"><%=dto.getName()%></div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Present Phone No:</label>
						</div>
						<div class="col-lg-4" id="presentPhoneNo"><%=dto.getName()%></div> 
						<div class="col-lg-2">
							<label class="control-label">Present Email:</label>
						</div>
						<div class="col-lg-4" id="presentEmail"><%=dto.getName()%></div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Present Contact Address:</label>
						</div>
						<div class="col-lg-4" id="presentContactAddress"><%=dto.getName()%></div> 
						 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Driving License Details</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
					
					
					<div class="form-group">
						<div class="col-lg-2">
							<label>Issue Date<span style="color: #ff0000">*</span>:</label>
						</div>
						<div class="col-lg-3" id="issueDate"> 
						</div>
					</div>
					  
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Driving License Renwal History</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Renewal Date</th>
					<th>Expiry Date</th>
					<th>Receipt No</th>
					<th>Receipt Date</th>
					<th>Delivered On</th>
					
					
				</tr>
			</thead>
			<tbody>
				<logic:notEmpty name="RENEWAL_LICENSE_LIST">
					<logic:iterate id="renewal" name="RENEWAL_LICENSE_LIST">
						<tr>
							<td><bean:write name="renewal" property="renewaldate"/></td>
							<td><bean:write name="renewal" property="expiryDate"/></td>
							<td><bean:write name="renewal" property="receiptNo"/></td> 
							<td><bean:write name="renewal" property="receiptDate"/></td>
							<td><bean:write name="renewal" property="deliveredon"/></td>
							
							
						</tr>
					</logic:iterate>
				</logic:notEmpty>
				<logic:empty name="RENEWAL_LICENSE_LIST">
					<tr>
						<td colspan="7" align="center">
							<font color='red'>NO RECORD FOUND</font>
						</td>
					</tr>
				</logic:empty>
			</tbody>
		</table>
			</div>
		</div>
	</div>
</div>
	
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Driving License Duplication History</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Duplication Date</th>
							<th>Delivered On</th>
							<th>Receipt No</th>
							<th>Receipt Date</th>
							
							
							
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="DUPLICATE_LICENSE_LIST">
							<logic:iterate id="duplicate" name="DUPLICATE_LICENSE_LIST">
								<tr>
									<td><bean:write name="duplicate" property="duplicationdate"/></td>
									<td><bean:write name="duplicate" property="deliveredon"/></td>
									<td><bean:write name="duplicate" property="receiptNo"/></td> 
									<td><bean:write name="duplicate" property="receiptDate"/></td>
									
									
									
								</tr>
							</logic:iterate>
						</logic:notEmpty>
						<logic:empty name="DUPLICATE_LICENSE_LIST">
							<tr>
								<td colspan="7" align="center">
									<font color='red'>NO RECORD FOUND</font>
								</td>
							</tr>
						</logic:empty>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
	<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Driving License Endorsement History</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>DriveType</th>
							<th>Endorsement Date</th>
							<th>Delivered On</th>
							<th>IID</th>
							<th>Receipt No</th>
							<th>Receipt Date</th>
							
							
							
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="LICENSE_ENDORSEMENT_LIST">
							<logic:iterate id="endorsement" name="LICENSE_ENDORSEMENT_LIST">
								<tr>
									<td><bean:write name="endorsement" property="drivetype"/></td>
									<td><bean:write name="endorsement" property="endorseddate"/></td>
									<td><bean:write name="endorsement" property="deliveredon"/></td> 
									<td><bean:write name="endorsement" property="iid"/></td>
									<td><bean:write name="endorsement" property="receiptNo"/></td> 
									<td><bean:write name="endorsement" property="receiptDate"/></td>
									
									
									
								</tr>
							</logic:iterate>
						</logic:notEmpty>
						<logic:empty name="LICENSE_ENDORSEMENT_LIST">
							<tr>
								<td colspan="7" align="center">
									<font color='red'>NO RECORD FOUND</font>
								</td>
							</tr>
						</logic:empty>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
	
	
	
	
</html:form> 
<script>

var firstName	=	'<%=request.getAttribute("firstName")%>';
var middleName	=	'<%=request.getAttribute("middleName")%>';
var lastName	=	'<%=request.getAttribute("lastName")%>';
var presentContactAddress	=	'<%=request.getAttribute("presentContactAddress")%>';
var presentPhoneNo	=	'<%=request.getAttribute("presentPhoneNo")%>';
var presentEmail	=	'<%=request.getAttribute("presentEmail")%>';
var bloodGroup	=	'<%=request.getAttribute("bloodGroup")%>';
var issueDate	=	'<%=request.getAttribute("issueDate")%>';
var drivingLicenseType	=	'<%=request.getAttribute("drivingLicenseType")%>';
var blood_group_type	=	'<%=request.getAttribute("blood_group_type")%>';

$('#name').html("<label class='control-label'>"+firstName+"</label>");

if(firstName=='null')
	firstName='';
else
	firstName=firstName+" ";
if(middleName=='null')
	middleName='';
else
	middleName=middleName+" ";
if(lastName=='null')
	lastName='';
if(presentContactAddress=='null')
	presentContactAddress='';
if(presentPhoneNo=='null')
	presentPhoneNo='';
if(presentEmail=='null')
	presentEmail='';
if(bloodGroup=='null')
	bloodGroup='';
if(issueDate=='null')
	issueDate='';
if(drivingLicenseType=='null')
	drivingLicenseType='';
if(blood_group_type=='null')
	blood_group_type='';



var name	=	firstName+middleName+lastName;

$('#name').html("<label class='control-label'>"+name+"</label>"); 
$('#presentContactAddress').html("<label class='control-label'>"+presentContactAddress+"</label>"); 
$('#presentPhoneNo').html("<label class='control-label'>"+presentPhoneNo+"</label>"); 
$('#presentEmail').html("<label class='control-label'>"+presentEmail+"</label>"); 
$('#bloodGroup').html("<label class='control-label'>"+blood_group_type+"</label>"); 

$('#issueDate').html("<label class='control-label'>"+issueDate+"</label>");
$('#drivingLicenseType').html("<label class='control-label'>"+drivingLicenseType+"</label>");
 
</script> 