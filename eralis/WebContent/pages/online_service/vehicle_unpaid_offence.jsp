<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<style>
/* The container */
.radio-container {
  display: block;
  position: relative;
  padding: 5px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.radio-container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
    top: 0;
    padding: 3px;
    left: 0;
    border: 1px solid #0000003d;
    height: 25px;
    width: 25px;
    background-color: #fff;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radio-container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radio-container input:checked ~ .checkmark {
  background-color: #2196F3;
  border: none;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.radio-container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.radio-container .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}
blockquote {
    font-size: 14.5px;
}
</style>
<div class="col-lg-12">
	<div class="box box-solid">
    	<div class="box-header with-border">
       		<h3 class="box-title text-danger">Note</h3>
     	</div>
     	<div class="box-body">
       		<blockquote>
       			<ul>
         			<li>Follwing data are unpaid offence</li>
         			<li>Click on select button to select the offence that you wish to pay</li>
         			<li>Please clear all your unpaid offence</li>
         		</ul>
       		</blockquote>
     	</div>
	</div>
</div>
<div class="table-responsive col-lg-12">
	<table  class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<th>TIN No</th>
				<th>Offence Date</th>
				<th>Status</th> 
				<th>Place of Inspection</th> 
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="OFFENCE_LIST">
				<logic:iterate id="offence_list" name="OFFENCE_LIST">
					<tr>
						<td> 
							<label class="radio-container">
								<input type="radio" name="gender" value="male" onClick="getSelected1('<bean:write name="offence_list" property="offenceId"/>')"> 
							  	<span class="checkmark"></span>
							</label>
						</td>
						<td><bean:write name="offence_list" property="TINno"/></td>
						<td><bean:write name="offence_list" property="offencedate"/></td>
						<td><bean:write name="offence_list" property="status"/></td>
						<td><bean:write name="offence_list" property="placeofInspection"/></td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>
<script>
	function getSelected1(offenceId)
	{
		getOffenceDtls(offenceId);
		//get_offence_list(offenceId);
		//getOffencePicture(offenceId);
	}

</script>