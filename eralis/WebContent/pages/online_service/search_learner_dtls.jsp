<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<style>
		.tooltip-inner {
    max-width: 350px;
    /* If max-width does not work, try using width instead */
    width: 350px;
}
	
	</style>
</head>
<body class="no-skin">
		<!-- header include -->
		<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
		<!-- ./header include -->

		<div class="main-container" id="main-container">
			  

			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							 <div class="col-lg-12 row">
									<html:form styleClass="form-horizontal" action="/service.html?method=learnerLicenseDetails">									
									<div class="widget-box">
										<div class="widget-header widget-header-small">
											<h5 class="widget-title lighter">Book a Driving Test</h5>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group" > 
															<div class="col-lg-4">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Issue Based On<span style="color: #ff0000">*</span></label>
															</div>
															<div class="col-lg-4">
																<html:select property="issueType" styleId="issueBasedOn" styleClass="form-control" onchange="checkIssueBasedOn(this.value)">
									                           		<html:option value="">Select Issue Based On</html:option>
									                           		<html:option value="Learner_License">Learner License</html:option>
									                           		<html:option value="DL">Driving License</html:option>
									                           		<html:option value="OTHERS">Others</html:option>
								                           		</html:select>
															</div>  
															
															<div class="col-xs-12 col-lg-1">
																<a href="#" data-toggle="tooltip"  data-html="true"  title="Select Issue Based On"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
															</div>
															<div class="col-lg-3">
																<label style="display:none;color: #ff0000" id="issueBasedOn_validation">Please select Issue Based On</label>
															</div>
														</div>
														<div class="form-group" id='others-issueType-div'  style="display:none"> 
															<div class="col-lg-4">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Issue Type<span style="color: #ff0000">*</span></label>
															</div> 
															<div class="col-lg-4">
							                                	<html:select property="issueType" styleId="issueType" styleClass="form-control" onchange="checkIssueType(this.value)">
							                                		<html:option value="">--SELECT--</html:option>
									                           		<html:option value="Arm_Force">Arm Force</html:option>
									                           		<html:option value="Driving_Institue">Driving Institue</html:option>
									                           		<html:option value="Foreign_Driving_License">Bhutanese_Foreign Driving License Holder</html:option>
							                                	</html:select>
															</div>  
															<div class="col-xs-12 col-lg-1">
																<a href="#" data-toggle="tooltip"  data-html="true"  title="Select Issue Type"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
															</div>
															<div class="col-lg-3">
																<label  style="display:none;color: #ff0000" id="issueType_validation">Please select Issue Type</label>
															</div>
														</div>
														<div class="form-group" id='cid-div'  style="display:none"> 
															<div class="col-lg-4">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Citizenship ID<span style="color: #ff0000">*</span></label>
															</div> 
															<div class="col-lg-4">
							                                	<html:text property="cid" styleId="cid" styleClass="form-control">	</html:text>
															</div> 
															<div class="col-xs-12 col-lg-1">
																<a href="#" data-toggle="tooltip" data-html="true" title="<table border='1' style='border-collapse: collapse;'><tr><td colspan='2'>For Bhutanese Nationals</td></tr><tr><td>1</td><td>CID Number</td></tr><tr><td colspan='2'>For Foreign Nationals</td></tr><tr><td>1</td><td>Passport Number</td></tr><tr><td>2</td><td>Work Permit Number</td></tr><tr><td>3</td><td>MC Number</td></tr></table>"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
															</div>
															<div class="col-lg-3">
																<label style="display:none;color: #ff0000" id="cid_validation">Please enter CID</label>	
															</div>
														</div>
														
														<div class="form-group" id="dl-ll-issueType-div" style="display:none"> 
															<div class="col-lg-4" id="ll-issueType">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Learner No<span style="color: #ff0000">*</span></label>
															</div> 
															<div class="col-lg-4" id="dl-issueType">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Driving License No<span style="color: #ff0000">*</span></label>
															</div>
															<div class="col-lg-4" id="driving-institute-issueType">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Learner/License No<span style="color: #ff0000">*</span></label>
															</div> 
															<div class="col-lg-4">
							                                	<html:text property="learnerNo" styleId="learnerNo" styleClass="form-control" ></html:text>
															</div> 	
															<div class="col-xs-12 col-lg-1">
																<a href="#" data-toggle="tooltip"  data-html="true"  title="Enter correct learner license no.(E.g. T/LL-12345) or Driving license no.(E.g. T-23456)"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
															</div>
															<div class="col-lg-3">
																<label  style="display:none;color: #ff0000" id="learnerNo_validation">Please enter Learner/License No</label>
															</div>	
														</div>
														<div class="form-group" style="display:none" id="type-test-div"> 
															<div class="col-lg-4">
																<label class="col-lg-9 control-label no-padding-right" for="Original CID">Type of test<span style="color: #ff0000">*</span></label>
															</div> 
															<div class="col-lg-4">
							                                	<html:select property="testType" styleId="testType" styleClass="form-control">
							                                		<html:option value="">Select Type of Test</html:option>
							                                		<html:option value="NEW">New License</html:option>
							                                		<html:option value="ENDORSEMENT">License Endorsement</html:option>
							                                	</html:select>
															</div>  
															<div class="col-xs-12 col-lg-1">
																<a href="#" data-toggle="tooltip"  data-html="true"  title="Select Type of Test"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
															</div>
															<div class="col-lg-3">
																<label  style="display:none;color: #ff0000" id="testType_validation">Please select Test Type</label>
															</div>
														</div>
														<div class="form-group" > 
															<div class="col-lg-12 center">
																<button type="button" onClick="leanerLicenseDetails()" class="btn btn-primary btn-sm">Submit</button>
															</div> 
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</html:form>
							</div>
							<div class="col-lg-12 row" id="book_test_div">
								 
					</div>
				</div><!-- /.contentDisplayDiv -->
			</div><!-- /.page-content -->
		</div>
	</div><!-- /.main-content -->
			<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
</div><!-- /.main-container -->
		
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script>
	function checkIssueBasedOn(issueType)
	{
		$("#cid-div").show();
		
		$("#driving-institute-issueType").hide();
		$("#ll-issueType").hide();
		$("#dl-issueType").hide();
		$("#dl-ll-issueType-div").hide();
		$("#others-issueType-div").hide();
		$("#type-test-div").hide();
		
		if(issueType=="OTHERS")
		{
			$("#others-issueType-div").show();
		}
		else if(issueType=="Learner_License" || issueType=="DL")
		{
			$("#dl-ll-issueType-div").show();
			if(issueType=="Learner_License")
			{
				$("#ll-issueType").show();
			}
			else if(issueType=="DL")
			{
				$("#dl-issueType").show();
			}
		}
	}
	function checkIssueType(testType)
	{
		
		$("#dl-ll-issueType-div").hide();
		$("#driving-institute-issueType").hide();
		$("#type-test-div").hide();
		
		if(testType=='Driving_Institue')
		{
			$("#dl-ll-issueType-div").show();
			$("#driving-institute-issueType").show();
			$("#type-test-div").show();
			
		}
		
	}
</script>
<script type="text/javascript">
	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	}); 
</script>

<script>
	function leanerLicenseDetails()
	{
		var learnerNo	=	$("#learnerNo").val();
		var cid			=	$("#cid").val();
		var testType 	= 	$('#testType').val();
		var issueType	=	$('#issueType').val();
		var issueBasedOn	=	$('#issueBasedOn').val();
		
		var validation	=	"0";
		$("#learnerNo_validation").hide();
		$("#cid_validation").hide();
		$("#testType_validation").hide();
		$("#issueType_validation").hide();
		$("#issueBasedOn_validation").hide();
		
		if(cid=="")
		{
			$("#cid_validation").show();
			validation	=	"1";
		}
		if(issueBasedOn=="")
		{
			$("#issueBasedOn_validation").show();
			validation	=	"1";
		}
		
		if(issueBasedOn=="OTHERS")
		{
			
			if(issueType=="")
			{
				$("#issueType_validation").show();
				validation	=	"1";
			}
			else if(issueType=="Driving_Institue")
			{
				if(learnerNo=="")
				{
					$("#learnerNo_validation").show();
					validation	=	"1";
				}
				
				if(testType=="")
				{
					$("#testType_validation").show();
					validation	=	"1";
				}
			}
			
		}
		else if(issueBasedOn=="Learner_License" || issueBasedOn=="DL")
		{
			if(learnerNo=="")
			{
				$("#learnerNo_validation").show();
				validation	=	"1";
			}
		}
		
		
		
		if(validation=="0")
		{
			$("#testDate").empty();
			$('#DRIVE_TYPE').empty();
			$('#testLocation').empty();
			$("#applicationNo").empty();
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=learnerLicenseDetails&learnerNo="+learnerNo+"&cid="+cid+"&testType="+testType+"&form_type=book_driving_test&issueType="+issueType+"&issueBasedOn="+issueBasedOn,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					 
					$("#book_test_div").html(responseText);
					$("#book_test_div").show();
					
				}
			});
		}
		else
		{
			$("#book_test_div").hide();
		}
	}
</script>
	</body>
</html>