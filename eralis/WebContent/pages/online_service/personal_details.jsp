	<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%
	ServiceDTO dto = (ServiceDTO)request.getAttribute("serviceDTO");
	String otpNo = dto.getOtpNumber();
%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<html:form styleId="personalForm" styleClass="form-horizontal" action="/service.html?method=editPersonalInfo" enctype="multipart/form-data">	
<div class="widget-box" style="display:none">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Information</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
						<div class="col-lg-12 form-group">
							<div class="col-lg-2">
								<span class="profile-picture">
									<img class="editable img-responsive" id="avatar2" src="<%=request.getContextPath() %>/ImageServlet?url=<%=dto.getImage() %>"  />
								</span>
							</div>
						</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Title of Courtesy:</label>
							</div>
							<div class="col-lg-6" >
								<html:select property="titleOfcourtesy" styleId="titleCourtesy"styleClass="col-lg-12"  >
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="titleOfcourtesyList" label="headerName" value="headerId"  />
								</html:select>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>First Name:</label>
							</div>
							<div class="col-lg-6" >
								<html:text property="firstname" styleId="firstName"  readonly="true" styleClass="col-lg-12" value='<%=dto.getFirstName() %>'></html:text>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Middle Name:</label>
							</div>
							<div class="col-lg-6" >
								<html:text property="middlename" styleId="middleName"  readonly="true" styleClass="col-lg-12"  value='<%=dto.getMiddleName() %>'></html:text>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Last Name:</label>
							</div>
							<div class="col-lg-6" >
								<html:text property="lastName" styleId="lastName" readonly="true"   styleClass="col-lg-12"  value='<%=dto.getLastName() %>'></html:text>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="col-lg-6 form-group">
							<div class="col-lg-4" >
								<label>Occupation:</label>
							</div>
							<div class="col-lg-6">
								<html:select property="occupation"  styleClass="col-lg-12" styleId="occupation">
									<html:optionsCollection name="occupationList"
										label="headerName" value="headerId" />
								</html:select>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Nationality:</label>
							</div>
							<div class="col-lg-6">
								<html:select property="nationality" styleClass="form-control" styleId="nationality">
										<html:option value="-1">--SELECT--</html:option>
										<html:optionsCollection name="nationalityList" label="headerName"
											value="headerId" />
									</html:select>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Date Of Birth:</label>
							</div>
							<div class="col-lg-6">
								<html:text property="DOB" readonly="true"   styleClass="col-lg-12" value='<%=dto.getDob()%>' ></html:text>
							</div>
						</div>
						<div class="col-lg-6 form-group">	
							<div class="col-lg-4">
								<label>Father's Name:</label>
							</div>
							<div class="col-lg-6">
								<html:text property="fatherName"  readonly="true" styleId="fatherName"   styleClass="col-lg-12" value='<%=dto.getFatherName() %>'></html:text>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Blood Group:</label>
							</div>
							<div class="col-lg-6">
								<html:select property="bloodGroup" styleClass="col-lg-12"   styleId="bloodGroup">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="bloodgroupList" label="headerName"
										value="headerId" />
								</html:select>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Gender:</label>
							</div>
							<div class="col-lg-6">
								<html:select property="gender" styleClass="col-lg-12" styleId="gender">
									<html:option value="">--SELECT--</html:option>
									<html:option value="M">Male</html:option>
									<html:option value="F">Female</html:option>
									 
								</html:select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="widget-box" style="display:none">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Permanent Address</h5>
		</div>
		<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="radio">
									<label class="col-sm-3 no-padding-right">
										<html:radio property="national" styleId="nationalRadio" onclick="enable('National')" styleClass="ace"
											value="N"></html:radio> <span class="lbl"> Bhutanese</span> </label>
									<label class="col-sm-3 no-padding-right">
										<html:radio property="national" styleId="internationalRadio" onclick="enable('International')" styleClass="ace"
											value="I"></html:radio> <span class="lbl">
											Foreign National</span> </label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Dzongkhag<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<select id="dzongkhag" class="form-control"  onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N')" >
										<option value="">--SELECT--</option>
										<logic:iterate id="dzongkhag" name="dzongkhagList">
											<option value='<bean:write name="dzongkhag" property="headerId"/>'><bean:write name="dzongkhag" property="headerName"/></option>
										</logic:iterate>
									</select>
								</div>
								<div id="requiredDzongkhag" style="display:none;" class="error">Please select Dzongkhag</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Gewog<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<select id="gewog" class="form-control">
										<option value="">--SELECT--</option>
									</select>
								</div>
								<div  id="requiredGewog" style="display:none;" class="error">Please select Gewog</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Village<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:text property="village" styleId="village"  styleClass="col-lg-12"  value='<%=dto.getPermanentVillage() %>'></html:text>
								</div>
								<div   class="error" id="requiredVillage" style="display:none;" >Please Enter Village</div>
								 
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Country:</label>
								</div>
								<div class="col-lg-6">
									<html:select property="country" styleClass="form-control" styleId="country">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="countryList" label="headerName" value="headerId" />
									</html:select>
								</div>
								<div id="requiredCountry" style="display:none;" class="error">Please select Country</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Address:</label>
								</div>
								<div class="col-lg-6">
									<html:text property="address"  readonly="true" styleId="address"  styleClass="col-lg-12"  value='<%=dto.getPermanentAddress() %>'></html:text>
								</div>
								<div id="requiredAddress" style="display:none;"  class="error">Please enter Address</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		
	</div>

	<div class="widget-box" style="display: none;" id="contactAddressDiv">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Contact Address</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="radio">
								<label class="col-sm-2 no-padding-right">
									   <html:radio property="contactRadio" styleId="officialRadio" onclick="enable('Official')" styleClass="ace"
										value="Official"></html:radio> <span class="lbl"> Official</span> </label>
								<label class="col-sm-2 no-padding-right">
									   <html:radio property="contactRadio" styleId="privateRadio" onclick="enable('Private')" styleClass="ace"
										value="Private"></html:radio> <span class="lbl">
									    Business/Personal</span> </label>
							     </div>
						 </div>
						<div class="form-group" id="ministryDIV" style="display:none">
							<div class="col-lg-3">
								<label>Ministry:</label>
							</div>
							<div class="col-lg-3">
							<html:select property="ministry" styleClass="form-control" styleId="ministry" onchange="populateDependentDropDown(this.value, 'department', '', 'DEPARTMENT_LIST', 'N')">
									<html:option value="0">--SELECT--</html:option>
					   				<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
							</html:select>
							</div>
							<div id="requiredMinistry" style="display:none;" class="error">Please select Ministry</div>
							<div class="col-lg-3">
								<label>Department:</label>
							</div>
							<div class="col-lg-3">
								<html:select property="department" styleClass="form-control" styleId="department">
									<html:option value="0">--SELECT--</html:option>
								</html:select>
							</div>
							<div id="requiredDepartment" style="display:none;" class="error">Please select Department</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label>Dzongkhag:</label>
							</div>
							<div class="col-lg-3">
								<html:select property="presentDzongkhag" styleClass="form-control"   styleId="presentDzongkhag">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="dzongkhagList" label="headerName"
										value="headerId" />
								</html:select>
							</div>
							<div class="col-lg-3">
								<label>Contact address:</label>
							</div>
							<div class="col-lg-3">
								<html:text property="contactAddress" styleId="presentContactAddress"  styleClass="form-control"  value='<%=dto.getPresentContactAddress() %>'></html:text>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label>Phone:</label>
							</div>
							<div class="col-lg-3">
								<html:text property="phone" styleId="presentPhoneNo"  styleClass="form-control"  value='<%=dto.getPresentPhoneNo() %>'></html:text>
							</div>
							<div class="col-lg-3">
								<label>Email:</label>
							</div>
							<div class="col-lg-3">
								<html:text property="email" styleId="presentEmail"  styleClass="form-control"  value='<%=dto.getPresentEmail() %>'></html:text>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Guardian Contact No :</label>
							</div>
							<div class="col-lg-3">
								<html:text property="guardianNo" styleId="guardianNo" styleClass="form-control" value="<%=dto.getGuardianNo() %>"></html:text>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-12">
								<input type="checkbox" id="agreeCheck" onclick="enableSubmitBtn()"/>
								&nbsp;&nbsp;
								<strong>I hereby declare that the information provided in the form is accurate, if found guilty I shall be liable for punishment as per RSTA rules and regulations</strong>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group centered" > 
							<div class="col-lg-12 centered">
								<html:hidden property="dzongkhag" styleId="dzongkhagHidden"/>
								<html:hidden property="gewog" styleId="gewogHidden"/>
								<html:hidden property="manualFlag" styleId="manualFlag" value="N"/>
								
								<html:hidden property="oldImage" value="<%=dto.getImage() %>"></html:hidden>
								<html:hidden property="personalInfoId" styleId="personalInfoId" value="<%=dto.getPersonalInfoId() %>" ></html:hidden>
								<%
									String cid = (String)request.getAttribute("CID");
								%>
								<html:hidden property="CID" value="<%=cid %>"></html:hidden>
								<button class="btn btn-sm  btn-primary centered" type="button" id="submitButton" onclick="openModal('confirmationModal')">
									Save
								</button> 
								&nbsp;&nbsp;
								<button class="btn btn-sm  btn-primary centered" onclick="searchPersonalInfo()">
									Cancel
								</button>
							</div>  		
						</div>
					</div>
					<div id="submit_message" class="col-lg-12">
				</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirmationModal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Confirmation</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12"> 
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label class="col-sm-9" for="CID"> Are you sure, you want to save the data? </label>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm" name="search" data-dismiss="modal" id="saveData">
						Yes
					</button>
					<button class="btn btn-sm" data-dismiss="modal">
						No
					</button>
				</div>
			</div>
		</div>
	</div>
</html:form>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

	<%
		String cid = null, dob = null, message = null;
		if(request.getAttribute("DOB") != null)
			dob = (String)request.getAttribute("DOB");
		if(request.getAttribute("CID") != null)
			cid = (String)request.getAttribute("CID");
		if(request.getAttribute("MESSAGE") != null)
			message = (String)request.getAttribute("MESSAGE");
	%>

	var cid = "<%=cid%>";
	var dob = "<%=dob%>";
	var message = "<%=message%>";
	var otp = "<%=otpNo%>";

	$(document).ready(function()
	{
		$('#otpDiv').show();
		$('#searchBtnDiv').hide();
		$('#submitButton').attr('disabled', true);
		
		if(message == "FAILURE")
		{
			$('#personalDetailsDiv').html("<div class='alert alert-danger'>Details couldnot be updated, please try again later</div>");
			$('#personalDetailsDiv').show();
			
		}
		else if(message == "SUCCESS")
		{
			$('#cid').val(cid);
			$('#dob').val(dob);
			getPersonalInfo();
		}
	});

	function enableSubmitBtn()
	{
		if($("#agreeCheck").is(':checked'))
			$('#submitButton').attr('disabled', false);
		else
			$('#submitButton').attr('disabled', true);
	}

	function validateOtp()
	{
		var enteredOtp = $('#otp').val();

		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=validateOTP&cid='+cid+'&otp='+enteredOtp,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					var status = $(this).find('status').text();

					if(status == "MATCH")
						$('#contactAddressDiv').show();
					else if(status == "OTP_EXPIRED")
						$('#personalDetailsDiv').html("<div class='alert alert-danger'>OTP has expired, please try again</div>");
					else
						$('#personalDetailsDiv').html("<div class='alert alert-danger'>OTP doesnot match, verify the OTP or the registered mobile number and try again</div>");
				});
			}
		});
	}


	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

 
		 
</script>
<script>
$(document).ready(function() {
	$("#titleCourtesy").attr("readonly",true);
	$('#nationalRadio').attr('checked', true);
	$('#country').attr('disabled', true);
	$('#address').attr('disabled', true);

	var ministryId = '<%=dto.getMinistry()%>';
	var departmentId = '<%=dto.getDepartment()%>';
	if(ministryId!="0")
	{
		$("#ministry").val(ministryId);
		populateDependentDropDown(ministryId, 'department', '', 'DEPARTMENT_LIST', 'N');
		$("#department").val(departmentId);
		enable('Official');	
		$('#officialRadio').prop('checked',true);
		$('#privateRadio').prop('checked',false);

	}
	else
	{
		enable('Private');	
		$('#privateRadio').prop('checked',true);
		$('#officialRadio').prop('checked',false);

	}
	
	
	
	
});
function enable(identifier)
{
	if("National" == identifier)
	{
		$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);

		$('#dzongkhag').attr('disabled', false);
		$('#gewog').attr('disabled', false);
		$('#village').attr('disabled', false);
		$("#personalForm").validate
        ({
			rules:
			{ 
				dzongkhag:
				{
					required: true,
					number: true,
				},
				gewog:
				{
					required: true,
					number: true,
				},
				village:
				{
					required: true,
					number: true,
				}
			},    
			messages:
			{
				 
				phone:{required: "Please enter phone number"},
				dzongkhag:{required: "Please enter Permanent Dzongkhag"},
				gewog:{required: "Please enter Gewog"},
				village:{required: "Please enter Village"}
			}
		});
	}
	else if("Official" == identifier)
	{
		$('#ministryDIV').show();
	    $('#departmentDIV').show();

		$('#ministry').attr('disabled', false);
		$('#department').attr('disabled', false);
	    $("#personalForm").validate
        ({
			rules:
			{ 
				ministry:
				{
					required: true,
					number: true,
				},
				department:
				{
					required: true,
					number: true,
				}
				
			},    
			messages:
			{
				 
				ministry:{required: "Please enter phone number"},
				department:{required: "Please enter Permanent Dzongkhag"},
			}
		});
	}
	else if("Private" == identifier)
	{
		$('#ministryDIV').hide();
	    $('#departmentDIV').hide();
	}
	else
	{
		$('#country').attr('disabled', false);
		$('#address').attr('disabled', false);

		$('#dzongkhag').attr('disabled', true);
		$('#gewog').attr('disabled', true);
		$('#village').attr('disabled', true);
	}
}

var nationality	=	"<%=dto.getNationality()%>";
var titleCourtesy	=	'<%=dto.getTitleCourtesy()%>';
var occupation	=	'<%=dto.getOccupation()%>';
var bloodGroup	=	'<%=dto.getBloodGroup()%>';
var gender	=	'<%=dto.getGender()%>';
var permanentCountry	=	'<%=dto.getPermanentCountry()%>';
var presentDzongkhag	=	'<%=dto.getPresentDzongkhag()%>';
var permanentDzongkhag	=	'<%=dto.getPermanentDzongkhag()%>';
var permanentGewog	=	'<%=dto.getPermanentGewog()%>';
var permanentVillage	=	'<%=dto.getPermanentVillage()%>';
$("#titleCourtesy").val(titleCourtesy);
$("#occupation").val(occupation);
$("#bloodGroup").val(bloodGroup);
$("#gender").val(gender);
$("#permanentCountry").val(permanentCountry);
$("#presentDzongkhag").val(presentDzongkhag);
$("#dzongkhag").val(permanentDzongkhag);
$("#nationality").val(nationality);
populateDependentDropDown(permanentDzongkhag, 'gewog', '', 'GEWOG_LIST', 'N');
populateDependentDropDown(permanentGewog, 'village', '', 'VILLAGE_LIST', 'N');
$("#gewog").val(permanentGewog);
$("#village").val(permanentVillage);

$(document).ready(function()
		{
			$("#personalForm").validate({
				rules:
				{
					reason:{required:true},
					titleCourtesy:{required:true},
					firstName:{required:true},
					dateOfBirth:{required:true},
					gender:{required:true},
					permanentDzongkhag:{required:true},
					permanentAddress:{required:true},
					presentContactAddress:{required:true},
					presentPhoneNo:{required:true},
					presentEmail:{required:true}
					
				},    
				messages:
				{
					reason:{required:"Please Provide the Data"},
					titleCourtesy:{required:"Please Provide the Data"},
					firstName:{required:"Please Provide the Data"},
					dateOfBirth:{required:"Please Provide the Data"},
					gender:{required:"Please Provide the Data"},
					permanentDzongkhag:{required:"Please Provide the Data"},
					permanentAddress:{required:"Please Provide the Data"},
					presentContactAddress:{required:"Please Provide the Data"},
					presentPhoneNo:{required:"Please Provide the Data"},
					presentEmail:{required:"Please Provide the Data"}
				}
			});
			$('#saveData').click(function(){
				if($('#personalForm').valid())
				{
					$('#dzongkhagHidden').val($('#dzongkhag').val());
					$('#gewogHidden').val($('#gewog').val());
					var options = {target:'#submit_message',url:'<%=request.getContextPath()%>/service.html?method=edit_personal_info',type:'POST',data: $("#personalForm").serialize()}; 
				    $("#personalForm").ajaxSubmit(options);
				    $('#submit_message').show();
			        setTimeout('hideStatus("submit_message")',10000);
			        //setTimeout('reload()',5000);
				}
				else
				{
					return false;
				}
			});		
		});
		
		
</script>
<link rel="stylesheet" media="screen" href="css/screen.css">
<style>
	#personalForm .error { color: red; }
</style>