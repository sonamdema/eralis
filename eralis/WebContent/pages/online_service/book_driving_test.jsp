<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<script>
	var context = "<%=request.getContextPath()%>";

</script>
<html:form styleClass="form-horizontal" action="/service.html" styleId="bookForm">	
	<div class="widget-box" id="learner_lcns_dtls">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Learner/Driving License Details</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-lg-6 form-group">
							<div class="col-lg-6"> 
								<label for="Original CID">Name:</label>
								&nbsp;
								<span id="name"></span>
							</div> 
						</div>
						<!-- <div class="col-lg-6 form-group">
							<div class="col-lg-4">
								<label>Phone No:</label>
								&nbsp;
								<span id="phoneNo"></span>
							</div> 
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Driving Test Details</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-lg-12 form-group" id='testBooked' style="display:none">
							<div class="alert alert-danger">
								You have already booked the driving test. Report at 9:00 am with your original citizenship identity card and one day refresher course certificate(for new issuance of license).<br>Following is your booking details
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<div class="col-lg-4">
								<label   for="Original CID">Driving Test Location:</label>
							</div> 
							<div class="col-lg-3">
	                              <html:select property="testLocation" styleId="testLocation" onchange="getTestDate(this.value)"  styleClass="form-control"  >
									<html:option value="">SELECT TEST LOCATION</html:option>
									<html:optionsCollection name="baseofficeList" label="testLocation" value="testLocationId"/>
								</html:select>
							</div>  	
							<div class="col-xs-12 col-lg-1">
								<a href="#" data-toggle="tooltip"  data-html="true"  title="Select driving test location where you want to give the test"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
							</div>
							<div class="col-lg-3">
								<label  style="display:none;color: #ff0000" id="location_validation">Please select test location</label>
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<div class="col-lg-4">
								<label   for="Original CID">Driving test date:</label>
							</div> 
							<div class="col-lg-3">
								 <html:select property="testDate" styleId="testDate" styleClass="form-control" onchange="getMaxApplicants(this.value)">
									<html:option value="">SELECT TEST DATE</html:option>
								</html:select>
								
							</div>  
							<div class="col-xs-12 col-lg-1">
								<a href="#" data-toggle="tooltip"  data-html="true"  title="Select test date when you want to give the test"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
							</div>
							<div class="col-lg-4">
								<div id="applicantMessage"></div>
								<label  style="display:none;color: #ff0000" id="date_validation">Please select test date</label>
							</div>
						</div>
						 <div class="col-lg-12 form-group">
							<div class="col-lg-4">
								<label   for="Original CID">Drive Type:</label>
							</div>
							<div class="col-lg-3">
	                              <html:select property="drivetype" styleId="DRIVE_TYPE"   styleClass="form-control"  onchange="getDriveTypeTestMarks(this.value)" >
									<html:option value="">SELECT DRIVE TYPE</html:option>
									<html:optionsCollection name="DRIVE_TYPE" label="headerName" value="headerId"  />
								</html:select>
							</div>
							<div class="col-xs-12 col-lg-1">
								<a href="#" data-toggle="tooltip"  data-html="true"  title="Select drive type for which you want to give the test"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
							</div>	
							<div class="col-lg-4">
								<div id="msgDIV" style="display:none;" class="alert alert-danger"></div>
								<label  style="display:none;color: #ff0000" id="driveType_validation">Please select drive type</label>
							</div>
						</div>
						
						
						<div class="col-lg-12">
							<span class="col-lg-12" id="book_message">
							</span>
						</div>
						<div class="col-lg-12">
							<input type='hidden' id='applicantNo'>
							<input type='hidden' id='applicationNo'>
							<html:hidden property="cid" styleId="CitizenID" styleClass="form-control" ></html:hidden>
							<html:hidden property="learnerNo" styleId="vehLeanerNo" styleClass="form-control"  ></html:hidden>
							<html:hidden property="testType" styleId="testType1"></html:hidden>
							<html:hidden property="customerID" styleId="customerID"></html:hidden>
							<html:hidden property="dob" styleId="dob" styleClass="form-control" ></html:hidden>
							<div class="form-group" > 
								<div class="col-lg-10 center">
									<button type="button" id="bookButton" class="btn btn-sm  btn-primary centre" onclick="showConfirmationModal()">
										Book Driving Test
									</button>
									<button type='button' id="okButton" style="display:none;" class='btn btn-sm  btn-primary centre' onclick='closeBook()'>Ok</button>
									<!--<button type="button" class="btn btn-sm  btn-primary centre" id="cancelButton" onclick="cancelDrivingTest()">
										Cancel Driving Test
									</button>
								--></div> 
							</div>
						</div> 
						<div class="col-lg-12">
							<div class="alert" style="   background-color: #23bac9; border-color: #faebcc;color: #fffdf8;">
								Disclaimer: The driving test fee is valid only for the given test date. It can not be refunded/transfered to future test date.
							</div> 
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="confirmationModal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Confirmation</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12"> 
							<div class="form-group">
								<label class="col-lg-12" for="CID"> Are you sure, you want to proceed to payment with the following data? </label>
							</div>
							<div class="widget-main padding-24">
								
								<div style="overflow: auto;">
									<table class="table table-striped table-bordered" id="normal" style="min-width:300px">
										<tbody>
											<tr>
												<td>Driving Test Location:</td>
												<td>
													<div class="input-group">
														<input type="text" readonly="readonly" class="form-control input-mask-phone" id="confirmLocation">
													</div>
												</td>
											</tr>
											<tr>
												<td>Driving test date:</td>
												<td>
													<div class="input-group">
														<input type="text" readonly="readonly" class="form-control input-mask-phone" id="confirmTestDate">
													</div>
												</td>
											</tr>
											<tr>
												<td>Drive Type:</td>
												<td>
													<div class="input-group">
														<input type="text" readonly="readonly" class="form-control input-mask-phone" id="confirmDriveType">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
								
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm" name="search" data-dismiss="modal" onclick="bookDrivingTest()">
						Yes
					</button>
					<button class="btn btn-sm" data-dismiss="modal">
						No
					</button>
				</div>
			</div>
		</div>
	</div>
</html:form>
	<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
	
	
	
<script type="text/javascript">
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<script>
	var issueType	=	$('#issueType').val();  
	$("#dob").val('<%=request.getAttribute("dob")%>'); 
	var name	=	'<%=request.getAttribute("firstName")%> '+'<%=request.getAttribute("middleName")%> '+'<%=request.getAttribute("lastName")%>';
	var applicationNo	=	'<%=request.getAttribute("test_applicationNo")%>';
	var testDate	=	'<%=request.getAttribute("Test_Date")%>';
	var testLocation	=	'<%=request.getAttribute("Test_Location_Id")%>';
	var testDriveType	=	'<%=request.getAttribute("Drive_Type_Id")%>';
	var testMessage	=	'<%=request.getAttribute("testMessage")%>';
	var testType = '<%=request.getAttribute("TEST_TYPE")%>';
	var customerId = '<%=request.getAttribute("CUSTOMER_ID")%>';
	var bookedTestDtls	= '<%=request.getAttribute("BOOKED_TEST_DTLS")%>';
	
	var issueBasedOn	=	$("#issueBasedOn").val();
	
	$("#image").val('<%=request.getAttribute("image")%>'); 
	$('#name').html("<label class='control-label'>"+name+"</label>");
	$('#phoneNo').html("<label class='control-label'>"+'<%=request.getAttribute("presentPhoneNo")%>'+"</label>");
	$("#vehLeanerNo").val('<%=request.getAttribute("learnerNo")%>'); 
	$("#CitizenID").val('<%=request.getAttribute("cid")%>'); 
	$('#testType1').val(testType);
	$('#customerID').val(customerId);

	
	 if(issueType=='Arm_Force' && issueBasedOn=='OTHERS')
	{
		$('#DRIVE_TYPE option:contains("Tourist Medium Bus")').remove();
		$('#DRIVE_TYPE option:contains("Tourist Light Vehicle")').remove();
		$('#DRIVE_TYPE option:contains("Tourist Heavy Bus")').remove();
		$('#DRIVE_TYPE option:contains("Taxi")').remove();
		$('#DRIVE_TYPE option:contains("Road Roller")').remove();
		$('#DRIVE_TYPE option:contains("Road Paver")').remove();
		$('#DRIVE_TYPE option:contains("Pay Loader")').remove();
		$('#DRIVE_TYPE option:contains("Medium Bus")').remove();
		$('#DRIVE_TYPE option:contains("Heavy Bus")').remove();
		$('#DRIVE_TYPE option:contains("Excavator")').remove();
		$('#DRIVE_TYPE option:contains("Bull Dozer")').remove();
		$('#DRIVE_TYPE option:contains("Power Tiller")').remove();
		$('#DRIVE_TYPE option:contains("Tractor")').remove();
		
	}
	else if(issueType=='Driving_Institue' && issueBasedOn=='OTHERS')
	{
		$('#DRIVE_TYPE option:contains("Tourist Medium Bus")').remove();
		$('#DRIVE_TYPE option:contains("Tourist Light Vehicle")').remove();
		$('#DRIVE_TYPE option:contains("Tourist Heavy Bus")').remove();
		$('#DRIVE_TYPE option:contains("Taxi")').remove();
		$('#DRIVE_TYPE option:contains("Road Roller")').remove();
		$('#DRIVE_TYPE option:contains("Road Paver")').remove();
		$('#DRIVE_TYPE option:contains("Pay Loader")').remove();
		$('#DRIVE_TYPE option:contains("Medium Vehicle")').remove();
		$('#DRIVE_TYPE option:contains("Medium Bus")').remove();
		$('#DRIVE_TYPE option:contains("Heavy Vehicle")').remove();
		$('#DRIVE_TYPE option:contains("Heavy Bus")').remove();
		$('#DRIVE_TYPE option:contains("Excavator")').remove();
		$('#DRIVE_TYPE option:contains("Bull Dozer")').remove();
		$('#DRIVE_TYPE option:contains("Power Tiller")').remove();
		$('#DRIVE_TYPE option:contains("Tractor")').remove();
	}
	  
		
	if(bookedTestDtls!=0)
	{
		var testDtlsArray	=	bookedTestDtls.split("#");
		var driveTypeID	=	testDtlsArray[0];
		var locationId	=	testDtlsArray[1];
		var testDate	=	testDtlsArray[2];
		var jurisTypeId	=	testDtlsArray[3];
	 	$('#testDate').empty();
		$("#testDate").append("<option>"+testDate+"</option>");
		$('#DRIVE_TYPE').val(driveTypeID);
		$('#testLocation').val(jurisTypeId+"#"+locationId).attr("selected", "selected");
		$("#applicationNo").val(applicationNo);
		
		$('#testDate').attr('disabled', true);
		$('#testLocation').attr('disabled', true);
		$('#DRIVE_TYPE').attr('disabled', true);
		
		$("#bookButton").hide();
		$("#cancelButton").show();
		$("#testBooked").show();
		
	}
	else
	{
		$("#testBooked").hide();
		$("#bookButton").show();
		$("#cancelButton").hide();

		$('#testDate').attr('disabled', false);
		$('#testLocation').attr('disabled', false);
		$('#DRIVE_TYPE').attr('disabled', false); 
	}
	if(testMessage==1)
	{
		$("#okButton").show();
		$('#book_message').html("<div  class='col-lg-12 alert alert-block alert-success'> <label class='control-label' ><i class='ace-icon fa fa-check'></i>"+'Booking Successfull: Report at 9:00 am with your original citizenship identity card and one day refresher course certificate (for new issuance of license)'+"</label></div>");
	}
	else if(testMessage==0)
	{
		$('#book_message').html("<div  class='col-lg-12 alert alert-block alert-success'><label class='control-label'><i class='ace-icon fa fa-check'></i>"+'Cancellation Successfull'+"</label></div>");
	}

	function getTestDate(testlocationId)
	{
		var issueType	=	$("#issueType").val();
		var issueBasedOn	=	$("#issueBasedOn").val();
		
		if(issueType=='Driving_Institue' && issueBasedOn=='OTHERS')
		{
			issueType='DRIVING_INSTITUTE';
		}
		else
		{
			issueType='ORDINARY';
		}
		$('#locationMessage').hide();
		if(testlocationId!='')
		{ 
			var tempArr = new Array();
			tempArr = testlocationId.split("#");
			var jurisTypeId = tempArr[0];
			var locationId = tempArr[1];

			var url = "<%=request.getContextPath()%>/EralisCommonServlet?q=getTestDate&locationId="+locationId+"&jurisTypeId="+jurisTypeId+"&issueType="+issueType;
			
			$.ajax({	
				type: "GET",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data) 
				{
					$("#testDate").empty();
					$("#testDate").append("<option value=''>SELECT TEST DATE</option>");
					for (var i = 0; i < data.length; i++)
					{
						if(issueType=='DRIVING_INSTITUTE')
						{
							$("#testDate").append("<option value=" + data[i].testDateId + ">"+ data[i].testDate +" ("+data[i].instituteName+")"+ "</option>");
						}
						else
						{
							$("#testDate").append("<option value=" + data[i].testDateId + ">"+ data[i].testDate + "</option>");
						}
				    }
				},
				error : function(jqXHR, textStatus, errorThrown) {		
					//alert(textStatus);
				}
			});
		}
		else
		{
			$("#testDate").val('');
		}
	}

	function getMaxApplicants(testDate)
	{
		$("#DRIVE_TYPE").val("");
		if(testDate!="")
		{
			$('#bookButton').attr('disabled', false);
			$('#DRIVE_TYPE').attr('disabled', false);
			$('#applicantMessage').hide();
			$('#date_validation').hide();
			var locationArray = $('#testLocation').val().split("#");
			var testJurisTypeId	=	locationArray[0];
			var testLocation	=	locationArray[1];
			var issueType	=	$("#issueType").val();
			var learnerNo	=	$("#learnerNo").val();
			var issueType	=	$("#issueBasedOn").val();
			
			if(issueType=='OTHERS')
			{
				var issueType	=	$("#issueType").val();
			}
			
			
			$.ajax
			({
				async: true,
				type: 'GET',
				url : "<%=request.getContextPath()%>/EralisCommonServlet?q=getMaxApplicants&testDate="+testDate+"&testLocation="+testLocation+"&issueType="+issueType+"&learnerNo="+learnerNo+"&customerId="+customerId+"&testJurisTypeId="+testJurisTypeId+"&searchBy=CITIZEN",
				success : function(responseText) 
				{ 
					$(responseText).find('xml-response').each(function()
					{ 
						var flag = $(this).find('flag').text();	
						var maturity = $(this).find('maturity').text();	
						var applicantNo = $(this).find('applicantNo').text();	
						var maxApplicant = $(this).find('max-applicant').text();

						if(flag==1)
						{
							$('#bookButton').attr('disabled', true);
							$('#applicantMessage').show();
							$('#applicantMessage').html("<div class='alert alert-danger'>"+"Sorry! You must be years 18 old or more</div>");
						}
						else if(flag==2)
						{
							$('#bookButton').attr('disabled', true);
							$('#applicantMessage').show();
							$('#applicantMessage').html("<div class='alert alert-danger'>"+"Sorry! You are over age</div>");
						}
						else if(flag==3)
						{
							$('#bookButton').attr('disabled', true);
							$('#applicantMessage').show();
							$('#applicantMessage').html("<div class='alert alert-danger'>"+"Sorry! You must maintain minimun "+maturity+" gap.</div>");
						}
						else if(parseInt(applicantNo) >= parseInt(maxApplicant))
						{
							$('#bookButton').attr('disabled', true);
							$('#DRIVE_TYPE').attr('disabled', true);
							$('#applicantMessage').show();
							$('#applicantMessage').html("<div class='alert alert-danger'>"+"Sorry! All seats are booked. Either choose another test date or contact nearest RSTA office for help.</div>");
						}
						 
					});
				}
			});
		}
		
	}
	function showConfirmationModal()
	{
		openModal('confirmationModal');
		var confirmLocation = $("#testLocation :selected").text();
		var	confirmTestDate = $("#testDate :selected").text();
		var	confirmDriveType = $("#DRIVE_TYPE :selected").text();

		$("#confirmLocation").val(confirmLocation);
		$("#confirmTestDate").val(confirmTestDate);
		$("#confirmDriveType").val(confirmDriveType);
		
		
	}
	function bookDrivingTest()
	{ 
		$('#bookButton').prop('disabled', true);
		var learnerNo	=	$("#learnerNo").val();
		var cid	=	$("#cid").val();
		var issueType	=	$("#issueType").val();
		var applicantNo	=	$("#applicantNo").val();
		var testLocation	=	$("#testLocation").val();
		var testDate	=	$("#testDate").val();
		var testType	=	$("#testType").val();
		var DRIVE_TYPE	=	$("#DRIVE_TYPE").val();
		var issueBasedOn	=	$('#issueBasedOn').val();
		
		
		

		$('#location_validation').hide();
		$('#date_validation').hide();
		$('#driveType_validation').hide();
		
		var validation	=	1;
		if(testLocation=="")
		{ 
			$('#location_validation').show();
			validation	=0;
		}
		if(testDate=="")
		{ 
			$('#date_validation').show();
			validation	=0;
		}
		if(DRIVE_TYPE=="")
		{
			$('#driveType_validation').show();
			validation	=0;
		}
		if(validation==1)
		{
			if(Number(applicantNo)<50 )
			{ 
				$.ajax
				({
					type : "POST",
					url:"<%=request.getContextPath()%>/service.html?method=bookDrivingTest&issueType="+issueType+"&cid="+cid+"&learnerNo="+learnerNo+"&testType="+testType+"&issueBasedOn="+issueBasedOn,
					data : $("#bookForm").serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#book_test_div").html(responseText);
						$('#bookButton').prop('disabled', true);
					}
				});
			} 
		}
	}
	function closeBook()
	{
		hideStatus("book_message");
		$('#book_test_div').hide(); 
	}
	function cancelDrivingTest()
	{
		var applicationNo =	$("#applicationNo").val();
		var testType =	$("#testType").val();
		if(applicationNo != '')
		{ 
			$.ajax
			({
				type : "POST",
				url:"<%=request.getContextPath()%>/service.html?method=cancelDrivingTest&applicationNo="+applicationNo+"&testType="+testType,
				data : $("#bookForm").serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#book_test_div").html(responseText);
					$("#book_test_div").show();
					setTimeout('hideStatus("book_message")',2000);
					setTimeout( "$('#book_test_div').hide();", 2000); 
					
				}
			}); 
		}
	}

	function hideStatus(id)
	{
		$('#'+id).hide();
	}

	function getDriveTypeTestMarks(driveTypeId)
	{
		$("#msgDIV").hide();
		$("#driveType_validation").hide();
		var licenseNO	=	$("#learnerNo").val();
		var testType	=	$("#testType").val();
		var testDate	=	$("#testDate").val();
		var customerID	=	$("#customerID").val();
		var issueType	=	$("#issueBasedOn").val();
		
		if(issueType=='OTHERS')
		{
			var issueType	=	$("#issueType").val();
		}
		/* if(testType=='ENDORSEMENT')
		{ */
			$.ajax
			({
				async: true,
				type: 'POST',
				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getDriveTypeTestMarks&driveTypeId='+driveTypeId+'&licenseNO='+licenseNO+'&formType=booking&testDate='+testDate+'&customerID='+customerID+'&issueType='+issueType,
				success: function(xml)
				{ 
					$(xml).find('xml-response').each(function()
					{
						var flag = $(this).find('flag').text();
						var testMark = $(this).find('testMark').text();
						var reason	=	 $(this).find('reason').text();
						if(flag=='0')
						{
							$('#msgDIV').html(reason);
							$("#msgDIV").show();
							$('#bookButton').attr('disabled',true);
						}
						else
						{
							 $('#bookButton').attr('disabled',false);
						}
						$("#testMark").val(testMark);
						 
					});
				}
			});
		//}
	}
	$(document).ready(function()
	{
	
		$("#learner_lcns_dtls").show();	
		/* if(issueType=="Learner_License" || issueType=="Driving_Institue" || issueType=="DL" )  
		{
			$("#learner_lcns_dtls").show();	
		}
		else
		{
			$("#learner_lcns_dtls").hide();
		} */
	});
</script>