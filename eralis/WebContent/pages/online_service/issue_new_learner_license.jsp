<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
		String regionId = (String) request.getAttribute("REGION_ID");
		String regionName = (String) request.getAttribute("REGION_NAME");
	%>
	<script>
		var context = "<%=request.getContextPath()%>";
		var nextID	=	1;
	</script>

</head>
<body class="no-skin">
<!-- header include -->
	<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
	<!-- ./header include -->
	 <div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div>
										<ul class="steps">
											<li data-step="1" class="active">
												<span class="step">1</span>
												<span class="title">Personal Information</span>
											</li>
	
											<li data-step="2">
												<span class="step">2</span>
												<span class="title">Application Details</span>
											</li>
											<li data-step="3">
												<span class="step">3</span>
												<span class="title">Payment Info</span>
											</li>
	 										<li data-step="4">
												<span class="step">4</span>
												<span class="title">Make Payment</span>
											</li>
	 
										</ul>
									</div>
	
									<hr />
									<div class="step-content pos-rel">
										<div class="step-pane active" data-step="1">  
											<div class="col-lg-12 row">
												<html:form styleClass="form-horizontal" action="/service.html?method=personal_info" styleId="personalForm">	    
													<div class="widget-box">
														<div class="widget-header widget-header-small">
															<h5 class="widget-title lighter">Personal Information</h5>
															
														</div>
														<div class="widget-body">
															<div class="widget-main">
																<div class="row">
																	<div class="col-lg-12">
																		<div class="pull-right">
																			<div id="pic" style="display:none;"></div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-lg-12">
																		<html:hidden property="customerID" styleClass="form-control" styleId="customerID"></html:hidden>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Citizen ID <i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<div class="input-group">
																					<html:text property="CID" styleClass="form-control" styleId="cid"></html:text>
																					<span class="input-group-btn">
																						<button type="button" class="btn btn-purple btn-sm" onclick="getCustomerDtls()">
																							Search
																						</button>
																					</span>
																				</div>
																			</div>
																			<div class="col-lg-7">
																				<div id="msgDiv">
																					<div class="alert alert-info">
																						Pull new PIS record from Census
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Title of Courtesy<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3" >
																				<html:select property="titleOfcourtesy"
																					styleClass="form-control" styleId="titleOfcourtesy">
																					<html:option value="">--SELECT--</html:option>
																					<html:optionsCollection name="titleOfcourtesyList"
																						label="headerName" value="headerId"  />
																				</html:select>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>First Name<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-2" >
																				<html:text property="firstname" styleId="firstname"
																					styleClass="form-control" readonly="true"></html:text>
																			</div>
																			<div class="col-lg-2">
																				<label>Middle Name:</label>
																			</div>
																			<div class="col-lg-2" >
																				<html:text property="middlename" styleId="middlename"
																					styleClass="form-control" readonly="true"></html:text>
																			</div>
																			<div class="col-lg-2">
																				<label>Last Name:</label>
																			</div>
																			<div class="col-lg-2" >
																				<html:text property="lastName" styleId="lastName"
																					styleClass="form-control" readonly="true"></html:text>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2" >
																				<label>Occupation<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:select property="occupation" styleClass="form-control" styleId="occupation">
																					<html:option value="">--SELECT--</html:option>
																					<html:optionsCollection name="occupationList"
																						label="headerName" value="headerId" />
																				</html:select>
																			</div>
																			<div class="col-lg-2">
																				<label>Nationality<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:select property="nationality" styleClass="form-control" styleId="nationality">
																					<html:option value="-1">--SELECT--</html:option>
																					<html:optionsCollection name="nationalityList" label="headerName"
																						value="headerId" />
																				</html:select>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Date Of Birth<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<div class="input-group">
																					<html:text property="DOB"
																						styleClass="form-control date-picker"
																						styleId="displayDob" readonly="true"></html:text>
																					<span class="input-group-addon"> <i
																						class="fa fa-calendar bigger-110"></i> </span>
																				</div>
																			</div>
																			<div class="col-lg-2">
																				<label>Father's Name<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:text property="fathersName" styleId="fathersName"
																					styleClass="form-control" readonly="true"></html:text>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Blood Group<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:select property="bloodGroup" styleClass="form-control" styleId="bloodGroup">
																					<html:option value="">--SELECT--</html:option>
																					<html:optionsCollection name="bloodgroupList" label="headerName"
																						value="headerId" />
																				</html:select>
																				
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Gender<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:radio property="gender" value="M" styleId="maleRadio" styleClass="ace">
																					<span class="lbl"> Male</span>
																				</html:radio>
																				<html:radio property="gender" value="F" styleId="femaleRadio" styleClass="ace">
																					<span class="lbl"> Female</span>
																				</html:radio>
																			</div>
																		</div>
																		<div class="form-group">
																			<div style="display: none;">
																				<div class="col-lg-2">
																					<label>Identification Marks:</label>
																				</div>
																				<div class="col-lg-3">
																					<html:text property="identificationMarks"
																						styleId="identificationMarks" styleClass="form-control" value="NA"></html:text>
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Remarks:</label>
																			</div>
																			<div class="col-lg-3">
																				<html:textarea property="remarks" styleId="remarks"
																					styleClass="form-control"></html:textarea>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
											
													<div class="widget-box">
														<div class="widget-header widget-header-small">
															<h5 class="widget-title lighter">Permanent Address</h5>
														</div>
														<div class="widget-body">
															<div class="widget-main">
																<div class="row">
																	<div class="col-lg-12">
																		<div class="form-group">
																			<div class="radio">
																				<label class="col-sm-2 no-padding-right">
																					<html:radio property="national" styleId="nationalRadio" onclick="enable('National')" styleClass="ace"
																						value="N"></html:radio> <span class="lbl"> National</span> </label>
																				<label class="col-sm-2 no-padding-right">
																					<html:radio property="national" styleId="internationalRadio" onclick="enable('International')" styleClass="ace"
																						value="I"></html:radio> <span class="lbl">
																						International</span> </label>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Dzongkhag<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<select id="dzongkhag" class="form-control"  onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N')" >
																					<option value="">--SELECT--</option>
																					<logic:iterate id="dzongkhag" name="dzongkhagList">
																						<option value='<bean:write name="dzongkhag" property="headerId"/>'><bean:write name="dzongkhag" property="headerName"/></option>
																					</logic:iterate>
																				</select>
																			</div>
																			<div id="requiredDzongkhag" style="display:none;" class="error">Please select Dzongkhag</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Gewog<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<select id="gewog" class="form-control">
																					<option value="">--SELECT--</option>
																				</select>
																			</div>
																			<div  id="requiredGewog" style="display:none;" class="error">Please select Gewog</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Village<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:text property="village" styleId="village"
																					styleClass="form-control" readonly="true"></html:text>
																			</div>
																			<div   class="error" id="requiredVillage" style="display:none;" >Please select Village</div>
																			 
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Country:</label>
																			</div>
																			<div class="col-lg-3">
																				<html:select property="country" styleClass="form-control" styleId="country">
																					<html:option value="">--SELECT--</html:option>
																					<html:optionsCollection name="countryList" label="headerName"
																						value="headerId" />
																				</html:select>
																			</div>
																			<div id="requiredCountry" style="display:none;" class="error">Please select Country</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Address:</label>
																			</div>
																			<div class="col-lg-3">
																				<html:text property="address" styleId="address"
																					styleClass="form-control"></html:text>
																			</div>
																			<div id="requiredAddress" style="display:none;"  class="error">Please enter Address</div>
																		</div>
											
																	</div>
																</div>
															</div>
														</div>
													</div>
											
													<div class="widget-box">
														<div class="widget-header widget-header-small">
															<h5 class="widget-title lighter">Contact Address</h5>
														</div>
														<div class="widget-body">
															<div class="widget-main">
																<div class="row">
																	<div class="col-xs-12">
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Dzongkhag<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:select property="presentDzongkhag" styleClass="form-control" styleId="presentDzongkhag">
																					<html:option value="">--SELECT--</html:option>
																					<html:optionsCollection name="dzongkhagList" label="headerName"
																						value="headerId" />
																				</html:select>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Contact address<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:text property="contactAddress" styleId="contactAddress"
																					styleClass="form-control"></html:text>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Phone<i class="light-red">*</i> :</label>
																			</div>
																			<div class="col-lg-3">
																				<html:text property="phone" styleId="phone"
																					styleClass="form-control"></html:text>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Email:</label>
																			</div>
																			<div class="col-lg-3">
																				<html:text property="email" styleId="email"
																					styleClass="form-control"></html:text>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-lg-2">
																				<label>Upload Photo:</label>
																			</div>
																			<div class="col-lg-3">
																				<html:file property="upload" styleClass="form-control"
																					styleId="id-input-file-2"></html:file>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
											
														</div>
													</div>
													
													<div id="messageDiv"></div>
													
													<div class="pull-right">
														<html:hidden property="dzongkhag" styleId="dzongkhagHidden"/>
														<html:hidden property="personalInfoId" styleId="personalInfoId"/>
														<html:hidden property="gewog" styleId="gewogHidden"/>
														<html:hidden property="manualFlag" styleId="manualFlag" value="N"/>
											
														
													</div>
													
												</html:form>
											</div>
										</div>
										<div class="step-pane" data-step="2">
											<div class="row">
												<html:form styleClass="form-horizontal" action="/service.html" styleId="serviceForm">
													<div class="row">
														<div class="widget-box">
															<div class="widget-header widget-header-small">
																<h5 class="widget-title lighter">Learner License Details</h5>
															</div>
															<div class="widget-body">
																<div class="widget-main">
																	<div class="row">
																		<div class="col-lg-12">
																		<div class="form-group"> 
															                    <div class="col-lg-2">
																					<label>Region <span style="color: #ff0000">*</span>:</label>
																			    </div>	
																		        <div class="col-sm-3" >
																		        	<html:select property="region" styleClass="form-control" styleId="learnerRegionId" onchange="populateDependentDropDown(this.value, 'baseOffice', '', 'ONLINE_PAYMENT_BASE_OFFICE_LIST', 'N')" >
																						<html:option value="">--SELECT--</html:option>
																				   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
																				   </html:select>
																					<label style="display:none;color: #ff0000" id="regionValidation">Please Select Region</label>
																                </div>
															                    <div class="col-lg-2">
																					<label>Base :</label>
																			    </div>	
																		        <div class="col-sm-3" >
																		        	<html:select property="baseoffice" styleClass="form-control" styleId="baseOffice" >
																						<html:option value="">--SELECT--</html:option>
																				   </html:select>
																                </div>
															           		</div>
																			<div class="form-group">
																				<div class="col-lg-2">
																					<label>Certifying Doctor:</label>
																				</div>
																				<div class="col-lg-3">
																					<html:text property="certifyingDoctor" styleId="certifyingDoctor"
																						styleClass="form-control"></html:text>
																				</div>
																			</div>
																			<div class="form-group">
																					<div class="col-lg-2">
																						<label>Drivetypes:</label>
																					</div>
																					<div class="col-sm-10">
																						<logic:notEmpty name="DRIVE_TYPE_LIST">
																							<logic:iterate id="driveType" name="DRIVE_TYPE_LIST" type="bt.gov.rsta.framework.dto.DropDownDTO" indexId="index">
																								<%
																									int a = index.intValue();
																								%>
																								<div class="checkbox">
																									<label>
																										<input type="checkbox" class="ace" id='<%="driveTypeId_"+a %>' value="<%=driveType.getHeaderId() %>"/>
																										<span class="lbl"> <bean:write name="driveType" property="headerName"/></span>
																									</label>
																								</div>
																							</logic:iterate>
																						</logic:notEmpty>
																					</div>
																				</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="widget-box">
															<div class="widget-header widget-header-small">
																<h5 class="widget-title lighter">Attachments</h5>
															</div>
															<div class="widget-body">
																<div class="widget-main">
																	<div class="row">
																		<div class="col-lg-12">
																			
																			<div class="form-group">
																				<div class="col-lg-2">
																					<label>Medical Certificate<span style="color: #ff0000">*</span>:</label>
																				</div>
																				<div class="col-lg-3">
																					<html:file property="fileMC" styleId="fileMC" styleClass="form-control fileupload" onchange="validation('learnerForm','fileMCValidation',this)"></html:file>
																					<label style="display:none;color: #ff0000" id="fileMCValidation">Please attach your file here</label>
																				</div>
																				<div class="col-lg-2">
																					<label>Application Form<span style="color: #ff0000">*</span>:</label>
																				</div>
																				<div class="col-lg-3">
																					<html:file property="fileApplForm" styleId="fileApplForm" styleClass="form-control fileupload" onchange="validation('learnerForm','fileApplFormValidation',this)"></html:file>
																					<label style="display:none;color: #ff0000" id="fileApplFormValidation">Please attach your file here</label>
																				</div>
																				
																			</div>
																			
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</html:form>
											</div>
										</div>
										<div class="step-pane" data-step="3">
											<div class="center">
												<jsp:include page="/pages/online_service/online_payment_info.jsp"></jsp:include>
											</div>
										</div>
										<div class="step-pane" data-step="4">
											<div class="center">
												<div id="online_receipt"></div>
											</div>
										</div>
									</div>
								</div>
	
								<hr />
								<div class="wizard-actions">
									<button type="button" class="btn btn-success btn-next" onClick="onClickNextButton()"  data-last="Finish">
											Next
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		</div>
	<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
</div><!-- /.main-container -->
<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace.min.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>

<script type="text/javascript">
	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	//radio button
	$(document).ready(function() {
		$('#nationalRadio').attr('checked', true);
		$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);
	});
	var nextID	=	1;
	function onClickNextButton()
	{  
		$("#regionValidation").hide();
		if(nextID==1)
		{
			savePersonalDtls();
		}
		else if(nextID==2)
		{
			var learnerRegionId	=	$("#learnerRegionId").val();
			if(learnerRegionId=="")
			{
				$("#regionValidation").show();
				nextID=1;
			}
			else
			{
				getPaymentInfo();
			}
		}
		else if(nextID==3)
		{
			formSubmit();
		}
		nextID	=	nextID+1; 
	}
	//radio button
	function enable(identifier)
	{
		if("National" == identifier)
		{
			$('#country').attr('disabled', true);
			$('#address').attr('disabled', true);

			$('#dzongkhag').attr('disabled', false);
			$('#gewog').attr('disabled', false);
			$('#village').attr('disabled', false);
		}
		else
		{
			$('#country').attr('disabled', false);
			$('#address').attr('disabled', false);

			$('#dzongkhag').attr('disabled', true);
			$('#gewog').attr('disabled', true);
			$('#village').attr('disabled', true);
		}
	}
</script>

<script>
function getPaymentInfo()
{
	var requestType = "LEARNER";
	var serviceType = "NEW";
	var identityNo = "";
	var identityTypeId = "L";
	$("#penaltyForm").hide();
	getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '');
}

function formSubmit()
{
	var amount = $('#amount').val();
	var cid = $('#cid').val();
	var options = {target:'#online_receipt',url:context+'/service.html?method=learnerLicenseOnlinePayment&formType=LL_NEW&pageId=45&amount='+amount+'&cid='+cid,type:'POST',data: $("#serviceForm").serialize()}; 
    $("#serviceForm").ajaxSubmit(options);
}
function savePersonalDtls()
{	
	var country	=	$("#country").val();
	var address	=	$("#address").val();
	var dzongkhag	=	$("#dzongkhag").val();
	var gewog	=	$("#gewog").val();
	var village	=	$("#village").val();

	$("#requiredDzongkhag").hide();
	$("#requiredGewog").hide();
	$("#requiredVillage").hide();
	$("#requiredCountry").hide();
	$("#requiredAddress").hide();
	
	if($("#nationalRadio").prop("checked"))
	{
		if(dzongkhag=='')
		{
			$("#requiredDzongkhag").show();
		}
		if(gewog=='')
		{
			$("#requiredGewog").show();
		}
		if(village=='')
		{
			$("#requiredVillage").show();
		}
	}
	else if($("#internationalRadio").prop("checked"))
	{
		if(country=='')
		{
			$("#requiredCountry").show();
		}
		if(address=='')
		{
			$("#requiredAddress").show();
		}
	}
	if($('#personalForm').valid()) 
	{
		if($("#nationalRadio").prop("checked"))
		{
			if(dzongkhag==''|| gewog=='' || village=='')
			{
				return false;
			}
		}
		else  if($("#internationalRadio").prop("checked"))
		{
			if(country==''|| address=='')
			{
				return false;
			}
		}
		$('#submitBtn').attr('disabled',true);
		
		if($('#manualFlag').val() == "Y")
		{
			$('#dzongkhagHidden').val($('#dzongkhag').val());
			$('#gewogHidden').val($('#gewog').val());
		}
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=personal_info&userType=CITIZEN',type:'POST',data: $("#personalForm").serialize()}; 
	    $("#personalForm").ajaxSubmit(options);
        $('#messageDiv').show();
        $('#messageDiv').get(0).scrollIntoView();
	}
	
}

</script>
<script type="text/javascript">
			jQuery(function($) {
				$('[data-rel=tooltip]').tooltip();
				$(".select2").css('width','200px').select2({allowClear:true})
				.on('change', function(){
					$(this).closest('form').validate().element($(this));
				}); 
				var $validation = false;
				$('#fuelux-wizard-container')
				.ace_wizard({
					//step: 2 //optional argument. wizard will jump to step "2" at first
					//buttons: '.wizard-actions:eq(0)'
				})
				.on('actionclicked.fu.wizard' , function(e, info){
					if(info.step == 1 && $validation) {
						if(!$('#validation-form').valid()) e.preventDefault();
					}
				})
				.on('finished.fu.wizard', function(e) {
					bootbox.dialog({
						message: "Thank you! Your information was successfully saved!", 
						buttons: {
							"success" : {
								"label" : "OK",
								"className" : "btn-sm btn-primary"
							}
						}
					});
				}).on('stepclick.fu.wizard', function(e){
				});
				$('#skip-validation').removeAttr('checked').on('click', function(){
					$validation = this.checked;
					if(this.checked) {
						$('#sample-form').hide();
						$('#validation-form').removeClass('hide');
					}
					else {
						$('#validation-form').addClass('hide');
						$('#sample-form').show();
					}
				});
				 
				
				
				$('#modal-wizard-container').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
				
				$(document).one('ajaxloadstart.page', function(e) {
					$('[class*=select2]').remove();
				});
			})
		</script>
		<script>
		function getCustomerDtls()
		{
			$('#submitBtn').show();
			$('#updateBtn').hide();
			var value = $('#cid').val();
			
			var manualFlag = $('#manualFlag').val();
			$('#msgDiv').html("");
			$('#msgDiv').hide();
			
			if("" == value)
			{
				$('#msgDiv').html("<div class='alert alert-danger'>Please enter a valid CID Number</div>");
				$('#errorMsg').show();
				$("#cid").focus();
				$('#firstname').val('');
				$('#middlename').val('');
				$('#lastName').val('');
				$('#displayDob').val('');
				$('#fathersName').val('');
				$('#dzongkhag').val('');
				$('#gewog').val('');
				$('#dzongkhagHidden').val('');
				$('#gewogHidden').val('');
				$('#village').val('');
			}
			else if(manualFlag == "N" || previousCidNumber != $('#cid').val())
			{
				$.blockUI
		        ({ 
		        	css: 
		        	{ 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
		        	} 
		        }); 
				
				$.ajax({
					async: true,
					type: 'GET',
					url: context+'/EralisCommonServlet?q=getCidDetails&cid='+value,	
					success: function(xml)
					{ 
							$(xml).find('xml-response').each(function()
							{ 
								var firstName = $(this).find('first-name').text();
								var middleName = $(this).find('middle-name').text();
								var lastName = $(this).find('last-name').text();
								var dob = $(this).find('dob').text();
								var fatherName = $(this).find('fatherName').text();
								var gender = $(this).find('gender').text(); 
								var dzongkhagId = $(this).find('dzongkhagId').text();
								var gewogId = $(this).find('gewogId').text();				
								var village = $(this).find('village').text();
								
								$('#firstname').val(firstName);
								$('#middlename').val(middleName);
								$('#lastName').val(lastName);
								$('#displayDob').val(dob);
								$('#fathersName').val(fatherName);
								$('#dzongkhag').val(dzongkhagId);
								$('#nationality').val(value);
								
								populateDependentDropDown(dzongkhagId, 'gewog', '', 'GEWOG_LIST', 'N');
								
								$('#gewog').val(gewogId);
								$('#village').val(village);
								$('#dzongkhagHidden').val(dzongkhagId);
								$('#gewogHidden').val(gewogId);
								 
								if(null != gender || "null" != gender || "" != gender)
								{
									if(gender == "M")
										$('#maleRadio').attr("checked", true);
									else
										$('#femaleRadio').attr("checked", true);
								}
							
								if("" == firstName || "" == dob)
								{
									$('#msgDiv').html("<div class='alert alert-info'>No Record Found, Fill up the form manually</div>");
									$('#msgDiv').show();
									$("#cid").focus();
									
									$('#firstname').val('');
									$('#middlename').val('');
									$('#lastName').val('');
									$('#displayDob').val('');
									$('#fathersName').val('');
									$('#dzongkhag').val('');
									$('#gewog').val('');
									$('#village').val('');
									$('#dzongkhagHidden').val('');
									$('#gewogHidden').val('');
									
									$('#manualFlag').val("Y");
									$('#name').attr('readonly', false);
									$('#firstname').attr('readonly', false);
									$('#middlename').attr('readonly', false);
									$('#lastName').attr('readonly', false);
									$('#displayDob').attr('readonly', false);
									$('#fathersName').attr('readonly', false);
									$('#dzongkhag').attr('disabled', false);
									$('#gewog').attr('disabled', false);
									$('#village').attr('readonly', false);
								}
								else
								{
									checkPrevApply();
								}
								
								setTimeout($.unblockUI, 1000); 
						});
				},
				error: function(data, textStatus, errorThrown) {
					previousCidNumber = $('#cid').val();
					
					checkPrevApply();
					
					$("#cid").focus();
					$('#firstname').val('');
					$('#middlename').val('');
					$('#lastName').val('');
					$('#displayDob').val('');
					$('#fathersName').val('');
					$('#dzongkhag').val('');
					$('#gewog').val('');
					$('#village').val('');
					$('#dzongkhagHidden').val('');
					$('#gewogHidden').val('');
					setTimeout($.unblockUI, 1000); 
					
					//if there is connection problem with the census webservice
					//then we implement manual process flow where students can fill in 
					//their information manually
					$('#msgDiv').html("<div class='alert alert-info'><strong>NOTE:</strong>Your CID information could not be fetched, thus we recommend you to fill up the form manually</div>");
					$('#msgDiv').show();
					$('#manualFlag').val("Y");
					$('#name').attr('readonly', false);
					$('#firstname').attr('readonly', false);
					$('#middlename').attr('readonly', false);
					$('#lastName').attr('readonly', false);
					$('#displayDob').attr('readonly', false);
					$('#fathersName').attr('readonly', false);
					$('#dzongkhag').attr('disabled', false);
					$('#gewog').attr('disabled', false);
					$('#village').attr('readonly', false);
				}
			});
		   }
		}




		</script>
	</body>
</html>