<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%
	ServiceDTO dto = (ServiceDTO)request.getAttribute("serviceDTO");
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<html:form styleClass="form-horizontal" action="/service.html" styleId="learnerForm">
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"><%=dto.getName()%></div>
								<div class="col-lg-2">
									<label>Customer ID:</label>
								</div>
								<div class="col-lg-4" id="displayCustomerId"><%=dto.getCustomerID()%></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"><%=dto.getDOB()%></div>
								 
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Identification Marks:</label>
								</div>
								<div class="col-lg-4">
									<%=dto.getIdentificationMarks()%>
								</div>
								 
							</div>
								
							<div class="form-group">
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"><%=dto.getGender()%></div>
								<div class="col-lg-2">
									<label>Nationality:</label>
								</div>
								<div class="col-lg-3">
									<%=dto.getNationality()%>
								</div>
								
							</div>
							
							<h4>Permanent Address(National)</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"><%=dto.getDzongkhag()%></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"><%=dto.getGewog()%></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" id="displayVillage"><%=dto.getVillage()%></div>
								
							</div>
							<h4>Permanent Address(Non-National)</h4>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-3">
									<%=dto.getCountry()%>
								</div>
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-3">
									<%=dto.getAddress()%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Endorsement Lists</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div id="endorsementlistTable">
					</div>
				</div>
			</div>
		</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Endorsed Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="endorseddate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-lg-2">
									<label>Receipt No<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="receiptNo" styleId="receiptNo"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Receipt Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="receiptDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-lg-2">
									<label>Test Marks:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="testmarks" styleId="testmarks"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								
								
								<div class="col-lg-2">
									<label>IID:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="iid" styleId="iid"
										styleClass="form-control"></html:text>
								</div>
								
							</div>
							<div class="form-group">
									
									<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
										<label>Drive Types:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="drivetype"
											styleClass="form-control" styleId="drivetype">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="DRIVE_TYPE_LIST"
												label="headerName" value="headerId"  /> 
										</html:select>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="displayMsgDiv">&nbsp;</div>
		<div class="pull-right">
			<html:text property="pageId" styleId="pageId" /> 
			<html:text property="region" value="<%=dto.getRegion() %>" styleId="region"/> 
			<html:text property="customerId" value="<%=dto.getCustomerID() %>" styleId="learnerCustomerId"/> 
			<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Save</button>
			<button type="reset" class="btn btn-primary btn-sm">Refresh</button>
			<button class="btn btn-primary btn-sm">Delete</button>
			<button class="btn btn-primary btn-sm">New</button>
			<button class="btn btn-primary btn-sm">Close</button>
			<button class="btn btn-primary btn-sm">Find</button>
		</div>
		</html:form>
		<script>
$(document).ready(function()
		{
			$('#submitBtn').click(function(){
				if($('#learnerForm').valid()) 
				{
					var options = {target:'#displayMsgDiv',url:context+'/service.html?method=driving_license_endorsement',type:'POST',data: $("#learnerForm").serialize()}; 
				    $("#learnerForm").ajaxSubmit(options);
			        $('#displayMsgDiv').show();
			        setTimeout('hideStatus("displayMsgDiv")',4000);
			        setTimeout('reloadPage()',2000);
				}
				else 
				{
					return false;
				}
			});
	});
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>	