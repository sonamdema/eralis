<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" /> 
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
		String regionId = (String) request.getAttribute("REGION_ID");
		String regionName = (String) request.getAttribute("REGION_NAME");
	%>
	<script>
		var context = "<%=request.getContextPath()%>";
		var nextID	=	1;
	</script>
	
</head>
<body class="no-skin">
<!-- header include -->
	<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
	<!-- ./header include -->
	 <div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div>
										<ul class="steps">
											<li data-step="1" id="fuelux_1" class="active">
												<span class="step">1</span>
												<span class="title">Application Details</span>
											</li>
	
											<li data-step="2" id="fuelux_2">
												<span class="step">2</span>
												<span class="title">Payment Info</span>
											</li>
											<li data-step="3" id="fuelux_3">
												<span class="step">3</span>
												<span class="title">Make Payment</span>
											</li>
	 
										</ul>
									</div>
	
									<hr />
									<%

									
									String formType = (String) request.getAttribute("form_type");
									
									String title = "";
									if(formType.equalsIgnoreCase("renew_learner_search"))
										title = "Renew your Learner License";
									else if(formType.equalsIgnoreCase("learner_license_duplicate"))
										title = "Replace your Learner License";
									
									%>
									<div class="step-content pos-rel">
										<div class="step-pane active"  id="page_1" data-step="1">  
											<div class="col-lg-12 row">
													<html:form styleClass="form-horizontal" action="/service.html?method=learnerLicenseDetails">									
													<div class="widget-box">
														<div class="widget-header widget-header-small">
														<h4 class="widget-title lighter"><%=title %></h4>
														</div>
														<div class="widget-body">
															<div class="widget-main">
																<div class="row">
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Search By : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="clearfix">
													                               <select class="form-control" id="selectSearchBy" onchange="searchBy(this.value)">
													                               		<option value="">Search By</option>
													                               		<option value="CID">CID No.</option>
													                               		<option value="LL">Learner License No</option>
													                               </select>
																				</div>
																			</div>
																		</div>
																		<div class="form-group param-div hidden cid-div">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Client Identification Number (CIN) : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="clearfix">
										                                			<html:text property="cid" styleId="cid" styleClass="form-control">	</html:text>
																				</div>
																			</div>
																			<div class="col-xs-12 col-sm-3">
																				<a href="#" data-toggle="tooltip" data-html="true" title="<table border='1' style='border-collapse: collapse;'><tr><td colspan='2'>For Bhutanese Nationals</td></tr><tr><td>1</td><td>CID Number</td></tr><tr><td colspan='2'>For Foreign Nationals</td></tr><tr><td>1</td><td>Passport Number</td></tr><tr><td>2</td><td>Work Permit Number</td></tr><tr><td>3</td><td>MC Number</td></tr></table>"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																			</div>
																			<label class="control-label col-xs-12 col-sm-2 no-padding-right red" style="display:none;text-align: left;" id="CIDValidation">Please enter CID</label>
																		</div>
																		<div class="form-group param-div hidden ll-div">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Learner License No : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="col-lg-3 no-padding">
													                               <select class="form-control" id="llPrefix">
													                               		<option value="">Select</option>
													                               		<option value="T/LL-">T/LL-</option>
													                               		<option value="P/LL-">P/LL-</option>
													                               		<option value="G/LL-">G/LL-</option>
													                               		<option value="S/LL-">S/LL-</option>
													                               		<option value="M/LL-">M/LL-</option>
													                               </select>
																				</div>
																				<div class="col-lg-9 no-padding">
													                               <input type="text" id="llNo" class="form-control">
													                               <!--<html:text property="cid" styleId="cid"  styleClass="form-control" >	</html:text>-->
													                    		</div>
																			</div>
																		</div>
																		<script>
																			function searchBy(param1)
																			{
																				$(".param-div").addClass("hidden");
																				if(param1=="CID")
																					$(".cid-div").removeClass("hidden");
																				else if(param1=="LL")
																					$(".ll-div").removeClass("hidden");
																				
																			}
																		
																		</script>
																		<div class="form-group" > 
																			<div class="col-lg-12 center">
																				<button type="button" onClick="leanerLicenseDetails()" class="btn btn-primary btn-sm">Search Details</button>
																			</div> 
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</html:form>
											</div>
											<div class="col-lg-12 row" id="learner_license_dtls"></div>
											
										</div>
										<div class="step-pane" id="page_2" data-step="2">
											<div class="row">
												<jsp:include page="/pages/online_service/online_payment_info.jsp"></jsp:include>
											</div>
										</div>
										<div class="step-pane"  id="page_3" data-step="3">
											<div class="center">
												<div id="online_receipt"></div>
											</div>
										</div>
									</div>
								</div>
	
								<hr />
								<div class="wizard-actions">
									<button type="button" class="btn btn-success btn-next" style="display:none" id="nextButton" onClick="onClickNextButton()"  data-last="Finish">
											Next
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
								<div id="confirmationModal" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Confirmation</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12"> 
														<div class="form-group">
															<label class="col-sm-9" for="CID"> Are you sure, you want to continue? </label>
														</div>
														
														<div class="widget-main padding-24">
															<div style="overflow: auto;">
																<table class="table table-striped table-bordered"  style="min-width:300px">
																	<tbody>
																		<tr class="renew-learner" style="display:none">
																			<td>Renewal Duration:</td>
																			<td>
																				<div class="input-group">
																					<input type="text" readonly="readonly" class="form-control input-mask-phone" id="confirmRenewalDuration">
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td>Location:</td>
																			<td>
																				<div class="input-group">
																					<input type="text" readonly="readonly" class="form-control input-mask-phone" id="confirmLocation">
																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm" name="search" data-dismiss="modal" onclick="makePayment()">
													Yes
												</button>
												<button class="btn btn-sm" data-dismiss="modal">
													No
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		</div>
	<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
</div><!-- /.main-container -->
<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace.min.js"></script> 

<script type="text/javascript">

	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});

	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	//radio button
	$(document).ready(function() {
		$('#nationalRadio').attr('checked', true);
		$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);
	});
	var nextID	=	1;
	function onClickNextButton()
	{  
		$("#regionValidation").hide();
		$("#renewalDurationValidation").hide();
		if(nextID==1)
		{
			var validation=0;
			var renewalDuration	=	$("#renewalDuration").val();
			var LearnerRegionId	=	$("#LearnerRegionId").val();
			var location = $("#locationId").val();
			
			if(renewalDuration=='')
			{
				$("#renewalDurationValidation").show();
				validation=1;
			}
			if(location=='')
			{
				$("#locationValidation").show();
				validation=1;
			}
			if(validation==0)
			{
				
				var str = location.split("_");
				
				$("#submittedLocationId").val(str[1]);
				$("#submittedLocationType").val(str[0]);
				$("#locationValidation").hide();
				
				
				getPaymentInfo();
				nextID	=	nextID+1; 
				$('#page_2').addClass('active');
				$('#page_1').removeClass('active'); 
				
				$('#fuelux_1').addClass('complete');
				$('#fuelux_1').removeClass('active'); 
				$('#fuelux_2').addClass('active');
				
			}
		}
		else if(nextID==2)
		{
			openModal('confirmationModal');
			var formType = '<%=formType%>';
			var locationName = $("#locationId").find('option:selected').text();
			
			if(formType=='renew_learner_search')
			{
				$(".renew-learner").show();
				var confirmRenewalDuration = $("#renewalDuration :selected").text();
				$("#confirmRenewalDuration").val(confirmRenewalDuration);
			}
			
			var confirmLocation = "";
			$("#confirmLocation").val(locationName);
		}
	}

	function makePayment()
	{
		formSubmit();
		$('#page_3').addClass('active');
		$('#page_2').removeClass('active'); 
		$('#fuelux_2').addClass('complete');
		$('#fuelux_2').removeClass('active'); 
		$('#fuelux_3').addClass('active');
		nextID	=	nextID+1; 
	}
	
	function leanerLicenseDetails()
	{
		
		
		
		
		var searchBy = $("#selectSearchBy").val();
		var cid	=	$("#cid").val();
		var llNo	=	$("#llNo").val();
		var llPrefix	=	$("#llPrefix").val();
		var cid			=	$("#cid").val();
		var validation			=	0;
		var form_type	=	'<%=request.getAttribute("form_type")%>';

		$("#CIDValidation").hide();
		$("#learnerNoValidation").hide();
		
		if(searchBy=="CID")
		{
			llNo = "";
			if(cid=="")
			{
				$("#CIDValidation").show();
				validation=1;
			}
		}
		else if(searchBy=="LL")
		{
			llNo = llPrefix + llNo;
			cid = "";
		}
		
		if(validation==0)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=learnerLicenseDetails&learnerNo="+llNo+"&cid="+cid+"&testType="+form_type+"&form_type="+form_type+"&issueType=Learner_License&testType=null",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{	 
					 
					$("#learner_license_dtls").html(responseText);
					$("#learner_license_dtls").show();
					$("#nextButton").show();
					
					
				}
			});
		}
	}
</script>
	</body>
</html>