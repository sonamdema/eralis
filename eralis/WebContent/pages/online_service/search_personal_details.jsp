<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	
</head>
<body class="no-skin">
		<!-- header include -->
		<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
		<!-- ./header include -->
		<div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							 <div class="col-lg-12 row">
									<html:form styleId="searchForm" styleClass="form-horizontal" action="/service.html?method=getPersonalInfo">									<div class="widget-box">
										<div class="widget-header widget-header-small">
											<h4 class="widget-title lighter">Change your Contact Address/Mobile number</h4>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="form-group">
													<div class="col-lg-6">
														<label class="control-label">Client Identification Number (CIN) </label>
														<a href="#" data-toggle="tooltip" data-html="true" data-placement="right" title="<table border='1' style='border-collapse: collapse;'><tr><td colspan='2'>For Bhutanese Nationals</td></tr><tr><td>1</td><td>CID Number</td></tr><tr><td colspan='2'>For Foreign Nationals</td></tr><tr><td>1</td><td>Passport Number</td></tr><tr><td>2</td><td>Work Permit Number</td></tr><tr><td>3</td><td>MC Number</td></tr></table>"><i class="fa fa-question-circle fa-1x" style="padding-top: 3px;cursor:pointer"></i></a>
													</div>
													<div class="col-lg-3">
														<html:text property="cid" styleClass="form-control" styleId="cid" ></html:text>
													</div>
												</div>
												<div class="form-group">
													<div class="col-lg-6">
														<label class="control-label">Date of Birth </label>
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<html:text property="dob" styleId="dob" styleClass="form-control date-picker" readonly="true"></html:text>
															<span class="input-group-addon"> <i
																class="fa fa-calendar bigger-110"></i> </span>
														</div>
													</div>
												</div>
												<div class="form-group" id="otpDiv" style="display:none;">
													<div class="col-lg-6">
															<label class="control-label">Enter OTP Number</label>
															<a href="#" data-toggle="tooltip" data-placement="right" title="You will receive an OTP pin via SMS from RSTA, Please input below"><i class="fa fa-question-circle fa-1x" style="padding-top: 3px;cursor:pointer"></i></a>
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control" id="otp"/>
															<span class="input-group-btn"> 
																<button type="button" class="btn btn-sm btn-primary" onclick="validateOtp()">
																	<i class="ace-icon fa fa-key"></i>
																	Ok
																</button>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group" id="searchBtnDiv">
													<div class="col-lg-12">
														<div class="pull-right">
															<button type="button" class="btn btn-sm  btn-primary" id="btnSubmit" onclick="getPersonalInfo()">
																<i class="ace-icon fa fa-search"></i>
																Search
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</html:form>
							</div>
							<div id="personalDetailsDiv" class="col-lg-12 row">
									 
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<!-- footer include -->
			<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
			<!-- /.footer include -->
			
		</div><!-- /.main-container -->
		
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript">
<!--
//-->
</script>
<script type="text/javascript">

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

	<%
		String cid = null, dob = null, message = null;
	
		if(request.getAttribute("DOB") != null)
			dob = (String)request.getAttribute("DOB");
		if(request.getAttribute("CID") != null)
			cid = (String)request.getAttribute("CID");
		if(request.getAttribute("MESSAGE") != null)
			message = (String)request.getAttribute("MESSAGE");
	%>

	var cid = "<%=cid%>";
	var dob = "<%=dob%>";
	var message = "<%=message%>";

	$(document).ready(function()
	{
		if(message == "FAILURE")
		{
			$('#personalDetailsDiv').html("<div class='alert alert-danger'>Details couldnot be updated, please try again later</div>");
			$('#personalDetailsDiv').show();
			$('#cid').val(cid);
			$('#dob').val(dob);
			getPersonalInfo();
		}
		else if(message == "SUCCESS")
		{
			$('#cid').val(cid);
			$('#dob').val(dob);
			getPersonalInfo();
		}
	});


	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

 
		 
</script>
<script><!--
function getPersonalInfo()
{	
	var cid	=	$("#cid").val();
	var dob=	$("#dob").val();
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/service.html?method=getPersonalInfo&cid="+cid+"&dob="+dob+"&pageType=editPersonalDetails",
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#personalDetailsDiv").html(responseText);
			$("#personalDetailsDiv").show();
		}
	});
}

</script>
	</body>
</html>