<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Home - eRaLIS</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" /> 
		<script> var nextID	=	1;</script>
	</head>
	<body class="no-skin">
		<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div>
										<ul class="steps">
											<li class="active" id="data-step1" disable>
												<span class="step">1</span>
												<span class="title">Offence Details</span>
											</li>
											<li id="data-step2" disabled="disable">
												<span class="step">2</span>
												<span class="title">Payment Info</span>
											</li>
											<li id="data-step3" disabled="disable">
												<span class="step">3</span>
												<span class="title">Make Payment</span>
											</li>
										</ul>
									</div>
									<hr />
									<div class="step-content pos-rel">
										<div class="step-pane active" data-step="1" id="step-pane1">  
											<div class="col-lg-12 row">
												<html:form styleClass="form-horizontal" styleId="offenceSearchForm" action="/service.html?method=getFineInfo">									
													<div class="widget-box">
														<div class="widget-header widget-header-small">
															<h4 class="widget-title lighter">Pay Fine (Offence- TIN)</h4>
														</div>
														<div class="widget-body">
															<div class="widget-main">
																<div class="row">
																	<div class="col-lg-12">
																		<%-- <div class="form-group">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">TIN No : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="clearfix">
													                               	<html:text property="tinNo" styleClass="col-xs-12" styleId="tin_no" ></html:text>
																				</div>
																				<label class="no-padding-right red" style="display:none;" id="tinNoValidation">Please enter TIN no.</label>
																			</div>
																			<div class="col-xs-12 col-sm-4">
																				<a href="#" data-toggle="tooltip" title="Input correct TIN no. as given in TIN Receipt"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																			</div>
																		</div> --%>
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Vehicle No : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="clearfix">
													                               	<html:text property="tinNo" styleClass="col-xs-12" styleId="vehicle_no" ></html:text>
																				</div>
																				<label class="no-padding-right red" style="display:none;" id="vehicleNoValidation">Please enter Vehicle no.</label>
																			</div>
																			<div class="col-xs-12 col-sm-4">
																				<a href="#" data-toggle="tooltip" title="Input correct format as given in the RC E.g:BP-1-A1234"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Vehicle Type : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="clearfix">
													                               	<html:select property="vehicleType" styleClass="col-xs-12" styleId="vehicle_type">
																					   <html:option value="">--SELECT--</html:option>
																					   <html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
																				   </html:select>
																				</div>
																				<label class="no-padding-right red" style="display:none;" id="vehicleTypeValidation">Please select Vehicle Type</label>
																			</div>
																			<div class="col-xs-12 col-sm-4">
																				<a href="#" data-toggle="tooltip" title="Select correct Vehicle type as given in RC"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																			</div>
																		</div>
																		<div class="form-group" > 
																			<div class="col-lg-12 center">
																				<button type="button" onClick="search_offence()" id="searchOffenceButton" class="btn btn-primary btn-sm">Search Details</button>
																			</div> 
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</html:form>
												<div id="unpaidOffenceList" class="col-lg-12" ></div>
												<form class="form-horizontal" id="offenceDtls" style="display:none;">
													<div id="offenceList" class="col-lg-12"></div>
												</form>
											</div>
										</div>
										<div class="step-pane" data-step="2" id="step-pane2">
											<div class="row">
												<div class="col-sm-10 col-sm-offset-1">
													<div class="widget-box transparent">
														<div class="widget-header widget-header-large">
															<h3 class="widget-title grey lighter">
																<i class="ace-icon fa fa-money orange"></i>
																	Payment Details
															</h3>
														</div>
														<div class="widget-body">
															<div class="widget-main padding-24">
																<div style="overflow: auto">
																	<table class="table table-striped table-bordered" id="normal" style="min-width: 300px">
																		<tbody>
																			<tr>
																				<td>Amount:</td>
																				<td>
																					<div class="input-group">
																						<span class="input-group-addon">
																						   Nu.
																						</span>
																						<input type="text" name="amount" readonly="readonly"  class="form-control input-mask-phone" id="offenceAmount"/>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Penalty:</td>
																				<td>
																					<div class="input-group">
																						<span class="input-group-addon">
																						   Nu.
																						</span>
																						<input type="text" name="penalty" readonly="readonly"  class="form-control input-mask-phone" id="penalty"/>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Total Amount:</td>
																				<td>
																					<div class="input-group">
																						<span class="input-group-addon">
																						   Nu.
																						</span>
																						<input type="text" readonly="readonly" name="totalAmount" class="form-control input-mask-phone" id="totalAmount"/>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="step-pane" data-step="3" id="step-pane3">
											<div class="center">
												<div id="online_receipt"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="wizard-actions">
									<button type="button" style="display: none;" class="btn btn-success" id="nextButton" onClick="onClickNextButton()" data-last="Finish">
											Next
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
								<div id="confirmationModal" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Confirmation</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12"> 
														<form class="form-horizontal" role="form">
															<div class="form-group">
																<label class="col-sm-9" for="CID"> Are you sure, you want to continue? </label>
															</div>
														</form>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm" name="search" data-dismiss="modal" onclick="makePayment()">
													Yes
												</button>
												<button class="btn btn-sm" data-dismiss="modal">
													No
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
		</div><!-- /.main-container -->
	<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
	<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/ace.min.js"></script>			
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
	
	<script type="text/javascript">

	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
	
		$(' #id-input-file-2').ace_file_input({
			no_file : 'No File ...',
			btn_choose : 'Choose',
			btn_change : 'Change',
			droppable : false,
			onchange : null,
			thumbnail : false,
			whitelist:'png|jpg|jpeg',
			blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
		});
		
		//datepicker plugin
		$('.date-picker').datepicker({
			autoclose : true,
			todayHighlight : true
		})
		
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function() {
			$(this).prev().focus();
		});
	</script>
	<script>
			
	function onClickNextButton()
	{ 
		if(nextID==1)
		{
			var penaltyAmount	=	$("#penalty").val();
			var countOffenceRow	=	$("#countOffenceRow").val();
			var total_amount	=	0;
			for(var i=1;i<countOffenceRow;i++)
			{
				total_amount	=	parseInt($("#amount"+i).val())+parseInt(total_amount);
			}
			$("#offenceAmount").val(total_amount);
			total_amount	=	parseInt(total_amount) +parseInt(penaltyAmount);
			$("#totalAmount").val(total_amount);
			nextID	=	nextID+1;

			$("#data-step1").attr("class", "complete");
			$("#data-step2").attr("class", "active");
			$("#step-pane1").attr("class", "step-pane");
			$("#step-pane2").attr("class", "active");
			 
		}
		else if(nextID==2)
		{
			openModal('confirmationModal');
		}
	}
	
	function makePayment()
	{
		$("#data-step2").attr("class", "complete");
		$("#data-step3").attr("class", "active");
		$("#step-pane2").attr("class", "step-pane");
		$("#step-pane3").attr("class", "active");
		var	application_no	=	$(".offenceId").val();
		var	service_name	=	"Offence";
		var total_amount	=	$("#totalAmount").val();
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/payment.html?method=onlinePayment&application_no="+application_no+"&service_name="+service_name+"&amount="+total_amount,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText)
			{
				$("#online_receipt").html(responseText);
				$("#nextButton").hide();
			}
		});
		nextID	=	nextID+1;
	}
	
	function payment_modal()
	{ 
		var penaltyAmount	=	$("#penalty").val();
		var countOffenceRow	=	$("#countOffenceRow").val();
		var total_amount	=	0;
		for(var i=1;i<countOffenceRow;i++)
		{
			total_amount	=	parseInt($("#amount"+i).val())+parseInt(total_amount);
		} 
		$("#offenceAmount").val(total_amount);
		total_amount	=	parseInt(total_amount) +parseInt(penaltyAmount);
		$("#totalAmount").val(total_amount);
		
		$('#payment-modal').modal('show');
	}
	
	function payment()
	{
		var	application_no	=	$(".offenceId").val();
		var	service_name	=	"FINES_AND_PENALTIES_ROAD";
		var total_amount	=	$("#totalAmount").val();
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/payment.html?method=onlinePayment&application_no="+application_no+"&service_name="+service_name+"&amount="+total_amount,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText)
			{
				$("#getOffenceSearchList").html(responseText);
				$("#getOffenceSearchList").show();
				getOffenceDtls();
			}
		});
	}
	function search_offence()
	{  
		
		$("#offenceDtls").hide();
		var vehicle_no		=	$("#vehicle_no").val();
		var vehicle_type	=	$("#vehicle_type").val();
		var validation	=	0;
		$("#vehicleNoValidation").hide();
		$("#vehicleTypeValidation").hide();
		
		if(vehicle_no=="")
		{
			$("#vehicleNoValidation").show();
			validation=1;
		}
		if(vehicle_type=="")
		{
			$("#vehicleTypeValidation").show();
			validation=1;
		}
		if(validation==0)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=offenceSearchedByCitizen&searchType=searchUnpaidOffence&offenceId=NA&vehicle_no="+vehicle_no+"&vehicle_type="+vehicle_type,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#unpaidOffenceList").html(responseText);
					//$("#offenceDtls").show();
					//$("#nextButton").show();
					//getOffenceDtls();
				}
			});
		}
	}
	
	function getOffenceDtls(offenceId)
	{
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/service.html?method=offenceSearchedByCitizen&searchType=getOffenceDtls&offenceId="+offenceId+"&vehicle_no=NA&vehicle_type=NA",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#offenceList").html(responseText);
				$("#offenceDtls").show();
				$("#nextButton").show();
			}
		});
		
		
		
		
		
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOffenceDtls&offenceId='+offenceId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					$("#calculatePaymentButton").show();
	                var offenceDate = $(this).find('offenceDate').text();
	                var Inspected_By = $(this).find('Inspected_By').text();
	                var Inspection_Type = $(this).find('Inspection_Type').text();
	                var Traffic_Branch = $(this).find('Traffic_Branch').text();
	                var Place_Of_Inspection = $(this).find('Place_Of_Inspection').text();
	                var Region_Id = $(this).find('Region_Id').text();
	                var Vehicle_Number = $(this).find('Vehicle_Number').text();
	                var Driving_License_No = $(this).find('Driving_License_No').text();
	                var Learner_License_No = $(this).find('Learner_License_No').text();
	                var Is_Bluebook_Seized = $(this).find('Is_Bluebook_Seized').text();
	                var Time_Of_Inspection = $(this).find('Time_Of_Inspection').text();
	                var Is_License_Seized = $(this).find('Is_License_Seized').text();
	                var Remarks = $(this).find('Remarks').text();
	                var amount	=	$(this).find('amount').text();
	                var Time_Of_Inspection	=$(this).find('Time_Of_Inspection').text();
	                var Traffic_Branch	=$(this).find('Traffic_Branch').text();
	                var tinNo	=	$(this).find('tinNo').text();
	                var offenceId	=	$(this).find('offenceId').text();
	                var totalOffenceDuration	=	$(this).find('totalOffenceDuration').text();
					var totalPenalty	=	totalOffenceDuration*750;
					if(parseInt(totalPenalty)>3000)
					{
						totalPenalty=3000;
					}
					
		            $("#penalty").val(totalPenalty);
	                if(Driving_License_No=="null")
		            {
	                	Driving_License_No="-";
			         }
	                if(Learner_License_No=="null")
		            {
	                	Learner_License_No="-";
			         }
					$("#displayVehicleNumber").val(Vehicle_Number);
					$("#licenseNo").val(Driving_License_No);
					$("#learnerLicenseNo").val(Learner_License_No);
					$("#offenceDate").val(offenceDate);
					$("#timepicker1").val(Time_Of_Inspection);
					
					$("#inspectedType").val(Inspection_Type);
					$("#trafficbranch").val(Traffic_Branch);
					$("#trafplaceofInspectionficbranch").val(Place_Of_Inspection);

					if(Is_License_Seized=='Y')
					{
						$('#learnerlicense_seized').prop('checked', true);
					}
					else
					{
						$('#learnerlicense_seized').prop('checked', false);
					}
					if(Is_Bluebook_Seized=='Y')
					{
						$('#bluebook_seized').prop('checked', true);
					}
					else
					{
						$('#bluebook_seized').prop('checked', false);
					}
					
					$("#updateregion").val(Region_Id);
				
					$("#remarks").val(Remarks);
					
					$("#TINno").val(tinNo);
					$("#new_TINno").val(tinNo);
					$("#penalty").val();
					$("#inspectionTime").val(Time_Of_Inspection);
					$("#udpatetrafficbranch").val(Traffic_Branch);
					
					if(Inspected_By=="RSTA")
					{
						$("#updateregionDisplay").val(Region_Id);
						$("#updateregionDisplay").show();
						$("#updatetrafficDisplay").hide();
					}
					else if(Inspected_By=="TRAFFIC_POLICE")
					{
						$("#updatetrafficDisplay").val(Traffic_Branch);
						$("#updateregionDisplay").hide();
						$("#updatetrafficDisplay").show();
					}
					$("#inspectedby").val(Inspected_By);

					if(Inspection_Type==1)
						$("#inspectedType").val("NORMAL");
					else
						$("#inspectedType").val("HIGHWAY");
					
				});
			}
		});
	}

	 
</script>
<script type="text/javascript">
			jQuery(function($) {
				$('[data-rel=tooltip]').tooltip();
				$(".select2").css('width','200px').select2({allowClear:true})
				.on('change', function(){
					$(this).closest('form').validate().element($(this));
				}); 
				var $validation = false;
				$('#fuelux-wizard-container')
				.ace_wizard({
					//step: 2 //optional argument. wizard will jump to step "2" at first
					//buttons: '.wizard-actions:eq(0)'
				})
				.on('actionclicked.fu.wizard' , function(e, info){
					if(info.step == 1 && $validation) {
						if(!$('#validation-form').valid()) e.preventDefault();
					}
				})
				.on('finished.fu.wizard', function(e) {
					bootbox.dialog({
						message: "Thank you! Your information was successfully saved!", 
						buttons: {
							"success" : {
								"label" : "OK",
								"className" : "btn-sm btn-primary"
							}
						}
					});
				}).on('stepclick.fu.wizard', function(e){
				});
				$('#skip-validation').removeAttr('checked').on('click', function(){
					$validation = this.checked;
					if(this.checked) {
						$('#sample-form').hide();
						$('#validation-form').removeClass('hide');
					}
					else {
						$('#validation-form').addClass('hide');
						$('#sample-form').show();
					}
				});
				 
				
				
				$('#modal-wizard-container').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
				
				$(document).one('ajaxloadstart.page', function(e) {
					$('[class*=select2]').remove();
				});
			})
			
			
			
			function searchOffence()
			{  
				$("#offenceList").hide();
				var vehicle_no		=	$("#vehicle_no").val();
				var vehicle_type	=	$("#vehicle_type").val();
				
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/service.html?method=offenceSearchedByCitizen&searchType=searchUnpaidOffence&vehicleId="+vehicle_no+"&vehicle_type="+vehicle_type,
					
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#getOffenceSearchList").html(responseText);
						$("#getOffenceSearchList").show();
					}
				});
			}
		</script>
	</body>
</html>