<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<script>
	var context = "<%=request.getContextPath()%>";
	
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/service.html" styleId="serviceForm">
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Learner Information</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-sm-2">
										<label class="control-label">Learner Number:</label>
									</div>
									<div class="col-sm-4" id="displaylearner"></div>
									<div class="col-sm-2">
										<label class="control-label">Name:</label>
									</div>
									<div class="col-sm-4" id="displayName"></div>
								</div>
								<!-- <div class="form-group">
									<div class="col-sm-2">
										<label class="control-label">Date Of Birth:</label>
									</div>
									<div class="col-sm-4" id="dob"></div>
									<div class="col-sm-2">
										<label class="control-label">Address:</label>
									</div>
									<div class="col-sm-4" id="displayAddress"></div>
								</div> -->
								<div class="form-group">
									<div class="col-sm-2">
										<label class="control-label">Expiry Date:</label>
									</div>
									<div class="col-sm-4" id="dob"><label class="control-label"><%=request.getAttribute("LATEST_EXPIRY_DATE") %></label></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-sm-3 control-label">
									<label>Renewal Duration <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-sm-4">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
	                           			<html:option value="">--SELECT--</html:option>
	                           			<html:option value="12">1 Year</html:option>
	                           			<html:option value="24">2 Year</html:option>
	                           			<html:option value="36">3 Year</html:option>
	                           			<html:option value="48">4 Year</html:option>
	                           			<html:option value="60">5 Year</html:option>
	                           			<html:option value="72">6 Year</html:option>
	                           			<html:option value="84">7 Year</html:option>
	                           			<html:option value="96">8 Year</html:option>
	                           			<html:option value="108">9 Year</html:option>
	                           			<html:option value="120">10 Year</html:option>
	                           			<html:option value="132">11 Year</html:option>
	                           			<html:option value="144">12 Year</html:option>
	                           			<html:option value="156">13 Year</html:option>
	                           			<html:option value="168">14 Year</html:option>
	                           			<html:option value="180">15 Year</html:option>
	                           			<html:option value="192">16 Year</html:option>
	                           			<html:option value="204">17 Year</html:option>
	                           			<html:option value="216">18 Year</html:option>
	                           			<html:option value="228">19 Year</html:option>
	                           			<html:option value="240">20 Year</html:option>
	                           		</html:select>
									<html:hidden property="customerId" styleId="learnerCustomerId"/>
									<html:hidden property="learnerLicenseId" styleClass="form-control" styleId="learnerLicenseId"></html:hidden>
									<html:hidden property="licenseNo" styleClass="form-control" styleId="learnerLicenseNo"></html:hidden>
									<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select renewal duration</label>
								</div>
							</div>
				            <div class="form-group"> 
			                    <div class="col-sm-3 control-label">
									<label>Submit to  <span style="color: #ff0000">*</span>:</label>
							    </div>	
						        <div class="col-sm-4" >
						        	<select class="form-control" id="locationId">
						        		<option value="">Select Submit To</option>
						        		<logic:iterate id="region" name="locationList">
											<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
										</logic:iterate>
						        	</select>
						        	<html:hidden property="jurisId" styleId="submittedLocationId"/>
									<html:hidden property="jurisTypeId" styleId="submittedLocationType"/>
								   <label style="display:none;color: #ff0000" id="locationValidation">Please select Location</label>
				                </div>
				                <div class="col-sm-5">
				                	<a href="#" data-toggle="tooltip" title="Select Region and Office where you want to collect the document from"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
				                </div>
			                </div> 
						</div>
					</div>
				</div>
			</div>
		</div>
		</html:form>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
		
		
		<script type="text/javascript">

		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
					
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			</script>
			<script>
			var name 		= '<%=request.getAttribute("name")%>';
			var bloodgroup	= '<%=request.getAttribute("bloodgroup")%>';
			var dob			= '<%=request.getAttribute("dob")%>';
			var dzongkhag 	= '<%=request.getAttribute("dzongkhag")%>';
			var gewog 		= '<%=request.getAttribute("gewog")%>';
			var address 	= '<%=request.getAttribute("address")%>';
			var country 	= '<%=request.getAttribute("country")%>';
			var village		= '<%=request.getAttribute("village")%>';
			var learnerlicenseNo = '<%=request.getAttribute("learnerlicenseNo")%>';
			var customerId	= '<%=request.getAttribute("customerId")%>';
			var region		= '<%=request.getAttribute("region")%>';
			var expirydate	= '<%=request.getAttribute("expirydate")%>';
			var issuedate	= '<%=request.getAttribute("issuedate")%>';
			var learnerLicenseId	= '<%=request.getAttribute("learnerLicenseId")%>';
			
			$('#displayName').html("<label class='control-label'>"+name+"</label>");
			$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
			$('#dob').html("<label class='control-label'>"+expirydate+"</label>");
			$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
			$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
			$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
			$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
			$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
			$('#licenseNo').val(learnerlicenseNo);
			$('#learnerCustomerId').val(customerId);
			$('#learnerLicenseId').val(learnerLicenseId);
			$('#learnerLicenseNo').val(learnerlicenseNo);
			$('#region').val(region);
			$('#displayregion').html("<label class='control-label'>"+region+"</label>");
			$('#displaylearner').html("<label class='control-label'>"+learnerlicenseNo+"</label>");
			$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
			$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
			

			function getPaymentInfo()
			{
				var requestType = "LEARNER";
				var serviceType = "RENEWAL";
				var identityNo = learnerLicenseId;
				var identityTypeId = "L";
				var renewalDuration = $('#renewalDuration').val();
				$("#penaltyForm").hide();
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '','', renewalDuration);
			}
			
			function formSubmit()
			{
				var amount = $('#amount').val();
				var options = {target:'#online_receipt',url:context+'/service.html?method=learnerLicenseOnlinePayment&formType=LL_RENEWAL&pageId=46&amount='+amount,type:'POST',data: $("#serviceForm").serialize()}; 
			    $("#serviceForm").ajaxSubmit(options);
			}
			
			</script>
			 