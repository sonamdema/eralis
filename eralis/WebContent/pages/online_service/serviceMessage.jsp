<%
	String message = (String)request.getAttribute("messageType");
	String parameters = (String)request.getAttribute("parameters");
	
	if(message.equalsIgnoreCase("success"))
	{
%>
	<div class="alert alert-success">
	<%=request.getAttribute("parameters")%> 
	</div>
	<%
		}
		else if(message.equalsIgnoreCase("validation"))
		{
			if(!parameters.equalsIgnoreCase("VEHICLE_OUTSTANDING"))
			{
	%>
	<div class="alert alert-danger">
		<i class="fa fa-exclamation-triangle"></i>
	 	<%=request.getAttribute("parameters")%>
	</div>
	<%		}
			else if(!parameters.equalsIgnoreCase("VEHICLE_OUTSTANDING"))
			{
	%>
				<div class="alert alert-danger">
					<i class="fa fa-exclamation-triangle"></i>
				 	<%=request.getAttribute("parameters")%>
				</div>
		<%		
			}
			else
			{
				String vehicleNo = (String)request.getAttribute("vehicleNo");
				String vehicleType = (String)request.getAttribute("vehicleType");
	%>
	<div id="outStandingMessage"></div>
	<script>

	var vehicleNo = "<%=vehicleNo%>";
	var vehicleType = "<%=vehicleType%>";
	var vehicleNoArray = vehicleNo.split("/");	
	var vehicleTypeArray = vehicleType.split("/");
	var vehcleCol = "";
	
	for(var i=0;i<vehicleNoArray.length;i++)
	{
		vehcleCol = vehcleCol+"<tr><td>"+vehicleNoArray[i]+"</td><td>"+vehicleTypeArray[i]+"</td></tr>";
	}
	$("#outStandingMessage").html('<div class="alert col-lg-12"><div class="col-lg-12 alert alert-danger" style="margin-bottom: 10px;font-size: 17px;">You have <b>vehicle outstanding</b> against the following vehicle : </div><div class="col-lg-6"><table class="table table-bordered"><thead><tr><th>Vehicle No.</th><th>Vehicle Type</th></tr></thead><tbody>'+vehcleCol+'</tbody></table></div></div>');
	$("#outStandingMessage").show();
	</script>
	<% }
	}
	else if(message.equalsIgnoreCase("warning"))
	{%>
	<div class="alert alert-warning">
		<i class="fa fa-exclamation-triangle"></i>
	 	<%=request.getAttribute("parameters")%> 
	</div>
	<%}
	else
	{
%>
	<div class="alert alert-danger">
		<i class="fa fa-exclamation-triangle"></i>
		Sorry <%=request.getAttribute("parameters")%> doesn't match. Please try with valid data or contact the nearest RSTA Office
	</div>
<%
	}
%>
<script>
	$(document).ready(function(){
		$("#nextButton").hide();
	});
</script>