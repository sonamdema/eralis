<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" /> 
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
		String regionId = (String) request.getAttribute("REGION_ID");
		String regionName = (String) request.getAttribute("REGION_NAME");
	%>
	<script>
		var context = "<%=request.getContextPath()%>";
		var nextID	=	1;
	</script>

</head>
<body class="no-skin">
<!-- header include -->
	<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
	<!-- ./header include -->
	 <div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div class="step-content pos-rel">
										<div class="step-pane active" id="page_1" data-step="1">  
											<div class="col-lg-12 row">
												<html:form styleClass="form-horizontal" action="/service.html?method=getLicenseDetails">									
													<div class="widget-box">
														<div class="widget-header widget-header-small">
															<h5 class="widget-title lighter">Acknowledgement Receipt</h5>
														</div>
														<div class="widget-body">
															<div class="widget-main">
																<div class="row">
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Online Application Number : </label>
																			<div class="col-xs-12 col-sm-3">
																				<div class="clearfix">
													                               <html:text property="applicationNumber" styleId="applicationNumber"  styleClass="form-control" >	</html:text>
																				</div>
																			</div>
																			<label class="control-label col-xs-12 col-sm-2 no-padding-right red" style="text-align: left;display:none" id="applicationNumberValidation">Please enter Online Application Number</label>
																		</div>
																		<div class="form-group" >
																			<div class="col-lg-12 center">
																				<button type="button" onClick="getAcknowledgementReceipt()" class="btn btn-primary btn-sm">Generate</button>
																			</div> 
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</html:form>
												<div class="widget-box" id="receipt-body" style="display:none">
													<div id="licenseDetails" class="panel panel-body"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		</div>
	<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
</div><!-- /.main-container -->
<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace.min.js"></script> 

<script>
	function getAcknowledgementReceipt(){
		
		var applicationNo = $("#applicationNumber").val();
		var validation = 0;
		$("#applicationNumberValidation").hide();
		if(applicationNo=="")
		{
			validation=1;
			$("#applicationNumberValidation").show();
		}
		if(validation==0)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=generateAcknowledgementReceipt&applicationNo="+applicationNo,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#receipt-body").show();
					$("#licenseDetails").html(responseText);
					$("#nextDiv").show();
					$("#licenseDetails").show();
				}
			});
		}
	}
	
</script>

	</body>
</html>