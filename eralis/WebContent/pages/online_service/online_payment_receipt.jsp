<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%  
	String bfsOrderNo = (String) request.getAttribute("BFS_ORDER_NO"); 
	String applicationNo = (String) request.getAttribute("APPLICATION_NO");
	if(!"BOOKING".equalsIgnoreCase(applicationNo))
	{
%>
<div class="alert alert-success">Your application number is <b><%=applicationNo %></b></div>
<%}%>

<!--<iframe style="min-height: 677px;width:100%" class="col-md-12" src="http://192.168.1.171:8080/G2CPaymentAggregator/payment.html?method=openPayment&orderNo=<%=bfsOrderNo%>" frameborder="0" id="frameId">-->
<iframe style="min-height: 677px;width:100%" class="col-md-12" src="https://www.citizenservices.gov.bt/G2CPaymentAggregator/payment.html?method=openPayment&orderNo=<%=bfsOrderNo%>" frameborder="0" id="frameId">
	<div class="alert alert-danger">Could Not Connect to Payment Gateway</div>
</iframe>


<div align="center" id="generateDiv" style="display:none;" class="alert alert-success">
	Click on the link below to generate your acknowledgement
	<br/>
	<a href="#" id="generate" onclick="generateReceipt()">[Generate Acknowledgement]</a>
</div>
<div id="onlineReceiptResult" style="display:none" >
</div>
<div id="errorDiv" style="display:none;" class="alert alert-danger">
	Transaction could not be completed, please try again later
</div>
<script>
	var link = "";
	var bfsOrderNo = "<%=bfsOrderNo%>";
	var applicationNo = "<%=applicationNo%>";

	
	$(document).ready(function() {
		 setTimeout('display()',10000);
		 $( "#generate" ).trigger( "click" );
	});
	

	function deleteApplicationDetails()
	{
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/onlineReceipt?applicationNo="+applicationNo+"&paymentDate=null&txnId=null&txnAmount=null&paymentStatus=UNSUCCESSFUL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#errorDiv").show();
				$('#frameId').removeAttr('src');
			}
		});
	}

	var appNo,receiptDate,receiptNo,amount;
	function display()
	{
		var orderNo = "<%=bfsOrderNo%>"; 
		$.ajax
		({
			async: true,
			type: 'POST',
			url : "<%=request.getContextPath()%>/service.html?method=getDataFromAggregator&orderNo="+orderNo,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					
					status = $(this).find('status').text();

					if(status == "00")
					{
						appNo = $(this).find('application_no').text();
						receiptDate = $(this).find('receipt_date').text();
						receiptNo = $(this).find('receipt_no').text();
						amount = $(this).find('amount').text();
						
						$('#frameId').hide();
						//link = "<%=request.getContextPath()%>/onlineReceipt?applicationNo="+appNo+"&paymentDate="+receiptDate+"&txnId="+receiptNo+"&txnAmount="+amount+"&paymentStatus=PAID";
						//$('#generate').attr("href",);
						$('#frame').hide();
						$('#generateDiv').show();
						generateReceipt();
					}
					else{
						setTimeout('display()',5000);
					}
				});
			}
		});
	 }

	function generateReceipt()
	{
		$("#onlineReceiptResult").empty();
		$.ajax
		({
			async: true,
			type: 'POST',
			url : "<%=request.getContextPath()%>/onlineReceipt",
			data : "applicationNo="+appNo+"&paymentDate="+receiptDate+"&txnId="+receiptNo+"&txnAmount="+amount+"&paymentStatus=PAID",
			success: function(result)
			{ 
				$("#onlineReceiptResult").show();
				$("#onlineReceiptResult").html(result);
			}
		});
	 }

--></script>
