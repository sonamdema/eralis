<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%
	ServiceDTO dto = (ServiceDTO)request.getAttribute("serviceDTO");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<html:form styleClass="form-horizontal" action="/service.html" styleId="serviceForm">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">License Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-sm-2">
									<label>License Number:</label>
								</div>
								<div class="col-sm-4" id="displayOwnerName"><%=dto.getLicenseNo()%></div>
								<div class="col-sm-2">
									<label>Name:</label>
								</div>
								<div class="col-sm-4" id="displayOwnerName"><%=dto.getName()%></div>
							</div>
							<%-- <div class="form-group">
								<div class="col-sm-2">
									<label>Customer ID:</label>
								</div>
								<div class="col-sm-4" id="displayCustomerId"><%=dto.getCustomerID()%></div>
								<div class="col-sm-2">
									<label>Citizen ID:</label>
								</div>
								<div class="col-sm-4" id="cid"><%=dto.getCID()%></div>
							</div> --%>
							
							<%-- <div class="form-group">
								<div class="col-sm-2">
									<label>Date Of Birth:</label>
								</div>
								<div class="col-sm-4" id="displayDob"><%=dto.getDOB()%></div>
								<div class="col-sm-2">
									<label>Blood Group:</label>
								</div>
								<div class="col-sm-4" id="bloodgroup"><%=dto.getBloodGroup()%></div>
							</div> --%>
							<div class="form-group">
								<div class="col-sm-2">
									<label>Expiry Date:</label>
								</div>
								<div class="col-sm-4" id="bloodgroup"><%=request.getAttribute("LATEST_EXPIRY_DATE")%></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Replacement Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
				         	<div class="form-group"> 
			                    <div class="col-sm-3 control-label">
									<label>Submit to <span style="color: #ff0000">*</span>:</label>
							    </div>	
						        <div class="col-sm-4" >
						        	<select class="form-control" id="locationId">
						        		<option value="">Select Submit To</option>
						        		<logic:iterate id="region" name="locationList">
											<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
										</logic:iterate>
						        	</select>
						        	<html:hidden property="jurisId" styleId="submittedLocationId"/>
						        	<html:hidden property="jurisTypeId" styleId="submittedLocationType"/>
								   <label style="display:none;color: #ff0000" id="locationValidation">Please select Location</label>
								   <html:hidden property="pageId" styleId="pageId" /> 
									<html:hidden property="drivinglicenseId" styleId="drivinglicenseId" value="<%=dto.getDrivinglicenseId() %>"/> 
									<html:hidden property="licenseNo" styleId="learnerNo" value="<%=dto.getLicenseNo() %>"/> 
									<html:hidden property="customerId" styleId="learnerCustomerId" value="<%=dto.getCustomerID() %>"/> 
				                </div>
				                <div class="col-sm-5">
				                	<a href="#" data-toggle="tooltip" title="Select Region and Office where you want to collect the document from"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
				                </div>
			                </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</html:form>
<script>

	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});

//validation starts
	function getPaymentInfo()
	{ 
		$("#penaltyForm").hide();
		requestType = "LICENSE";
		var serviceType = "DUPLICATE";
		var identityNo = '<%=dto.getDrivinglicenseId()%>';
		var identityTypeId = '<%=dto.getLicensetype()%>'; //if non-commercial N else C
		getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '', '');
	}
	function formSubmit()
	{
		var amount = $('#amount').val();
		var options = {target:'#online_receipt',url:context+'/service.html?method=drivingLicenseOnlinePayment&formType=DL_DUPLICATE&pageId=52&amount='+amount+'&penalty=0&cardCost=0&ODLRenewalCost=0&licenseType=0&PDRenewalCost=0',type:'POST',data: $("#serviceForm").serialize()}; 
	    $("#serviceForm").ajaxSubmit(options);
	}

</script>
		