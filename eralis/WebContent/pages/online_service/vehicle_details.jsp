<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%
ServiceDTO dto = (ServiceDTO)request.getAttribute("serviceDTO");
%>
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Owner Details</h5>
	</div>						
	<div class="widget-body">
	<div class="widget-main">
	<div class="form-group">
	<div class="col-lg-2">
<label class=control-label>Owner Type:</label>
</div>
<div class="col-lg-3" id="displayOwnerType">
<%=dto.getRegistrationType()%>
           </div>
 
  <div class="col-lg-2">
<label class=control-label>Owner ID :</label>
</div>
            <div class="col-lg-3" id="displayOwnerID"><%=dto.getName()%></div> 
 		</div> 
      	<div class="form-group">
<div class="col-lg-2">
	<label class=control-label>Owner Name:</label>
</div>
                <div class="col-lg-3" id="displayOwnerName"><%=dto.getName()%></div>
  
   <div class="col-lg-2">
  <label class=control-label>Citizen ID :</label>
</div>
            <div class="col-lg-3" id="displayCitizenID"><%=dto.getName()%></div> 
                </div>    
               <div class="form-group">
<div class="col-lg-2">
	<label class=control-label>Phone:</label>
</div>
                <div class="col-lg-3" id="displayPhone"><%=dto.getName()%></div>
                </div>   
           
        <h4>Address (National)</h4>
             <div class="form-group">
               <div class="col-lg-2">
<label class=control-label>Dzongkhag:</label>
</div>
          <div class="col-lg-3" id="displayDzongkhag"><%=dto.getName()%></div>
               <div class="col-lg-2">
<label class=control-label>Gewog:</label>
</div>
           <div class="col-lg-3" id="displayGewog"><%=dto.getName()%></div>
                </div>
              <div class="form-group">
                <div class="col-lg-2">
<label class=control-label>Village:</label>
 </div>
           <div class="col-lg-3" id="displayVillage"><%=dto.getName()%></div>  
               </div>  
         
    
         <h4>Address (Non_National)</h4>
              <div class="form-group">
                <div class="col-lg-2">
<label class=control-label>Country:</label>
 </div>
           <div class="col-lg-3" id="displayCountry"><%=dto.getName()%></div>
               <div class="col-lg-2">
<label class=control-label>Address:</label>
</div>
           <div class="col-lg-3" id="displayAddress"><%=dto.getName()%></div>
                       </div>                   
                     </div>
			       </div>
</div>
<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Registration Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
						<div class="form-group">
							<div class="col-lg-2">
								<label class=control-label>Last Registration Date:</label>
							</div>					
					         <div class="col-lg-3"  id="displayLastRegistrationDate"><%=dto.getName()%></div>
  
   <div class="col-lg-2">
	<label class=control-label>Expiry Date :</label>
</div>
            <div class="col-lg-3" id="displayExpiryDate"><%=dto.getName()%></div> 
                   </div> 
                <div class="form-group">
<div class="col-lg-2">
	<label class=control-label>Penality :</label>
</div>
                <div class="col-lg-3" id="displayPenality"><%=dto.getName()%></div>
			   </div>
                      </div>
                     </div>
	</div>
<div class="widget-box">
       <div class="widget-header widget-header-small">
       <h5 class="widget-title lighter">Renewal Lists</h5>
        </div>						
          <div class="widget-body">
         <div class="widget-main">
            
            <div id="renewalHistoryTable">
            	<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Si No</th>
							<th>Renewal Date</th>
							<th>Expiry Date</th> 
							<th>Receipt NO</th> 
							<th>Receipt Date</th> 
							
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="RENEWAL_HISTORY">
<logic:iterate id="renewal" name="RENEWAL_HISTORY">
<tr>
	<td>
		<input type="radio" name="renewalHistoryRadio" id='<bean:write name="renewal" property="renewalHistoryId"/>' onclick="addSelected(this.id)"/>
</td>
<td><bean:write name="renewal" property="renewalDate"/></td>
<td><bean:write name="renewal" property="expiryDate"/></td>

<td><bean:write name="renewal" property="receiptNo"/></td> 
<td><bean:write name="renewal" property="receiptDate"/></td> 

</tr>
</logic:iterate>
</logic:notEmpty>
<logic:empty name="RENEWAL_HISTORY">
<tr>
	<td colspan="7" align="center">
		<font color='red'>NO RECORD FOUND</font>
	</td>
</tr>
</logic:empty>
								</tbody>
							</table>
			            </div>
                   
                      </div>
				   </div>
</div>			
<div class="widget-box">
       <div class="widget-header widget-header-small">
       <h5 class="widget-title lighter">Vehicle Duplication History</h5>
        </div>						
          <div class="widget-body">
         <div class="widget-main">
             <table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Si No</th>
			<th>Duplicate Issue Date</th>
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			
			
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="DUPLICATION_HISTORY">
			<logic:iterate id="duplication" name="DUPLICATION_HISTORY">
				<tr>
					<td>
						<input type="radio" name="renewalHistoryRadio" id='<bean:write name="renewal" property="renewalHistoryId"/>' onclick="addSelected(this.id)"/>
					</td>
					<td><bean:write name="duplication" property="renewalDate"/></td>
					<td><bean:write name="duplication" property="receiptNo"/></td>
					<td><bean:write name="duplication" property="receiptDate"/></td> 
					
					
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="DUPLICATION_HISTORY">
			<tr>
				<td colspan="7" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>
             
                   
                      </div>
				   </div>
</div>					