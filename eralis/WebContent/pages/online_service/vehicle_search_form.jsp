<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" /> 
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
		String regionId = (String) request.getAttribute("REGION_ID");
		String regionName = (String) request.getAttribute("REGION_NAME");
		
		String formType = (String) request.getAttribute("form_type");
		
		String title = "";
		if(formType.equalsIgnoreCase("vehicle_duplication"))
			title = "Replace your Vehicle Registration Certificate";
		else if(formType.equalsIgnoreCase("vehicle_fitness"))
			title = "Vehicle Fitness Test Certificate";
		
		
	%>
	<script>
		var context = "<%=request.getContextPath()%>";
		var nextID	=	1;
	</script>

</head>
<body class="no-skin">
<!-- header include -->
	<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
	<!-- ./header include -->
	 <div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div>
										<ul class="steps">
											<li data-step="1" id="fuelux_1" class="active">
												<span class="step">1</span>
												<span class="title">Application Details</span>
											</li>
	
											<li data-step="2" id="fuelux_2">
												<span class="step">2</span>
												<span class="title">Payment Info</span>
											</li>
											<li data-step="3" id="fuelux_3">
												<span class="step">3</span>
												<span class="title">Make Payment</span>
											</li>
										</ul>
									</div>
	
									<hr />
									<div class="alert alert-info" style="display:none;" id="changesMessage"><h4 class="no-margin"><i class="fa fa-comments" style="
									    padding-right: 10px;"></i>Please do the necessary changes and proceed again.</h4></div>
									<div class="step-content pos-rel">
										<div class="step-pane active" id="page_1" data-step="1">  
											<div class="col-lg-12">
												<div>
			  									<html:form styleClass="form-horizontal" action="/service.html?method=personalVehicleRenewaldtls" styleId="vehicleForm">
														<div class="widget-box">
															<div class="widget-header widget-header-small">
																<h5 class="widget-title lighter"><%=title %></h5>
															</div>
															<div class="widget-body">
																<div class="widget-main">
																	<div class="form-group">
																		<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Vehicle Number : </label>
																		<div class="col-xs-12 col-sm-3">
																			<div class="clearfix">
												                               	<html:text property="vehicleNumber" styleClass="form-control" styleId="vehicleNo" ></html:text>
																			</div>
																			<label class="no-padding-right red" style="display:none;" id="vehicleNoValidation">Please enter Vehicle No</label>
																		</div>
																		<div class="col-xs-12 col-sm-4">
																			<a href="#" data-toggle="tooltip" title="Enter correct Vehicle no. as given in RC (Eg: BP-1-A2345)"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																		</div>
																	</div>
																	<!-- <div class="form-group">
																		<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Owner CID : </label>
																		<div class="col-xs-12 col-sm-3">
																			<div class="clearfix">
												                               	<html:text property="cid" styleClass="form-control" styleId="cid" ></html:text>
																			</div>
																			<label class="no-padding-right red" style="display:none;" id="CIDValidation">Please Enter CID</label>
																		</div>
																		<div class="col-xs-12 col-sm-4">
																			<a href="#" data-toggle="tooltip" title="Enter correct CID no. (Registered Vehicle Owners CID)"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																		</div>
																	</div> -->
																	<div class="form-group">
																		<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Vehicle Type : </label>
																		<div class="col-xs-12 col-sm-3">
																			<div class="clearfix">
												                               	<html:select property="vehicleType" styleClass="col-xs-12" styleId="vehicle_type">
																				   <html:option value="">--SELECT--</html:option>
																				   <html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
																			   </html:select>
																			</div>
																			<label class="c no-padding-right red" style="display:none;" id="vehicleTypeValidation">Please Select Vehicle Type</label>
																		</div>
																		<div class="col-xs-12 col-sm-4">
																			<a href="#" data-toggle="tooltip" title="Select correct Vehicle Type as given in RC"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																		</div>
																	</div>
																	<div class="form-group" > 
																		<div class="col-lg-12 center">
																			<input type="hidden" value="<%=formType %>" id="formType"/>
																			<button type="button" onClick="searchVehicleDtls()" class="btn btn-primary btn-sm">Search Details</button>
																		</div> 
																	</div>
																</div>
															</div>
														</div>
													</html:form>
												</div>
											<div id="renewalDtls"></div>
										</div>
										</div>
										<div class="step-pane" id="page_2" data-step="2">
											<div class="row">
												<jsp:include page="/pages/online_service/online_payment_info.jsp"></jsp:include>
											</div>
										</div>
										<div class="step-pane" id="page_3" data-step="3">
											<div class="center">
												<div id="online_receipt"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="wizard-actions">
									<button type="button" class="btn btn-success btn-next" onClick="onClickNextButton()" style="display: none" id="nextButton" data-last="Finish">
											Next
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
								<div id="confirmationModal" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Confirmation</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12"> 
														<div class="form-group">
															<label class="col-sm-9" for="CID"> Are you sure, you want to proceed to payment? </label>
														</div>
														<% if(!formType.equalsIgnoreCase("vehicle_fitness")){ %>
														
														<div class="widget-main padding-24">
															<div style="overflow: auto;">
																<table class="table table-striped table-bordered"  style="min-width:300px">
																	<tbody>
																		<tr>
																			<td>Location:</td>
																			<td>
																				<div class="input-group">
																					<input type="text" readonly="readonly" class="form-control input-mask-phone" id="viewLocation">
																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														<%} %>
														
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm" name="search" data-dismiss="modal" onclick="makePayment()">
													Yes
												</button>
												<button class="btn btn-sm" data-dismiss="modal"  onclick="showPreviousPage()">
													No
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		</div>
	<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
</div><!-- /.main-container -->
<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace.min.js"></script>
<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script type="text/javascript">

	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	//radio button
	$(document).ready(function() {
		$('#nationalRadio').attr('checked', true);
		$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);
	});

	//radio button
	function enable(identifier)
	{
		if("National" == identifier)
		{
			$('#country').attr('disabled', true);
			$('#address').attr('disabled', true);

			$('#dzongkhag').attr('disabled', false);
			$('#gewog').attr('disabled', false);
			$('#village').attr('disabled', false);
		}
		else
		{
			$('#country').attr('disabled', false);
			$('#address').attr('disabled', false);

			$('#dzongkhag').attr('disabled', true);
			$('#gewog').attr('disabled', true);
			$('#village').attr('disabled', true);
		}
	}
</script>
 <script>
	var nextID	=	1;
	var formType = '<%=formType%>';
	
	
	function showPreviousPage()
	{
		nextID = 1;
		$( "#fuelux_1" ).addClass( "active" );
		$( "#fuelux_2" ).removeClass( "active" );
		$( "#fuelux_3" ).removeClass( "active" );

		$( "#page_1" ).addClass( "active" );
		$( "#page_2" ).removeClass( "active" );
		$( "#page_3" ).removeClass( "active" );
		
		
		$( "#changesMessage" ).show();
		
	}
	
	
 	function onClickNextButton()
	{ 
 		$("#regionValidation").hide();
		if(nextID==1)
		{
			var location = $("#locationId").val();			
			if (formType=='vehicle_fitness')
			{
				paymentDtls();

				$('#page_2').addClass('active');
				$('#page_1').removeClass('active'); 
				
				$('#fuelux_1').addClass('complete');
				$('#fuelux_1').removeClass('active'); 
				$('#fuelux_2').addClass('active');
				nextID	=	nextID+1; 
			
			}
			else if(location!='' && location!='undefined')
			{
				var str = location.split("_");
				$("#submittedLocationId").val(str[1]);
				$("#submittedLocationType").val(str[0]);
				$("#locationValidation").hide();
				
				paymentDtls();

				$('#page_2').addClass('active');
				$('#page_1').removeClass('active'); 
				
				$('#fuelux_1').addClass('complete');
				$('#fuelux_1').removeClass('active'); 
				$('#fuelux_2').addClass('active');
				nextID	=	nextID+1; 
			}
			else
			{
				$("#locationValidation").show();
			}
		}
		else if(nextID==2)
		{
			openModal('confirmationModal');
			var locationName = $("#locationId").find('option:selected').text();
			$("#viewLocation").val(locationName);
		}
	}
 
 	function makePayment()
	{
 		formSubmit();
		$('#page_3').addClass('active');
		$('#page_2').removeClass('active');

		$('#fuelux_2').addClass('complete');
		$('#fuelux_2').removeClass('active');
		$('#fuelux_3').addClass('active');
		nextID	=	nextID+1;
	}
	
	function searchVehicleDtls()
	{ 
		var formType	=   $('#formType').val();
		var vehicleNo	=	$("#vehicleNo").val();
		var cid			=	$("#cid").val();
		var vehicleType	=	$("#vehicle_type").val();
		var validation	=	0;

		$("#vehicleNoValidation").hide();
		$("#CIDValidation").hide();
		$("#vehicleTypeValidation").hide();
		
		if(vehicleNo=="")
		{
			$("#vehicleNoValidation").show();
			validation=1;
		}
		if(cid=="")
		{
			$("#CIDValidation").show();
			validation=1;
		}
		if(vehicleType=="")
		{
			$("#vehicleTypeValidation").show();
			validation=1;
		}
		if(validation==0)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=getVehicleDtls&formType="+formType+"&vehicleNo="+vehicleNo+"&cid="+cid+"&vehicleType="+vehicleType,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#renewalDtls").html(responseText);
					$("#renewalDtls").show();
					$("#nextButton").show();
					
				}
			});
			
	        setTimeout('hideStatus("messageDiv")',4000);
		}
	}
	 
	</script>
	</body>
</html>