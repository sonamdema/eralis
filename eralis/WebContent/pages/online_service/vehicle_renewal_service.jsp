<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" /> 
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
		String regionId = (String) request.getAttribute("REGION_ID");
		String regionName = (String) request.getAttribute("REGION_NAME");
	%>
	<script>
		var context = "<%=request.getContextPath()%>";
		var nextID	=	1;
	</script>

</head>
<body class="no-skin">
<!-- header include -->
	<jsp:include page="/pages/online_service/header.jsp"></jsp:include>
	<!-- ./header include -->
	 <div class="main-container" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div>
										<ul class="steps">
											<li data-step="1" id="fuelux_1" class="active">
												<span class="step">1</span>
												<span class="title">Application Details</span>
											</li>
	
											<li data-step="2" id="fuelux_2">
												<span class="step">2</span>
												<span class="title">Payment Info</span>
											</li>
											<li data-step="3" id="fuelux_3">
												<span class="step">3</span>
												<span class="title">Make Payment</span>
											</li>
	 
										</ul>
									</div>
									<hr />
									<div class="step-content pos-rel">
										<div class="step-pane active"  id="page_1"  data-step="1">  
											<div class="col-lg-12">
			   									<div>
			   										<html:form styleClass="form-horizontal" action="/service.html?method=personalVehicleRenewaldtls" styleId="vehicleForm">
														<div class="widget-box">
															<div class="widget-header widget-header-small">
																<h4 class="widget-title lighter">Renew your Vehicle Registration Certificate</h4>
															</div>
															<div class="widget-body">
																<div class="widget-main">
																	<div class="form-group">
																		<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Vehicle Number : </label>
																		<div class="col-xs-12 col-sm-3">
																			<div class="clearfix">
												                               	<html:text property="vehicleNumber" styleClass="form-control" styleId="vehicleNo" ></html:text>
																			</div>
																			<label class="no-padding-right red" style="display:none;" id="vehicleNoValidation">Please enter Vehicle No.</label>
																		</div>
																		<div class="col-xs-12 col-sm-4">
																			<a href="#" data-toggle="tooltip" title="Enter correct Vehicle no. as given in RC (Eg: BP-1-A2345)"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																		</div>
																	</div>
																 
																	<div class="form-group">
																		<label class="control-label col-xs-12 col-sm-5 no-padding-right" for="email">Vehicle Type : </label>
																		<div class="col-xs-12 col-sm-3">
																			<div class="clearfix">
												                               	<html:select property="vehicleType" styleClass="col-xs-12" styleId="vehicle_type">
																				   <html:option value="">--SELECT--</html:option>
																				   <html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
																			   </html:select>
																			</div>
																			<label class="no-padding-right red" style="display:none;" id="vehicleTypeValidation">Please select Vehicle Type</label>
																		</div>
																		<div class="col-xs-12 col-sm-4">
																			<a href="#" data-toggle="tooltip" title="Select correct Vehicle Type as given in RC"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
																		</div>
																	</div>
																	<div class="form-group" > 
																		<div class="col-lg-12 center">
																			<button type="button" onClick="personalVehicleRenewaldtls()" class="btn btn-primary btn-sm" >Search Details</button>
																		</div> 
																	</div>
																</div>
															</div>
														</div>
													</html:form>
												</div>
												<div id="renewalDtls"></div>
											</div>
										</div>
										<div class="step-pane" data-step="2" id="page_2" >
											<div class="row">
												<jsp:include page="/pages/online_service/online_payment_info.jsp"></jsp:include>
											</div>
										</div>
										<div class="step-pane" data-step="3" id="page_3" >
											<div class="center">
												<div id="online_receipt"></div>
											</div>
										</div>
									</div>
								</div>
								<hr />
								<div class="wizard-actions">
									<button type="button" id="nextButton" style="display:none" class="btn btn-success btn-next" onClick="onClickNextButton()"  data-last="Finish">
											Next
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
								<div id="confirmationModal" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Confirmation</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12"> 
														<div class="form-group">
															<label class="col-sm-9" for="CID"> Are you sure, you want to proceed to payment? </label>
														</div>
														<div class="widget-main padding-24">
															<div style="overflow: auto;">
																<table class="table table-striped table-bordered"   style="min-width:300px">
																	<tbody>
																		<tr>
																			<td>Renewal Duration:</td>
																			<td>
																				<div class="input-group">
																					<input type="text" readonly="readonly" class="form-control input-mask-phone" id="viewRenewalDurations">
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td>Submitted To:</td>
																			<td>
																				<div class="input-group">
																					<input type="text" readonly="readonly" class="form-control input-mask-phone" id="viewLocation">
																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
															
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm" name="search" data-dismiss="modal" onclick="makePayment()">
													Yes
												</button>
												<button class="btn btn-sm" data-dismiss="modal">
													No
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		</div>
	<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
</div><!-- /.main-container -->
<script src="<%=request.getContextPath()%>/js/fuelux.wizard.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script> 
<script src="<%=request.getContextPath()%>/js/select2.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace-elements.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ace.min.js"></script>
<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script type="text/javascript">
	$(' #id-input-file-2').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	})
	
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	//radio button
	$(document).ready(function() {
		$('#nationalRadio').attr('checked', true);
		$('#country').attr('disabled', true);
		$('#address').attr('disabled', true);
	});

	//radio button
	function enable(identifier)
	{
		if("National" == identifier)
		{
			$('#country').attr('disabled', true);
			$('#address').attr('disabled', true);

			$('#dzongkhag').attr('disabled', false);
			$('#gewog').attr('disabled', false);
			$('#village').attr('disabled', false);
		}
		else
		{
			$('#country').attr('disabled', false);
			$('#address').attr('disabled', false);

			$('#dzongkhag').attr('disabled', true);
			$('#gewog').attr('disabled', true);
			$('#village').attr('disabled', true);
		}
	}
</script>
 <script>
	var nextID	=	1;
 	function onClickNextButton()
	{
		$("#invalidRenewalDurationMessag").hide();
 		var location = $("#locationId").val();
 		var latest_expiry_date = $("#life_span_expiry_date").val();
 		
 		if(latest_expiry_date!='null')
 		{
 			var strDate = latest_expiry_date.split("/");
 	 		var renewalDuration = ($("#renewalDuration").val())/12;
 	 		var validateExpiryDate = (parseInt(strDate[2])+parseInt(renewalDuration))+'-'+strDate[1]+'-'+strDate[0];
 	 		var CurrentDate = new Date();

 	 		validateExpiryDate = new Date(validateExpiryDate);
 	 		
 	 		if(validateExpiryDate <= CurrentDate){
 	 			$("#invalidRenewalDurationMessag").show();
 	 			$("#invalidRenewalDurationMessag").html("Your Vehicle has expired on : <b>"+latest_expiry_date+"</b>, So renewal Duration should be greater than next expiry date");
 	 			return false;
 	 		} 
 		}
 		
		if(nextID==1)
		{
			if(location!='')
			{
				
				var str = location.split("_");
				$("#submittedLocationId").val(str[1]);
				$("#submittedLocationType").val(str[0]);
				
				$("#locationValidation").hide();
				
				var identityTypeId;
				var requestType = "VEHICLE";
				var serviceType = "RENEWAL";
				var identityNo = $('#vehicleId').val();
				if( $("#engineType").val() == "Electric")
					identityTypeId = '0'; 
				else
					identityTypeId = $('#vehicleType').val();
				var loadingCapacity = $('#loadCapacity').val();
				var seatingCapacity = $('#seatCapacity').val();
				var vehicleHP = $('#vehicleHorsePower').val();
				var kilowatts = $('#vehicleKiloWatt').val();
				var engineCC = $('#engineCC').val();
				var purchaseDate = "";
				var saleDeedAmount = "";
				var saleDeedDate = "";
				var renewalDuration = $('#renewalDuration').val();
				var vehicleRegistrationCode = $('#vehicleRegistrationCode').val();


				latestRenewalDuration = $("#renewalDuration :selected").val(); 
				
				 
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration, vehicleRegistrationCode);
   
				getPreviousPaymentDtls(identityNo);

				$('#page_2').addClass('active');
				$('#page_1').removeClass('active');
				
				$('#fuelux_1').addClass('complete');
				$('#fuelux_1').removeClass('active'); 
				$('#fuelux_2').addClass('active');

				nextID	=	nextID+1; 
			}
			else
			{
				$("#locationValidation").show();

			}
		}
		else if(nextID==2)
		{
			openModal('confirmationModal');
			
			var viewRenewalDurations = $("#renewalDuration :selected").text();
			$("#viewRenewalDurations").val(viewRenewalDurations);
			var locationName = $("#locationId").find('option:selected').text();
			$("#viewLocation").val(locationName);
		}
	}

 	function makePayment()
	{
 		renewalFormSubmit();
		$('#page_3').addClass('active');
		$('#page_2').removeClass('active'); 
		$('#fuelux_2').addClass('complete');
		$('#fuelux_2').removeClass('active'); 
		$('#fuelux_3').addClass('active');
		nextID	=	nextID+1; 
	}
	
	function formSubmit()
	{ 	  
		var formType	=	$("#formType").val();
		var cid			=	$("#cid").val();
		var newCustomer	=	$("#newCustomer").val();
		if(newCustomer=='Y')
		{
			if(formType=='personal')
			{
				if($('#manualFlag').val() == "Y")
				{
					$('#dzongkhagHidden').val($('#dzongkhag').val());
					$('#gewogHidden').val($('#gewog').val());
				}
				var manualFlag	=	$('#manualFlag').val();
				var vehRegion	=	$('#vehRegion').val();
				var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/service.html?method=personal_info&manualFlag='+manualFlag+'&regionId='+vehRegion,type:'POST',data: $("#personalForm").serialize()}; 
			    $("#personalForm").ajaxSubmit(options);
		        $('#messageDiv').show();
		        $('#messageDiv').get(0).scrollIntoView();
		        setTimeout('hideStatus("messageDiv")',4000);
			}if(formType=='organisation')
			{ 
				var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/service.html?method=organisational_info',type:'POST',data: $("#personalForm").serialize()}; 
			    $("#personalForm").ajaxSubmit(options);
		        $('#messageDiv').show();
		        $('#messageDiv').get(0).scrollIntoView();
		        setTimeout('hideStatus("messageDiv")',4000);
			}
		}
		else if(newCustomer=='N'){
			if(formType=='personal')
			{
				var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/service.html?method=personal_info&cid='+cid+'&newCustomer=N',type:'POST',data: $("#personalForm").serialize()}; 
			    $("#personalForm").ajaxSubmit(options);
		        $('#messageDiv').show();
		        $('#messageDiv').get(0).scrollIntoView();
		        setTimeout('hideStatus("messageDiv")',4000);
			}if(formType=='organisation')
			{ 
				var customerID	=	$("#customerID").val();
				var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/service.html?method=organisational_info&newCustomer=N&customerId='+customerID,type:'POST',data: $("#personalForm").serialize()}; 
			    $("#personalForm").ajaxSubmit(options);
		        $('#messageDiv').show();
		        $('#messageDiv').get(0).scrollIntoView();
		        setTimeout('hideStatus("messageDiv")',4000);
			}
		}
	}

	
	function getPreviousPaymentDtls(vehicleId)
	{ 	
		var viewRenewalDurations = $("#renewalDuration :selected").text();
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getVehiclePreviousPayment&vehicleId='+vehicleId+'&viewRenewalDurations='+viewRenewalDurations,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{	
					vehiclePrevAmount = $(this).find('amount').text();
					previousDuration = $(this).find('duration').text();

					
					if(vehicleAmount!=0)
					{
						if((vehiclePrevAmount==vehicleAmount))
						{
							 
						}
						else{
							$("#perviousPaymentDiv").hide(); 
							if(previousDuration==latestRenewalDuration)
							{
								$("#perviousPaymentDiv").show();
								$("#perviousPaymentDiv").html("The payment amount do not tally with previous amount : <span class='bolder'>Nu."+vehiclePrevAmount+"</span>. Contact nearest RSTA office for payment verfication <a href='https://eralis.rsta.gov.bt/eralis/public/serviceredirection.html?q=vehicle_renewal_service' class='btn btn-danger'>Back</a>");
							}
						}
							
					}
					
				});
			}
		});
	}

	 
	function personalVehicleRenewaldtls()
	{ 	
		var vehicleType	=	$("#vehicle_type").val();
		var vehicleNo	=	$("#vehicleNo").val();
		//var cid			=	$("#cid").val();
		var validation	=	0;
		$("#vehicleNoValidation").hide();
		//$("#CIDValidation").hide();
		$("#vehicleTypeValidation").hide();
		
		if(vehicleNo=="")
		{
			$("#vehicleNoValidation").show();
			validation=1;
		}
		if(vehicleType=="")
		{
			$("#vehicleTypeValidation").show();
			validation=1;
		}
		
		if(validation==0)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=getVehicleDtls&formType=VEH_RENEWAL&vehicleNo="+vehicleNo+"&vehicleType="+vehicleType,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#renewalDtls").html(responseText);
					$("#renewalDtls").show();
					$("#nextButton").show();
					//searchVehicleNumber();
					populateRenewalDurationDropdown();
					
				}
			});
	        setTimeout('hideStatus("messageDiv")',4000);
		}
	}
	
	</script>
	<script type="text/javascript">
	 
		</script>
	</body>
</html>