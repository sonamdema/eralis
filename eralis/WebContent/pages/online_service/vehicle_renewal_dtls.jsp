<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
 <%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
	
	EralisCommonDTO dto = (EralisCommonDTO) request.getAttribute("EDTO");
%> 

 <div class="row">
  <div class="col-lg-12">
   <html:form styleClass="form-horizontal" action="/service.html" styleId="validation-form">
	<div class="row">
		<div id="renewalValidationMsg"></div>
		<div id="incompleteValidationMsg"></div>
		<div id="Msg"></div>
	</div>
		 
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-sm-2">
						 <label class=control-label>Owner Type:</label>
					</div>
                    <div class="col-sm-4"><label class=control-label><%=dto.getOwnerType()%></label></div>
                    
                   <%--  <%
                    	if(dto.getOwnerType().equalsIgnoreCase("Personal")){
                    %>
				    <div class="col-sm-2">
						<label class=control-label>Citizen ID :</label>
					</div>
                    <div class="col-sm-3"><label class=control-label><%=dto.getCID()%></label></div> 
                    <%
                    	} else {
                    %>
                    <div class="col-sm-2">
						<label class=control-label>Owner ID :</label>
					</div>
                    <div class="col-sm-3"><label class=control-label><%=dto.getOwnerId()%></label></div> 
                    <%
                    	}
                    %> --%>
                    <div class="col-sm-2">
						<label class=control-label>Owner Name:</label>
					</div>
					<%
						if(dto.getOwnerType().equalsIgnoreCase("Personal")){
					%>
	               	<div class="col-sm-4"><label class=control-label><%=dto.getName()%></label></div>
	               	<%
						} else {
							if(dto.getOwnerTypeDesc().equalsIgnoreCase("GOVERNMENT")){
	               	%>
	               			<div class="col-sm-4"><label class=control-label><%=dto.getMinistry() %>,&nbsp;<%=dto.getDepartment() %></label></div>
	               	<%
							} else if(dto.getOwnerTypeDesc().equalsIgnoreCase("PRIVATE")){
					%>
							<div class="col-sm-4"><label class=control-label><%=dto.getPrivateCompany() %></label></div>
					<%
							} else {
			        %>					
							<div class="col-sm-4"><label class=control-label><%=dto.getOwner() %></label></div>
					<%
							}
						}
	               	%>
               </div> 
               <%-- <div class="form-group">
					
					<div class="col-sm-2">
						<label class=control-label>Phone:</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getPhone()%></label></div>
	             </div> --%>  
       		</div>
       		<div class="widget-main" id="Organization" style="display:none">
				<div class="form-group">
					<div class="col-sm-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getOwnerType()%></label></div>
				    <div class="col-sm-2">
						<label class=control-label>Agency :</label>
					</div>
	                <div class="col-sm-3"><label class=control-label></label></div> 
	           	</div> 
	            <div class="form-group">
					<div class="col-sm-2">
						<label class=control-label>Department/Name:</label>
					</div>
	               	<div class="col-sm-3"><label class=control-label><%=dto.getGewog()%></label></div>
				    <div class="col-sm-2">
					   <label class=control-label>Address :</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getAddress()%></label></div> 
	            </div>    
		        <div class="form-group">
					<div class="col-sm-2">
						<label class=control-label>Phone:</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getPhone()%></label></div>
	                <div class="col-sm-2">
						<label class=control-label>Dzongkhag:</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getDzongkhag()%></label></div>
	            </div>   
      		</div>
       	</div>
     </div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Vehicle Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="form-group">
		          	<div class="col-sm-2">
						<label class=control-label>Vehicle Expiry Date:</label>
					</div>
	                <div class="col-sm-3">
	                	<label class=control-label><%=request.getAttribute("LATEST_EXPIRY_DATE") %></label>
	                	<input type="hidden" value="<%=request.getAttribute("LIFE_SPAN_EXPIRY_DATE") %>" id="life_span_expiry_date">
	                </div>
	                <div class="col-sm-2">
						<label class=control-label>Fitness Expiry Date:</label>
					</div>
	                <div class="col-sm-3"><label class=control-label><%=dto.getValidUpto()%></label></div>
	        	</div>
            </div>
  		</div>
	</div>
    <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Renewal Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
               <!--  <div class="form-group">
	            	<div class="col-sm-3 control-label">
		            	<label>Renewal Type:</label>
		           	</div>
			        <div class="col-sm-4">
			        	<select id="renewalType" class="form-control" onchange="showAttachment(this.value)">
	                      	<option value="N">Normal</option>
	                        <option value="E">Exemption</option>
	              		</select>
	          		</div>
	          	</div> -->
	          	<div class="form-group">
                	<div class="col-sm-3 control-label">
                    	<label>Renewal Duration :</label>
                    </div>
                    <div class="col-sm-4">
                    	<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
                    		<html:option value="">--SELECT--</html:option>
                     	</html:select>
                    </div>
              	</div>
              	<div class="form-group"> 
                    <div class="col-sm-3 control-label">
						<label>Submit to  <span style="color: #ff0000">*</span>:</label>
				    </div>	
			        <div class="col-sm-4" >
			        	<select class="form-control" id="locationId">
			        		<option value="">Select Submit To</option>
			        		<logic:iterate id="region" name="locationList">
								<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
							</logic:iterate>
			        	</select>
			        	<html:hidden property="jurisId" styleId="submittedLocationId"/>
						<html:hidden property="jurisTypeId" styleId="submittedLocationType"/>
					   	<label style="display:none;color: #ff0000" id="locationValidation">Please select Location</label>
	                </div>
	                <div class="col-sm-5">
	                	<a href="#" data-toggle="tooltip" title="Select Region and Office where you want to collect the document from"><i class="fa fa-question-circle fa-2x" style="padding-top: 3px;cursor:pointer"></i></a>
	                </div>
                </div>
               <%--  <div class="form-group" style="margin-bottom:50px"> 
                    <div class="col-sm-3 control-label">
						<label>Base :</label>
				    </div>	
			        <div class="col-sm-4" >
			        	<html:select property="baseoffice" styleClass="form-control" styleId="baseOffice" >
							<html:option value="">--SELECT--</html:option>
					   </html:select>
	                </div>
           		</div> --%>
            </div>
  		</div>
	</div>
	<div class="widget-box" id="attachmentDIV" style="display: none;">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-sm-2">
								<label> Supporting Document <span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-sm-3">
								<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload" onchange="validation('validation-form','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="attchValidation">Please attach your file here</label>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
			              </div>
                    		 </div> 
                  		</div>
               	 </div>
			</div>
		</div>	
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
			<div class="pull-left"  style="margin-bottom:50px">
				<html:hidden property="engineType" styleId="engineType" value="<%=dto.getEngineType() %>"></html:hidden>
				<html:hidden property="vehicleType" styleId="vehicleType" value="<%=dto.getVehicleType() %>"></html:hidden>
				<input type="hidden" id="vehicleTypeDesc" value="<%=dto.getVehicleTypeDesc()%>"/>
				<input type="hidden"  id="vehicleRegistrationCode"  value="<%=dto.getVehicleRegistrationType()%>"/>
				<html:hidden property="loadCapacity" styleId="loadCapacity" value="<%=dto.getLoadCapacity() %>"></html:hidden>
				<html:hidden property="seatCapacity" styleId="seatCapacity" value="<%=dto.getSeatCapacity() %>"></html:hidden>
				<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower" value="<%=dto.getVehicleHorsePower() %>"></html:hidden>
				<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt" value="<%=dto.getVehicleKiloWatt() %>"></html:hidden>
				<html:hidden property="engineCC" styleId="engineCC" value="<%=dto.getEngineCC() %>"></html:hidden>
				<html:hidden property="customerId" styleId="customerID" value="<%=dto.getCustomerID() %>"></html:hidden>
				<html:hidden property="vehicleRegistrationType" styleId="ownerType" value=""></html:hidden>
				<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
				<html:hidden property="vehicleId" styleClass="form-control" value="<%=dto.getVehicleId() %>" styleId="vehicleId" ></html:hidden>
			</div>
		</html:form>
		<div class="alert alert-danger" id="invalidRenewalDurationMessag" style="display:none"></div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});

	var ownerType	=	'<%=dto.getOwnerType() %>';
	if(ownerType=='Personal')
	{
		$('#ownerType').val("P");
	}
	else if(ownerType=='Organization')
    {
	    $('#ownerType').val("O");
	}

	function showAttachment(val)
	{
		if(val == "E")
			$('#attachmentDIV').show();
		else
			$('#attachmentDIV').hide();
	}

	function validation()
	{
		var vehicleNumber = $('#displayVehicleNumber').val();
		var vehicleRegionId	=	$("#vehicleRegionId").val();
		//var renewalType = $('#renewalType').val();
		
		$("#regionValidation").hide(); 
		$("#attchValidation").hide();
		$("#vehicleNumberValidation").hide();
		
		if(vehicleNumber == "")
		{
			$('#vehicleNumberValidation').show();
			$('#vehicleNumberValidation').get(0).scrollIntoView();
			return "0";
		} 
		if(vehicleRegionId=="")
		{
 			$("#regionValidation").show(); 
 			$('#regionValidation').get(0).scrollIntoView();
 			return "0";
		}
	 
		
	}
   var pageIdentifier = "<%=pageIdentifier%>";
   var pageId = "<%=pageId %>";
  		
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	});

  $('.fileupload').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});

	function searchRenewalInfo()
	{ 
		var vehicleNumber = $('#vehicleNumberRenewalModal').val();
		var engineNumber = $('#engineNumberRenewalModal').val();
		var chasisNumber = $('#chasisNumberRenewalModal').val();
		var cidNumber = $('#citizenIdRenewalModal').val();

		if(vehicleNumber == "")
			vehicleNumber = "NA";
		if(engineNumber == "")
			engineNumber = "NA";
		if(chasisNumber == "")
			chasisNumber = "NA";
		if(cidNumber == "")
			cidNumber = "NA";

		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=NA&searchType=VEH_RENEWAL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#renewalListTable").html(responseText);
				$("#renewalListTable").show();
			}
		});
	}

	function searchVehicleNumber()
	{
		var vehicleId = '<%=dto.getVehicleId()%>';
		 
		if(vehicleId == "")
			vehicleId = "NA";

		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getRenewalHistoryList&vehicleId="+vehicleId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#renewalHistoryTable").html(responseText);
				$("#renewalHistoryTable").show();

				//checkIfFitnessNEmissionValid();

				
			}
		  });
	 }

	 function populateRenewalDurationDropdown()
	 {
		
		$('#renewalDuration').empty();
		var vehicleTypeDesc = $('#vehicleTypeDesc').val();

		if(vehicleTypeDesc == "TWO_WHEELER" || vehicleTypeDesc == "LIGHT_VEHICLE")
		{
			$('#renewalDuration').append("<option value='12'>1 Year</option>");
			$('#renewalDuration').append("<option value='24'>2 Years</option>");
			$('#renewalDuration').append("<option value='36'>3 Years</option>");
			$('#renewalDuration').append("<option value='48'>4 Years</option>");
			$('#renewalDuration').append("<option value='60'>5 Years</option>");
			$('#renewalDuration').append("<option value='72'>6 Years</option>");
			$('#renewalDuration').append("<option value='84'>7 Years</option>");
			$('#renewalDuration').append("<option value='96'>8 Years</option>");
			$('#renewalDuration').append("<option value='108'>9 Years</option>");
			$('#renewalDuration').append("<option value='120'>10 Years</option>");
		}
		else if(vehicleTypeDesc == "POWER_TILLER")
		{
			 
			$('#renewalDuration').append("<option value='120'>10 Years</option>");
		}
		else if(vehicleTypeDesc == "EARTH_MOVING_EQUIPMENT" || vehicleTypeDesc == "TRACTOR")
		{
			$('#renewalDuration').append("<option value='12'>1 Year</option>");
			$('#renewalDuration').append("<option value='24'>2 Years</option>");
			$('#renewalDuration').append("<option value='36'>3 Years</option>");
			$('#renewalDuration').append("<option value='48'>4 Years</option>");
			$('#renewalDuration').append("<option value='60'>5 Years</option>");
			$('#renewalDuration').append("<option value='72'>6 Years</option>");
			$('#renewalDuration').append("<option value='84'>7 Years</option>");
			$('#renewalDuration').append("<option value='96'>8 Years</option>");
			$('#renewalDuration').append("<option value='108'>9 Years</option>");
			$('#renewalDuration').append("<option value='120'>10 Years</option>");
		}
		else
		{
			$('#renewalDuration').append("<option value='6'>6 Months</option>");
			$('#renewalDuration').append("<option value='12'>1 Year</option>");
			$('#renewalDuration').append("<option value='24'>2 Years</option>");
			$('#renewalDuration').append("<option value='36'>3 Years</option>");
	//		$('#renewalDuration').append("<option value='48'>4 Years</option>");
	//		$('#renewalDuration').append("<option value='60'>5 Years</option>");
	//		$('#renewalDuration').append("<option value='72'>6 Years</option>");
	//		$('#renewalDuration').append("<option value='84'>7 Years</option>");
	//		$('#renewalDuration').append("<option value='96'>8 Years</option>");
	//		$('#renewalDuration').append("<option value='108'>9 Years</option>");
	//		$('#renewalDuration').append("<option value='120'>10 Years</option>");
		}
	 }

	function checkIfFitnessNEmissionValid()
	{
		var vehicleId = $('#vehicleId').val();
		 
		if(vehicleId == "")
			vehicleId = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/vehicle.html?method=checkIfFitnessNEmissionValid&vehicleId="+vehicleId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#renewalValidationMsg").html(responseText);
				$("#renewalValidationMsg").show();
				setTimeout('reloadPage()',5000);
			}
		 });
	}

	function renewalFormSubmit()
	{
		var renewalType = "N";
		var totalAmount = $('#totalAmount').val();
		var amount = $('#amount').val();
		var penalty = $('#penalty').val();
		var options = {target:'#online_receipt',url:context+'/service.html?method=vehicleRenewal&renewalType='+renewalType+'&amount='+amount+'&penalty='+penalty+"&totalAmount="+totalAmount,type:'POST',data: $("#validation-form").serialize()}; 
	    $("#validation-form").ajaxSubmit(options);
	}	
	
</script>
			   
           
    