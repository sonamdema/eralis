<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<script>
	var context = "<%=request.getContextPath()%>";
</script>

<jsp:include page="/pages/online_service/header.jsp"></jsp:include>

<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter" style="font-size: 18px;">
			View Application Status
			<small style="font-size: 14px;">
				<i class="ace-icon fa fa-angle-double-right"></i>
				 Track status of your application
			</small>
		</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
		    	<div class="col-lg-12">
		    		<form>
						<div class="row">
							<div class="col-lg-12 form-group">
						 		<div class="col-lg-2">
									<label>Service Type:</label>
						 		</div>
								<div class="col-lg-3">
									<div class="form-group">
										<select class="form-control" onchange="changeService(this.value)" id="serviceType">
											<option value="ALL">ALL Service Type</option>
											<option value="VL">Vehicle</option>
											<option value="DL">Driving License</option>
											<option value="LL">Learner License</option>
										</select>
								 	</div>
							  	</div>
						 		<div class="col-lg-2">
									<label>Application Number:</label>
						 		</div>
								<div class="col-lg-3">
									<div class="form-group">
										<input type="text" id="applicationNo" class="form-control" maxlength="15"/>
										<div id="applicationNoValidation" class="text-danger">Please select Application Type</div>
								 	</div>
							  	</div>
						    </div>
							<div class="col-lg-12 form-group ">
						 		<div class="col-lg-2 cid">
									<label>CID No.:</label>
						 		</div>
								<div class="col-lg-3 cid">
									<div class="form-group">
										<input type="text" id="cid" class="form-control" maxlength="15"/>
								 	</div>
							  	</div>
						 		<div class="col-lg-2 LL">
									<label>Learner License No.:</label>
						 		</div>
								<div class="col-lg-3 LL">
									<div class="form-group">
										<input type="text" id="llNo" class="form-control" maxlength="15"/>
								 	</div>
							  	</div>
							  	<div class="col-lg-2 DL">
									<label>Driving License No.:</label>
						 		</div>
								<div class="col-lg-3 DL">
									<div class="form-group">
										<input type="text" id="dlNo" class="form-control" maxlength="15"/>
								 	</div>
							  	</div>
							  	<div class="col-lg-2 VL">
									<label>Vehicle No.:</label>
						 		</div>
								<div class="col-lg-3 VL">
									<div class="form-group">
										<input type="text" id="vlNo" class="form-control" maxlength="15"/>
								 	</div>
							  	</div>
						    </div>
							<div class="col-lg-12 form-group ">
							  	<div class="col-lg-2 VL">
									<label>Vehicle Type:</label>
						 		</div>
								<div class="col-lg-3 VL">
									<div class="form-group">
										<select class="form-control" id="vehicleType">
											<option value="">Select Vehicle Type</option>
											<logic:iterate id="vehicleType" name="vehicleTypeList">
												<option value='<bean:write name="vehicleType" property="headerId"/>'><bean:write name="vehicleType" property="headerName"/></option>
											</logic:iterate>
										</select>
								 	</div>
							  	</div>
							  	<div class="col-lg-8 alert alert-danger" id="validationMessage">You must enter any of the above field</div>
						    </div>
						    <div class="col-lg-12 text-center">
						    	<button class="btn btn-sm btn-primary" name="search" onclick="searchApplicationStatus()" type="button">
									<i class="ace-icon fa fa-search"></i>
									Search Application Status
								</button>
						    </div>
						</div>
						<div id="result"></div>
					</form>
		    	</div>
		   	</div>
		</div>
	</div>
</div>
<jsp:include page="/pages/online_service/footer.jsp"></jsp:include>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	
<script type="text/javascript">

$(document).ready(function(){
	$(".DL").hide();
	$(".LL").hide();
	$(".VL").hide();
	$(".TOP").hide();
	$(".cid").hide();
	$("#validationMessage").hide();
	$("#applicationNoValidation").hide();
});
function changeService(serviceType)
{
	$(".DL").hide();
	$(".LL").hide();
	$(".VL").hide();
	$(".TOP").hide();
	$(".cid").hide();
	$("."+serviceType).show();
	if(serviceType!='ALL')
		$(".cid").show();
}
 
function searchApplicationStatus()
{
	var applicationNo = $('#applicationNo').val();
	var serviceType = $('#serviceType').val();
	var cid = $('#cid').val();
	var llNo = $('#llNo').val();
	var dlNo = $('#dlNo').val();
	var vlNo = $('#vlNo').val();
	var vehicleType = $('#vehicleType').val();
	var valid = 1;
	$("#validationMessage").hide();

	if(serviceType=='DL' && cid=="" && applicationNo=="" && dlNo=="")
	{
		valid = 0;
	}
	else if(serviceType=='LL' && cid=="" && applicationNo=="" && llNo=="")
	{
		valid = 0;
	}
	else if(serviceType=='VL' && cid=="" && vlNo=="" && vehicleType=="" && applicationNo=="")
	{
		valid = 0;
	}
	else if(serviceType=='ALL' && applicationNo=="")
	{
		valid = 0;
	}
	 if(valid==1)
	{ 
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/service.html?method=getApplicationStatus&applicationNo="+applicationNo+"&serviceType="+serviceType+"&cid="+cid+"&LLNO="+llNo+"&dlNo="+dlNo+"&VNo="+vlNo+"&vehicleType="+vehicleType,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#result").html(responseText);
				$("#result").show();
			}
		});
	}
	 else
	{
		 $("#validationMessage").show();
		 
	}	
}

</script>		

