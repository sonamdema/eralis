<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<br><br>
<div class="row">
	<div class="table-responsive">
			<table id="applicationSeachTable" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th ><strong>Application No</strong></th>
						<th ><strong>Service</strong></th>
						<th ><strong>LL/DL/Vehicle No.</strong></th>
						<th ><strong>CID</strong></th>
						<th ><strong>Status</strong></th>
						<th ><strong>Submitted To</strong></th>
						<th ><strong>Action Taken By</strong></th>
						<th ><strong>Action Date</strong></th>
						<th ><strong>Claimed By</strong></th>
					</tr>
				</thead>
				<tbody>
					<logic:iterate name="status_dto" id="status_dto_id" type="bt.gov.rsta.eralis.dto.online_service.ApplicationStatusTrailDTO" property="appStatusTrailList">	
						<tr>
							<td>
								<a style="padding: 0px;box-shadow: 1px 1px 5px #00000061!important;" onclick='getAcknowledgementReceipt("<bean:write name="status_dto_id" property="applicationNo"/>")' class="btn btn-primary" href="#">View Receipt</a>
								 <bean:write name="status_dto_id" property="applicationNo"/>
							</td>
							<td>
								<span ><bean:write name="status_dto_id" property="serviceName"/></span>
							</td>
							<td>
								<span ><bean:write name="status_dto_id" property="serviceTypeNo"/></span>
							</td>
							<td>
								<span ><bean:write name="status_dto_id" property="cid"/></span>
							</td>
							<td>
								<span ><bean:write name="status_dto_id" property="action"/></span>
							</td>
							<td>
								<span ><bean:write name="status_dto_id" property="submittedTo"/></span>
							</td>
							<td>
								<span ><bean:write name="status_dto_id" property="role"/></span><br>
								(<bean:write name="status_dto_id" property="actor"/>) 
							</td>
							<td ><bean:write name="status_dto_id" property="date"/></td>
							<td ><bean:write name="status_dto_id" property="claimedBy"/></td>
						</tr>
					</logic:iterate>
				</tbody>
			</table>
	</div>
	<div id="receiptDetails"></div>
</div>
<script>
	$(document).ready(function() 
	{
   		//$('#applicationSeachTable').DataTable({
       //     responsive: true
    //	});
	});
</script>


<script>
	function getAcknowledgementReceipt(applicationNo){
		
		//var applicationNo = $("#applicationNumber").val();
		var validation = 0;
		$("#applicationNumberValidation").hide();
		if(applicationNo=="")
		{
			validation=1;
			$("#applicationNumberValidation").show();
		}
		if(validation==0)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/service.html?method=generateAcknowledgementReceipt&applicationNo="+applicationNo,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#receipt-body").show();
					$("#receiptDetails").html(responseText);
					$("#nextDiv").show();
					$("#receiptDetails").show();
				}
			});
		}
	}
	
</script>
