<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<script>
	var context = "<%=request.getContextPath()%>";
	
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="row">
	<div class="col-xs-12">
	<html:form styleClass="form-horizontal" action="/service.html" styleId="learnerForm">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayName"></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
							
							
							
							<div class="form-group">
								
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="dob"></div>
							</div>
							<h4>Permanent Address</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" id="displayVillage"></div>
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-4" id="displayCountry"></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-4" id="displayAddress"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driving License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>learnerNo<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="learnerNo" styleId="learnerNo"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Region<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="region"
										styleClass="form-control" styleId="region">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="regionList"
											label="headerName" value="headerId"  />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-lg-2">
									<label>Receipt Number<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="receiptNo" styleId="receiptNo"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Receipt Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="receiptDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pull-right">
			<html:text property="pageId" styleId="pageId" /> 
			<html:text property="customerId" styleId="learnerCustomerId"/>
			<button type="button" class="btn btn-primary btn-sm" onClick="formSubmit()">Save</button>
			<button type="reset" class="btn btn-primary btn-sm">Refresh</button>
			<button class="btn btn-primary btn-sm">Delete</button>
			<button class="btn btn-primary btn-sm">New</button>
			<button class="btn btn-primary btn-sm">Close</button>
			<button class="btn btn-primary btn-sm">Find</button>
		</div>
		<div id="displayMsgDiv">&nbsp;</div>
		</html:form>
	</div>
</div>

 
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
		
		
		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
					
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
	
			</script>
			<script>
			var name 		= '<%=request.getAttribute("name")%>';
			alert(name);
			var bloodgroup	= '<%=request.getAttribute("bloodgroup")%>';
			var dob			= '<%=request.getAttribute("dob")%>';
			var dzongkhag 	= '<%=request.getAttribute("dzongkhag")%>';
			var gewog 		= '<%=request.getAttribute("gewog")%>';
			var address 	= '<%=request.getAttribute("address")%>';
			var country 	= '<%=request.getAttribute("country")%>';
			var village		= '<%=request.getAttribute("village")%>';
			var learnerlicenseNo = '<%=request.getAttribute("learnerlicenseNo")%>';
			var customerId	= '<%=request.getAttribute("customerId")%>';
			var region		= '<%=request.getAttribute("region")%>';
			var expirydate	= '<%=request.getAttribute("expirydate")%>';
			var issuedate	= '<%=request.getAttribute("issuedate")%>';
			
			$('#displayName').html("<label class='control-label'>"+name+"</label>");
			$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
			$('#dob').html("<label class='control-label'>"+dob+"</label>");
			$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
			$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
			$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
			$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
			$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
			$('#licenseNo').val(learnerlicenseNo);
			$('#learnerCustomerId').val(customerId);
		//	$('#region').val(region);
			$('#displayregion').html("<label class='control-label'>"+region+"</label>");
			$('#displaylearner').html("<label class='control-label'>"+learnerlicenseNo+"</label>");
			$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
			$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");

			function formSubmit()
			{	alert('ascascascacasc');
				var options = {target:'#displayMsgDiv',url:context+'/service.html?method=issue_non_commercial_driving_license',type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDiv').show();
		        setTimeout('hideStatus("displayMsgDiv")',2000);
		        setTimeout('reloadPage()',2000);
			}
			
			</script>
			 