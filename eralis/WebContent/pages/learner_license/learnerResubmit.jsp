<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<script>
		var context = "<%=request.getContextPath()%>";
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		New Learner License Resubmit
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			issuance of new learner license
		</small>
	</h1>
</div><!-- /.page-header -->
<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
<div class="row">
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Information</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Name:</label>
							</div>
							<div class="col-lg-4" id="displayOwnerName"><%=dto.getName() %></div>
							<div class="col-lg-2">
								<label class="control-label">Blood Group:</label>
							</div>
							<div class="col-lg-4" id="displayBloodgroup"><%=dto.getBloodgroup() %></div>
						</div>
						
						<div class="form-group">
							
							<div class="col-lg-2">
								<label class="control-label">Date Of Birth:</label>
							</div>
							<div class="col-lg-4" id="displayDob"><%=dto.getDOB() %></div>
							
						</div>
						
						<h4>Permanent Address</h4>
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Dzongkhag:</label>
							</div>
							<div class="col-lg-4" id="displayDzongkhag"><%=dto.getDzongkhag() %></div>
							
							<div class="col-lg-2">
								<label class="control-label">Gewog:</label>
							</div>
							<div class="col-lg-4" id="displayGewog"><%=dto.getGewog() %></div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Village:</label>
							</div>
							<div class="col-lg-4" id="displayVillage"><%=dto.getVillage() %></div>
							
							<div class="col-lg-2">
								<label class="control-label">Country:</label>
							</div>
							<div class="col-lg-4" id="displayCountry"><%=dto.getCountry() %></div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Address:</label>
								
							</div>
							<div class="col-lg-4" id="displayAddress"><%=dto.getAddress() %></div>
							<html:hidden property="applicationNo" value="<%=dto.getApplicationNo() %>"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/pages/learner_license/include_new_learner_resubmit.jsp"></jsp:include>
 	<div class="row">
		<div id="displayMsgDiv"></div>
	</div>
	  	 
</div>
</html:form>