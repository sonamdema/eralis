<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Learner Offence Information
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			offence learner license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html?method=license_license_offence">
			
						<div class="row">
							<div class="col-lg-12">
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Vehicle No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="vehicleNo" styleClass="form-control"></html:text>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('learneroffenceVehicleModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-lg-2">
											<label>Vehicle Owner:</label>
										</div>
										<div class="col-lg-3">
											
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Learner License No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="LLNo" styleClass="form-control"></html:text>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-lg-2">
											<label>Driver:</label>
										</div>
										<div class="col-lg-3">
											
										</div>
									</div>
							</div>
						</div>
						
								
									
								
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Offence Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Offence 1<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="offence1" styleClass="form-control">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="OffenceList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Offence 2:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="offence2" styleClass="form-control">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="OffenceList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Offence 3:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="offence3" styleClass="form-control">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="OffenceList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-lg-2">
									<label class="control-label">Offence Date:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="offencedate"
											styleClass="form-control date-picker"
											styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon"> <i
											class="fa fa-calendar bigger-110"></i> </span>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								
								<div class="col-lg-2">
									<label class="control-label">Time of Inspection <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="timeofInspection"
											styleClass="form-control date-picker"
											styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon"> <i
											class="fa fa-calendar bigger-110"></i> </span>
									</div>
								</div>
								<div class="col-lg-2">
									<label class="control-label">Inspected By <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="inspectedby">
										<html:option value="">--SELECT--</html:option>
										<html:option value="">RSTA</html:option>
										<html:option value="">Traffic Police</html:option>
									</html:select>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Inspection Type <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="inspectedby">
										<html:option value="">--SELECT--</html:option>
										<html:option value="">Normal</html:option>
										<html:option value="">Highway Inspection</html:option>
									</html:select>
								</div>
								<div class="col-lg-2">
									<label class="control-label">Traffic Branch:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="trafficbranch" styleId="trafficbranch"
										styleClass="form-control"></html:text>
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Place Of Inspection:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="placeofInspection" styleId="trafplaceofInspectionficbranch"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-3">
									
										<html:select property="region">
											<html:option value="">--SELECT--</html:option>
											
										</html:select>
									
									
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drive Types :</label>
								</div>
								<div class="col-lg-3">
									<div class="checkbox">
                                      	<label>
                                            <html:checkbox property="learnerlicense" styleClass="ace"></html:checkbox>
											<span class="lbl"> Learner License</span>
                                        </label>
                                        <label>
                                        <html:checkbox property="bluebook" styleClass="ace"></html:checkbox>
											<span class="lbl"> Blue Book</span>
                                        </label>
		
		                             </div>
								</div>
								<div class="col-lg-2">
									<label>TIN No<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="TINno" styleId="TINno"
										styleClass="form-control"></html:text>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Receipt No:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="receiptNo" styleId="receiptNo"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Receipt Date:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="receiptDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
									</div>								
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Received From:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="receivedfrom" styleId="receivedfrom"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label> Received Date:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="receivedDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
									</div>								
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="pull-right">

			<button class="btn btn-primary btn-sm" onclick="formSubmit()">Save</button>
			<button type="reset" class="btn btn-primary btn-sm">Refresh</button>
			<button class="btn btn-primary btn-sm">Delete</button>
			<button class="btn btn-primary btn-sm">New</button>
			<button class="btn btn-primary btn-sm">Close</button>
			<button class="btn btn-primary btn-sm">Print</button>



		</div>	
		</html:form>
	</div>
</div>

<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									
									<div class="form-group">
										<label>Name:</label>
										<span class="pull-right">
											<input type="text" id="namelearnerlicenseModal" placeholder="Name"  style="width: 200px" />
										</span>
									</div>
									
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidlearnerlicenseModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									
									
									<div class="form-group">
										<label>License No:</label>
										<span class="pull-right">
											<input type="text" id="licensenolearnerlicenseModal" placeholder="CustomerID" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<select id="regionlearnerlicenseModal" style="width: 200px" >
											<option value="">SELECT</option>
											<logic:iterate id="region" name="regionList">
												<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
											</logic:iterate>
										</select> 
										</span>
									</div>
									<div class="form-group">
										<label>License Type:</label>
										<span class="pull-right">
											 <select id="licensetypelearnerlicenseModal" style="width: 200px">
                                      			<option value="">SELECT</option>
                                      			<option value="">commercial</option>
                                      			<option value="">non-commercial</option>
                                      		</select>
										</span>
									</div>
									
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" onclick="searchCommonInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			
			
		</div>
	</div>
</div>

<div id="learneroffenceVehicleModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									
									<div class="form-group">
										<label>Owner Type:</label>
										<span class="pull-right">
											<input type="text" id="ownerTypeAccidentModal" placeholder="Name"  style="width: 200px" />
										</span>
									</div>
									
									<div class="form-group">
										<label>Vehicle Type:</label>
										<span class="pull-right">
											
											<select id="vehicleTypeAccidentModal" style="width: 200px">
											<option value="">SELECT</option>
											<logic:iterate id="vehicleType" name="vehicleTypeList">
												<option value='<bean:write name="vehicleType" property="headerId"/>'><bean:write name="vehicleType" property="headerName"/></option>
											</logic:iterate>
										</select> 
										</span>
									</div>
									
									
									<div class="form-group">
										<label> Vehicle Number:</label>
										<span class="pull-right">
											<input type="text" id="vehiclenumberAccidentModal" placeholder="Vehicle Number" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label> Owner's Name:</label>
										<span class="pull-right">
											<input type="text" id="ownersnameAccidentModal" placeholder="Owner's Name" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidAccidentModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									
									<div class="form-group">
										<label>Engine Number:</label>
										<span class="pull-right">
											<input type="text" id="enginenumberAccidentModal" placeholder="Engine Number" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label>Chassis Number:</label>
										<span class="pull-right">
											<input type="text" id="chasssisnumberAccidentModal" placeholder="Chassis Number" style="width: 200px"  />
										</span>
									</div>
									
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" onclick="searchlearnerOffenceInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="learnerOffenceTable">
		   </div>
			
		</div>
	</div>
</div>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
		
		
		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
	
			</script>

		<script>
			function searchlearnerOffenceInfo()
						{ 
							var ownerType = $('#ownerTypeAccidentModal').val();
							var vehicleType = $('#vehicleTypeAccidentModal').val();
							var vehicleNumber = $('#vehiclenumberAccidentModal').val();
							var ownerName = $('#ownersnameAccidentModal').val();
							var engineNumber = $('#enginenumberAccidentModal').val();
							var chasisNumber = $('#chasssisnumberAccidentModal').val();
							var cidNumber = $('#cidAccidentModal').val();
			
							if(ownerType == "")
								ownerType = "NA";
							if(vehicleType == "")
								vehicleType = "NA";
							if(vehicleNumber == "")
								vehicleNumber = "NA";
							if(ownerName == "")
								ownerName = "NA";
							if(engineNumber == "")
								engineNumber = "NA";
							if(chasisNumber == "")
								chasisNumber = "NA";
							if(cidNumber == "")
								cidNumber = "NA";
			
			
							$.ajax
							({
								type : "POST",
								url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&ownerType="+ownerType+"&vehicleType="+vehicleType+"&vehicleNumber="+vehicleNumber+"&ownerName="+ownerName+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber,
							data : $('form').serialize(),
							cache : false,
							dataType : "html",
							success : function(responseText) 
							{
								$("#learnerOffenceTable").html(responseText);
								$("#learnerOffenceTable").show();
							}
						});
					}

           </script>
