<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Learner License Renewal
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			renew driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			
						<div class="row">
							<div class="col-lg-12">
								<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
									<div id="msgDiv"></div>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Learner License No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="licenseNo" styleClass="form-control" styleId="learnerLicenseNo" readonly="true"></html:text>
												<html:hidden property="learnerLicenseId" styleClass="form-control" styleId="learnerLicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')" >
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-lg-3">
								      		<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a learner license number</label>
								      	</div>
									</div>
									<!--<div class="col-lg-10">
	                                      <html:button property="search" styleClass="btn btn-primary btn-sm">Search</html:button>
										  <html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>                 
                                 
									</div>-->
							</div>
						</div>
						
								
									
								
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayName"></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
							
							
							
							<div class="form-group">
								
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="dob"></div>
							</div>
							<h4>Permanent Address</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" id="displayVillage"></div>
								<div id="internationalDIV">
									<div class="col-lg-2">
										<label class="control-label">Country:</label>
									</div>
									<div class="col-lg-4" id="displayCountry"></div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-4" id="displayAddress"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner License Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Learner License No:</label>
									</div>
									<div class="col-lg-4" id="displaylearner"></div>
									<div class="col-lg-2">
										<label class="control-label">Region:</label>
									</div>
									<div class="col-lg-4" id="displayregion"></div>
								</div>
								
								
								
								<div class="form-group">
									
									<div class="col-lg-2">
										<label class="control-label">Issue Date:</label>
									</div>
									<div class="col-lg-4" id="displayissue"></div>
									<div class="col-lg-2">
										<label class="control-label">Expiry Date:</label>
									</div>
									<div class="col-lg-4" id="displayexpiry"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label>Status:</label>
									</div>
									<div class="col-lg-3">
										
									</div>
								
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label>Drivetypes:</label>
									</div>
									<div id="drivetypelist">
									</div>
		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Lists</h5>
			</div>
			<div class="widget-body">
					<div class="widget-main">
						<div id="renewallistTable">
						</div>
					</div>
			</div>
			</div>
			
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							 <div class="form-group">
								<div class="col-lg-2">
									<label>Renewal Duration:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
	                           			<html:option value="">--SELECT--</html:option>
	                           			<html:option value="12">1 Year</html:option>
	                           			<html:option value="24">2 Year</html:option>
	                           			<html:option value="36">3 Year</html:option>
	                           			<html:option value="48">4 Year</html:option>
	                           			<html:option value="60">5 Year</html:option>
	                           			<html:option value="72">6 Year</html:option>
	                           			<html:option value="84">7 Year</html:option>
	                           			<html:option value="96">8 Year</html:option>
	                           			<html:option value="108">9 Year</html:option>
	                           			<html:option value="120">10 Year</html:option>
	                           			<html:option value="132">11 Year</html:option>
	                           			<html:option value="144">12 Year</html:option>
	                           			<html:option value="156">13 Year</html:option>
	                           			<html:option value="168">14 Year</html:option>
	                           			<html:option value="180">15 Year</html:option>
	                           			<html:option value="192">16 Year</html:option>
	                           			<html:option value="204">17 Year</html:option>
	                           			<html:option value="216">18 Year</html:option>
	                           			<html:option value="228">19 Year</html:option>
	                           			<html:option value="240">20 Year</html:option>
	                           		</html:select>
									<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select renewal duration</label>
								</div>
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="pageId" styleId="pageId" />
			<html:hidden property="customerId" styleId="learnerCustomerId"/>
			<html:hidden property="region" styleId="region"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal>
			



		</div>	
		</html:form>
	</div>
</div>
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidlicenseModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						
						<div class="form-group">
							<label>Learner License No:</label>
							<span class="pull-right">
								<input type="text" id="learnerNolicenseModal" placeholder="Learner License No" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="learnerRenewalListTable">
			</div>
		</div>
	</div>
</div>
<!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
		
		
		<script type="text/javascript">

				$('.fileupload').ace_file_input({
					no_file : 'No File ...',
					btn_choose : 'Choose',
					btn_change : 'Change',
					droppable : false,
					onchange : null,
					thumbnail : false,
					whitelist:'png|jpg|jpeg',
					blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
				});
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
					
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
	
			</script>
			<script>
			function searchCommonInfo()
				{
					var cid = $('#cidlicenseModal').val();
					var learnerno = $('#learnerNolicenseModal').val();
					if(cid == "")
						cid = "NA";
					if(learnerno == "")
						learnerno = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&cid="+cid+"&learnerno="+learnerno+"&searchType=LL_RENEWAL",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							
							$("#learnerRenewalListTable").html(responseText);
							$("#learnerRenewalListTable").show();
						}
					});
					
				}
			</script>
			
			<script>
			//renewal lists
			function searchlearnerrenewallist(learnerLicenseId)
			{
				var learnerLicenseId = $('#learnerLicenseId').val();
				
				if(learnerLicenseId == "")
					learnerLicenseId = "NA";
				
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getlearnerrenewallist&learnerLicenseId="+learnerLicenseId,
					
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewallistTable").html(responseText);
						$("#renewallistTable").show();

						getdrivetype(learnerLicenseId);
					}
				});
			}
			
			function getdrivetype(learnerLicenseId)
			{
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getdrivetype&learnerLicenseId="+learnerLicenseId,
					
					data : $('form').serialize(),
					cache : false,
					async: true,
					dataType : "html",
					success : function(responseText) 
					{
						$("#drivetypelist").html("<label class='control-label'>"+responseText+"</label>");
						$("#drivetypelist").show();
					}
				});
			}
			
			$(document).ready(function()
			{
				$('#submitBtn').click(function()
				{
					var licenseNo = $('#licenseNo').val();
					var renewalDuration	=	$("#renewalDuration").val();
					if(licenseNo == "" || renewalDuration=="")
					{
						if(licenseNo == "")
						{
							$('#licenseNoValidation').show();
							$('#licenseNoValidation').get(0).scrollIntoView();
							return false;
						}
						else if(renewalDuration=="")
						{
							$('#renewalDurationValidation').show();
							$('#renewalDurationValidation').get(0).scrollIntoView();
							return false;
						}

						
						
					}
					else
					{
						//For Learner License Renewal
						var requestType = "LEARNER";
						var serviceType = "RENEWAL";
						var identityNo = $('#learnerLicenseId').val();
						var identityTypeId = "L";
						var renewalDuration = $('#renewalDuration').val();
						getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '','', renewalDuration);
					}
				});
			});

			function formSubmit()
			{
				var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_renewal_learner',type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDiv').show();
		        setTimeout('hideStatus("displayMsgDiv")',10000);
		        //setTimeout('reloadPage()',10000);
			}

			var fileError;
           	function validation(thisform,msgId,fileObj)
           	{
           		var fileId = fileObj.id;
           		with(thisform)
           		{
           			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           		}
           	}
           	
			<%
				String pageIdentifier = (String) request.getAttribute("page_identifier");
				String pageId = (String) request.getAttribute("page_id");
			%>

			var pageIdentifier = "<%=pageIdentifier%>";
			var pageId = "<%=pageId%>";
			</script>
			<link rel="stylesheet" media="screen" href="css/screen.css">
				<style>
					#learnerForm .error { color: red; }
				</style>