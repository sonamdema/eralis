<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="learner-main-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Receipt No</th>
			<th>Receipt Date</th>
			<th>Issue Date</th>
			<th>Learner License No</th>
			<th>Certifying Doctor</th>
			<th>Supporting Doc</th>
	</thead>
	<tbody>
		<logic:notEmpty name="LEARNER_LICENSE_DTLS">
			<logic:iterate id="renewal" name="LEARNER_LICENSE_DTLS">
				<tr>
					<td><bean:write name="renewal" property="receiptNo"/></td> 
					<td><bean:write name="renewal" property="receiptDate"/></td> 
					<td><bean:write name="renewal" property="issuedate"/></td>
					<td><bean:write name="renewal" property="learnerlicenseNo"/></td>
					<td><bean:write name="renewal" property="certifyingDoctor"/></td> 
					<td><jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#learner-main-history-table').DataTable({
	            responsive: true
	    });
	});

</script>


