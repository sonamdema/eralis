<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>

<%
LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<script>
	function formSubmit()
	{
		$('#approveBTN').attr('disabled', true);

		$.blockUI
        ({ 
        	css: 
        	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
        	} 
        });
        
		var application=$("#applicationNo").val();
		var customerId=$("#customerId").val();

		var options = {target:'#messageDiv',url:context+'/license.html?method=license_duplicate_application_approve&applicationNo='+application+'&customerId='+customerId,type:'POST',data: $("#learnerForm").serialize()}; 

	    $("#learnerForm").ajaxSubmit(options);
	    $('#messageDiv').show();
	    setTimeout('hideStatus("messageDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}
	
	function formReject()
	{
		var application=$("#applicationNo").val();
		
		
		var options = {target:'#messageDiv',url:context+'/license.html?method=learner_duplicate_reject&applicationNo='+application,type:'POST',data: $("#learnerForm").serialize()}; 
	
	    $("#learnerForm").ajaxSubmit(options);
	    $('#messageDiv').show();
	    setTimeout('hideStatus("messageDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}

	function dispatch()
	{
		var application=$("#applicationNo").val();
		var options = {target:'#messageDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=LEARNER&serviceType=DUPLICATION',type:'POST',data: $("#learnerForm").serialize()}; 
	    $("#learnerForm").ajaxSubmit(options);
	    $('#messageDiv').show();
	    setTimeout('hideStatus("messageDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}
		
	var a="<%=request.getAttribute("applicationNo")%>";
	$("#applicationNo").val(a);

	var c="<%=request.getAttribute("customerId")%>";
	$("#customerId").val(c);
		
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Learner License
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			    Application For License Replacement
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
	
	 <div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Learner License Replacement Application Details for Application No: <strong><%=request.getAttribute("applicationNo")%></strong></h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
						
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Learner License No:</label>
								</div>
								<div class="col-lg-4" ><label class="control-label"><%=dto.getLearnerlicenseno()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" ><label class="control-label"><%=dto.getName()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Date of Birth:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getDOB()%></label></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Gender:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getGender() %></label></div>
								<div class="col-lg-2">
									<label class="control-label">Occupation:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getOccupation()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getBloodgroup()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getGewog()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getVillage()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getRegion()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Remarks:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getRemarks()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Submission Date:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getAppsubmissiondate()%></label></div>
							</div> 
							<div class="form-group">
								<div class="col-lg-12">
									<h5 class="widget-title lighter"><strong><u>Payment Details</u></strong></h5>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Receipt Number:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Receipt Date:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getReceiptDate()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Amount Paid:</label>
								</div>
								<div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getAmount()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Penalty Paid:</label>
								</div>
								<div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getPenalty()%></label></div>
							</div>
							<logic:equal value="APPROVE" name="param">
							<div class="form-group">
								<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
								<html:hidden property="learnerLicenseId" value="<%=dto.getLearnerLicenseId()%>"></html:hidden>
							</div>
							</logic:equal>
		                </div>      
                    </div>
               </div>
        </html:form>
     	<jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	 </div>
	 <input type="hidden" id="applicationNo"/>
<div id="messageDiv"></div> 
<div>
	<logic:equal value="APPROVE" name="param">
	<button type="button" class="btn btn-sm" id="approveBTN" onclick="formSubmit()">
		<i class="ace-icon fa fa-check"  ></i>
		Approve
	</button>
	<button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
		<i class="ace-icon fa fa-times red2"></i>
		Reject
	</button>
	</logic:equal>
	<logic:equal value="DISPATCH" name="param">
	<button type="button" class="btn btn-sm" onclick="dispatch()">
		<i class="ace-icon fa fa-check"  ></i>
		Dispatch
	</button>
	</logic:equal>
</div>