<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Replacement of Learner License
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			replacement of learner license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<div id="msgDiv"></div>
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			
						<div class="row">
							<div class="col-lg-12">
								<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">License No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="licenseNo" styleClass="form-control" styleId="learnerLicenseNo" readonly="true"></html:text>
												<html:hidden property="learnerLicenseId" styleClass="form-control" styleId="learnerLicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('duplicationModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>	
											</div>
										</div>
										<div class="col-lg-3">
								      		<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a learner license number</label>
								      	</div>
									</div>
							</div>
						</div>
				
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayName"></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
							<div class="form-group">
								
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="dob"></div>
							</div>
							<h4>Permanent Address</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" id="displayVillage"></div>
								<div id="internationalDIV">
									<div class="col-lg-2">
										<label class="control-label">Country:</label>
									</div>
									<div class="col-lg-4" id="displayCountry"></div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-4" id="displayAddress"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner License Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Learner License No:</label>
									</div>
									<div class="col-lg-4" id="displaylearner"></div>
									<div class="col-lg-2">
										<label class="control-label">Region:</label>
									</div>
									<div class="col-lg-4" id="displayregion"></div>
								</div>
								
								
								
								<div class="form-group">
									
									<div class="col-lg-2">
										<label class="control-label">Issue Date:</label>
									</div>
									<div class="col-lg-4" id="displayissue"></div>
									<div class="col-lg-2">
										<label class="control-label">Last Expiry Date:</label>
									</div>
									<div class="col-lg-4" id="displayexpiry"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label>Status:</label>
									</div>
									<div class="col-lg-3">
										
									</div>
								
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label>Drivetypes:</label>
									</div>
									<div id="drivetypelist">
									</div>
								
								</div>
							
							
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Replacement Lists</h5>
			</div>
			<div class="widget-body">
					<div class="widget-main">
						<div id="duplicatelistTable">
						</div>
					</div>
			</div>
			</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Replacement Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<!--<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Issue Date <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="issuedate" styleClass="form-control date-picker" styleId="issuedate"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								
							</div>
							--><div class="form-group">
								<!--<div class="col-lg-2">
									<label class="control-label">Delivered On:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="deliveredOn" styleClass="form-control date-picker" styleId="deliveredOn"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								--><div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="attchValidation">Please attach your file here</label>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="region" styleId="region"/>
			<html:hidden property="pageId" styleId="pageId" />
			<html:hidden property="customerId" styleId="learnerCustomerId"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal>
		</div>	
		</html:form>
	</div>
</div>

<div id="duplicationModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidduplicationModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						
						<div class="form-group">
							<label>Learner License No:</label>
							<span class="pull-right">
								<input type="text" id="learnerNoduplicationModal" placeholder="Learner License No" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="learnerDuplicateListTable">
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
		$('.fileupload').ace_file_input({
			no_file : 'No File ...',
			btn_choose : 'Choose',
			btn_change : 'Change',
			droppable : false,
			onchange : null,
			thumbnail : false,
			whitelist:'png|jpg|jpeg',
			blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
		});
		//datepicker plugin
		//link
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});

	</script>
<script>
function searchCommonInfo()
	{
		var cid = $('#cidduplicationModal').val();
		var learnerno = $('#learnerNoduplicationModal').val();
		if(cid == "")
			cid = "NA";
		if(learnerno == "")
			learnerno = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&cid="+cid+"&learnerno="+learnerno,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#learnerDuplicateListTable").html(responseText);
				$("#learnerDuplicateListTable").show();
			}
		});
	}
</script>
<script>
			//renewal lists
function searchlearnerrenewallist(learnerLicenseId)
{
		 
		
		if(learnerLicenseId == "")
			learnerLicenseId = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getlearnerduplicationlist&learnerLicenseId="+learnerLicenseId,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#duplicatelistTable").html(responseText);
				$("#duplicatelistTable").show();

				getdrivetype(learnerLicenseId);
			}
		});
}
function getdrivetype(learnerLicenseId)
{
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/common.html?method=getdrivetype&learnerLicenseId="+learnerLicenseId,
		
		data : $('form').serialize(),
		cache : false,
		async: true,
		dataType : "html",
		success : function(responseText) 
		{
			
			$("#drivetypelist").html("<label class='control-label'>"+responseText+"</label>");
			$("#drivetypelist").show();
		}
	});
}

$(document).ready(function()
{
	$('#submitBtn').click(function()
	{
		var licenseNo = $('#licenseNo').val();
		
		if(licenseNo == "")
		{
			$('#licenseNoValidation').show();
			$('#licenseNoValidation').get(0).scrollIntoView();
			return false;
		}
		else
		{
			//For Learner License Duplicate
			var requestType = "LEARNER";
			var serviceType = "DUPLICATE";
			var identityNo = $('#learnerLicenseId').val();
			var identityTypeId = "L";
			
			getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '');
		}
	});
});

function formSubmit()
{
	var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_duplication_learner',type:'POST',data: $("#learnerForm").serialize()}; 
    $("#learnerForm").ajaxSubmit(options);
       $('#displayMsgDiv').show();
       setTimeout('hideStatus("displayMsgDiv")',10000);
       //setTimeout('reloadPage()',10000);
}

	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>

	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";


	
	var fileError;
   	function validation(thisform,msgId,fileObj)
   	{
   		var fileId = fileObj.id;
   		with(thisform)
   		{
   			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
   			{
   				document.getElementById(fileId).value = "";
   				return false;
   			}
   			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
   			{
   				document.getElementById(fileId).value = "";
   				return false;
   			}
   		}
   	}





				
			</script>
			<link rel="stylesheet" media="screen" href="css/screen.css">
				<style>
					#learnerForm .error { color: red; }
				</style>
			
			
			