<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">License Details</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label>Certifying Doctor:</label>
							</div>
							<div class="col-lg-3">
								<html:text property="certifyingDoctor" styleId="certifyingDoctor" value="<%=dto.getCertifyingDoctor() %>"
									styleClass="form-control"></html:text>
							</div>
							<div class="col-lg-2">
								<label>Remarks:</label>
							</div>
							<div class="col-lg-3">
								<html:textarea property="remarks" styleId="remarks"
									styleClass="form-control"><%=dto.getRemarks() %></html:textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-2">
								<label>Drivetypes:</label>
							</div>
							<div class="col-sm-10">
								<jsp:include page="/pages/common/drivetype.jsp"></jsp:include>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-2">
								<label>Medical Certificate<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="fileMC" styleId="fileMC" styleClass="form-control fileupload"></html:file>
							</div>
							<div class="col-lg-2">
								<label>Application Form<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="fileApplForm" styleId="fileApplForm" styleClass="form-control fileupload"></html:file>
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" id="driveTypeIds" name="driveTypeIds"/>
		</div>
	</div>
<script>
	function getCheckedVals()
	{
		var listSize = "<%=(Integer) request.getAttribute("DRIVE_TYPE_LIST_SIZE")%>";
		var arrLen = listSize;
		var vals = '';
		var countFlag = 0;
	 	for(var i=0;i<arrLen;i++)
	  	{
		  	var driveTypeId = "0";
		 	if($('#driveType_'+i).is(':checked'))
		 		driveTypeId = $('#driveType_'+i).val();
	 		if(parseInt(countFlag) > 0){
				vals = vals + '#';
		 	}
		 	vals = vals + driveTypeId;
		 	countFlag++;
	  	}
		$('#driveTypeIds').val(vals);
	}
</script>