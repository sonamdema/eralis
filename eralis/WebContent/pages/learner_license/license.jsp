<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		New Learner License
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			issuance of new learner license
		</small>
	</h1>
</div><!-- /.page-header -->
<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
<div class="row">
	<div class="col-xs-12">
		<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
		<div class="row">
			<div class="col-lg-12">
				<div id="Msg" style="display:none;"></div>
				<div id="msgDiv" style="display:none;"></div>
				<div id="validationMsg" style="display:none;"></div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Citizen ID:</label>
					</div>
					<div class="col-lg-3">
						<div class="input-group">
							<html:text property="CID" styleClass="form-control" styleId="cid" readonly="true"></html:text>
							<span class="input-group-btn">
								<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licensemodalform')">
									<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								</button>
							</span>
						</div>
					</div>
					<div class="col-lg-3">
			      		<label style="display:none;color: #ff0000" id="citizenIDValidation">Select a personal information</label>
			      	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Information</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Name:</label>
							</div>
							<div class="col-lg-4" id="displayOwnerName"></div>
							<div class="col-lg-2">
								<label class="control-label">Blood Group:</label>
							</div>
							<div class="col-lg-4" id="displayBloodgroup"></div>
						</div>
						
						<div class="form-group">
							
							<div class="col-lg-2">
								<label class="control-label">Date Of Birth:</label>
							</div>
							<div class="col-lg-4" id="displayDob"></div>
							
							<div class="col-lg-2">
								<label class="control-label">CID No:</label>
							</div>
							<div class="col-lg-4" id="displayCID"></div>
							 
							
						</div>
						
						<h4>Permanent Address</h4>
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Dzongkhag:</label>
							</div>
							<div class="col-lg-4" id="displayDzongkhag"></div>
							
							<div class="col-lg-2">
								<label class="control-label">Gewog:</label>
							</div>
							<div class="col-lg-4" id="displayGewog"></div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Village:</label>
							</div>
							<div class="col-lg-4" id="displayVillage"></div>
							
							<div class="col-lg-2">
								<label class="control-label">Country:</label>
							</div>
							<div class="col-lg-4" id="displayCountry"></div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Address:</label>
								
							</div>
							<div class="col-lg-4" id="displayAddress"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">License Details</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label>Certifying Doctor:</label>
							</div>
							<div class="col-lg-3">
								<html:text property="certifyingDoctor" styleId="certifyingDoctor"
									styleClass="form-control"></html:text>
							</div>
							<div class="col-lg-2">
								<label>Remarks:</label>
							</div>
							<div class="col-lg-3">
								<html:textarea property="remarks" styleClass="form-control"></html:textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-2">
								<label>Region<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<input type="text" disabled="disabled" value="<%=regionName %>" class="form-control"/>
								<html:hidden property="region" value="<%=regionId %>"/>
							</div>
							
						</div>
						<div class="form-group">
								<div class="col-lg-2">
									<label>Drivetypes:</label>
								</div>
								<div class="col-sm-10">
									<logic:notEmpty name="DRIVE_TYPE_LIST">
										<logic:iterate id="driveType" name="DRIVE_TYPE_LIST" type="bt.gov.rsta.framework.dto.DropDownDTO" indexId="index">
											<%
												int a = index.intValue();
											%>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="ace" id='<%="driveTypeId_"+a %>' value="<%=driveType.getHeaderId() %>"/>
													<span class="lbl"> <bean:write name="driveType" property="headerName"/></span>
												</label>
											</div>
										</logic:iterate>
									</logic:notEmpty>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-2">
								<label>Medical Certificate<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="fileMC" styleId="fileMC" styleClass="form-control fileupload" onchange="validation('learnerForm','fileMCValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="fileMCValidation">Please attach your file here</label>
							</div>
							<div class="col-lg-2">
								<label>Application Form<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="fileApplForm" styleId="fileApplForm" styleClass="form-control fileupload" onchange="validation('learnerForm','fileApplFormValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="fileApplFormValidation">Please attach your file here</label>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
	<div class="pull-right">
		<html:hidden property="pageId" styleId="pageId" />
		<input type="hidden" id="driveTypeIds" name="driveTypeIds"/>
		<html:hidden property="customerId" styleId="learnerCustomerId"/>
		<logic:equal value="Y" name="priviledge" property="isNew">
			<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
		</logic:equal>
		<%-- <logic:equal value="Y" name="priviledge" property="isEdit">
			<button class="btn btn-primary btn-sm">Update</button>
		</logic:equal>
		<logic:equal value="Y" name="priviledge" property="isDelete">
			<button class="btn btn-primary btn-sm">Delete</button>
		</logic:equal> --%>
	</div>	
</div>
</html:form>
					
<div id="licensemodalform" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									
									<div class="form-group">
										<label>Customer ID:</label>
										<span class="pull-right">
											<input type="text" id="customerIdPersonalModal" placeholder="CustomerID" style="width: 200px"  />
										</span>
									</div>
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="licenselisttable">
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript">

		$('.fileupload').ace_file_input({
			no_file : 'No File ...',
			btn_choose : 'Choose',
			btn_change : 'Change',
			droppable : false,
			onchange : null,
			thumbnail : false,
			whitelist:'png|jpg|jpeg',
			blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
		});
		
		var listSize = "<%=(Integer) request.getAttribute("DRIVE_TYPE_LIST_SIZE")%>";
		var arrLen = listSize;
		
		//datepicker plugin
		//link
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
		
		function searchCommonInfo()
		{
			var name = $('#namePersonalModal').val();
			var cidNumber = $('#cidPersonalModal').val();
			var customerId = $('#customerIdPersonalModal').val();
	
			if(name == "")
				name = "NA";
			if(cidNumber == "")
				cidNumber = "NA";
			if(customerId == "")
				customerId = "NA";
	
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList&name="+name+"&cid="+cidNumber+"&customerId="+customerId+"&searchType=LEARNER_NEW",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#licenselisttable").html(responseText);
					$("#licenselisttable").show();
				}
			});
		}
	
		function getCheckedVals()
		{
			var vals = '';
			var countFlag = 0;
	
		 	for(var i=0;i<arrLen;i++)
		  	{
			  	var driveTypeId = "0";
			  	
			 	if($('#driveTypeId_'+i).is(':checked'))
			 		driveTypeId = $('#driveTypeId_'+i).val();
	
		 		if(parseInt(countFlag) > 0){
					vals = vals + '#';
			 	}
	
			 	vals = vals + driveTypeId;
			 	countFlag++;
		  	}
		  	
			$('#driveTypeIds').val(vals);
		}

		$(document).ready(function()
		{
			$("#learnerForm").validate({
					invalidHandler: function(form, validator) {
			             var errors = validator.numberOfInvalids();
			             if (errors) {                    
			                 var firstInvalidElement = $(validator.errorList[0].element);
			                 $('html,body').scrollTop(firstInvalidElement.offset().top);
			                 firstInvalidElement.focus();
			             }
			         	},
						rules:
						{
							certifyingDoctor:{
								required:true
							}
						},    
						messages:
						{
							certifyingDoctor:{required: "Please enter certifying doctor"}
						}
				});
	
			$('#submitBtn').click(function(){
				if($('#learnerForm').valid()) 
				{
					var cidVal = $('#cid').val();
					if(cidVal == "")
					{
						$('#citizenIDValidation').show();
						$('#citizenIDValidation').get(0).scrollIntoView();
						return false;
					}
					else
					{
						getCheckedVals();
						//For Learner License New
						var requestType = "LEARNER";
						var serviceType = "NEW";
						var identityNo = "";
						var identityTypeId = "L";
				
						getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '');
					}
				}
				else 
				{
					return false;
				}
			});
		});
		function formSubmit()
		{
	        var options = {target:'#displayMsgDiv',url:context+'/license.html?method=learner_license_new',type:'POST',data: $("#learnerForm").serialize()}; 
		    $("#learnerForm").ajaxSubmit(options);
	        $('#displayMsgDiv').show();
	        setTimeout('hideStatus("displayMsgDiv")',10000);
	        //setTimeout('reloadPage()',10000);
		}
		var fileError;
	   	function validation(thisform,msgId,fileObj)
	   	{
	   		var fileId = fileObj.id;
	   		with(thisform)
	   		{
	   			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
	   			{
	   				document.getElementById(fileId).value = "";
	   				return false;
	   			}
	   			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
	   			{
	   				document.getElementById(fileId).value = "";
	   				return false;
	   			}
	   		}
	   	}
	
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
</script>
<style>
	#learnerForm .error { color: red; }
</style>