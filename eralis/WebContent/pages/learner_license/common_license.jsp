<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="row">
	<html:form styleClass="form-horizontal">
		
	<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Learner License No <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="LLNo" styleId="LLNo" styleClass="form-control">></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Region <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="region" styleId="region" styleClass="form-control">></html:text>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Issue Date <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="issuedate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								<div class="col-lg-2">
									<label>Receipt No <span style="color: #ff0000">*</span> : </label>
								</div>
								<div class="col-lg-3">
									<html:text property="receiptNo" styleId="receiptNo" styleClass="form-control">></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Expiry Date <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="expiryDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								<div class="col-lg-2">
									<label>Receipt Date <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="receiptDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Certifying Doctor :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="certifyingDoctor" styleId="certifyingDoctor"
										styleClass="form-control"></html:text>
								</div>
								<div class="col-lg-2">
									<label>Remarks :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="remarks" styleId="remarks"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Delivery On :</label>
								</div>
								<div class="col-lg-3">
									<html:text property="deliveryon"
										styleId="deliveryon" styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Renewal Date <span style="color: #ff0000">*</span> :</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="renewaldate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>	
		
		
		
	</html:form>
</div>