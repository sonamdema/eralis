<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Learner License
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Learner License Suspension
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<div id="msgDiv"></div>
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerSuspensionForm">
			<div class="row">
				<div class="col-lg-12">
					<div id="msgDiv"></div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Learner License No:</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="licenseNo" styleClass="form-control" styleId="learnerLicenseNo" readonly="true"></html:text>
								<html:hidden property="learnerLicenseId" styleClass="form-control" styleId="learnerLicenseId"></html:hidden>
								<span class="input-group-btn">
									<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')" >
										<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="col-lg-3">
				      		<label style="display:none;color: #ff0000" id="learnerNoValidation">Please search a learner license number</label>
				      	</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Personal Information</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Name:</label>
									</div>
									<div class="col-lg-4" id="displayName"></div>
									<div class="col-lg-2">
										<label class="control-label">Blood Group:</label>
									</div>
									<div class="col-lg-4" id="bloodgroup"></div>
								</div>
								<div class="form-group">

									<div class="col-lg-2">
										<label class="control-label">Date Of Birth:</label>
									</div>
									<div class="col-lg-4" id="dob"></div>
								</div>
								<h4>Permanent Address</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Dzongkhag:</label>
									</div>
									<div class="col-lg-4" id="displayDzongkhag"></div>
									<div class="col-lg-2">
										<label class="control-label">Gewog:</label>
									</div>
									<div class="col-lg-4" id="displayGewog"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Village:</label>
									</div>
									<div class="col-lg-4" id="displayVillage"></div>
									<div id="internationalDIV">
										<div class="col-lg-2">
											<label class="control-label">Country:</label>
										</div>
										<div class="col-lg-4" id="displayCountry"></div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label>Address:</label>
									</div>
									<div class="col-lg-4" id="displayAddress"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner License Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Learner License No:</label>
								</div>
								<div class="col-lg-4" id="displaylearner"></div>
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-4" id="displayregion"></div>
							</div>
							<div class="form-group">

								<div class="col-lg-2">
									<label class="control-label">Issue Date:</label>
								</div>
								<div class="col-lg-4" id="displayissue"></div>
								<div class="col-lg-2">
									<label class="control-label">Expiry Date:</label>
								</div>
								<div class="col-lg-4" id="displayexpiry"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Status:</label>
								</div>
								<div class="col-lg-3">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drivetypes:</label>
								</div>
								<div id="drivetypelist">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner License Suspension Lists</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div id="suspensionlistTable">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner License Suspension Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12"><div class="form-group">
							<div class="col-lg-3">
								<label>Start Date <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								<div class="input-group">
									<html:text property="startDate"
										styleClass="form-control date-picker"
										styleId="startDate" readonly="true"></html:text>
									<span class="input-group-addon"> <i
										class="fa fa-calendar bigger-110"></i> </span>
								</div>
							</div>
							<div class="col-lg-2">
								<label>End Date <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								<div class="input-group">
									<html:text property="endDate"
										styleClass="form-control date-picker"
										styleId="endDate" readonly="true"></html:text>
									<span class="input-group-addon"> <i
										class="fa fa-calendar bigger-110"></i> </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label>Reason For Suspension <span style="color: #ff0000">*</span> :</label>
							</div>
							<div class="col-lg-3">
								<html:textarea property="reason" styleId="reason"
									styleClass="form-control"></html:textarea>
							</div>
						</div>
						<div class="col-lg-3">
							<label> Document <span style="color: #ff0000">*</span> :</label>
						</div>
						<div class="col-lg-3">
							<html:file property="imgPath" styleId="suspenDocumentFile" styleClass="form-control fileupload" onchange="validation('licenseSuspension','suspenDocumentValidation',this)"></html:file>
							<label style="display:none;color: #ff0000" id="suspenDocument">Please attach file here</label>
							<label style="display:none;color: #ff0000" id="suspenDocumentValidation"></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="displayMsgDiv"></div>
	<div class="pull-right">
		<html:hidden property="customerId" styleId="learnerCustomerId"/>
		<logic:equal value="Y" name="priviledge" property="isNew">
			<button type="button" class="btn btn-primary btn-sm" onClick="formSubmit()" id="submitBtn">Submit</button>
		</logic:equal>
	</div>
		</html:form>
	</div>
</div>
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidlicenseModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>

						<div class="form-group">
							<label>Learner License No:</label>
							<span class="pull-right">
								<input type="text" id="learnerNolicenseModal" placeholder="Learner License No" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="learnerRenewalListTable">
			</div>
		</div>
	</div>
</div>
<!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
	$('.fileupload').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});
	//datepicker plugin
	//link
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

</script>
<script>
	function searchCommonInfo()
	{
		$("#learnerNoValidation").hide();
		var cid = $('#cidlicenseModal').val();
		var learnerno = $('#learnerNolicenseModal').val();
		if(cid == "")
			cid = "NA";
		if(learnerno == "")
			learnerno = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&searchType=LEARNER_SUSPENSION&cid="+cid+"&learnerno="+learnerno,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText)
			{
				$("#learnerRenewalListTable").html(responseText);
				$("#learnerRenewalListTable").show();
			}
		});
	}
</script>
<script>

	$(document).ready(function()
	{
		$("#learnerSuspensionForm").validate(
		{
			rules:
			{
				reason:{
					required:true,
				},
				startDate:{
					required:true,
				},
				endDate:{
					required:true,
				},
				learnerLicenseNo:{
					required:true,
				}
			},    
			messages:
			{
				reason:{required: "Please Provide A Valid Reason"},
				startDate:{required: "Please Provide Start Date"},
				endDate:{required: "Please Provide End Date"},
				learnerLicenseNo:{required: "Please Provide Learner License No"}
			}
		});
	});


			
	function formSubmit()
	{
		var learnerLicenseNo = $("#learnerLicenseNo").val();
		if(learnerLicenseNo == "")
		{
			$('#learnerNoValidation').show();
			$('#learnerNoValidation').get(0).scrollIntoView();
			return false;
		}
		else
		{
			if($('#learnerSuspensionForm').valid()) 
			{
				var suspenDocumentFile = $("#suspenDocumentFile").val();
				if(suspenDocumentFile!=""){
					var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_suspension&FORM_TYPE=LL',type:'POST',data: $("#learnerSuspensionForm").serialize()}; 
				    $("#learnerSuspensionForm").ajaxSubmit(options);
			        $('#displayMsgDiv').show();
			        setTimeout('hideStatus("displayMsgDiv")',4000);
				}
				else
				{
					$("#suspenDocument").show();

				}
			}
		}
	}

	var fileError;
	function validation(thisform,msgId,fileObj)
	{
		var fileId = fileObj.id;
		with(thisform)
		{
			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
			{
				document.getElementById(fileId).value = "";
				return false;
			}
			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
			{
				document.getElementById(fileId).value = "";
				return false;
			}
		}
	}
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>
	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";
</script>
<style>
	#learnerSuspensionForm .error { color: red; }
</style>