<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="learner-search-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th>CID</th>
			<th>License Number</th>
			<th>Region</th>
			<th>Dzongkhag</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_LEARNER_LIST">
			<logic:iterate id="renewal" name="RENEWAL_LEARNER_LIST">
				<tr>
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="renewal" property="personalInfoId"/>' onclick="getSelectedItem(this.id)">SELECT</button>
					</td>
					<td><bean:write name="renewal" property="name"/></td>
					<td><bean:write name="renewal" property="CID"/></td>
					<td><bean:write name="renewal" property="licenseNo"/></td>
					<td><bean:write name="renewal" property="region"/></td>
					<td><bean:write name="renewal" property="dzongkhag"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
<div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelectedItem()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>
<script>
	$(document).ready(function() 
	{
   		$('#learner-search-table').DataTable({
            responsive: true
    	});
	});
	<%
		String searchType = (String)request.getAttribute("SEARCH_TYPE");
	%>
	
	var searchType = "<%=searchType%>";
	function getSelectedItem(globalId)
	{
		 $("#msgDiv").hide();
		 $("#submitBtn").prop('disabled', false);
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getLearnerLicenseInfoList&learnerlicenseInfoId='+globalId+'&searchType='+searchType,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					var name = $(this).find('name').text();
					var bloodgroup=$(this).find('bloodgroup').text();
					var dob=$(this).find('dob').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var country = $(this).find('country').text();
					var village=$(this).find('village').text();
					var learnerlicenseNo=$(this).find('learnerlicenseNo').text();
					var isInternational = $(this).find('isInternational').text();
					var customerId=$(this).find('customerId').text();
					var region=$(this).find('region').text();
					var regionName = $(this).find('region-name').text();
					var expirydate=$(this).find('expirydate').text();
					var issuedate=$(this).find('issuedate').text();
					var status=$(this).find('status').text();
					var learnerLicenseId=$(this).find('learnerLicenseId').text();
					var personalInfoId=$(this).find('personalInfoId').text();
					var CID=$(this).find('CID').text();
					var gender=$(this).find('gender').text();
					var phoneNo=$(this).find('phoneNo').text();
					var fatherName=$(this).find('fatherName').text();
					var cancellationDate = $(this).find('cancellationDate').text();
					var cancellationReason = $(this).find('cancellationReason').text();
					var amount = $(this).find('amount').text();
					var receiptDate = $(this).find('receiptDate').text();
					var receiptNo = $(this).find('receiptno').text();
					
					if(name=="" || name=="null" || dzongkhag=="" || dzongkhag=="null" || CID=="" || CID=="null" || gender=="" || gender=="null"
						|| phoneNo=="" || phoneNo=="null" || bloodgroup=="" || bloodgroup=="null")
						
					{
						$("#msgDiv").show();
						$("#msgDiv").addClass("alert alert-danger");
						$("#msgDiv").html("Incomplete information of learner license holder of <b>Customer Id : " + customerId+"</b>");
						return false;
					}
					
					
					
					if(status == "LICENSE_EXPIRED")
					{
						$("#msgDiv").html("<div class='alert alert-danger'>The learner license has been expired </div>");
						$('#msgDiv').show();
				        setTimeout('hideStatus("msgDiv")',3000);
				        $('#learnerLicenseNo').val('');
					}
					$('#initialReceiptNo').html("<label class='control-label'>"+receiptNo+"</label>");
					$('#initialAmount').html("<label class='control-label'>"+amount+"</label>");
					$('#initialReceiptDate').html("<label class='control-label'>"+receiptDate+"</label>");
					

					$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
					$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
					$('#LearnerHolderName').html("<label class='control-label'>"+name+"</label>");
					$('#learnerLicenseNo').val(learnerlicenseNo);
					$('#newLearnerLicenseNo').val(learnerlicenseNo);
					$('#newLearnerLicenseId').val(learnerLicenseId);
					
					if(searchType=='LEARNER_HISTORY')
					{
						if(gender=='M')
							gender="Male";
						else if(gender=='F')
							gender="Female";
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayCustomerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#displayCID').html("<label class='control-label'>"+CID+"</label>");
						$('#displayDateOfBirth').html("<label class='control-label'>"+dob+"</label>");
						$('#displayPhoneNo').html("<label class='control-label'>"+phoneNo+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fatherName+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
						$('#learnerNo').val(learnerlicenseNo);
						$('#learnerLicenseId').val(learnerLicenseId);
						$('#learnerCustomerId').val(customerId);
						getLearnerHistory();
						get_PIS(personalInfoId);
					}
					else if(searchType=='WITHDRAW_LEARNER_CANCELLATION')
					{
						if(status == "LEARNER_CANCELLED")
						{
							$('#cancellationDate').val(cancellationDate);
							$('#cancellationReason').val(cancellationReason);
							$('#LearnerHolderName').html("<label class='control-label'>"+name+"</label>");
							$('#displayName').html("<label class='control-label'>"+name+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#dob').html("<label class='control-label'>"+dob+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
							$('#learnerCustomerId').val(customerId);
							$('#region').val(region);
							$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
							$('#displaylearner').html("<label class='control-label'>"+learnerlicenseNo+"</label>");
							$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
							$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
							$('#learnerLicenseId').val(learnerLicenseId);
							$('#learnerLicenseNo').val(learnerlicenseNo);
							$('#drivinglicenseId').val('0');
							if(isInternational == "I")
							{
								$('#internationalDIV').show();
							}
							else
							{
								$('#internationalDIV').hide();
							}
						}
						else
						{
							$("#msgDiv").html("<div class='alert alert-danger'>The learner has no cancellation record </div>");
							$('#msgDiv').show();
					        $('#learnerLicenseNo').val('');
						}
					}
					else
					{
						if(status == "SUCCESS")
						{
							$('#LearnerHolderName').html("<label class='control-label'>"+name+"</label>");
							$('#displayName').html("<label class='control-label'>"+name+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#dob').html("<label class='control-label'>"+dob+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
							$('#learnerCustomerId').val(customerId);
							$('#region').val(region);
							$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
							$('#displaylearner').html("<label class='control-label'>"+learnerlicenseNo+"</label>");
							$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
							$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
							$('#learnerLicenseId').val(learnerLicenseId);
							$('#learnerLicenseNo').val(learnerlicenseNo);
							$('#drivinglicenseId').val('0');
							if(isInternational == "I")
							{
								$('#internationalDIV').show();
							}
							else
							{
								$('#internationalDIV').hide();
							}


							if(searchType=='LEARNER_SUSPENSION')
							{ 
								getSuspensionList();
							}
							else if(searchType=='LEARNER_CANCELLATION')
							{ 
								getCancellationList();
							}
							else
							{ 
								searchlearnerrenewallist(learnerLicenseId);
								//searchlearnerduplicationlist(learnerLicenseId);
							}
						}
						else if(status == "HAS_OFFENCE")
						{
							$("#msgDiv").html("<div class='alert alert-danger'>The learner license has offence </div>");
							$('#msgDiv').show();
					        setTimeout('hideStatus("msgDiv")',3000);
					        $('#learnerLicenseNo').val('');
						}
						else if(status == "LEARNER_SUSPENDED")
						{
							$("#msgDiv").html("<div class='alert alert-danger'>The learner license is suspended </div>");
							$('#msgDiv').show();
					        setTimeout('hideStatus("msgDiv")',3000);
					        $('#learnerLicenseNo').val('');
						}
						else if(status == "LEARNER_CANCELLED")
						{
							$("#msgDiv").html("<div class='alert alert-danger'>The learner license has been cancelled </div>");
							$('#msgDiv').show();
					        setTimeout('hideStatus("msgDiv")',3000);
					        $('#learnerLicenseNo').val('');
						}
					}
					if(status == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancellationReason+"</div>");
						$('#Msg').show();
						if(searchType!="LEARNER_HISTORY")
						{
							$("#msgDiv").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancellationReason+"</div>");
							$('#msgDiv').show();
						}
						
					}
				});
			}
		});
	}
	function get_PIS(personalInfoId)
	{	
		$("#originalCID").val('');
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtlsForEdit&personalInfoId='+personalInfoId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
				 	var customerId = $(this).find('customerId').text();
				 	var cid = $(this).find('cid').text();
				 	var titleCourtesy = $(this).find('title-courtesy').text();
				 	var firstName = $(this).find('firstname').text();
				 	var middleName = $(this).find('middlename').text();
				 	var lastName = $(this).find('lastname').text();
				 	var occupation = $(this).find('occupation').text();
				 	var nationality = $(this).find('nationality').text();
				 	var dob = $(this).find('dob').text();
				 	var fatherName = $(this).find('father-name').text();
				 	var bloodGroup = $(this).find('blood-group').text();
				 	var gender = $(this).find('gender').text();
				 	var remarks = $(this).find('remarks').text();
				 	var isInternational = $(this).find('is-international').text();
				 	var dzongkhag = $(this).find('dzongkhag').text();
				 	var gewog = $(this).find('gewog').text();
				 	var village = $(this).find('village').text();
				 	var country = $(this).find('country').text();
				 	var address = $(this).find('address').text();
				 	var presentDzongkhag = $(this).find('present-dzongkhag').text();
				 	var contactAddress = $(this).find('present-address').text();
				 	var phone = $(this).find('phone').text();
				 	var email = $(this).find('email').text();
				 	var personalInfoId = $(this).find('personal-info-id').text();
				 	var imagePath = $(this).find('image-path').text();
				 	var contactRadio = $(this).find('contactRadio').text();
				 	populateDependentDropDown(dzongkhag, 'gewog', '', 'GEWOG_LIST', 'N');

				 	$('#customerID').val(customerId);
				 	$('#cid').val(cid);
				 	$('#titleOfcourtesy').val(titleCourtesy);
				 	$('#firstname').val(firstName);
				 	$('#middlename').val(middleName);
				 	$('#lastName').val(lastName);
				 	$('#personalOccupation').val(occupation);
				 	$('#personalNationality').val(nationality);
				 	$('#displayDob').val(dob);
				 	$('#fathersName').val(fatherName);
				 	$('#bloodGroup').val(bloodGroup);

				 	if(gender == "M")
					 	$('#maleRadio').attr('checked', true);
				 	else
				 		$('#femaleRadio').attr('checked', true);

			 		$('#remarks').val(remarks);

			 		
			 		if(isInternational == "N")
		 			{
			 			$('#nationalRadio').attr('checked', true);
			 			enable('National');
			 		}
			 		else
			 		{
			 			$('#internationalRadio').attr('checked', true);
			 			enable('International');
			 		}
			 		if(contactRadio == "Official")
			 		{
				 		$('#officialRadio').attr('checked', true);
				 		$('#ministry').val(ministry);
			 			$('#department').val(department);
			 			enable('Official');
			 		}
			 		else
			 		{
			 			$('#privateRadio').attr('checked', true);
			 			enable('Private');
			 		}
			 		/* 
			 		if(isInternational == "N")
				 		$('#nationalRadio').attr('checked', true);
			 		else
			 			$('#internationalRadio').attr('checked', true); */

		 			$('#dzongkhag').val(dzongkhag);
		 			$('#gewog').val(gewog);
		 			$('#village').val(village);
		 			$('#country').val(country);

		 			if(address == "" || address == "null")
		 				$('#address').val("");
		 			else
		 				$('#address').val(address);
	 				
		 			$('#presentDzongkhag').val(presentDzongkhag);
		 			$('#contactAddress').val(contactAddress);
		 			$('#phone').val(phone);
		 			$('#email').val(email);
		 			$('#personalInfoId').val(personalInfoId);
		 			$('#msgDiv').html('');

		 			if(imagePath != "null")
			 		{
		 				$('#pic').html('<span class="profile-picture"><img class="editable img-responsive" id="" style="width:150px; height: 150px;" src="http://202.144.155.195:8080/eRaLISImageService/ImageServlet?url='+imagePath+'"/></span>');
						$('#pic').show();
			 		}
		 			else
			 		{
		 				$('#pic').html('');
						$('#pic').hide();
				 	}

		 			$('#firstname').attr('readonly', false);
		 			$('#middlename').attr('readonly', false);
		 			$('#lastName').attr('readonly', false);
		 			$('#fathersName').attr('readonly', false);
		 			$('#village').attr('readonly', false);
		 			$('#country').attr('disabled', false);
		 			$('#address').attr('disabled', false);
				});
			}
		});
	}
	function getSuspensionList()
	{
		var learnerLicenseId = $('#learnerLicenseId').val();
		if(learnerLicenseId == "")
			learnerLicenseId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getsuspensionlist&FORM_TYPE=LL&drivinglicenseId="+learnerLicenseId,
			async: false,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#suspensionlistTable").html(responseText);
				$("#suspensionlistTable").show();

			}
		});
	}
	function getCancellationList()
	{
		
	}
</script>