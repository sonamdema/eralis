	<div class="footer">
		<div class="footer-inner">
			<div class="footer-content">
				<span class="bigger-120">
					<span class="blue"><strong>eRaLIS</strong></span>,
					Road Safety and Transport Authority &copy; 2018
				</span>
			</div>
		</div>
	</div>
	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>
	
	<!-- basic scripts -->
	
	<!--[if !IE]> -->
	<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/bootstrap-datepicker.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/bootstrap-timepicker.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.bootstrap-duallistbox.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.raty.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/bootstrap-multiselect.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/select2.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/typeahead.jquery.min.js"></script>
	<!-- <![endif]-->

	<!--[if IE]>
	<script src="<%=request.getContextPath() %>/js/jquery.1.11.1.min.js"></script>
	<![endif]-->

	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery.min.js'>"+"<"+"/script>");
	</script>
	<!-- <![endif]-->

	<!--[if IE]>
	<script type="text/javascript">
	 window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery1x.min.js'>"+"<"+"/script>");
	</script>
	<![endif]-->
	
	<script type="text/javascript">
		if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath() %>/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>
	
	<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>
	<script src="<%=request.getContextPath() %>/js/jqueryAjaxFormSubmit.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.form.js"></script>
	
	<!-- page specific plugin scripts -->
	<script src="<%=request.getContextPath() %>/js/bootbox.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.easypiechart.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.flot.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.flot.pie.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.flot.resize.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.blockUI.js"></script>
	
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"></script>
  	<script type="text/javascript" src="<%=request.getContextPath()%>/js/dataTables.bootstrap.min.js"></script>
	
	<!-- ace scripts -->
	<script src="<%=request.getContextPath() %>/js/ace-elements.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/ace.min.js"></script>
	
	<!-- file exporter -->
	<script src="<%=request.getContextPath() %>/js/FileSaver.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.wordexport.js"></script>
	<script src="<%=request.getContextPath() %>/js/tableexport/tableExport.js"></script>
	<script src="<%=request.getContextPath() %>/js/tableexport/jquery.base64.js"></script>
	
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/tableexport/sprintf.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/tableexport/jspdf.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/tableexport/base64.js"></script>   
	
	<!--<script type="text/javascript" src="<%=request.getContextPath() %>/js/export_table/table_export_jquery.js"></script>-->
  	<script type="text/javascript" src="<%=request.getContextPath() %>/js/export_table/xlsx.core.min.js"></script>
  	<script type="text/javascript" src="<%=request.getContextPath() %>/js/export_table/FileSaver.min.js"></script>
  
  	<script type="text/javascript" src="<%=request.getContextPath() %>/js/export_table/jspdf.min.js"></script>
  	<script type="text/javascript" src="<%=request.getContextPath() %>/js/export_table/jspdf.plugin.autotable.js"></script>
  	<script type="text/javascript" src="<%=request.getContextPath() %>/js/export_table/html2canvas.min.js"></script> 
	
	<script type="text/javascript" src = "<%=request.getContextPath() %>/js/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.fileupload.js"></script>
	
	<!-- Graphical Report -->
	<script src="<%=request.getContextPath() %>/js/graphical/highcharts.js"></script>
	<script src="<%=request.getContextPath() %>/js/graphical/jquery.highchartTable-min.js"></script>
	<script src="<%=request.getContextPath() %>/js/graphical/highcharts-3d.js"></script>
	<script src="<%=request.getContextPath() %>/js/graphical/data.js"></script>
	<script src="<%=request.getContextPath() %>/js/graphical/exporting.js"></script>
	
	<script src="<%=request.getContextPath() %>/js/jquery.idletimer.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/js/jquery.idletimeout.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/js/html2canvas.js" type="text/javascript"></script>
	
	<!-- inline scripts related to this page -->
	<script>
		jQuery(function($) {
			$('.easy-pie-chart.percentage').each(function(){
				var $box = $(this).closest('.infobox');
				var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
				var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
				var size = parseInt($(this).data('size')) || 50;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
			});


			 $("#bootbox-confirm").on(ace.click_event, function() {
					bootbox.confirm("Are you sure?", function(result) {
						if(result) {
							//
						}
					});
				});
				
		});

	</script>
	
	<script>

	function Delete_Cookie(name)
	{
	   	var cookies = document.cookie.split(";");
	   	for (var i = 0; i < cookies.length; i++)
	   	{
	   	 	x=cookies[i].substr(0,cookies[i].indexOf("="));
	   		x=x.replace(/^\s+|\s+$/g,"");
	   			 
	   	 	if(x==name)
	   	 	{
	   	 		document.cookie = name + "=" +";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	   	 	}
		}
	}
	
	function getCookie(c_name)
	{
	   	var i,x,ARRcookies=document.cookie.split(";");
	   	for (i=0;i<ARRcookies.length;i++)
	   	{
	   	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	   	  x=x.replace(/^\s+|\s+$/g,"");
	   	  if (x==c_name)
	   	  {
	   	    return true;
	   	  }
	  	}
	  return false;
	}
	
	function setCookie()
	{
	  if(getCookie("clicked")==false) 
	  {
	  	document.cookie='clicked=1';
	  }   
	}
		 
	$(".Button").click(function()
	 {
	  	setCookie();  
	 });
	 
	$("[href]").click(function()
	 {
	    setCookie();
	 });
	  
	 window.onload= function()
	 {		
	     if(getCookie("clicked")==false && window.location.href !="<%=request.getContextPath()%>")
	     {    	
				window.history.forward();
		 }
	   Delete_Cookie("clicked");
	 };
	</script>