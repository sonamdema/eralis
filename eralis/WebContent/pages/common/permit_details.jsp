<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Permit No</th>
			<th>Driver Name</th>
			
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="PERMIT_LIST">
			<logic:iterate id="permit" name="PERMIT_LIST">
				<tr>
					<td>
						<input type="radio" name="permitListRadio" id='<bean:write name="permit" property="permitDetailsId"/>' onclick="addSelected(this.id)"/>
					</td>
					<td><bean:write name="permit" property="permitID"/></td>
					<td><bean:write name="permit" property="driverName"/></td>
					
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="PERMIT_LIST">
			<tr>
				<td colspan="3" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>
<div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>

<script>
	var globalId = "";
	function addSelected(id)
	{
		globalId = id;
	}

	function getSelected()
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPermitInfoList&permitDetailsId='+globalId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					
					var driverName = $(this).find('driverName').text();
					var identificationMarks=$(this).find('identificationMarks').text();
					var address = $(this).find('address').text();
					var phone=$(this).find('phone').text();
					var permitID = $(this).find('permitID').text();
					var registrationNo=$(this).find('registrationNo').text();
					var region = $(this).find('region').text();
					var baseoffice=$(this).find('baseoffice').text();
					var routeBetween = $(this).find('routeBetween').text();
					var to=$(this).find('to').text();
					var vehicleType=$(this).find('vehicleType').text();
					var carryingCapacity = $(this).find('carryingCapacity').text();
					var seatCapacity=$(this).find('seatCapacity').text();
					var receiptNo = $(this).find('receiptNo').text();
					var dateOfissue=$(this).find('dateOfissue').text();
					var validity = $(this).find('validity').text();
					var remarks=$(this).find('remarks').text();

					
					$('#driverName').val(driverName);
					$('#identificationMarks').val(identificationMarks);
					$('#address').val(address);
					$('#phone').val(phone);
					$('#permitID').val(permitID);
					$('#permitIDNo').val(permitID);
					$('#registrationNo').val(registrationNo);
					$('#region').val(region);
					$('#baseoffice').val(baseoffice);
					$('#routeBetween').val(routeBetween);
					$('#to').val(to);
					$('#vehicleType').val(vehicleType);
					$('#carryingCapacity').val(carryingCapacity);
					$('#seatCapacity').val(seatCapacity);
					$('#receiptNo').val(receiptNo);
					$('#dateOfissue').val(dateOfissue);
					$('#validity').val(validity);
					$('#remarks').val(remarks);
					$('#displayregion').html("<label class='control-label'>"+region+"</label>");
					
				});
			}
		});
	}

</script>