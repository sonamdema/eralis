<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<div id="accordion" class="accordion-style1 panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#groupTask">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							<span data-rel="tooltip" data-placement="top" title="Group Tasks - This panel lists the applications that are currently in the common task pool">
									Group Task (Click Here To Expand)
							</span>
							&nbsp;<span id="spnGroupCount" class="badge badge-warning"></span>
						</a>
					</div>
					<div class="panel-collapse collapse" id="groupTask">
						<div class="panel-body">
							<div id="spnGroupTaskMessage">&nbsp;</div>
							<div class="table-responsive">
								<table id="group-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Application Number</th>
											<th>Application Type</th>
											<th>Current Status</th>
											<th>LL/DL/VL No</th>
											<th>CID</th>
											<th>Customer Name</th>
											<th>User Name</th>
											<th>Last Action Date</th>
										</tr>
									</thead>
									<tbody>
										<logic:present name="groupTaskList" scope="session">
											<logic:iterate id="taskDTO" name="groupTaskList" indexId="index">
												<tr id='grpTaskTR_<bean:write name="taskDTO" property="appNo"/>'>
													<td>
														<a href="javascript:void(0)" onclick="claimApplication('<bean:write name="taskDTO" property="appNo"/>')"><bean:write name="taskDTO" property="appNo"/></a>
													</td>
													<td>
														<bean:write name="taskDTO" property="serviceName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="statusName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="serviceTypeNo"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="customerId"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="customerName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="actorName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="lastUpdatedDate"/>
														<input type="hidden" id='myServiceCode_<bean:write name="taskDTO" property="appNo"/>' value='<bean:write name="taskDTO" property="serviceCode"/>'/>
													</td>
												</tr>
											</logic:iterate>
										</logic:present>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#myTask">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							&nbsp;
							<span data-rel="tooltip" data-placement="bottom" title="My Tasks - This panel lists the applications that have been assigned in your name">
									My Task (Click Here To Expand)
							</span>
							&nbsp;<span id="spnMyCount" class="badge badge-warning"></span>
						</a>
					</div>
					<div class="panel-collapse collapse in" id="myTask">
						<div class="panel-body">
							<div class="col-lg-12">
								<div class="col-lg-2 pull-left">
									<input type="checkbox" id="checkAll"> <b>Select All Application</b>
								</div>
								<div class="col-lg-10">
									<button type="button" class="btn btn-primary btn-sm" onclick="dispatch()">Dispatch</button>
								</div>
						  	</div>
							<div id="spnMyTaskMessage" class="col-lg-12">&nbsp;</div>
							<div class="table-responsive col-lg-12">
								
								
							  	
								<table id="my-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<!--<th></th>-->
											<th>Application Number</th>
											<th>Application Type</th>
											<th>Current Status</th>
											<th>LL/DL/VL No</th>
											<th>CID</th>
											<th>Customer Name</th>
											<th>User Name</th>
											<th>Last Action Date</th>
										</tr>
									</thead>
									<tbody>
										<%
											int a = 0;
										%>
										<logic:present name="ownTaskList" scope="session">
											<logic:iterate id="taskDTO" name="ownTaskList" indexId="index">
												
											<%
												a = index.intValue();
											%>
												<tr id='myTaskTR_<bean:write name="taskDTO" property="appNo"/>'>
													<!--<td>
														<input type="checkbox" class="ace" value='<bean:write name="taskDTO" property="appNo"/>' id='<%="app_"+a %>'/>
														<span class="lbl"></span>
													</td>-->
													<td>
														<logic:equal name="taskDTO" property="statusName" value="APPROVED">
														    <input type="checkbox" value='<bean:write name="taskDTO" property="appNo"/>' id='<%="app_"+a %>'/>
														</logic:equal>
														<img src="<%=request.getContextPath()%>/images/delete.jpg" onclick="unclaimApplication('<bean:write name="taskDTO" property="appNo"/>')" style="cursor:pointer"/>
   															<a href="javascript:void(0)" onclick="openApplication('<bean:write name="taskDTO" property="appNo"/>')" ><bean:write name="taskDTO" property="appNo"/></a>
													</td>
													<td>
														<bean:write name="taskDTO" property="serviceName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="statusName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="serviceTypeNo"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="customerId"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="customerName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="actorName"/>
													</td>
													<td>
														<bean:write name="taskDTO" property="lastUpdatedDate"/>
														<input type="hidden" id='myServiceCode_<bean:write name="taskDTO" property="appNo"/>' value='<bean:write name="taskDTO" property="serviceCode"/>'/>
														<input type="hidden" id='<%="dispatchApplicationNo_"+a %>' value='<bean:write name="taskDTO" property="appNo"/>'/>
													</td>
												</tr>
											</logic:iterate>
										</logic:present>
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>	
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<script>

	$(document).ready(function() 
	{
	    $('#group-table').DataTable({
	            responsive: true
	    });

	    $('#my-table').DataTable({
            responsive: true
    	});

	    $('[data-rel=tooltip]').tooltip();

	    setCount();
	});

	function claimApplication(applicationNumber)
	{
		//the tr is identified with the id - the 3rd child of the tr is the status code
		var statusCode = $("#grpTaskTR_"+applicationNumber+" td:nth-child(3)").text();
		
		$.ajax({
              async: false,
              type: 'GET',
              url: '<%= request.getContextPath()%>/tasklist.html?q=claimApplication&applicationNumber='+applicationNumber+'&statusCode='+statusCode,
              success: function(xml)
              {
                  $(xml).find('status').each(function()
		          { 
                      var statusCode = $(this).find('status-code').text();
                      var statusName = $(this).find('status-name').text();
                      
                     if(statusCode =='OK')
	                 {
		                 var msg = "<div class='alert alert-success'>"+statusName+"</div>";
                    	 $('#spnMyTaskMessage').html(msg);
                    	 
                    	  //insert this row in the my task panel
                    	 var cloneNode = $("#grpTaskTR_"+applicationNumber).clone();
                    	 $(cloneNode).attr('id','myTaskTR_'+applicationNumber);
                    	 $(cloneNode).find('td').each(function(i)
		                 {
                    		if(i==0)
                    		{
	                    		 $(this).find('a').each(function(j)
			                     {
	                    			$(this).removeAttr('title');
	                    			$(this).attr('title',applicationNumber +' - Unclaim');
	                    			$(this).removeAttr('onclick');
	                    			
	                    			$(this).click(function(){
	                    				openApplication(applicationNumber);
	                    			});
	                    		 });
                		 	}
                    	 });
               
                    	 var imageNode=$("<img></img>");
					     $(imageNode).attr('src','<%=request.getContextPath()%>/images/delete.jpg');
						 $(imageNode).click(function()
						 {
							 unclaimApplication(applicationNumber);
             			 });
             			 
						 $(cloneNode).find('td:first').prepend(imageNode);
						 $('#my-table > tbody:last').append(cloneNode);
                    	 $("#grpTaskTR_"+applicationNumber).remove();
                     }
                     else
	                 {
                    	 var msg = "<div class='alert alert-danger'>"+statusName+"</div>";
                    	 $('#spnGroupTaskMessage').html(msg);
                    	 $("#grpTaskTR_"+applicationNumber).remove();
                     }
                  });
              },
              error: function(data, textStatus, errorThrown) {
              }
          });		  

		setCount();
	}

	function unclaimApplication(applicationNumber)
	{
		 
		//the tr is identified with the id - the 3th child of the tr is the status code
		  var statusCode = $("#myTaskTR_"+applicationNumber+" td:nth-child(3)").text();
          $.ajax({
              async: false,
              type: 'GET',
              url: '<%= request.getContextPath()%>/tasklist.html?q=releaseApplication&applicationNumber='+applicationNumber+'&statusCode='+statusCode,
              success: function(xml){
                  $(xml).find('status').each(function(){ 
                      var statusCode = $(this).find('status-code').text();
                      var statusName = $(this).find('status-name').text();
                     if(statusCode =='OK'){
                    	 var msg = "<div class='alert alert-success'>"+statusName+"</div>";
                    	 $('#spnGroupTaskMessage').html(msg);   
                    	  //insert this row in the group task panel
                    	 var cloneNode = $("#myTaskTR_"+applicationNumber).clone();
                    	 $(cloneNode).attr('id','grpTaskTR_'+applicationNumber);
                    	 $(cloneNode).find('td').each(function(i){
                    		 if(i==0){
                    		 $(this).find('a').each(function(j){
                    			$(this).removeAttr('title');
                    			$(this).removeAttr('onclick');
                    			
                    			$(this).click(function(){
                    				claimApplication(applicationNumber);
                    			});
                    		 });
                    		 $(this).find('img').remove();

                    		 }
                    	 });
               
                    	 $('#group-table > tbody:last').append(cloneNode);
                    	 $("#myTaskTR_"+applicationNumber).remove();
                     }
                     else{
                    	 var msg = "<div class='alert alert-danger'>"+statusName+"</div>";
                    	 $('#spnGroupTaskMessage').html(msg);    
                    	 $("#myTaskTR_"+applicationNumber).remove();
                     }
                  });
                  
              },
              error: function(data, textStatus, errorThrown) {
              }
          });		  

	    setCount();
	}

	function openApplication(applicationNo)
	{
		var serviceCode = $('#myServiceCode_'+applicationNo).val();
		$.blockUI
	    ({ 
	    	css: 
	    	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	    	} 
	    });  

		$.ajax
		({
			type : "POST",
			url : '<%=request.getContextPath()%>/TaskListRedirectionServlet?applicationNo='+applicationNo+'&serviceCode='+serviceCode,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#contentDisplayDiv").html(responseText);
				setTimeout($.unblockUI, 1000); 
			}
		});
	}

	function claimApplication(applicationNumber)
	{
		//the tr is identified with the id - the 3rd child of the tr is the status code
		var statusCode = $("#grpTaskTR_"+applicationNumber+" td:nth-child(3)").text();
		
		$.ajax({
              async: false,
              type: 'GET',
              url: '<%= request.getContextPath()%>/tasklist.html?q=claimApplication&applicationNumber='+applicationNumber+'&statusCode='+statusCode,
              success: function(xml)
              {
                  $(xml).find('status').each(function()
		          { 
                      var statusCode = $(this).find('status-code').text();
                      var statusName = $(this).find('status-name').text();
                      
                     if(statusCode =='OK')
	                 {
		                 var msg = "<div class='alert alert-success'>"+statusName+"</div>";
                    	 $('#spnMyTaskMessage').html(msg);
                    	 
                    	  //insert this row in the my task panel
                    	 var cloneNode = $("#grpTaskTR_"+applicationNumber).clone();
                    	 $(cloneNode).attr('id','myTaskTR_'+applicationNumber);
                    	 $(cloneNode).find('td').each(function(i)
		                 {
                    		if(i==0)
                    		{
	                    		 $(this).find('a').each(function(j)
			                     {
	                    			$(this).removeAttr('title');
	                    			$(this).attr('title',applicationNumber +' - Unclaim');
	                    			$(this).removeAttr('onclick');
	                    			
	                    			$(this).click(function(){
	                    				openApplication(applicationNumber);
	                    			});
	                    		 });
                   		 	}
                    	 });
               
                    	 var imageNode=$("<img></img>");
					     $(imageNode).attr('src','<%=request.getContextPath()%>/images/delete.jpg');
						 $(imageNode).click(function()
						 {
							 unclaimApplication(applicationNumber);
             			 });
             			 
						 $(cloneNode).find('td:first').prepend(imageNode);
						 $('#my-table > tbody:last').append(cloneNode);
                    	 $("#grpTaskTR_"+applicationNumber).remove();
                     }
                     else
	                 {
                    	 var msg = "<div class='alert alert-danger'>"+statusName+"</div>";
                    	 $('#spnGroupTaskMessage').html(msg);
                    	 $("#grpTaskTR_"+applicationNumber).remove();
                     }
                  });
              },
              error: function(data, textStatus, errorThrown) {
              }
          });		  

		setCount();
	}

	function unclaimApplication(applicationNumber)
	{
		 
		//the tr is identified with the id - the 3th child of the tr is the status code
		  var statusCode = $("#myTaskTR_"+applicationNumber+" td:nth-child(3)").text();
          $.ajax({
              async: false,
              type: 'GET',
              url: '<%= request.getContextPath()%>/tasklist.html?q=releaseApplication&applicationNumber='+applicationNumber+'&statusCode='+statusCode,
              success: function(xml){
                  $(xml).find('status').each(function(){ 
                      var statusCode = $(this).find('status-code').text();
                      var statusName = $(this).find('status-name').text();
                     if(statusCode =='OK'){
                    	 var msg = "<div class='alert alert-success'>"+statusName+"</div>";
                    	 $('#spnGroupTaskMessage').html(msg);   
                    	  //insert this row in the group task panel
                    	 var cloneNode = $("#myTaskTR_"+applicationNumber).clone();
                    	 $(cloneNode).attr('id','grpTaskTR_'+applicationNumber);
                    	 $(cloneNode).find('td').each(function(i){
                    		 if(i==0){
                    		 $(this).find('a').each(function(j){
                    			$(this).removeAttr('title');
                    			$(this).removeAttr('onclick');
                    			
                    			$(this).click(function(){
                    				claimApplication(applicationNumber);
                    			});
                    		 });
                    		 $(this).find('img').remove();

                    		 }
                    	 });
               
                    	 $('#group-table > tbody:last').append(cloneNode);
                    	 $("#myTaskTR_"+applicationNumber).remove();
                     }
                     else{
                    	 var msg = "<div class='alert alert-danger'>"+statusName+"</div>";
                    	 $('#spnGroupTaskMessage').html(msg);    
                    	 $("#myTaskTR_"+applicationNumber).remove();
                     }
                  });
                  
              },
              error: function(data, textStatus, errorThrown) {
              }
          });		  

	    setCount();
	}

	function openApplication(applicationNo)
	{ 
		var serviceCode = $('#myServiceCode_'+applicationNo).val();
		$.blockUI
	    ({ 
	    	css: 
	    	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	    	} 
	    });  
	
		$.ajax
		({
			type : "POST",
			url : '<%=request.getContextPath()%>/TaskListRedirectionServlet?applicationNo='+applicationNo+'&serviceCode='+serviceCode,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#contentDisplayDiv").html(responseText);
				setTimeout($.unblockUI, 1000); 
			}
		});
	}


	function setCount()
	{
		var groupCount = 0;
		var myCount = 0;
	
		groupCount = $('#group-table >tbody >tr').length;
		myCount = $('#my-table >tbody >tr').length;
		$('#spnGroupCount').html(groupCount);
		$('#spnMyCount').html(myCount);
	}

	function dispatch()
	{
		var totalMyTask = "<%=a%>";
		var checkedApplication = "0";
		for(var i=0;i<=totalMyTask;i++)
		{
			if($("#app_"+i).prop("checked") == true){
				checkedApplication = checkedApplication+"~"+$("#dispatchApplicationNo_"+i).val();
			}
		}
		if(checkedApplication!="0")
		{
			var url = '<%=request.getContextPath()%>/common.html?method=dispatch&applicationNo='+checkedApplication+'&requestType=ALL&serviceType=NA';
			$.blockUI
	        ({ 
	        	css: 
	        	{ 
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px', 
		            '-moz-border-radius': '10px', 
		            opacity: .5, 
		            color: '#fff' 
	        	} 
	        });
			$.ajax
			({
				type : "POST",
				url : url,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#contentDisplayDiv").html(responseText);
					setTimeout($.unblockUI, 1000); 
					setTimeout(showTaskList(), 3000); 
				}
			});
		}
	}
	$('#checkAll').click(function () {    
	     $('input:checkbox').prop('checked', this.checked);    
	 });
</script>