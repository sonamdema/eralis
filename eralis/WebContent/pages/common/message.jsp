<%
	String msg = (String) request.getAttribute("MESSAGE");
	if("FIRST_UPDATE_SUCCESS".equalsIgnoreCase(msg))
	{
	%>
	<div class="alert alert-success">
		Password successfully updated, you will be automatically redirected to your respective dashboard
	</div>
	<%
	}
	else if("FIRST_UPDATE_FAILURE".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-danger">
		Password could not be updated, please try again later your will be redirected to the dashboard
	</div>
	<%
	}
	else if("NO_MATCH".equalsIgnoreCase(msg))
	{
 %>
 	<div class="alert alert-danger">
		Current password you entered is incorrect, please try again
	</div>
 <%
	}
	else if("SUCCESS".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-success">
		Action successfully performed...Please wait, Page will be reloaded
		</div>
		<script>
			setTimeout('reloadPage()',4000);		
		</script>
	<%
	}
	else if("FAILURE".equalsIgnoreCase(msg))
	{
 %>
 	<div class="alert alert-danger">
		Action couldnot be performed, try again later
	</div>
 <%
	}
	else if("NO_CHANGES".equalsIgnoreCase(msg))
	{
 %>
 	<div class="alert alert-danger">
		No Changes Made
	</div>
 <%
	}
%>