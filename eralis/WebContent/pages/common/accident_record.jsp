<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
<table id="vehicle-list-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Sl</th>
			<th>Police Station</th>
			<th>Date of Occurrence</th>
			<th>Place</th> 
			<th>Gewog</th> 
			<th>Dzongkhag</th> 
			<th>Driver Name</th> 
			<th>CID</th> 
			<th>DL/LL</th> 
			<th>Accident Type</th> 
			<th>Nature of Accident</th> 
			<th>Cause of Accident</th> 
			<th>Total Injure</th> 
			<th>Total Killed</th> 
			<th>Remarks</th> 
		</tr>
	</thead>
	<tbody>
		<% int i=1; %>
		<logic:notEmpty name="ACCIDENT_RECORD">
			<logic:iterate id="accidentRecord" name="ACCIDENT_RECORD">
				<tr>
					<td>
						<%=i++ %>
					</td>
					<td><bean:write name="accidentRecord" property="policeStation"/></td>
					<td><bean:write name="accidentRecord" property="dateOfOccurrence"/></td>
					<td><bean:write name="accidentRecord" property="placeOfOccurrence"/></td>
					<td><bean:write name="accidentRecord" property="gewog"/></td>
					<td><bean:write name="accidentRecord" property="dzongkhag"/></td> 
					<td><bean:write name="accidentRecord" property="driverName"/></td> 
					<td><bean:write name="accidentRecord" property="CID"/></td> 
					<td><bean:write name="accidentRecord" property="LLNo"/></td> 
					<td><bean:write name="accidentRecord" property="accidentType"/></td> 
					<td><bean:write name="accidentRecord" property="accidentNature"/></td> 
					<td><bean:write name="accidentRecord" property="accidentCause"/></td> 
					<td><bean:write name="accidentRecord" property="injuredDtls"/></td> 
					<td><bean:write name="accidentRecord" property="killedDtls"/></td> 
					<td><bean:write name="accidentRecord" property="remarks"/></td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
</div>
<script>
	 $(document).ready(function() 
	{
	    $('#vehicle-list-table').DataTable({
	            responsive: true
	    });
	});
</script>