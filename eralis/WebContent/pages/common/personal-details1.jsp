<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
	<table id="personal-data-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<th>Customer Code</th>
				<th>CID</th>
				<th>Name</th>
				<th>Blood Group</th>
				<th>Region</th>
				<th>Permanent Dzongkhag</th>
				<th>Permanent Gewog</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="PERSONAL_LIST1">
				<logic:iterate id="personal1" name="PERSONAL_LIST1">
					<tr>
						<td>
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="personal1" property="personalInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="personal1" property="customerID"/></td>
						<td><bean:write name="personal1" property="CID"/></td>
						<td><bean:write name="personal1" property="name"/></td>
						<td><bean:write name="personal1" property="bloodGroup"/></td>
						<td><bean:write name="personal1" property="region"/></td>
						<td><bean:write name="personal1" property="dzongkhag"/></td>
						<td><bean:write name="personal1" property="gewog"/></td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function() 
	{
   		$('#personal-data-table').DataTable({
            responsive: true
    	});
	});
		
	function getSelected(globalId)
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtls&personalInfoId='+globalId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{	 
					var name = $(this).find('name').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					
					var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var customerId = $(this).find('customerId').text();
					var personalInfoId = $(this).find('personalInfoId').text(); 

					//vehicle registration

					$('#TransferOwnerName').html("<label class='control-label'>"+name+"</label>");
					$('#TransferDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
					$('#TransferGewog').html("<label class='control-label'>"+gewog+"</label>");
					$('#TransferAddress').html("<label class='control-label'>"+address+"</label>");
					$('#transfereeCustomerId1').val(customerId);
					$('#transfereeCustomerId').val(customerId);
					$('#citizenID').val(customerId);
					$('#vehicleRegistrationType').val("P");
					$("#personalInfoId").val(personalInfoId);

				});
			}
		});
	}
</script>