<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
	 <div class="widget-header widget-header-small">
		  <h5 class="widget-title lighter"> Non-Commercial License Application Details</h5>
		  <html:hidden property="learnerLicenseId" styleClass="form-control" value="<%=dto.getLearnerLicenseId() %>" styleId="learnerLicenseId"></html:hidden>
	</div>						
	<div class="widget-body">
		<div class="widget-main">
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Learner License No:</label>
				</div>
				<div class="col-lg-4" ><%=dto.getLearnerlicenseno()%></div>
			</div>
			  
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Region:</label>
				</div>
				<div class="col-lg-4"><%=dto.getRegion()%></div>
				<div class="col-lg-2">
					<label class="control-label">Receipt Number:</label>
				</div>
				<div class="col-lg-4"><%=dto.getReceiptNo()%></div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Receipt Date:</label>
				</div>
				<div class="col-lg-4"><%=dto.getReceiptDate()%></div>
				<div class="col-lg-2">
					<label class="control-label">Remarks:</label>
				</div>
				<div class="col-lg-4"><%=dto.getRemarks()%></div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Submission Date:</label>
				</div>
				<div class="col-lg-4"><%=dto.getAppsubmissiondate()%></div>
			</div> 
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Expiry Date</label>
				</div>
				<div class="col-lg-4"><%=dto.getExpiryDate()%></div>
			</div> 
			<logic:equal value="APPROVE" name="param">
				<div class="form-group">
					<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
				</div>
			</logic:equal>
   		</div>      
     </div>
</div>
<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driving License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Issue Type<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="issueType" styleId="issueType" styleClass="form-control" onchange="changeDriveType()">
		                           		<html:option value="">Select Issue Type</html:option>
		                           		<html:option value="AMC">AMC</html:option>
		                           		<html:option value="Arm_Force">Arm Force</html:option>
		                           		<html:option value="Driving_Institue">Driving Institue</html:option>
		                           		<html:option value="Learner_License">Learner License</html:option>
		                           		<html:option value="Foreign/International_Driving_License">Foreign/International Driving License</html:option>
		                           		<html:option value="Old_Learner_License">Old Learner License</html:option>
		                           		<html:option value="VTI">VTI</html:option>
	                           		</html:select>
									<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select validity issued for</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drive Type<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<!--<input type="text" disabled class="form-control" id="driveTypeName">-->
									
									 
									 <html:select property="drivetype" styleClass="form-control" styleId="driveTypeTemp">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="DRIVE_TYPE_LIST"
											label="headerName" value="headerId" />
									</html:select>
									
							        <label style="display:none;color: #ff0000" id="driveTypeValidation">Please select a drive type</label>
								</div>
								<div class="col-lg-2">
									<label>Validity Issued For<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
		                           		<html:option value="120">10 Years</html:option>
		                           		<html:option value="60">5 Years</html:option>
		                           		<html:option value="12">1 Year</html:option>
	                           		</html:select>
										<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select validity issued for</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Test Result<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<input type="text" disabled class="form-control" value="<%=dto.getStatus() %>" id="testResult">
								</div>
								<div id="msgDIV" style="display:none;" class="col-lg-7 alert alert-danger"></div>
								<div class="col-lg-2">
									<label>Overall Test Marks:</label>
								</div>
								<div class="col-lg-3">
									<input type="text" disabled class="form-control" value="<%=dto.getTestmarks() %>" id="testmarks">
								</div>
								<div id="msgDIV" style="display:none;" class="col-lg-7 alert alert-danger"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks" value="<%=dto.getRemarks() %>" styleClass="form-control"></html:textarea>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Attachments</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
					
					<div class="form-group">
						<div class="col-lg-2">
							<label>Application Form<span style="color: #ff0000">*</span>:</label>
						</div>
						<div class="col-lg-3">
							<html:file property="fileApplForm" styleId="fileApplForm" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
							<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
						</div>
						<div class="col-lg-3">
							<label>Refresher Course Certificate<span style="color: #ff0000">*</span>:</label>
						</div>
						<div class="col-lg-3">
							<html:file property="rcCertificate" styleId="rcCertificate" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation1',this)"></html:file>
							<label style="display:none;color: #ff0000" id="supportingDocumentValidation1"></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var	driveTypeId	=	'<%=dto.getDriveTypeId()%>';
	var	issueType	=	"<%=dto.getIssueType()%>";

	$("#driveTypeTemp").val(driveTypeId);
	$("#renewalDuration").val('<%=dto.getRenewalDuration()%>');
	$("#issueType").val(issueType);
	function changeDriveType()
   	{
   		$("#msgDIV").hide();
		$("#driveTypeTemp").val('');
		$('#submitBtn').attr('disabled',false);

   	 }
	function validateNonCommercialCriteria(driveTypeId)
	{
		$("#msgDIV").hide();
		var issueType	=	$("#issueType").val();
		var learnerLicenseId	=	$("#learnerLicenseId").val();
		if(learnerLicenseId=='')
		{
			learnerLicenseId	=	$("#duplicateCID").val();
		}
		if(issueType=='Old_Learner_License' || issueType=='Learner_License' || issueType=='Driving_Institue')
		{
			$.ajax
			({
				async: true,
				type: 'POST',
				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=validateNonCommercialCriteria&driveTypeId='+driveTypeId+'&learnerLicenseId='+learnerLicenseId+'&issueType='+issueType,
				success: function(xml)
				{ 
					$(xml).find('xml-response').each(function()
					{
						var testStatus = $(this).find('testStatus').text();
						var reason = $(this).find('testReason').text();
						if(testStatus=='0')
						{
							$("#msgDIV").show();
							$('#msgDIV').html("<label>"+reason+"</label>");
							$('#submitBtn').attr('disabled',true);
						}
						else if(testStatus=='1')
						{
							 $('#submitBtn').attr('disabled',false);
						}
					});
				}
			});
		}
	}
</script>