<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">License Renewal Application Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
			
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Driving License No:</label>
					</div>
					<div class="col-lg-4" ><%=dto.getLicenseNo()%></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Region:</label>
					</div>
					<div class="col-lg-4"><%=dto.getRegion()%></div>
					<div class="col-lg-2">
						<label class="control-label">Receipt Number:</label>
					</div>
					<div class="col-lg-4"><%=dto.getReceiptNo()%></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Receipt Date:</label>
					</div>
					<div class="col-lg-4"><%=dto.getReceiptDate()%></div>
					<div class="col-lg-2">
						<label class="control-label">Remarks:</label>
					</div>
					<div class="col-lg-4"><%=dto.getRemarks()%></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Submission Date:</label>
					</div>
					<div class="col-lg-4"><%=dto.getAppsubmissiondate()%></div>
				</div> 
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Expiry Date</label>
					</div>
					<div class="col-lg-4"><%=dto.getExpiryDate()%></div>
					
				</div> 
				<logic:equal value="APPROVE" name="param">
					<div class="form-group">
						<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
					</div>
				</logic:equal>
               </div>      
                 </div>
            </div>
            <div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Renewal Duration:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
	                           			<html:option value="">--SELECT--</html:option>
	                           		</html:select>
								</div>
							<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" value="<%=dto.getRemarks()%>" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
<script>
	var str	=	"<%=dto.getLicenseNo()%>";
	var licenseType = str.split("-",1);
	var	duration = null;
	$('#renewalDuration').empty();
	var totalDuration	=	'<%=dto.getDuration()%>';
	var dateDifference	=	'<%=dto.getRenewalDuration()%>';
	var age	=	25;
	
	if(licenseType == "T")
	{ 
		//if 70 n above
		if(age>=70)
		{
			$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
		}
		//if above 69
		else if(age>69)
		{
			$('#renewalDuration').append("<option value='"+totalDuration+"'>"+dateDifference+"</option>");
		}
		//if above 65
		else if(age>65)
		{
			$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
			$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
			$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
		}
		//if above 60
		else if(age>60)
		{
			$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
			$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
			$('#renewalDuration').append("<option value='48_month'>4 Years</option>");
			$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
			$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
			$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
			
		}
		else
		{
			$('#renewalDuration').append("<option value='120_month'>10 Years</option>");
			$('#renewalDuration').append("<option value='108_month'>9 Years</option>");
			$('#renewalDuration').append("<option value='96_month'>8 Years</option>");
			$('#renewalDuration').append("<option value='84_month'>7 Years</option>");
			$('#renewalDuration').append("<option value='72_month'>6 Years</option>");
			$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
			$('#renewalDuration').append("<option value='48_month'>4 Years</option>");
			$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
			$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
			$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
		}
	}
	else
	{
		if(age>55)
		{
			$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
		}
		else if(age>54)
		{
			$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
		}
		else if(age>52)
		{
			$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
			$('#renewalDuration').append("<option value='12_month'>1 Years</option>");
		}
		else
		{
			$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
			$('#renewalDuration').append("<option value='24_month'>2 Year</option>");
			$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
		}
		
	}
	$('#renewalDuration').val(totalDuration);
</script>