<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
ServiceDTO dto = (ServiceDTO)request.getAttribute("ServiceDTO");
String serviceName	=	(String)request.getAttribute("SERVICE_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	$("#serviceCode").val('<%=request.getAttribute("serviceCode")%>');
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		<%=request.getAttribute("SERVICE_TITLE") %> Resubmit
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="widget-body">
		<div class="widget-main">
			<div class="col-lg-12">
				<div class="form-group">
					<div id="msgDIV" class="col-lg-12 alert alert-danger">
						 <strong>Rejection Reason :</strong> <%=dto.getReason() %>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<html:form styleClass="form-horizontal" action="/common.html" styleId="resubmitForm">
<div class="row">
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Information</h5>
			<logic:equal value="PERSONAL" name="OWNER_TYPE">
				<button type="button" class="pull-right btn btn-purple btn-sm" onclick="openModal('edit_PIS')">
					Edit Personal Details
				</button>
			</logic:equal>
			<logic:equal value="ORGANISATION" name="OWNER_TYPE">
				<button type="button" class="pull-right btn btn-purple btn-sm" onclick="openModal('edit_ORGANISATION')">
					Edit Organisation Details
				</button>
			</logic:equal>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Name:</label>
							</div>
							<div class="col-lg-4" id="displayOwnerName"><%=dto.getName() %></div>
							<div class="col-lg-2">
								<label class="control-label">Blood Group:</label>
							</div>
							<div class="col-lg-4" id="displayBloodgroup"><%=dto.getBloodGroup()%></div>
						</div>
						<div class="form-group">
							
							<div class="col-lg-2">
								<label class="control-label">Date Of Birth:</label>
							</div>
							<div class="col-lg-4" id="displayDob"><%=dto.getDOB() %></div>
							
						</div>
						<h4>Permanent Address</h4>
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Dzongkhag:</label>
							</div>
							<div class="col-lg-4" id="displayDzongkhag"><%=dto.getDzongkhag() %></div>
							
							<div class="col-lg-2">
								<label class="control-label">Gewog:</label>
							</div>
							<div class="col-lg-4" id="displayGewog"><%=dto.getGewog() %></div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Village:</label>
							</div>
							<div class="col-lg-4" id="displayVillage"><%=dto.getVillage() %></div><!--
							
							<div class="col-lg-2">
								<label class="control-label">Country:</label>
							</div>
							<div class="col-lg-4" id="displayCountry"><%=dto.getCountry() %></div>
						--> 
							<div class="col-lg-2">
								<label class="control-label">Address:</label>
								
							</div>
							<div class="col-lg-4" id="displayAddress"><%=dto.getAddress() %></div>
							<html:hidden property="applicationNo" value="<%=dto.getApplicationNo() %>" styleId="applicationNo"/>
							<input type="hidden" id="serviceCode">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<logic:equal value="ILL" name="serviceCode">
		<jsp:include page="/pages/learner_license/include_new_learner_resubmit.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="REGV" name="serviceCode">
		<jsp:include page="/pages/common/include_veh_reg_resubmit.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="RV" name="serviceCode">
		<jsp:include page="/pages/common/include_veh_dtls.jsp"></jsp:include>
		<jsp:include page="/pages/common/include_veh_renew_resubmit.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="TV" name="serviceCode">
		<jsp:include page="/pages/common/include_veh_transfer_resubmit.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="CV" name="serviceCode">
		<jsp:include page="/pages/common/include_veh_dtls.jsp"></jsp:include>
		<jsp:include page="/pages/common/include_veh_conversion_resubmit.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="RLL" name="serviceCode">
		<jsp:include page="/pages/common/include_learner_renewal.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="INDL" name="serviceCode">
		<jsp:include page="/pages/common/include_issue_non_commercial.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="ICDL" name="serviceCode">
		<jsp:include page="/pages/common/include_issue_commercial.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="RDL" name="serviceCode">
		<jsp:include page="/pages/common/include_license_renewal.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="DDL" name="serviceCode">
		<jsp:include page="/pages/common/include_license_replacement.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="EDL" name="serviceCode">
		<jsp:include page="/pages/common/include_license_endorse.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="DLL" name="serviceCode">
		<jsp:include page="/pages/common/include_learner_replacement.jsp"></jsp:include>
	</logic:equal>
	<logic:equal value="TOP" name="serviceCode">
		<jsp:include page="/pages/common/include_top_resubmit.jsp"></jsp:include>
	</logic:equal>
	<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
	<div class=""  style="margin-bottom:50px"> 
		<div class="col-lg-12">
			<div class="form-group">
				<logic:notEqual value="TOP" name="serviceCode">
					<button type="button" class="btn btn-primary btn-sm" onClick="formUpdation()">ReSubmit</button>
				</logic:notEqual>
				
				<button type="button" class="btn btn-primary btn-sm" onclick="openModal('edit_payment_dtls')">
					Edit Payment Details
				</button>
				<button type="button" class="btn btn-primary btn-sm" onClick="deleteApplication()">Delete</button>
			</div>
		</div> 
	</div>
 	<div class="row">
		<div id="displayMsgDiv" class="col-lg-12"></div>
	</div>
</div>
</html:form>

<jsp:include page="/pages/common/edit_receipt_dtls.jsp"></jsp:include>
<logic:equal value="PERSONAL" name="OWNER_TYPE">
	<jsp:include page="/pages/common/edit_resubmit_personal_dtls.jsp"></jsp:include>
</logic:equal>
<logic:equal value="ORGANISATION" name="OWNER_TYPE">
	<jsp:include page="/pages/common/edit_organisation_detail.jsp"></jsp:include> 
</logic:equal>
<script>

$('.date-picker').datepicker({
	autoclose: true,
	todayHighlight: true
})

//show datepicker when clicking on the icon
.next().on(ace.click_event, function() {
	$(this).prev().focus();
});

	function formUpdation()
	{
		var serviceCode	= $('#serviceCode').val();
		if(serviceCode=='ILL')
		{
			getCheckedVals();
		}
		var options = {target:'#displayMsgDiv',url:context+'/common.html?method=formResubmit&serviceCode='+serviceCode,type:'POST',data: $("#resubmitForm").serialize()}; 
	    $("#resubmitForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show(); 
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',3000);
	}
	function deleteApplication()
	{
		var serviceCode	= $('#serviceCode').val();
		var options = {target:'#displayMsgDiv',url:context+'/common.html?method=deleteApplication&serviceCode='+serviceCode,type:'POST',data: $("#resubmitForm").serialize()}; 
	    $("#resubmitForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show(); 
	 	setTimeout('hideStatus("displayMsgDiv")',4000);
	 	setTimeout('showTaskList()',3000);
	}
	function editPaymentDtls()
	{
		var serviceType	=	'<%=serviceName%>';
		var applicationNo	=	$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=edit_receipt_dtls&applicationNo='+applicationNo+'&serviceType='+serviceType,type:'POST',data: $("#editReceiptDtlsForm").serialize()}; 
	    $("#editReceiptDtlsForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    $('#displayMsgDiv').get(0).scrollIntoView();
	}
	
	function calculateTotalEditPayment()
	{
		var amount = $("#amount").val();
		var penalty = $("#penalty").val();
		var totalAmount = parseFloat(amount) + parseFloat(penalty);
		$("#totalAmount").val(totalAmount);
		
	}
	calculateTotalEditPayment();
</script>