<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="organization-search-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Customer Id</th>
			<th>Name</th>
			<th>Region</th>
			<th>Dzongkhag</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="ORGANISATION_LIST">
			<logic:iterate id="organisation" name="ORGANISATION_LIST">
				<tr>
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="organisation" property="organizationInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
					</td>
					<td><bean:write name="organisation" property="customerID"/></td>
					<td><bean:write name="organisation" property="name"/></td>
					<td><bean:write name="organisation" property="region"/></td>
					<td><bean:write name="organisation" property="dzongkhag"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>
	$(document).ready(function() 
	{
   		$('#organization-search-table').DataTable({
            responsive: true
    	});
	});
		

	function getSelected(globalId)
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOrganisationDtls&organisationInfoId='+globalId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					$("#message").hide();
					var status=$(this).find('status').text();
					if(status == 'VEHICLE_OUTSTANDING')
					{
						var vehicle_type =  $(this).find('vehicle_type').text(); 
						var vehicleNo = $(this).find('vehicleNo').text();
						$("#message").html("This customer has <b>vehicle outstanding</b> against the vehicle no. : <b>"+vehicleNo+"</b> and vehicle type: <b>"+vehicle_type+"</b>");
						$("#message").show();
					}
					else
					{
						var name = $(this).find('name').text();
						var dzongkhag = $(this).find('dzongkhag').text();
						var gewog = $(this).find('gewog').text();
						var address = $(this).find('address').text();
						var region = $(this).find('region').text();
						var phone = $(this).find('phone').text();
						var email=$(this).find('email').text();
						var remarks=$(this).find('remarks').text();
						var customerId = $(this).find('customerId').text();
						var organisationInfoId = $(this).find('organisationInfoId').text();
	
						$('#organisationOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#organisationDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#organisationGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#organisationAddress').html("<label class='control-label'>"+address+"</label>");
						$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#ownerID').val(customerId);
						$('#vehicleRegistrationType').val("O");
						$('#customerID').val(customerId);
						$('#name').val(name);
						$('#dzongkhag').val(dzongkhag);
						$('#region').val(region);
						$('#address').val(address);
						$('#phone').val(phone);
						$('#email').val(email);
						$('#remarks').val(remarks);
						$("#organisationInfoId").val(organisationInfoId);
					}
				});
			}
		});
	}

</script>