<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<input type="hidden" id="drivinglicenseId" value="<%=dto.getDrivinglicenseId()%>">
<div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">License Renewal Application Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Driving License No:</label>
					</div>
					<div class="col-lg-4" ><%=dto.getLicenseNo()%></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Region:</label>
					</div>
					<div class="col-lg-4"><%=dto.getRegion()%></div>
					<div class="col-lg-2">
						<label class="control-label">Receipt Number:</label>
					</div>
					<div class="col-lg-4"><%=dto.getReceiptNo()%></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Receipt Date:</label>
					</div>
					<div class="col-lg-4"><%=dto.getReceiptDate()%></div>
					<div class="col-lg-2">
						<label class="control-label">Remarks:</label>
					</div>
					<div class="col-lg-4"><%=dto.getRemarks()%></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Submission Date:</label>
					</div>
					<div class="col-lg-4"><%=dto.getAppsubmissiondate()%></div>
				</div> 
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Expiry Date</label>
					</div>
					<div class="col-lg-4"><%=dto.getExpiryDate()%></div>
					
				</div> 
				<logic:equal value="APPROVE" name="param">
					<div class="form-group">
						<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
					</div>
				</logic:equal>
               </div>      
                 </div>
            </div>
            <div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="col-lg-12 form-group">
								<div class="col-lg-2">
									<label>Is TCB Endorsement?</label>
								</div>
								<div class="col-lg-3">
									<html:radio property="tcbEndorsement" value="N" styleId="tcbN" styleClass="ace" onclick="showDriveTypeList(this.value)">
										<span class="lbl">No</span>
									</html:radio>
									&nbsp;
									<html:radio property="tcbEndorsement" value="Y" styleId="tcbY" styleClass="ace" onclick="showDriveTypeList(this.value)">
										<span class="lbl">Yes</span>
									</html:radio>
								</div>
							</div>
							<!--<div class="col-lg-3">-->
								<!--<html:select property="drivetype" styleClass="form-control" styleId="drivetype" onchange="getDriveTypeTestMarks(this.value)">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="DRIVE_TYPE_LIST"
										label="headerName" value="headerId"  /> 
								</html:select>
								--><div class="form-group" id="ordinaryDriveTypeList">
									<div class="col-lg-2">
										<label>Drive Types:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="drivetype" styleClass="form-control" styleId="drivetype">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="DRIVE_TYPE_LIST" label="headerName" value="headerId"  /> 
										</html:select>
									</div>
									<div class="col-lg-3">
									 	<label style="display:none;color: #ff0000" id="driveTypeValidation">Please select a drive type</label>
									 	<div id="msgDIV" style="display:none;" class="alert alert-danger">
										</div>
									</div>
								</div>
							<!--</div>	-->
							<div class="form-group" id="tcbDriveTypeList">
								<div class="col-lg-2">
									<label>Drive Types:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="drivetypeTCB" styleClass="form-control" styleId="drivetypeTCB" multiple="true">
										<html:option value="">--SELECT--</html:option>
									</html:select>
								</div>
								<div class="col-lg-3">
								 	<label style="display:none;color: #ff0000" id="driveTypeValidationTCB">Please select a drive type</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Test Marks:</label>
								</div>
								<div class="col-lg-3">
									<input type="number" class="form-control" id="testMark">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks" styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script>
	var driveTypeId	=	'<%=dto.getDriveTypeId()%>';
	var is_TCB_endorsement	=	'<%=dto.getTcbEndorsement()%>';
	if(is_TCB_endorsement=="Y")
	{
		var roleIdArray = new Array();
		roleIdArray = driveTypeId.split("`");
		$('#drivetypeTCB').val(roleIdArray);
	}
	else
	{	
		$("#drivetype").val(driveTypeId);
	}
	showDriveTypeList(is_TCB_endorsement);
	function getDriveTypeTestMarks(driveTypeId)
	{
		$("#validationMessage").hide();
		var licenseNO	=	'<%=dto.getLicenseNo()%>';
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getDriveTypeTestMarks&driveTypeId='+driveTypeId+'&licenseNO='+licenseNO,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var flag = $(this).find('flag').text();
					var testMark = $(this).find('testMark').text();
					var reason	=	 $(this).find('reason').text();
					if(flag=='0')
					{
						$("#validationMessage").show();
						$('#validationMessage').html("<label>"+reason+"</label>");
						 $('#submitBtn').attr('disabled',true);
					}
					else if(flag=='1')
					{
						 $('#submitBtn').attr('disabled',false);
					}
					$("#testMark").val(testMark);
					 
				});
			}
		});
	}
	function showDriveTypeList(val)
	{
		$('#ordinaryDriveTypeList').hide();
		$('#tcbDriveTypeList').hide();
		
		if(val == "N")
			$('#ordinaryDriveTypeList').show();
		else 
		{
			$('#tcbDriveTypeList').show();
			
			var drivinglicenseId = $('#drivinglicenseId').val();

			if(drivinglicenseId == "")
			{
				$('#licenseNoValidation').show();
				$('#licenseNoValidation').get(0).scrollIntoView();
				return false;
			}
			else
			{
				$('#drivetypeTCB').empty();
				
				var url = '<%=request.getContextPath()%>/common.html?method=getTCBDriveTypeList&drivingLicenseId='+drivinglicenseId;
				
				$.ajax
				({	
					type: "GET",
					url : url,
					cache: false,
					async: false,
					dataType : "json",
					success : function(data) 
					{
						$('#drivetypeTCB').append("<option value='0' selected>--SELECT--</option>");
						
						for (var i = 0; i < data.length; i++) {
							$('#drivetypeTCB').append("<option value=" + data[i].headerId + ">"+ data[i].headerName + "</option>");
					    }
					},
					error : function(jqXHR, textStatus, errorThrown) {		
					}
				});
			}
		}
	}
</script>