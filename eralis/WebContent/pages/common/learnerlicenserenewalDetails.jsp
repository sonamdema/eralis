<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="learner-renewal-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Renewal Date</th>
			<th>Receipt Date</th>
			<th>Expiry Date</th>
			<th>Remarks</th>
			<th>Supporting Doc</th>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_LEARNER_LICENSE_LIST">
			<logic:iterate id="renewal" name="RENEWAL_LEARNER_LICENSE_LIST">
				<tr>
					<td><bean:write name="renewal" property="renewaldate"/></td>
					<td><bean:write name="renewal" property="receiptDate"/></td>
					<td><bean:write name="renewal" property="expiryDate"/></td> 
					<td><bean:write name="renewal" property="remarks"/></td>
					<td>
						<a href="#" onclick="downloadFile('<bean:write name="renewal" property="fileUUID" filter="false"/>','<bean:write name="renewal" property="fileName" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="renewal" property="fileName" filter="false"/>
	  					</a>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
<script>

	$(document).ready(function() 
	{
	    $('#learner-renewal-history-table').DataTable({
	            responsive: true
	    });
	});

</script>

