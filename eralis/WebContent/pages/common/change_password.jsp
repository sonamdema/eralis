<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
    <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
	<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login - eRaLIS</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- favicon -->
		<link rel="shortcut icon" href="<%=request.getContextPath() %>/ images/favicon.ico" type="image/x-icon" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/fonts/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-ie.min.css" />
		<![endif]-->

		<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="<%=request.getContextPath() %>/js/html5shiv.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/respond.min.js"></script>
		<![endif]-->
		
		<script>
		  <%
		  	String uid = (String) request.getAttribute("uid");
		  %>
		  
		  function saveDetails()
		  {
				var uid = $('#uid').val();
				var cpassword = $('#cpassword').val();
				var newpassword = $('#npassword').val();
				var password_confirmation = $('#confirm_password').val();
				var security_question = $('#security_question').val();
				var answer = $('#answer').val();

				if(cpassword == "")
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("Current password cannot be empty");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	return;
				}
				if(newpassword == "")
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("New password cannot be empty");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	return;
				}
				if(password_confirmation == "")
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("Password Confirmation cannot be empty");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	return;
				}
				if(cpassword == newpassword)
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("New password and Current password cannot be the same");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	$('#npassword').val("");
			      	$('#cpassword').val("");
			      	$('#confirm_password').val("");
			      	$('#security_question').val("");
			      	$('#answer').val("");
			      	return;
				}
				if(newpassword != password_confirmation)
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("New password and Password Confirmation doesnot match");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	$('#npassword').val("");
			      	$('#confirm_password').val("");
			      	$('#security_question').val("");
			      	$('#answer').val("");
			      	return;
				}
				if(security_question == "")
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("select atleast one security question");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	return;
				}
				if(answer == "")
				{
					$('#errorMsg').html("");
					$('#errorMsg').html("security answer cannot be empty");
			      	$('#errorMsg').show();
			      	setTimeout('hideStatus("errorMsg")',3000);
			      	return;
				}
				else
				{
				   $.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=updateFirstLoginDtls&cpassword="+cpassword+"&newpassword="+newpassword+"&uid="+uid+"&securityQuestionId="+security_question+"&answer="+answer,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#messageDiv").html(responseText);
							setTimeout('hideStatus("messageDiv")',5000);
							setTimeout('redirectPage()',6000);
						}
					});
				}
			}

		   function redirectPage()
			{
				var url = "<%=request.getContextPath()%>/Login";
				document.forms[0].action = url;
				document.forms[0].submit();
			}
		</script>
</head>
<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<!-- <i class="ace-icon fa fa-road green"></i> -->
									<img src="<%=request.getContextPath() %>/images/eRaLIS-Logo.png" width="70px" height="80px"/>
									<span class="gray"><b>e</b></span>-<font color="gray"><b>RaLIS</b></font>
									<!-- <span class="white" id="id-text2">RaLIS</span> -->
								</h1>
								<h4 class="blue" id="id-company-text">&copy; Road Safety and Transport Authority</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								
								<div id="signup-box" class="signup-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger">
												<i class="ace-icon fa fa-key blue"></i>
												Change your password
											</h4>

											<div class="space-6"></div>
											<p> Change your password to proceed further </p>

											<form action="" method="POST">
												<input type="hidden" id="uid" value="<%=uid%>"/>
												<div class="row" id="messageDiv">
								     		  		<div class="alert alert-danger" id="errorMsg" style="display:none"></div>
								     		  	</div>
												<fieldset>
													<label class="block clearfix">
														Current Password:
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Current Password" id="cpassword" name="cpassword"/>
															<i class="ace-icon fa fa-key"></i>
														</span>
													</label>
													
													<label class="block clearfix">
														New Password:
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="New Password" id="npassword" name="npassword"/>
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>
													
													<label class="block clearfix">
														Confirm Password:
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Confirm Password" id="confirm_password" name="confirm_password"/>
															<i class="ace-icon fa fa-retweet"></i>
														</span>
													</label>
													
													<label class="block clearfix">
														Security Question
														<span class="block input-icon input-icon-right">
															<select class="form-control" id="security_question">
																<option value="1">--SELECT--</option>
																<logic:iterate id="security" name="securityQuestionList">
										     						<option value='<bean:write name="security" property="headerId"/>'><bean:write name="security" property="headerName"/></option>
										     					</logic:iterate>
															</select>
														</span>
													</label>
													
													<label class="block clearfix">
														Answer
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Answer" id="answer"/>
															<i class="ace-icon fa fa-unlock-alt"></i>
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="button" class="width-65 pull-right btn btn-sm btn-success" onclick="saveDetails()">
															<span class="bigger-110">Save</span>
															<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
														</button>
													</div>
												</fieldset>
											</form>
										</div>

									</div><!-- /.widget-body -->
								</div><!-- /.signup-box -->
								
							</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
        </div>
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script src="<%=request.getContextPath() %>/js/jquery.1.11.1.min.js"></script>
		<![endif]-->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath() %>/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		
		<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
			
			
			$('document').ready(function()
			{
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
			});
			
		</script>
	</body>
</html>