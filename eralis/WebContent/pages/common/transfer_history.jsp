<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="transfer-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Transfer Date</th>
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			<th>Transfered From</th> 
			<th>Transfered To</th> 
			<th>Amount Paid</th> 
			<th>Supporting Doc</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="TRANSFER_HISTORY">
			<logic:iterate id="transfer" name="TRANSFER_HISTORY">
				<tr>
					<td><bean:write name="transfer" property="transferDate"/></td>
					<td><bean:write name="transfer" property="receiptNo"/></td>
					<td><bean:write name="transfer" property="receiptDate"/></td> 
					<td><bean:write name="transfer" property="transferorName"/></td> 
					<td><bean:write name="transfer" property="transfereeName"/></td> 
					<td align="right">Nu. <bean:write name="transfer" property="amount"/></td> 
					<td>
						 <logic:notEmpty name="transfer" property="fileUUID">
					        <a href="#" onclick="downloadFile('<bean:write name="transfer" property="fileUUID" filter="false"/>','<bean:write name="transfer" property="fileName" filter="false"/>')">
	  							<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="transfer" property="fileName" filter="false"/>
					    </logic:notEmpty>
	  				</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#transfer-history-table').DataTable({
	            responsive: true
	    });
	});

</script>