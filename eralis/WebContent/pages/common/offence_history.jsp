<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="offence-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Offence Date</th>
			<th>Place of Inspection</th> 
			<th>TIN No</th>
			<th>Offence</th>
			<th>Amount</th>
			<th>Inspected By</th>
			<th>Inspection Type</th>
			<th>Time of Inspection</th>
			<th>Traffic Branch</th>
			<th>Status</th>
			<th>Supporting Doc</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="OFFENCE_HISTORY">
			<logic:iterate id="offence" name="OFFENCE_HISTORY">
				<tr>
					<td><bean:write name="offence" property="offenceDate"/></td>
					<td><bean:write name="offence" property="placeOfInspection"/></td>
					<td><bean:write name="offence" property="TIN"/></td>
					<td><bean:write name="offence" property="offence1"/></td>
					<td><bean:write name="offence" property="amount"/></td>
					<td><bean:write name="offence" property="inspectedBy"/></td>
					<td><bean:write name="offence" property="inspectionType"/></td>
					<td><bean:write name="offence" property="timeOfInspection"/></td>
					<td><bean:write name="offence" property="trafficBranch"/></td>
					<td><bean:write name="offence" property="isPaid"/></td>
					<td>
						<a href="#" onclick="downloadFile('<bean:write name="offence" property="fileUUID" filter="false"/>','<bean:write name="offence" property="fileName" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="offence" property="fileName" filter="false"/>
	  					</a>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#offence-history-table').DataTable({
	            responsive: true
	    });
	});

</script>