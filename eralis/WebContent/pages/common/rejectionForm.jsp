<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:form styleClass="form-horizontal" action="/common.html" styleId="rejectApplication">
	<div id="rejectionModalForm" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Are you sure you want reject this application?</h4>
				</div>
					<div class="modal-body col-lg-12">
						<div class="col-lg-5">Please fill rejection reason</div>
						<div><html:textarea property="reason" styleId="rejectionReason" styleClass="col-lg-6"></html:textarea></div>
						<div id="msgDIV" style="display:none;" class="col-lg-12 alert alert-danger">Please fill rejection reason</div>
						<div id="rejectionMessage"  class="col-lg-12"></div>
					</div>
					<div class="modal-footer">
						 
						<button type="button" class="btn btn-sm btn-primary"  onclick="reject()">
							<i class="ace-icon fa fa-check"></i> Yes
						</button>
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							No
						</button>
					</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="serviceCode">
	<input type="hidden" id="applicationNo"/>
</html:form>

<script>
$(document).ready(function()
 {
	$("#serviceCode").val('<%=request.getAttribute("serviceCode")%>');
	$("#applicationNo").val('<%=request.getAttribute("applicationNo")%>');
 });
function reject()
{	
	
	$("#msgDIV").hide();
	var rejectionReason=$("#rejectionReason").val();
	if(rejectionReason=='')
	{
		$("#msgDIV").show();
		return false;
	}
	else 
	{
		applicationReject();
	}
}
function applicationReject()
{
	var serviceCode	= $('#serviceCode').val();
	var application=$("#applicationNo").val();
	var options = {target:'#rejectionMessage',url:context+'/common.html?method=reject_application&applicationNo='+application+'&serviceCode='+serviceCode,type:'POST',data: $("#rejectApplication").serialize()}; 
	$("#rejectApplication").ajaxSubmit(options);
    $('#rejectionMessage').show();
    setTimeout('hideStatus("rejectionMessage")',4000);
    setTimeout('showTaskList()',2000);
} 
</script>