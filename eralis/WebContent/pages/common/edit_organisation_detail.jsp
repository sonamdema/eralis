<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
EralisCommonDTO organisationDTO = (EralisCommonDTO)request.getAttribute("ORGANISATIONDTO");
%>
<div id="edit_ORGANISATION" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Edit Organisation Details</h4>
			</div>
			<div class="modal-body">
				<div class="row">
				
	<html:form styleClass="form-horizontal" action="/eralis_common.html" styleId="organizationInfo">
					<div class="widget-box">
						<div class="widget-header widget-header-small">
							<h5 class="widget-title lighter">Organizational Information</h5>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<div class="col-lg-2">
												<label>Type<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select property="type"
													 styleId="typedropdown" onchange="changetextbox()" styleClass="form-control">
													<html:option value="0">--SELECT--</html:option>
													<html:optionsCollection name="ownerTypeList"
														label="headerName" value="headerId" />
												</html:select>
											</div>
										</div>
										<div class="form-group" id="ministryDIV" style="display:none">
											<div class="col-lg-2">
												<label>Ministry:</label>
											</div>
											<div class="col-lg-3">
											<html:select property="ministry" styleClass="form-control" styleId="ministry" onchange="populateDependentDropDown(this.value, 'department', '', 'DEPARTMENT_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
											</html:select>
												
											</div>
										</div>
										<div class="form-group" id="departmentDIV" style="display:none">
											<div class="col-lg-2">
												<label>Department:</label>
											</div>
											<div class="col-lg-3">
												<html:select property="department" styleClass="form-control" styleId="department">
													<html:option value="0">--SELECT--</html:option>
												</html:select>
											</div>
										</div>
										<div class="form-group" id="privateDIV" style="display:none;">
											<div class="col-lg-2">
												<label>Private Company:</label>
											</div>
											<div class="col-lg-3">
												<html:select property="privateCompany" styleClass="form-control" styleId="privateCompany">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="privateCompanyList" label="headerName" value="headerId"/>
												</html:select>
											</div>
										</div>
										<div class="form-group" id="nameDIV" style="display:none;">
											<div class="col-lg-2">
												<label>Name<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:text property="name" styleId="name"
													styleClass="form-control" value="<%=organisationDTO.getName()%>"></html:text>
											</div>
										</div>
                                        
                                        <div class="form-group" id="regionDIV" style="display:none;">
											<div class="col-lg-2">
												<label>Region<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select property="region" styleClass="form-control" styleId="region" onchange="populateDependentDropDown(this.value, 'dzongkhag', '', 'DZONGKHAG_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
											</div>
										</div>
										  
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Dzongkhag<span style="color: #ff0000">*</span>:</label>
											</div>
											<div class="col-lg-3">
												<html:select property="dzongkhag" styleClass="form-control" styleId="dzongkhag">
													<html:option value="0">--SELECT--</html:option>
									   				<html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
												</html:select>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Address:</label>
											</div>
											<div class="col-lg-3">
												<html:textarea property="address" styleId="address" value="<%=organisationDTO.getAddress()%>"
													styleClass="form-control"></html:textarea>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Phone:</label>
											</div>
											<div class="col-lg-3">
												<html:text property="phone" styleId="phone" value="<%=organisationDTO.getPhone()%>"
													styleClass="form-control"></html:text>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Email:</label>
											</div>
											<div class="col-lg-3">
												<html:text property="email" styleId="email" value="<%=organisationDTO.getEmail()%>"
													styleClass="form-control"></html:text>
											</div>
										</div> 
                                        <div class="form-group">
											<div class="col-lg-2">
												<label>Remarks:</label>
											</div>
											<div class="col-lg-3">
												<html:textarea property="remarks" styleId="remarks" value="<%=organisationDTO.getRemarks()%>"
													styleClass="form-control"></html:textarea>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>	
					
				<div id="messageDiv"></div>
			
			<div class="pull-right">
				<html:hidden property="organisationInfoId" value="<%=organisationDTO.getOrganisationInfoId() %>" styleId="organisationInfoId"/>
				<button type="button" onclick="editOrganizationInfo()" class="btn btn-sm btn-primary">Update</button>
			</div>
			
		</html:form>
	</div>
</div>
</div>
</div>
</div>
<script>

var type_id	=	'<%=organisationDTO.getOrganizationTypeId()%>';
var type	=	'<%=organisationDTO.getOwnerName()%>';
var ministry	=	'<%=organisationDTO.getMinistry()%>';
var department	=	'<%=organisationDTO.getDepartment()%>';
var private_company	=	'<%=organisationDTO.getPrivateCompany()%>';
var region	=	'<%=organisationDTO.getRegionId()%>';
var dzongkhag	=	'<%=organisationDTO.getDzongkhagId()%>';



$("#typedropdown").val(type_id);
$("#ministry").val(ministry);
$("#department").val(department);
$("#privateCompany").val(private_company);
$("#region").val(region);
$("#dzongkhag").val(dzongkhag);



changetextbox(type);
function changetextbox(type)
{
	var selectedText	=	null;
	if(type=='')
	{
		selectedText = $('#typedropdown option:selected').text();
	}
	else
	{
		selectedText = type;
	}

   if(selectedText == "Government")
	{
		$('#ministry').attr('disabled', false);
		$('#department').attr('disabled', false);
		$('#regionDIV').show();
		$('#nameDIV').hide();
	    $('#privateDIV').hide();
	    $('#ministryDIV').show();
	    $('#departmentDIV').show();
	}
   else if(selectedText == "Royal Family")
	{
	   $('#ministry').attr('disabled', true);
	   $('#department').attr('disabled', true);
	   $('#nameDIV').show();
	   $('#privateDIV').hide();
	   $('#regionDIV').hide();
	   $('#ministryDIV').hide();
	    $('#departmentDIV').hide();
	}
   else if(selectedText == "Private")
	{
	   $('#ministry').attr('disabled', true);
	   $('#department').attr('disabled', true);
	   $('#nameDIV').hide();
	   $('#privateDIV').show();
	   $('#regionDIV').hide();
	   $('#ministryDIV').hide();
	    $('#departmentDIV').hide();
	}
}
function editOrganizationInfo()
{
	var options = {target:'#messageDiv',url:context+'/eralis_common.html?method=edit_organization_info',type:'POST',data: $("#organizationInfo").serialize()}; 
    $("#organizationInfo").ajaxSubmit(options);
    $('#messageDiv').show();
    setTimeout('hideStatus("messageDiv")',5000);
   // setTimeout('reloadPage()',5000);
}
</script>