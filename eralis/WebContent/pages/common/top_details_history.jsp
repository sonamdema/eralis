<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="top-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>TOP Number</th>
			<th>Region Name</th> 
			<th>Dzongkhag Name</th> 
			<th>Exact Location</th> 
			<th>Issue Date</th> 
			<th>Expiry Date</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="TOP_DETAILS_HISTORY">
			<logic:iterate id="topDetailsHistory" name="TOP_DETAILS_HISTORY">
				<tr>
					<td><bean:write name="topDetailsHistory" property="topNumber"/></td>
					<td><bean:write name="topDetailsHistory" property="region"/></td> 
					<td><bean:write name="topDetailsHistory" property="dzongkhag"/></td>
					<td><bean:write name="topDetailsHistory" property="exactLocation"/></td>
					<td><bean:write name="topDetailsHistory" property="dateOfissue"/></td> 
					<td><bean:write name="topDetailsHistory" property="expiryDate"/></td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#top-history-table').DataTable({
	            responsive: true
	    });
	});

</script>