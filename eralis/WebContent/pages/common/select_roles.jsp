<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
    <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
	<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login - eRaLIS</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- favicon -->
		<link rel="shortcut icon" href="<%=request.getContextPath() %>/ images/favicon.ico" type="image/x-icon" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/fonts/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-ie.min.css" />
		<![endif]-->

		<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="<%=request.getContextPath() %>/js/html5shiv.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/respond.min.js"></script>
		<![endif]-->
		
		<script>
		  function chooseRole()
		  {
		  	var roleCode = $('#idRoleSelect').val();        	
		  	if(roleCode == '')
		  	{
		  		alert('Please select a role');
		  		return false;
		  	}
		  	else
		  	{
		  		document.forms[0].submit();
		  	}
		  }
		</script>
</head>
<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<!-- <i class="ace-icon fa fa-road green"></i> -->
									<img src="<%=request.getContextPath() %>/images/eRaLIS-Logo.png" width="70px" height="80px"/>
									<span class="gray"><b>e</b></span>-<font color="gray"><b>RaLIS</b></font>
									<!-- <span class="white" id="id-text2">RaLIS</span> -->
								</h1>
								<h4 class="blue" id="id-company-text">&copy; Road Safety and Transport Authority</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								
								<div id="signup-box" class="signup-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger">
												<i class="ace-icon fa fa-check blue"></i>
												Select a role to proceed
											</h4>

											<div class="space-6"></div>
											<p> Select a role: </p>

											<form action="<%=request.getContextPath()%>/SelectRoleServlet" method="POST">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<html:select property="roleCode" value="-1" styleClass="form-control" styleId="idRoleSelect">
						                                        <html:option  value="" >Select</html:option>                                        
						                                        <logic:present name="roleList" scope="request">
						                                            <html:options collection="roleList" property="roleCode" labelProperty="roleName"/>
						                                        </logic:present>
						                                    </html:select>  
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="button" class="width-65 pull-right btn btn-sm btn-success" onclick="chooseRole()">
															<span class="bigger-110">Proceed</span>
															<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
														</button>
													</div>
												</fieldset>
											</form>
										</div>

									</div><!-- /.widget-body -->
								</div><!-- /.signup-box -->
								
							</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
        </div>
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script src="<%=request.getContextPath() %>/js/jquery.1.11.1.min.js"></script>
		<![endif]-->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath() %>/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
			
			
			$('document').ready(function()
			{
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
			});
			
		</script>
	</body>
</html>