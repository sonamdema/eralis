<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="page-header">
	<logic:equal value="LICENSE_PRINT_ALERT" name="IDENTIFIER">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			License Application Alert
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				List of license whose transactions are approved but not printed
			</small>
		</h1>
	</logic:equal>
	<logic:equal value="APPLICATION_ALERT" name="IDENTIFIER">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			Application Approval Alert
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				List of applications whose transactions are submitted but not approved
			</small>
		</h1>
	</logic:equal>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="space-6"></div>
			<div class="widget-box">
				<div class="widget-header">
					<div class="pull-right">
						<button type="button" class="btn btn-sm btn-success" onclick="exportToExcel()">
							<i class="fa fa-file-excel-o"></i>
							Export to Excel
						</button>
					</div>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="table-responsive">
							<logic:equal value="LICENSE_PRINT_ALERT" name="IDENTIFIER">
								<table id="alert-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Application No</th>
											<th>Service</th>
											<th>Processed From</th>
											<th>License No</th>
											<th>Receipt Date</th>
											<th>Elapsed Days</th>
										</tr>
									</thead>
									<tbody>
										<logic:iterate id="alert" name="ALERT_LIST" type="bt.gov.rsta.framework.dto.AlertDTO" indexId="index">
											<%
												int a = index.intValue();
											%>
											<tr>
												<td><%=++a %></td>
												<td>
													<bean:write name="alert" property="applicationNo"/>
												</td>
												<td>
													<bean:write name="alert" property="serviceName"/>
												</td>
												<td>
													<bean:write name="alert" property="processedFrom"/>
												</td>
												<td>
													<bean:write name="alert" property="licenseNo"/>
												</td>
												<td>
													<bean:write name="alert" property="receiptDate"/>
												</td>
												<td>
													<bean:write name="alert" property="elapsedDuration"/>
												</td>
											</tr>
										</logic:iterate>
									</tbody>
								</table>
							</logic:equal>
							<logic:equal value="APPLICATION_ALERT" name="IDENTIFIER">
								<table id="alert-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Application No</th>
											<th>Service</th>
											<th>Submitted On</th>
											<th>Status</th>
											<th>Processed From</th>
											<th>Elapsed Days</th>
										</tr>
									</thead>
									<tbody>
										<logic:iterate id="alert" name="ALERT_LIST" type="bt.gov.rsta.framework.dto.AlertDTO" indexId="index">
											<%
												int a = index.intValue();
											%>
											<tr>
												<td><%=++a %></td>
												<td>
													<bean:write name="alert" property="applicationNo"/>
												</td>
												<td>
													<bean:write name="alert" property="serviceName"/>
												</td>
												<td>
													<bean:write name="alert" property="receiptDate"/>
												</td>
												<td>
													<bean:write name="alert" property="status"/>
												</td>
												<td>
													<bean:write name="alert" property="processedFrom"/>
												</td>
												<td>
													<bean:write name="alert" property="elapsedDuration"/>
												</td>
											</tr>
										</logic:iterate>
									</tbody>
								</table>
							</logic:equal>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.col -->
</div><!-- /.row -->
<script>

	$(function () {
		 $('#alert-table').DataTable({
	            responsive: true,
	            aLengthMenu: [[10,25, 50, 100, -1],
					           [10,25, 50, 100, "All"]],
	             iDisplayLength: -1
	    });
  	});

  	function exportToExcel()
  	{
  		$('#alert-table').tableExport({type:'excel',escape:'false'});
  	}

</script>
