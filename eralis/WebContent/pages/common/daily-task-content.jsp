<div class="table-responsive">
	<table id="daily-task-table" class="table table-striped table-bordered table-hover">
		<thead>
		  <tr>
		    <td rowspan="3"><div align="center">Name</div></td>
		    <td rowspan="3"><div align="center">Region</div></td>
		    <td colspan="18"><div align="center">Learner License</div></td>
		    <td colspan="30"><div align="center">Driving License</div></td>
		    <td colspan="30"><div align="center">Vehicle</div></td>
		    <td colspan="6"><div align="center">TOP</div></td>
		    <td rowspan="3"><div align="center">Total</div></td>
		  </tr>
		  <tr>
		    <td colspan="6"><div align="center">New</div></td>
		    <td colspan="6"><div align="center">Renewal</div></td>
		    <td colspan="6"><div align="center">Replacement</div></td>
		    <td colspan="6"><div align="center">Non Commercial</div></td>
		    <td colspan="6"><div align="center">Commercial</div></td>
		    <td colspan="6"><div align="center">Renewal</div></td>
		    <td colspan="6"><div align="center">Replacement</div></td>
		    <td colspan="6"><div align="center">Endorsement</div></td>
		    <td colspan="6"><div align="center">Registration</div></td>
		    <td colspan="6"><div align="center">Renewal</div></td>
		    <td colspan="6"><div align="center">Transfer</div></td>
		    <td colspan="6"><div align="center">Replacement</div></td>
		    <td colspan="6"><div align="center">Conversion</div></td>
		    <td colspan="6"><div align="center">New</div></td>
		  </tr>
		  <tr>
		  	<td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		    
		    <td>Submitted</td>
		    <td>Approved</td>
		    <td>Rejected</td>
		    <td>Resubmitted</td>
		    <td>Verified</td>
		    <td>Dispatched</td>
		  </tr>
		</thead>
		<tbody>
			<%=(String) request.getAttribute("DAILY_TASK")%>
		</tbody>
	</table>
</div>

<script>

	$(document).ready(function() 
	{
   		$('#daily-task-table').DataTable({
            responsive: true
    	});
	});

</script>