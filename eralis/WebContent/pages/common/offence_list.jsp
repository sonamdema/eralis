<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Offence List</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				 <div class="table-responsive">
					<table id="renewal-search-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Sl</th>
								<th>Offence</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<% int a=1;%>
							<logic:notEmpty name="OFFENCE_LIST">
								<logic:iterate id="offence_list" name="OFFENCE_LIST">
									<tr>
										<td align="right"><%=a %></td>
										<td><bean:write name="offence_list" property="offenceName"/></td>
										<td>
											<bean:write name="offence_list" property="amount"/>
											<input type='hidden' id='amount<%=a++%>' value='<bean:write name="offence_list" property="amount"/>'>
											<input type='hidden' class='offenceId' value='<bean:write name="offence_list" property="offenceId"/>'>
										</td>
									</tr>
								</logic:iterate>
							</logic:notEmpty>
						</tbody>
					</table>
					</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="countOffenceRow" value="<%=a%>">
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Offence Details</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-lg-3">
							<label class="control-label">Learner No:</label>
						</div>
						<div class="col-lg-3">
							<div class="">
								<input type="text" id="learnerLicenseNo" readonly="readonly" class="form-control">
							</div>
						</div>
						<div class="col-lg-3">
							<label class="control-label">License No: <span style="color: #ff0000">*</span> :</label>
						</div>
						<div class="col-lg-3">
							<div class="">
								<input type="text" id="licenseNo" readonly="readonly"  class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						
						<div class="col-lg-3">
							<label class="control-label">Vehicle No. :</label>
						</div>
						<div class="col-lg-3">
							<div class="">
								<input type="text" id="displayVehicleNumber" readonly="readonly" class="form-control">
							</div>
						</div>
						<div class="col-lg-3">
							<label class="control-label">Offence Date:</label>
						</div>
						<div class="col-lg-3">
							<div class="">
								<input type="text" id="fetchOffenceDate" readonly="readonly" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3">
							<label class="control-label">Inspected By <span style="color: #ff0000">*</span>:</label>
						</div>
						<div class="col-lg-3">
							<input type="text" readonly="readonly" id="inspectedby" class="form-control">
						</div>
						<div style="display:none;"  id="updateregionDisplay">
							<div class="col-lg-3">
								<label class="control-label">Region:</label>
							</div>
							<div class="col-lg-3">
								<input type="text" readonly="readonly" id="updateregion" class="form-control">
							</div>
						</div>
						<div style="display:none" id="updatetrafficDisplay">
							<div class="col-lg-3">
								<label class="control-label">Traffic Branch:</label>
							</div>
							<div class="col-lg-3">
								<input type="text" readonly="readonly" id="udpatetrafficbranch" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3">
							<label class="control-label">Inspection Type <span style="color: #ff0000">*</span>:</label>
						</div>
						<div class="col-lg-3">
							<input type="text" readonly="readonly" id="fetchInspectedType" class="form-control">
						</div>
						<div class="col-lg-3">
							<label>Place Of Inspection:</label>
						</div>
						<div class="col-lg-3">
							<input type="text" readonly="readonly" id="trafplaceofInspectionficbranch" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3">
							<label>Document Seized :</label>
						</div>
						<div class="col-lg-3">
							<div class="checkbox">
                                    	<label>
                                    		<input type="checkbox" disabled="disabled" id="learnerlicense_seized">
									<span class="lbl"> Learner/License</span>
                                      </label>
                                      <label>
                                      	<input type="checkbox" disabled="disabled" id="bluebook_seized">
									<span class="lbl"> Blue Book</span>
                                      </label>
                             </div>
						</div>
						<div class="col-lg-3">
							<label class="control-label">Time of Inspection <span style="color: #ff0000">*</span> :</label>
						</div>
						<div class="col-lg-3">
							<div class="">
								<input type="text" id="inspectionTime" readonly="readonly"  class="form-control">

							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-lg-3">
							<label>TIN No<span style="color: #ff0000">*</span>:</label>
						</div>
						
						<div class="col-lg-3">
							<input type="text" readonly="readonly" readonly="readonly" id="new_TINno" class="form-control">
						</div>
						<div class="col-lg-3">
							<label>Remarks:</label>
						</div>
						<div class="col-lg-3">
							<textarea readonly="readonly" id="remarks" class="form-control"></textarea>
						</div>
					</div>
					<div id="uploadedDocument"></div>
				</div>
				<!--<div class="form-group">
					<div class="col-lg-3">
						<button type="button" onClick="payment_modal()" class="btn btn-primary btn-sm">Next</button>
					</div>
				</div>-->
			</div>
		</div>
	</div>
</div>