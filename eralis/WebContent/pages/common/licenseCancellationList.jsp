<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="cancell-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<td>Sl No</td>
			<td>Cancellation Date</td>
			<td>Cancellation Reason</td>
			<td>Status</td>
			<td>Withdrawn Date</td>
			<td>Withdrawn Reason</td>
			<td>Cancellation Document</td>
			<td>Withdrawn Document</td>
		</tr>
	</thead>
	<tbody><% int a=1; %>
		<logic:notEmpty name="LICENSE_CANCELLATION_LIST">
			<logic:iterate id="licenseCancellationList" name="LICENSE_CANCELLATION_LIST">
				<tr>
					<td><%=a++ %></td>
					<td><bean:write name="licenseCancellationList" property="cancellationdate"/></td>
					<td><bean:write name="licenseCancellationList" property="cancellationReason"/></td> 
					<td><bean:write name="licenseCancellationList" property="status"/></td> 
					<td><bean:write name="licenseCancellationList" property="withdrawnDate"/></td> 
					<td><bean:write name="licenseCancellationList" property="withdrawnReason"/></td> 
					<td>
						<logic:notEmpty name="licenseCancellationList" property="canDOCUUID">
							<a href="#" onclick="downloadFile('<bean:write name="licenseCancellationList" property="canDOCUUID" filter="false"/>','<bean:write name="licenseCancellationList" property="canDOCName" filter="false"/>')">
		  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
		  						Cancellation Doc
		  					</a>
	  					</logic:notEmpty>
					<td>
						<logic:notEmpty name="licenseCancellationList" property="withDOCUUID">
							<a href="#" onclick="downloadFile('<bean:write name="licenseCancellationList" property="withDOCUUID" filter="false"/>','<bean:write name="licenseCancellationList" property="withDOCName" filter="false"/>')">
		  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
		  						Withdrawn Doc
		  					</a>
		  				</logic:notEmpty>
	  				</td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#cancell-history-table').DataTable({
	            responsive: true
	    });
	});

</script>
<script>

	function downloadFile(uuid,fileName)
	{
		 window.open('<%=request.getContextPath()%>/FileDownloadServlet?uuid='+uuid+'&fileName='+fileName,'','width=600,height=600');
	}	

</script>


