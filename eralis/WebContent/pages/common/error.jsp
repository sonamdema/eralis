<div class="error-container">
	<div class="well">
		<h1 class="grey lighter smaller">
			<span class="blue bigger-125">
				<i class="ace-icon fa fa-random"></i>
				500
			</span>
			Something Went Wrong
		</h1>

		<hr />
		<h3 class="lighter smaller">
			But we are working
			<i class="ace-icon fa fa-wrench icon-animated-wrench bigger-125"></i>
			on it!
		</h3>

		<div class="space"></div>

		<div>
			<h4 class="lighter smaller">Meanwhile, try one of the following:</h4>

			<ul class="list-unstyled spaced inline bigger-110 margin-15">
				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Contact the system administrator with your error code: <font color='red'><strong><%=request.getAttribute("errorCode") %></strong></font>
				</li>

				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Give us more info on how this specific error occurred!
				</li>
			</ul>
		</div>

		<hr />
		
	</div>
</div>