<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<%
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	//get current date time with Date()
	Date date = new Date();
	String dd = dateFormat.format(date);
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Daily Task Report
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 View daily task performed by you
		</small>
	</h1>
</div>

<form>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<div class="col-lg-3">
	               	<label>Start Date<span style="color: #ff0000">*</span>:</label>
	              	</div>	
				<div class="col-lg-3">
					<div class="input-group">
						<input type="text" class="form-control date-picker" id="startDate" readonly="readonly" value="<%=dd%>"/>
						<span class="input-group-addon" style="cursor: pointer;">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
					</div>
			 	</div>	
			 	<div class="col-lg-3">
	               	<label>End Date<span style="color: #ff0000">*</span>:</label>
	              	</div>	
				<div class="col-lg-3">
					<div class="input-group">
						<input type="text" class="form-control date-picker" id="endDate" value="<%=dd%>" readonly="readonly"/>
						<span class="input-group-addon" style="cursor: pointer;">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
					</div>
			 	</div>	
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="pull-right">
			<button type="button" class="btn btn-primary btn-sm" id="submitBtn" onclick="getReport()">Generate Report</button>
		</div>
	</div>
</form>
<br>
<div id="report"></div>

<script>

	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	$('document').ready(function()
	 {
		 var startDate = $('#startDate').val();	
		 getReportInitial(startDate);
	 });
	 
	 function getReportInitial(startDate)
	 {
		 $.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=daily_task&startDate="+startDate+"&requestType=individual&reportType=initial",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#report").html(responseText);
				}
			});
	 }
	 
	 function getReport()
	 {
		 var startDate = $('#startDate').val();	
		 var endDate = $('#endDate').val();
		 
		 $.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=daily_task&startDate="+startDate+"&endDate="+endDate+"&requestType=individual&reportType=noninitial",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#report").html(responseText);
				}
			});
	 }	

</script>