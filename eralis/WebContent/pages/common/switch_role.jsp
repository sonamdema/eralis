<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-gears"></i>
		Switch Role
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Change your current role &amp; privileges
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="space-6"></div>
			<div class="widget-box">
				<div class="widget-header widget-header-flat widget-header-small">
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<h4 class="header green lighter bigger">
							<i class="ace-icon fa fa-check blue"></i>
							Select a role to proceed
						</h4>

						<div class="space-6"></div>
						<p> Select a role: </p>

						<form action="<%=request.getContextPath()%>/SelectRoleServlet" method="POST">
							<fieldset>
								<label class="block clearfix">
									<span class="block input-icon input-icon-right">
										<html:select property="roleCode" value="-1" styleClass="form-control" styleId="idRoleSelect">
	                                        <html:option  value="" >Select</html:option>                                        
	                                        <logic:present name="roleList" scope="request">
	                                            <html:options collection="roleList" property="roleCode" labelProperty="roleName"/>
	                                        </logic:present>
	                                    </html:select>  
									</span>
								</label>

								<div class="space-24"></div>

								<div class="clearfix">
									<button type="button" class="pull-right btn btn-sm btn-success" onclick="chooseRole()">
										<span class="bigger-110">Proceed</span>
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
							</fieldset>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.col -->
</div><!-- /.row -->

<script>
  function chooseRole()
  {
  	var roleCode = $('#idRoleSelect').val();        	
  	if(roleCode == '')
  	{
  		alert('Please select a role');
  		return false;
  	}
  	else
  	{
  		document.forms[0].submit();
  	}
  }
</script>
