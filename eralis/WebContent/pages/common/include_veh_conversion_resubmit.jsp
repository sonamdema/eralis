<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
//ServiceDTO dto = (ServiceDTO)request.getAttribute("ServiceDTO");
%>

<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");

%>
<div class="widget-box">
     <div class="widget-header widget-header-small">
     	<h5 class="widget-title lighter">Conversion Details</h5>
     </div>
     <div class="widget-body">
     	<div class="widget-main">
            <div class="form-group">
            	<div class="col-lg-2">
					<label>Conversion Reason :<span style="color: #ff0000">*</span> </label>
		        </div>
                <div class="col-lg-3">
                    <html:select property="conversionReason" styleClass="form-control" styleId="conversionReason">
	                    <html:option value="">--SELECT--</html:option>
	                    <html:optionsCollection  name="vehicleConversionList" label="headerName" value="headerId"/>
                   	</html:select>
                </div>
                <div class="col-lg-2">
					<label>Vehicle Rgn. Code :</label>
				</div>	
                <div class="col-lg-3">
                    <html:select property="vehicleRegistrationId" styleClass="form-control" styleId="vehicleRegistrationId">
						<html:option value="">--SELECT--</html:option>
						<html:optionsCollection name="vehicleCodeList" label="headerName" value="headerId"/>
					</html:select>
            	</div> 
        	</div>
            <div class="form-group">
	        	<div class="col-lg-2">
					<label>Region :</label>
		        </div>	
                <div class="col-lg-3">
                	<html:select property="region" styleClass="form-control" styleId="region">
						<html:option value="">--SELECT--</html:option>
				   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
				    </html:select>
                </div>
	            <div class="col-lg-2">
					<label>Vehicle Type :</label>
				</div>	
                <div class="col-lg-3">
                	<html:select property="vehicleType" styleClass="form-control" styleId="vehicleTypes">
						<html:option value="">--SELECT--</html:option>
					  	<html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
				  	</html:select>
                </div> 
	  		</div>
     	</div>
	</div>
</div>
<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Attachments</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								
								<div class="form-group">
									<div class="col-lg-2">
										<label> Supporting Document:</label>
									</div>
									<div class="col-lg-3">
										<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload"  onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
									</div>
					              </div>
	                     		 </div> 
	                   		</div>
	                	 </div>
					</div>
				</div>	
<script>
$('#conversionReason').attr('disabled', true);
$('#vehicleRegistrationId').attr('disabled', true);
$('#region').attr('disabled', true);
$('#vehicleTypes').attr('disabled', true);

	var conversionReason	=	'<%=dto.getConversionReasonId()%>';
	$("#conversionReason").val(conversionReason);
	var vehRgnCode	=	'<%=dto.getVehicleRegistrationId()%>';
	$("#vehicleRegistrationId").val(vehRgnCode);
	var region	=	'<%=dto.getRegion()%>';
	$("#region").val(region);
	var vehicleType	=	'<%=dto.getVehicleTypeId()%>';
	$("#vehicleTypes").val(vehicleType);

</script>