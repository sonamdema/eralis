<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
</head>
<%
	EralisUserRolePriviledge userRolePriv = null;
	String regionId = null, regionName = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		regionId = userRolePriv.getRegionId();
		regionName  = userRolePriv.getRegionName();
	}
%>
<body class="no-skin">
		<!-- header include -->
		<jsp:include page="/pages/common/header.jsp"></jsp:include>
		<!-- ./header include -->

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- menu include -->
			<jsp:include page="/pages/common/menu.jsp"></jsp:include>
			<!-- ./menu include -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<!-- <ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul> /.breadcrumb -->
						
						<div class="nav-search" id="nav-search">
							<i class="ace-icon fa fa-calendar fa-lg"></i> 
							<%
							 	String DATE_FORMAT_NOW = "dd-MM-yyyy";
								Calendar cal = Calendar.getInstance();

								SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
							    String day = sdfDay.format(cal.getTime());
							    
							    SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
							    String year = sdfYear.format(cal.getTime());
							    
							    Calendar mCalendar = Calendar.getInstance();    
							    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
							%>
							<span class="blue"><%=day%>&nbsp;<%=month %>,&nbsp;<%=year %></span>
							<input type="hidden" id="mainRegionId" value="<%=regionId%>"/>
							<input type="hidden" id="mainRegionName" value="<%=regionName%>"/>
						</div>
					</div>

					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="page-header">
								<h1>
									<i class="ace-icon fa fa-tachometer"></i> Print List
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
										list of pending license for printing....
									</small>
								</h1>
							</div><!-- /.page-header -->
							<div class="row">
								<div class="col-xs-12">
										<div class="row">
											<div class="space-6"></div>
											<div class="table-responsive">
												<table id="license-print-table" class="table table-striped table-bordered table-hover">
													<thead>
														<tr>
															<th></th>
															<th>Application Number</th>
															<th>Service Name</th>
															<th>License Number</th>
															<th>Document Type</th> 
															<th>Approved Date</th> 
														</tr>
													</thead>
													<tbody>
														<logic:iterate id="license" name="PRINT_LIST" type="bt.gov.rsta.eralis.dto.common.PrintDTO" indexId="index">
															<% int a = index.intValue(); %>
															<tr>
																<td><%=++a %></td>
																<td>
																	<bean:write name="license" property="applicationNo"/>
																</td>
																<td>
																	<bean:write name="license" property="serviceName"/>
																</td>
																<td>
																	<bean:write name="license" property="identityNo"/>
																</td>
																<td>
																	<bean:write name="license" property="documentType"/>
																</td>
																<td>
																	<bean:write name="license" property="approvedDate"/>
																</td>
																
															</tr>
														</logic:iterate>
													</tbody>
												</table>
											</div>
											
										</div><!-- /.row -->
									<!-- PAGE CONTENT ENDS -->
								</div><!-- /.col -->
							</div><!-- /.row -->
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<!-- footer include -->
			<jsp:include page="/pages/common/footer.jsp"></jsp:include>
			<!-- /.footer include -->
			
		</div><!-- /.main-container -->
<div id="pendingModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="red bigger">Pending Form</h3>
			</div>
			<div class="modal-body">
				<html:form styleClass="form-horizontal" action="/common.html?method=pendingPrint" styleId="pendingForm">
					<div class="row">
						<div class='col-lg-12 form-group' >
	                       <div class="col-lg-4">
								<label>Remarks </label>
						   </div>
	                       <div class="col-sm-7">
	                          <html:textarea property="remarks" styleClass="form-control" ></html:textarea>
	                       </div>
	                	</div>
					</div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-primary btn-sm"  onclick="savePending()">Save</button>
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
							 Cancel
						</button>
						<input type="hidden" id="printId">
					</div>
				</html:form>
			</div>
		</div>
	</div>
</div>		
		
		<script>

			$(document).ready(function() 
				{
				    $('#license-print-table').DataTable({
				            responsive: true
				    });
				});

			function pendingModal(printId)
			{
				$("#pendingModal").show();
				$("#printId").val(printId);
			}

			function savePending()
			{	
				var printId	=	$("#printId").val();
				alert(printId);


				var options = {target:'#displayMsgDiv',url:context+'/common.html?method=savePendingPrint&printId='+printId,type:'POST',data: $("#learnerForm").serialize()};  
			    $("#pendingForm").ajaxSubmit(options);
			    $('#pendingModal').modal('hide');
			    setTimeout('hideStatus("displayMsgDiv")',4000);
			}
		</script>
		
	</body>
</html>