
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
<table id="vehicle-list-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Vehicle Number</th>
			<th>Vehicle Type</th>
			<th>Category</th> 
			<th>Name</th> 
			<th>CID</th> 
			<th>Engine Number</th> 
			<th>Chassis Number</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_LIST">
			<logic:iterate id="renewal" name="RENEWAL_LIST">
				<tr>
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="renewal" property="renewalInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
					</td>
					<td>
						<bean:write name="renewal" property="vehicleNo"/>
					</td>
					<td><bean:write name="renewal" property="vehicleType"/></td>
					<td>
						<logic:equal value="P" name="renewal" property="ownerType">
							Personal
						</logic:equal>
						<logic:equal value="O" name="renewal" property="ownerType">
							Organization
						</logic:equal>
						<logic:equal value="Foreign" name="renewal" property="ownerType">
							Foreign
						</logic:equal>
					</td>
					<td><bean:write name="renewal" property="personalOwnerName"/></td>
					<td><bean:write name="renewal" property="CID"/></td>
					<td><bean:write name="renewal" property="engineNumber"/></td>
					<td><bean:write name="renewal" property="chasisNumber"/></td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
</div>
<script>
	 $(document).ready(function() 
	{
	    $('#vehicle-list-table').DataTable({
	            responsive: true
	    });
	});
</script>
<script>
	<%
		String SEARCH_TYPE = (String) request.getAttribute("SEARCH_TYPE");
		String type = (String) request.getAttribute("TYPE");
	%>
	
	var type = "<%=type%>";
	var SEARCH_TYPE = "<%=SEARCH_TYPE%>";

	function getSelected(globalId)
	{
		$("#submitBtn").prop('disabled',false);
		$("#submitBtn").show();
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getRenewalDtls&renewalInfoId='+globalId+'&type='+type,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{	
					$("#cancelledStatus").hide();
					$("#Msg").hide();
					$("#incompleteValidationMsg").hide();
					
					var applicationNo = $(this).find('applicationNo').text();
					var lifeExpiryDate = $(this).find('lifeExpiryDate').text();
					var Status = $(this).find('Status').text();
					var cancelledDate = $(this).find('cancelDate').text();
					var cancelReason = $(this).find('reason').text();
					var receiptNo = $(this).find('receiptNo').text();
					var receiptDate = $(this).find('receiptDate').text();
					var CurrentDate = new Date();
					var lastLifeDate = new Date(lifeExpiryDate);

					var initialReceiptDate = $(this).find('initialReceiptDate').text();
					var initialReceiptNo = $(this).find('initialReceiptNo').text();
					var penalty = $(this).find('penalty').text();
					var amount = $(this).find('initial-price').text();
					if(type!="OFFENCE" && Status=="VEHICLE_OUTSTANDING")
					{
						
						var vehicle_type =  $(this).find('outsatndingVehicleType').text(); 
						var vehicleNo = $(this).find('outsatndingVehicleNo').text();
						if(SEARCH_TYPE=="ROUTE_PERMIT")
						{
							$("#rpVMsg").html("<div class='alert alert-danger'>This customer has <b>vehicle outstanding</b> against the following vehicle details");
							$("#rpVMsg").show();
						}
						else
						{
							var vehicleNoArray = vehicleNo.split("/");	
							var vehicleTypeArray = vehicle_type.split("/");	
							var vehcleCol = "";
							for(var i=0;i<vehicleNoArray.length;i++)
							{
								vehcleCol = vehcleCol+"<tr><td>"+vehicleNoArray[i]+"</td><td>"+vehicleTypeArray[i]+"</td></tr>";
							}
							$("#Msg").html('<div class="alert alert-danger col-lg-12"><div class="col-lg-12" style="margin-bottom: 10px;font-size: 17px;">This customer has <b>vehicle outstanding</b> against the following vehicle : </div><div class="col-lg-6"><table class="table table-bordered"><thead><tr><th>Vehicle No.</th><th>Vehicle Type</th></tr></thead><tbody class="bg-warning">'+vehcleCol+'</tbody></table></div></div>');
							$("#Msg").show();
						}
						if(type!='HISTORY')
						{
							return false;
						}
					}
					else if(type!="OFFENCE" && Status == 'DRIVING_LICENSE_OUTSTANDING')
					{
						var licenseNO = $(this).find('licenseNo').text();
						$("#Msg").html("<div class='alert alert-danger'>This customer has <b>Driving License outstanding</b> against the Driving License no. : <b>"+licenseNO+"</b>, please clear the outstanding.</div>");
						$("#Msg").show();
						if(type!='HISTORY')
						{
							return false;
						}
					}
					
					
					$("#initialReceiptNo").text(initialReceiptNo);
					$("#initialReceiptDate").text(initialReceiptDate);
					$("#initialPenaly").text(penalty);
					$("#initialAmount").text(amount);
					
					
					if(SEARCH_TYPE=='WITHDRAW_VEH_CANCELLATION')
					{
						if(Status=='CANCELLED')
						{
							$("#cancelledDate").text(cancelledDate);
							$("#cancelReason").text(cancelReason);
							$("#receiptNo").text(receiptNo);
							$("#receiptDate").text(receiptDate);
							Status="";
						}
						else
						{

						}
					}
					
					if(SEARCH_TYPE=='UPDATE_OFFENCE')
					{	
						var Inspected_By	=	$(this).find('Inspected_By').text(); 
						var vehicleId	=	$(this).find('vehicleId').text(); 
	                    var vehicleNumber = $(this).find('vehicleNumber').text();               
						var ownerName = $(this).find('OwnerName').text();
						var nationalType = $(this).find('nationalType').text();
					    $('#Inspected_By').val(Inspected_By);
					    $('#updatevehicleId').val(vehicleId);
					    $('#updatedisplayVehicleNumber').val(vehicleNumber);
						$('#updatedisplayOwnerName').html("<label>"+ownerName+"</label>");
						searchOffence(vehicleId,"NA",nationalType);
					} 
					else 
					{
						
						var personalInfoId	=	$(this).find('personalInfoId').text();
						var ownerType = $(this).find('OwnerType').text();
						var ownerID = $(this).find('ownerid').text();
						var ownerName = $(this).find('OwnerName').text();
						var CitizenID = $(this).find('CID').text();
						var phone = $(this).find('phoneno').text();
						var Dzongkhag = $(this).find('Dzongkhag').text();
						var Gewog = $(this).find('Gewog').text();
	                    var Village =$(this).find('Village').text();
						var Country = $(this).find('Countryname').text();
						var Address = $(this).find('Address').text();
	                    var registrationDate = $(this).find('RegistrationDate').text();
	                    var expiryDate = $(this).find('ExpiryDate').text(); 
	                    var Company = $(this).find('Company').text(); 
	                    var Model = $(this).find('Model').text(); 
	                    var chasisNumber = $(this).find('chasisNumber').text(); 
	                    var engineNumber = $(this).find('engineNumber').text(); 
	                    var engineType = $(this).find('engineType').text(); 
	                    var seatCapacity = $(this).find('seatCapacity').text(); 
	                    var loadCapacity = $(this).find('loadCapacity').text(); 
	                    var Region = $(this).find('Region').text(); 
	                    var regionId = $(this).find('Region-Id').text();
	                    var lastIssueDate = $(this).find('lastIssueDate').text(); 
	                    var Color = $(this).find('Color').text(); 
	                    var colorName = $(this).find('colorName').text(); 
	                    
	                    if(Status == "1")
	                        Status = "Active";
                        
	                    var Email = $(this).find('Email').text();
	                    var vehicleNumber = $(this).find('vehicleNumber').text();               
	                    var customerId  = $(this).find('customerId').text();  
						var vehicleType	=$(this).find('vehicleType').text();
						var manufactureYear = $(this).find('manufacture-year').text(); 
						var vehicleTypeName = $(this).find('vehicle-type-name').text();
						var vehicleHorsePower	=$(this).find('vehicleHorsePower').text();  	
						var engineCC	=$(this).find('engineCC').text();
						var vehicleKiloWatt	=$(this).find('vehicleKiloWatt').text();  	 	
						var vehicleId	=	$(this).find('vehicleId').text(); 
						var isInternational = $(this).find('IsInternational').text(); 
						var vehicleTypeDesc = $(this).find('vehicleTypeDesc').text();
						var vehicleRegistrationCode = $(this).find('vehicle-registration-code').text();
						var region = $(this).find('region').text();
						var initialPrice = $(this).find('initial-price').text();
						var ministry = $(this).find('ministry').text();
						var department = $(this).find('department').text();
						var privateName = $(this).find('private').text();
						var owner = $(this).find('owner').text();
						var ownerTypeDesc = $(this).find('owner-type-desc').text();
						var passangerBusType = $(this).find('passangerBusType').text();
						var latestExpiryDate=$(this).find('latestExpiryDate').text();
						var transferExpiryDate=$(this).find('transferExpiryDate').text();
						
						$('#displayVehicleNumber').val(vehicleNumber);
						$('#newDisplayVehicleNumber').val(vehicleNumber);
						$('#newVehicleId').val(vehicleId);

						if(ownerType == "Personal")
						{
							$('#Personal').show();
							$('#Organization').hide();
						}
						else
						{
							$('#Personal').hide();
							$('#Organization').show();
						}
						//newly commented by pramod uchicha
						//if(type == "NA" || type=="FITNESS_REPLACEMENT")
						//{
							if(type!="HISTORY" || SEARCH_TYPE!="OFFENCE") 
								{
								if(Country=="Bhutan")
								{

									if((isInternational=='N' && (Dzongkhag == "" || Dzongkhag == "null")) || phone<1 || phone=="" || phone=="null" ||  registrationDate == "" || registrationDate == "null" ||
										Company == "" || Company == "null" || Model == "" || Model == "null" ||
										chasisNumber == "" || chasisNumber == "null" || engineNumber == "" || engineNumber == "null" ||
										engineType == "" || engineType == "null" || vehicleType == "" || vehicleType == "null" ||
										manufactureYear == "" || manufactureYear == "null" || initialPrice == "null" || initialPrice == "" ||
										phone == "" || phone == "null" || Address == "" || Address == "null" || vehicleRegistrationCode == "" || 
										vehicleRegistrationCode == "null")
									{
										if((isInternational=='N' || isInternational==''  || isInternational=="null") && (phone<1 || phone=="" || phone=="null" || Dzongkhag == "" || Dzongkhag == "null"  || Address == "" || Address == "null" ))
										{
											$("#submitBtn").hide();
											$('#incompleteValidationMsg').html('<div class="alert alert-danger">This vehicle doesnot have complete owner information of customer Id: <b>'+customerId+'</b></div>');
											$("#incompleteValidationMsg").show();
											return false;
										}
										else if((isInternational=='Y') && (phone<1 || phone=="" || phone=="null"  || Address == "" || Address == "null" ))
										{
											$('#incompleteValidationMsg').html('<div class="alert alert-danger">This vehicle doesnot have complete owner information of customer Id: <b>'+customerId+'</b></div>');
											$("#incompleteValidationMsg").show();
											$("#submitBtn").hide();
											return false;
										}
										else
										{
	
											$('#incompleteValidationMsg').html('<div class="alert alert-danger">This vehicle doesnot have complete vehicle information</div>');
											$("#incompleteValidationMsg").show();
											$("#submitBtn").hide();
											return false;
										}
	
									}
								}
								}
								else
								{
									
									
									if(Status=='HAS_OFFENCE')
									{
										$("#Msg").html("<div class='alert alert-danger'>Vehicle Has Offence</div>");
										$("#Msg").show();
										$("#submitBtn").prop('disabled',true);
										if(type!="HISTORY" || SEARCH_TYPE!="OFFENCE") {
											return false;	
										}
										
									}
									else if(Status=='CANCELLED')
									{
										$('#Msg').html("<div class='alert alert-danger'><h4>Sorry! Transaction is not allowed:</h4>This vehicle has been <strong>Cancelled</strong> in our record with effect from <strong>"+cancelledDate+"</strong> based on the the following reason: <strong>"+cancelReason+"</strong> </div>");
										$("#Msg").show();
										$("#submitBtn").prop('disabled',true);
										if(type!="HISTORY" || SEARCH_TYPE!="OFFENCE") {
											return false;	
										}									}
									
									else if(Status == "PENDING_APPLICATION")
									{
										$("#submitBtn").prop('disabled', true);
										$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
										$('#Msg').show();
										
									}
									
									else if(lastLifeDate!='Invalid' && lastLifeDate < CurrentDate && SEARCH_TYPE!="VEHICLE_CANCELLATION"){
										
										$("#Msg").html("<div class='alert alert-danger'>Sorry! Transaction is not allowed: This Vehicle has reached its age. Validity Date : "+lifeExpiryDate+"</div>");
										$("#Msg").show();
										$("#submitBtn").prop('disabled',true);
										return false;
									}
									else
									{
										if(Status=='EXPIRED' && type=='FITNESS_REPLACEMENT')
										{
											$('#Msg').html("<div class='alert alert-danger'><h4>Sorry! Transaction is not allowed:</h4>This vehicle fitness has expired.</div>");
											$("#Msg").show();
											$("#submitBtn").prop('disabled',true);
											$("#submitBtn").prop('disabled',true);
											if(type!="HISTORY" || SEARCH_TYPE!="OFFENCE") {
												return false;	
											}										}
										if(Status=='NO-RECORD' && type=='FITNESS_REPLACEMENT')
										{
											$('#Msg').html("<div class='alert alert-danger'><h4>Sorry! Transaction is not allowed:</h4>This vehicle's fitness test data is not found.</div>");
											$("#Msg").show();
											$("#submitBtn").prop('disabled',true);
											$("#submitBtn").prop('disabled',true);
											if(type!="HISTORY" || SEARCH_TYPE!="OFFENCE") {
												return false;	
											}										}
										else if(Status=='PASSANGER_BUS_REACHED_LIMITATION' && SEARCH_TYPE=='VEH_RENEWAL')
										{
											$('#Msg').html("<div class='alert alert-danger'><h4>Sorry! Transaction is not allowed:</h4>This <b>Passenger Bus</b> has reached its age.</div>");
											$("#Msg").show();
											$("#submitBtn").prop('disabled',true);
											$("#submitBtn").prop('disabled',true);
											if(type!="HISTORY" || SEARCH_TYPE!="OFFENCE") {
												return false;	
											}										}
										if(lastLifeDate!='Invalid Date')
										{
											$("#Msg").html("<div class='alert alert-info'>Life Validity Date of this vehicle is : "+lifeExpiryDate+"</div>");
											$("#Msg").show();
										}
										
										$('#passangerBusType').html("<label class='control-label'>"+passangerBusType+"</label>");
										$('#displayOwnerType').html("<label class='control-label'>"+ownerType+"</label>");
										$('#displayOwnerID').html("<label class='control-label'>"+ownerID+"</label>");
										$('#displayOwnerName').html("<label class='control-label'>"+ownerName+"</label>");
										$('#displayCitizenID').html("<label class='control-label'>"+CitizenID+"</label>");
										$('#displayPhone').html("<label class='control-label'>"+phone+"</label>");  
										$('#displayDzongkhag').html("<label class='control-label'>"+Dzongkhag+"</label>");
										$('#displayGewog').html("<label class='control-label'>"+Gewog+"</label>");
										$('#displayVillage').html("<label class='control-label'>"+Village+"</label>");
										$('#displayCountry').html("<label class='control-label'>"+Country+"</label>");
										$('#displayAddress').html("<label class='control-label'>"+Address+"</label>");
										$('#displayLastRegistrationDate').html("<label class='control-label'>"+registrationDate+"</label>");
										$('#displayExpiryDate').html("<label class='control-label'>"+expiryDate+"</label>");
										$('#displayCompany').html("<label class='control-label'>"+Company+"</label>");
										$('#displayModel').html("<label class='control-label'>"+Model+"</label>");
										$('#displayChasisNumber').html("<label class='control-label'>"+chasisNumber+"</label>");
									    $('#displayEngineNumber').html("<label class='control-label'>"+engineNumber+"</label>");
										$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
										$('#displaySeatCapacity').html("<label class='control-label'>"+seatCapacity+"</label>");
										$('#displayLoadCapacity').html("<label class='control-label'>"+loadCapacity+"</label>");
									    $('#displayRegion').html("<label class='control-label'>"+Region+"</label>");
										$('#displayLastRegistrationDate').html("<label class='control-label'>"+lastIssueDate+"</label>");
									    $('#displayColor').html("<label class='control-label'>"+colorName+"</label>");
								        $('#displayStatus').html("<label class='control-label'>"+Status+"</label>");
								        $('#displayEmail').html("<label class='control-label'>"+Email+"</label>");
								        $('#vehicleType').html("<label class='control-label'>"+vehicleType+"</label>");
										$('#displayVehicleTypeName').html("<label class='control-label'>"+vehicleTypeName+"</label>");
										$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
										$('#displayInitialPrice').html("<label class='control-label'>Nu.&nbsp;"+initialPrice+"</label>");
										$('#displayEngineCC').html("<label class='control-label'>"+engineCC+"</label>");
										//$('#displaylatestExpiryDate').html("<label class='control-label'>"+latestExpiryDate+"</label>");
										$('#displayTransferExpiryDate').html("<label class='control-label'>"+transferExpiryDate+"</label>");
										$('#displayOrgOwnerType').html("<label class='control-label'>"+owner+"</label>");

										if(ownerTypeDesc == "GOVERNMENT")
										{
												$('#displayOrgAgency').html("<label class='control-label'>"+ministry+"</label>");
												$('#displayOrgOwnerName').html("<label class='control-label'>"+department+"</label>");
										}
										else if(ownerTypeDesc == "PRIVATE")
										{
											$('#displayOrgOwnerName').html("<label class='control-label'>"+privateName+"</label>");
										}
										else
										$('#displayOrgOwnerName').html("<label class='control-label'>"+ownerName+"</label>");
										$('#displayOrgAddress').html("<label class='control-label'>"+Address+"</label>");
										$('#displayOrgPhone').html("<label class='control-label'>"+phone+"</label>");
										$('#displayOrgDzongkhag').html("<label class='control-label'>"+Dzongkhag+"</label>");
										$('#vehicleTypeName').val(vehicleTypeName);
										$('#vehicleType').val(vehicleType);
										$('#engineType').val(engineType);
										$('#vehicleHorsePower').val(vehicleHorsePower);
										$('#vehicleKiloWatt').val(vehicleKiloWatt);
										$('#loadCapacity').val(loadCapacity);
										$('#seatCapacity').val(seatCapacity);
										$('#engineCC').val(engineCC);
									    $('#displayVehicleNumber').val(vehicleNumber);
									    $('#newDisplayVehicleNumber').val(vehicleNumber);
									    $('#customerID').val(customerId);
									    $('#transferorCustomerId').val(customerId); 
									    $('#manufactureYear').val(manufactureYear);
									    $('#vehicleId').val(vehicleId);
									    $('#newVehicleId').val(vehicleId);
									    $('#ownerName').val(ownerName);
									    $('#model').val(Model);
									    $('#make').val(Company);
									    $('#engineNo').val(engineNumber);
									    $('#chassisNo').val(chasisNumber);
									    $('#ownerType').val(ownerType);
									    $('#vehicleTypeDesc').val(vehicleTypeDesc);
									    $('#vehicleRegistrationCode').val(vehicleRegistrationCode);
									    $('#mainRegion').val(regionId);

										if(ownerType=='Personal')
										    {$('#ownerType').val("P");}
									    else if(ownerType=='Organization')
										    {$('#ownerType').val("O");}
									    if(isInternational == "N")
										{
									    	$('#internationalDIV').hide();
									    	$('#nationalDIV').show();
										}
									    else
										{
									    	$('#internationalDIV').show();
									    	$('#nationalDIV').hide();
										}
								        searchVehicleNumber();
									}
								}
							//newly commented by pramod uchicha
							//}
							//else
						//{
							if(Status=='HAS_OFFENCE')
							{
								$("#Msg").html("<div class='alert alert-danger'>Vehicle Has Offence</div>");
								$("#Msg").show();
								
							}
							else if(Status=='CANCELLED')
							{
								$('#Msg').html("<div class='alert alert-danger'><h4>Sorry! Transaction is not allowed:</h4>This vehicle has been <strong>Cancelled</strong> in our record with effect from <strong>"+cancelledDate+"</strong> based on the the following reason: <strong>"+cancelReason+"</strong> </div>");
								$("#Msg").show();
							} 
							else if(Status == "PENDING_APPLICATION")
							{
								$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
								$('#Msg').show();
							}
							if(Status!="Active")
							{
								if(type!="HISTORY" && type!="OFFENCE" && type!="WITHDRAW_VEH_CANCELLATION")
								{
									return false;
								}
							}
							
							 	var testResult = $(this).find('testResult').text(); 
							 	$("#testResult").val(testResult);
								$('#displayOwnerType').html("<label class='control-label'>"+ownerType+"</label>");
								$('#displayOwnerID').html("<label class='control-label'>"+ownerID+"</label>");
								$('#displayOwnerName').html("<label class='control-label'>"+ownerName+"</label>");
								$('#displayCitizenID').html("<label class='control-label'>"+CitizenID+"</label>");
								$('#displayPhone').html("<label class='control-label'>"+phone+"</label>");  
								$('#displayDzongkhag').html("<label class='control-label'>"+Dzongkhag+"</label>");
								$('#displayGewog').html("<label class='control-label'>"+Gewog+"</label>");
								$('#displayVillage').html("<label class='control-label'>"+Village+"</label>");
								$('#displayCountry').html("<label class='control-label'>"+Country+"</label>");
								$('#displayAddress').html("<label class='control-label'>"+Address+"</label>");
								$('#displayLastRegistrationDate').html("<label class='control-label'>"+registrationDate+"</label>");
								$('#displayExpiryDate').html("<label class='control-label'>"+expiryDate+"</label>");
								$('#displayCompany').html("<label class='control-label'>"+Company+"</label>");
								$('#displayModel').html("<label class='control-label'>"+Model+"</label>");
								$('#displayChasisNumber').html("<label class='control-label'>"+chasisNumber+"</label>");
							    $('#displayEngineNumber').html("<label class='control-label'>"+engineNumber+"</label>");
								$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
								$('#displaySeatCapacity').html("<label class='control-label'>"+seatCapacity+"</label>");
								$('#displayLoadCapacity').html("<label class='control-label'>"+loadCapacity+"</label>");
							    $('#displayRegion').html("<label class='control-label'>"+Region+"</label>");
								$('#displayLastRegistrationDate').html("<label class='control-label'>"+lastIssueDate+"</label>");
							    $('#displayColor').html("<label class='control-label'>"+colorName+"</label>");
						        $('#displayStatus').html("<label class='control-label'>"+Status+"</label>");
						        $('#displayEmail').html("<label class='control-label'>"+Email+"</label>");
						        $('#vehicleType').html("<label class='control-label'>"+vehicleType+"</label>");
								$('#displayVehicleTypeName').html("<label class='control-label'>"+vehicleTypeName+"</label>");
								$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
								$('#displayInitialPrice').html("<label class='control-label'>Nu.&nbsp;"+initialPrice+"</label>");
								$('#displayEngineCC').html("<label class='control-label'>"+engineCC+"</label>");
	
								$('#vehicleTypeName').val(vehicleTypeName);
								$('#vehicleType').val(vehicleType);
								$('#engineType').val(engineType);
								$('#vehicleHorsePower').val(vehicleHorsePower);
								$('#vehicleKiloWatt').val(vehicleKiloWatt);
								$('#loadCapacity').val(loadCapacity);
								$('#seatCapacity').val(seatCapacity);
								$('#engineCC').val(engineCC);
							    $('#displayVehicleNumber').val(vehicleNumber);
							    $('#customerID').val(customerId);
							    $('#transferorCustomerId').val(customerId); 
							    $('#manufactureYear').val(manufactureYear);
							    $('#vehicleId').val(vehicleId);
							    $('#ownerName').val(ownerName);
							    $('#model').val(Model);
							    $('#make').val(Company);
							    $('#engineNo').val(engineNumber);
							    $('#chassisNo').val(chasisNumber);
							    $('#ownerType').val(ownerType);
							    $('#vehicleTypeDesc').val(vehicleTypeDesc);
							    $('#vehicleRegistrationCode').val(vehicleRegistrationCode);
							    $('#mainRegion').val(regionId);

							    $('#displayOrgOwnerType').html("<label class='control-label'>"+owner+"</label>");
								if(ownerTypeDesc == "GOVERNMENT")
								$('#displayOrgAgency').html("<label class='control-label'>"+ministry+"</label>");
								if(ownerTypeDesc == "GOVERNMENT")
								$('#displayOrgOwnerName').html("<label class='control-label'>"+department+"</label>");
								else if(ownerTypeDesc == "PRIVATE")
								$('#displayOrgOwnerName').html("<label class='control-label'>"+privateName+"</label>");
								else
								$('#displayOrgOwnerName').html("<label class='control-label'>"+ownerName+"</label>");
								
								$('#displayOrgAddress').html("<label class='control-label'>"+Address+"</label>");
								$('#displayOrgPhone').html("<label class='control-label'>"+phone+"</label>");
								$('#displayOrgDzongkhag').html("<label class='control-label'>"+Dzongkhag+"</label>");
							    
								if(ownerType=='Personal')
								    {$('#ownerType').val("P");}
							    else if(ownerType=='Organization')
								    {$('#ownerType').val("O");}
	
							    if(isInternational == "N")
								{
							    	$('#internationalDIV').hide();
							    	$('#nationalDIV').show();
								}
							    else
								{
							    	$('#internationalDIV').show();
							    	$('#nationalDIV').hide();
								}
								
						        searchVehicleNumber();
						        $("#viewPersonalDtlsButton").hide();
						        if(personalInfoId!=null && personalInfoId!="null" && personalInfoId!="")
							    {

							        $("#viewPersonalDtlsButton").show();
						        	get_PIS(personalInfoId);
								}
						        
						        get_vehicle_edit_dtls(vehicleNumber,vehicleType);
						}

						//newly commented by pramod uchicha
					//}
					
					
					
					//if(Status=='CANCELLED')
					//{
					//	$('#cancelReason').append("<label>This vehicle has been <strong>Cancelled</strong> in our record with effect from <strong>"+cancelledDate+"</strong> based on the the following reason: <strong>"+cancelReason+"</strong> </label>");
					//	$("#cancelledStatus").show();
					//}
					//else
					if(Status == "PENDING_APPLICATION"  && SEARCH_TYPE!="EMISSION")
					{
						$("#Msg").append("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
						$('#Msg').show();
					}
					if(Status!='CANCELLED' && lastLifeDate!='Invalid' && lastLifeDate < CurrentDate && SEARCH_TYPE!="VEHICLE_CANCELLATION"){
						
						$("#Msg").append("<div class='alert alert-danger'>Sorry! Transaction is not allowed: This Vehicle has reached its age. Validity Date : "+lifeExpiryDate+"</div>");
						$("#Msg").show();
						$("#submitBtn").prop('disabled',true);
					}
					if(Status!='CANCELLED'  && lastLifeDate!='Invalid Date')
					{
						$("#Msg").append("<div class='alert alert-info'>Life Validity Date of this vehicle is : "+lifeExpiryDate+"</div>");
						$("#Msg").show();
					}
					if(type=='HISTORY')
					{
						getVehicleRegSupportingDoc(applicationNo);
					}
				});
			}
		});
	}
	function get_PIS(personalInfoId)
	{	
		$("#originalCID").val('');
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtlsForEdit&personalInfoId='+personalInfoId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
				 	var customerId = $(this).find('customerId').text();
				 	var cid = $(this).find('cid').text();
				 	var titleCourtesy = $(this).find('title-courtesy').text();
				 	var firstName = $(this).find('firstname').text();
				 	var middleName = $(this).find('middlename').text();
				 	var lastName = $(this).find('lastname').text();
				 	var occupation = $(this).find('occupation').text();
				 	var nationality = $(this).find('nationality').text();
				 	var dob = $(this).find('dob').text();
				 	var fatherName = $(this).find('father-name').text();
				 	var bloodGroup = $(this).find('blood-group').text();
				 	var gender = $(this).find('gender').text();
				 	var remarks = $(this).find('remarks').text();
				 	var isInternational = $(this).find('is-international').text();
				 	var dzongkhag = $(this).find('dzongkhag').text();
				 	var gewog = $(this).find('gewog').text();
				 	var village = $(this).find('village').text();
				 	var country = $(this).find('country').text();
				 	var address = $(this).find('address').text();
				 	var presentDzongkhag = $(this).find('present-dzongkhag').text();
				 	var contactAddress = $(this).find('present-address').text();
				 	var phone = $(this).find('phone').text();
				 	var email = $(this).find('email').text();
				 	var personalInfoId = $(this).find('personal-info-id').text();
				 	var imagePath = $(this).find('image-path').text();
				 	var contactRadio = $(this).find('contactRadio').text();
				 	populateDependentDropDown(dzongkhag, 'gewog', '', 'GEWOG_LIST', 'N');

				 	$('#customerID').val(customerId);
				 	$('#cid').val(cid);
				 	$('#titleOfcourtesy').val(titleCourtesy);
				 	$('#firstname').val(firstName);
				 	$('#middlename').val(middleName);
				 	$('#lastName').val(lastName);
				 	$('#personalOccupation').val(occupation);
				 	$('#personalNationality').val(nationality);
				 	$('#displayDob').val(dob);
				 	$('#fathersName').val(fatherName);
				 	$('#bloodGroup').val(bloodGroup);

				 	if(gender == "M")
					 	$('#maleRadio').attr('checked', true);
				 	else
				 		$('#femaleRadio').attr('checked', true);

			 		$('#remarks').val(remarks);

			 		if(isInternational == "N")
		 			{
			 			$('#nationalRadio').attr('checked', true);
			 			enable('National');
			 		}
			 		else
			 		{
			 			$('#internationalRadio').attr('checked', true);
			 			enable('International');
			 		}
			 		if(contactRadio == "Official")
			 		{
				 		$('#officialRadio').attr('checked', true);
				 		$('#ministry').val(ministry);
			 			$('#department').val(department);
			 			enable('Official');
			 		}
			 		else
			 		{
			 			$('#privateRadio').attr('checked', true);
			 			enable('Private');
			 		}

		 			$('#dzongkhag').val(dzongkhag);
		 			$('#gewog').val(gewog);
		 			$('#village').val(village);
		 			$('#country').val(country);

		 			if(address == "" || address == "null")
		 				$('#address').val("");
		 			else
		 				$('#address').val(address);
	 				
		 			$('#presentDzongkhag').val(presentDzongkhag);
		 			$('#contactAddress').val(contactAddress);
		 			$('#phone').val(phone);
		 			$('#email').val(email);
		 			$('#personalInfoId').val(personalInfoId);
		 			$('#msgDiv').html('');

		 			if(imagePath != "null")
			 		{
		 				$('#pic').html('<span class="profile-picture"><img class="editable img-responsive" id="" style="width:150px; height: 150px;" src="<%=request.getContextPath()%>/ImageServlet?url='+imagePath+'"/></span>');
						$('#pic').show();
			 		}
		 			else
			 		{
		 				$('#pic').html('');
						$('#pic').hide();
				 	}

		 			$('#firstname').attr('readonly', false);
		 			$('#middlename').attr('readonly', false);
		 			$('#lastName').attr('readonly', false);
		 			$('#fathersName').attr('readonly', false);
		 			$('#village').attr('readonly', false);
		 			$('#country').attr('disabled', false);
		 			$('#address').attr('disabled', false);
				});
			}
		});
	}
</script>