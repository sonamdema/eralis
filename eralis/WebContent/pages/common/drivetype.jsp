<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	
<logic:notEmpty name="DRIVETYPE_LIST">
	<logic:iterate id="type" name="DRIVETYPE_LIST" indexId="index" type="bt.gov.rsta.eralis.dto.license.DriveTypeDTO">
		<%
			int a = index.intValue();
		%>
		<div class="checkbox">
			<label>
				<logic:equal value="Y" name="type" property="isChecked">
					<input type="checkbox" name="type" id='<%="driveType_"+a %>' class="ace" value="<%=type.getDriveTypeId() %>"/>
					<span class="lbl">
						<bean:write name="type" property="driveType"/>
					</span>
					<script type="text/javascript">
			             $("#driveType_<%=a%>").attr('checked', true);
		            </script>
				</logic:equal>
				<logic:equal value="N" name="type" property="isChecked">
					<input type="checkbox" name="type" id='<%="driveType_"+a %>' class="ace" value="<%=type.getDriveTypeId() %>"/>
					<span class="lbl">
						<bean:write name="type" property="driveType"/>
					</span>
				</logic:equal>
			</label>
		</div>
	</logic:iterate>
</logic:notEmpty>


