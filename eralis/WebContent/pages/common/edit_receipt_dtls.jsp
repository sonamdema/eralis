<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
ServiceDTO dto = (ServiceDTO)request.getAttribute("ServiceDTO"); 
%>
<style>
	.datepicker{z-index:1151 !important;}
</style>
<html:form styleClass="form-horizontal" action="/common.html" styleId="editReceiptDtlsForm">
	<div id="edit_payment_dtls" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
	
				<div class="modal-body">
					
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-large">
									<h3 class="widget-title grey lighter">
										<i class="ace-icon fa fa-money orange"></i>
											Payment Details
									</h3>
								</div>
	
								<div class="widget-body">
									<div class="widget-main padding-24">
										<div>
											<table class="table table-striped table-bordered" id="normal" >
												<tbody>
													<tr>
														<td>Amount:</td>
														<td>
															<div class="input-group">
																<span class="input-group-addon">
																   Nu.
																</span>
																<input type="text" name="amount" id="amount" onchange="calculateTotalEditPayment()" class="form-control input-mask-phone" value="<%=dto.getAmount()%>"/>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
											<table class="table table-striped table-bordered" id="penaltyTR">
												<tbody>
													<tr>
														<td>Penalty:</td>
														<td>
															<div class="input-group">
																<span class="input-group-addon">
																   Nu.
																</span>
																<input type="text" name="penalty" onchange="calculateTotalEditPayment()" class="form-control input-mask-phone" id="penalty"  value="<%=dto.getPenalty()%>"/>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="hr hr8 hr-double hr-dotted"></div>
										<div class="row">
											<div class="col-sm-5 pull-left">
												<h4 class="pull-left">
													Total amount :
												</h4>
											</div>
											<div class="col-sm-5 pull-right">
												<h4 class="pull-right">
													<div class="input-group">
														<span class="input-group-addon">
														   Nu.
														</span>
														<input class="form-control input-mask-phone" type="text" id="totalAmount" readonly="readonly">
													</div>
												</h4>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-4">
									<label>Receipt No<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-6">
									<html:text property="receiptNo" styleId="receiptNo"  value="<%=dto.getReceiptNo()%>" styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-4">
									<label>Receipt Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-6">
									<div class="input-group">
										<html:text property="receiptDate" value="<%=dto.getReceiptDate()%>" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm" data-dismiss="modal" type="button" onClick="editPaymentDtls()">
						<i class="ace-icon fa fa-tick"></i>
						Submit
					</button>
				</div>
				
			</div>
		</div>
	</div>
</html:form>