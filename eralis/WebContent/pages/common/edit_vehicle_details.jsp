 <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
  
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<div id="edit_vehicle_history" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div id="vehicle_edit_form" class="row">
					  
				</div>
			</div>
		</div>
	</div>
</div>
<!-- PAGE CONTENT ENDS -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<style>
	#personalForm .error { color: red; }
</style>
<script>
	function editPersonalInfo()
	{
		$('#dzongkhagHidden').val($('#dzongkhag').val());
		$('#gewogHidden').val($('#gewog').val());
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=edit_personal_info',type:'POST',data: $("#personalForm").serialize()}; 
	    $("#personalForm").ajaxSubmit(options);
	    $('#messageDiv').show();
	    $('#messageDiv').get(0).scrollIntoView();
	    
	    var drivinglicenseId	=	$("#drivinglicenseId").val();
	}
</script>
