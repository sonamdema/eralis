<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="emission-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Test Date</th>
			<th>Valid Till Date</th> 
			<th>Test Location</th> 
			<th>CO Level</th>
			<th>HSU Level</th>
			<th>Result</th>
			<th>Price</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="EMISSION_HISTORY">
			<logic:iterate id="emission" name="EMISSION_HISTORY">
				<tr>
					<td><bean:write name="emission" property="testDate"/></td>
					<td><bean:write name="emission" property="validUpto"/></td>
					<td><bean:write name="emission" property="dzongkhag"/></td>
					<td><bean:write name="emission" property="CO"/></td>
					<td><bean:write name="emission" property="HSU"/></td>
					<td><bean:write name="emission" property="testResult"/></td>
					<td><bean:write name="emission" property="amount"/></td>
					<td>
						<a href="#" class="btn btn-primary btn-sm" onclick="printEmissionReport('<bean:write name="emission" property="emissionId"/>')">
						 	<i class="ace-icon fa fa-print bigger-160"></i>
						 	Print Certificate
					 	</a>
					</td>
					
					
					
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#emission-history-table').DataTable({
	            responsive: true
	    });
	});

</script>