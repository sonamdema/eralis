	<!-- favicon -->
<%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<link rel="shortcut icon" href="<%=request.getContextPath() %>/images/favicon.ico" type="image/x-icon" />

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap-duallistbox.min.css" />
	<!-- text fonts -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/fonts/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

	<!--[if lte IE 9]>
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-part2.min.css" class="ace-main-stylesheet" />
	<![endif]-->

	<!--[if lte IE 9]>
	  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-ie.min.css" />
	<![endif]-->

	<!-- ace settings handler -->
	<script src="<%=request.getContextPath() %>/js/ace-extra.min.js"></script>
	

	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
	<!--[if lte IE 8]>
	<script src="<%=request.getContextPath() %>/js/html5shiv.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- application specific css -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/dataTables.bootstrap.css" rel="stylesheet">
  	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/dataTables.responsive.css" rel="stylesheet">
  	
	<%
	 	String userName = null;
	 	EralisUserRolePriviledge userRolePriv = null;
	 	Role[] roles = null;
	 	String roleCode = null;
	 	if(session.getAttribute(Constants.USER_DETAILS) != null)
	 	{
	 		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
	 		userName = userRolePriv.getUserName();
	 		roles = userRolePriv.getRoles();
	 		
	 		Role currentRole = userRolePriv.getCurrentRole();
			roleCode = currentRole.getRoleCode();
	 	}
	 %>
	 
	 <style type="text/css">
		#idletimeout { background:#0088AA; border:0px solid #969696; color:#fff; font-family:arial, sans-serif; text-align:center; font-size:12px; padding:10px; position:relative; top:0px; left:0; right:0; z-index:100000; display:none; }
		#idletimeout a { color:#fff; font-weight:bold }
		#idletimeout span { font-weight:bold }
	</style>
	
	<body>
		</head>
		<body>
		<div id="idletimeout">
			<strong>You will be logged off in <span><!-- countdown place holder --></span>&nbsp;seconds due to inactivity.</strong>
			<a id="idletimeout-resume" href="#" class="btn btn-success">Stay</a>
			&nbsp;
			<button class="btn btn-warning" onclick="logout()">Leave</button>
		</div>
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>
	 
			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>
	
					<span class="icon-bar"></span>
	
					<span class="icon-bar"></span>
	
					<span class="icon-bar"></span>
				</button>
	
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand" onclick="displayHomePage()">
						<small>
							<img src="<%=request.getContextPath() %>/images/eRaLIS-Logo.png" width="25px" height="25px"/>
							eRaLIS
						</small>
					</a>
				</div>
	
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
					
						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<%
									int total = 0;
									String licenseAlert = (String)request.getAttribute("TOTAL_LICENSE_ALERT");
									String applicationAlert = (String) request.getAttribute("TOTAL_APPLICATION_ALERT");
									total = Integer.parseInt(licenseAlert)+Integer.parseInt(applicationAlert);
								%>
								<span class="badge badge-important"><%=total %></span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									 <%=total %> Notifications
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>
											<a href="#" onclick="loadAlertDetails('LICENSE_PRINT_ALERT')">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-pink fa fa-credit-card"></i>
														License not printed within 3 days
													</span>
													<span class="pull-right badge badge-info"><%=request.getAttribute("TOTAL_LICENSE_ALERT") %></span>
												</div>
											</a>
										</li>
									</ul>
								</li>
								
								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>
											<a href="#" onclick="loadAlertDetails('APPLICATION_ALERT')">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-pink fa fa-users"></i>
														List of applications not approved
													</span>
													<span class="pull-right badge badge-info"><%=request.getAttribute("TOTAL_APPLICATION_ALERT") %></span>
												</div>
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
	
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<%=request.getContextPath() %>/avatars/male.jpg" alt="User's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<%=userName %>
								</span>
	
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
	
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#" onclick="loadPageDetails('<%=request.getContextPath()%>/redirect.html?q=account_setting&page_id=NA')">
										<i class="ace-icon fa fa-cog"></i>
										Settings
									</a>
								</li>
								<%
									if(roles != null && roles.length > 1)
									{
								%>
										<li>
											<a href="#" onclick="loadPageDetails('<%=request.getContextPath()%>/common.html?method=switch_roles')">
												<i class="ace-icon fa fa-gears"></i>
												Switch Roles
											</a>
										</li>
								<%
									}
									if(!roleCode.equalsIgnoreCase("EMISSION_CENTER") && !roleCode.equalsIgnoreCase("OTHERS"))
									{
								%>
									<li>
										<a href="#" onclick="loadPageDetails('<%=request.getContextPath()%>/common.html?method=daily_task_redirect')">
											<i class="ace-icon fa fa-bar-chart-o"></i>
											Daily Task
										</a>
									</li>
								<%
									}
								%>
								<!-- <li>
									<a href="#" onclick="loadFAQ('<%=request.getContextPath()%>/common.html?method=get_faq_list')">
										<i class="ace-icon fa fa-info-circle"></i>
										Help
									</a>
								</li> -->
								<li>
									<a href="https://docs.google.com/forms/d/e/1FAIpQLSdjzTWe7-dyphWlz0kBSCz6v0H6CmlTMgH7gU8H3eE7gwgsBw/viewform" target="_blank">
										<i class="ace-icon fa fa-info-circle"></i>
										Help Desk
									</a>
								</li>
								<li>
									<a href="#" onclick="getUserManual()">
										<i class="ace-icon fa fa-book"></i>
										User Manual
									</a>
								</li>
	
								<li class="divider"></li>
	
								<li>
									<a href="<%=request.getContextPath() %>/logout.html">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
		
		<script>

			function loadFAQ(page)
			{
				$("#contentDisplayDiv").load(page);
				$('#contentDisplayDiv').show();
			}

			function getUserManual()
			{
				window.open("<%=request.getContextPath()%>/user_manual.pdf",'','width=400,height=700');
			}

		</script>
		
	</body>