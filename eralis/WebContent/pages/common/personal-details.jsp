<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="table-responsive">
	<table id="personal-search-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<th>Customer Code</th>
				<th>CID</th>
				<th>Name</th>
				<th>Blood Group</th>
				<th>Region</th>
				<th>Permanent Dzongkhag</th>
				<th>Permanent Gewog</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="PERSONAL_LIST">
				<logic:iterate id="personal" name="PERSONAL_LIST">
					<tr>
						<td>
							<!-- <input type="radio" name="personalListRadio" id='<bean:write name="personal" property="personalInfoId"/>' onclick="addSelected(this.id)"/> -->
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="personal" property="personalInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="personal" property="customerID"/></td>
						<td><bean:write name="personal" property="CID"/></td>
						<td><bean:write name="personal" property="name"/></td>
						<td><bean:write name="personal" property="bloodGroup"/></td>
						<td><bean:write name="personal" property="region"/></td>
						<td><bean:write name="personal" property="dzongkhag"/></td>
						<td><bean:write name="personal" property="gewog"/></td> 
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>
<!-- <div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div> -->

<script>
	$(document).ready(function() 
	{
   		$('#personal-search-table').DataTable({
            responsive: true
    	});
	});
		

	<%
		String searchType = (String)request.getAttribute("SEARCH_TYPE");
	%>
	var searchType = "<%=searchType%>";
	//var globalId = "";
	//function addSelected(id)
	//{
	//	globalId = id;
	//}
								
	function getSelected(globalId)
	{		
		$("#Msg").hide();
		$("#validationMsg").hide();
		
		$("#originalCID").val('');
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtls&personalInfoId='+globalId+'&searchType='+searchType,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{

					$("#message").hide();
					var status=$(this).find('status').text();
					if(status == 'VEHICLE_OUTSTANDING')
					{
						var vehicle_type =  $(this).find('vehicle_type').text(); 
						var vehicleNo = $(this).find('vehicleNo').text();
						$("#message").html("This customer has <b>vehicle outstanding</b> against the vehicle no. : <b>"+vehicleNo+"</b> and vehicle type : <b>"+vehicle_type+"</b>");
						$("#message").show();
					}
					else if(status == 'DRIVING_LICENSE_OUTSTANDING')
					{
						var licenseNO = $(this).find('licenseNO').text();
						$("#message").html("This customer has <b>Driving License outstanding</b> against the Driving License no. : <b>"+licenseNO+"</b>");
						$("#message").show();
					}
					else
					{
						
						$('#displayDzongkhag').html("<label class='control-label'></label>");
						$('#customerID').val('');
						$('#displayBloodgroup').html("<label class='control-label'></label>");
						$('#displayDob').html("<label class='control-label'></label>");
						$('#learnerCustomerId').val('');
						$('#customerID').html("<label class='control-label'></label>");
						$('#displayOwnerName').html("<label class='control-label'></label>");
						$('#displayGewog').html("<label class='control-label'></label>");
						$('#displayAddress').html("<label class='control-label'></label>");
						$('#TransferOwnerName').html("<label class='control-label'></label>");
						$('#TransferDzongkhag').html("<label class='control-label'></label>");
						$('#TransferGewog').html("<label class='control-label'></label>");
						$('#TransferAddress').html("<label class='control-label'></label>");
						$('#citizenID').val('');
						$('#vehicleRegistrationType').val('');
						$('#cid').val(CID);
						$('#titleOfcourtesy').val('');
						$('#name').val(name);
						$('#occupation').val('');
						$('#nationality').val('');
						$('#bloodGroup').val('');
						$('#displayDob').val('');
						$('#fathersName').val('');
						$('#identificationMarks').val('');
						$('#remarks').val('');
						$('#age').val('');
						$('#occupation').html("<label class='control-label'></label>");
						$('#identificationMarks').html("<label class='control-label'></label>");
						$('#gender').html("<label class='control-label'></label>");
						$('#nationality').html("<label class='control-label'></label>");
						$('#bloodgroup').html("<label class='control-label'></label>");
						$('#fathersname').html("<label class='control-label'></label>");
						$('#displayCountry').html("<label class='control-label'></label>");
						$('#cid').html("<label class='control-label'></label>");
						$("#duplicateCID").val('');
						$("#personalInfoId").val('');
						
						
						var vehicleNo = $(this).find('vehicleNo').text();
						var is_international = $(this).find('is_international').text();
						var reason = $(this).find('reason').text();
						var name = $(this).find('name').text();
						var age = $(this).find('age').text();
						var dzongkhag = $(this).find('dzongkhag').text();
						var gewog = $(this).find('gewog').text();
						var address = $(this).find('address').text();
						var titleOfCourtesy = $(this).find('courtesy').text();
						var occupation = $(this).find('occupation').text();
						var customerId = $(this).find('customerId').text();
						var bloodgroup = $(this).find('bloodgroup').text();
						var bloodgroupId = $(this).find('bloodgroupId').text();
						var dob = $(this).find('dob').text();
						var nationality=$(this).find('nationality').text();
						var CID=$(this).find('CID').text();
						var fathersname=$(this).find('fathersname').text();
						var identificationMarks=$(this).find('identificationMarks').text();
						var remarks=$(this).find('remarks').text();
						var gender = $(this).find('gender').text();
						var country = $(this).find('country').text();
						var personalInfoId = $(this).find('personalInfoId').text();
						var village=$(this).find('village').text();
						var learnerApplicationAge=$(this).find('duration').text();
	
						var driveTypeId=$(this).find('driveTypeId').text();
						var Theory_Marks_Obtained = $(this).find('Theory_Marks_Obtained').text();
						var Practical_Marks_Obtained = $(this).find('Practical_Marks_Obtained').text();
						var Issue_Type = $(this).find('Issue_Type').text();
						var testmarks	=	$(this).find('testmarks').text();
						$('#driveTypeTemp').children('option').hide();
						if(is_international=='N' && Issue_Type!="null")
						{
							$("#issueType").val(Issue_Type);
							$("#driveTypeTemp").val(driveTypeId);
							$("#testmarks").val(testmarks);
							$("#testResult").val(status);
							$('#driveTypeTemp').children('option').hide();
							$('#driveTypeTemp value:contains('+driveTypeId+')').show();
							$('#issueType').children('option').hide();
							$('#issueType option:contains('+Issue_Type+')').show();
						}
						else if(is_international=='N' && Issue_Type=="null")
						{
							$('#issueType option:contains(AMC)').show();
							$('#issueType option:contains(VTI)').show();
							$('#issueType option:contains(CDCL)').show();
						}
						else if(is_international=='Y' && Issue_Type!="null")
						{
							$('#issueType option:contains('+Issue_Type+')').show();
						}
						else if(is_international=='Y' && Issue_Type=="null")
						{
							$('#issueType option:contains(International_Driving_License)').show();
						}
											
						if(country=='null')
						{
							country='';
						}
						$("#submitBtn").prop('disabled', false);
						$('#msgDiv').hide();
						 if(status == "PENDING_APPLICATION")
						{
							$("#submitBtn").prop('disabled', true);
							$("#msgDiv").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+reason+"</div>");
							$('#msgDiv').show();
							$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+reason+"</div>");
							$('#Msg').show();
						}
						else if(searchType == "LEARNER_NEW")
						{
							
							if(status == "ISSUED")
							{
								$("#Msg").html("<div class='alert alert-danger'>Learner is already issued or already applied against this Customer</div>");
								$('#Msg').show();
							}
							else if(status == "UNDER_AGE")
							{
								$("#Msg").html("<div class='alert alert-danger'>Sorry. Applicant is under age</div>");
								$('#Msg').show();
							}
							else
							{
								$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
								$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
								$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
								$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
								$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
								$('#TransferOwnerName').html("<label class='control-label'>"+name+"</label>");
								$('#TransferDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
								$('#TransferGewog').html("<label class='control-label'>"+gewog+"</label>");
								$('#TransferAddress').html("<label class='control-label'>"+address+"</label>");
								$('#displayCID').html("<label class='control-label'>"+CID+"</label>");
								$('#citizenID').val(customerId);
								$('#vehicleRegistrationType').val("P");
								$('#cid').val(CID);
								
								//personal information
								$('#customerID').val(customerId);
								$('#titleOfcourtesy').val(titleOfCourtesy);
								$('#name').val(name);
								$('#occupation').val(occupation);
								$('#nationality').val(nationality);
								//$('#cid').val(CID);
								$('#bloodGroup').val(bloodgroupId);
								$('#displayDob').val(dob);
								$('#fathersName').val(fathersname);
								$('#identificationMarks').val(identificationMarks);
								$('#remarks').val(remarks);
								$('#age').val(age);
								
								if(gender == "M")
									$('#maleRadio').attr('checked', true);
								else if(gender == "F")
									$('#femaleRadio').attr('checked', true);
								
								//new learner license
								$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
								$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
								$('#learnerCustomerId').val(customerId);
							
								//replaceCID
								$('#displayDob').val(CID);
			
								//non-commercial
								$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
								$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
								$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
								$('#gender').html("<label class='control-label'>"+gender+"</label>");
								$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
								$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
								$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
								$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
								
			
								/*TOOLS START*/
								$('#cid').html("<label class='control-label'>"+CID+"</label>");
								$("#duplicateCID").val(CID);
								$("#personalInfoId").val(personalInfoId);
								/*TOOLS END*/
			
								$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
			
								if (age < learnerApplicationAge)
								{
									$('#validationMsg').html("<div class='alert alert-danger'>You are underage to apply for learner license</div>");
									$('#validationMsg').show();
							       //setTimeout('hideStatus("validationMsg")',3000);
								}
							}
						}
						else if((searchType == "REPLACE_CID") || (searchType == "VEHICLE_REGISTRATION") || (searchType == "VEHICLE_TRANSFER"))
						{
							//vehicle registration
							$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#TransferOwnerName').html("<label class='control-label'>"+name+"</label>");
							$('#TransferDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#TransferGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#TransferAddress').html("<label class='control-label'>"+address+"</label>");
							$('#citizenID').val(customerId);
							$('#vehicleRegistrationType').val("P");
							$('#cid').val(CID);
							
							if(searchType == "VEHICLE_REGISTRATION")
							{
								checkPersonalOutstanding(globalId);
							}
							
							//personal information
							$('#customerID').val(customerId);
							$('#titleOfcourtesy').val(titleOfCourtesy);
							$('#name').val(name);
							$('#occupation').val(occupation);
							$('#nationality').val(nationality);
							//$('#cid').val(CID);
							$('#bloodGroup').val(bloodgroupId);
							$('#displayDob').val(dob);
							$('#fathersName').val(fathersname);
							$('#identificationMarks').val(identificationMarks);
							$('#remarks').val(remarks);
							$('#age').val(age);
							
							if(gender == "M")
								$('#maleRadio').attr('checked', true);
							else if(gender == "F")
								$('#femaleRadio').attr('checked', true);
								
							//new learner license
							$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
							$('#learnerCustomerId').val(customerId);
							
							//replaceCID
							$('#displayDob').val(CID);
		
							//non-commercial
							$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
							$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
							$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
							$('#gender').html("<label class='control-label'>"+gender+"</label>");
							$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							
		
							/*TOOLS START*/
							$('#cid').html("<label class='control-label'>"+CID+"</label>");
							$("#duplicateCID").val(CID);
							$("#personalInfoId").val(personalInfoId);
							/*TOOLS END*/
		
							$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
		
							if (age < learnerApplicationAge)
							{
								$('#validationMsg').html("<div class='alert alert-danger'>You are underage to apply for learner license</div>");
								$('#validationMsg').show();
							}
						}
						else 
						{
							if(status == "ISSUED")
							{
								$("#Msg").html("<div class='alert alert-danger'>License is already issued or already applied against this Customer</div>");
								$('#Msg').show();
							}
							else
							{
								$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
								$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
								$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
								$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
								$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
								$('#TransferOwnerName').html("<label class='control-label'>"+name+"</label>");
								$('#TransferDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
								$('#TransferGewog').html("<label class='control-label'>"+gewog+"</label>");
								$('#TransferAddress').html("<label class='control-label'>"+address+"</label>");
								$('#displayCID').html("<label class='control-label'>"+CID+"</label>");
								$('#citizenID').val(customerId);
								$('#vehicleRegistrationType').val("P");
								$('#cid').val(CID);
								
								//personal information
								$('#customerID').val(customerId);
								$('#titleOfcourtesy').val(titleOfCourtesy);
								$('#name').val(name);
								$('#occupation').val(occupation);
								$('#nationality').val(nationality);
								//$('#cid').val(CID);
								$('#bloodGroup').val(bloodgroupId);
								$('#displayDob').val(dob);
								$('#fathersName').val(fathersname);
								$('#identificationMarks').val(identificationMarks);
								$('#remarks').val(remarks);
								$('#age').val(age);
								
								if(gender == "M")
									$('#maleRadio').attr('checked', true);
								else if(gender == "F")
									$('#femaleRadio').attr('checked', true);
								
								//new learner license
								$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
								$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
								$('#learnerCustomerId').val(customerId);
							
								//replaceCID
								$('#displayDob').val(CID);
			
								//non-commercial
								$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
								$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
								$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
								$('#gender').html("<label class='control-label'>"+gender+"</label>");
								$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
								$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
								$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
								$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
			
								/*TOOLS START*/
								$('#cid').html("<label class='control-label'>"+CID+"</label>");
								$("#duplicateCID").val(CID);
								$("#personalInfoId").val(personalInfoId);
							}
						}
					}
				});
			}
		});
	}
</script>
