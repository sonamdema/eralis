<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Select all</th>
			<th>Vehicle Number</th>
			<th>Vehicle Type</th> 
			<th>Owner Name</th> 
			<th>Owner Type</th>
			<th>Engine Number</th> 
			<th>Chasis Number</th>  
			
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="STATUS">
			<logic:iterate id="status" name="STATUS">
				<tr>
					<td>
						<input type="radio" name="statusHistoryRadio" id='<bean:write name="status" property="selectall"/>' onclick="addSelected(this.id)"/>
					</td>
					<td><bean:write name="status" property="vehicleNo"/></td>
					<td><bean:write name="status" property="vehicleType"/></td>
					<td><bean:write name="status" property="name"/></td>
					<td><bean:write name="status" property="vehicleRegistrationType"/></td> 
					<td><bean:write name="status" property="engineNumber"/></td>
					<td><bean:write name="status" property="chasisNumber"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="STATUS">
			<tr>
				<td colspan="7" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>



