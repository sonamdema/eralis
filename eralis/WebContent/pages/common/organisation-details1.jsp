<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
	<table id="organization-data-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<th>Customer Id</th>
				<th>Name</th>
				<th>Region</th>
				<th>Dzongkhag</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="ORGANISATION_LIST1">
				<logic:iterate id="organisation1" name="ORGANISATION_LIST1">
					<tr>
						<td>
							<!-- <input type="radio" name="organisationListRadio1" id='<bean:write name="organisation1" property="organizationInfoId"/>' onclick="addSelected(this.id)"/> -->
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="organisation1" property="organizationInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="organisation1" property="customerID"/></td>
						<td><bean:write name="organisation1" property="name"/></td>
						<td><bean:write name="organisation1" property="region"/></td>
						<td><bean:write name="organisation1" property="dzongkhag"/></td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>
<div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>

<script>
	$(document).ready(function() 
	{
   		$('#organization-data-table').DataTable({
            responsive: true
    	});
	});
		
	//var globalId = "";
	//function addSelected(id)
	//{
	//	globalId = id;
	//}

	function getSelected(globalId)
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOrganisationDtls&organisationInfoId='+globalId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var name = $(this).find('name').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					//var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var region = $(this).find('region').text();
					//var phone = $(this).find('phone').text();
					//var email=$(this).find('email').text();
					//var remarks=$(this).find('remarks').text();
					var customerId = $(this).find('customerId').text();

					var organisationInfoId = $(this).find('organisationInfoId').text();

					$('#transferOwnerName').html("<label class='control-label'>"+name+"</label>");
					$('#transferDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
					$('#transferRegion').html("<label class='control-label'>"+region+"</label>");
					$('#transferAddress').html("<label class='control-label'>"+address+"</label>");


					$('#transfereeCustomerId1').val(customerId);
					$('#transfereeCustomerId').val(customerId);
					$('#ownerID').val(customerId);
					$('#vehicleRegistrationType').val("O");
					$("#organisationInfoId").val(organisationInfoId);

					
					
				});
			}
		});
	}

</script>