<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
ServiceDTO dto = (ServiceDTO)request.getAttribute("ServiceDTO");

%>
<div class="widget-box">
			 <div class="widget-header widget-header-small">
				  <h5 class="widget-title lighter">Vehicle Registration Details</h5>
			</div>						
		      <div class="widget-body">
			    <div class="widget-main">
						<div class="form-group ">
							<div class="col-lg-2">
								<label>Registration Type<span style="color: #ff0000">*</span>:</label>
							</div>	
		                   
                              <div class="col-lg-3">
									       <html:select property="registrationType" styleClass="form-control" styleId="registrationType">
										      <html:option value="">--SELECT--</html:option>
								   		      <html:optionsCollection name="registrationTypeList" label="headerName" value="headerId"/>
								          </html:select>
                               </div> 
                              <div class="col-lg-2">
								  <label>Status <span style="color: #ff0000">*</span>:</label>
					           </div>	
		                      <div class="col-sm-3"> 	
		                      	<input type="text" value="Active" class="form-control"  disabled="disabled">
								<html:hidden property="status"  value="1"></html:hidden>
							  </div>
		                 </div>
		                <div class="form-group">
							<div class="col-lg-2">
								<label>Vehicle Rgn. Code<span style="color: #ff0000">*</span>:</label>
							</div>	
	                       <div class="col-lg-3">
	                               <html:select property="vehicleRegistrationId" styleClass="form-control" onchange="changeAttribute(this.value,'vehicleRegionId')" styleId="vehicleRegistrationId">
										<html:option value="">--SELECT--</html:option>
								   		<html:optionsCollection name="vehicleCodeList" label="headerName" value="headerId"/>
								   </html:select>
                            </div>
                             <div class="col-lg-2">
                                    <label>Purchase Date<span style="color: #ff0000">*</span>:</label>
                                </div>	
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="purchaseDate" styleClass="form-control date-picker" styleId="purchaseDate"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
							 </div>
	                    </div>
		                <div class="form-group ">
							<div class="col-lg-2">
								<label>Vehicle Type<span style="color: #ff0000">*</span>:</label>
							</div>	
			                <div class="col-lg-3">
				            	<html:select property="vehicleType" styleClass="form-control" styleId="vehicleType" onchange="onChangeVehicleType()">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
							   </html:select>
	                        </div> 
	                        <div id="busTypeDiv">
		                        <div class="col-lg-2">
									<label>Bus Type<span style="color: #ff0000">*</span>:</label>
								</div>	
				                <div class="col-lg-3">
					            	<html:select property="busType" styleId="busType" styleClass="form-control">
										<html:option value="0">--SELECT--</html:option>
										<html:optionsCollection name="busTypeList" label="headerName" value="headerId"/>
									</html:select>
		                        </div> 
	                        </div>
		                </div>
		                <div class="form-group">
		                  <div class="col-lg-2">
								<label>Vehicle Company<span style="color: #ff0000">*</span>:</label>
							</div>	
		                       <div class="col-lg-3">
			                       <html:select property="vehicleCompany" styleClass="form-control" styleId="vehicleCompany" onchange="populateDependentDropDown(this.value, 'vehicleModel', '', 'VECHILE_MODEL_LIST', 'N')">
											<html:option value="">--SELECT--</html:option>
									   		<html:optionsCollection name="vehicleCompanyList" label="headerName" value="headerId"/>
								 </html:select>
                              </div> 
		                     <div class="col-lg-2">
								<label>Vehicle Model<span style="color: #ff0000">*</span>:</label>
							 </div>	
		                       <div class="col-lg-3">
			                       <html:select property="vehicleModel" styleClass="form-control" styleId="vehicleModel">
											<html:option value="">--SELECT--</html:option>
								 </html:select>
                               </div> 
		                  </div>
		                <div class="form-group">
		                	<div class="col-lg-2">
								<label>Engine Type<span style="color: #ff0000">*</span>:</label>
							</div>	
                            <div class="col-lg-3">
			                	<html:select property="engineType" styleClass="form-control" styleId="engineType" onchange="onChangeVehicleType()">
									<html:option value="">--SELECT--</html:option>
							   		<html:optionsCollection name="engineTypeList" label="headerName" value="headerId"/>
							   	</html:select>
                            </div> 
                            <div style="display:none;">
			                	<div class="col-lg-2" >
									<label>Vanity Number<span style="color: #ff0000">*</span>:</label>
						        </div>	
		                        <div class="col-lg-3">
	                            	<html:text property="vanityNumber"  styleClass="form-control" styleId="vanityNumber"></html:text>
	                            </div>
	                    	</div>
	                    	<div class="col-lg-2">
								<label>Engine/Motor No<span style="color: #ff0000">*</span>:</label>
					     	</div>	
                            <div class="col-sm-3"> 	
								<html:text property="engineNumber" styleClass="form-control" styleId="engineNumber"></html:text> 
						   	</div>
		             	</div>
		                <div class="form-group">
		             		<div class="col-lg-2">
								<label>Chasis Number<span style="color: #ff0000">*</span>:</label>
							</div>	
                           	<div class="col-sm-3"> 	
								<html:text property="chasisNumber" styleClass="form-control" styleId="chasisNumber"></html:text> 
							</div>
							<div class="col-lg-2">
								<label>Dealers Name<span style="color: #ff0000">*</span>:</label>
				           	</div>	
		                  	<div class="col-sm-3"> 	
						       <html:select property="dealersName" styleClass="form-control" styleId="dealersName">
							      <html:option value="">--SELECT--</html:option>
					   		      <html:optionsCollection name="dealersNameList" label="headerName" value="headerId"/>
					          </html:select>
							</div>
		          		</div>
		            	<div class="form-group">
		            		<div id="loadCapacityDiv" style="display: none;">
			                	<div class="col-lg-2">
									<label>Gross Vehicle Weight<span style="color: #ff0000">*</span>:</label>
								</div>	
		                        <div class="col-sm-3"> 	
									<html:text property="loadCapacity" value="0.00" styleClass="form-control" styleId="loadCapacity"></html:text> 
								</div>
							</div>
		                	<div id="vehicleHorsePowerDiv" style="display: none;">
			                	<div class="col-lg-2">
									<label>Horse Power<span style="color: #ff0000">*</span>:</label>
								</div>	
		                        <div class="col-sm-3"> 	
									<html:text property="vehicleHorsePower" styleClass="form-control" value="0.00" styleId="vehicleHorsePower"></html:text> 
								</div>
							</div>
							<div id="vehicleKiloWattDiv" style="display: none;">
		                      	<div class="col-lg-2">
							 		<label>KiloWatt<span style="color: #ff0000">*</span>:</label>
						      	</div>	
	                            <div class="col-sm-3"> 	
									<html:text property="vehicleKiloWatt" styleClass="form-control" value="0.00" styleId="vehicleKiloWatt"></html:text> 
								</div>
							</div>
							<div id="engineCCDiv" style="display: none;">
		               			<div class="col-lg-2" >
								     <label>Engine CC<span style="color: #ff0000">*</span>:</label>
							    </div>	
	                            <div class="col-sm-3"> 	
									<html:text property="engineCC" styleClass="form-control" styleId="engineCC"></html:text> 
								</div>
							</div>
							<div id="unladenWeightDiv" style="display: none;">
			                   	<div class="col-lg-2">
									 <label>Unladen Weight<span style="color: #ff0000">*</span>:</label>
							  	</div>	
	                              	<div class="col-sm-3"> 	
							     	<html:text property="unladenWeight" styleClass="form-control" styleId="unladenWeight"></html:text> 
						     	</div>
						     </div>
	                   	</div>
		                  <div class="form-group">
		                  	   <div class="col-lg-2">
							 		<label> Seat Capacity<span style="color: #ff0000">*</span>:</label>
							   	</div>	
		                       	<div class="col-sm-3"> 	
									<html:text property="seatCapacity" styleClass="form-control" styleId="seatCapacity"></html:text> 
								</div>
		                       <div class="col-lg-2">
								   <label>Colour<span style="color: #ff0000">*</span>:</label>
							   </div>	
	                                <div class="col-sm-3"> 	
									   <html:text property="colour" styleClass="form-control" styleId="colour"></html:text> 
								    </div>
		                   </div>
		                  <div class="form-group">
		                  	<div class="col-lg-2">
								<label>Price<span style="color: #ff0000">*</span>:</label>
					        </div>	
	                        <div class="col-sm-3"> 	
							  <html:text property="price" styleClass="form-control" styleId="price"></html:text> 
						    </div>
		                    <div class="col-lg-2">
								<label>Manufacture Year<span style="color: #ff0000">*</span>:</label>
						    </div>	
                            <div class="col-sm-3"> 	
								<html:text property="manufactureYear" styleClass="form-control" styleId="manufactureYear"></html:text> 
						    </div>
		                  </div>
		                <div class="form-group">
		                	<div class="col-lg-2">
								<label>Manufacture Country<span style="color: #ff0000">*</span>:</label>
					        </div>	
	                        <div class="col-sm-3"> 	
							  <html:select property="manufactureCountry" styleClass="form-control" styleId="manufactureCountry">
							      <html:option value="">--SELECT--</html:option>
					   		      <html:optionsCollection name="manufactureCountryList" label="headerName" value="headerId"/>
					          </html:select>
						    </div>
						    <div class="col-lg-2">
							    <label>Purchase Type:</label>
				          	</div>	
	                        <div class="col-sm-3"> 	
						       <html:select property="purchaseType" styleClass="form-control" styleId="purchaseType">
							   <html:option value="">-----SELECT-----</html:option>
							   <html:option value="1">Normal</html:option>
                                  <html:option value="2">Imported</html:option>
							   </html:select>
						    </div>
                    	</div>
                 </div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Attachments</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label> Invoice <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="invoice" styleId="invoice" styleClass="form-control fileupload"></html:file>
								</div>
								<div class="col-lg-2">
									<label> Challan <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="challan" styleId="challan" styleClass="form-control fileupload"></html:file>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label> Letter of Authenticity <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="letterOfAuthenticity" styleId="letterOfAuthenticity" styleClass="form-control fileupload"></html:file>
								</div>
								<div class="col-lg-2">
									<label> Emission <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="emission" styleId="emission" styleClass="form-control fileupload"></html:file>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label> Exemption Certificate <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="exemptionCertificate" styleId="exemptionCertificate" styleClass="form-control fileupload"></html:file>
								</div>
								<div class="col-lg-2">
									<label> Excise Invoice <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="exciseInvoice" styleId="exciseInvoice" styleClass="form-control fileupload"></html:file>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label> Custom Declaration <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:file property="customDeclaration" styleId="customDeclaration" styleClass="form-control fileupload"></html:file>
								</div>
							</div>
						  </div>
					   </div>
				     </div>
		         </div>
	         </div>
			<div id="registrationMsgDiv"></div>
			<div class="pull-left">
				<html:hidden property="customerId" styleId="customerID"></html:hidden>
				<html:hidden property="vehicleRegistrationType" styleId="vehicleRegistrationType"></html:hidden>
       	   </div>
<script><!--
	populateDependentDropDown('<%=dto.getVehicleCompany()%>', 'vehicleModel', '', 'VECHILE_MODEL_LIST', 'N');
	$("#vehicleType").val('<%=dto.getVehicleType()%>');
	$("#chasisNumber").val('<%=dto.getChasisNumber()%>');
	$("#purchaseDate").val('<%=dto.getPurchaseDate()%>');
	$("#registrationType").val('<%=dto.getRegistrationType()%>');
	$("#amount").val('<%=dto.getAmount()%>');

	$("#busType").val('<%=dto.getBusType()%>');
	$("#vehicleRegistrationId").val('<%=dto.getVehicleRegistrationId()%>');
	$("#vehicleRegionId").val('<%=dto.getRegionId()%>');
	$("#vehiclePrefix").val('<%=dto.getVehiclePrefix()%>');
	$("#vanityNumber").val('<%=dto.getVanityNumber()%>');
	$("#vehicleCompany").val('<%=dto.getVehicleCompany()%>');
	$("#vehicleModel").val('<%=dto.getVehicleModel()%>');
	$("#engineType").val('<%=dto.getEngineType()%>');
	$("#receiptDate").val('<%=dto.getReceiptDate()%>');
	$("#engineNumber").val('<%=dto.getEngineNumber()%>');
	$("#receiptNo").val('<%=dto.getReceiptNo()%>');
	$("#status").val('<%=dto.getStatus()%>');
	$("#colour").val('<%=dto.getColour()%>');
	$("#price").val('<%=dto.getPrice()%>');
	$("#dealersName").val('<%=dto.getDealersName()%>');
	$("#seatCapacity").val('<%=dto.getSeatCapacity()%>');
	$("#manufactureYear").val('<%=dto. getManufactureYear()%>');
	$("#manufactureCountry").val('<%=dto. getManufactureCountry()%>');
	$("#engineCC").val('<%=dto. getEngineCC()%>');
	$("#purchaseType").val('<%=dto.getPurchaseType()%>');
	
	var vehicleType	=	$("#vehicleType :selected").text();
	var engineType	=	$("#engineType :selected").text();
	if(vehicleType=='Heavy Vehicle' || vehicleType=='Medium Vehicle')
	{
		$('#loadCapacityDiv').show();
		$('#unladenWeightDiv').show();
	}
	else if(vehicleType=='Tractor')
	{
		$('#vehicleHorsePowerDiv').show();
	}
	else if(vehicleType=='Heavy Bus' || vehicleType=='Medium Bus')
	{
		$("#busTypeDiv").show();
	}
	else
	{
		$("#busTypeDiv").hide();
	}
	
	if(vehicleType == 'Light Vehicle' && engineType == 'Diesel')
	{
		$('#engineCCDiv').show();
	}
	else if(vehicleType == 'Light Vehicle' && engineType == 'Electric')
	{
		$('#vehicleKiloWattDiv').show();
	}
	else if(vehicleType == 'Light Vehicle' && engineType=='Hybrid')
	{
		$('#engineCCDiv').show();
	}
	else if(vehicleType == 'Light Vehicle' && engineType=='Petrol')
	{
		$('#engineCCDiv').show();
	}
	
	
	function onChangeVehicleType()
	{
		var vehicleType	=	$("#vehicleType :selected").text();
		var engineType	=	$("#engineType :selected").text();
		
		$('#loadCapacity').val('0.00');
		$('#loadCapacityDiv').hide();
		$('#vehicleHorsePower').val('0');
		$('#vehicleHorsePowerDiv').hide();
		$('#vehicleKiloWatt').val('0');
		$('#vehicleKiloWattDiv').hide();
		$('#engineCC').val('0');
		$('#engineCCDiv').hide();
		$('#unladenWeight').val('0');
		$('#unladenWeightDiv').hide();
		 
		if(vehicleType=='Heavy Vehicle' || vehicleType=='Medium Vehicle')
		{
			$('#loadCapacityDiv').show();
			$('#loadCapacity').val('');
			$('#unladenWeight').val('');
			$('#unladenWeightDiv').show();
			$("#busTypeDiv").hide();
			$("#busType").val('0');
		}
		else if(vehicleType=='Tractor')
		{
			$('#vehicleHorsePowerDiv').show();
			$('#vehicleHorsePower').val('');
			$("#busTypeDiv").hide();
			$("#busType").val('0');
		}
		else if(vehicleType=='Heavy Bus' || vehicleType=='Medium Bus')
		{
			$("#busTypeDiv").show();
		}
		else
		{
			$("#busTypeDiv").hide();
			$("#busType").val('0');
		}
		
		if(vehicleType == 'Light Vehicle' && engineType == 'Diesel')
		{
			$('#engineCCDiv').show();
			$('#engineCC').val('');
		}
		else if(vehicleType == 'Light Vehicle' && engineType == 'Electric')
		{
			$('#vehicleKiloWattDiv').show();
			$('#vehicleKiloWatt').val('');
		}
		else if(vehicleType == 'Light Vehicle' && engineType=='Hybrid')
		{
			$('#engineCCDiv').show();
			$('#engineCC').val('');
		}
		else if(vehicleType == 'Light Vehicle' && engineType=='Petrol')
		{
			$('#engineCCDiv').show();
			$('#engineCC').val('');
		}
		
	}
	function engineTypeOnChange()
	{	
		var engineType	=	$('#engineType option:selected').text();
		var vehicleType	=	$('#vehicleType option:selected').text();
	
	
		if(vehicleType=='Tractor'){
			if(engineType=='Electric' || engineType=='Hybrid'){
				 $("#engineCC").prop("readonly", true);
				 $("#vehicleHorsePower").prop("readonly", false);
				 $("#vehicleKiloWatt").prop("readonly", false);
			}
			else{
				 $("#engineCC").prop("readonly", false);
				 $("#vehicleHorsePower").prop("readonly", false);
				 $("#vehicleKiloWatt").prop("readonly", true);
			}
		}
		else{
			if(engineType=='Electric' || engineType=='Hybrid'){
				 $("#engineCC").prop("readonly", true);
				 $("#vehicleHorsePower").prop("readonly", true);
				 $("#vehicleKiloWatt").prop("readonly", false);
			}
			else{
				 $("#engineCC").prop("readonly", false);
				 $("#vehicleHorsePower").prop("readonly", true);
				 $("#vehicleKiloWatt").prop("readonly", true);
			}
		}
		
	}
	var engineType	=	$('#engineType option:selected').text();
	var vehicleType	=	$('#vehicleType option:selected').text();
	
	if(vehicleType=='Tractor'){
		if(engineType=='Electric' || engineType=='Hybrid'){
			 $("#engineCC").prop("readonly", true);
			 $("#vehicleHorsePower").prop("readonly", false);
			 $("#vehicleKiloWatt").prop("readonly", false);
		}
		else{
			 $("#engineCC").prop("readonly", false);
			 $("#vehicleHorsePower").prop("readonly", false);
			 $("#vehicleKiloWatt").prop("readonly", true);
		}
	}
	else{
		if(engineType=='Electric' || engineType=='Hybrid'){
			 $("#engineCC").prop("readonly", true);
			 $("#vehicleHorsePower").prop("readonly", true);
			 $("#vehicleKiloWatt").prop("readonly", false);
		}
		else{
			 $("#engineCC").prop("readonly", false);
			 $("#vehicleHorsePower").prop("readonly", true);
			 $("#vehicleKiloWatt").prop("readonly", true);
		}
	}
</script>