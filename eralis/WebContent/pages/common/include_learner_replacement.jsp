<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Learner License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Learner License No:</label>
								</div>
								<div class="col-lg-4" ><%=dto.getLicenseNo()%></div>
								 
							</div>
							    
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-4"><%=dto.getRegion()%></div>
								<div class="col-lg-2">
									<label class="control-label">Receipt Number:</label>
								</div>
								<div class="col-lg-4"><%=dto.getReceiptNo()%></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Receipt Date:</label>
								</div>
								<div class="col-lg-4"><%=dto.getReceiptDate()%></div>
								<div class="col-lg-2">
									<label class="control-label">Remarks:</label>
								</div>
								<div class="col-lg-4"><%=dto.getRemarks()%></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Submission Date:</label>
								</div>
								<div class="col-lg-4"><%=dto.getAppsubmissiondate()%></div>
							</div> 
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Last Expiry Date:</label>
								</div>
								<div class="col-lg-4"><%=dto.getExpiryDate()%></div>
								<div class="col-lg-2">
									<label class="control-label">Next Expiry Date:</label>
								</div>
								<div class="col-lg-4"><%=dto.getNextExpiry()%></div>
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" value="<%=dto.getRemarks()%>" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>