<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">TOP Application Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">License Number:</label>
					</div>
					<div class="col-lg-4" ><label class="control-label"><%=dto.getLicenseNo()%></label></div>
					<div class="col-lg-2">
						<label class="control-label">Vehicle Number:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNo()%></label></div>
				</div>
				
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">TOP Number:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getTopNo()%></label></div>
					<div class="col-lg-2">
						<label class="control-label">Region:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getRegion()%></label></div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Dzongkhag:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag()%></label></div>
					<div class="col-lg-2">
						<label class="control-label">Exact Location:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getExactLocation()%></label></div>
				</div>	
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Receipt Number:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo()%></label></div>
					<div class="col-lg-2">
						<label class="control-label">Receipt Date:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getReceiptDate()%></label></div>
				</div>	
				<div class="form-group">
					<div class="col-lg-2">
						<label class="control-label">Amount Paid:</label>
					</div>
					<div class="col-lg-4"><label class="control-label"><%=dto.getAmount()%></label></div>
				</div>	
	              </div>      
	                </div>
	           </div>