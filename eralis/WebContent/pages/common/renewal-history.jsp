<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="renewal-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Sl</th>
			<th>Renewal Date</th>
			<th>Expiry Date</th> 
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			<th>Supporting Doc</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_HISTORY">
			<% int i =1; %>
			<logic:iterate id="renewal" name="RENEWAL_HISTORY">
				<tr>
					<td><%=i++ %></td>
					<td><bean:write name="renewal" property="renewalDate"/></td>
					<td><bean:write name="renewal" property="expiryDate"/></td>
					<td><bean:write name="renewal" property="receiptNo"/></td> 
					<td><bean:write name="renewal" property="receiptDate"/></td> 
					<td>
						 <logic:notEmpty name="renewal" property="fileUUID">
					        <a href="#" onclick="downloadFile('<bean:write name="renewal" property="fileUUID" filter="false"/>','<bean:write name="renewal" property="fileName" filter="false"/>')">
	  							<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="renewal" property="fileName" filter="false"/>
					    </logic:notEmpty>
	  				</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#renewal-history-table').DataTable({
	            responsive: true
	    });
	});

</script>