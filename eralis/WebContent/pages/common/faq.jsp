<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="page-header">
	<h1>
		FAQ
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Frequently Asked Questions
		</small>
	</h1>
	<div class="pull-right">
		<a href="" class="btn btn-app btn-success btn-xs" target="_blank">
			<i class="ace-icon fa fa-refresh bigger-100"></i>
			Help-Desk
		</a>
		<a href="#" class="btn btn-app btn-warning btn-xs" target="_blank">
			<i class="ace-icon fa fa-refresh bigger-100"></i>
			Manual
		</a>
	</div>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="tabbable">
			<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#faq-tab-1">
						<i class="blue ace-icon fa fa-question-circle bigger-120"></i>
						General
					</a>
				</li>
	
				<li>
					<a data-toggle="tab" href="#faq-tab-2">
						<i class="orange ace-icon fa fa-credit-card bigger-120"></i>
						Payments
					</a>
				</li>
	
			</ul>
			
			<div class="tab-content no-border padding-24">
				<div id="faq-tab-1" class="tab-pane fade in active">
					<h4 class="blue">
						<i class="ace-icon fa fa-check bigger-110"></i>
						General Questions
					</h4>
	
					<div class="space-8"></div>
					
					<logic:notEmpty name="FAQ_LIST">
						<logic:iterate id="faqG" name="FAQ_LIST" indexId="index" type="bt.gov.rsta.eralis.dto.faq.FaqDTO">
							<%
								int a = index.intValue();
							%>
							<logic:equal value="G" name="faqG" property="faqType">
								<div id="faq-list-<%=a %>" class="panel-group accordion-style1 accordion-style2">
									<div class="panel panel-default">
									
										<div class="panel-heading">
											<a href="#faq-<%=a %>" data-parent="#faq-list-<%=a %>" data-toggle="collapse" class="accordion-toggle collapsed">
												<i class="ace-icon fa fa-chevron-left pull-right" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>
												<i class="ace-icon fa fa-plus smaller-80" data-icon-hide="ace-icon fa fa-minus" data-icon-show="ace-icon fa fa-plus"></i>&nbsp; 
												<%=faqG.getFaqTitle() %>
											</a>
										</div>
										
										<div class="panel-collapse collapse" id="faq-<%=a%>">
											<div class="panel-body">
												<%=faqG.getFaqDesc() %>
											</div>
										</div>
										
									</div>
								</div>
							</logic:equal>
						</logic:iterate>
					</logic:notEmpty>
					
				</div>
				
				<div id="faq-tab-2" class="tab-pane fade">
					<h4 class="blue">
						<i class="orange ace-icon fa fa-credit-card bigger-110"></i>
						Payment Questions
					</h4>
	
					<div class="space-8"></div>
					
					<logic:notEmpty name="FAQ_LIST">
						<logic:iterate id="faqP" name="FAQ_LIST" indexId="index" type="bt.gov.rsta.eralis.dto.faq.FaqDTO">
							<%
								int b = index.intValue();
							%>
							<logic:equal value="P" name="faqP" property="faqType">
								<div id="faq-list-<%=b %>" class="panel-group accordion-style1 accordion-style2">
									<div class="panel panel-default">
									
										<div class="panel-heading">
											<a href="#faq-<%=b %>" data-parent="#faq-list-<%=b %>" data-toggle="collapse" class="accordion-toggle collapsed">
												<i class="ace-icon fa fa-chevron-left pull-right" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>
												<i class="ace-icon fa fa-plus smaller-80" data-icon-hide="ace-icon fa fa-minus" data-icon-show="ace-icon fa fa-plus"></i>&nbsp; 
												<%=faqP.getFaqTitle() %>
											</a>
										</div>
										
										<div class="panel-collapse collapse" id="faq-<%=b%>">
											<div class="panel-body">
												<%=faqP.getFaqDesc() %>
											</div>
										</div>
										
									</div>
								</div>
							</logic:equal>
						</logic:iterate>
					</logic:notEmpty>
					
				</div>
				
			</div>
			
		</div>
	</div>
</div>