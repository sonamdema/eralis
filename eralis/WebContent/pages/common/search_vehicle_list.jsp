<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
<table id="vehicle-list-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Vehicle Number</th>
			<th>Vehicle Type</th>
			<th>Category</th> 
			<th>Name</th> 
			<th>CID</th> 
			<th>Engine Number</th> 
			<th>Chassis Number</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_LIST">
			<logic:iterate id="renewal" name="RENEWAL_LIST">
				<tr>
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="renewal" property="renewalInfoId"/>' onclick="getSelected(this.id)">SELECT</button>
					</td>
					<td>
						<bean:write name="renewal" property="vehicleNo"/>
					</td>
					<td><bean:write name="renewal" property="vehicleType"/></td>
					<td>
						<logic:equal value="P" name="renewal" property="ownerType">
							Personal
						</logic:equal>
						<logic:equal value="O" name="renewal" property="ownerType">
							Organization
						</logic:equal>
						<logic:equal value="Foreign" name="renewal" property="ownerType">
							Foreign
						</logic:equal>
					</td>
					<td><bean:write name="renewal" property="personalOwnerName"/></td>
					<td><bean:write name="renewal" property="CID"/></td>
					<td><bean:write name="renewal" property="engineNumber"/></td>
					<td><bean:write name="renewal" property="chasisNumber"/></td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
</div>
<script>
	 $(document).ready(function() 
	{
	    $('#vehicle-list-table').DataTable({
	            responsive: true
	    });
	});
</script>
<script>
	<%
		String SEARCH_TYPE = (String) request.getAttribute("SEARCH_TYPE");
		String type = (String) request.getAttribute("TYPE");
	%>
	
	var type = "<%=type%>";
	var SEARCH_TYPE = "<%=SEARCH_TYPE%>";

	function getSelected(globalId)
	{
		$("#submitBtn").prop('disabled',false);
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getRenewalDtls&renewalInfoId='+globalId+'&type='+type,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{	
					$("#cancelledStatus").hide();
					$("#Msg").hide();
					$("#incompleteValidationMsg").hide();

					var lifeExpiryDate = $(this).find('lifeExpiryDate').text();
					var Status = $(this).find('Status').text();
					var cancelledDate = $(this).find('cancelDate').text();
					var cancelReason = $(this).find('reason').text();
					var receiptNo = $(this).find('receiptNo').text();
					var receiptDate = $(this).find('receiptDate').text();
					var CurrentDate = new Date();
					var lastLifeDate = new Date(lifeExpiryDate);
					
					
					
						
						
						var personalInfoId	=	$(this).find('personalInfoId').text();
						var ownerType = $(this).find('OwnerType').text();
						var ownerID = $(this).find('ownerid').text();
						var ownerName = $(this).find('OwnerName').text();
						var CitizenID = $(this).find('CID').text();
						var phone = $(this).find('phoneno').text();
						var Dzongkhag = $(this).find('Dzongkhag').text();
						var Gewog = $(this).find('Gewog').text();
	                    var Village =$(this).find('Village').text();
						var Country = $(this).find('Countryname').text();
						var Address = $(this).find('Address').text();
	                    var registrationDate = $(this).find('RegistrationDate').text();
	                    var expiryDate = $(this).find('ExpiryDate').text(); 
	                    var Company = $(this).find('Company').text(); 
	                    var Model = $(this).find('Model').text(); 
	                    var chasisNumber = $(this).find('chasisNumber').text(); 
	                    var engineNumber = $(this).find('engineNumber').text(); 
	                    var engineType = $(this).find('engineType').text(); 
	                    var seatCapacity = $(this).find('seatCapacity').text(); 
	                    var loadCapacity = $(this).find('loadCapacity').text(); 
	                    var Region = $(this).find('Region').text(); 
	                    var regionId = $(this).find('Region-Id').text();
	                    var lastIssueDate = $(this).find('lastIssueDate').text(); 
	                    var Color = $(this).find('Color').text(); 
	                    var colorName = $(this).find('colorName').text(); 
	                    
	                    if(Status == "1")
	                        Status = "Active";
                        
	                    var Email = $(this).find('Email').text();
	                    var vehicleNumber = $(this).find('vehicleNumber').text();               
	                    var customerId  = $(this).find('customerId').text();  
						var vehicleType	=$(this).find('vehicleType').text();
						var manufactureYear = $(this).find('manufacture-year').text(); 
						var vehicleTypeName = $(this).find('vehicle-type-name').text();
						var vehicleHorsePower	=$(this).find('vehicleHorsePower').text();  	
						var engineCC	=$(this).find('engineCC').text();
						var vehicleKiloWatt	=$(this).find('vehicleKiloWatt').text();  	 	
						var vehicleId	=	$(this).find('vehicleId').text(); 
						var isInternational = $(this).find('IsInternational').text(); 
						var vehicleTypeDesc = $(this).find('vehicleTypeDesc').text();
						var vehicleRegistrationCode = $(this).find('vehicle-registration-code').text();
						var region = $(this).find('region').text();
						var initialPrice = $(this).find('initial-price').text();
						var ministry = $(this).find('ministry').text();
						var department = $(this).find('department').text();
						var privateName = $(this).find('private').text();
						var owner = $(this).find('owner').text();
						var ownerTypeDesc = $(this).find('owner-type-desc').text();
						var passangerBusType = $(this).find('passangerBusType').text();
						var latestExpiryDate=$(this).find('latestExpiryDate').text();
						var transferExpiryDate=$(this).find('transferExpiryDate').text();
						if(ownerType == "Personal")
						{
							$('#Personal').show();
							$('#Organization').hide();
						}
						else
						{
							$('#Personal').hide();
							$('#Organization').show();
						}
					    $('#displayVehicleNumber').val(vehicleNumber);
						$('#passangerBusType').html("<label class='control-label'>"+passangerBusType+"</label>");
						$('#displayOwnerType').html("<label class='control-label'>"+ownerType+"</label>");
						$('#displayOwnerID').html("<label class='control-label'>"+ownerID+"</label>");
						$('#displayOwnerName').html("<label class='control-label'>"+ownerName+"</label>");
						$('#displayCitizenID').html("<label class='control-label'>"+CitizenID+"</label>");
						$('#displayPhone').html("<label class='control-label'>"+phone+"</label>");  
						$('#displayDzongkhag').html("<label class='control-label'>"+Dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+Gewog+"</label>");
						$('#displayVillage').html("<label class='control-label'>"+Village+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+Country+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+Address+"</label>");
						$('#displayLastRegistrationDate').html("<label class='control-label'>"+registrationDate+"</label>");
						$('#displayExpiryDate').html("<label class='control-label'>"+expiryDate+"</label>");
						$('#displayCompany').html("<label class='control-label'>"+Company+"</label>");
						$('#displayModel').html("<label class='control-label'>"+Model+"</label>");
						$('#displayChasisNumber').html("<label class='control-label'>"+chasisNumber+"</label>");
					    $('#displayEngineNumber').html("<label class='control-label'>"+engineNumber+"</label>");
						$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
						$('#displaySeatCapacity').html("<label class='control-label'>"+seatCapacity+"</label>");
						$('#displayLoadCapacity').html("<label class='control-label'>"+loadCapacity+"</label>");
					    $('#displayRegion').html("<label class='control-label'>"+Region+"</label>");
						$('#displayLastRegistrationDate').html("<label class='control-label'>"+lastIssueDate+"</label>");
					    $('#displayColor').html("<label class='control-label'>"+colorName+"</label>");
				        $('#displayStatus').html("<label class='control-label'>"+Status+"</label>");
				        $('#displayEmail').html("<label class='control-label'>"+Email+"</label>");
				        $('#vehicleType').html("<label class='control-label'>"+vehicleType+"</label>");
						$('#displayVehicleTypeName').html("<label class='control-label'>"+vehicleTypeName+"</label>");
						$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
						$('#displayInitialPrice').html("<label class='control-label'>Nu.&nbsp;"+initialPrice+"</label>");
						$('#displayEngineCC').html("<label class='control-label'>"+engineCC+"</label>");
						//$('#displaylatestExpiryDate').html("<label class='control-label'>"+latestExpiryDate+"</label>");
						$('#displayTransferExpiryDate').html("<label class='control-label'>"+transferExpiryDate+"</label>");
						$('#displayOrgOwnerType').html("<label class='control-label'>"+owner+"</label>");

						if(ownerTypeDesc == "GOVERNMENT")
						{
								$('#displayOrgAgency').html("<label class='control-label'>"+ministry+"</label>");
								$('#displayOrgOwnerName').html("<label class='control-label'>"+department+"</label>");
						}
						else if(ownerTypeDesc == "PRIVATE")
						{
							$('#displayOrgOwnerName').html("<label class='control-label'>"+privateName+"</label>");
						}
						else
						$('#displayOrgOwnerName').html("<label class='control-label'>"+ownerName+"</label>");
						$('#displayOrgAddress').html("<label class='control-label'>"+Address+"</label>");
						$('#displayOrgPhone').html("<label class='control-label'>"+phone+"</label>");
						$('#displayOrgDzongkhag').html("<label class='control-label'>"+Dzongkhag+"</label>");
					    if(isInternational == "N")
						{
					    	$('#internationalDIV').hide();
					    	$('#nationalDIV').show();
						}
					    else
						{
					    	$('#internationalDIV').show();
					    	$('#nationalDIV').hide();
						}
					    
					    fetchAccidentRecord();
						

				});
			}
		});
	}
	
</script>