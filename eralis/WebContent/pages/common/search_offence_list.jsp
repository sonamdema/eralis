<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
<table id="renewal-search-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>TIN No</th>
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			<th>Offence Date</th>
			<th>Vehicle No</th> 
			<th>License/Learner No</th> 
			<th>Status</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="OFFENCE_LIST">
			<logic:iterate id="offence_list" name="OFFENCE_LIST">
				<tr>
					<td> 
						<input type="radio" name="gender" value="male" onClick="getSelected1('<bean:write name="offence_list" property="offenceId"/>')"> 
					</td>
					<td><bean:write name="offence_list" property="TINno"/></td>
					<td><bean:write name="offence_list" property="receiptNo"/></td>
					<td><bean:write name="offence_list" property="receiptDate"/></td>
					<td><bean:write name="offence_list" property="offencedate"/></td>
					<td><bean:write name="offence_list" property="vehicleNo"/></td>
					<td><bean:write name="offence_list" property="licenseNo"/></td>
					<td><bean:write name="offence_list" property="isPaid"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
</div>
<script>
	function getSelected1(offenceId)
	{
		get_offence_list(offenceId);
		getOffencePicture(offenceId);
	}

</script>