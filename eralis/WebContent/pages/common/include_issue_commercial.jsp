<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%
	LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
	 <div class="widget-header widget-header-small">
		  <h5 class="widget-title lighter">Non Commercial License Application Details</h5>
	</div>						
	<div class="widget-body">
		<div class="widget-main">
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">License No:</label>
				</div>
				<div class="col-lg-4" ><%=dto.getLicenseNo()%></div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Region:</label>
				</div>
				<div class="col-lg-4"><%=dto.getRegion()%></div>
				<div class="col-lg-2">
					<label class="control-label">Receipt Number:</label>
				</div>
				<div class="col-lg-4"><%=dto.getReceiptNo()%></div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Receipt Date:</label>
				</div>
				<div class="col-lg-4"><%=dto.getReceiptDate()%></div>
				<div class="col-lg-2">
					<label class="control-label">Remarks:</label>
				</div>
				<div class="col-lg-4"><%=dto.getRemarks()%></div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Submission Date:</label>
				</div>
				<div class="col-lg-4"><%=dto.getAppsubmissiondate()%></div>
			</div> 
			<div class="form-group">
				<div class="col-lg-2">
					<label class="control-label">Expiry Date</label>
				</div>
				<div class="col-lg-4"><%=dto.getExpiryDate()%></div>
				
			</div> 
		</div>      
	</div>
</div>
<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driving License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							 <div class="form-group">
								<div class="col-lg-2">
									<label>Drive Types<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="drivetype" onchange="validateCommercialCriteria(this.value)"
										styleClass="form-control" styleId="drivetype">
										<html:optionsCollection name="COMMERCIAL_DRIVE_TYPE_LIST"
											label="headerName" value="headerId"  /> 
									</html:select>
								</div>
								<div id="validationMsg" style="display:none;" class="col-lg-7 alert alert-danger"></div>
  							</div>
  							<div class="form-group">
								<div class="col-lg-2">
									<label>Renewal Duration:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
	                           			<html:option value="">--SELECT--</html:option>
		                           		<html:option value="36">3 Years</html:option>
		                           		<html:option value="12">1 Year</html:option>
	                           		</html:select>
										<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select renewal duration</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" value="<%=dto.getRemarks()%>" styleId="remarks" styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input id="drivinglicenseId" type="hidden" value="<%=dto.getDrivinglicenseId()%>">
<script>

	$("#renewalDuration").val(<%=dto.getRenewalDuration()%>);
	var	driveTypeId	=	'<%=dto.getDriveTypeId()%>';
	$("#drivetype").val(driveTypeId);
	function validateCommercialCriteria(driveTypeId)
	{
		$("#validationMsg").hide();
		var drivinglicenseId	=	$("#drivinglicenseId").val();
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=validateCommercialCriteria&driveTypeId='+driveTypeId+'&drivinglicenseId='+drivinglicenseId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var testStatus = $(this).find('testStatus').text();
					var reason = $(this).find('testReason').text();
					if(testStatus=='0')
					{
						$("#validationMsg").show();
						$('#validationMsg').html("<label>"+reason+"</label>");
						 $('#submitBtn').attr('disabled',true);
					}
					else if(testStatus=='1')
					{
						 $('#submitBtn').attr('disabled',false);
					}
				});
			}
		});
	}
</script>