<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="fitness-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Sl</th>
			<th>Fitness Date</th>
			<th>Valid Till Date</th> 
			<th>Receipt Number</th> 
			<th>Receipt Date</th>  
			<th>Inspected By</th>
			<th>Remarks</th>  
			<th>Region</th>  
			<th>Base</th> 
		</tr>
	</thead>
	<tbody>
		<% int i = 1; %>
		<logic:notEmpty name="FITNESS_HISTORY">
			<logic:iterate id="fitness" name="FITNESS_HISTORY">
				<tr>
					<td><%=i++ %></td>
					<td><bean:write name="fitness" property="testDate"/></td>
					<td><bean:write name="fitness" property="validUpto"/></td>
					<td><bean:write name="fitness" property="receiptNo"/></td>
					<td><bean:write name="fitness" property="receiptDate"/></td>
					<td><bean:write name="fitness" property="inspectedBy"/></td>
					<td><bean:write name="fitness" property="remarks"/></td>
					<td><bean:write name="fitness" property="region"/></td>
					<td><bean:write name="fitness" property="baseoffice"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#fitness-history-table').DataTable({
	            responsive: true
	    });
	});

</script>