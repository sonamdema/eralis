
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
<table id="vehicle-list-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Vehicle Number</th>
			<th>Driver Name</th>
			<th>LL/DL/No</th> 
			<th>Vehicle Type</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="ACCIDENT_VEHICLE_LIST">
			<logic:iterate id="renewal" name="ACCIDENT_VEHICLE_LIST">
				<tr>
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="renewal" property="vehicleNo"/>' onclick="fetchAccidentRecord(this.id)">SELECT</button>
					</td>
					<td><bean:write name="renewal" property="vehicleNo"/></td>
					<td><bean:write name="renewal" property="driverName"/></td>
					<td><bean:write name="renewal" property="LLNo"/></td>
					<td><bean:write name="renewal" property="vehicleType"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
</div>
<script>
	 $(document).ready(function() 
	{
	    $('#vehicle-list-table').DataTable({
	            responsive: true
	    });
	});
</script>
<script>
	<%
		String SEARCH_TYPE = (String) request.getAttribute("SEARCH_TYPE");
		String type = (String) request.getAttribute("TYPE");
	%>
	
	var type = "<%=type%>";
	var SEARCH_TYPE = "<%=SEARCH_TYPE%>";

	function getSelected(globalId)
	{
		$("#submitBtn").prop('disabled',false);
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getaccidentvehicleDtls&renewalInfoId='+globalId+'&type='+type,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{	
					$("#cancelledStatus").hide();
					$("#Msg").hide();
					$("#incompleteValidationMsg").hide();
					
					var applicationNo = $(this).find('applicationNo').text();
					var lifeExpiryDate = $(this).find('lifeExpiryDate').text();
					var Status = $(this).find('Status').text();
					var cancelledDate = $(this).find('cancelDate').text();
					var cancelReason = $(this).find('reason').text();
					var receiptNo = $(this).find('receiptNo').text();
					var receiptDate = $(this).find('receiptDate').text();
					var CurrentDate = new Date();
					var lastLifeDate = new Date(lifeExpiryDate);

					var initialReceiptDate = $(this).find('initialReceiptDate').text();
					var initialReceiptNo = $(this).find('initialReceiptNo').text();
					var penalty = $(this).find('penalty').text();
					var amount = $(this).find('initial-price').text();
					$("#initialReceiptNo").text(initialReceiptNo);
					$("#initialReceiptDate").text(initialReceiptDate);
					$("#initialPenaly").text(penalty);
					$("#initialAmount").text(amount);
					
					
				});
			}
		});
	}
	
</script>