<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="duplication-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Duplicate Issue Date</th>
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			<th>Supporting Doc</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="DUPLICATION_HISTORY">
			<logic:iterate id="duplication" name="DUPLICATION_HISTORY">
				<tr>
					<td><bean:write name="duplication" property="duplicateIssueDate"/></td>
					<td><bean:write name="duplication" property="receiptNo"/></td>
					<td><bean:write name="duplication" property="receiptDate"/></td> 
					<td>
						 <logic:notEmpty name="duplication" property="fileUUID">
					        <a href="#" onclick="downloadFile('<bean:write name="duplication" property="fileUUID" filter="false"/>','<bean:write name="duplication" property="fileName" filter="false"/>')">
	  							<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="duplication" property="fileName" filter="false"/>
					    </logic:notEmpty>
	  				</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#duplication-history-table').DataTable({
	            responsive: true
	    });
	});

</script>