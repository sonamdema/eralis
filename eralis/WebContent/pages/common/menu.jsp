<%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
	EralisUserRolePriviledge userRolePriv = null;
	String regionId = null, regionName = null, roleCode = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		regionId = userRolePriv.getRegionId();
		regionName  = userRolePriv.getRegionName();
		
		Role currentRole = userRolePriv.getCurrentRole();
		roleCode = currentRole.getRoleCode();
	}
%>
	<div id="sidebar" class="sidebar responsive">
		<script type="text/javascript">
			try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
		</script>
		<ul class="nav nav-list">
			<li class="">
				<a href="#" onclick="displayHomePage()">
					<i class="menu-icon fa fa-tachometer"></i>
					<span class="menu-text"> Dashboard </span>
				</a>
				<b class="arrow"></b>
			</li>
			<%
				if(!roleCode.equalsIgnoreCase("EMISSION_CENTER") && !roleCode.equalsIgnoreCase("OTHERS") && !roleCode.equalsIgnoreCase("HEAD_OFFICE"))
				{
			%>
				<li class="">
					<a href="#" onclick="showTaskList()">
						<i class="menu-icon fa fa-desktop"></i>
						<span class="menu-text"> View TaskList </span>
					</a>
					<b class="arrow"></b>
				</li>
			<%
				}
			%>
			<logic:notEmpty name="menuList">
				<logic:iterate id="menu" name="menuList" type="bt.gov.rsta.framework.vo.MenuVO">
					<li>
						<a href="#" class="dropdown-toggle">
							<i class='<bean:write name="menu" property="menuIcon"/>'></i>
							<span class="menu-text">
								<bean:write name="menu" property="menuName"/>
							</span>
							<logic:notEmpty name="menu" property="subMenuList">
								<b class="arrow fa fa-angle-down"></b>
							</logic:notEmpty>
						</a>
						<b class="arrow"></b>
						<logic:notEmpty name="menu" property="subMenuList">
							<ul class="submenu">
								<logic:iterate id="subMenu" name="menu" property="subMenuList" type="bt.gov.rsta.framework.vo.SubMenuVO">
									<logic:notEmpty name="subMenu" property="thirdLvlMenuList">
										<li>
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-caret-right"></i>
												<bean:write name="subMenu" property="menuName"/>
												<b class="arrow fa fa-angle-down"></b>
											</a>
											<b class="arrow"></b>
											<ul class="submenu">
												<logic:iterate id="thirdMenu" name="subMenu" property="thirdLvlMenuList" type="bt.gov.rsta.framework.vo.ThirdLevelMenuVo">
													<li>
														<a href="#" onclick="loadPageDetails('<bean:write name="thirdMenu" property="menuLink"/>')">
															<i class="menu-icon fa fa-caret-right"></i>
															<bean:write name="thirdMenu" property="menuName"/>
														</a>
														<b class="arrow"></b>
													</li>
												</logic:iterate>
											</ul>
										</li>
									</logic:notEmpty>
									<logic:empty name="subMenu" property="thirdLvlMenuList">
										<li>
											<a href="#" onclick="loadPageDetails('<bean:write name="subMenu" property="menuLink"/>')">
												<i class="menu-icon fa fa-caret-right"></i>
												<bean:write name="subMenu" property="menuName"/>
											</a>
											<b class="arrow"></b>
										</li>
									</logic:empty>
								</logic:iterate>
							</ul>
						</logic:notEmpty>
					</li>
				</logic:iterate>
			</logic:notEmpty>
			
		</ul><!-- /.nav-list -->

		<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
			<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
		</div>

		<script type="text/javascript">
			var context = "<%=request.getContextPath()%>";
			
			try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}

			function loadPageDetails(page)
			{
				$.blockUI
		        ({ 
		        	css: 
		        	{ 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
		        	} 
		        });
			        
				$("#contentDisplayDiv").load(page);
				$('#contentDisplayDiv').show();

				setTimeout($.unblockUI, 1000); 
			}
		</script>
	</div>