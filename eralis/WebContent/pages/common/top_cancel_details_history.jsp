<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="top-cancel-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>TOP Number</th>
			<th>Cancellation Date</th> 
			<th>Cancellation Reason</th> 
			<th>Processed Region Name</th> 
			<th>Processed Base office Name</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="TOP_CANCELLATION_HISTORY">
			<logic:iterate id="topCancellationDtlsHistory" name="TOP_CANCELLATION_HISTORY">
				<tr>
					<td><bean:write name="topCancellationDtlsHistory" property="topNumber"/></td>
					<td><bean:write name="topCancellationDtlsHistory" property="cancellationdate"/></td> 
					<td><bean:write name="topCancellationDtlsHistory" property="reason"/></td>
					<td><bean:write name="topCancellationDtlsHistory" property="region"/></td>
					<td><bean:write name="topCancellationDtlsHistory" property="baseoffice"/></td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#top-cancel-history-table').DataTable({
	            responsive: true
	    });
	});

</script>