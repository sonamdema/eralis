<%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-0WQ7MH1BGV"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'G-0WQ7MH1BGV');
		</script>
</head>
<%
	EralisUserRolePriviledge userRolePriv = null;
	String regionId = null, regionName = null, roleCode = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		regionId = userRolePriv.getRegionId();
		regionName  = userRolePriv.getRegionName();
		
		Role currentRole = userRolePriv.getCurrentRole();
		roleCode = currentRole.getRoleCode();
	}
%>

<script>
	var projectContextPath = '<%=request.getContextPath()%>';
</script>
<body class="no-skin">
		
		<!-- header include -->
		<jsp:include page="/pages/common/header.jsp"></jsp:include>
		<!-- ./header include -->

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- menu include -->
			<jsp:include page="/pages/common/menu.jsp"></jsp:include>
			<!-- ./menu include -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<div class="nav-search" id="nav-search">
							<i class="ace-icon fa fa-calendar fa-lg"></i> 
							<%
							 	String DATE_FORMAT_NOW = "dd-MM-yyyy";
								Calendar cal = Calendar.getInstance();

								SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
							    String day = sdfDay.format(cal.getTime());
							    
							    SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
							    String year = sdfYear.format(cal.getTime());
							    
							    Calendar mCalendar = Calendar.getInstance();    
							    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
							%>
							<span class="blue"><%=day%>&nbsp;<%=month %>,&nbsp;<%=year %></span>
							<input type="hidden" id="mainRegionId" value="<%=regionId%>"/>
							<input type="hidden" id="mainRegionName" value="<%=regionName%>"/>
						</div>
					</div>

					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="page-header">
								<h1>
									<i class="ace-icon fa fa-tachometer"></i> Dashboard
									<%
										if(!roleCode.equalsIgnoreCase("EMISSION_CENTER"))
										{
									%>
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
										overview &amp; stats for today for your Jurisdiction
									</small>
									<%
										}
										else if(roleCode.equalsIgnoreCase("EMISSION_CENTER"))
										{
									%>
										<small>
											<i class="ace-icon fa fa-angle-double-right"></i>
											overview &amp; stats for today
										</small>
									<%
										}
									%>
								</h1>
							</div><!-- /.page-header -->
						<%
							if(!roleCode.equalsIgnoreCase("EMISSION_CENTER"))
							{
						%>
							<div class="row">
								<div class="col-xs-12">
										<div class="row">
											<div class="space-6"></div>
		
											<div class="col-sm-7 infobox-container">
											
												<div class="infobox infobox-pink" style="cursor: pointer;"
												 onclick="loadPageDetails('redirect.html?q=STATISTIC_DTL_REPORT&page_id=STATS_TOTAL_VEH_REGISTERED')">
													<div class="infobox-icon">
														<i class="ace-icon fa fa-car"></i>
													</div>
													<div class="infobox-data" >
														<span class="infobox-data-number"><%=request.getAttribute("VEHICLE_REGISTERED") %></span>
														<div class="infobox-content">Vehicles Registered</div>
													</div>
												</div>
												
												<div class="infobox infobox-orange2"  style="cursor: pointer;"  onclick="loadPageDetails('redirect.html?q=STATISTIC_DTL_REPORT&page_id=STATS_TOTAL_VEH_RENEWED')">
													<div class="infobox-icon">
														<i class="ace-icon fa fa-car"></i>
													</div>
													<div class="infobox-data">
														<span class="infobox-data-number"><%=request.getAttribute("VEHICLE_RENEWAL") %></span>
														<div class="infobox-content">Vehicles Renewed</div>
													</div>
												</div>
		
												<div class="infobox infobox-blue"   style="cursor: pointer;" onclick="loadPageDetails('redirect.html?q=STATISTIC_DTL_REPORT&page_id=STATS_TOTAL_DL_PRINTED')">
													<div class="infobox-icon">
														<i class="ace-icon fa fa-print"></i>
													</div>
		
													<div class="infobox-data" >
														<span class="infobox-data-number"><%=request.getAttribute("NEW_LICENSE_PRINTED") %></span>
														<div class="infobox-content">DL Printed</div>
													</div>
												</div>
		
												<div class="infobox infobox-green" style="cursor: pointer;"  onclick="loadPageDetails('redirect.html?q=STATISTIC_DTL_REPORT&page_id=STATS_TOTAL_LL_ISSUED')">
													<div class="infobox-icon">
														<i class="ace-icon fa fa-credit-card"></i>
													</div>
		
													<div class="infobox-data">
														<span class="infobox-data-number"><%=request.getAttribute("NEW_LEARNER_ISSUED") %></span>
														<div class="infobox-content">Learner License Issued</div>
													</div>
												</div>
		
												<div class="infobox infobox-green2" style="cursor: pointer;"  onclick="loadPageDetails('redirect.html?q=STATISTIC_DTL_REPORT&page_id=STATS_TOTAL_LIC_ISSUED')">
													<div class="infobox-icon">
														<i class="ace-icon fa fa-credit-card"></i>
													</div>
		
													<div class="infobox-data">
														<span class="infobox-data-number"><%=request.getAttribute("DL_ISSUED") %></span>
														<div class="infobox-content">DL Issued</div>
													</div>
												</div>
		
												<div class="infobox infobox-blue2" style="cursor: pointer;" onclick="loadPageDetails('redirect.html?q=STATISTIC_DTL_REPORT&page_id=STATS_TOTAL_LIC_RENEWED')">
													<div class="infobox-icon">
														<i class="ace-icon fa fa-credit-card"></i>
													</div>
		
													<div class="infobox-data">
														<span class="infobox-data-number"><%=request.getAttribute("LICENSE_RENEWAL") %></span>
														<div class="infobox-content">DL Renewed</div>
													</div>
												</div>
												<div class="space-6"></div>
												<div class="vspace-12-sm"></div>
											</div>
											
											<div class="col-sm-5">
												<div class="widget-box">
													<div class="widget-header widget-header-flat widget-header-small">
														<h5 class="widget-title">
															<i class="ace-icon fa fa-signal"></i>
															Overall Stats
														</h5>
													</div>
													<div class="widget-body">
														<div class="widget-main">
																	
															<div class="infobox infobox-green infobox-small infobox-dark">
																<div class="infobox-progress">
																	<div class="easy-pie-chart percentage" data-percent="70" data-size="39">
																		<span class="percent"><%=request.getAttribute("TOTAL_TASK_COMPLETION") %></span>%
																	</div>
																</div>
	
																<div class="infobox-data">
																	<div class="infobox-content">Task</div>
																	<div class="infobox-content">Completion</div>
																</div>
															</div>
														
															<div class="infobox infobox-blue infobox-small infobox-dark">
																<div class="infobox-icon">
																	<i class="ace-icon fa fa-bar-chart-o"></i>
																</div>
					
																<div class="infobox-data">
																	<div class="infobox-content">Earnings</div>
																	<div class="infobox-content">Nu.<%=request.getAttribute("TOTAL_EARNING") %></div>
																</div>
															</div>
					
															<div class="infobox infobox-grey infobox-small infobox-dark">
																<div class="infobox-icon">
																	<i class="ace-icon fa fa-print"></i>
																</div>
					
																<div class="infobox-data">
																	<div class="infobox-content">Printings</div>
																	<div class="infobox-content"><%=request.getAttribute("GET_TOTAL_PRINTINGS") %></div>
																</div>
															</div>
																		
														</div>
													</div>
												</div>
											</div>
										</div><!-- /.row -->
									<!-- PAGE CONTENT ENDS -->
								</div><!-- /.col -->
							</div><!-- /.row -->
							<!--<div class="row">
			                    <div class="col-sm-5">
			                    	<div class="panel panel-danger">
			                            <div class="panel-heading">
			                                <div class="row">
			                                    <div class="col-xs-3">
			                                        <i class="fa fa-bell-o fa-5x"></i>
			                                    </div>
			                                    <div class="col-xs-9 text-right">
			                                        <div class="huge"><h2><%=request.getAttribute("TOTAL_LICENSE_ALERT") %></h2></div>
			                                        <div>
			                                        	<h5>License Application Alert!</h5>
			                                        	(Not Printed Within TAT)
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                            <a href="#" onclick="loadAlertDetails()">
			                                <div class="panel-footer">
			                                    <span class="pull-left">View Details</span>
			                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
			                                    <div class="clearfix"></div>
			                                </div>
			                            </a>
			                        </div>
			                    </div>
							</div>-->
						<%
							}
							else if(roleCode.equalsIgnoreCase("EMISSION_CENTER"))
							{
						%>
							<div class="row">
								<div class="col-lg-12">
									<div class="widget-box transparent">
										<div class="widget-header widget-header-flat">
											<h4 class="widget-title lighter">
												<i class="ace-icon fa fa-car green"></i>
												Total Vehicles Tested:
												&nbsp;
												<%=request.getAttribute("TOTAL_EMISSION") %>
											</h4>
										</div>
										<div class="widget-body">
											<div class="widget-main no-padding">
												<table class="table table-bordered table-striped">
													<tbody>
														<tr>
															<td>Total Number of Vehicles who passed the test</td>
															<td>
																<span class="label label-success arrowed-in arrowed-in-right">Passed</span>
															</td>
															<td align="center">
																<%=request.getAttribute("TOTAL_EMISSION_PASSED") %>
															</td>
														</tr>
														<tr>
															<td>Total Number of Vehicles who failed the test</td>
															<td>
																<span class="label label-danger arrowed-in arrowed-in-right">Failed</span>
															</td>
															<td align="center">
																<%=request.getAttribute("TOTAL_EMISSION_FAILED") %>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						<%
							}
						%>	
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<!-- footer include -->
			<jsp:include page="/pages/common/footer.jsp"></jsp:include>
			<!-- /.footer include -->
			
		</div><!-- /.main-container -->
		
		<script>

		$(document).keypress(function(event){
		    var keycode = (event.keyCode ? event.keyCode : event.which);
		    if(keycode == '13')
		    {
		    	var element_id	=	$("*:focus").attr("id");
		        var div_id	=	$(".modal").find("#"+element_id).closest("div[id]").attr('id');
		       	$("#"+div_id).find("button[name='search']").click();
		    }
		  });
		
		
		// IDEL TIMEOUT
		  
			$.idleTimeout('#idletimeout', '#idletimeout a', 
			{
				idleAfter: 600,
				pollingInterval: 2,
				serverResponseEquals: 'OK',
				onTimeout: function()
				{
					$(this).slideUp(); 
					window.location = "<%=request.getContextPath()%>/logout.html";
				},
				onIdle: function()
				{
					$(this).slideDown();  //show the warning bar
					$(this).get(0).scrollIntoView();
				},
				onCountdown: function( counter )
				{
					$(this).find("span").html( counter );  //update the counter
				},
				onResume: function()
				{
					$(this).slideUp(); // hide the warning bar
				}
		   });

			function logout()
			{
				window.location = "<%=request.getContextPath()%>/logout.html";
			}

			function loadAlertDetails(identifier)
			{
				$.blockUI
		        ({ 
		        	css: 
		        	{ 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
		        	} 
		        });

	        	$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getAlertList&identifier="+identifier,
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#contentDisplayDiv").html(responseText);
						$("#contentDisplayDiv").show();
						setTimeout($.unblockUI, 1000); 
					}
				});
			}

		</script>
		
	</body>
</html>