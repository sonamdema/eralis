<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="conversion-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Conversion Date</th>
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			<th>Converted From</th>
			<th>Supporting Doc</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="CONVERSION_HISTORY">
			<logic:iterate id="conversion" name="CONVERSION_HISTORY">
				<tr>
					<td><bean:write name="conversion" property="conversionDate"/></td>
					<td><bean:write name="conversion" property="receiptNo"/></td>
					<td><bean:write name="conversion" property="receiptDate"/></td> 
					<td><bean:write name="conversion" property="convertedFrom"/></td> 
					<td>
						 <logic:notEmpty name="conversion" property="fileUUID">
					        <a href="#" onclick="downloadFile('<bean:write name="conversion" property="fileUUID" filter="false"/>','<bean:write name="conversion" property="fileName" filter="false"/>')">
	  							<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="conversion" property="fileName" filter="false"/>
					    </logic:notEmpty>
	  				</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#conversion-history-table').DataTable({
	            responsive: true
	    });
	});

</script>