<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");

%>
<html:form styleClass="form-horizontal" action="/common.html" styleId="resubmitForm">
<div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Transferor Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
					<div class="form-group ">
							<div class="col-lg-2">
								<label>Vehicle Number:</label>
						    </div>	
                         <div class="col-lg-4"><%=dto.getVehicleNumber() %>
                       </div> 
					</div>
					   <div class="form-group">
							<div class="col-lg-2">
								<label>Owner Type:</label>
							</div>	
                                  <div class="col-lg-4"><%=dto.getTransfereeVehicleRegistrationType()%>
                                  </div> 
                            <div class="col-lg-2">
								<label>Owner ID :</label>
					        </div>	
		                           <div class="col-lg-4"><%=dto.getTransferorCustomerId()%>
                                  </div>
		                   </div>
		                 <div class="form-group">   
		                    <div class="col-lg-2">
								<label>Owner Name :</label>
					        </div>	
		                          <div class="col-lg-4"><%=dto.getName() %>
                                  </div>
		                    <div class="col-lg-2">
								<label>Citizen ID:</label>
							</div>	
                                  <div class="col-lg-4"><%=dto.getCitizenID() %>
                                  </div>
		                   </div>
		                  <div class="form-group">
			       				<div class="col-lg-2">
									<label>Phone:</label>
								</div>
		          				<div class="col-lg-4"><%=dto.getPhone()%></div>
		                   </div>
		                   <%
		                  		System.out.println(dto.getIsInternational());
		                   		if(dto.getIsInternational().equals("N"))
		                   		{
							%>
		                 <h4>Address (National)</h4>
		                 <div class="form-group">
		                    <div class="col-lg-2">
								 <label>Dzongkhag:</label>
							</div>	
                                <div class="col-lg-4"><%=dto.getDzongkhag()%>
                            	</div> 
                            <div class="col-lg-2">
								<label>Gewog:</label>
					        </div>	
		                          <div class="col-lg-4"><%=dto.getGewog()%>
                                   </div>
		                    </div>
		                   <div class="form-group"> 
		                    <div class="col-lg-2">
								<label>Village:</label>
							</div>	
                             <div class="col-lg-4"><%=dto.getVillage()%>
                             </div> 
		                    </div>
		                  <%
	                   		}
	                   		else
	                   		{
		                  %>
		                  <h4>Address (Non_National)</h4> 
		                   <div class="form-group ">
		                    <div class="col-lg-2">
								<label>Country:</label>
					        </div>	
		                           <div class="col-lg-4"><%=dto.getCountry() %>
                                    </div>
		                      <div class="col-lg-2">
								  <label>Address:</label>
							  </div>	
                                    <div class="col-lg-4"><%=dto.getAddress()%>
                                    </div>   
                              </div> 
                             <%
		                   		}
                             %>
                           </div> 
                        </div>  
                   <div class="widget-header widget-header-small">
			          <h5 class="widget-title lighter">Registration Details</h5>
	                </div>						
		                <div class="widget-body">
			              <div class="widget-main">
                            <div class="form-group">
                              <div class="col-lg-2">
								 <label>Last Registration Date:</label>
					           </div>	
		                             <div class="col-lg-4"> <%=dto.getLastRegistrationDate() %>
                                      </div>
		                     <div class="col-lg-2">
								<label>Expiry Date :</label>
							 </div>	
                                  <div class="col-lg-4"><%=dto.getExpiryDate() %>
                                  </div> 
		                    </div>    
		                  </div> 
		               </div> 
		               <div class="widget-header widget-header-small">
			              <h5 class="widget-title lighter">Transferee Details</h5>
		                </div>						
		                <div class="widget-body">
			              <div class="widget-main">
		                    <div class="form-group">
                                <div class="col-lg-2">
								   <label>Owner Name:</label>
					            </div>	
		                            <div class="col-lg-4"> <%=dto.getTransfereeName() %>
                                    </div>
		                         <div class="col-lg-2">
								     <label>Dzongkhag :</label>
							    </div>	
                                     <div class="col-lg-4"><%=dto.getTransfereeDzongkhag() %>
                                     </div> 
		                     </div>
		                    <div class="form-group">
			                 	<div class="col-lg-2">
									<label>Gewog:</label>
						        </div>	
			                         <div class="col-lg-4"> <%=dto.getTransfereeGewog() %>
	                                 </div>
			                 	<div class="col-lg-2">
									<label>Address:</label>
								</div>	
	                                 <div class="col-lg-4"><%=dto.getTransfereeAddress() %>
	                                 </div> 
                            </div> 
		                  </div> 
			                <div class="widget-body">
				              	<div class="widget-main">  
				              		<div class="form-group"> 
			                             <div class="col-lg-2">
									           <label>Transfer Date :</label>
						                 </div>	
				                             <div class="col-lg-4"><%=dto.getTransferDate() %>
		                                     </div>  
					                 </div>    
					                 <div class="form-group"> 
			                             <div class="col-lg-2">
											 <label>Transfer Amount :</label>
								         </div>	
					                         <div class="col-lg-4"><%=dto.getAmount() %>
			                                  </div>
			                              <div class="col-lg-2">
									           <label>Penality :</label>
						                  </div>	
					                             <div class="col-lg-4"><%=dto.getPenalty() %>
			                                     </div>  
						              </div>    
					                  <div class="form-group">
					                      <div class="col-lg-2">
											 <label>Receipt Date:</label>
										  </div>	
			                                   <div class="col-lg-4"><%= dto.getReceiptDate() %>
			                                   </div> 
			                             <div class="col-lg-2">
											 <label>Receipt No:</label>
								         </div>	
					                          <div class="col-lg-4"><%=dto.getReceiptNo() %>
			                                  </div>
		                              </div>
		                              <div class="form-group">
					                     	<logic:equal value="APPROVE" name="param">
												<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
											</logic:equal>
											<html:hidden property="vehicleId" value="<%=dto.getVehicleId() %>"></html:hidden>
											<html:hidden property="vehicleRegistrationType" styleId="vehicleRegistrationType"></html:hidden>
									    </div>
				              	</div>
				            </div>
		               </div>   
		            </div> 
		          <div class="widget-box">
		     <div class="widget-header widget-header-small">
			      <h5 class="widget-title lighter">Transferree Details</h5>
		     </div>						
	         <div class="widget-body">
		        <div class="widget-main">
                      <div class="row">
				      <div class="col-lg-12">
					    <div class="form-group">
						   <div class="col-lg-3">
							<label>
								<html:radio property="personalRadio" styleClass="ace" styleId="personalRadio" value="p" onclick="showPersonal()"></html:radio>	
								<span class="lbl"> Personal</span>
							</label>
							
							<label>
								<html:radio property="personalRadio" styleClass="ace" styleId="organisationRadio" value="o" onclick="showOrganisation()"></html:radio>	
								<span class="lbl"> Organisation</span>
							</label>
					    	</div>  
					    	<div class="col-lg-3">
					    		<label style="display:none;color: #ff0000" id="radioValidation">Please select atleast one option</label>
					    	</div>
                          </div>  
                         <div class="form-group" style="display:none" id="personal1"> 
							  <div class="col-lg-2">
							     <label>Citizen ID :<span style="color: #ff0000">*</span></label>
						      </div>
							  <div class="col-lg-3">                                         
							     <div class="input-group"> 
							       <html:text property="citizenID" value="<%=dto.getTransferorCustomerId() %>" styleClass="form-control" styleId="citizenID" readonly="true"></html:text>
									<span class="input-group-btn">
										 <a href="#" onclick="openModal('citizen')" class="btn btn-purple btn-sm">
											<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
										</a>
									</span>
					           	  </div>
					          </div>
					          <div class="col-lg-3">
					          	<label style="display:none;color: #ff0000" id="citizenIDValidation">Please search for transferee details</label>
					          </div>
						</div>	  
					    <div class="form-group" style="display:none" id="organization1"> 
						       <div class="col-lg-2">
							     <label> Owner ID :<span style="color: #ff0000">*</span></label>
						       </div>
						         <div class="col-lg-3">
						         	<div class="input-group">
                                       <html:text property="ownerID" value="<%=dto.getTransferorCustomerId() %>" styleClass="form-control" styleId="ownerID" readonly="true"></html:text>
                                    
						              <span class="input-group-btn">
						      	         <a href="#" onclick="openModal('owner')" class="btn btn-purple btn-sm">	
									   		<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								         </a>
										</span>
									</div>
								 </div>
								 <div class="col-lg-3">
						          	<label style="display:none;color: #ff0000" id="ownerIDValidation">Please search for transferee details</label>
						          </div>
	                           </div>
					        </div>
	                     </div>
		             </div>
		          </div>
                  </div>
     <div class="widget-box" id="personal" style="display:none">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label>Owner Name :</label>
							</div>
		                     <div class="col-lg-3"  id="TransferOwnerName">
                   				<%=dto.getTransfereeName() %>
                             </div>
						   
						    <div class="col-lg-2">
								<label>Dzongkhag :</label>
							</div>
			                <div class="col-lg-3" id="TransferDzongkhag">
                 				<%=dto.getTransfereeDzongkhag() %>
                           </div> 
                         </div> 
                       <div class="form-group">
							<div class="col-lg-2">
								<label>Gewog:</label>
							</div>
		                     <div class="col-lg-3" id="TransferGewog">
                   				<%=dto.getTransfereeGewog() %>
                             </div>
						   
						    <div class="col-lg-2">
								<label>Address :</label>
							</div>
			                <div class="col-lg-3" id="TransferAddress">
                 				<%=dto.getTransfereeAddress() %>
                           </div> 
                        </div>    
                    </div> 				
				</div>
			</div> 
		</div>
	</div>
	 <div class="widget-box" id="organization" style="display:none">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Organisation Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label>Owner Name:</label>
							</div>
		                     <div class="col-lg-3" id="transferOwnerName">
                           		<%=dto.getTransfereeName() %>
                             </div>
						   
						    <div class="col-lg-2">
								<label>Dzongkhag :</label>
							</div>
			                <div class="col-lg-3" id="transferDzongkhag">
                 				<%=dto.getTransfereeDzongkhag() %>
                           </div> 
                         </div> 
                       <div class="form-group">
							
						   
						    <div class="col-lg-2">
								<label>Address :</label>
							</div>
			                <div class="col-lg-3" id="transferAddress">
			                	<%=dto.getTransfereeAddress() %>
                           </div> 
                       </div>    
                   </div> 				
				</div>
			</div> 
		</div>
	</div>
	<div class="widget-box">
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
			
			       <div class="form-group">
							<div class="col-lg-2">
								<label>Sale Deed Date :<span style="color: #ff0000">*</span></label>
							</div>
		                     <div class="col-lg-3">
                                 <div class="input-group">
									<html:text property="saleDeedDate" value="<%=dto.getSaleDeedDate() %>" styleClass="form-control date-picker" styleId="saleDeedDate" readonly="true"></html:text>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
								<label style="display:none;color: #ff0000" id="saleDeedDateValidation">Please enter sale deed date</label>
                             </div>
							<div class="col-lg-2">
								<label>Sale Deed Amount :<span style="color: #ff0000">*</span></label>
							</div>
		                     <div class="col-lg-3">
                                 <html:text property="saleDeedAmount" styleClass="form-control" value="<%=dto.getSaleDeedAmount() %>" styleId="saleDeedAmount"></html:text>
                                 <label style="display:none;color: #ff0000" id="saleDeedAmountValidation">Please enter sale deed amount</label>
                             </div>
	                   </div>  
                         	<div style="display: none;">
							     <div class="col-lg-2">
										<label>Remarks :</label>
								 </div>
			                     <div class="col-lg-3">
	                                 <html:textarea property="remarks" styleClass="form-control" styleId="remarks" value="<%=dto.getRemarks() %>"></html:textarea>
	                             </div>
                             </div>
	                       </div> 
			              </div>
	                    </div> 
	                  </div>
	                </div> 
            
                  
		
    <div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Attachments</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
					
					<div class="form-group">
						<div class="col-lg-2">
							<label> Supporting Document:</label>
						</div>
						<div class="col-lg-3">
							<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
							<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							<html:hidden property="transfereeCustomerId" styleId="transfereeCustomerId1"></html:hidden>
						</div>
		              </div>
	                  		 </div> 
	                		</div>
	             	 </div>
		</div>
	</div>
</html:form>
  <div id="citizen" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					

						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="cidPersonalModal" placeholder="CID"  />
                                 </div>
						</div>
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CustomerID"> Customer ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="customerIdPersonalModal" placeholder="CustomerID"  />
                                 </div>
						</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button  type="button" class="btn btn-sm" onclick="searchPersonalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm"  type="button" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="personalListTable1">
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<div id="owner" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Code"> Code : </label>
                            <div class="col-sm-4">
                                 <input type="text" id="codeOrganisationModal" placeholder="Code"  />
                            </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Name"> Name : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="nameOrganisationModal" placeholder="Name"  />
                                 </div>
						</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" type="button"  onclick="searchOrganisationInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" type="button"  data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			
			<div id="organisationListTable">
		   </div>
	  </div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
  
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
			<script type="text/javascript">
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})

				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function() {
					$(this).prev().focus();
				});

				$('.fileupload').ace_file_input({
					no_file : 'No File ...',
					btn_choose : 'Choose',
					btn_change : 'Change',
					droppable : false,
					onchange : null,
					thumbnail : false,
					whitelist:'png|jpg|jpeg',
					blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
				});
	
				$(document).ready(function()
				{
					$('#personalRadio').attr('checked', true);
					$('#personal').show();
					$('#personal1').show();
				});
				
				function showPersonal()
				{
					$("#organization").hide(); 
				    $("#personal").show(); 
					$("#organization1").hide(); 
				    $("#personal1").show(); 
				}
					
				function showOrganisation()
				{
					$("#organization").show(); 
				    $("#personal").hide(); 
					$("#organization1").show(); 
				    $("#personal1").hide(); 
				}

				function searchRenewalInfo()
				{ 
					var ownerType = $('#ownerTypeRenewalModal').val();
			
					var vehicleType = $('#vehicleTypeRenewalModal').val();
					var vehicleNumber = $('#vehicleNumberRenewalModal').val();
					var ownerName = $('#ownerNameRenewalModal').val();
					var engineNumber = $('#engineNumberRenewalModal').val();
					var chasisNumber = $('#chasisNumberRenewalModal').val();
					var cidNumber = $('#citizenIdRenewalModal').val();
			
					if(ownerType == "")
						ownerType = "NA";
					if(vehicleType == "")
						vehicleType = "NA";
					if(vehicleNumber == "")
						vehicleNumber = "NA";
					if(ownerName == "")
						ownerName = "NA";
					if(engineNumber == "")
						engineNumber = "NA";
					if(chasisNumber == "")
						chasisNumber = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
			
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&ownerType="+ownerType+"&vehicleType="+vehicleType+"&vehicleNumber="+vehicleNumber+"&ownerName="+ownerName+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#renewalListTable").html(responseText);
							$("#renewalListTable").show();
						}
					});
				 }

				function searchPersonalInfo()
				{
					var name = $('#namePersonalModal').val();
					var cidNumber = $('#cidPersonalModal').val();
					var customerId = $('#customerIdPersonalModal').val();
					var regionId = $('#regionPersonalModal').val();
					var dzongkhagId = $('#dzongkhagPersonalModal').val();
			 
					if(name == "")
						name = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
					if(customerId == "")
						customerId = "NA";
					if(regionId == "")
						regionId = "NA";
					if(dzongkhagId == "")
						dzongkhagId = "NA";
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList1&name="+name+"&cid="+cidNumber+"&customerId="+customerId+"&region="+regionId+"&dzongkhag="+dzongkhagId+"&searchType=VEHICLE_TRANSFER",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#personalListTable1").html(responseText);
							$("#personalListTable1").show();
						}
					});
				}

				function searchOrganisationInfo()
				{  
					var code = $('#codeOrganisationModal').val();
					var name = $('#nameOrganisationModal').val();
					var type = $('#typeOrganisationModal').val();
					var regionId = $('#regionOrganisationModal').val();
					var dzongkhagId = $('#dzongkhagOrganisationModal').val();	
					
					if(code == "")
						code = "NA";
					if(name == "")
						name = "NA";
					if(type == "")
						type = "NA";
					if(regionId == "")
						regionId = "NA";
					if(dzongkhagId == "")
						dzongkhagId = "NA";
			
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getOrganisationInfoList1&code="+code+"&name="+name+"&type="+type+"&region="+regionId+"&dzongkhag="+dzongkhagId,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#organisationListTable").html(responseText);
							$("#organisationListTable").show();
						}
					});
				  }

				  function searchVehicleNumber()
				  { 
						var vehicleId = $('#vehicleId').val();
						if(vehicleId == "")
							vehicleId = "NA";
						
						$.ajax
						({
							type : "POST",
							url : "<%=request.getContextPath()%>/common.html?method=getTransferHistoryList&vehicleId="+vehicleId,
							data : $('form').serialize(),
							cache : false,
							dataType : "html",
							success : function(responseText) 
							{
								$("#transferHistoryTable").html(responseText);
								$("#transferHistoryTable").show();
							}
						});
				   }

	             $(document).ready(function()
			     {
					$('#submitBtn').click(function()
					{
						var returnVal = validateForm();
	
						if(returnVal == "1")
						{
							var identityTypeId;
						    var requestType = "VEHICLE";
							var serviceType = "TRANSFER";
							var identityNo = $('#vehicleId').val();
							if( $("#engineType").val() == "Electric")
								   identityTypeId = '0'; 
							else
								identityTypeId = $('#vehicleType').val();
							var loadingCapacity = $('#loadCapacity').val();
							var seatingCapacity = $('#seatCapacity').val();
							var vehicleHP = $('#vehicleHorsePower').val();
							var kilowatts = $('#vehicleKiloWatt').val();
							var engineCC = $('#engineCC').val();
							var purchaseDate = "";
							var saleDeedAmount = $('#saleDeedAmount').val();
							var saleDeedDate = $('#saleDeedDate').val();
							
							getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate);
						}
						else
							return false;
					});
				});
	
			function formSubmit()
			{
				var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_transfer',type:'POST',data: $("#vehicleForm").serialize()}; 
			    $("#vehicleForm").ajaxSubmit(options);
		        $('#displayMsgDiv').show();
		        setTimeout('hideStatus("displayMsgDiv")',10000);
		        //setTimeout('reloadPage()',10000);
			}

			function validateForm()
			{
				var vehicleNumber = $('#displayVehicleNumber').val();
				var citizenID = $('#citizenID').val();
				var ownerID = $('#ownerID').val();
				var saleDeedDate = $('#saleDeedDate').val();
				var saleDeedAmount = $('#saleDeedAmount').val();

				$('#vehicleNumberValidation').hide();
				$('#radioValidation').hide();
				$('#citizenIDValidation').hide();
				$('#ownerIDValidation').hide();
				$('#saleDeedDateValidation').hide();
				$('#saleDeedAmountValidation').hide();

				if(vehicleNumber == "")
				{
					$('#vehicleNumberValidation').show();
					$('#vehicleNumberValidation').get(0).scrollIntoView();
					return "0";
				}
				if ($('input[type=radio].ace:checked').length == '0')
				{
					$('#radioValidation').show();
					$('#radioValidation').get(0).scrollIntoView();
					return "0";
				}
				if ($('input[type=radio].ace:checked').val() == 'p')
				{
					if(citizenID == "")
					{
						$('#citizenIDValidation').show();
						$('#citizenIDValidation').get(0).scrollIntoView();
						return "0";
					}
				}
				if ($('input[type=radio].ace:checked').val() == 'o')
				{
					if(ownerID == "")
					{
						$('#ownerIDValidation').show();
						$('#ownerIDValidation').get(0).scrollIntoView();
						return "0";
					}
				}
				if(saleDeedDate == "")
				{
					$('#saleDeedDateValidation').show();
					$('#saleDeedDateValidation').get(0).scrollIntoView();
					return "0";
				}
				if(saleDeedAmount == "")
				{
					$('#saleDeedAmountValidation').show();
					$('#saleDeedAmountValidation').get(0).scrollIntoView();
					return "0";
				}
				else
				{
					return "1";
				}
			}

			var fileError;
           	function validation(thisform,msgId,fileObj)
           	{
           		var fileId = fileObj.id;
           		with(thisform)
           		{
           			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           		}
           	}

	     var context = "<%=request.getContextPath()%>";

</script>
<style>
	#vehicleForm .error { color: red; }
</style>	