<%@page import="bt.gov.rsta.framework.dto.ServiceDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
ServiceDTO dto = (ServiceDTO)request.getAttribute("ServiceDTO");
%>
<div class="widget-box">
	 <div class="widget-header widget-header-small">
		  <h5 class="widget-title lighter">Renewal Details</h5>
	</div>						
</div>	   
<div class="widget-box">
	<div class="widget-body">
    	<div class="widget-main">
            <div class="form-group"> 
        		<div class="col-lg-2">
     				<label>Renewal Type <span style="color: #ff0000">*</span>:</label>
        		</div>
      			<div class="col-lg-3">
      				<html:select property="renewalType" styleClass="form-control" onchange="showAttachment(this.value)" styleId="renewalType">
						<html:option value="N">Normal</html:option> 
						<html:option value="E">Exemption</html:option> 
					</html:select>
      			</div>
      			<div class="form-group">
          			<div class="col-lg-2">
                   		<label>Renewal Duration <span style="color: #ff0000">*</span>:</label>
                   </div>
                   <div class="col-lg-3">
                   		<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
                   			<html:option value="">--SELECT--</html:option>
                   		</html:select>
                   </div>
          		</div>
            	<div style="display: none;">
               		<div class="col-lg-2">
						<label>Remarks :</label>
			   		</div>
	                <div class="col-sm-3">
	                	<html:textarea property="remarks" styleClass="form-control" styleId="remarks" value="NA"></html:textarea>
	          		</div>
                </div>
      		</div>
      	</div>
	</div>
</div>
<div class="widget-box" id="attachmentDIV" style="display: none;">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Attachments</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<div class="row">
								<div class="col-lg-12">
									
									<div class="form-group">
										<div class="col-lg-2">
											<label> Supporting Document <span style="color: #ff0000">*</span>:</label>
										</div>
										<div class="col-lg-3">
											<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
											<label style="display:none;color: #ff0000" id="attchValidation">Please attach your file here</label>
											<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
										</div>
						              </div>
		                     		 </div> 
		                   		</div>
		                	 </div>
						</div>
					</div>	
<script type="text/javascript">
var vehicleTypeDesc =	'<%=dto.getVehicleType()%>';
var renewalDuration =	'<%=dto.getRenewalDuration()%>';
	if(vehicleTypeDesc == "TWO_WHEELER" || vehicleTypeDesc == "LIGHT_VEHICLE")
	{
		$('#renewalDuration').append("<option value='12'>1 Year</option>");
		$('#renewalDuration').append("<option value='24'>2 Years</option>");
		$('#renewalDuration').append("<option value='36'>3 Years</option>");
		$('#renewalDuration').append("<option value='48'>4 Years</option>");
		$('#renewalDuration').append("<option value='60'>5 Years</option>");
		$('#renewalDuration').append("<option value='72'>6 Years</option>");
		$('#renewalDuration').append("<option value='84'>7 Years</option>");
		$('#renewalDuration').append("<option value='96'>8 Years</option>");
		$('#renewalDuration').append("<option value='108'>9 Years</option>");
		$('#renewalDuration').append("<option value='120'>10 Years</option>");
	}
	else
	{
		$('#renewalDuration').append("<option value='6'>6 Months</option>");
		$('#renewalDuration').append("<option value='12'>1 Year</option>");
		$('#renewalDuration').append("<option value='24'>2 Years</option>");
		$('#renewalDuration').append("<option value='36'>3 Years</option>");
		$('#renewalDuration').append("<option value='48'>4 Years</option>");
		$('#renewalDuration').append("<option value='60'>5 Years</option>");
		$('#renewalDuration').append("<option value='72'>6 Years</option>");
		$('#renewalDuration').append("<option value='84'>7 Years</option>");
		$('#renewalDuration').append("<option value='96'>8 Years</option>");
		$('#renewalDuration').append("<option value='108'>9 Years</option>");
		$('#renewalDuration').append("<option value='120'>10 Years</option>");
	}
	$("#renewalDuration").val(renewalDuration);
	
	var rowCount =<%=dto.getRowCount()%>;
	if(rowCount==0)
	{
		$("#renewalType").val('N');
	}
	else
	{
		$("#renewalType").val('E');
		$('#attachmentDIV').show();
	}
	function showAttachment(val)
	{
		if(val == "E")
			$('#attachmentDIV').show();
		else
			$('#attachmentDIV').hide();
	}

	function validation()
	{
		var vehicleNumber = $('#displayVehicleNumber').val();
		var vehicleRegionId	=	$("#vehicleRegionId").val();
		var renewalType = $('#renewalType').val();
		
		$("#regionValidation").hide(); 
		$("#attchValidation").hide();
		$("#vehicleNumberValidation").hide();
		
		if(vehicleNumber == "")
		{
			$('#vehicleNumberValidation').show();
			$('#vehicleNumberValidation').get(0).scrollIntoView();
			return "0";
		} 
		if(vehicleRegionId=="")
		{
 			$("#regionValidation").show(); 
 			$('#regionValidation').get(0).scrollIntoView();
 			return "0";
		}
		if(renewalType=="E")
		{
			var supportingDocument = $('#supportingDocument').val();

			if(supportingDocument == "")
			{
				$("#attchValidation").show(); 
				$('#attchValidation').get(0).scrollIntoView();
				return "0";
			}
			else
			{
				return "1";
			}
		}
		else
		{
			return "1";
		}
		
	}
				   
			   		
					$('.date-picker').datepicker({
						autoclose: true,
						todayHighlight: true
					});

				  $('.fileupload').ace_file_input({
						no_file : 'No File ...',
						btn_choose : 'Choose',
						btn_change : 'Change',
						droppable : false,
						onchange : null,
						thumbnail : false,
						whitelist:'png|jpg|jpeg',
						blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
					});

				function searchRenewalInfo()
				{ 
					var ownerType = $('#ownerTypeRenewalModal').val();
					var vehicleType = $('#vehicleTypeRenewalModal').val();
					var vehicleNumber = $('#vehicleNumberRenewalModal').val();
					var ownerName = $('#ownerNameRenewalModal').val();
					var engineNumber = $('#engineNumberRenewalModal').val();
					var chasisNumber = $('#chasisNumberRenewalModal').val();
					var cidNumber = $('#citizenIdRenewalModal').val();
	
					if(ownerType == "")
						ownerType = "NA";
					if(vehicleType == "")
						vehicleType = "NA";
					if(vehicleNumber == "")
						vehicleNumber = "NA";
					if(ownerName == "")
						ownerName = "NA";
					if(engineNumber == "")
						engineNumber = "NA";
					if(chasisNumber == "")
						chasisNumber = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
	
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&ownerType="+ownerType+"&vehicleType="+vehicleType+"&vehicleNumber="+vehicleNumber+"&ownerName="+ownerName+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#renewalListTable").html(responseText);
							$("#renewalListTable").show();
						}
					});
				}

				function searchVehicleNumber()
				{
					var vehicleId = $('#vehicleId').val();
					 
					if(vehicleId == "")
						vehicleId = "NA";
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalHistoryList&vehicleId="+vehicleId,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#renewalHistoryTable").html(responseText);
							$("#renewalHistoryTable").show();

							//checkIfFitnessNEmissionValid();

							populateRenewalDurationDropdown();
						}
					  });
				 }

				 function populateRenewalDurationDropdown()
				 {
					$('#renewalDuration').empty();
					var vehicleTypeDesc = $('#vehicleTypeDesc').val();

					if(vehicleTypeDesc == "TWO_WHEELER" || vehicleTypeDesc == "LIGHT_VEHICLE")
					{
						$('#renewalDuration').append("<option value='12'>1 Year</option>");
						$('#renewalDuration').append("<option value='24'>2 Years</option>");
						$('#renewalDuration').append("<option value='36'>3 Years</option>");
						$('#renewalDuration').append("<option value='48'>4 Years</option>");
						$('#renewalDuration').append("<option value='60'>5 Years</option>");
					}
					else
					{
						$('#renewalDuration').append("<option value='6'>6 Months</option>");
						$('#renewalDuration').append("<option value='12'>1 Year</option>");
					}
				 }

				function checkIfFitnessNEmissionValid()
				{
					var vehicleId = $('#vehicleId').val();
					 
					if(vehicleId == "")
						vehicleId = "NA";
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/vehicle.html?method=checkIfFitnessNEmissionValid&vehicleId="+vehicleId,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#renewalValidationMsg").html(responseText);
							$("#renewalValidationMsg").show();
							setTimeout('reloadPage()',5000);
						}
					 });
				}

				$('#submitBtn').click(function()
				{ 
					var returnVal = validation();
					//alert(returnVal);
					if(returnVal == 1) 
					{
						var identityTypeId;
						var requestType = "VEHICLE";
						var serviceType = "RENEWAL";
						var identityNo = $('#vehicleId').val();
						//alert($("#engineType").val());
						if( $("#engineType").val() == "Electric")
							identityTypeId = '0'; 
						else
							identityTypeId = $('#vehicleType').val();
						var loadingCapacity = $('#loadCapacity').val();
						var seatingCapacity = $('#seatCapacity').val();
						var vehicleHP = $('#vehicleHorsePower').val();
						var kilowatts = $('#vehicleKiloWatt').val();
						var engineCC = $('#engineCC').val();
						var purchaseDate = "";
						var saleDeedAmount = "";
						var saleDeedDate = "";
						var renewalDuration = $('#renewalDuration').val();
						
						getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
					}
					else 
					{
						return false;
					}
				});

				function formSubmit()
				{
					var renewalType = $('#renewalType').val();
					
					var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_renewal&renewalType='+renewalType,type:'POST',data: $("#vehicleForm").serialize()}; 
				    $("#vehicleForm").ajaxSubmit(options);
			        $('#displayMsgDiv').show();
			        setTimeout('hideStatus("displayMsgDiv")',10000);
				}	
           </script>