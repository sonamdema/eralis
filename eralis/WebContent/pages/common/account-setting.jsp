<%@page import="bt.gov.rsta.eralis.dto.administration.UserDTO"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%
	UserDTO dto = (UserDTO)request.getAttribute("USER_DETAILS");
%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-gears"></i>
		Account Settings
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Change profile details &amp; login credentials
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="space-6"></div>
			<div class="widget-box">
				<div class="widget-header widget-header-flat widget-header-small">
					<h5 class="widget-title">
						<i class="ace-icon fa fa-gears"></i>
						General Account Setting
					</h5>
					<input type="hidden" id="uid" value="<%=dto.getLoginId() %>">
		       		<input type="hidden" id="email" value="<%=dto.getEmailAddress() %>">
		       		<input type="hidden" id="mobileNumber" value="<%=dto.getMobileNumber()%>">
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<ul class="list-group">
		         			<li class="list-group-item">
		         				Name
		         				<span class="pull-right"><%=dto.getUserName() %></span>
		         			</li>
		         			<li class="list-group-item">
		         				Username
		         				<span class="pull-right"><%=dto.getLoginId() %></span>
		         			</li>
		         			<li class="list-group-item">
		         				Password
		         				<span class="pull-right">
		         				<button data-toggle="modal" href="#" data-target="#change-password-modal" class="btn btn-primary btn-xs">
									<i class="ace-icon fa fa-key"></i>
									Change Password
								</button>
								</span>
		         			</li>
		         			<li class="list-group-item">
		         				Email Address
		         				<span class="pull-right">
		         					<%=dto.getEmailAddress() %>
									<button data-toggle="modal" href="#" data-target="#change-email-modal" class="btn btn-primary btn-xs">
										<i class="ace-icon fa fa-pencil fa-fw"></i>
									</button>
		         				</span>
		         			</li>
		         			<li class="list-group-item">
		         				Mobile Number
		         				<span class="pull-right">
		         					<%=dto.getMobileNumber() %>
									<button data-toggle="modal" href="#" data-target="#change-mobile-modal" class="btn btn-primary btn-xs">
										<i class="ace-icon fa fa-pencil fa-fw"></i>
									</button>
		         				</span>
		         			</li>
		         		</ul>	
					</div>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div id="change-password-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Change Password</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div id="changePasswordMsgDiv" style="display: none"></div>
						<form>
							<div class="form-group">
								<label>Login Id:</label>
								<span class="pull-right">
									<label><%=dto.getLoginId() %></label>
								</span>
							</div>
							<div class="form-group">
								<label>Current Password:</label>
								<span class="pull-right">
									<input type="password" class="form-control" id="cpassword" placeholder="Current Password"/>
								</span>
							</div>
							<div class="form-group">
								<label>New Password:</label>
								<span class="pull-right">
									<input type="password" class="form-control" id="npassword" placeholder="New Password"/>
								</span>
							</div>
							<div class="form-group">
								<label>Confirm Password:</label>
								<span class="pull-right">
									<input type="password" class="form-control" id="password_confirmation" placeholder="Confirm Password"/>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>

				<button type="button" class="btn btn-sm btn-primary" onclick="changePassword()">
					<i class="ace-icon fa fa-check"></i>
					Change
				</button>
			</div>
		</div>
	</div>
</div><!-- Modal End -->

<div id="change-email-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Change Email Address</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div id="emailChangeMsgDiv" style="display: none"></div>
						<form>
							<div class="form-group">
								<label>Login Id:</label>
								<span class="pull-right">
									<label><%=dto.getLoginId() %></label>
								</span>
							</div>
							<div class="form-group">
								<label>New Email Address:</label>
								<span class="pull-right">
									<input type="text" class="form-control" id="nemail" placeholder="New Email Address"/>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>

				<button type="button" class="btn btn-sm btn-primary" onclick="changeEmail()">
					<i class="ace-icon fa fa-check"></i>
					Change
				</button>
			</div>
		</div>
	</div>
</div><!-- Modal End -->

<div id="change-mobile-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Change Mobile Number</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div id="changeMobileMsgDiv" style="display: none"></div>
						<form>
							<div class="form-group">
								<label>Login Id:</label>
								<span class="pull-right">
									<label><%=dto.getLoginId() %></label>
								</span>
							</div>
							<div class="form-group">
								<label>New Mobile Number:</label>
								<span class="pull-right">
									<input type="text" class="form-control" id="nmobile" placeholder="New Mobile Number"/>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>

				<button type="button" class="btn btn-sm btn-primary" onclick="changeMobileNumber()">
					<i class="ace-icon fa fa-check"></i>
					Change
				</button>
			</div>
		</div>
	</div>
</div><!-- Modal End -->

<script>

function changePassword()
{
	var uid = $('#uid').val();
	var cpassword = $('#cpassword').val();
	var newpassword = $('#npassword').val();
	var password_confirmation = $('#password_confirmation').val();

	if(cpassword == "")
	{
		$('#changePasswordMsgDiv').html("");
		$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Current password cannot be empty</div>");
      	$('#changePasswordMsgDiv').show();
      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
      	return;
	}
	if(newpassword == "")
	{
		$('#changePasswordMsgDiv').html("");
		$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New password cannot be empty</div>");
      	$('#changePasswordMsgDiv').show();
      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
      	return;
	}
	if(password_confirmation == "")
	{
		$('#changePasswordMsgDiv').html("");
		$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Password Confirmation cannot be empty</div>");
      	$('#changePasswordMsgDiv').show();
      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
      	return;
	}
	if(cpassword == newpassword)
	{
		$('#changePasswordMsgDiv').html("");
		$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New password and Current password cannot be the same</div>");
      	$('#changePasswordMsgDiv').show();
      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
      	$('#npassword').val("");
      	$('#cpassword').val("");
      	$('#password_confirmation').val("");
      	return;
	}
	if(newpassword != password_confirmation)
	{
		$('#changePasswordMsgDiv').html("");
		$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New password and Password Confirmation doesnot match</div>");
      	$('#changePasswordMsgDiv').show();
      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
      	$('#npassword').val("");
      	$('#password_confirmation').val("");
      	return;
	}
	else
	{
		$.ajax({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=changePassword&uid='+uid+'&cpassword='+cpassword+'&newpassword='+newpassword,	
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var data = $(this).find('status').text();	
					
					if(data == "ERROR")
					{
						$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Password could not be changed</div>");
				      	$('#changePasswordMsgDiv').show();
				      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
				      	$('#npassword').val("");
				      	$('#cpassword').val("");
				      	$('#password_confirmation').val("");
					}
					else if(data == "NOT_UPDATED")
					{
						$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Password could not be changed</div>");
				      	$('#changePasswordMsgDiv').show();
				      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
				      	$('#npassword').val("");
				      	$('#cpassword').val("");
				      	$('#password_confirmation').val("");
					}
					else if(data == "NO_MATCH")
					{
						$('#changePasswordMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Current password you entered is not correct</div>");
				      	$('#changePasswordMsgDiv').show();
				      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
				      	$('#npassword').val("");
				      	$('#cpassword').val("");
				      	$('#password_confirmation').val("");
					}
					else if(data == "UPDATED")
					{
						$('#changePasswordMsgDiv').html("<div class='alert alert-success'><i class='ace-icon fa fa-check'></i> Password has been changed successfully</div>");
				      	$('#changePasswordMsgDiv').show();
				      	setTimeout('hideStatus("changePasswordMsgDiv")',3000);
				      	$('#npassword').val("");
				      	$('#cpassword').val("");
				      	$('#password_confirmation').val("");
				      	setTimeout('reloadPage()',2000);
					}
				});
			}
		});
	}
}

function changeEmail()
{
	var uid = $('#uid').val();
	var nemail = $('#nemail').val();
	var cemail = $('#email').val();

	if(nemail == "")
	{
		$('#emailChangeMsgDiv').html("");   
		$('#emailChangeMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New email address cannot be empty</div>");
      	$('#emailChangeMsgDiv').show();
      	
      	return;
	}
	if(nemail == cemail)
	{
		$('#emailChangeMsgDiv').html("");
		$('#emailChangeMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New email address and Current email address cannot be the same</div>");
      	$('#emailChangeMsgDiv').show();
      	$('#nemail').val("");
      	return;
	}
	else
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=changeEmailAddress&uid='+uid+'&nemail='+nemail,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var data = $(this).find('status').text();	
					
					if(data == "FAILURE")
					{
						$('#emailChangeMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Email address couldnot be changed</div>");
				      	$('#emailChangeMsgDiv').show();
				      	$('#nemail').val("");
					}
					else if(data == "SUCCESS")
					{
						$('#emailChangeMsgDiv').html("<div class='alert alert-success'><i class='ace-icon fa fa-check'></i> Email address has been changed successfully</div>");
				      	$('#emailChangeMsgDiv').show();
				      	$('#nemail').val("");
				      	setTimeout('reloadPage()',2000);
					}
				});
			}
		});
	}
}

function changeMobileNumber()
{
	var uid = $('#uid').val();
	var nmobileNumber = $('#nmobile').val();
	var cmobileNumber = $('#mobileNumber').val();

	if(nmobileNumber == "")
	{
		$('#changeMobileMsgDiv').html("");
		$('#changeMobileMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New mobile number cannot be empty</div>");
      	$('#changeMobileMsgDiv').show();
      	
      	return;
	}
	if(nmobileNumber == cmobileNumber)
	{
		$('#changeMobileMsgDiv').html("");
		$('#changeMobileMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> New mobile number and Current mobile number cannot be the same</div>");
      	$('#changeMobileMsgDiv').show();

      	$('#nmobile').val("");
      	return;
	}
	else
	{
		$.ajax({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=changeMobileNumber&uid='+uid+'&nmobilenumber='+nmobileNumber,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var data = $(this).find('status').text();	
					
					if(data == "FAILURE")
					{
						$('#changeMobileMsgDiv').html("<div class='alert alert-danger'><i class='ace-icon fa fa-times'></i> Mobile number could not be changed</div>");
				      	$('#changeMobileMsgDiv').show();
				      	$('#nmobile').val("");
					}
					else if(data == "SUCCESS")
					{
						$('#changeMobileMsgDiv').html("<div class='alert alert-success'><i class='ace-icon fa fa-check'></i> Mobile number has been changed successfully</div>");
				      	$('#changeMobileMsgDiv').show();
				      	$('#nmobile').val("");
				      	setTimeout('reloadPage()',2000);
					}
				});
			}
		});
	}
}

<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
%>

	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";


</script>
