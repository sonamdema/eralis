<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>CID</th> 
			<th>Name</th>
			<th>License No</th>
			<th>Region</th>
			<th>Issue Date</th> 
			<th>Expiry Date</th> 
			
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_LICENSE_LIST">
			<logic:iterate id="renewal" name="RENEWAL_LICENSE_LIST">
				<tr>
					<td>
						<input type="radio" name="renewalListRadio" id='<bean:write name="renewal" property="drivinglicenseId"/>' onclick="addSelected(this.id)"/>
						
					</td>
					<td><bean:write name="renewal" property="name"/></td> 
					<td><bean:write name="renewal" property="CID"/></td>
					<td><bean:write name="renewal" property="licenseNo"/></td>
					<td><bean:write name="renewal" property="region"/></td>
					<td><bean:write name="renewal" property="dateOfissue"/></td>
					<td><bean:write name="renewal" property="expiryDate"/></td> 
					
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="RENEWAL_LICENSE_LIST">
			<tr>
				<td colspan="7" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>
<div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>

<script>
	var globalId = "";
	function addSelected(id)
	{
		globalId = id;
	}

	function getSelected()
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getlicenserenewalInfoList&drivinglicenseId='+globalId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var name = $(this).find('name').text();
					var bloodgroup = $(this).find('bloodgroup').text();
					var dob = $(this).find('dob').text();
					var cid=$(this).find('cid').text();
					var fathersname=$(this).find('fathersname').text();
					var gender=$(this).find('gender').text();
					var dzongkhag=$(this).find('dzongkhag').text();
					var village=$(this).find('village').text();
					var gewog=$(this).find('gewog').text();
					var customerId=$(this).find('customerId').text();
					var licenseno=$(this).find('licenseno').text();
					var region=$(this).find('region').text();

					$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
					$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
					$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
					$('#cid').html("<label class='control-label'>"+cid+"</label>");
					$('#gender').html("<label class='control-label'>"+gender+"</label>");
					$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
					$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
					$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
					$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
					$('#displayCustomerId').html("<label class='control-label'>"+customerId+"</label>");
					$('#displayregion').html("<label class='control-label'>"+region+"</label>");
					$('#licenseNo').val(licenseno);
					$('#customerId').val(customerId);
					$('#region').val(region);
					searchlicenserenewallist();
					
					searchlicenseduplicationlist();

					searchlicenseendorsementlist();

				});
			}
		});
	}

</script>