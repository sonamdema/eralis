<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
EralisCommonDTO personalDTO = (EralisCommonDTO)request.getAttribute("PERSONALDTO");
%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<div id="edit_PIS" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Edit Personal Details</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					 <html:form styleClass="form-horizontal"action="/eralis_common.html" styleId="personalForm">
						<div class="widget-box">
							<div class="widget-header widget-header-small">
								<h5 class="widget-title lighter">Personal Information</h5>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<div class="row">
										<div class="col-lg-12">
											<div class="pull-right">
												<div id="pic" style="display:none;"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<html:hidden property="customerID" styleClass="form-control" styleId="customerID"></html:hidden>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Citizen ID <i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<div class="input-group">
														<html:text property="CID" styleClass="form-control" value="<%=personalDTO.getCID()%>" styleId="cid"></html:text>
														 
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Title of Courtesy<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6" >
													<html:select property="titleOfcourtesy"
														styleClass="form-control" styleId="titleOfcourtesy">
														<html:option value="">--SELECT--</html:option>
														<html:optionsCollection name="titleOfcourtesyList"
															label="headerName" value="headerId"  />
													</html:select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>First Name<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6" >
													<html:text property="firstname" styleId="firstname" value="<%=personalDTO.getFirstname()%>"
														styleClass="form-control"  ></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Middle Name:</label>
												</div>
												<div class="col-lg-6" >
													<html:text property="middlename" styleId="middlename" value="<%=personalDTO.getMiddlename()%>"
														styleClass="form-control"></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Last Name:</label>
												</div>
												<div class="col-lg-6" >
													<html:text property="lastName" styleId="lastName" value="<%=personalDTO.getLastName()%>"
														styleClass="form-control" ></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3" >
													<label>Occupation<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:select property="occupation" styleClass="form-control" styleId="personalOccupation">
														<html:option value="">--SELECT--</html:option>
														<html:optionsCollection name="personalOccupationList"
															label="headerName" value="headerId" />
													</html:select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Nationality<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:select property="nationality" styleClass="form-control" styleId="personalNationality">
														<html:option value="-1">--SELECT--</html:option>
														<html:optionsCollection name="personalNationalityList" label="headerName"
															value="headerId" />
													</html:select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Date Of Birth<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<div class="input-group">
														<html:text property="DOB"
															styleClass="form-control date-picker" value="<%=personalDTO.getDOB()%>"
															styleId="displayDob" readonly="true"></html:text>
														<span class="input-group-addon"> <i
															class="fa fa-calendar bigger-110"></i> </span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Father's Name<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:text property="fathersName" styleId="fathersName"	 value="<%=personalDTO.getFathersName()%>"			
														styleClass="form-control" readonly="true"></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Blood Group<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:select property="bloodGroup" styleClass="form-control" styleId="bloodGroup">
														<html:option value="">--SELECT--</html:option>
														<html:optionsCollection name="bloodgroupList" label="headerName"
															value="headerId" />
													</html:select>
													
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Gender<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:radio property="gender" value="M" styleId="maleRadio" styleClass="ace">
														<span class="lbl"> Male</span>
													</html:radio>
													<html:radio property="gender" value="F" styleId="femaleRadio" styleClass="ace">
														<span class="lbl"> Female</span>
													</html:radio>
												</div>
											</div>
											<div class="form-group">
												<div style="display: none;">
													<div class="col-lg-3">
														<label>Identification Marks:</label>
													</div>
													<div class="col-lg-6">
														<html:text property="identificationMarks"  value="<%=personalDTO.getIdentificationMarks()%>"
															styleId="identificationMarks" styleClass="form-control" value="NA"></html:text>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Remarks:</label>
												</div>
												<div class="col-lg-6">
													<html:textarea property="remarks" styleId="remarks" value="<%=personalDTO.getRemarks()%>"
														styleClass="form-control"></html:textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="widget-box">
							<div class="widget-header widget-header-small">
								<h5 class="widget-title lighter">Permanent Address</h5>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="radio">
													<label class="col-sm-3 no-padding-right">
														<html:radio property="national" styleId="nationalRadio" onclick="enable('National')" styleClass="ace"
															value="N"></html:radio> <span class="lbl"> National</span> </label>
													<label class="col-sm-3 no-padding-right">
														<html:radio property="national" styleId="internationalRadio" onclick="enable('International')" styleClass="ace"
															value="I"></html:radio> <span class="lbl">
															International</span> </label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Dzongkhag<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<select id="dzongkhag" class="form-control"  onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N')" >
														<option value="">--SELECT--</option>
														<logic:iterate id="dzongkhag" name="dzongkhagList">
															<option value='<bean:write name="dzongkhag" property="headerId"/>'><bean:write name="dzongkhag" property="headerName"/></option>
														</logic:iterate>
													</select>
												</div>
												<div id="requiredDzongkhag" style="display:none;" class="error">Please select Dzongkhag</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Gewog<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<select id="gewog" class="form-control">
														<option value="">--SELECT--</option>
													</select>
												</div>
												<div  id="requiredGewog" style="display:none;" class="error">Please select Gewog</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Village<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:text property="village" styleId="village" value="<%=personalDTO.getVillage()%>"
														styleClass="form-control" readonly="true"></html:text>
												</div>
												<div   class="error" id="requiredVillage" style="display:none;" >Please select Village</div>
												 
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Country:</label>
												</div>
												<div class="col-lg-6">
													<html:select property="country" styleClass="form-control" styleId="country">
														<html:option value="">--SELECT--</html:option>
														<html:optionsCollection name="countryList" label="headerName"
															value="headerId" />
													</html:select>
												</div>
												<div id="requiredCountry" style="display:none;" class="error">Please select Country</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Address:</label>
												</div>
												<div class="col-lg-6">
													<html:text property="address" styleId="address"  value="<%=personalDTO.getAddress()%>"
														styleClass="form-control"></html:text>
												</div>
												<div id="requiredAddress" style="display:none;"  class="error">Please enter Address</div>
											</div>
				
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget-box">
							<div class="widget-header widget-header-small">
								<h5 class="widget-title lighter">Contact Address</h5>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<div class="col-lg-3">
													<label>Dzongkhag<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:select property="presentDzongkhag" styleClass="form-control" styleId="presentDzongkhag">
														<html:option value="">--SELECT--</html:option>
														<html:optionsCollection name="dzongkhagList" label="headerName"
															value="headerId" />
													</html:select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Contact address<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:text property="contactAddress" styleId="contactAddress" value="<%=personalDTO.getContactAddress()%>"
														styleClass="form-control"></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Phone<i class="light-red">*</i> :</label>
												</div>
												<div class="col-lg-6">
													<html:text property="phone" styleId="phone" value="<%=personalDTO.getPhone()%>"
														styleClass="form-control"></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Email:</label>
												</div>
												<div class="col-lg-6">
													<html:text property="email" styleId="email" value="<%=personalDTO.getEmail()%>"
														styleClass="form-control"></html:text>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-3">
													<label>Upload Photo:</label>
												</div>
												<div class="col-lg-6">
													<html:file property="upload" styleClass="form-control"
														styleId="id-input-file-2"></html:file>
												</div>
											</div>
										</div>
									</div>
								</div>
				
							</div>
						</div>
						<div id="messageDiv"></div>
						<div class="pull-right">
							<html:hidden property="dzongkhag" styleId="dzongkhagHidden"/>
							<html:hidden property="personalInfoId" styleId="personalInfoId" value="<%=personalDTO.getPersonalInfoId() %>"/>
							<html:hidden property="gewog" styleId="gewogHidden"/>
							<html:hidden property="manualFlag" styleId="manualFlag" value="N"/>
							<button type="button" class="btn btn-primary btn-sm" onclick="editPersonalInfo()">Update</button>
						</div>
					</html:form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- PAGE CONTENT ENDS -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<style>
	#personalForm .error { color: red; }
</style>
<script>
	var gender	=	'<%=personalDTO.getGender()%>';
	var imagePath	=	'<%=personalDTO.getImagePath()%>';
	var	isInternational	=	'<%=personalDTO.getIsInternational()%>';
	$("#titleOfcourtesy").val('<%=personalDTO.getTitleOfcourtesy()%>');
	$("#personalOccupation").val('<%=personalDTO.getOccupation()%>');
	$("#personalNationality").val('<%=personalDTO.getNationality()%>');
	$("#bloodGroup").val('<%=personalDTO.getBloodgroup()%>');
	$("#dzongkhag").val('<%=personalDTO.getDzongkhag()%>');
	populateDependentDropDown('<%=personalDTO.getDzongkhag()%>', 'gewog', '', 'GEWOG_LIST', 'N');
	$("#gewog").val('<%=personalDTO.getGewog()%>');
	$("#country").val('<%=personalDTO.getCountry()%>');
	$("#presentDzongkhag").val('<%=personalDTO.getPresentDzongkhag()%>');
	if(gender == "M")
 		$('#maleRadio').attr('checked', true);
	else
		$('#femaleRadio').attr('checked', true);
	
	if(isInternational == "N")
 		$('#nationalRadio').attr('checked', true);
	else
		$('#internationalRadio').attr('checked', true);

	if(imagePath != "null")
		{
			$('#pic').html('<span class="profile-picture"><img class="editable img-responsive" id="" style="width:150px; height: 150px;" src="<%=request.getContextPath() %>/ImageServlet?url='+imagePath+'"/></span>');
		$('#pic').show();
		}
		else
		{
			$('#pic').html('');
		$('#pic').hide();
 	}


	
	function editPersonalInfo()
	{
		$('#dzongkhagHidden').val($('#dzongkhag').val());
		$('#gewogHidden').val($('#gewog').val());
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=edit_personal_info',type:'POST',data: $("#personalForm").serialize()}; 
	    $("#personalForm").ajaxSubmit(options);
	    $('#messageDiv').show();
	    $('#messageDiv').get(0).scrollIntoView();
	}
	
</script>
