<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<div class="row">
	<div class="col-lg-12">
		<ul class="list-group">
			<logic:present  name="uploadedDocsList" scope="request">
	   			<logic:iterate id="documentVO" name="uploadedDocsList" indexId="index">
	   				<li class="list-group-item">
	  					<a href="#" onclick="downloadFile('<bean:write name="documentVO" property="uuid" filter="false"/>','<bean:write name="documentVO" property="name" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="documentVO" property="name" filter="false"/>
	  					</a>
	 				</li>
				</logic:iterate>
          	</logic:present>
       	</ul>	
	</div>
</div>

<script>

	function downloadFile(uuid,fileName)
	{
		 window.open('<%=request.getContextPath()%>/FileDownloadServlet?uuid='+uuid+'&fileName='+fileName,'','width=600,height=600');
	}	

</script>