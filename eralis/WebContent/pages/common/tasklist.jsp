<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
</head>
<body class="no-skin">
		<!-- header include -->
		<jsp:include page="/pages/common/header.jsp"></jsp:include>
		<!-- ./header include -->

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- menu include -->
			<jsp:include page="/pages/common/menu.jsp"></jsp:include>
			<!-- ./menu include -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<!-- <ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul> /.breadcrumb -->
						
						<div class="nav-search" id="nav-search">
							<i class="ace-icon fa fa-calendar fa-lg"></i> 
							<%
							 	String DATE_FORMAT_NOW = "dd-MM-yyyy";
								Calendar cal = Calendar.getInstance();

								SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
							    String day = sdfDay.format(cal.getTime());
							    
							    SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
							    String year = sdfYear.format(cal.getTime());
							    
							    Calendar mCalendar = Calendar.getInstance();    
							    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
							%>
							<span class="blue"><%=day%>&nbsp;<%=month %>,&nbsp;<%=year %></span>
						</div>
					</div>

					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="page-header">
								<h1>
									<i class="ace-icon fa fa-tachometer"></i> Task List
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
										overview &amp; stats
									</small>
									<!--<a class="btn btn-primary btn-sm pull-right" onclick="openModal('view-status-modal')">
	               						<i class="fa fa-eye fa-fw"></i>&nbsp;
	               						View Application Status
	               					</a>-->
								</h1>
							</div><!-- /.page-header -->
							
							<html:form action="/tasklist.html" method="post" styleClass="form-horizontal">
							<div class="row">
								<div class="col-xs-12">
									<div class="widget-box">
										<div class="widget-header">
											<h5 class="widget-title lighter">Filter Selection Criteria</h5>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="row">
												    <div class="col-lg-12">
												    
												    	<div class="form-group">
												    		<div class="col-lg-3">
												    			<label>Select Service:</label>
												    		</div>
												    		<div class="col-lg-3">
												    			<html:select property="filterServiceCode" styleClass="form-control" title="Filters applications of the selected service" styleId="idServiceSelect" onchange="populateStatus()">
											  						<html:option value="">All</html:option>                                 
								              						<logic:present name="svc_status_list" scope="session">
								                  						<html:options collection="svc_status_list" property="serviceCode" labelProperty="serviceName"/>
								              						</logic:present>					
																</html:select>
												    		</div>
												    		<div class="col-lg-3">
												    			<label>Select Status:</label>
												    		</div>
												    		<div class="col-lg-3">
												    			<html:select property="filterStatusCode" styleClass="form-control" title="Based on service selection, the available status will be shown" styleId="idStatusSelect">
											  						<html:option value="">All</html:option>                                 				
																</html:select>
												    		</div>
												    	</div>
												    	<div class="form-group">
												    		<div class="col-lg-3">
												    			<label>Select Limit:</label>
												    		</div>
												    		<div class="col-lg-3">
												    			<html:select property="limitRows" styleClass="form-control" title="Limit rows to the selected number of applications" styleId="idLimitSelect">
																	<html:option value="9999">All</html:option>  
																	<html:option value="10">10</html:option>
																	<html:option value="15">15</html:option>
																	<html:option value="20">20</html:option>
																</html:select>
												    		</div>
												    	</div>
												    	<div class="pull-right">
												    		<button type="button" class="btn btn-primary btn-sm" onclick="filter()">Filter</button>
												    	</div>
												    </div>
											    </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							</html:form>
							
							<div id="taskListAccordion">
								<jsp:include page="/pages/common/tasklist-content.jsp"></jsp:include>
							</div>
							
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			
			<div id="view-status-modal" class="modal" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="blue bigger">Find/Filter Application Status</h4>
						</div>
						 <div class="modal-body">
							<div class="row">
								<div class="col-xs-12"> 
									<form class="form-horizontal">
										<div class="form-group">
											<input type="radio" id="dlRadio" name="typeRadio" value="DL" class="ace" onclick="showDiv(this.id)">
											<span class="lbl">Driving License</span>
											&nbsp;
											<input type="radio" id="vRadio" name="typeRadio" value="VEHICLE" class="ace" onclick="showDiv(this.id)">
											<span class="lbl">Vehicle</span>
											&nbsp;
											<input type="radio" id="llRadio" name="typeRadio" value="LL" class="ace" onclick="showDiv(this.id)">
											<span class="lbl">Learner License</span>
										</div>
										<div class="form-group" id="dlDiv" style="display:none;">
											<div class="col-lg-2">
												<label class="control-label">Service</label>
											</div>
											<div class="col-lg-4">
												<select id="dlDropDown" class="form-control" onchange="showSearchParamter(this.value)">
													<option value="0">--SELECT--</option>
													<option value="NONCOMMERCIAL">Issuance of Non-commercial License</option>
													<option value="COMMERCIAL">Issuance of Commercial License</option>
													<option value="LICENSERENEWAL">Renewal of Driving License</option>
													<option value="LICENSEDUPLICATION">Replacement of Driving License</option>
													<option value="LICENSEENDORSE">Endorsement of Driving License</option>
												</select>
											</div>
											<div id="licenseNoDiv" style="display:none;">
												<div class="col-lg-2">
													<label class="control-label">License No</label>
												</div>
												<div class="col-lg-4">
													<input type="text" id="dlNumber" class="form-control"/>
												</div>
											</div>
											<div id="applNoDiv" style="display:none;">
												<div class="col-lg-2">
													<label class="control-label">Appl No</label>
												</div>
												<div class="col-lg-4">
													<input type="text" id="applNumber" class="form-control"/>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="col-xs-12" id="resultDiv"></div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-sm" onclick="searchApplStatus()">
								<i class="ace-icon fa fa-search"></i>
								Search
							</button>
							<button class="btn btn-sm" data-dismiss="modal">
								<i class="ace-icon fa fa-times"></i>
								Cancel
							</button>
					  	</div>
				    	<div id="personalListTable"></div>
			    	</div>
			 	</div>
			</div><!-- PAGE CONTENT ENDS -->

			<!-- footer include -->
			<jsp:include page="/pages/common/footer.jsp"></jsp:include>
			<!-- /.footer include -->
			
		</div><!-- /.main-container -->
		
		
		<script>

		$(document).ready(function()
	    {
			filter();
	    });
   
			function filter()
			{
				$.blockUI
			    ({ 
			    	css: 
			    	{ 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			    	} 
			    });  

				$.ajax
				({
					type : "POST",
					url : '<%= request.getContextPath()%>/tasklist.html?q=filterApplication',
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#taskListAccordion").html(responseText);
						$("#taskListAccordion").show();
						setTimeout($.unblockUI, 1000); 
					}
				});
			  }

			  function showDiv(id)
			  {
				  $('#dlDiv').hide();
				  $('#vDiv').hide();
				  $('#llDiv').hide();
				  
				if(id == "dlRadio")
					$('#dlDiv').show();
				else if(id == "vRadio")
					$('#vDiv').show();
				else if(id == "llRadio")
					$('#llDiv').show();
			  }

			function showSearchParamter(val)
			{
				$('#applNoDiv').hide();
				$('#licenseNoDiv').hide();
				
				if(val == "NONCOMMERCIAL" || val == "COMMERCIAL")
					$('#applNoDiv').show();
				else 
					$('#licenseNoDiv').show();
			}

			function searchApplStatus()
			{
				var requestType = $('input[type=radio].ace:checked').val();
				var serviceType = "", identityNo = "";

				if(requestType == "DL")
				{
					serviceType = $('#dlDropDown').val();

					if(serviceType == "NONCOMMERCIAL" || serviceType == "COMMERCIAL")
						identityNo = $('#applNumber').val();
					else
						identityNo = $('#dlNumber').val();
				}

				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getApplicationStatus&requestType="+requestType+"&serviceType="+serviceType+"&identityNo="+identityNo,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#resultDiv").html(responseText);
						$("#resultDiv").show();
					}
				});
			}

		</script>
		
	</body>
</html>