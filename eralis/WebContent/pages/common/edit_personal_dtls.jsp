 <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<div id="edit_PIS" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
			<div class="row">
					 <html:form styleClass="form-horizontal" action="/eralis_common.html" styleId="personalForm">

		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="pull-right">
								<div id="pic" style="display:none;"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<html:hidden property="customerID" styleClass="form-control" styleId="customerID"></html:hidden>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Citizen ID <i class="light-red">*</i> :</label>
								</div><%int count=1; %>
								<div class="col-lg-6">
									<div class="input-group">
										<html:text property="CID" styleClass="form-control" styleId="cid"></html:text>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Title of Courtesy<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6" >
									<html:select property="titleOfcourtesy"
										styleClass="form-control" styleId="titleOfcourtesy">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="titleOfcourtesyList"
											label="headerName" value="headerId"  />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>First Name<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6" >
									<html:text property="firstname" styleId="firstname"
										styleClass="form-control"  ></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Middle Name:</label>
								</div>
								<div class="col-lg-6" >
									<html:text property="middlename" styleId="middlename"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Last Name:</label>
								</div>
								<div class="col-lg-6" >
									<html:text property="lastName" styleId="lastName"
										styleClass="form-control" ></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3" >
									<label>Occupation<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:select property="occupation" styleClass="form-control" styleId="personalOccupation">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="personalOccupationList"
											label="headerName" value="headerId" />
									</html:select>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-3">
									<label>Date Of Birth<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<div class="input-group">
										<html:text property="DOB"
											styleClass="form-control date-picker"
											styleId="displayDob" readonly="true"></html:text>
										<span class="input-group-addon"> <i
											class="fa fa-calendar bigger-110"></i> </span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Father's Name<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:text property="fathersName" styleId="fathersName"
										styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Blood Group<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:select property="bloodGroup" styleClass="form-control" styleId="bloodGroup">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="bloodgroupList" label="headerName"
											value="headerId" />
									</html:select>
									
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Gender<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:radio property="gender" value="M" styleId="maleRadio" styleClass="ace">
										<span class="lbl"> Male</span>
									</html:radio>
									<html:radio property="gender" value="F" styleId="femaleRadio" styleClass="ace">
										<span class="lbl"> Female</span>
									</html:radio>
								</div>
							</div>
							<div class="form-group">
								<div style="display: none;">
									<div class="col-lg-3">
										<label>Identification Marks:</label>
									</div>
									<div class="col-lg-6">
										<html:text property="identificationMarks"
											styleId="identificationMarks" styleClass="form-control" value="NA"></html:text>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-6">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Permanent Address</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="radio">
									<label class="col-sm-3 no-padding-right">
										<html:radio property="national" styleId="nationalRadio" onclick="enable('National')" styleClass="ace"
											value="N"></html:radio> <span class="lbl"> Bhutanese</span> </label>
									<label class="col-sm-3 no-padding-right">
										<html:radio property="national" styleId="internationalRadio" onclick="enable('International')" styleClass="ace"
											value="I"></html:radio> <span class="lbl">
											Foreign National</span> </label>
								</div>
							</div>
							<div class="form-group" id="dzongkhag_div">
								<div class="col-lg-3">
									<label>Dzongkhag<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<select id="dzongkhag" class="form-control"  onchange="populateDependentDropDown(this.value, 'gewog', '', 'GEWOG_LIST', 'N')" >
										<option value="">--SELECT--</option>
										<logic:iterate id="dzongkhag" name="dzongkhagList">
											<option value='<bean:write name="dzongkhag" property="headerId"/>'><bean:write name="dzongkhag" property="headerName"/></option>
										</logic:iterate>
									</select>
								</div>
								<div id="requiredDzongkhag" style="display:none;" class="error">Please select Dzongkhag</div>
							</div>
							<div class="form-group" id="gewog_div" id="gewog_div">
								<div class="col-lg-3">
									<label>Gewog<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<select id="gewog" class="form-control">
										<option value="">--SELECT--</option>
									</select>
								</div>
								<div  id="requiredGewog" style="display:none;" class="error">Please select Gewog</div>
							</div>
							<div class="form-group" id="village_div">
								<div class="col-lg-3">
									<label>Village<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:text property="village" styleId="village" styleClass="form-control" readonly="true"></html:text>
								</div>
								<div   class="error" id="requiredVillage" style="display:none;" >Please select Village</div>
								 
							</div>
							<div class="form-group" id="country_div">
								<div class="col-lg-3">
									<label>Country:</label>
								</div>
								<div class="col-lg-6">
									<html:select property="country" styleClass="form-control" styleId="country">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="countryList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
								<div id="requiredCountry" style="display:none;" class="error">Please select Country</div>
							</div>
							<div class="form-group" id="nationality_div">
								<div class="col-lg-3">
									<label>Nationality<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:select property="nationality" styleClass="form-control" styleId="personalNationality">
										<html:option value="-1">--SELECT--</html:option>
										<html:optionsCollection name="personalNationalityList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
							</div>
							<div class="form-group" id="address_div">
								<div class="col-lg-3">
									<label>Address:</label>
								</div>
								<div class="col-lg-6">
									<html:text property="address" styleId="address"
										styleClass="form-control"></html:text>
								</div>
								<div id="requiredAddress" style="display:none;"  class="error">Please enter Address</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Contact Address</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<div class="radio">
									<label class="col-sm-4 no-padding-right">
										   <html:radio property="contactRadio" styleId="officialRadio" onclick="enable('Official')" styleClass="ace"
											value="Official"></html:radio> <span class="lbl"> Official</span> </label>
									<label class="col-sm-4 no-padding-right">
										   <html:radio property="contactRadio" styleId="privateRadio" onclick="enable('Private')" styleClass="ace"
											value="Private"></html:radio> <span class="lbl">
										    Business/Personal</span> </label>
								     </div>
							 </div>
							<div class="form-group" id="ministryDIV" style="display:none">
								<div class="col-lg-3">
									<label>Ministry:</label>
								</div>
								<div class="col-lg-6">
								<html:select property="ministry" styleClass="form-control" styleId="ministry" onchange="populateDependentDropDown(this.value, 'department', '', 'DEPARTMENT_LIST', 'N')">
										<html:option value="0">--SELECT--</html:option>
						   				<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
								</html:select>
								</div>
								<div id="requiredMinistry" style="display:none;" class="error">Please select Ministry</div>
							</div>
							<div class="form-group" id="departmentDIV" style="display:none">
								<div class="col-lg-3">
									<label>Department:</label>
								</div>
								<div class="col-lg-6">
									<html:select property="department" styleClass="form-control" styleId="department">
										<html:option value="0">--SELECT--</html:option>
									</html:select>
								</div>
								<div id="requiredDepartment" style="display:none;" class="error">Please select Department</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Dzongkhag<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:select property="presentDzongkhag" styleClass="form-control" styleId="presentDzongkhag">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="dzongkhagList" label="headerName"
											value="headerId" />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Contact address<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:text property="contactAddress" styleId="contactAddress"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Phone<i class="light-red">*</i> :</label>
								</div>
								<div class="col-lg-6">
									<html:text property="phone" styleId="phone"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Email:</label>
								</div>
								<div class="col-lg-6">
									<html:text property="email" styleId="email"
										styleClass="form-control"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3">
									<label>Upload Photo:</label>
								</div>
								<div class="col-lg-6">
									<html:file property="upload" styleClass="form-control"
										styleId="id-input-file-2"></html:file>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<div id="messageDiv"></div>
		
		<div class="pull-right">
			<html:hidden property="dzongkhag" styleId="dzongkhagHidden"/>
			<html:hidden property="personalInfoId" styleId="personalInfoId"/>
			<html:hidden property="gewog" styleId="gewogHidden"/>
			<html:hidden property="manualFlag" styleId="manualFlag" value="N"/>

			 
			<button type="button" class="btn btn-primary btn-sm" onclick="editPersonalInfo()">Update</button>
			 
		</div>
		
	</html:form>
				</div>
			</div>
			 
		 
		</div>
	</div>
</div>
<!-- PAGE CONTENT ENDS -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<style>
	#personalForm .error { color: red; }
</style>
<script>
	function editPersonalInfo()
	{
		$('#dzongkhagHidden').val($('#dzongkhag').val());
		$('#gewogHidden').val($('#gewog').val());
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/eralis_common.html?method=edit_personal_info',type:'POST',data: $("#personalForm").serialize()}; 
	    $("#personalForm").ajaxSubmit(options);
	    $('#messageDiv').show();
	    $('#messageDiv').get(0).scrollIntoView();

	    
	    var drivinglicenseId	=	$("#drivinglicenseId").val();
	  
	}
 
</script>
