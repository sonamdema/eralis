<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");
%>
<script>
function formSubmit()
	{
		var application=$("#applicationNo").val();
		 
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=passenger_bus_route_permit_approval&applicationNo='+application, type:'POST',data: $("#vehicleForm").serialize()}; 

	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}

function formReject()
    {
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=renewal_application_reject&applicationNo='+application,type:'POST',data: $("#vehicleForm").serialize()}; 
	
	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
     }
 
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Route Permit
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			     Passenger Bus Route Permit Application
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	<div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Passenger Bus Route Permit Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
						 
		                   <div class="form-group ">
		                     <div class="col-lg-12">	
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >CID No. : </label>
										<label class="col-lg-2 no-padding-right" style="text-align: left;"><%=dto.getCitizenID() %> </label>
										<label class="col-lg-2 no-padding-right" >Applicant Name : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getName() %> </label>
									</div>
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Date of Birth : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getDob() %> </label>
										<label class="col-lg-2 no-padding-right" >Gender : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getGender() %> </label>
									</div>
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Permanent Dzongkhag : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getDzongkhag() %> </label>
										<label class="col-lg-2 no-padding-right" >Permanent Gewog : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getGewog() %> </label>
									</div>
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Permanent Village : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getVillage() %> </label>
										<label class="col-lg-2 no-padding-right" for="Mobile">Present Mobile No : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getPhone() %> </label>
									</div>
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Present Email : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getEmail() %> </label>
										<label class="col-lg-2 no-padding-right" >Present Address : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getAddress() %> </label>
									</div>
									
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Route Type : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getType() %> </label>
										<label class="col-lg-2 no-padding-right" >Bus Category : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getBusType() %> </label>
									</div>
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Route From : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getRouteFrom() %> </label>
										<label class="col-lg-2 no-padding-right" >Route To : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getRouteTo() %> </label>
									</div>
									<div class="form-group col-lg-12">
										<label class="col-lg-2 no-padding-right" >Timing : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getTiming() %> </label>
										<label class="col-lg-2 no-padding-right" >Transport Name : </label>
										<label class="col-lg-2 no-padding-right"  style="text-align: left;"><%=dto.getTransport() %> </label>
									</div>
								</div>
                         </div>  
                       </div>   
		                    
        </html:form>
        <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	 </div>
<input type="hidden" id="applicationNo"/>
<div id="displayMsgDiv"></div>

<div>
	<logic:equal value="APPROVE" name="param">
	<button type="button" class="btn btn-sm" onclick="formSubmit()">
		<i class="ace-icon fa fa-check"  ></i>
		Approve
	</button>
	<button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
		<i class="ace-icon fa fa-times red2"></i>
		Reject
	</button>
	</logic:equal>
	<logic:equal value="DISPATCH" name="param">
	<button type="button" class="btn btn-sm" onclick="dispatch()">
		<i class="ace-icon fa fa-check"  ></i>
		Dispatch
	</button>
	</logic:equal>
 </div>