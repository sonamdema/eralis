<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
 <%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Passenger Bus Inspection
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Pre-Departure/Post-Arrival Inspection Form
		</small>
	</h1>
</div>
 <div class="row">
  <div class="col-lg-12">
   <html:form styleClass="form-horizontal" action="/vehicle.html" styleId="inspectionForm">
   		  
   		  <div class="row">
   		  	<div id="renewalValidationMsg"></div>
   		  	<div id="incompleteValidationMsg"></div>
   		  	<div id="Msg"></div>
   		  </div>
		  <div class="widget-box">
		    <div class="widget-body">
			  <div class="widget-main">
				<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
				 <div class="form-group">
					 <div class="col-lg-2">
						<label>Bus Number:<span style="color: #ff0000">*</span></label>
					 </div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
								<html:hidden property="vehicleId" styleClass="form-control" styleId="vehicleId" ></html:hidden>
								  <span class="input-group-btn">
							      	 <a href="#" onclick="openModal('renewal')" class="btn btn-purple btn-sm">
								  <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
									</a>
								  </span>
							 </div>
						  </div>
						  <div class="col-lg-3">
						  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
						  </div>
					    </div>
				     </div>
				   </div>
				</div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main" id="Personal">
				
	             <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
            </div>
         </div>
					
    <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Bus Details</h5>
		</div>						
		<div class="widget-body">
			  <div class="widget-main">
					<div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Registration Date:</label>
						</div>					
					         <div class="col-lg-3"  id="displayLastRegistrationDate">
                             </div>
					    <div class="col-lg-2">
							<label class=control-label>Expiry Date :</label>
						</div>
			             <div class="col-lg-3" id="displayExpiryDate"></div> 
                	</div> 
					<div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Bus Type:</label>
						</div>					
					         <div class="col-lg-3"  id="passangerBusType">
                             </div>
					    <div class="col-lg-2">
							<label class=control-label>Transport Name:</label>
						</div>
			             <div class="col-lg-3" id="displayExpiryDate"></div> 
                	</div> 
             	</div>
             </div>
        </div>
      	<div class="widget-box">
			<div class="widget-header widget-header-small">
		       <h5 class="widget-title lighter">Inspection Details</h5>
	       	</div>						
           	<div class="widget-body">
	         	<div class="widget-main">
                    <div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">License No:</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
								<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
								<span class="input-group-btn">
									<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
										<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="col-lg-2">
							<label class="control-label">Driver Name:</label>
						</div>
						<div class="col-lg-3">
							<div id="driverName"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Inspection Type:</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:select property="inspectionType" styleId="inspectionType" styleClass="form-control" onchange="selectInspection(this.value)">
                           			<html:option value="">Select Inspection Type</html:option>
                           			<html:option value="PRE">Pre-Departure Inspection</html:option>
                           			<html:option value="POST">Post-Arrival Inspection</html:option>
                           		</html:select>
							</div>
						</div>
						
						<div class="col-lg-2">
							<label class="control-label">Driver Type:</label>
						</div>
						<div class="col-lg-3">
							<div id="drivetypelist"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Is Drug Abuse :</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:select property="isDrug" styleId="isDrug" styleClass="form-control">
                           			<html:option value="N">No</html:option>
                           			<html:option value="Y">Yes</html:option>
                           		</html:select>
							</div>
						</div>
						<div class="col-lg-2">
							<label class="control-label">Alcohol Test:</label>
						</div>
						<div class="col-lg-3">
							<html:text property="alcoholTest" styleClass="form-control" styleId="alcoholTest"></html:text>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Departure From :</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:select property="departureFrom" styleClass="form-control" styleId="departureFrom" >
							      	<html:option value="">--SELECT--</html:option>
					   		    	<html:optionsCollection name="routeList" label="headerName" value="headerId"/>
					          	</html:select>
								<%-- <html:text property="departureFrom" styleClass="form-control" styleId="departureFrom"></html:text> --%>
							</div>
						</div>
						<div class="col-lg-2">
							<label class="control-label">Destination:</label>
						</div>
						<div class="col-lg-3">
								<html:select property="destination" styleClass="form-control" styleId="destination" >
							      	<html:option value="">--SELECT--</html:option>
					   		    	<html:optionsCollection name="routeList" label="headerName" value="headerId"/>
					          	</html:select>
							<%-- <html:text property="destination" styleClass="form-control" styleId="destination"></html:text> --%>
						</div>
					</div>
					<div class="form-group departureDateTime">
						<div class="col-lg-2">
							<label class="control-label">Departure Time :</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<div class="input-group bootstrap-timepicker">
									<html:text property="departureTime" styleClass="form-control timepicker" styleId="departureTime"></html:text>
									<span class="input-group-addon">
										<i class="fa fa-clock-o bigger-110"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-2">
							<label class="control-label">Departure Date:</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="departureDate" readonly="true" styleClass="form-control date-picker" styleId="departureDate"></html:text>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group arrivalDateTime ">
						<div class="col-lg-2">
							<label class="control-label">Arrival Time :</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<div class="input-group bootstrap-timepicker">
									<html:text property="arrivalTime" styleClass="form-control timepicker" styleId="arrivalTime"></html:text>
									<!--<html:text property="departureTime" styleClass="form-control timepicker" styleId="departureTime"></html:text>
									 <input id="timepicker1" type="text" class="form-control" /> -->
									<span class="input-group-addon">
										<i class="fa fa-clock-o bigger-110"></i>
									</span>
								</div>
								
							</div>
						</div>
						<div class="col-lg-2">
							<label class="control-label">Arrival Date:</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="arrivalDate" styleClass="form-control date-picker" readonly="true" styleId="arrivalDate"></html:text>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
						</div>
					</div>
             	 </div>
	  		</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
		       <h5 class="widget-title lighter">Passenger Details</h5>
	       	</div>						
           	<div class="widget-body">
	         	<div class="widget-main">
                    
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">No. of Male :</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="noMale" styleClass="form-control" styleId="noMale"></html:text>
							</div>
						</div>
						<div class="col-lg-2">
							<label class="control-label">No. of Female :</label>
						</div>
						<div class="col-lg-3">
							<html:text property="noFemale" styleClass="form-control" styleId="noFemale"></html:text>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">No. of Children :</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="noChildren" styleClass="form-control" styleId="licenseNO"></html:text>
							</div>
						</div>
					</div>
             	 </div>
	  		</div>
		</div>
				
				
				<div class="widget-box" id="attachmentDIV" style="display: none;">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Attachments</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<div class="row">
								<div class="col-lg-12">
									
									<div class="form-group">
										<div class="col-lg-2">
											<label> Supporting Document <span style="color: #ff0000">*</span>:</label>
										</div>
										<div class="col-lg-3">
											<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
											<label style="display:none;color: #ff0000" id="attchValidation">Please attach your file here</label>
											<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
										</div>
						              </div>
		                     		 </div> 
		                   		</div>
		                	 </div>
						</div>
					</div>	
					<div class="row">
						<div id="displayMsgDiv"></div>
					</div>
					<div class="pull-left"  style="margin-bottom:50px">
						<html:hidden property="engineType" styleId="engineType"></html:hidden>
						<html:hidden property="vehicleType" styleId="vehicleType"></html:hidden>
						<input type="hidden" id="vehicleTypeDesc"/>
						<input type="hidden" id="vehicleRegistrationCode"/>
						<html:hidden property="loadCapacity" styleId="loadCapacity"></html:hidden>
						<html:hidden property="seatCapacity" styleId="seatCapacity"></html:hidden>
						<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower"></html:hidden>
						<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt"></html:hidden>
						<html:hidden property="engineCC" styleId="engineCC"></html:hidden>
						<html:hidden property="customerId" styleId="customerID"></html:hidden>
						<html:hidden property="vehicleRegistrationType" styleId="ownerType"></html:hidden>
						<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
						<logic:equal value="Y" name="priviledge" property="isNew">
						   <button type="button" class="btn btn-primary btn-sm" onclick="formSubmit()">Save Inspection Details</button>
						</logic:equal>
					</div>
		</html:form>
	</div>
  </div>

<div id="renewal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
	
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
	                         <form class="form-horizontal" role="form">
							<div class="form-group">
									<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
		
	                                 <div class="col-sm-4">
	                                      <input type="text" id="searchVehicleNo" placeholder="Vehicle Number"  />
	                                 </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	                                <div class="col-sm-4">
	                                     <input type="text" id="searchCID" placeholder="Citizen ID"  />
	                                </div>
							</div>
						</form>
					</div>
				</div>
			 </div>
			<div class="modal-footer">	
				<button class="btn btn-sm" name="search" onclick="searchVehicleDtls()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="vehicleList">
		   </div>
		 </div>
	  </div>
</div>

<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>License No:</label>
								<span class="pull-right">
									<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
								</span>
							</div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
		</div>
	</div>
</div>
			
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-timepicker.min.js"></script>

<script>
	function selectInspection(inspectionType)
	{
		if(inspectionType=='POST')
		{
			$(".departureDateTime").hide();
			$(".arrivalDateTime").show();
			$("#departureTime").val('');
			$("#departureDate").val('');
		}
		else if(inspectionType=='PRE')
		{
			$(".departureDateTime").show();
			$(".arrivalDateTime").hide();
			$("#arrivalTime").val('');
			$("#arrivalDate").val('');
		}
	}
</script>
<script type="text/javascript">

	$('.date-picker').datepicker({
		autoclose : true,
		todayHighlight : true
	});
	
	

	$('#departureTime').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#departureTime').timepicker('showWidget');
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	$('#arrivalTime').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#arrivalTime').timepicker('showWidget');
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
	
	
	
	function searchPersonalInfo()
	{
		var cidNumber 	= $('#cidPersonalModal').val();
		var licenseNo	= $('#licenseNoModal').val();
		
		if(cidNumber == "")
			cidNumber = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=PASSANGER_BUS",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#licenseDetailList").html(responseText);
				$("#licenseDetailList").show();
			}
		});
	} 

	function searchVehicleDtls()
	{ 
		var vehicleNumber = $('#searchVehicleNo').val();
		var engineNumber = '';
		var chasisNumber = '';
		var cidNumber = 	$('#searchCID').val();
	
		if(vehicleNumber == "")
			vehicleNumber = "NA";
		if(engineNumber == "")
			engineNumber = "NA";
		if(chasisNumber == "")
			chasisNumber = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
	
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=NA&searchType=BUS_INSPECTION",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#vehicleList").html(responseText);
				$("#vehicleList").show();
			}
		});
	}
 
		

	function formSubmit()
	{
		var renewalType = $('#renewalType').val();
		
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=saveInspectionDtls',type:'POST',data: $("#inspectionForm").serialize()}; 
	    $("#inspectionForm").ajaxSubmit(options);
        $('#displayMsgDiv').show();
        setTimeout('hideStatus("displayMsgDiv")',4000);
        setTimeout('reloadPage()',4000);
	}	
				
</script>

			   
           
    