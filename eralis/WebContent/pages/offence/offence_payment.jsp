<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<style>
	.datepicker{z-index:1151 !important;}
</style>
<div id="payment-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body">
				
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-large">
								<h3 class="widget-title grey lighter">
									<i class="ace-icon fa fa-money orange"></i>
										Payment Details
								</h3>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-24">
									<div>
										<table class="table table-striped table-bordered" id="normal" >
											<tbody>
												<tr>
													<td>Amount:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" name="amount" class="form-control input-mask-phone" id="amount"/>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="hr hr8 hr-double hr-dotted"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-3">
								<label>Receipt No<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-6">
								<input type="text" id="reciptNo"class="form-control" autocomplete="off" name="receiptNo">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label>Receipt Date<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-6">
								<div class="input-group">
									<input type="text" class="form-control date-picker" id="reciptDate"  autocomplete="off" name="receiptDate">
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal" onClick="formSubmit()">
					<i class="ace-icon fa fa-tick"></i>
					submit
				</button>
			</div>
			
		</div>
	</div>
</div>