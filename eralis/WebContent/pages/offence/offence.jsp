<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
<% String repeatedOffenceTypeId = (String) request.getAttribute("repeatedOffenceTypeId"); %>
<script>
	var context = "<%=request.getContextPath()%>";
	var repeatedOffenceTypeId = "<%=repeatedOffenceTypeId%>";
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		 Offence Information
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm" enctype="multipart/form-data">
			<div class="form-group">
				<div class="radio">
					<label class="col-sm-3 no-padding-right">
						<html:radio property="offenceLicesneType" styleId="newOffence"  styleClass="ace"
							value="DL" onclick="showNewOffence()"></html:radio> <span class="lbl">Bhutanese Vehicle Offence</span> 
					</label>
					<label class="col-sm-2 no-padding-right">
						<html:radio property="offenceLicesneType" styleId="foreignOffence"  styleClass="ace"
							value="Foreign" onclick="showForeignOffence()"></html:radio> <span class="lbl">Foreign Vehicle offence</span> 
					</label>
					<label class="col-sm-2 no-padding-right">
						<html:radio property="offenceLicesneType" styleId="foreignOffence"  styleClass="ace"
							value="Foreign" onclick="showPedestrianOffence()"></html:radio> <span class="lbl">Pedestrian Offence</span> 
					</label>
					<label class="col-sm-2 no-padding-right">
						<html:radio property="offenceLicesneType" styleId="updatePayment"  styleClass="ace"
							value="LL" onclick="showUpdatePayment()"></html:radio> <span class="lbl">Search Offence</span> 
					</label>
				</div>
			</div>
			<jsp:include page="/pages/offence/offence_payment.jsp"></jsp:include>
			<div id="newOffence_form">
				<div class="widget-box">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Search Offence</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Vehicle No <span class="text-danger">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="vehicleNo" styleClass="form-control" styleId="newDisplayVehicleNumber" readonly="true"></html:text>
											<html:hidden property="vehicleId" value="0" styleClass="form-control" styleId="newVehicleId"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('newLicenseoffenceVehicleModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-lg-3">
										<label>Vehicle Owner:</label>
									</div>
									<div class="col-lg-3" id="displayOwnerName">
										
									</div>
								</div>
								<div class="form-group" >
									<div class="col-lg-2">
										<label class="control-label">License No:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="licenseNo" styleClass="form-control" styleId="newLicenseNo" readonly="true"></html:text>
											<html:hidden property="drivinglicenseId" styleClass="form-control" value="0" styleId="newDrivinglicenseId"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('newLicenseModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<div class="col-lg-2">
										<label>Driver:</label>
									</div>
									<div class="col-lg-3" id="newLicenseHolderName"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Learner License No:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="licenseNo" styleClass="form-control" styleId="newLearnerLicenseNo" readonly="true"></html:text>
											<html:hidden property="learnerLicenseId" value="0"  styleClass="form-control" styleId="newLearnerLicenseId"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('newLearnerModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<div class="col-lg-2">
										<label>Driver:</label>
									</div>
									<div class="col-lg-3" id="LearnerHolderName"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">CID No.:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<input type="text" id="offenceCID" readonly="readonly" class="form-control">
											<html:hidden property="customerId"  styleClass="form-control" styleId="newCustomerID"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('personalModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<div class="col-lg-2">
										<label>Driver:</label>
									</div>
									<div class="col-lg-3" id="offenceDriverName"></div>
								</div>
							</div>
						</div>
					</div>
				  </div>
			   </div>
			</div>
			<div id="foreign_vehicle_offense_form" style="display: none;" >
			  <div class="widget-box">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Foreign Vehicle offense</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Vehicle Number <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
									    <html:text property="vehicleNoFor" styleId="vehicleNoFor" styleClass="form-control"></html:text>
									</div>
									<div class="col-lg-2">
										<label>Vehicle Owner <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
									    <html:text property="ownerNameFor" styleId="ownerNameFor" styleClass="form-control"></html:text>
									</div>
									</div>
								<div class="form-group" >
									<div class="col-lg-2">
										<label class="control-label">License No <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
									   <html:text property="licenseNoFor" styleId="licenseNoFor" styleClass="form-control"></html:text>
									</div>
									<div class="col-lg-2">
										<label>Driver <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
									     <html:text property="diverNameFor" styleId="diverNameFor" styleClass="form-control"></html:text>
                                     </div>
								</div>
								<div class="form-group" >
									<div class="col-lg-2">
										<label class="control-label">Mobile No <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
									   <html:text property="mobileNoFor" styleId="mobileNoFor" styleClass="form-control"></html:text>
									</div>
								   <div class="col-lg-2">
								      <label>Vehicle Type <span style="color: #ff0000">*</span> :</label>
							       </div>	
		                           <div class="col-lg-3">
			                        <html:select property="vehicleType" styleClass="form-control" styleId="vehicleType">
										   <html:option value="">--SELECT--</html:option>
										   <html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
								   </html:select>
                                </div> 
							</div>
						 </div>
				        </div>
					</div>
			     </div>
	         </div>
		</div>
		<div id="pedestrian_offense_form" style="display: none;" >
			  <div class="widget-box">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Foreign Vehicle offense</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group" >
									<div class="col-lg-3">
										<label class="control-label">Driving License/CID/Passport No. <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
									   <html:text property="CID" styleId="pCID" styleClass="form-control"></html:text>
									</div>
								   <div class="col-lg-2">
								      <label>Name <span style="color: #ff0000">*</span> :</label>
							       </div>	
		                           <div class="col-lg-3">
				                        <html:text property="name" styleId="pName" styleClass="form-control"></html:text>
	                                </div> 
								</div>
								<div class="form-group" >
									<div class="col-lg-3">
										<label class="control-label">Mobile No <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
									   <html:text property="mobile" styleId="pMobile" styleClass="form-control"></html:text>
									</div>
								   <div class="col-lg-2">
								      <label>Address <span style="color: #ff0000">*</span> :</label>
							       </div>	
		                           <div class="col-lg-3">
				                        <html:text property="address" styleId="pAddress" styleClass="form-control"></html:text>
	                                </div> 
								</div>
						 	</div>
				        </div>
					</div>
			     </div>
	         </div>
	         <div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Offence Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								
								<div class="row">
									<div class="col-lg-12">
										<table class="table table-striped table-bordered table-hover col-lg-9" id="offenceTableP">
			                               <thead>
			                                   <tr>
			                                       <th>#</th>
			                                       <th>
			                                       	Offence
			                                       	<span class="pull-right">
														<button type="button" class="btn btn-success btn-sm" onclick="addRow('offenceTableP')">
															<i class="fa fa-plus bigger-110"></i>&nbsp;Add More
														</button>
														&nbsp;
														<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow('offenceTableP')">
															<i class="fa fa-times bigger-110"></i>&nbsp;Delete
														</button>
													</span>
			                                       </th>
			                                   </tr>
			                               </thead>
			                               <tbody>
			                               		<nested:iterate id="offence" property="offenceList" name="licenseForm" indexId="index">
													<% int a = index.intValue(); %>
													<nested:nest property="offence">
														<tr>
															<td>1</td>
															<td>
																<nested:select property="offenceId" name="offence" styleId='<%="offenceId_"+a %>' styleClass="form-control" indexed="true">
																	<html:option value="0">--SELECT--</html:option>
																	<html:optionsCollection name="OffenceList" label="headerName" value="headerId" />
																</nested:select>
															</td>
														</tr>
													</nested:nest>
												</nested:iterate>
			                               </tbody>
			                            </table>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Offence Date:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="offencedateP" styleClass="form-control date-picker" styleId="offenceDateFor"></html:text>
											<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i> </span>
										</div>
									</div>
									<div class="col-lg-3">
										<label class="control-label">Time of Inspections <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group bootstrap-timepicker">
											<html:text property="timeofInspectionP" styleClass="form-control" styleId="timepicker2"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Inspected By <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="inspectedbyP" styleId="" onchange="change_inspectedTypeP(this.value)" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="RSTA">RSTA</html:option>
											<html:option value="TRAFFIC_POLICE">Traffic Police</html:option>
										</html:select>
									</div>
									<div style="display:none;"  id="regionDisplayP">
										<div class="col-lg-3">
											<label class="control-label">Region:</label>
										</div>
										<div class="col-lg-3">
											<html:select property="regionP" styleClass="form-control" styleId="regionP" onchange="populateDependentDropDown(this.value, 'baseoffice', '', 'BASE_OFFICE_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
											   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</div>
									</div>
									<div style="display:none" id="trafficDisplayP">
										<div class="col-lg-3">
											<label class="control-label">Traffic Branch:</label>
										</div>
										<div class="col-lg-3">
											<html:text property="trafficbranchP" styleId="trafficbranchP"
												styleClass="form-control"></html:text>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Inspection Type <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="inspectedTypeP" styleId="inspectedType" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="NORMAL">Normal</html:option>
											<html:option value="HIGHWAY">Highway Inspection</html:option>
										</html:select>
									</div>
									<div class="col-lg-3">
										<label>Place Of Inspection:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="placeofInspectionP" styleId="placeofInspection"
											styleClass="form-control"></html:text>
									</div>
								</div>
								
								<div class="form-group">
									
									<div class="col-lg-3">
										<label>Document Seized :</label>
									</div>
									<div class="col-lg-3">
										<div class="checkbox">
	                                      	<label>
	                                            <html:checkbox property="learnerlicense" styleClass="ace"></html:checkbox>
												<span class="lbl"> Learner/License</span>
	                                        </label>
	                                        <label>
	                                        	<html:checkbox property="bluebook" styleClass="ace"></html:checkbox>
												<span class="lbl"> CID/Passport</span>
	                                        </label>
			
			                             </div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label>TIN Nobb<span style="color: #ff0000">*</span>:</label>
									</div>
									
									<div class="col-lg-3">
										<select class="form-control">
											<option value="T">T</option>
											<option value="P">P</option>
											<option value="S">S</option>
											<option value="G">G</option>
											<option value="M">M</option>
										</select>
										<html:text property="TINnoP" styleId="TINnoP" styleClass="form-control"></html:text>
									</div>
									<div class="col-lg-3">
										<label>Remarks:</label>
									</div>
									<div class="col-lg-3">
										<html:textarea property="remarksP" styleId="remarksP"
											styleClass="form-control"></html:textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Offence Picture 1</label>
									</div>
									<div class="col-lg-3">
										<html:file property="offence1" styleId="offence1" styleClass="form-control fileupload" onchange="validation('learnerForm','pic1',this)"></html:file>
										<label style="display:none;color: #ff0000" id="pic1"></label>
									</div>
									<div class="col-lg-3">
										<label>Offence Picture 2</label>
									</div>
									<div class="col-lg-3">
										<html:file property="offence2" styleId="offence2" styleClass="form-control fileupload" onchange="validation('learnerForm','pic2',this)"></html:file>
										<label style="display:none;color: #ff0000" id="pic2"></label>
									</div>
								</div>
							</div>
							<div id="displayMsgDivForP" class="col-lg-12">&nbsp;</div>
							<div class="pull-right">
								<button type="button" class="btn btn-primary btn-sm"  onClick="submitOffence('P')">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="offense_form" style="display: none;">
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Offence Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								
								<div class="row">
									<div class="col-lg-12">
										<table class="table table-striped table-bordered table-hover col-lg-9" id="offenceTable" style="  margin-bottom: 6px;">
			                               <thead>
			                                   <tr>
			                                       <th>#</th>
			                                       <th>
				                                       	Offence
			                                       </th>
			                                   </tr>
			                               </thead>
			                               <tbody>
			                               		<nested:iterate id="offenceB" property="offenceListB" name="licenseForm" indexId="index">
													<% int a = index.intValue(); %>
													<nested:nest property="offence">
														<tr>
															<td>1</td>
															<td>
																<nested:select property="offenceId" name="offenceB" onchange="checkDoubleOffence(this.value,'BHUTANESE',this.id)" styleId='<%="bhutanese_offenceId_"+a %>' styleClass="form-control" indexed="true">
																	<html:option value="0">--SELECT--</html:option>
																	<html:optionsCollection name="OffenceList" label="headerName" value="headerId" />
																</nested:select>
																<div id="repeatedOffenceMessage_0" class="text-danger" style="display:none">Repeated Offence</div>
																<input type="hidden" id="isOffenceRepeated_0" value="N">
															</td>
														</tr>
													</nested:nest>
												</nested:iterate>
			                               </tbody>
			                            </table>
			                            	<span class="pull-right" style="margin-bottom: 18px;">
													<button type="button" class="btn btn-success btn-sm" onclick="addRow('offenceTable')">
														<i class="fa fa-plus bigger-110"></i>&nbsp;Add Offence
													</button>
													&nbsp;
													<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow('offenceTable')">
														<i class="fa fa-times bigger-110"></i>&nbsp;Delete Offence
													</button>
												</span>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Offence Date:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="offencedate" styleClass="form-control date-picker" styleId="offenceDate"></html:text>
											<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i> </span>
										</div>
									</div>
									<div class="col-lg-3">
										<label class="control-label">Time of Inspection <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group bootstrap-timepicker">
											<html:text property="timeofInspection" styleClass="form-control" styleId="timepicker1"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Inspected By <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="inspectedby" styleId="" onchange="change_inspectedType(this.value)" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="RSTA">RSTA</html:option>
											<html:option value="TRAFFIC_POLICE">Traffic Police</html:option>
										</html:select>
									</div>
									<div style="display:none;"  id="regionDisplay">
										<div class="col-lg-3">
											<label class="control-label">Region:</label>
										</div>
										<div class="col-lg-3">
											<html:select property="region" styleClass="form-control" styleId="newregion" onchange="populateDependentDropDown(this.value, 'baseoffice', '', 'BASE_OFFICE_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
											   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</div>
									</div>
									<div style="display:none" id="trafficDisplay">
										<div class="col-lg-3">
											<label class="control-label">Traffic Branch:</label>
										</div>
										<div class="col-lg-3">
											<html:text property="trafficbranch" styleId="newtrafficbranch"
												styleClass="form-control"></html:text>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Inspection Type <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="inspectedType" styleId="" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="NORMAL">Normal</html:option>
											<html:option value="HIGHWAY">Highway Inspection</html:option>
										</html:select>
									</div>
									<div class="col-lg-3">
										<label>Place Of Inspection:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="placeofInspection" styleId=""
											styleClass="form-control"></html:text>
									</div>
								</div>
								
								<div class="form-group">
									
									<div class="col-lg-3">
										<label>Document Seized :</label>
									</div>
									<div class="col-lg-3">
										<div class="checkbox">
	                                      	<label>
	                                            <html:checkbox property="learnerlicense" styleClass="ace"></html:checkbox>
												<span class="lbl"> Learner/License</span>
	                                        </label>
	                                        <label>
	                                        	<html:checkbox property="bluebook" styleClass="ace"></html:checkbox>
												<span class="lbl"> Blue Book</span>
	                                        </label>
			
			                             </div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label>TIN No<span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<div class="col-lg-2 no-padding">
											<html:select property="tinPrefix" styleId="bhutaneseVehTinPrefix" onchange="validateTin('bhutaneseVehOffence')" styleClass="form-control no-padding">
												<html:option value=""></html:option>
												<html:option value="T">T</html:option>
												<html:option value="P">P</html:option>
												<html:option value="G">G</html:option>
												<html:option value="S">S</html:option>
												<html:option value="M">M</html:option>
											</html:select>
										</div>
										<div class="col-lg-10 no-padding">
											<html:text property="TINno" styleId="bhutaneseVehTinNo"  onchange="validateTin('bhutaneseVehOffence')" styleClass="form-control"></html:text>
										</div>
										<div class="bhutaneseTinDuplicateMessages error hidden"></div>
									</div>
									
									<div class="col-lg-3">
										<label>Remarks:</label>
									</div>
									<div class="col-lg-3">
										<html:textarea property="remarks" styleId=""
											styleClass="form-control"></html:textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Offence Picture 1</label>
									</div>
									<div class="col-lg-3">
										<html:file property="offence3" styleId="offence3" styleClass="form-control fileupload" onchange="validation('learnerForm','pic1',this)"></html:file>
										<label style="display:none;color: #ff0000" id="pic1"></label>
									</div>
									<div class="col-lg-3">
										<label>Offence Picture 2</label>
									</div>
									<div class="col-lg-3">
										<html:file property="offence4" styleId="offence4" styleClass="form-control fileupload" onchange="validation('learnerForm','pic2',this)"></html:file>
										<label style="display:none;color: #ff0000" id="pic2"></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="displayMsgDivForB" class="col-lg-12">&nbsp;</div>
			<div class="alert alert-danger col-lg-12" id="validateRepeatedOffenceMsg" style="display:none">Please select Repeated Offence</div>
			<div class="pull-right">
				<button type="button" class="btn btn-primary btn-sm" id="bOffenceBhutan" onClick="submitOffence('B')">Save</button>
			</div>
		</div>
		
		<div id="offense_form_foreign" style="display: none;">
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Offence Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								
								<div class="row">
									<div class="col-lg-12">
										<table class="table table-striped table-bordered table-hover col-lg-9" id="offenceTableFor">
			                               <thead>
			                                   <tr>
			                                       <th>#</th>
			                                       <th>
			                                       	Offence
			                                       	<span class="pull-right">
														<button type="button" class="btn btn-success btn-sm" onclick="addRow('offenceTableFor')">
															<i class="fa fa-plus bigger-110"></i>&nbsp;Add More
														</button>
														&nbsp;
														<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow('offenceTableFor')">
															<i class="fa fa-times bigger-110"></i>&nbsp;Delete
														</button>
													</span>
			                                       </th>
			                                   </tr>
			                               </thead>
			                               <tbody>
			                               		<nested:iterate id="offenceF" property="offenceListF" name="licenseForm" indexId="index">
													<% int a = index.intValue(); %>
													<nested:nest property="offence">
														<tr>
															<td>1</td>
															<td>
																<nested:select property="offenceId" name="offenceF" styleId='<%="offenceId_"+a %>' styleClass="form-control" indexed="true">
																	<html:option value="0">--SELECT--</html:option>
																	<html:optionsCollection name="OffenceList" label="headerName" value="headerId" />
																</nested:select>
															</td>
														</tr>
													</nested:nest>
												</nested:iterate>
			                               </tbody>
			                            </table>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Offence Date:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="offencedateFor" styleClass="form-control date-picker" styleId="offenceDateFor"></html:text>
											<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i> </span>
										</div>
									</div>
									<div class="col-lg-3">
										<label class="control-label">Time of Inspections <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group bootstrap-timepicker">
											<html:text property="timeofInspectionFor" styleClass="form-control" styleId="timepicker2"></html:text>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Inspected By <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="inspectedbyFor" styleId="" onchange="change_inspectedTypefor(this.value)" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="RSTA">RSTA</html:option>
											<html:option value="TRAFFIC_POLICE">Traffic Police</html:option>
										</html:select>
									</div>
									<div style="display:none;"  id="regionDisplayfor">
										<div class="col-lg-3">
											<label class="control-label">Region:</label>
										</div>
										<div class="col-lg-3">
											<html:select property="regionfor" styleClass="form-control" styleId="regionfor" onchange="populateDependentDropDown(this.value, 'baseoffice', '', 'BASE_OFFICE_LIST', 'N')">
													<html:option value="0">--SELECT--</html:option>
											   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</div>
									</div>
									<div style="display:none" id="trafficDisplayfor">
										<div class="col-lg-3">
											<label class="control-label">Traffic Branch:</label>
										</div>
										<div class="col-lg-3">
											<html:text property="trafficbranchFor" styleId="trafficbranchFor"
												styleClass="form-control"></html:text>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label class="control-label">Inspection Type <span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="inspectedTypeFor" styleId="inspectedType" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="NORMAL">Normal</html:option>
											<html:option value="HIGHWAY">Highway Inspection</html:option>
										</html:select>
									</div>
									<div class="col-lg-3">
										<label>Place Of Inspection:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="placeofInspectionFor" styleId="placeofInspection"
											styleClass="form-control"></html:text>
									</div>
								</div>
								
								<div class="form-group">
									
									<div class="col-lg-3">
										<label>Document Seized :</label>
									</div>
									<div class="col-lg-3">
										<div class="checkbox">
	                                      	<label>
	                                            <html:checkbox property="learnerlicense" styleClass="ace"></html:checkbox>
												<span class="lbl"> Learner/License</span>
	                                        </label>
	                                        <label>
	                                        	<html:checkbox property="bluebook" styleClass="ace"></html:checkbox>
												<span class="lbl"> Blue Book</span>
	                                        </label>
			
			                             </div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-3">
										<label>TIN No<span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<div class="col-lg-2 no-padding">
											<html:select property="tinPrefix" styleId="foreignVehTinPrefix" onchange="validateTin('foreignVehicle')" styleClass="form-control no-padding">
												<html:option value=""></html:option>
												<html:option value="T">T</html:option>
												<html:option value="P">P</html:option>
												<html:option value="G">G</html:option>
												<html:option value="S">S</html:option>
												<html:option value="M">M</html:option>
											</html:select>
										</div>
										<div class="col-lg-10 no-padding">
											<html:text property="TINno" styleId="foreignVehTinNo"  onchange="validateTin('foreignVehicle')" styleClass="form-control"></html:text>
										</div>
										<div class="error hidden bhutaneseTinDuplicateMessages"></div>
									</div>
									<div class="col-lg-3">
										<label>Remarks:</label>
									</div>
									<div class="col-lg-3">
										<html:textarea property="remarksFor" styleId="remarksFor"
											styleClass="form-control"></html:textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-3">
										<label>Offence Picture 1</label>
									</div>
									<div class="col-lg-3">
										<html:file property="offence5" styleId="offence5" styleClass="form-control fileupload" onchange="validation('learnerForm','pic1',this)"></html:file>
										<label style="display:none;color: #ff0000" id="pic1"></label>
									</div>
									<div class="col-lg-3">
										<label>Offence Picture 2</label>
									</div>
									<div class="col-lg-3">
										<html:file property="offence6" styleId="offence6" styleClass="form-control fileupload" onchange="validation('learnerForm','pic2',this)"></html:file>
										<label style="display:none;color: #ff0000" id="pic2"></label>
									</div>
								</div>
							</div>
							<div id="displayMsgDivForF"  class="col-lg-12">&nbsp;</div>
							<div class="pull-right">
								<button type="button" class="btn btn-primary btn-sm" id="fOffenceButton"  onClick="submitOffence('F')">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="updatePayment_form">
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Search Offence</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Vehicle No:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="vehicleNo" styleClass="form-control" styleId="updatedisplayVehicleNumber" readonly="true"></html:text>
											<html:hidden property="vehicleId" value="0" styleClass="form-control" styleId="updatevehicleId"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('updatelicenseoffenceVehicleModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-lg-2">
										<label>Vehicle Owner:</label>
									</div>
									<div class="col-lg-3" id="updatedisplayOwnerName">
										
									</div>
								</div>
								<div class="form-group" id="">
									<div class="col-lg-2">
										<label class="control-label">License No:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="licenseNo" styleClass="form-control" styleId="updateLicenseNo" readonly="true"></html:text>
											<html:hidden property="drivinglicenseId" styleClass="form-control" value="0" styleId="updateDrivingLicenseId"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('updateLicenseModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<div class="col-lg-2">
										<label>Driver:</label>
									</div>
									<div class="col-lg-3" id="updateLicenseHolderName">
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">CID No.:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<input type="text" id="updateOffenceCID" readonly="readonly" class="form-control">
											<html:hidden property="customerId"  styleClass="form-control" styleId="updateOffenceCustomerID"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('updateOffenceByPersonal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>	
										</div>
									</div>
									<div class="col-lg-2">
										<label>Driver:</label>
									</div>
									<div class="col-lg-3" id="updateOffenceDriverName"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Offence List</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
						<div id="getOffenceSearchList"></div>
						 	<div id="offenceList"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="updatedisplayMsgDiv" style="display:none;"></div>
			<div class="pull-right">
				<button type="button" class="btn btn-primary btn-sm" style="display:none;" id="updateOffenceButton" onclick="openModal('payment-modal')"  >Update Payment</button>
			</div>
		</div>	
		</html:form>
	</div>
</div>

<div id="modal-form1" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Name"> Name : </label>
                            <div class="col-sm-4">
                                 <input type="text" id="Name"   />
                            </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="CID"   />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="licenseNo"> License Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="licenseNo" />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="region"> Region : </label>
                                 <div class="col-sm-4">
                                      <select><option>--ALL--</option></select>
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="licenseType"> License Type : </label>
                                 <div class="col-sm-4">
                                      <select><option>--ALL--</option></select>
                                 </div>
						     </div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" data-dismiss="modal">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-check"></i>
					Select
				</button>
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

<div id="updatelicenseoffenceVehicleModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                        <form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="updatevehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                <div class="col-sm-4">
                                      <input type="text" id="updatecitizenIdRenewalModal" placeholder="Citizen ID"  />
                                </div>
							</div>
							<div class="form-group">
									<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	                                 <div class="col-sm-4">
	                                      <input type="text" id="updateengineNumberRenewalModal" placeholder="Engine Number"  />
	                                 </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
                                <div class="col-sm-4">
                                     <input type="text" id="updatechasisNumberRenewalModal" placeholder="Chasis Number"  />
                                </div>
						   	</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="updatesearchlicenseOffenceInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="updaterenewalListTable">
		   </div>
			
		</div>
	</div>
</div>

<div id="newLicenseoffenceVehicleModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
						  </div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchlicenseOffenceInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="newrenewalListTable">
		   </div>
			
		</div>
	</div>
</div>

<div id="newLicenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label>License No:</label>
										<span class="pull-right">
											<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
										</span>
									</div>
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<div id="updateLicenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="updatecidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="updatelicenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="updatesearchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="updatelicenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<div id="personalModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="personalCID" placeholder="CID" style="width: 200px"  />
								</span>
						</div>
						<div class="form-group">
							<label>Customer ID:</label>
							<span class="pull-right">
								<input type="text" id="personalCustomerId" placeholder="CustomerID" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo('searchPisForOffence')">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="searchResultForOffence">
			</div>
			
		</div>
	</div>
</div>
<div id="updateOffenceByPersonal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="updateOffencePersonalCID" placeholder="CID" style="width: 200px"  />
								</span>
						</div>
						<div class="form-group">
							<label>Customer ID:</label>
							<span class="pull-right">
								<input type="text" id="updateOffencePersonalCustomerId" placeholder="CustomerID" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo('searchPisForUpdateOffence')">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="searchResultForUpdateOffence">
			</div>
			
		</div>
	</div>
</div>
<div id="newLearnerModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
						<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidduplicationModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>Learner License No:</label>
								<span class="pull-right">
									<input type="text" id="learnerNoduplicationModal" placeholder="Learner License No" style="width: 200px"  />
								</span>
							</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchLearnerInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="learnerDuplicateListTable">
			</div>
			
		</div>
	</div>
</div>
<div id="updateLearnerModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
						<div class="col-xs-12 col-sm-12">
									
							<div class="form-group">
								<label>Name:</label>
								<span class="pull-right">
									<input type="text" id="nameduplicationModal" placeholder="Name"  style="width: 200px" />
								</span>
							</div>
							

							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidduplicationModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							
							<div class="form-group">
								<label>Learner License No:</label>
								<span class="pull-right">
									<input type="text" id="learnerNoduplicationModal" placeholder="Learner License No" style="width: 200px"  />
								</span>
							</div>
			
							<div class="form-group">
								<label>Region:</label>
								<span class="pull-right">
								
									<select id="regionduplicationModal" style="width: 200px">
										<option value="NA">SELECT</option>
										    <logic:iterate id="region" name="regionList">
													   <option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
										   </logic:iterate>
									</select> 
								</span>
							</div>	
					
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="updatesearchLearnerInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="updatelearnerDuplicateListTable">
			</div>
			
		</div>
	</div>
</div>
<!--<div id="searchOffenceDtls" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>TIN No:</label>
							<span class="pull-right">
								<input type="text" id="offenceTIN" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>Learner License No:</label>
							<span class="pull-right">
								<input type="text" id="offenceLearnerNo"style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="offenceLicenseNo" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>Vehicle No:</label>
							<span class="pull-right">
								<input type="text" id="offenceVehicleNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchOffence()">
					<i class="ace-icon fa fa-search"></i>
					
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="getOffenceSearchLista">
			</div>
			
		</div>
	</div>
</div>
--><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-timepicker.min.js"></script>



<script>
	var isTinValidate = 0;
	function checkDoubleOffence(offenceTypeId,Nationality,rowId)
	{
		var typeArray = offenceTypeId.split("_");
		var isOffenceRepeatAble = typeArray[1];
		 
		if(isOffenceRepeatAble=="N")
		{
			return false;
		}
		var identificationNo = 0;
		var identificationType = 0; 
		
		var newDrivinglicenseId = $("#newDrivinglicenseId").val();
		var newLearnerLicenseId = $("#newLearnerLicenseId").val();
		var newCustomerID = $("#newCustomerID").val();
		
		if(newDrivinglicenseId!="")
		{
			identificationNo = newDrivinglicenseId;
			identificationType = 'DL'; 
		}
		else if(newLearnerLicenseId!="")
		{
			identificationNo = newLearnerLicenseId;
			identificationType = 'LL'; 
		}
		else if(newCustomerID!="")
		{
			identificationNo = newCustomerID;
			identificationType = 'CID'; 
		}
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=checkDoubleOffence&offenceTypeId='+offenceTypeId+'&identificationNo='+identificationNo+'&identificationType='+identificationType,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					var result = $(this).find('result').text();
					var rowIdArray =	 rowId.split("_");
					
					if(result=="REPEATED")
					{
						$("#isOffenceRepeated_"+rowIdArray[2]).val('Y');
						$("#repeatedOffenceMessage_"+rowIdArray[2]).show();
					}
					else
					{
						$("#repeatedOffenceMessage_"+rowIdArray[2]).hide();
						$("#isOffenceRepeated_"+rowIdArray[2]).val('N');
					}
				});
			}
		});
	}
	
	function submitOffence(offenceFormType)
	{
		
		//$("#bhutaneseTinDuplicateMessages").addClass("hidden");
		//if(offenceFormType=="B" && ($("#bhutaneseVehTinPrefix").val()=="" || $("#bhutaneseVehTinNo").val()=="")) 
		//{
		//	if($("#bhutaneseVehTinNo").val()=="")
		//	{
		//		$("#bhutaneseTinDuplicateMessages").text("Enter TIN No.");
		//	}
		//	else
		//	{
		//		$("#bhutaneseTinDuplicateMessages").text("Select TIN Prefix");
		//	}
		//	$("#bhutaneseTinDuplicateMessages").removeClass("hidden");
		//}
		
		$("#validateRepeatedOffenceMsg").hide();
		var isRepeatedOffenceSelected = 0;
		var checkRepeatedOffence = 0;
		if(offenceFormType=='B')
		{
			var table = document.getElementById('offenceTable');
			var rowCount = (table.rows.length)-1;
			for(var i=0 ;i<rowCount;i++)
			{
				if($("#isOffenceRepeated_"+i).val()=='Y')
				{
					checkRepeatedOffence = 1;
				}
				
				var offenceId = $("#bhutanese_offenceId_"+i).val();
				var dataArray = offenceId.split('_');
				
				if(dataArray[0]==repeatedOffenceTypeId)
				{
					isRepeatedOffenceSelected = 1;
				}
			}
			
		}
		if(checkRepeatedOffence==1 && isRepeatedOffenceSelected==0)
		{
			$("#validateRepeatedOffenceMsg").show();
			return false;
			
		}
		
		if($('#learnerForm').valid() && isTinValidate==0) 
		{
			var displayMessage = "";
			if(offenceFormType=='B')
			{
				$("#bOffenceBhutan").hide();
				var options = {target:'#displayMsgDivForB',url:context+'/license.html?method=license_offence&offenceFormType='+offenceFormType,type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDivForB').show();
		        timeofInspection('hideStatus("displayMsgDivForB")',5000);
		        timeofInspectionfor('hideStatus("displayMsgDivForB")',5000);
			}
			else if(offenceFormType=='F')
			{
				$("#fOffenceBhutan").hide();
				var options = {target:'#displayMsgDivForF',url:context+'/license.html?method=license_offence&offenceFormType='+offenceFormType,type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDivForF').show();
		        timeofInspection('hideStatus("displayMsgDivForF")',5000);
		        timeofInspectionfor('hideStatus("displayMsgDivForF")',5000);
			}
			else if(offenceFormType=='P')
			{
				$("#fOffenceBhutan").hide();
				var options = {target:'#displayMsgDivForP',url:context+'/license.html?method=license_offence&offenceFormType='+offenceFormType,type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDivForP').show();
		        timeofInspection('hideStatus("displayMsgDivForP")',5000);
		        timeofInspectionfor('hideStatus("displayMsgDivForP")',5000);
			}
			
			
		}
		else 
		{
			return false;
		}

	}
</script>
<script type="text/javascript">

		$("#calculatePaymentButton").hide();
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";

		$('.fileupload').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false,
			whitelist:'gif|png|jpg|jpeg',
			blacklist:'exe|php'
		});
		
		function showLearner()
		{
			$("#license").hide(); 
		    $("#learner").show(); 
			
		}
		function showLicense()
		{
			$("#license").show(); 
		    $("#learner").hide(); 
		}	
			
		function showNewOffence()
		{
			$("#newOffence_form").show();
			$("#offense_form").show();
			$("#pedestrian_offense_form").hide();
		    $("#updatePayment_form").hide(); 
		    $("#foreign_vehicle_offense_form").hide();
		    $("#offense_form_foreign").hide();
		}		
		function showUpdatePayment()
		{
			$("#updatePayment_form").show();
			$("#offense_form").hide(); 
		    $("#newOffence_form").hide(); 
		    $("#foreign_vehicle_offense_form").hide();
		    $("#offense_form_foreign").hide();
		}	
		function showForeignOffence()
		{
			$("#foreign_vehicle_offense_form").show();
			$("#pedestrian_offense_form").hide();
			$("#offense_form").hide();
			$("#updatePayment_form").hide(); 
		    $("#newOffence_form").hide();
		    $("#offense_form_foreign").show();
		}	
		function showPedestrianOffence()
		{
			$("#pedestrian_offense_form").show();
			$("#foreign_vehicle_offense_form").hide();
			$("#offense_form").hide();
			$("#updatePayment_form").hide(); 
		    $("#newOffence_form").hide();
		    $("#offense_form_foreign").hide();
		}	

		function change_inspectedTypefor(inspectedbyFor)
		{
			$("#regionDisplayfor").hide();
			$("#trafficDisplayfor").hide();
			if(inspectedbyFor=="RSTA")
			{
				$("#regionDisplayfor").show();
			}
			else if(inspectedbyFor=="TRAFFIC_POLICE")
			{
				$("#trafficDisplayfor").show();
			}
		}
		
		function change_inspectedTypeP(inspectedbyP)
		{
			$("#regionDisplayP").hide();
			$("#trafficDisplayP").hide();
			if(inspectedbyP=="RSTA")
			{
				$("#regionDisplayP").show();
			}
			else if(inspectedbyP=="TRAFFIC_POLICE")
			{
				$("#trafficDisplayP").show();
			}
		}

		function change_inspectedType(inspectedBy)
		{
			$("#regionDisplay").hide();
			$("#trafficDisplay").hide();
			if(inspectedBy=="RSTA")
			{
				$("#regionDisplay").show();
			}
			else if(inspectedBy=="TRAFFIC_POLICE")
			{
				$("#trafficDisplay").show();
			}
		}
		
		$(document).ready(function()
		{
			$('#newOffence').attr('checked', true);
			//$('#licenseRadio').attr('checked', true);
			$('#newOffence_form').attr('checked', true);
			 $("#updatePayment_form").hide(); 
			 $("#offense_form").show(); 
			 
		});	

		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
				$('#timepicker1').timepicker
				({
					minuteStep: 1,
					showSeconds: false,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});


				$('#timepicker2').timepicker
				({
					minuteStep: 1,
					showSeconds: false,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

				function searchCommonInfo(searchType)
				{
					
					$('#submitBtn').hide();
					$('#updateBtn').show();
					var cidNumber = $('#personalCID').val();
					var customerId = $('#personalCustomerId').val();
					
					if(searchType=='searchPisForOffence')
					{
						cidNumber = $('#personalCID').val();
						customerId = $('#personalCustomerId').val();
					}
					else if(searchType=='searchPisForUpdateOffence')
					{
						cidNumber = $('#updateOffencePersonalCID').val();
						customerId = $('#updateOffencePersonalCustomerId').val();
					}
					
					
					if(cidNumber == "")
						cidNumber = "NA";
					if(customerId == "")
						customerId = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoForEdit&cid="+cidNumber+"&customerId="+customerId+"&searchType="+searchType,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							if(searchType=='searchPisForOffence')
							{
								$("#searchResultForOffence").html(responseText);
								$("#searchResultForOffence").show();
							}
							else if(searchType=='searchPisForUpdateOffence')
							{
								$("#searchResultForUpdateOffence").html(responseText);
								$("#searchResultForUpdateOffence").show();
							}
							
						}
					});
				}
				
				function updatesearchPersonalInfo()
				{
					var name 		= $('#updatenamePersonalModal').val();
					var cidNumber 	= $('#updatecidPersonalModal').val();
					var regionId 	= $('#updateregionPersonalModal').val();
					var licenseNo	= $('#updatelicenseNoModal').val();
					var licenseType	= $('#updatelicenseTypeModal').val();
					
					if(name == "")
						name = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
					if(regionId == "")
						regionId = "NA";
					if(licenseNo == "")
						licenseNo = "NA";
					if(licenseType == "")
						licenseType = "NA";
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&name="+name+"&cid="+cidNumber+"&region="+regionId+"&licenseNo="+licenseNo+"&licenseType="+licenseType+"&searchType=UPDATE_LICENSE_OFFENCE",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#updatelicenseDetailList").html(responseText);
							$("#updatelicenseDetailList").show();
						}
					});
					 
				}

				function searchPersonalInfo()
				{
					var cidNumber 	= $('#cidPersonalModal').val();
					var licenseNo	= $('#licenseNoModal').val();
					
					if(cidNumber == "")
						cidNumber = "NA";
					if(licenseNo == "")
						licenseNo = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=LICENSE_OFFENCE",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#licenseDetailList").html(responseText);
							$("#licenseDetailList").show();
						}
					});
					 
				}
				function formSubmit()
				{
					var offenceId	=	$(".offenceId").val();
					var TINno	=	$("#new_TINno").val();
					var receiptNo	=	$("#reciptNo").val();
					var receiptDate	=	$("#reciptDate").val();
					var amount	=	$("#amount").val();
					var options = {target:'#updatedisplayMsgDiv',url:context+'/license.html?method=pay_offence&receiptNo='+receiptNo+'&receiptDate='+receiptDate+'&TINno='+TINno+'&amount='+amount+'&offenceId='+offenceId,type:'POST',data: $("#learnerForm").serialize()}; 
				    $("#learnerForm").ajaxSubmit(options);
			        $('#updatedisplayMsgDiv').show();
			        setTimeout('hideStatus("updatedisplayMsgDiv")',10000);
			        //setTimeout('reloadPage()',10000);
				}

				function updatesearchlicenseOffenceInfo()
				{ 
					var vehicleNumber = $('#updatevehicleNumberRenewalModal').val();
					var engineNumber = $('#updateengineNumberRenewalModal').val();
					var chasisNumber = $('#updatechasisNumberRenewalModal').val();
					var cidNumber = $('#updatecitizenIdRenewalModal').val();
					
					if(vehicleNumber == "")
						vehicleNumber = "NA";
					if(engineNumber == "")
						engineNumber = "NA";
					if(chasisNumber == "")
						chasisNumber = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&searchType=UPDATE_OFFENCE&type=OFFENCE",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#updaterenewalListTable").html(responseText);
							$("#updaterenewalListTable").show();
						}
					});
				}
				function searchlicenseOffenceInfo()
				{ 
					var vehicleNumber = $('#vehicleNumberRenewalModal').val();
					var engineNumber = $('#engineNumberRenewalModal').val();
					var chasisNumber = $('#chasisNumberRenewalModal').val();
					var cidNumber = $('#citizenIdRenewalModal').val();

					if(vehicleNumber == "")
						vehicleNumber = "NA";
					if(engineNumber == "")
						engineNumber = "NA";
					if(chasisNumber == "")
						chasisNumber = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+'&searchType=NEW_OFFENCE&type=OFFENCE',
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#newrenewalListTable").html(responseText);
							$("#newrenewalListTable").show();
						}
					});
				}
				function updatelicenseDetailList()
				{
					var name = $('#nameduplicationModal').val();
					var cid = $('#cidduplicationModal').val();
					var learnerno = $('#learnerNoduplicationModal').val();
					var regionId = $('#regionduplicationModal').val();
					
					if(name == "")
						name = "NA";
					if(cid == "")
						cid = "NA";
					if(learnerno == "")
						learnerno = "NA";
					if(regionId == "")
						regionId = "NA";
					
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&name="+name+"&cid="+cid+"&learnerno="+learnerno+"&region="+regionId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#updatelearnerDuplicateListTable").html(responseText);
							$("#updatelearnerDuplicateListTable").show();
						}
					});
					
				}
				
				function updatesearchLearnerInfo()
				{
					var name = $('#nameduplicationModal').val();
					var cid = $('#cidduplicationModal').val();
					var learnerno = $('#learnerNoduplicationModal').val();
					var regionId = $('#regionduplicationModal').val();
					
					if(name == "")
						name = "NA";
					if(cid == "")
						cid = "NA";
					if(learnerno == "")
						learnerno = "NA";
					if(regionId == "")
						regionId = "NA";
					
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&name="+name+"&cid="+cid+"&learnerno="+learnerno+"&region="+regionId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#updatelearnerDuplicateListTable").html(responseText);
							$("#updatelearnerDuplicateListTable").show();
						}
					});
					
				}
				function searchLearnerInfo()
				{
					var cid = $('#cidduplicationModal').val();
					var learnerno = $('#learnerNoduplicationModal').val();
					if(cid == "")
						cid = "NA";
					if(learnerno == "")
						learnerno = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&cid="+cid+"&learnerno="+learnerno,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#learnerDuplicateListTable").html(responseText);
							$("#learnerDuplicateListTable").show();
						}
					});
					
				}
				
				function searchLearnerInfo()
				{
					var name = $('#nameduplicationModal').val();
					var cid = $('#cidduplicationModal').val();
					var learnerno = $('#learnerNoduplicationModal').val();
					var regionId = $('#regionduplicationModal').val();
					
					if(name == "")
						name = "NA";
					if(cid == "")
						cid = "NA";
					if(learnerno == "")
						learnerno = "NA";
					if(regionId == "")
						regionId = "NA";
					
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicenseInfoList&name="+name+"&cid="+cid+"&learnerno="+learnerno+"&region="+regionId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#learnerDuplicateListTable").html(responseText);
							$("#learnerDuplicateListTable").show();
						}
					});
					
				}
				
					$(document).ready(function()
					{
					 	$("#learner").hide(); 
						$("#learnerForm").validate
						({
							rules:
							{
								vehicleNoFor:{required:true},
								TINnoFor:{required:true},
								offencedateFor:{required:true},  	
								ownerNameFor:{required:true},
								licenseNoFor:{required:true},
								diverNameFor:{required:true},
								mobileNoFor:{required:true},
								tinPrefix:{required:true},
								vehicleType:{required:true},
								timeofInspectionFor:{required:true},
								inspectedbyFor:{required:true},
								inspectedTypeFor:{required:true},
								placeofInspectionFor:{required:true},
					            TINno:{required:true},
								offencedate:{required:true}, 
								region:{required:true},
								vehicleNo:{required:true},
								timeofInspection:{required:true},
								inspectedby:{required:true},
								inspectedType:{required:true}
							},
							messages:
							{
								vehicleNoFor:{required: "Please Provide vehicle no"},
								tinPrefix: {required:"Please Provide TIN Prefix"},
								TINnoFor: {required:"Please Provide TIN no"},
								offencedateFor:{required:"Please Offence Date"},
								ownerNameFor:{required: "Please Provide Owner Name"},
								licenseNoFor: {required:"Please Provide license No"},
								diverNameFor:{required:"Please Provide Diver Name"},
								mobileNoFor:{required:"Please Provide Mobile No"},
								vehicleType: {required:"Please Select Vehicle Type"},
								timeofInspectionFor:{required: "Please Provide Time Of Inspection"},
								inspectedbyFor: {required:"Please Select Inspected By"},
								inspectedTypeFor:{required:"Please Select Inspected Type"},
								placeofInspectionFor:{required:"Please Provide Place of Inspection"},
								TINno:{required: "Please Provide TIN no"},
								offencedate: {required:"Please Provide Offence Date"},
								region:{required:"Please Provide Region"},
								vehicleNo:{required:"Please Select Vehicle Number"},
								timeofInspection:{required:"Please Provide Time Of Inspection"},
								inspectedby:{required:"Please Select Inspected By"},
								inspectedType:{required:"Please Select Inspected Type"}
							}
						});
						
						//$('#submitBtn').click(function()
						//{ 
					    //});
					});

					

					
					function addRow(tableID) 
					{
						var table = document.getElementById(tableID);
						var rowCount = table.rows.length;
						var row = table.insertRow(rowCount);
						var colCount = table.rows[0].cells.length;
						for ( var i = 0; i < colCount; i++) 
						{
							var newcell = row.insertCell(i);
							var text = table.rows[1].cells[i].innerHTML;
							table.rows[rowCount].cells[0].innerHTML = rowCount;
							if (text.indexOf("[0]") > 0)
							{
								text = text.replace("[0]", "[" + (rowCount - 1) + "]");
								text = text.replace("[0]", "[" + (rowCount - 1) + "]");
							}
							if (text.indexOf("_0") > 0) 
							{
								text = text.replace("_0", "_" + (rowCount - 1));
								text = text.replace("_0", "_" + (rowCount - 1));
								text = text.replace("_0", "_" + (rowCount - 1));
							}
							newcell.innerHTML = text.replace(/CHECKED/g, '');

							switch (newcell.childNodes[0].type) 
							{
								case "select-one":
									newcell.childNodes[0].selectedIndex = 0;
									break;
								case "text":
									newcell.childNodes[0].value = "";
									break;
								case "checkbox":
									newcell.childNodes[0].checked = false;
									break;
							}
						}
						var repeatedOffence = rowCount - 1;
						$("#isOffenceRepeated_"+repeatedOffence).val('N');
						$("#repeatedOffenceMessage_"+repeatedOffence).hide();
					}

					function deleteRow(tableID) 
					{
						try 
						{
							var table = document.getElementById(tableID);
							var rowCount = table.rows.length;
							
							if (rowCount > 2) 
							{
								table.deleteRow(rowCount - 1);
								$('#rowCount').val(table.rows.length-1);	
							} 
							else 
							{
								alert("All rows cannot be deleted.");
							}
						} 
						catch (e) {
							alert(e);
						}
					}

					var fileError;
		           	function validation(thisform,msgId,fileObj)
		           	{
		           		var fileId = fileObj.id;
		           		with(thisform)
		           		{
		           			if(validateFileExtension(fileObj, msgId, "Only image file allowed!", new Array("jpg","jpeg","png","JPG","JPEG","PNG")) == false)
		           			{
		           				document.getElementById(fileId).value = "";
		           				return false;
		           			}
		           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
		           			{
		           				document.getElementById(fileId).value = "";
		           				return false;
		           			}
		           		}
		           	}
				function get_offence_list(offenceId)
				{
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=get_offence_list&offenceId="+offenceId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#offenceList").html(responseText);
							$("#offenceList").show();
							$("#updateOffenceButton").show();
							$.ajax
							({
								async: true,
								type: 'POST',
								url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOffenceDtls&offenceId='+offenceId,
								success: function(xml)
								{
									$(xml).find('xml-response').each(function()
									{
										$("#calculatePaymentButton").show();
					                    var offenceDate = $(this).find('offenceDate').text();
					                    var Inspected_By = $(this).find('Inspected_By').text();
					                    var Inspection_Type = $(this).find('Inspection_Type').text();
					                    var Traffic_Branch = $(this).find('Traffic_Branch').text();
					                    var Place_Of_Inspection = $(this).find('Place_Of_Inspection').text();
					                    var Region_Id = $(this).find('Region_Id').text();
					                    var Vehicle_Number = $(this).find('Vehicle_Number').text();
					                    var Driving_License_No = $(this).find('Driving_License_No').text();
					                    var Learner_License_No = $(this).find('Learner_License_No').text();
					                    var Is_Bluebook_Seized = $(this).find('Is_Bluebook_Seized').text();
					                    var Time_Of_Inspection = $(this).find('Time_Of_Inspection').text();
					                    var Is_License_Seized = $(this).find('Is_License_Seized').text();
					                    var Remarks = $(this).find('Remarks').text();
					                    var amount	=	$(this).find('amount').text();
					                    var Time_Of_Inspection	=$(this).find('Time_Of_Inspection').text();
					                    var Traffic_Branch	=$(this).find('Traffic_Branch').text();
					                    var tinNo	=	$(this).find('tinNo').text();
					                    var offenceId	=	$(this).find('offenceId').text();
					                    

					                    if(Learner_License_No=='null')
										{
					                    	Learner_License_No = "-";
										}
										if(Driving_License_No=='null')
										{
											Driving_License_No = "-";
										}
										$("#displayVehicleNumber").val(Vehicle_Number);
										$("#licenseNo").val(Driving_License_No);
										$("#learnerLicenseNo").val(Learner_License_No);
										$("#fetchOffenceDate").val(offenceDate);
										$("#timepicker1").val(Time_Of_Inspection);
										$("#updateOffenceId").val(offenceId);
										$("#fetchInspectedType").val(Inspection_Type);
										$("#trafficbranch").val(Traffic_Branch);
										$("#trafplaceofInspectionficbranch").val(Place_Of_Inspection);

										if(Is_License_Seized=='Y')
										{
											$('#learnerlicense_seized').prop('checked', true);
										}
										else
										{
											$('#learnerlicense_seized').prop('checked', false);
										}
										if(Is_Bluebook_Seized=='Y')
										{
											$('#bluebook_seized').prop('checked', true);
										}
										else
										{
											$('#bluebook_seized').prop('checked', false);
										}
										
										
										
										$("#updateregion").val(Region_Id);
									
										$("#remarks").val(Remarks);
										
										$("#TINno").val(tinNo);
										$("#new_TINno").val(tinNo);
										$("#penalty").val();
										$("#inspectionTime").val(Time_Of_Inspection);
										$("#udpatetrafficbranch").val(Traffic_Branch);
										
										if(Inspected_By=="RSTA")
										{
											$("#updateregionDisplay").val(Region_Id);
											$("#updateregionDisplay").show();
											$("#updatetrafficDisplay").hide();
										}
										else if(Inspected_By=="TRAFFIC_POLICE")
										{
											$("#updatetrafficDisplay").val(Traffic_Branch);
											$("#updateregionDisplay").hide();
											$("#updatetrafficDisplay").show();
										}
										$("#inspectedby").val(Inspected_By);

										if(Inspection_Type==1)
											$("#fetchInspectedType").val("NORMAL");
										else
											$("#fetchInspectedType").val("HIGHWAY");
									});
								}
							});
						}
					});
				}
				
				
				function searchOffence(vehicleId,licenseId,offenceType)
				{  
					$("#offenceList").hide();
					$("#updateOffenceButton").hide();
					
					var learnerId = "NA";
					if(licenseId=='')
					{
						licenseId = "NA";
					}
					if(vehicleId=='')
					{
						vehicleId="NA";
					}
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=searchCustomerOffence&offenceType="+offenceType+"&learnerId="+learnerId+"&licenseId="+licenseId+"&vehicleId="+vehicleId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#getOffenceSearchList").html(responseText);
							$("#getOffenceSearchList").show();
						}
					});
				}
				
				function getOffencePicture(offenceId)
				{
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getOffencePicture&id="+offenceId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#uploadedDocument").html(responseText);
							$("#uploadedDocument").show();
						}
					});
				}
				</script>
				<script>
					function validateTin(tinType)
					{
						
						var requriedField = 0;
						var tinNo = "";
						if(tinType=='bhutaneseVehOffence')
						{
							if($("#bhutaneseVehTinPrefix").val()=="" || $("#bhutaneseVehTinNo").val()=="")
							{
								requriedField = 1;
							}
							else
							{
								tinNo = $("#bhutaneseVehTinPrefix").val()+$("#bhutaneseVehTinNo").val();
							}
						}
						if(tinType=='foreignVehicle')
						{
							if($("#foreignTinPrefix").val()=="" || $("#foreignTinNo").val()=="")
							{
								requriedField = 1;
							}
							else
							{
								tinNo = $("#foreignTinPrefix").val()+$("#foreignTinNo").val();
							}
						}
						
						
						if(requriedField==0)
						{
							$.ajax
							({
								async: true,
								type: 'POST',
								url: '<%=request.getContextPath()%>/EralisCommonServlet?q=checkDoubleTin&tinNo='+tinNo,
								success: function(xml)
								{
									$(xml).find('xml-response').each(function()
									{
										var result = $(this).find('result').text();
										if(result=="DUPLICATE")
										{
											$(".bhutaneseTinDuplicateMessages").text("Duplicate TIN No.");
											$(".bhutaneseTinDuplicateMessages").removeClass("hidden");
											isTinValidate = 1;
										}
										else
										{
											$(".bhutaneseTinDuplicateMessages").addClass("hidden");
											isTinValidate = 0;
										}
									});
								}
							});
						}
						
					}
				
				</script>
				<style>
					#learnerForm .error { color: red; }
				</style>

			