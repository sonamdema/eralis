<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
    <div class="main-container ace-save-state" id="main-container">
    <% String asdf = "asfd"; %>
        <script type="text/javascript">
            try {
                ace.settings.loadState('main-container')
            } catch (e) {}
        </script>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <!-- /.breadcrumb -->

                    <div class="nav-search" id="nav-search">
                        <form class="form-search">
                            <span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
                            </span>
                        </form>
                    </div>
                    <!-- /.nav-search -->
                </div>

                <div class="page-content" id="mainContainer">
                  	<div class="col-lg-12">
                  		<table class="table table-bordered">
                  			<thead>
                  				<tr>
                  					<th>Token No.</th>
                  					<th>Trasanction Type</th>
                  					<th>Service Name</th>
                  					<th>Option</th>
                  				</tr>
                  			</thead>
                  			<tbody>
                  				<logic:notEmpty name="tokenList">
									<logic:iterate id="master" name="tokenList" type="bt.gov.rsta.eralis.dto.token.TokenCounterDTO" indexId="index">
										<%
											int a = index.intValue();
										%>
										<tr>
											<td align="center"><bean:write name="master" property="token_no"/></td>
											<td>
												<bean:write name="master" property="transaction_type"/>
											</td>
											<td>
												<bean:write name="master" property="service_type"/>
											</td>
											<td>
												<a class="btn btn-primary" onclick='getTokenDetails(<bean:write name="master" property="id"/>)'>Select</a>
											</td>
										</tr>
									</logic:iterate>
								</logic:notEmpty>
               				</tbody>
                  		</table>
                  	</div>
                  </div>
                    <!-- /.row -->
                </div>
                <!-- /.page-content -->
            </div>
        </div>
        <!-- /.main-content -->
    </div>
    <!-- /.main-container -->

    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="assets/js/jquery-2.1.4.min.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.dataTables.bootstrap.min.js"></script>
    <script src="assets/js/dataTables.buttons.min.js"></script>
    <script src="assets/js/buttons.flash.min.js"></script>
    <script src="assets/js/buttons.html5.min.js"></script>
    <script src="assets/js/buttons.print.min.js"></script>
    <script src="assets/js/buttons.colVis.min.js"></script>
    <script src="assets/js/dataTables.select.min.js"></script>

    <!-- ace scripts -->
    <script src="assets/js/ace-elements.min.js"></script>
    <script src="assets/js/ace.min.js"></script>

    <!-- inline scripts related to this page -->
    
    
    
    
	<script type="text/javascript">
		
		function getTokenDetails(tokenId)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getTokenDetails&tokenId="+tokenId,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#mainContainer").html(responseText);
				}
			});
		}
		
</script>	
