<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");
%>
<script>
	function formSubmit()
	{
		$('#approveBTN').attr('disabled', true);
	
		$.blockUI
	    ({ 
	    	css: 
	    	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	    	} 
	    });
    
		var application=$("#applicationNo").val();
		var vehicleNumber=$("#vehicleNumber1").val();
		var expiryDate=$("#expiryDate1").val();
		 
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=renewal_application_approval&applicationNo='+application+'&vehicleNumber='+vehicleNumber+'&expiryDate='+expiryDate, type:'POST',data: $("#vehicleForm").serialize()}; 

	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}

	function formReject()
    {
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=renewal_application_reject&applicationNo='+application,type:'POST',data: $("#vehicleForm").serialize()}; 
	
	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
     }

	function dispatch()
	{
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=VEHICLE&serviceType=RENEWAL',type:'POST',data: $("#vehicleForm").serialize()}; 
	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}
		
	var a="<%=request.getAttribute("applicationNo")%>";
	$("#applicationNo").val(a);
	var b="<%=dto.getVehicleNumber()%>";
	$("#vehicleNumber1").val(b);

	var c="<%=dto.getExpiryDate()%>";
	$("#expiryDate1").val(c);
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Renewal
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			     Renewal of Vehicle
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	
	<div class="row">
		<div class="col-lg-12">
			<span>Vehicle Registration Certificate Renewal for Application No: <strong><%=request.getAttribute("applicationNo")%></strong></span>
		</div>
	</div>
	
	<div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Vehicle & Owner Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="form-group ">
					<div class="col-lg-2">
						<label class="control-label">Vehicle Number:</label>
					</div>	
                          <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNumber() %></label></div> <div class="col-lg-2">
						<label class="control-label">Vehicle Type:</label>
					</div>	
                          <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleType() %></label></div> 
			    </div>
			    <div class="form-group ">
					<div class="col-lg-2">
						<label>Vehicle Registration Code:</label>
					</div>	
                          <div class="col-lg-4">
                          	<label ><%=dto.getVehicleRegistrationCode() %></label>
                         	</div> 
                         	<div class="col-lg-2">
						<label>Engine CC/GVW/HP/KW:</label>
					</div>	
                          <div class="col-lg-4"><label class="control-label"><%=dto.getEngineCC() %></label></div> 
			    </div>
			    <div class="form-group ">
					<div class="col-lg-2">
						<label class="control-label">Seating Capacity:</label>
					</div>	
                          <div class="col-lg-4"><label class="control-label"><%=dto.getSeatCapacity() %></label></div> 
					    </div>
			 	<%
                	if(dto.getVehicleRegistrationType().equals("Personal"))
                	{
                %>
						
						 <div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Owner Type:</label>
							</div>	
                               <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleRegistrationType()%></label></div> 
                            <div class="col-lg-2">
								<label class="control-label">CID :</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getCitizenID() %></label></div>
		                    </div>
		                   <div class="form-group ">
		                    <div class="col-lg-2">
								<label class="control-label">Owner Name :</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Phone:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div> 
		                     </div>
		                   <div class="form-group ">
		                   <%
		                   		if(dto.getIsInternational().equals("N"))
		                   		{
		                   %> 
		                    <div class="col-lg-2">
								 <label class="control-label">Dzongkhag:</label>
							</div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div> 
                            <div class="col-lg-2">
								<label class="control-label">Gewog:</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getGewog() %></label></div>
                             <%
		                   		}
                             %>
		                     </div>
		                    <div class="form-group ">
		                    <%
			                    if(dto.getIsInternational().equals("N"))
		                   		{
		                    %>
		                    <div class="col-lg-2">
								<label class="control-label">Village:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label"><%=dto.getVillage() %></label></div> 
                             <%
		                   		}
			                    if(dto.getIsInternational().equals("I"))
		                   		{
                             %>
		                    <div class="col-lg-2">
								<label class="control-label">Country:</label>
					        </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getCountry() %></label></div>
                            <%
		                   		}
                            %>
                            	<div class="col-lg-2">
								 <label class="control-label">Address:</label>
							    </div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div> 
		                	</div> 
		         <%
                	}
                	else
                	{
		         %>
		        		<!--<div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Vehicle Number:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNumber() %></label></div> 
					    </div>
		         		-->
			         		<div class="form-group">
					 			<div class="col-lg-2">
					 				<label class="control-label">Registration Type</label>
					 			</div>
					 			<div class="col-lg-4">
					 				<label class="control-label"><%=dto.getVehicleRegistrationType() %></label>
					 			</div> 
					 			<div class="col-lg-2">
									<label class="control-label">Customer ID :</label>
						        </div>	
		                       	<div class="col-lg-4">
		                       		<label class="control-label"><%=dto.getCustomerId() %></label>
		                    	</div>
		                    </div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Owner Type</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getOwner() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Agency</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getMinistry() %></label></div>
					 		</div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Department/Name</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Address</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div>
					 		</div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Phone No</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Dzongkhag</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div>
					 		</div>
		         <%
                	}
		         %>
							<div class="form-group ">
                                <div class="col-lg-2">
								   <label>Initial Registration Date:</label>
					             </div>	
		                        <div class="col-lg-4">
		                        	<label class="control-label"><%=dto.getRegistrationDate() %></label>
	                        	</div>
		                     	
		                  </div>  
                         </div>  
                       </div>   
                       <div class="widget-header widget-header-small">
                           <h5 class="widget-title lighter">Renewal Details</h5>
                       </div>						
		                  <div class="widget-body">
			                <div class="widget-main"> 
		                	<div class="form-group ">
		                		<div class="col-lg-2">
									<label class="control-label">Expiry Date :</label>
							 	</div>	
                                <div class="col-lg-4">
                                	<label class="control-label"><%=dto.getRenewalDate() %></label>
                                </div> 
                               	<div class="col-lg-2">
									<label class="control-label">Renewal Duration:</label>
					         	</div>	
					         	<%
					         		int renewalDurationMonths = Integer.parseInt(dto.getRenewalDuration());
					         		int renewalDurationYears = renewalDurationMonths/12;
					         	%>
		                        <div class="col-lg-4"><label class="control-label"><%=renewalDurationYears %> Years</label></div>
		                    </div>
		                   	<div class="form-group "> 
		                   		<div class="col-lg-2">
									<label class="control-label">Next Expiry Date:</label>
					         	</div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getExpiryDate() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Remarks:</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getRemarks() %></label></div>
                   			</div>
			            </div>
			       </div>
			       <div class="widget-header widget-header-small">
                       <h5 class="widget-title lighter">Payment Details</h5>
                   </div>						
		           <div class="widget-body">
			       		<div class="widget-main"> 
		             	<div class="form-group">
                           	<div class="col-lg-2">
								<label class="control-label">Amount:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getAmount() %></label></div> 
                              <div class="col-lg-2">
								<label class="control-label">Penality:</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getPenalty() %></label></div>
                         </div>
		                 <div class="form-group ">
		                 	 <div class="col-lg-2">
								<label class="control-label">Receipt No:</label>
					        </div>	
                     		<div class="col-lg-4">
                     			<label class="control-label"><%=dto.getReceiptNo() %></label>
                     		</div>
	                 		<div class="col-lg-2">
								<label class="control-label">Receipt Date:</label>
				 			</div>	
                          	<div class="col-lg-4">
                           		<label class="control-label"><%=dto.getReceiptDate() %></label>
                        	</div> 
                		</div> 
	                 	<div class="form-group">
		                  <div class="col-lg-2">
		                     <label class="control-label">Total Amount:</label>
		                  </div>
		                  <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=Double.parseDouble(dto.getAmount())+Double.parseDouble(dto.getPenalty())%></label></div>
		                  <div class="col-lg-2">
		                     <label>Application Submission Date:</label>
		                  </div>
		                  <div class="col-lg-4"><label class="control-label"><%=dto.getAppsubmissiondate()%></label></div>
		               </div> 
                        <logic:equal value="APPROVE" name="param">
                           	<div class="form-group">
								<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
							</div>
						</logic:equal>
                      </div>
              </div>         
        </html:form>
        <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	 </div>
<input type="hidden" id="applicationNo"/>
<input type="hidden" id="vehicleNumber1"/>
<input type="hidden" id="expiryDate1"/>
<html:hidden property="vehicleId" value="<%=dto.getVehicleId() %>"></html:hidden>
<div id="displayMsgDiv"></div>

<div>
	<logic:equal value="APPROVE" name="param">
	<button type="button" class="btn btn-sm" id="approveBTN" onclick="formSubmit()">
		<i class="ace-icon fa fa-check"  ></i>
		Approve
	</button>
	<button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
		<i class="ace-icon fa fa-times red2"></i>
		Reject
	</button>
	</logic:equal>
	<logic:equal value="DISPATCH" name="param">
	<button type="button" class="btn btn-sm" onclick="dispatch()">
		<i class="ace-icon fa fa-check"  ></i>
		Dispatch
	</button>
	</logic:equal>
 </div>