<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
String pageIdentifier = (String) request.getAttribute("page_identifier");
String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<style>
	#vehicleForm .error { color: red; }
</style>
  <div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Conversion
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Conversion of Vehicle
		</small>
	</h1>
</div><!-- /.page-header -->
 <div class="row">
    <div class="col-lg-12"> 
     <html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
    	<div id="Msg"></div>
		<div id="incompleteValidationMsg"></div>
		<div class="widget-box">
		  <div class="widget-body">
			<div class="widget-main">
			  <jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
			    <div class="form-group">
					<div class="col-lg-2">
						<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
					</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
								<html:hidden property="vehicleId" styleClass="form-control" styleId="vehicleId" ></html:hidden>
								  <span class="input-group-btn">
							      	<a href="#" onclick="openModal('conversion')" class="btn btn-purple btn-sm">
										<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
									</a>
								  </span>
							 </div>
						  </div>
						  <div class="col-lg-3">
						  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
						  </div>
					    </div>
				     </div>
				   </div>
				</div>
	 
    <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Details</h5>
		</div>						
		 <div class="widget-body">
			<div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
      </div>
    </div>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Vehicle Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="form-group">
							    <div class="col-lg-2">
								   <label class="control-label"> Company :</label>
							    </div>
		                        <div class="col-lg-3" id="displayCompany">
                                </div>
						        <div class="col-lg-2">
								   <label class="control-label"> Model :</label>
							    </div>
			                    <div class="col-lg-3" id="displayModel">
                                </div> 
               </div> 
               <div class="form-group">
							   <div class="col-lg-2">
								  <label class="control-label"> Color :</label>
							   </div>
		                       <div class="col-lg-3" id="displayColor">
                               </div>
						       <div class="col-lg-2">
								  <label class="control-label"> Chasis Number :</label>
							   </div>
			                   <div class="col-lg-3" id="displayChasisNumber">
                               </div> 
              </div>    
              <div class="form-group">
							  <div class="col-lg-2">
								<label class="control-label"> Engine Number :</label>
							  </div>
		                      <div class="col-lg-3" id="displayEngineNumber">
                              </div>
                              <div class="col-lg-2">
								 <label class="control-label"> Engine Type :</label>
						      </div>
			                  <div class="col-lg-3" id="displayEngineType">
                              </div> 
              </div> 	
			  <div class="form-group">
							  <div class="col-lg-2">
								<label class="control-label"> Seat Capacity :</label>
							  </div>
		                      <div class="col-lg-3" id="displaySeatCapacity">
                              </div>
                              <div class="col-lg-2">
								  <label class="control-label"> Load Capacity :</label>
						      </div>
			                  <div class="col-lg-3" id="displayLoadCapacity">
                           </div> 
                    </div> 	
              </div>
          </div>
    </div>
  
	<div class="widget-box">
      <div class="widget-header widget-header-small">
	     <h5 class="widget-title lighter">Conversion Details</h5>
        </div>
	       <div class="widget-body">
            <div class="widget-main">
            			<div class="form-group">
            				<div class="col-lg-2">
            					<label>Conversion Type:</label>
            				</div>
            				<div class="col-lg-3">
                                <select class="form-control" id="conversionType">
                                 	<option value="">--SELECT--</option>
                                 	<option value="VEHICLE_TYPE_CONVERSION">Vehicle Type</option>
                                 	<option value="REGIONAL_CONVERSION">Region</option>
                                 </select>
                                 <html:hidden property="conversionType" styleId="conversionTypeHidden"></html:hidden>
            				</div>
            			</div>
                       <div class="form-group">
                          <div class="col-lg-2">
					    		<label>Conversion Reason :<span style="color: #ff0000">*</span> </label>
			         		</div>
                              <div class="col-lg-3">
                                 <select class="form-control" id="conversionReason">
                                 	<option value="">--SELECT--</option>
                                 	<logic:iterate id="vehicleConversionReason" name="vehicleConversionList">
                                 		<option value='<bean:write name="vehicleConversionReason" property="headerId"/>'>
                                 			<bean:write name="vehicleConversionReason" property="headerName"/>
                                 		</option>
                                 	</logic:iterate>
                                 </select>
                                 <html:hidden property="conversionReason" styleId="conversionReasonHidden"></html:hidden>
                              </div>
                   	      	<div class="col-lg-2">
								<label>Vehicle Rgn. Code :</label>
							</div>	
		                       <div class="col-lg-3">
							   	<select class="form-control" id="vehicleRegistrationId">
	                               	<option value="">--SELECT--</option>
	                               	<logic:iterate id="vehicleCode" name="vehicleCodeList">
	                               		<option value='<bean:write name="vehicleCode" property="headerId"/>'>
	                               			<bean:write name="vehicleCode" property="headerName"/>
	                               		</option>
	                               	</logic:iterate>
                                </select>
                                <html:hidden property="vehicleRegistrationId" styleId="vehicleRegistrationIdHidden"></html:hidden>
                             </div> 
                   	     </div>
                        <div class="form-group">
		                    <div class="col-lg-2">
								<label>Region :</label>
					        </div>	
		                     <div class="col-lg-3">
							   <select class="form-control" id="region">
	                               	<option value="">--SELECT--</option>
	                               	<logic:iterate id="region1" name="regionList">
	                               		<option value='<bean:write name="region1" property="headerId"/>'>
	                               			<bean:write name="region1" property="headerName"/>
	                               		</option>
	                               	</logic:iterate>
                                </select>
                                <html:hidden property="region" styleId="regionHidden"></html:hidden>
                           </div>
		                 <div class="col-lg-2">
								<label>Vehicle Type :</label>
						</div>	
		                       <div class="col-lg-3">
								    <select class="form-control" id="vehicleTypes">
		                               	<option value="">--SELECT--</option>
		                               	<logic:iterate id="vehicleType" name="vehicleTypeList">
		                               		<option value='<bean:write name="vehicleType" property="headerId"/>'>
		                               			<bean:write name="vehicleType" property="headerName"/>
		                               		</option>
		                               	</logic:iterate>
                                	</select>
                                	<html:hidden property="vehicleType" styleId="vehicleTypeHidden"></html:hidden>
                               </div> 
		                 </div>
                      </div>
                    </div>
				 </div>	
				<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Attachments</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								
								<div class="form-group">
									<div class="col-lg-2">
										<label> Supporting Document:</label>
									</div>
									<div class="col-lg-3">
										<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload"  onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
									</div>
					              </div>
	                     		 </div> 
	                   		</div>
	                	 </div>
					</div>
				</div>	
				<div class="row">
					<div id="displayMsgDiv"></div>
				</div>
				<div class="pull-left">
		          	<html:hidden property="engineType" styleId="engineType"></html:hidden>
					<html:hidden property="loadCapacity" styleId="loadCapacity"></html:hidden>
					<html:hidden property="seatCapacity" styleId="seatCapacity"></html:hidden>
					<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower"></html:hidden>
					<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt"></html:hidden>
					<html:hidden property="engineCC" styleId="engineCC"></html:hidden>
					<html:hidden property="customerId" styleId="customerID"></html:hidden>
					<html:hidden property="vehicleRegistrationType" styleId="ownerType"></html:hidden>
					<input type="hidden" name="vehicleTypeDesc" id="vehicleTypeDesc"/>
					<input type="hidden" id="vehicleType"/>
					<input type="hidden" id="vehicleRegistrationCode"/>
					<input type="hidden" id="mainRegion"/>
					<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
					<input type="hidden" id="userRegion" value="<%=regionId%>"/>
				    <logic:equal value="Y" name="priviledge" property="isNew">
					   <button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
					</logic:equal>
				</div>
		</html:form>
	</div>
 </div>
<div id="conversion" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Owner's Name"> Owner's Name : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="ownerNameRenewalModal" placeholder="Owner's Name"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                            <div class="col-sm-4">
                                 <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                            </div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
                            <div class="col-sm-4">
                                 <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                            </div>
						</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="renewalListTable">
		  </div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
			<script type="text/javascript">

				   var pageIdentifier = "<%=pageIdentifier%>";
				   var pageId = "<%=pageId%>";
			
					$('.date-picker').datepicker({
						autoclose: true,
						todayHighlight: true
					});

				 $('.fileupload').ace_file_input({
						no_file : 'No File ...',
						btn_choose : 'Choose',
						btn_change : 'Change',
						droppable : false,
						onchange : null,
						thumbnail : false,
						whitelist:'png|jpg|jpeg',
						blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
					});
				
				function searchRenewalInfo()
				{ 
					var vehicleNumber = $('#vehicleNumberRenewalModal').val();
					var engineNumber = $('#engineNumberRenewalModal').val();
					var chasisNumber = $('#chasisNumberRenewalModal').val();
					var cidNumber = $('#citizenIdRenewalModal').val();
	
					if(vehicleNumber == "")
						vehicleNumber = "NA";
					if(engineNumber == "")
						engineNumber = "NA";
					if(chasisNumber == "")
						chasisNumber = "NA";
					if(cidNumber == "")
						cidNumber = "NA";
	
	
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&searchType=CONVERSION&type=NA",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#renewalListTable").html(responseText);
							$("#renewalListTable").show();
						}
					});
				}
				
				function searchVehicleNumber()
				{ 
					var vehicleNumber = $('#displayVehicleNumber').val();
	
					if(vehicleNumber == "")
						vehicleNumber = "NA";
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getRenewalHistoryList&vehicleNumber="+vehicleNumber,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#renewalHistoryTable").html(responseText);
							$("#renewalHistoryTable").show();

							selectValues();
						}
					});
				}

				function get_vehicle_edit_dtls()
				{
					var registrationCode = $('#vehicleRegistrationCode').val();
				}

				function selectValues()
				{
					$('#conversionType').val('');
					$('#conversionReason').val('');
					$('#vehicleRegistrationId').val('');
					$('#region').val('');
					$('#vehicleTypes').val('');
					
					$('#conversionType').attr('disabled', true);
					$('#conversionReason').attr('disabled', true);
					$('#vehicleRegistrationId').attr('disabled', true);
					$('#region').attr('disabled', true);
					$('#vehicleTypes').attr('disabled', true);
					
					var registrationCode = $('#vehicleRegistrationCode').val();
					var vehicleType = $('#vehicleType').val();
					var userRegion = $('#userRegion').val();
					$('#region').val(userRegion);

					if(registrationCode == "5")
					{
						$('#conversionType').val('VEHICLE_TYPE_CONVERSION');
						$("#conversionReason option:contains(Taxi to Private Conversion)").attr('selected', 'selected');
						$("#vehicleRegistrationId option:contains(BP)").attr('selected', 'selected');
						$("#vehicleTypes option:contains(Light Vehicle)").attr('selected', 'selected');
					}
					else if(registrationCode == "2")
					{
						$('#conversionType').val('VEHICLE_TYPE_CONVERSION');
						$("#conversionReason option:contains(Government to Private Conversion)").attr('selected', 'selected');
						$("#vehicleRegistrationId option:contains(BP)").attr('selected', 'selected');
						$('#vehicleTypes').val(vehicleType);
					}
					else if(registrationCode == "3")
					{
						$('#conversionType').val('VEHICLE_TYPE_CONVERSION');
						$("#conversionReason option:contains(BHT to Private Conversion)").attr('selected', 'selected');
						$("#vehicleRegistrationId option:contains(BP)").attr('selected', 'selected');
						$('#vehicleTypes').val(vehicleType);
					}
					else if(registrationCode == "6")
					{
						$('#conversionType').val('VEHICLE_TYPE_CONVERSION');
						$("#conversionReason option:contains(CD to Private Conversion)").attr('selected', 'selected');
						$("#vehicleRegistrationId option:contains(BP)").attr('selected', 'selected');
						$('#vehicleTypes').val(vehicleType);
					}
					else
					{
						$('#conversionType').val('REGIONAL_CONVERSION');
						$("#conversionReason option:contains(Conversion among Regional Office)").attr('selected', 'selected');
						$('#vehicleRegistrationId').val(registrationCode);
						$('#vehicleTypes').val(vehicleType);
					}
				}
				
				$(document).ready(function()
				{
					$("#vehicleForm").validate({
							rules:
							{
								conversionType:{
									required:true,
									},
								conversionReason:{
									required:true,
									},
								receiptNo:{
									required:true,
									},
								receiptDate:{
									required:true
								    },
							    vehicleRegistrationId:{
									required:true
									},
								region:{
									required:true,
									},
								vehicleType:{
									required:true
									}
							},    
							messages:
							{
								conversionType:{required: "Please select conversion type"},
								conversionReason:{required: "Please Select a Cancellation Reason"},
								receiptNo:{required: "Please Provide Receipt Number"},
								receiptDate:{required: "Please Select a Date"},
								vehicleRegistrationId:{required: "Please Select a Vehicle Code"},
								region:{required: "Please Select a Region"},
								vehicleType:{required: "Please Select a Vehicle Type"},
							}
						});
	
						$('#submitBtn').click(function()
						{
							if($('#vehicleForm').valid()) 
							{
								var vehicleNumber = $('#displayVehicleNumber').val();
								
								if(vehicleNumber == "")
								{
									$('#vehicleNumberValidation').show();
									$('#vehicleNumberValidation').get(0).scrollIntoView();
									return false;
								}
								else
								{
								    var identityTypeId;
								    var requestType = "VEHICLE";
									var serviceType = "CONVERSION";
									var identityNo = $('#vehicleId').val();
									if( $("#engineType").val() == "Electric")
										identityTypeId = '0'; 
									else
										identityTypeId = $('#vehicleType').val();
									var loadingCapacity = $('#loadCapacity').val();
									var seatingCapacity = $('#seatCapacity').val();
									var vehicleHP = $('#vehicleHorsePower').val();
									var kilowatts = $('#vehicleKiloWatt').val();
									var engineCC = $('#engineCC').val();
									var purchaseDate = "";
									var saleDeedAmount = "";
									var saleDeedDate = "";
									var vehicleRegistrationCode = $('#vehicleRegistrationCode').val();
									var renewalDuration = "";

									getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration, vehicleRegistrationCode);
								}
							}
							else 
							{
								return false;
							}
						});
					});
	
					function formSubmit()
					{
						$('#conversionTypeHidden').val($('#conversionType').val());
						$('#conversionReasonHidden').val($('#conversionReason').val());
						$('#vehicleRegistrationIdHidden').val($('#vehicleRegistrationId').val());
						$('#regionHidden').val($('#region').val());
						$('#vehicleTypeHidden').val($('#vehicleTypes').val());
						
						var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_conversion',type:'POST',data: $("#vehicleForm").serialize()}; 
					    $("#vehicleForm").ajaxSubmit(options);
				        $('#displayMsgDiv').show();
				        setTimeout('hideStatus("displayMsgDiv")',10000);
					}
	
				   var context = "<%=request.getContextPath()%>";

				   var fileError;
		           	function validation(thisform,msgId,fileObj)
		           	{
		           		var fileId = fileObj.id;
		           		with(thisform)
		           		{
		           			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
		           			{
		           				document.getElementById(fileId).value = "";
		           				return false;
		           			}
		           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
		           			{
		           				document.getElementById(fileId).value = "";
		           				return false;
		           			}
		           		}
		           	}
	
	       </script>
           
           
           
