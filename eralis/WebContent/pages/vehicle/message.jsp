<%
	String message = (String)request.getAttribute("MESSAGE");

	if(message.equalsIgnoreCase("SUCCESS"))
	{
%>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 Action successfully performed
	</div>
<%
	}
	else if(message.equalsIgnoreCase("FAILURE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 Action couldn't be performed, please try again
	</div>
<%
	}
	else if(message.equalsIgnoreCase("DUPLICATE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 Vehicle with the same information already exists in your system
	</div>
<%
	}
	else if(message.equalsIgnoreCase("ALREADY_APPLIED"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 An application for the same vehicle has already been applied
	</div>
<%
	}
	else if(message.equalsIgnoreCase("NO_EMISSION_RECORD"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 This vehicle does not have a emission record
	</div>
<%
	}
	else if(message.equalsIgnoreCase("EMISSION_INVALID"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 This vehicle doesnot have a valid emission detail
	</div>
<%
	}
	else if(message.equalsIgnoreCase("NO_FITNESS_RECORD"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 This vehicle doesnot have a valid fitness detail
	</div>
<%
	}
%>