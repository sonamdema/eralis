<%
	String message = (String)request.getAttribute("MESSAGE");
	String emissionId = (String)request.getAttribute("EMISSION_ID");

	
	if(message.equalsIgnoreCase("SUCCESS"))
	{
%>
	<input type="hidden" id="emissionId" value="<%=emissionId%>"/>
	<div class="alert alert-success">
		<i class="ace-icon fa fa-check"></i>
		 Emission details successfully saved.
		 <a href="#" class="btn btn-primary btn-sm" onclick="printReport()">
		 	<i class="ace-icon fa fa-print bigger-160"></i>
		 	Print Certificate
		 </a>
	</div>
<%
	}
	else if(message.equalsIgnoreCase("FAILURE"))
	{
%>
	<div class="alert alert-danger">
		<i class="ace-icon fa fa-check"></i>
		 Emission details couldnot be saved.
	</div>
<%
	}
%>

<script>

	function printReport()
	{
		var emissionId = $('#emissionId').val();

		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/vehicle.html?method=print_emission_report&emissionId="+emissionId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				var mywindow = window.open('', 'my div', 'height=400,width=600');
				mywindow.document.write(responseText);

				setTimeout(function() {
					mywindow.print();
					mywindow.close();
				}, 400);
			}
		});
	}

</script>
