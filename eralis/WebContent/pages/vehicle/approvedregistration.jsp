<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");
%>
<script>
	var context = "<%=request.getContextPath()%>";

	function formSubmit()
	{
		$('#approveBTN').attr('disabled', true);

		$.blockUI
        ({ 
        	css: 
        	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
        	} 
        });
        
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=registration_application_approval&applicationNo='+application,type:'POST',data: $("#vehicleForm").serialize()}; 

	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	    setTimeout($.unblockUI, 1000);
	}
	

   	function formReject()
	{
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=registration_application_reject&applicationNo='+application,type:'POST',data: $("#vehicleForm").serialize()}; 

	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}

	function dispatch()
	{
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=VEHICLE&serviceType=REGISTRATION',type:'POST',data: $("#vehicleForm").serialize()}; 
	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}

			
	var a="<%=request.getAttribute("applicationNo")%>";
	$("#applicationNo").val(a);		


    var status	="<%=dto.getStatus()%>";
	if(status==1)
	{
		status	=	'Active';
	}else if(status==2)
	{
		status	=	'Cancelled';
	}else if(status==3)
	{
		status	=	'Transfered';
	}else if(status==4)
	{
		status	=	'Document Seized';
	}
	
	$('#status').html("<label class='control-label'>"+status+"</label>");

   var registrationType	="<%= dto.getVehicleRegistrationType()%>";

    if(registrationType==1)
	{
	   registrationType	=	'New';
	}else if(registrationType==2)
	{
		registrationType	=	'Qouta';
	}else if(registrationType==3)
	{
		registrationType	=	'Conversion';
	}

    $('#registrationType').html("<label class='control-label'>"+registrationType+"</label>");


    var purchaseType ="<%= dto.getPurchaseType()%>";

    if(purchaseType==1)
	{
    	purchaseType	=	'Normal';
	}else if(purchaseType==2)
	{
		purchaseType	=	'Imported';
	}

    $('#purchaseType').html("<label class='control-label'>"+purchaseType+"</label>");

	
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Registration
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			     Registration of Vehicle
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	 
	 <logic:equal value="DISPATCH" name="param">
	     <div class="row">
	     	<div class="col-lg-12">
	     		<div class="alert alert-block alert-success">
					<i class="ace-icon fa fa-check green"></i>
					Generated Vehicle Number:
					<strong class="green">
						<%=dto.getVehicleNumber() %>
					</strong>
				</div>
	     	</div>
	     </div>
     </logic:equal>
     <div class="row">
	     <div class="col-lg-12">
	     	<span>Vehicle Registration Application Details for Application No: <strong><%=request.getAttribute("applicationNo")%></strong></span>
	     </div>
	 </div>
	 <div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Owner Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				 <%
                	if(dto.getVehicleRegistrationType().equals("Personal"))
                	{
                %>
						<div class="form-group ">
							<div class="col-lg-2 ">
								<label class="control-label">Registration Type:</label>
							</div>	
		                          <div class="col-lg-4" id="registrationType"></div>
							<div class="col-lg-2 ">
								<label class="control-label">Owner Name:</label>
							</div>	
		                          <div class="col-lg-4"><label class="control-label"><%=dto.getName()%></label></div> 
		                 </div>
		                 <div class="form-group "> 
		                 	<div class="col-lg-2 ">
								<label class="control-label">Dzongkhag:</label>
					        </div>	
		                    <div class="col-lg-4 "><label class="control-label"><%=dto.getDzongkhag() %></label></div> 
		                    <div class="col-lg-2">
								<label class="control-label">Gewog :</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getGewog() %></label></div>
						 </div> 
						 <div class="form-group">
						 	<div class="col-lg-2">
								<label class="control-label">Address:</label>
							</div>	
		                    <div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div> 
		                    <div class="col-lg-2">
								<label class="control-label">CID No :</label>
					        </div>	
		                    <div class="col-lg-4"><label class="control-label"><%=dto.getCitizenID() %></label></div>
						 </div>
				 <%
                	}
                	else
                	{
				 %>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Registration Type</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getVehicleRegistrationType() %></label></div> 
				 		</div>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Owner Type</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getOwner() %></label></div>
				 			<div class="col-lg-2"><label class="control-label">Agency</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getMinistry() %></label></div>
				 		</div>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Department/Name</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
				 			<div class="col-lg-2"><label class="control-label">Address</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div>
				 		</div>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Phone No</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div>
				 			<div class="col-lg-2"><label class="control-label">Dzongkhag</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div>
				 		</div>
				 <%
                	}
				 %>
			</div>       
					
		                   <div class="widget-header widget-header-small">
			                  <h5 class="widget-title lighter">Vehicle Registration Details</h5>
		                  </div>
						<div class="widget-body">
			              <div class="widget-main">
		                 <div class="form-group ">   
		                   <div class="col-lg-2">
								<label class="control-label">Vehicle Registration:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleRegistrationId() %></label></div>
                            <div class="col-lg-2">
								<label class="control-label">Region:</label>
					        </div>	
		                    <div class="col-lg-4"><label class="control-label"><%=dto.getRegion()%></label></div>
                         </div> 
		                  <div class="form-group">  
		                    <div class="col-lg-2">
								<label class="control-label">Vehicle Type:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleType() %></label></div> 
                            <% if(dto.getVehicleType().equalsIgnoreCase("Heavy Bus") || dto.getVehicleType().equalsIgnoreCase("Medium Bus")){ %>
                            <div class="col-lg-2">
								<label class="control-label">Bus Type:</label>
							</div>	
                      		<div class="col-lg-4"><label class="control-label"><%=dto.getBusType() %></label></div> 
                      		<% }%>
                      	</div>
                      	<div class="form-group">  
                            <div class="col-lg-2">
								 <label class="control-label">Vehicle Company:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleCompany() %></label></div> 
	                   	</div> 
                        <div class="form-group">  
                              <div class="col-lg-2">
								 <label class="control-label">Vehicle Model:</label>
					          </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleModel() %></label></div>
		                     <div class="col-lg-2">
								<label class="control-label">Engine Type:</label>
							 </div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getEngineType() %></label></div> 
		                  </div>  
		                  <div class="form-group"> 
		                   <div class="col-lg-2">
								<label class="control-label">Engine Number:</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getEngineNumber() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Chasis Number:</label>
							</div>	
                               <div class="col-lg-4"><label class="control-label"><%=dto.getChasisNumber() %></label></div> 
		                   </div>
		                 <div class="form-group"> 
		                   <div class="col-lg-2">
								<label class="control-label">Vehicle Horse Power:</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleHorsePower() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Vehicle Kilowatt:</label>
							</div>	
                               <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleKiloWatt() %></label>
                             </div> 
		                   </div>
		                  <div class="form-group">  
		                     <div class="col-lg-2">
								<label class="control-label">Color:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label"><%=dto.getColour() %></label>
                               </div> 
                               <div class="col-lg-2">
								<label class="control-label">Price:</label>
					        </div>	
		                       <div class="col-lg-4"> <label class="control-label">Nu.&nbsp;<%=dto.getPrice() %></label>
                               </div>
                            </div>
		                 <div class="form-group">
		                 	<div class="col-lg-2">
								<label class="control-label">Seat Capacity:</label>
					         </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getSeatCapacity()%></label>
                                </div>
                                <div class="col-lg-2">
								<label class="control-label">Dealer Name:</label>
							</div>
                               <div class="col-lg-4"><label class="control-label"><%=dto.getDealersName()%></label>
                                </div>
		                     </div>
		                  <logic:equal value="APPROVE" name="param">
                           <div class="form-group"> 
							<div class="col-lg-2">
								<label class="control-label">Gross Vehicle Weight:</label>
							</div>
                               <div class="col-lg-4"><label class="control-label"><%=dto.getLoadCapacity()%></label>
                                </div>
                            <div class="col-lg-2">
								<label class="control-label">Unladen Weight:</label>
					        </div>
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getUnladenWeight()%></label>
                                </div>
		                   </div> 
		                  </logic:equal>
		                  <logic:equal value="DISPATCH" name="param">
		                  	<%
	                  		double gvw = 0;
	                  		double unladenWeight = 0;
	                  		double ladenWeightDouble = 0;
	                  		int ladenWeight =0;
		                  	%>
		                  	<div class="form-group"> 
							<div class="col-lg-2">
								<label class="control-label">Laden Weight:</label>
							</div>	
                               <div class="col-lg-4"><label class="control-label"><%=ladenWeight %> Tons</label></div> 
		                   </div> 
		                  </logic:equal>
		                   <div class="form-group">
		                     <div class="col-lg-2">
								  <label class="control-label">Manufacture Year:</label>
							 </div>	
                                 <div class="col-lg-4"><label class="control-label"><%=dto.getManufactureYear() %></label>
                             </div> 
                             <div class="col-lg-2">
								<label class="control-label">Engine CC:</label>
					         </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getEngineCC()%></label>
                              </div>
		                   </div>
		                   <div class="form-group"> 
		                   	 <div class="col-lg-2">
								  <label class="control-label">Country Name:</label>
							 </div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getManufactureCountry()%></label>
                                </div> 
                                <div class="col-lg-2">
								 <label class="control-label">Registration Date:</label>
							  </div>	
				                   <div class="col-lg-4"><label class="control-label"><%=dto.getRegistrationDate() %></label>
		                           </div>
                            </div>
                           <div class="form-group">  
                           	 <div class="col-lg-2">
								 <label class="control-label">Expiry Date:</label>
							 </div>	
							 <%
							 	int validityDurationInt = Integer.parseInt(dto.getRenewalDuration());
							 	int validityDurationInYears = validityDurationInt/12;
							 %>
                             <div class="col-lg-4"><label class="control-label"><%=dto.getExpiryDate() %>&nbsp;(Registered for <%=validityDurationInYears %>&nbsp;years)</label></div> 
                             <div class="col-lg-2">
								<label class="control-label">Purchase Type:</label>
					         </div>	
		                        <div class="col-lg-4" id="purchaseType">
                                </div>
		                   </div> 
		                </div>  
		              </div> 
		              <div class="widget-header widget-header-small">
			                  <h5 class="widget-title lighter">Payment Details</h5>
		                  </div>
						<div class="widget-body">
			              <div class="widget-main">
				              <div class="form-group "> 
		                           <div class="col-lg-2">
									<label class="control-label">Receipt Date:</label>
					           	</div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptDate() %></label></div>
			                    <div class="col-lg-2">
							  		 <label class="control-label">Receipt Number:</label>
							    </div>	
		                              <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo()%></label></div> 
		                           </div>
			                 <div class="form-group">
		                                 <div class="col-lg-2">
									 <label class="control-label">Registration Amount:</label>
								  </div>	
		                                 <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getAmount() %></label>
		                                 </div>
		                                 <div class="col-lg-2">
									 <label class="control-label">Penality:</label>
								   </div>	
		                                 <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getPenalty() %></label>
		                              </div> 
		                           </div>  
				              <logic:equal value="APPROVE" name="param">
				                  <div class="form-group">
										<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
								  </div>
							  </logic:equal> 
							</div>
						</div>
		           </div>     
               </div>
           </html:form>
           <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	 </div>
     
     
     <div id="displayMsgDiv"></div>
	<div>
		<logic:equal value="APPROVE" name="param">
			<button type="button" class="btn btn-sm" id="approveBTN" onclick="formSubmit()">
				<i class="ace-icon fa fa-check"  ></i>
				Approve
			</button>
			<button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
				<i class="ace-icon fa fa-times red2"></i>
				Reject
			</button>
		</logic:equal>
		<logic:equal value="DISPATCH" name="param">
			<button type="button" class="btn btn-sm" onclick="dispatch()">
				<i class="ace-icon fa fa-check"  ></i>
				Dispatch
			</button>
		</logic:equal>
	</div>