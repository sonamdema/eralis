<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<%
	VehicleDTO dto = (VehicleDTO)request.getAttribute("DTO");
%>
<body >
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div>
	<table width="1000" border="0" align="center" style="border-collapse:collapse;">
	  <tr>
	    <td width="326" rowspan="4">
	    	<div align="center" style="display: block;"><img src="<%=request.getContextPath() %>/images/emission-logo.png" width="100px" height="100px"></div>
	    </td>
	    <td width="327"><div align="center"><strong><font color="#009900" style="font-size: 19px;">YANGKI AUTO EMISSION CENTER</font></strong></div></td>
	    <td width="325" rowspan="4">
	    	<div align="center" style="display: block;"><img src="<%=request.getContextPath() %>/images/rsta.png" width="100px" height="100px"></div>
	    </td>
	  </tr>
	  <tr>
	    <td><div align="center"><strong>Government Approved Agency</strong></div></td>
	  </tr>
	  <tr>
	    <td height="23"><div align="center"><strong>YANGKI AUTOMOBILES</strong></div></td>
	  </tr>
	  <tr>
	    <td height="23"><div align="center"><strong>P.O. Box 384</strong></div></td>
	  </tr>
	  <tr>
	    <td height="23" colspan="3">
	    	<div align="center"><strong><font color="#CC3300" style="font-size: 20px;"><u>VEHICLE EMISSION CERTIFICATE</u></font></strong></div>
	    </td>
	  </tr>
	  <tr>
	    <td height="23" colspan="3">
	    	<div align="center">
	        	<span>Authorized by Royal Government of Bhutan Vide letter No. RSTA/RS�-12/2016/2231�-36</span>
	        </div>
	    </td>
	  </tr>
	  <tr>
	    <td height="30" colspan="3">
	    </td>
	  </tr>
	  <tr>
	  	<td colspan="3">
	    	<table border="1" style="border-collapse: collapse;" width="100%">
	            <thead>
	                <tr>
	                    <td style="" class="text-left">EMISSION REF #</td>
	                    <td colspan="2"><%=request.getAttribute("EMISSION_ID")%></td>
	                    <td colspan="2" class="text-left">Tested Date</td>
	                    <td colspan="2"><%=dto.getTestedOn()%></td>
	                </tr>
	                <tr>
	                    <td class="text-left">Vehicle #</td>
	                    <td colspan="2"><%=dto.getVehicleNumber()%></td>
	                    <td colspan="2" class="text-left">Registration Date</td>
	                    <td colspan="2"><%=dto.getRegistrationDate()%></td>
	                </tr>
	                <tr>
	                    <td class="text-left">Make</td>
	                    <td colspan="2"><%=dto.getVehicleCompany() %></td>
	                    <td colspan="2" class="text-left">Type</td>
	                    <td colspan="2"><%=dto.getVehicleType() %></td>
	                </tr>
	                <tr>
	                    <td class="text-left">Chassis #</td>
	                    <td colspan="2"><%=dto.getChasisNumber()%></td>
	                    <td colspan="2" class="text-left">Engine #</td>
	                    <td colspan="2"><%=dto.getEngineNumber() %></td>
	                </tr>
	                <tr>
	                    <td></td>
	                    <td colspan="2"></td>
	                    <td colspan="2" class="text-left">Fuel</td>
	                    <td colspan="2">
	                        <logic:equal value="P" name="DTO" property="emissionTestType">
								Petrol
							</logic:equal>
							<logic:equal value="D" name="DTO" property="emissionTestType">
								Diesel
							</logic:equal>
	                    </td>
	                </tr>
	                <tr>
	                    <th align="center">Test Type</th>
	                    <th align="center">Test Value</th>
	                    <th align="center">Average/Spread</th>
	                    <th align="center">Result</th>
	                    <th align="center">Validity</th>
	                    <th align="center">Remarks</th>
	                </tr>
	            </thead>
	            	<logic:equal value="P" name="DTO" property="emissionTestType">
		                <tbody>
		                     <tr>
		                        <td align="center">CO</td>
		                        <td align="center"><%=dto.getcO()%></td>
		                        <td align="center" rowspan=""><%=dto.getcO()%></td>
		                        <td rowspan="" align="center"><%=dto.getTestResult() %></td>
		                        <td rowspan="" align="center"><%=dto.getExpiryDate() %></td>
		                        <td rowspan="" align="center"></td>
		                    </tr>
		                    <tr>
		                        <td colspan="5" align="right"><b>Amount (Nu)</b></td>
		                        <td align="right"><b><%=dto.getAmount() %></b></td>
		                    </tr>
		                </tbody>
	                </logic:equal>
	                <logic:equal value="D" name="DTO" property="emissionTestType">
		                <tbody>
		                    <tr>
		                        <td align="center">T1</td>
		                        <td align="center"><%=dto.getT1()%></td>
		                        <td rowspan="3" align="center"><%=dto.gethSU() %></td>
		                        <td rowspan="3" align="center"><%=dto.getTestResult() %></td>
		                        <td rowspan="3" align="center"><%=dto.getExpiryDate() %></td>
		                        <td rowspan="3" align="center"></td>
		                    </tr>
		                    <tr>
		                        <td align="center">T2</td>
		                        <td align="center"><%=dto.getT2()%></td>
		                    </tr>
		                    <tr>
		                        <td align="center">T3</td>
		                        <td align="center"><%=dto.getT3()%></td>
		                    </tr>
		                    <tr>
		                        <td colspan="5" align="right"><b>Amount (Nu)</b></td>
		                        <td align="right"><b><%=dto.getAmount() %></b></td>
		                    </tr>
		                </tbody>
		            </logic:equal>
	        </table>
	        <table width="100%">
	        	<tr><td></td></tr>
	        	<tr>
	                <td>
	                    <div align="center">
	                    	<logic:equal value="P" name="DTO" property="emissionTestType">
	                       		Maximum permissible CO level = 4.5 for < 2005 Model & 4.00 for 2005 > Model
	                       	</logic:equal>
	                       	<logic:equal value="D" name="DTO" property="emissionTestType">
	                       		Maximum permissible HSU level = 75% for < 2005 Model & 70% for 2005 > Model
	                       	</logic:equal>
	                    </div>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <div align="center">
	                        For queries contact @ 17666333 / 17177177, E�mail : yangkiauto@hotmail.com
	                    </div>
	                    <div align="center" id="resultMessageDiv" style='color:red;display:none'>
                       		<h3>Note : Re-test must be done within 15 Days after rectification.</h3>
	                    </div>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                   &nbsp;
	                </td>
	            </tr>
	             <tr>
	                <td>
	                   &nbsp;
	                </td>
	            </tr>
	             <tr>
	                <td>
	                   <div align="right">
	                   		<strong><u>Authorized Signatory</u></strong>
	                   </div>
	                </td>
	            </tr>
	        </table>
	    </td>
	  </tr>
	</table>
</div>
<script>
	var result	=	'<%=dto.getTestResult() %>';
	if(result=='FAIL')
	{
		document.getElementById('resultMessageDiv').style.display = "block";
	}
</script>
</body>
</html>