<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
String pageIdentifier = (String) request.getAttribute("page_identifier");
String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Replacement
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Replacement of Vehicle Registration Certificate
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
 <div class="col-lg-12">
  <html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
		<div id="incompleteValidationMsg"></div>
		<div id="Msg"></div>
    <div class="widget-box">
	  <div class="widget-body">
		<div class="widget-main">
			 <jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
			  <div class="form-group">
				<div class="col-lg-2">
					<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
				 </div>
					<div class="col-lg-3">
						<div class="input-group">
							<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
							<html:hidden property="vehicleId" styleClass="form-control" styleId="vehicleId" ></html:hidden>
							  <span class="input-group-btn">
						      	 <a href="#" onclick="openModal('duplication')" class="btn btn-purple btn-sm">
									<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
							  </span>
						 </div>
					  </div>
					  <div class="col-lg-3">
					  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
					  </div>
				    </div>
			     </div>
			   </div>
			</div>
	
	  <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Details</h5>
		 </div>
		    <div class="widget-body">
			   <div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
			      </div>
			  </div>
		   <div class="widget-box">
			   <div class="widget-header widget-header-small">
				    <h5 class="widget-title lighter">Vehicle Details</h5>
			   </div>
			     <div class="widget-body">
					  <div class="widget-main">
                                  <div class="form-group">
                                               <div class="col-lg-2">
                                                  <label class="control-label"> Company : </label>
                                               </div>
	                                                <div class="col-lg-4" id="displayCompany"> 
	                                                </div>
                                               <div class="col-lg-2">
                                                  <label class="control-label"> Model :</label>
                                               </div>
	                                                <div class="col-lg-4" id="displayModel">
	                                                </div> 
                                      </div>
                                     <div class="form-group">
                                                  <div class="col-lg-2">
								                     <label class="control-label"> Chasis Number :</label>
					                               </div>
	                                                   <div class="col-lg-4" id="displayChasisNumber"> 
	                                                   </div>
                                                   <div class="col-lg-2">
								                       <label class="control-label"> Engine Number :</label>
					                                </div>
	                                                    <div class="col-lg-4" id="displayEngineNumber">
	                                                    </div> 
                                    </div> 
                                    <div class="form-group">
                                                  <div class="col-lg-2">
								                      <label class="control-label"> Engine Type :</label>
					                               </div>
		                                                   <div class="col-lg-4" id="displayEngineType"> </div>
		                                                   <div class="col-lg-2">
								                      <label class="control-label"> Seat Capacity :</label>
					                                </div>
                                                          <div class="col-lg-4" id="displaySeatCapacity"> </div> 
                                    </div> 
                                    <div class="form-group">
                                                 <div class="col-lg-2">
								                       <label class="control-label"> Load Capacity :</label>
					                               </div>
                                                         <div class="col-lg-4" id="displayLoadCapacity"> </div>
                                                   <div class="col-lg-2">
								                       <label class="control-label"> Region :</label>
					                                </div>
                                                         <div class="col-lg-4" id="displayRegion"> </div> 
                                   </div> 
                                   <div class="form-group">
                                                 <div class="col-lg-2">
								                        <label class="control-label"> Last Issue Date :</label>
					                               </div>
                                                        <div class="col-lg-4" id="displayLastRegistrationDate"> </div>
                                                   <div class="col-lg-2">
								                       <label class="control-label"> Expiry Date :</label>
					                                </div>
                                                         <div class="col-lg-4" id="displayExpiryDate"> </div> 
                                    </div>   
                                    <div class="form-group">
                                                  <div class="col-lg-2">
								                        <label class="control-label"> Color :</label>
					                               </div>
                                                       <div class="col-lg-4" id="displayColor"> </div>
                                                 <div class="col-lg-2">
								                       <label class="control-label"> Status :</label>
					                             </div>
                                                      <div class="col-lg-4" id="displayStatus"> </div> 
                                           </div>  
               	                      </div>
               	                  </div>
               	              </div>
                          
					        <div class="widget-box">
						      <div class="widget-header widget-header-small">
							     <h5 class="widget-title lighter">Replacement Lists</h5>
						       </div>
						     <div class="widget-body">
								 <div class="widget-main">
								   <div id="duplicationHistoryTable">
			                       </div>
			                    </div>   
							</div>
						</div>
			       
			       <div class="widget-box">
				      <div class="widget-header widget-header-small">
					    <h5 class="widget-title lighter">Enter Replacement Details</h5>
				     </div>
				       <div class="widget-body">
			            <div class="widget-main">
                         <div class="form-group">
						   <div class="col-lg-2">
								<label>Region :</label>
					       </div>	
		                     <div class="col-sm-3">
							   <html:hidden property="region" styleId="vehicleRegionId" value="<%=regionId %>"></html:hidden>
								   <input type="text" disabled="disabled" id="regionName" class="form-control" value="<%=regionName %>"/>
                            </div>
                            <div style="display: none;">
		                        <div class="col-lg-2">
									   <label> Remarks :</label>
							    </div>   
		                         <div class="col-lg-3">
		                                 <html:textarea property="remarks" styleClass="form-control" styleId="remarks" value="NA"></html:textarea>
		                         </div> 
		                    </div>
                       </div>                       	
                     </div>
                   </div>
				</div>
			<div class="widget-box">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Attachments</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<div class="row">
								<div class="col-lg-12">
									
									<div class="form-group">
										<div class="col-lg-2">
											<label> Supporting Document:</label>
										</div>
										<div class="col-lg-3">
											<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
											<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
										</div>
						              </div>
		                     		 </div> 
		                   		</div>
		                	 </div>
						</div>
					</div>	
					<div class="row">
						<div id="displayMsgDiv"></div>
					</div>
					<div class="pull-left">
					    <html:hidden property="engineType" styleId="engineType"></html:hidden>
						<html:hidden property="vehicleType" styleId="vehicleType"></html:hidden>
						<html:hidden property="loadCapacity" styleId="loadCapacity"></html:hidden>
						<html:hidden property="seatCapacity" styleId="seatCapacity"></html:hidden>
						<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower"></html:hidden>
						<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt"></html:hidden>
						<html:hidden property="engineCC" styleId="engineCC"></html:hidden>
						<html:hidden property="customerId" styleId="customerID"></html:hidden>
						<html:hidden property="vehicleRegistrationType" styleId="ownerType"></html:hidden>
						<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
						<logic:equal value="Y" name="priviledge" property="isNew">
							   <button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
						</logic:equal>
					</div>
		</html:form>
	 </div>
   </div>
<div id="duplication" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
						  </div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="renewalListTable">
		  </div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
			<script type="text/javascript">

			   var pageIdentifier = "<%=pageIdentifier%>";
			   var pageId = "<%=pageId%>";
		
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				});

			 $('.fileupload').ace_file_input({
					no_file : 'No File ...',
					btn_choose : 'Choose',
					btn_change : 'Change',
					droppable : false,
					onchange : null,
					thumbnail : false,
					whitelist:'png|jpg|jpeg',
					blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
				});

			 var fileError;
	           	function validation(thisform,msgId,fileObj)
	           	{
	           		var fileId = fileObj.id;
	           		with(thisform)
	           		{
	           			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
	           			{
	           				document.getElementById(fileId).value = "";
	           				return false;
	           			}
	           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
	           			{
	           				document.getElementById(fileId).value = "";
	           				return false;
	           			}
	           		}
	           	}

			function searchRenewalInfo()
			{ 
				var vehicleNumber = $('#vehicleNumberRenewalModal').val();
				var engineNumber = $('#engineNumberRenewalModal').val();
				var chasisNumber = $('#chasisNumberRenewalModal').val();
				var cidNumber = $('#citizenIdRenewalModal').val();

				if(vehicleNumber == "")
					vehicleNumber = "NA";
				if(engineNumber == "")
					engineNumber = "NA";
				if(chasisNumber == "")
					chasisNumber = "NA";
				if(cidNumber == "")
					cidNumber = "NA";

				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=NA",
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewalListTable").html(responseText);
						$("#renewalListTable").show();
					}
				});
			}

			function searchVehicleNumber()
			{ 
				var vehicleId = $('#vehicleId').val();
				if(vehicleId == "")
					vehicleId = "NA";
				
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getDuplicationHistoryList&vehicleId="+vehicleId,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#duplicationHistoryTable").html(responseText);
						$("#duplicationHistoryTable").show();
					}
				});
			}
		//	function formSubmit()
		//	{	var pageId=$("#pageId").val();
			//	var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_duplication&pageId='+pageId,type:'POST',data: $("#vehicleForm").serialize()}; 
			  //  $("#vehicleForm").ajaxSubmit(options);
		        //$('#displayMsgDiv').show();
		        //setTimeout('hideStatus("displayMsgDiv")',4000);
			//}

			$(document).ready(function()
					{
						$('#submitBtn').click(function()
						{
							var returnVal = validateForm();

							if(returnVal == "1")
							{
								var identityTypeId;
							    var requestType = "VEHICLE";
								var serviceType = "DUPLICATION";
								var identityNo = $('#vehicleId').val();
								if( $("#engineType").val() == "Electric")
									identityTypeId = '0'; 
								else
									identityTypeId = $('#vehicleType').val();
								var loadingCapacity = $('#loadCapacity').val();
								var seatingCapacity = $('#seatCapacity').val();
								var vehicleHP = $('#vehicleHorsePower').val();
								var kilowatts = $('#vehicleKiloWatt').val();
								var engineCC = $('#engineCC').val();
								var purchaseDate = "";
								var saleDeedAmount = "";
								var saleDeedDate = "";
								
								getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate);
							}
							else
								return false;
						});
					});

			function validateForm()
			{
				var vehicleNumber = $('#displayVehicleNumber').val();
				$('#vehicleNumberValidation').hide();

				if(vehicleNumber == "")
				{
					$('#vehicleNumberValidation').show();
					$('#vehicleNumberValidation').get(0).scrollIntoView();
					return "0";
				}
				else
					return "1";
			}

						function formSubmit()
						{
							var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_duplication',type:'POST',data: $("#vehicleForm").serialize()}; 
						    $("#vehicleForm").ajaxSubmit(options);
					        $('#displayMsgDiv').show();
					        setTimeout('hideStatus("displayMsgDiv")',10000);
					        //setTimeout('reloadPage()',10000);
						}
		       </script>
           		<style>
					#vehicleForm .error { color: red; }
			   </style>	