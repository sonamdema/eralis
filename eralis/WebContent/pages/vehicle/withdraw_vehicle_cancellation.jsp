<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
  <div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Cancellation Withdrawal
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Withdrawn Cancellation of Vehicle
		</small>
	</h1>
</div><!-- /.page-header -->
 <div class="row">
  <div class="col-lg-12">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	
		<div class="row">
			<div id="incompleteValidationMsg"></div>
			<div id="Msg"></div>
		</div>
		
		<div class="widget-box">
		  <div class="widget-body">
			<div class="widget-main">
				<div class="form-group">
					<div class="col-lg-2">
						<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
					</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
								<html:hidden property="vehicleId" styleClass="form-control" styleId="vehicleId" ></html:hidden>
								  <span class="input-group-btn">
							        <a href="#" onclick="openModal('cancellation')" class="btn btn-purple btn-sm">
										<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
									</a>
								  </span>
							 </div>
						  </div>
						  <div class="col-lg-3">
						  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
						  </div>
					    </div>
				     </div>
				   </div>
				</div>
	  <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Details</h5>
		</div>						
		 <div class="widget-body">
			<div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
       </div>
     </div>
			
	  <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Vehicle Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
						<div class="form-group">
							  <div class="col-lg-2">
								   <label class=control-label> Company :</label>
							   </div>
		                         <div class="col-lg-3" id="displayCompany">
                   
                                  </div>
						   
						        <div class="col-lg-2">
								 <label class=control-label> Model :</label>
							    </div>
			                      <div class="col-lg-3" id="displayModel">
                 
                                </div> 
                         </div> 
                       <div class="form-group">
							<div class="col-lg-2">
								<label class=control-label> Color :</label>
							</div>
		                      <div class="col-lg-3" id="displayColor">
                   
                              </div>
						   
						    <div class="col-lg-2">
								<label class=control-label> Chasis Number :</label>
							</div>
			                 <div class="col-lg-3" id="displayChasisNumber">
                 
                            </div> 
                         </div>    
                        <div class="form-group">
							<div class="col-lg-2">
								<label class=control-label> Engine Number :</label>
							</div>
		                      <div class="col-lg-3" id="displayEngineNumber">
                   
                              </div>
                          <div class="col-lg-2">
								<label class=control-label> Engine Type :</label>
						 </div>
			                 <div class="col-lg-3" id="displayEngineType">
                 
                            </div> 
                     </div> 	
			        <div class="form-group">
							<div class="col-lg-2">
								<label class=control-label> Seat Capacity :</label>
							</div>
		                      <div class="col-lg-3" id="displaySeatCapacity">
                   
                              </div>
                          <div class="col-lg-2">
								<label class=control-label> Load Capacity :</label>
						 </div>
			                 <div class="col-lg-3" id="displayLoadCapacity">
                 
                             </div> 
                           </div> 	
			             </div>
            	       </div>
            	    </div>
  		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Vehicle Cancellation Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<table  id="cancellation-search-table"  class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Cancellation Date</th>
									<th>Receipt No</th>
									<th>Receipt Date</th>
									<th>Cancellation Reason</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="cancelledDate"></div></td>
									<td><div id="receiptNo"></div></td>
									<td><div id="receiptDate"></div></td>
									<td><div id="cancelReason"></div></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	<div class="widget-box">
      <div class="widget-header widget-header-small">
	     <h5 class="widget-title lighter">Withdrawn Cancellation</h5>
        </div>
	       <div class="widget-body">
            <div class="widget-main">
               <div class="form-group">
                   <div class="col-lg-2">
					<label>Withdrawn Date :<span style="color: #ff0000">*</span> </label>
			 		</div>         
                      <div class="col-lg-3">
                               <div class="input-group">
							<html:text property="withdrawnDate" styleClass="form-control date-picker" styleId="cancellationDate" readonly="true"></html:text>
							<span class="input-group-addon" style="cursor: pointer">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>
                       </div>
                          <div class="col-lg-2">
					    		<label>Withdrawn Reason :<span style="color: #ff0000">*</span> </label>
		         		</div>
                        <div class="col-lg-3">
                          <html:textarea property="withdrawnReason"></html:textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-2">
							<label> Document :</label>
						</div>
						<div class="col-lg-3">
							<html:file property="imgPath" styleId="invoice" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
							<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
							<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
						</div>
                       </div>
                     </div>
                   </div>
				</div>	
				<div class="row">
					<div id="displayMsgDiv"></div>
				</div>
			<div class="pull-left">
				<html:hidden property="customerId" styleId="customerID"></html:hidden>
				<button type="button" class="btn btn-primary btn-sm" id="formSubmit">Save</button>
			</div>
		</html:form>
	</div>
  </div>

<div id="cancellation" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
								
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
				
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
				
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
				
						</div>
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="renewalListTable">
		  </div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
		<script type="text/javascript">
		
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>
			    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
			<script>
			function searchRenewalInfo()
			{ 
				var vehicleNumber = $('#vehicleNumberRenewalModal').val();
				var engineNumber = $('#engineNumberRenewalModal').val();
				var chasisNumber = $('#chasisNumberRenewalModal').val();
				var cidNumber = $('#citizenIdRenewalModal').val();

				if(vehicleNumber == "")
					vehicleNumber = "NA";
				if(engineNumber == "")
					engineNumber = "NA";
				if(chasisNumber == "")
					chasisNumber = "NA";
				if(cidNumber == "")
					cidNumber = "NA";


				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=WITHDRAW_VEH_CANCELLATION&searchType=WITHDRAW_VEH_CANCELLATION",
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewalListTable").html(responseText);
						$("#renewalListTable").show();
					}
				});
			}


			function searchVehicleNumber()
			{ 
				var vehicleNumber = $('#displayVehicleNumber').val();

				if(vehicleNumber == "")
					vehicleNumber = "NA";
				
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getRenewalHistoryList&vehicleNumber="+vehicleNumber,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewalHistoryTable").html(responseText);
						$("#renewalHistoryTable").show();
					}
				});
			}
			
			$(document).ready(function()
					{
						$("#vehicleForm").validate({
									rules:
									{
										cancellationDate:{
											required:true
											},
										cancellationReason:{
											required:true,
											},
										receiptNo:{
											required:true,
											},
										receiptDate:{
											required:true
												},
									      },    
									messages:
									{
										cancellationDate:{required: "Please Select a Date"},
										cancellationReason:{required: "Please Select a Cancellation Reason"},
										receiptNo:{required: "Please Provide Receipt Number"},
										receiptDate:{required: "Please Select a Date"},
									}
							});

						$('#formSubmit').click(function(){
							if($('#vehicleForm').valid()) 
							{
								var vehicleNumber = $('#displayVehicleNumber').val();
								
								if(vehicleNumber == "")
								{
									$('#vehicleNumberValidation').show();
									$('#vehicleNumberValidation').get(0).scrollIntoView();
									return false;
								}
								else
								{
									var pageId=$("#pageId").val();
									var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=withdraw_veh_cancellation&pageId='+pageId,type:'POST',data: $("#vehicleForm").serialize()};  
								    $("#vehicleForm").ajaxSubmit(options);
							        $('#displayMsgDiv').show();
							        setTimeout('hideStatus("displayMsgDiv")',5000);
							        setTimeout('reloadPage()',5000);
								}
							}
							else 
							{
								return false;
							}
						});
					});

			    var context = "<%=request.getContextPath()%>";

			   var pageIdentifier = "<%=pageIdentifier%>";
			   var pageId = "<%=pageId%>";
			   </script>
              
               <style>
					#vehicleForm .error { color: red; }
			   </style>
           
           
