 <%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	EralisUserRolePriviledge userRolePriv = null;
	String regionId = null, regionName = null, roleCode = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		Role currentRole = userRolePriv.getCurrentRole();
		roleCode = currentRole.getRoleCode();
	}
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Fitness Test
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Fitness Test of Vehicle
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
 <div class="col-lg-12">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
		<div id="incompleteValidationMsg"></div>
		<div id="Msg"></div>
		<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
		<div class="widget-box">
	     <div class="widget-body">
		    <div class="widget-main">
			  <div class="form-group">
				<div class="col-lg-2">
					<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
				</div>
					<div class="col-lg-3">
						<div class="input-group">
							<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
							  <span class="input-group-btn">
						      	<a href="#" onclick="openModal('fitness')" class="btn btn-purple btn-sm">
									<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
							  </span>
						 </div>
					  </div>
					   <div class="col-lg-3">
					  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
					  </div>
				    </div>
			   </div>
		   </div>
	  </div>	
	  
	  <div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Owner Details</h5>
	 </div>
	    <div class="widget-body">
		   <div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
        </div>
     </div>
	  
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Vehicle Details</h5>
	 	</div>
	    <div class="widget-body">
		   <div class="widget-main">
			       <div class="form-group">
                       <div class="col-lg-2">
						    <label class="control-label">Make :</label>
					    </div>
                        <div class="col-lg-4" id="displayCompany"> </div>
                        <div class="col-lg-2">
			             <label class="control-label">Model  :</label>
                      	</div>
                        <div class="col-lg-4" id="displayModel"> </div> 
                    </div> 
	              <div class="form-group">
                          <div class="col-lg-2">
						<label class="control-label">Engine No :</label>
			         </div>
                           <div class="col-lg-4" id="displayEngineNumber"> </div>
                           <div class="col-lg-2">
						<label class="control-label">Chassis No :</label>
			         </div>
                           <div class="col-lg-4"id="displayChasisNumber"> </div> 
                  </div>
                <div class="form-group">
                    <div class="col-lg-2">
				       <label class="control-label">Vehicle Type :</label>
			        </div>
                     <div class="col-lg-4" id="displayVehicleTypeName"></div>
                     <div class="col-lg-2">
                     	<label class="control-label">Engine Type :</label>
                     </div>
                     <div class="col-lg-4" id="displayEngineType">
                     </div>
              </div>                             
           </div>
        </div>
     </div>	
  
   <div class="widget-box">
	   <div class="widget-header widget-header-small">
		    <h5 class="widget-title lighter">Fitness Test List</h5>
	   </div>
	     <div class="widget-body">
		   <div class="widget-main">
		   		<div id="fitnessHistoryTable"></div>
            </div>
    	 </div>
    </div>
   
   <div class="widget-box">
	      <div class="widget-header widget-header-small">
		    <h5 class="widget-title lighter">Fitness Test Details</h5>
	     </div>
	       <div class="widget-body">
			 <div class="widget-main">
			 
			 <div class="form-group">
			 	<div class="col-lg-12">
			 	
			 		<logic:notEmpty name="FITNESS_ITEM_LIST">
			 			<%
		 					int a = 0;
		 				%>
			 			<logic:iterate id="fitness" name="FITNESS_ITEM_LIST">
				 			<div class="col-lg-4">
				 				<div class="widget-box">
					 				<div class="widget-header widget-header-small">
				    					<h5 class="widget-title lighter">
				    						<bean:write name="fitness" property="itemName"/>
				    					</h5>
			     					</div>
			       					<div class="widget-body">
					 					<div class="widget-main">
					 						<ul class="list-group">
						 						<logic:iterate id="subFitness" name="fitness" property="fitnessItemList" type="bt.gov.rsta.eralis.dto.vehicle.FitnessItemDTO" indexId="index">
						 							<li class="list-group-item">
						 								<input type="hidden" id='<%="fitnessItemId_"+a %>' value="<%=subFitness.getItemId()%>">
								         				<strong><bean:write name="subFitness" property="itemName"/></strong>
								         				<br>
								         				<input type="radio" class="ace" name='<%="subFitnessRadio_"+a %>' id='<%="itemIdOK_"+a %>'>
								         				<span class="lbl">OK</span>
								         				<script type="text/javascript">
									  		                $("#<%="itemIdOK_"+a %>").attr('checked', 'checked');
									                    </script>
								         				&nbsp;
								         				<input type="radio" class="ace" name='<%="subFitnessRadio_"+a %>' id='<%="itemIdX_"+a %>'>
								         				<span class="lbl">Defective</span>
								         				&nbsp;
								         				<input type="radio" class="ace" name='<%="subFitnessRadio_"+a %>' id='<%="itemIdNA_"+a %>'>
								         				<span class="lbl">Not applicable</span>
								         			</li>
								         			<%
								         				a++;
								         			%>
						 						</logic:iterate>
					 						</ul>
					 					</div>
					 				</div>
				 				</div>
				 			</div>
				 		</logic:iterate>
			 		</logic:notEmpty>
			 	</div>
			 </div> 
	           
		     <div class="form-group">
		         <div class="col-lg-2">
					  <label> Tested On : <span style="color: #ff0000">*</span> </label>
				 </div>
		        <div class="col-lg-3">
		              <div class="input-group">
		                  <html:text property="testedOn" styleClass="form-control date-picker" styleId="testedOn" readonly="true" onchange="calculateValidity(this.value)"></html:text>
			            <span class="input-group-addon">
				          <i class="fa fa-calendar bigger-110"></i>
			           </span>
		             </div>
		        </div>
		        <div class="col-lg-2">
					  <label> Valid Until : <span style="color: #ff0000">*</span> </label>
				 </div>  
		       	<div class="col-lg-3">
		           	<div class="input-group">
			            <html:text property="validUntil" styleClass="form-control date-picker" styleId="validUntil" readonly="true"></html:text>
				        <span class="input-group-addon">
					       <i class="fa fa-calendar bigger-110"></i>
				        </span>
		        	</div>
		     	</div>
		  	</div>
     
        	<div class="form-group">
        	
        		<div class="col-lg-2">
					  <label> Inspected By : <span style="color: #ff0000">*</span> </label>
				 </div>   
		        <div class="col-lg-3">
					<html:select property="inspectedBy" styleClass="form-control" styleId="inspectedBy" >
					      <html:option value="">--SELECT--</html:option>
			   		      <html:optionsCollection name="userList" label="headerName" value="headerId"/>
			        </html:select>
		        </div>
	           <div class="col-lg-2">
				  <label> Remarks : </label>
			   </div>                 
	             <div class="col-lg-3">
	                 <html:textarea property="remarks" styleClass="form-control" styleId="remarks" ></html:textarea>
	             </div> 
           	</div>                  	
       
        </div>
     </div>                  	
   </div>             
   
<div class="pull-left">
	<html:hidden property="customerId" styleId="customerID"></html:hidden>
	<input type="hidden" id="checkedVals" name="checkedVals"/>
	<html:hidden property="vehicleId" styleId="vehicleId"></html:hidden>
	<html:hidden property="engineType" styleId="engineType"></html:hidden>
	<html:hidden property="vehicleType" styleId="vehicleType"></html:hidden>
	<input type="hidden" id="vehicleTypeDesc"/>
	<html:hidden property="loadCapacity" styleId="loadCapacity"></html:hidden>
	<html:hidden property="seatCapacity" styleId="seatCapacity"></html:hidden>
	<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower"></html:hidden>
	<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt"></html:hidden>
	<html:hidden property="engineCC" styleId="engineCC"></html:hidden>
	<html:hidden property="customerId" styleId="customerID"></html:hidden>
	<html:hidden property="vehicleRegistrationType" styleId="ownerType"></html:hidden>
	<%
		if(!"MVI".equals(roleCode)){
	%>
		<button type="button" class="btn btn-primary btn-sm" id="calculatePaymentBTN">Calculate Payment</button> 
	<%
		} else if("MVI".equals(roleCode)){
	%>
		<button type="button" class="btn btn-primary btn-sm" onclick="mviFormSubmit()">Submit</button>
	<%
		}
	%>
	<div id="displayMsgDiv">
 	</div>
</div>
		</html:form>
	</div>
</div>
  
<div id="fitness" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
                             <div class="col-sm-4">
                             	<input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                             </div>
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
				
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
				
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
				
						</div>
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="renewalListTable">
		  </div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>
			  <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
			<script>

			function searchRenewalInfo()
			{ 
				var vehicleNumber = $('#vehicleNumberRenewalModal').val();
				var engineNumber = $('#engineNumberRenewalModal').val();
				var chasisNumber = $('#chasisNumberRenewalModal').val();
				var cidNumber = $('#citizenIdRenewalModal').val();

				if(vehicleNumber == "")
					vehicleNumber = "NA";
				if(engineNumber == "")
					engineNumber = "NA";
				if(chasisNumber == "")
					chasisNumber = "NA";
				if(cidNumber == "")
					cidNumber = "NA";


				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=NA",
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewalListTable").html(responseText);
						$("#renewalListTable").show();
					}
				});
			}

			function searchVehicleNumber()
			{ 
				var vehicleId = $('#vehicleId').val();
				 
				if(vehicleId == "")
					vehicleId = "NA";

				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getFitnessHistoryList&vehicleId="+vehicleId,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#fitnessHistoryTable").html(responseText);
						$("#fitnessHistoryTable").show();
					}
				});
			}

			function calculateValidity(testDate)
			{
				var vehicleId = $('#vehicleId').val();
				var vehicleTypeDesc = $('#vehicleTypeDesc').val();
				
				$.ajax
				({
					async: true,
					type: 'GET',
					url: context+'/EralisCommonServlet?q=getFitnessValidity&vehicleId='+vehicleId+'&testDate='+testDate+"&vehicleTypeDesc="+vehicleTypeDesc,	
					success: function(xml)
					{ 
						$(xml).find('xml-response').each(function()
						{
							var validity = $(this).find('validity').text();
							$('#validUntil').val(validity);
						});
					}
				});
			}
			
			$(document).ready(function()
			{
				$("#vehicleForm").validate({
					rules:
					{
						testedOn:{
							required:true
							},
						inspectedBy:{
								required:true
							}	
					},    
					messages:
					{
						testedOn:{required: "Please Select a Date"},
						inspectedBy:{required: "Please Select a inspected by"}
					}
				});
				
				$('#calculatePaymentBTN').click(function()
				{
					if($('#vehicleForm').valid()) 
					{
						var vehicleNumber = $('#displayVehicleNumber').val();

						if(vehicleNumber == "")
						{
							$('#vehicleNumberValidation').show();
							$('#vehicleNumberValidation').get(0).scrollIntoView();
							return false;
						}
						else
						{
							getCheckedVals();
							var identityTypeId;
							var requestType = "VEHICLE";
							var serviceType = "RWC";
							var identityNo = $('#vehicleId').val();
							if( $("#engineType").val() == "Electric")
								identityTypeId = '0'; 
							else
								identityTypeId = $('#vehicleType').val();
							var loadingCapacity = $('#loadCapacity').val();
							var seatingCapacity = $('#seatCapacity').val();
							var vehicleHP = $('#vehicleHorsePower').val();
							var kilowatts = $('#vehicleKiloWatt').val();
							var engineCC = $('#engineCC').val();
							var purchaseDate = "";
							var saleDeedAmount = "";
							var saleDeedDate = "";
							getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate);
						}
					}
					else
						return false;
				});
			}); 

			function formSubmit()
			{
				var pageId=$("#pageId").val();
				var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_fitness&pageId='+pageId,type:'POST',data: $("#vehicleForm").serialize()}; 
			    $("#vehicleForm").ajaxSubmit(options);
		       	$('#displayMsgDiv').show();
		        setTimeout('hideStatus("displayMsgDiv")',10000);
		        setTimeout('reloadPage()',10000);
			}

			function mviFormSubmit()
			{
				if($('#vehicleForm').valid()) 
				{
					var vehicleNumber = $('#displayVehicleNumber').val();

					if(vehicleNumber == "")
					{
						$('#vehicleNumberValidation').show();
						$('#vehicleNumberValidation').get(0).scrollIntoView();
						return false;
					}
					else
					{
						getCheckedVals();

						$('#receiptNo').val("NA");
						$('#id-date-picker-1').val("00-00-0000");
						$('#amount').val("0");
						
						var pageId=$("#pageId").val();
						var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_fitness&pageId='+pageId,type:'POST',data: $("#vehicleForm").serialize()}; 
					    $("#vehicleForm").ajaxSubmit(options);
				       	$('#displayMsgDiv').show();
				        setTimeout('hideStatus("displayMsgDiv")',10000);
				        setTimeout('reloadPage()',10000);
					}
				}
				else
					return false;
			}

			function getCheckedVals()
			{
				var vals = '';
				var countFlag = 0;

			 	for(var i=0;i<20;i++)
			  	{
				 	var fitnessItemId = $('#fitnessItemId_'+i).val();

				 	var item = "";

				 	if($('#itemIdOK_'+i).is(':checked'))
				 		item ="OK";
				 	if($('#itemIdX_'+i).is(':checked'))
				 		item ="X";
				 	if($('#itemIdNA_'+i).is(':checked'))
				 		item ="NA";

			 		if(parseInt(countFlag) > 0){
						vals = vals + '#';
				 	}

				 	vals = vals + fitnessItemId + '~' + item;
				 	countFlag++;
			  	}
			  	
				$('#checkedVals').val(vals);
			}
			
		    var context = "<%=request.getContextPath()%>";

	        <%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		    %>

		   var pageIdentifier = "<%=pageIdentifier%>";
		   var pageId = "<%=pageId%>";


		  </script>
        <style>
			#vehicleForm .error { color: red; }
		</style>   