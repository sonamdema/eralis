<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<style>
	#vehicleForm .error { color: red; }
</style>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Emission Test
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Emission Test result
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
  <div id="emissionPrintPreview"></div>
  <div class="col-lg-12" id="emissionForm">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
		<div id="Msg"></div>
		<div id="incompleteValidationMsg"></div>
     <div class="widget-box">
	  <div class="widget-body">
		<div class="widget-main">
			  <div class="form-group">
				<div class="col-lg-2">
					<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
				</div>
					<div class="col-lg-3">
						<div class="input-group">
							<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
							  <span class="input-group-btn">
						      	<a href="#" onclick="openModal('emission')" class="btn btn-purple btn-sm">
									<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
							  </span>
						 </div>
					  </div>
					   <div class="col-lg-3">
						  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
						  </div>
				    </div>
			     </div>
			   </div>
			</div>

  <div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Owner Details</h5>
	 </div>
	    <div class="widget-body">
		   <div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
        </div>
      </div>

   <div class="widget-box">
	   <div class="widget-header widget-header-small">
		    <h5 class="widget-title lighter">Emission Test List</h5>
	   </div>
	     <div class="widget-body">
			  <div class="widget-main">
				  <div id="emissionHistoryTable"></div>
       	     </div>
       	  </div>
   </div>
    
    <div class="widget-box">
       <div class="widget-header widget-header-small">
	     <h5 class="widget-title lighter">Emission Test Details</h5>
       </div>
         <div class="widget-body">
            <div class="widget-main">
            	<div class="form-group" id="uploadFileDiv">
            		<div class="col-lg-2">
            			<label>Upload Test File: <span style="color: #ff0000">*</span> </label>
            		</div>
            		<div class="col-lg-3">
            			<input type="file" class="form-control" id="fileUpload"/>
            		</div>
            	</div>
	            <div class="form-group">
		              <div class="col-lg-2">
						     <label> Price : </label>
					   </div>       
	                   <div class="col-lg-3">
	                       <html:text property="amount" styleId="price" styleClass="form-control"></html:text>
	                   </div>
	               </div> 
                 <div class="form-group">
                    <div class="col-lg-2">
				       <label> Tested On : <span style="color: #ff0000">*</span> </label>
			        </div>
                      <div class="col-lg-3">
                          <div class="input-group">
								<html:text property="testedOn" styleClass="form-control date-picker" styleId="testedOn" readonly="true" onchange="calculateValidityDate(this.value)"></html:text>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
					     </div>
                      </div>
                        <div class="col-lg-2">
				            <label> Valid Until : <span style="color: #ff0000">*</span> </label>
			            </div>         
                          <div class="col-lg-3">
                            <div class="input-group">
			                    <html:text property="validUntil" styleClass="form-control" styleId="validUntil" readonly="true"></html:text>
				                 <span class="input-group-addon">
					                <i class="fa fa-calendar bigger-110"></i>
				                 </span>
		                   </div>
                      </div>
                 </div>
                 <div class="form-group">
                    <div class="col-lg-2">
				         <label> Test Location : <span style="color: #ff0000">*</span> </label>
			        </div>   
                    <div class="col-lg-3">
                        <html:select property="testLocation" styleClass="form-control" styleId="testLocation">
						  <html:option value="">--SELECT--</html:option>
						  <html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
						</html:select>
                    </div>
                     <div class="col-lg-2">
				         <label> Engine Type : <span style="color: #ff0000">*</span> </label>
			        </div>   
                    <div class="col-lg-3">
                      	<div class="radio control-label">
							<label>
								<html:radio property="personalRadio" styleClass="ace" styleId="petrol" value="P" onclick="disableField(this.value)"></html:radio>	
								<span class="lbl"> Petrol</span>
							</label>
							<label>
								<html:radio property="personalRadio" styleClass="ace" styleId="diesel" value="D" onclick="disableField(this.value)"></html:radio>	
								<span class="lbl"> Diesel</span>
							</label>
				   		</div>  
                    </div>
                  </div>
	            <div class="form-group">
               		<div class="col-lg-2">
				       <label> CO : <span style="color: #ff0000">*</span> </label>
			   		</div> 
                  	<div class="col-lg-3">
                      <html:text property="cO" styleClass="form-control" styleId="cO" onchange="evaluateResult('PETROL')" readonly="true"></html:text>
                  	</div>
	            </div>
	            <div class="form-group">
	            	<div class="col-lg-2">
				       <label> Test Type : <span style="color: #ff0000">*</span> </label>
			    	</div>        
                  	<div class="col-lg-10">
                  		<div class="col-lg-1">
                  			<label>T1:</label>
                  		</div>
                  		<div class="col-lg-3">
                  			<html:text property="t1" styleId="t1" onchange="calculateAverage(this.value, this.id)" readonly="true"></html:text>
                  		</div>
                  		<div class="col-lg-1">
                  			<label>T2:</label>
                  		</div>
                  		<div class="col-lg-3">
                  			<html:text property="t2" styleId="t2" onchange="calculateAverage(this.value, this.id)" readonly="true"></html:text>
                  		</div>
                  		<div class="col-lg-1">
                  			<label>T3:</label>
                  		</div>
                  		<div class="col-lg-3">
                  			<html:text property="t3" styleId="t3" onchange="calculateAverage(this.value, this.id)" readonly="true"></html:text>
                  		</div>
                  		<html:hidden property="hSU" styleClass="form-control" styleId="hSU"></html:hidden>
                  	</div>
	            </div>
	          	<div class="form-group">
	              <div class="col-lg-2">
					     <label> Test Result(Pass/Fail) : </label>
				   </div>       
                   <div class="col-lg-3">
                   		<span id="result"></span>
                       <html:hidden property="testResult" styleClass="form-control" styleId="testResult"></html:hidden>
                       <input type="hidden" id="vehicleTypeDesc">
                   </div>
               </div> 
         </div>
     </div>
</div>
<div id="displayMsgDiv"></div>		
<div class="pull-left">
   	<html:hidden property="customerId" styleId="customerID"></html:hidden>
  	<html:hidden property="vehicleId" styleId="vehicleId"></html:hidden>
   	<input type="hidden" id="manufactureYear"/>
   	<input type="hidden" id="engineType"/>
   	<input type="hidden" id="vehicleTypeDesc"/>
   	<input type="hidden" id="testResult"/>
	<button type="button" class="btn btn-primary btn-sm" id="formSubmit">Save</button>
</div>
		</html:form>
	</div>
  </div>

<div id="emission" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
								
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
				
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
				
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
				
						</div>
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="renewalListTable">
		  </div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

		<script type="text/javascript">

			<%
				String coBefore2005 = (String)request.getAttribute("CO_BEFORE_2005");
				String hsuBefore2005 = (String)request.getAttribute("HSU_BEFORE_2005");
				String coAfter2005 = (String)request.getAttribute("CO_AFTER_2005");
				String hsuAfter2005 = (String)request.getAttribute("HSU_AFTER_2005");
			%>
		
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>
			     <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
			<script>

			 $('#price').attr('readOnly', true);
			function disableField(val)
			{
				$('#t1').attr('readonly',false);
				$('#t2').attr('readonly',false);
				$('#t3').attr('readonly',false);
				$('#cO').attr('readonly',false);
				
				if(val == "P"){
					$('#t1').attr('readonly',true);
					$('#t2').attr('readonly',true);
					$('#t3').attr('readonly',true);

					$('#t1').val('0');
					$('#t2').val('0');
					$('#t3').val('0');
					$('#hSU').val('0');
				}
				else if(val == "D"){
					$('#cO').attr('readonly',true);
					$('#cO').val('0');
				}
			}

			function calculateAverage(val, id)
			{
				var total = 0;
				for(var i=1; i<=3; i++)
				{
					total += parseInt($('#t'+i).val());
				}

				$('#hSU').val(total);

				if(id == "t3")
				{
					var totalValue = $('#hSU').val();
					var average = parseInt(totalValue)/3;
					$('#hSU').val(average);

					evaluateResult('DIESEL');
				}
			}

			function searchRenewalInfo()
			{ 
				var vehicleNumber = $('#vehicleNumberRenewalModal').val();
				var engineNumber = $('#engineNumberRenewalModal').val();
				var chasisNumber = $('#chasisNumberRenewalModal').val();
				var cidNumber = $('#citizenIdRenewalModal').val();
				if(vehicleNumber == "")
					vehicleNumber = "NA";
				if(engineNumber == "")
					engineNumber = "NA";
				if(chasisNumber == "")
					chasisNumber = "NA";
				if(cidNumber == "")
					cidNumber = "NA";


				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=EMISSION&searchType=EMISSION",
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewalListTable").html(responseText);
						$("#renewalListTable").show();
					}
				});
			}

			function searchVehicleNumber()
			{ 
				var testResult	=	$("#testResult").val();
				var vehicleTypeDesc	=	$("#vehicleTypeDesc").val();
				if(vehicleTypeDesc=='TWO_WHEELER' && testResult=='FAIL')
				{
					$("#price").val('50');
				}
				else if(vehicleTypeDesc=='TWO_WHEELER')
				{
					$("#price").val('100');
				}
				else if(vehicleTypeDesc=='LIGHT_VEHICLE' && testResult=='FAIL')
				{
					$("#price").val('75');
				}
				else if(vehicleTypeDesc=='LIGHT_VEHICLE')
				{
					$("#price").val('150');
				}

				else if((vehicleTypeDesc=='HEAVY_VEHICLE' || vehicleTypeDesc=='MEDIUM_VEHICLE') && testResult=='FAIL')
				{
					$("#price").val('100');
				}
				else if((vehicleTypeDesc=='HEAVY_VEHICLE' || vehicleTypeDesc=='MEDIUM_VEHICLE'))
				{
					$("#price").val('200');
				}
				else if(vehicleTypeDesc=='TAXI' && testResult=='FAIL')
				{
					$("#price").val('75');
				}
				else if(vehicleTypeDesc=='TAXI')
				{
					$("#price").val('100');
				}
				else if((vehicleTypeDesc=='MEDIUM_BUS' || vehicleTypeDesc=='HEAVY_BUS')  && testResult=='FAIL')
				{
					$("#price").val('100');
				}
				else if((vehicleTypeDesc=='MEDIUM_BUS' || vehicleTypeDesc=='HEAVY_BUS'))
				{
					$("#price").val('200');
				}
				
				var vehicleId = $('#vehicleId').val();
				 
				if(vehicleId == "")
					vehicleId = "NA";

				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getEmissionHistoryList&vehicleId="+vehicleId,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#emissionHistoryTable").html(responseText);
						$("#emissionHistoryTable").show();

						selectEngineType();
					}
				});
			}

			function selectEngineType()
			{
				var engineType = $('#engineType').val();

				if(engineType == "Petrol"){
					val = "P";
					$('#petrol').attr('checked', true);
					$('#uploadFileDiv').show();
					$("#testedOn").removeClass("date-picker");
				}
				else {
					val = "D";
					$('#diesel').attr('checked', true);
					$('#uploadFileDiv').hide();
				}

				disableField(val);
			}

			function calculateValidityDate(testedDate)
			{
				var vehicleTypeDesc	=	$("#vehicleTypeDesc").val();
				var testDateArray = new Array();
				testDateArray = testedDate.split("/");
				var day = testDateArray[0];
				var month = testDateArray[1];
				var year = testDateArray[2];
				
				if(vehicleTypeDesc=='LIGHT_VEHICLE' || vehicleTypeDesc=='TWO_WHEELER')
				{
					// The value of `meses`
					var intYear = parseInt(year);
					var noOfYears = parseInt(1);
					validityYear = intYear + noOfYears;
					$('#validUntil').val(day+"/"+month+"/"+validityYear);
				}
				else{
					var d = new Date(year, month, day);
					d.setMonth(d.getMonth() + 6);
					month	=	d.getMonth(); 
					if(month<9)
					{
						month	=	"0"+d.getMonth();
					}
					$('#validUntil').val(day+"/"+month+"/"+d.getFullYear());
				}
			}
			
			function evaluateResult(engineType)
			{
				var manufactureYear = $('#manufactureYear').val();
				var coBefore2005 = "<%=coBefore2005%>";
				var hsuBefore2005 = "<%=hsuBefore2005%>";
				var coAfter2005 = "<%=coAfter2005%>";
				var hsuAfter2005 = "<%=hsuAfter2005%>";
				var co = $('#cO').val();
				var hsu = $('#hSU').val();
				
				if(engineType == "PETROL")
				{
					
					if(parseInt(manufactureYear) <= 2005 && co > coBefore2005)
					{
						$('#testResult').val("FAIL");
						$('#result').html('<span class="label label-danger arrowed-in">FAIL</span>');
					}
					else if(parseInt(manufactureYear) > 2005 &&  co > coAfter2005)	
					{
						$('#testResult').val("FAIL");
						$('#result').html('<span class="label label-danger arrowed-in">FAIL</span>');
					}
					else {
						$('#testResult').val("PASS");
						$('#result').html('<span class="label label-success arrowed-in">PASS</span>');
					}
				}
				else
				{
					if(parseInt(manufactureYear) <= 2005 &&
							parseInt(hsu) > hsuBefore2005)
					{
						$('#testResult').val("FAIL");
						$('#result').html('<span class="label label-danger arrowed-in">FAIL</span>');
					}
					else if(parseInt(manufactureYear) > 2005 &&
							parseInt(hsu) > hsuAfter2005)	
					{
						$('#testResult').val("FAIL");
						$('#result').html('<span class="label label-danger arrowed-in">FAIL</span>');
					}
					else {
						$('#testResult').val("PASS");
						$('#result').html('<span class="label label-success arrowed-in">PASS</span>');
					}
				}
			}

			$(document).ready(function()
			{
				$("#vehicleForm").validate({
							rules:
							{
								testedOn:{
									required:true
									},
								validUntil:{
									required:true,
									},
								testLocation:{
									required:true
									},
								cO:{
									required:true
								    },
							    hSU:{
									required:true
									},
								testResult:{
									required:true
									},
								testFrequency:{
									required:true
									},
							},    
							messages:
							{
								testedOn:{required: "Please Select a Date"},
								validUntil:{required: "Please Select a Date"},
								testLocation:{required: "Please Provide Receipt Number"},
								cO:{required: "Please Provide CO"},
								hSU:{required: "Please Provide HSU"},
								testResult:{required: "Please Provide test result"},
								testFrequency:{required: "Please Provide test frequency"},
							}
					});

				$('#formSubmit').click(function(){
					if($('#vehicleForm').valid()) 
					{
						var vehicleNumber = $('#displayVehicleNumber').val();
						
						if(vehicleNumber == "")
						{
							$('#vehicleNumberValidation').show();
							$('#vehicleNumberValidation').get(0).scrollIntoView();
							return false;
						}
						else
						{
							var pageId=$("#pageId").val();
							var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_emission&pageId='+pageId,type:'POST',data: $("#vehicleForm").serialize()}; 
						    $("#vehicleForm").ajaxSubmit(options);
					        $('#displayMsgDiv').show();
					        $('#formSubmit').attr('disabled', true);
					        setTimeout('reloadPage()',10000);
						}
					}
					else 
					{
						return false;
					}
				});
			}); 
			    var context = "<%=request.getContextPath()%>";

		        <%
				String pageIdentifier = (String) request.getAttribute("page_identifier");
				String pageId = (String) request.getAttribute("page_id");
			    %>

			   var pageIdentifier = "<%=pageIdentifier%>";
			   var pageId = "<%=pageId%>";


			   $(function () 
				{
				    'use strict';
				    var url = '<%=request.getContextPath()%>/FileUploadServlet';
				    $('#fileUpload').fileupload
				    ({
						url : url,
						autoUpload: true,
						acceptFileTypes: /(\.|\/)(dat)$/i,
						maxFileSize: 1000000,
						type: "GET",
						success: function(xml) 
						{
							$(xml).find('xml-response').each(function()
							{
								var coValue = $(this).find('co-value').text();
								//var hcValue = $(this).find('hc-value').text();
								var testDate = $(this).find('test-date').text();

								$('#cO').val(coValue);
								$('#testedOn').val(testDate);

								calculateValidityDate(testDate);
								evaluateResult('PETROL');
							})
						}
				   });
				});

				function printEmissionReport(emissionId)
				{
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/vehicle.html?method=print_emission_report&emissionId="+emissionId,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							var mywindow = window.open('', 'my div', 'height=400,width=600');
							mywindow.document.write(responseText);

							setTimeout(function() {
								mywindow.print();
								mywindow.close();
							}, 400);
						}
					});
				}
						   
	           </script>
             
<style>
	#vehicleForm .error { color: red; }
</style>
           
           
