<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");
%>

<script>
function formSubmit()
	{	 
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=duplication_application_approval&applicationNo='+application,type:'POST',data: $("#learnerForm").serialize()}; 

	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}
function formReject()
     {
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=duplication_application_reject&applicationNo='+application,type:'POST',data: $("#learnerForm").serialize()}; 
		
		$("#vehicleForm").ajaxSubmit(options);
		$('#displayMsgDiv').show();
		setTimeout('hideStatus("displayMsgDiv")',4000);
		setTimeout('showTaskList()',2000);
     }	

function dispatch()
{
	var application=$("#applicationNo").val();
	var options = {target:'#displayMsgDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=VEHICLE&serviceType=DUPLICATION',type:'POST',data: $("#vehicleForm").serialize()}; 
    $("#vehicleForm").ajaxSubmit(options);
    $('#displayMsgDiv').show();
    setTimeout('hideStatus("displayMsgDiv")',4000);
    setTimeout('showTaskList()',2000);
}
		
		var a="<%=request.getAttribute("applicationNo")%>";
		$("#applicationNo").val(a);

</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Replacement
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			     Replacement of Vehicle Registration Certificate
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	
	 <div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Owner's Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
						<div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Vehicle Number:</label>
						    </div>	
                         	<div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNumber() %></label></div> 
					</div>
				    <%
	                	if(dto.getVehicleRegistrationType().equals("Personal"))
	                	{
                	%>
						<div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Owner Type:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleRegistrationType() %></label></div> 
	                        <div class="col-lg-2">
								<label class="control-label">Owner ID :</label>
					        </div>	
	                       	<div class="col-lg-2"><label class="control-label"><%=dto.getCustomerId() %></label></div>
		                </div>
		                <div class="form-group">
		                    <div class="col-lg-2">
								<label class="control-label">Owner Name :</label>
					        </div>	
		                    <div class="col-lg-4"><label class="control-label"><%=dto.getName()%></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Citizen ID:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getCitizenID() %></label></div> 
		                 </div> 
                         <div class="form-group">
                           	<div class="col-lg-2">
								<label class="control-label">Phone:</label>
				        	</div>	
	                       	<div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div>
		                 </div>  
		                 <%
		                	if(dto.getIsInternational().equals("N"))
		                	{
		                %> 
		                <div class="form-group">
		                	 <div class="col-lg-2">
								 <label class="control-label">Dzongkhag:</label>
							 </div>	
                             <div class="col-lg-2"><label class="control-label"><%=dto.getDzongkhag() %></label></div> 
                           	 <div class="col-lg-2">
								<label class="control-label">Gewog:</label>
					         </div>	
		                     <div class="col-lg-2"><label class="control-label"><%=dto.getGewog() %></label></div>
	                    	 <div class="col-lg-2">
								<label class="control-label">Village:</label>
							 </div>	
                             <div class="col-lg-2"><label class="control-label"><%=dto.getVillage() %></label></div> 
		                  </div>   
		                  <%
		                	}
		                	else
		                	{
		                  %>  
		                   <div class="form-group">  
			                    <div class="col-lg-2">
									<label class="control-label">Country:</label>
						        </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getCountry() %></label></div>
			                    <div class="col-lg-2">
									 <label class="control-label">Address:</label>
								</div>	
	                            <div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div> 
                           </div> 
                             <%
		                		}
	                	}
	                	else
	                	{
                         %>  
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Owner Type</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getOwner() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Agency</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getMinistry() %></label></div>
					 		</div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Department/Name</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Address</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div>
					 		</div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Phone No</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Dzongkhag</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div>
					 		</div> 
                             
                        <%
	                	}
                        %>
                            </div>   
                          </div>   
                         <div class="widget-header widget-header-small">
				                 <h5 class="widget-title lighter">Vehicle Details</h5>
			              </div>
			              <div class="widget-body">
					        <div class="widget-main">
                              <div class="form-group">
                              <div class="col-lg-2">
								 <label class="control-label">Company:</label>
					          </div>	
		                      <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleCompany() %></label></div>
		                     <div class="col-lg-2">
								<label class="control-label">Model:</label>
							 </div>	
                             <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleModel() %></label></div> 
		                 </div>    
		                <div class="form-group">
                            <div class="col-lg-2">
								<label class="control-label">Color:</label>
					        </div>	
	                       	<div class="col-lg-4"><label class="control-label"><%=dto.getColour() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Chasis Number:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getChasisNumber() %></label></div> 
		                </div> 
	                  	<div class="form-group">
	                    	<div class="col-lg-2">
								<label class="control-label">Engine Number:</label>
				         	</div>	
	                        <div class="col-lg-4"><label class="control-label"><%=dto.getEngineNumber() %></label></div>
	                 		<div class="col-lg-2">
								<label class="control-label">Engine Type:</label>
							</div>	
                         	<div class="col-lg-4"><label class="control-label"><%=dto.getEngineType() %></label></div> 
             			</div>
          				<div class="form-group">
                     		<div class="col-lg-2">
								<label class="control-label">Seat Capacity:</label>
      						</div>	
                 			<div class="col-lg-4"><label class="control-label"><%=dto.getSeatCapacity() %></label></div>
               				<div class="col-lg-2">
								<label class="control-label">Load Capacity:</label>
							</div>	
                        	<div class="col-lg-4"><label class="control-label"><%=dto.getLoadCapacity() %></label></div> 
    					</div>
                        <div class="form-group ">
                       		<div class="col-lg-2">
								<label class="control-label">Last Issue Date:</label>
        					</div>	
                   			<div class="col-lg-4"><label class="control-label"><%=dto.getRegistrationDate() %></label></div>
                 			<div class="col-lg-2">
								<label class="control-label">Expiry Date:</label>
							</div>	
                          	<div class="col-lg-4"><label class="control-label"><%=dto.getExpiryDate() %></label></div> 
		                	</div>
		                </div>
		              </div> 
		                  <div class="widget-header widget-header-small">
							    <h5 class="widget-title lighter">Replacement Details</h5>
						     </div>
						       <div class="widget-body">
					            <div class="widget-main">
		                 
		                  <div class="form-group">
		                    <div class="col-lg-2">
								<label class="control-label">Receipt No:</label>
							</div>	
                               <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo() %></label></div> 
		                      <div class="col-lg-2">
								<label class="control-label">Receipt Date:</label>
					        </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptDate() %></label></div>
		                   </div>   
		                   <div class="form-group">
			                    <div class="col-lg-2">
									<label class="control-label">Replacement Amount:</label>
								</div>	
	                                <div class="col-lg-4"><label class="control-label"><%=dto.getAmount() %></label></div> 
			                      <div class="col-lg-2">
									<label class="control-label">Penalty :</label>
						        </div>	
			                        <div class="col-lg-4"><label class="control-label"><%=dto.getPenalty() %></label></div>
			               </div>   
		                   <div class="form-group">
                              <div class="col-lg-2">
								<label class="control-label">Remarks :</label>
					          </div>	
			                       <div class="col-lg-4"><label class="control-label"><%=dto.getRemarks() %></label></div>
	                          <div class="col-lg-2">
								<label class="control-label">Traffic Remarks :</label>
					          </div>	
	                       	<div class="col-lg-4"><label class="control-label"><%=dto.getTrafficRemarks() %></label></div>
                            </div>
                            <logic:equal value="APPROVE" name="param">
                             <div class="form-group">
								<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
								 <html:hidden property="vehicleId" value="<%=dto.getVehicleId() %>"></html:hidden>
							</div>
						    </logic:equal>
                       </div>
                  </div>
              </div>
        </html:form> 
	 </div>
<input type="hidden" id="applicationNo"/>
<div id="displayMsgDiv"></div>

<div>
	<logic:equal value="APPROVE" name="param">
		<button type="button" class="btn btn-sm" onclick="formSubmit()">
			<i class="ace-icon fa fa-check"  ></i>
			Approve
		</button>
		<button type="button" class="btn btn-sm" onclick="openModal('vehiclemodalform')">
			<i class="ace-icon fa fa-times red2"></i>
			Reject
		</button>
	</logic:equal>
	
	<logic:equal value="DISPATCH" name="param">
		<button type="button" class="btn btn-sm" onclick="dispatch()">
			<i class="ace-icon fa fa-check"  ></i>
			Dispatch
		</button>
	</logic:equal>
	
</div>

<div id="vehiclemodalform" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Reject Application</h4>
			</div>

			<div class="modal-body">
				Are you sure you want to Reject?
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" onclick="formReject()">
					<i class="ace-icon fa fa-check"></i>
					Yes
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					No
				</button>
			</div>
		</div>
	</div>
</div>

