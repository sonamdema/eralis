<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-credit-card"></i>
			Update Vehicle Information
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				 Update incomplete vehicle data
			</small>
		</h1>
	</div>
	
	<form>
	
		<div class="row">
			<div class="form-group">
		 		<div class="col-lg-2">
					<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
		 		</div>
				<div class="col-lg-3">
					<div class="input-group">
							<input type="text" id="vehicleNumber" class="form-control" readonly="readonly"/>
					  		<span class="input-group-btn">
				      	 		<a href="#" onclick="openModal('vehiclesearch')" class="btn btn-purple btn-sm">
					  				<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
					  		</span>
				 	</div>
			  	</div>
			  	<div class="col-lg-3">
			  		<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please enter a vehicle number</label>
			  	</div>
		    </div>
		</div>
	
		<div id="result"></div>
		   
	</form>
	
<div id="vehiclesearch" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Vehicle Details</h4>
			</div>
			 <div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                    	<form class="form-horizontal" role="form">
						   <div class="form-group">
						   		<label class="col-sm-4 control-label no-padding-right" for="Name"> Vehicle No : </label>
                            	<div class="col-sm-4">
                                 	<input type="text" id="vehicleNoModal" placeholder="Vehicle Number"  />
                            	</div>
						   </div>
						   <div class="form-group">
						   		<label class="col-sm-4 control-label no-padding-right" for="Name"> Vehicle Type : </label>
                            	<div class="col-sm-4">
                            		 <select class="col-sm-12 modal_input" id="vehicleTypeModal">
                                 		<option value="">SELECT</option>
										<logic:iterate id="vehicleType" name="vehicleTypeList">
											<option value='<bean:write name="vehicleType" property="headerId"/>'><bean:write name="vehicleType" property="headerName"/></option>
										</logic:iterate>
									 </select>
                            	</div>
						   </div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm" name="search" onclick="getVehicleDetails()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
		  	</div>
	    	<div id="personalListTable"></div>
    	</div>
 	</div>
</div><!-- PAGE CONTENT ENDS -->
	
	<script type="text/javascript">
		
		function getVehicleDetails()
		{
			var vehicleNo = $('#vehicleNoModal').val();
			var vehicleType = $('#vehicleTypeModal').val();
			var regionId = "<%=regionId%>";
			var regionName = "<%=regionName%>";
			var pageIdentifier = "<%=pageIdentifier%>";
			var pageId = "<%=pageId%>";

			if(vehicleNo == "")
			{
				$('#vehicleNumberValidation').show();
				$('#vehicleNumberValidation').get(0).scrollIntoView();
				return false;
			}
			else
			{
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/vehicle.html?method=get_vehicle_details&vehicleNo="+vehicleNo+"&regionId="+regionId+"&regionName="+regionName+"&pageIdentifier="+pageIdentifier+"&pageId="+pageId+"&vehicleType="+vehicleType,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#result").html(responseText);
						$("#result").show();
						$('#vehiclesearch').modal('hide');
					}
				});
			}
		}
		
</script>		