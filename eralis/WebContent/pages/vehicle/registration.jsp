<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/select2.min.css">
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
	    Registration
	     <small>
		   <i class="ace-icon fa fa-angle-double-right"></i>
		     Registration of Vehicle
	     </small>
	</h1>
</div><!-- /.page-header -->
      <!--DIV STARTS--> 
  <div class="row">
  	<div id="msgDiv">
    </div>
   <html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	 <div class="widget-box">
	 	<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner Type</h5>
		 </div>
	   <div class="widget-body">
		<div class="widget-main">
	       <div class="row">
		    <div class="col-lg-12">
		       <jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
		      <div class="form-group ">
				 <div class="col-lg-3 control-label">
					<label >
						<html:radio property="personalRadio" styleClass="ace" styleId="personRadio" value="p" onclick="showPersonal()"></html:radio>	
						<span class="lbl"> Personal</span>
					</label>
				  </div> 
				  <div class="col-lg-2 control-label" style="text-align: left; "> 	 
					  <label>
							<html:radio property="personalRadio" styleClass="ace" styleId="organisationRadio" value="o" onclick="showOrganisation()"></html:radio>	
							<span class="lbl"> Organisation</span>
					  </label>
				   </div> 
				   <label style="display:none;color: #ff0000" id="radioValidation">Please select at least one option</label>
			  	</div> 
			  	 
		    </div>  
	      </div> 
         <div class="row">
				<div class="col-lg-12">
					<div class="form-group"  style="display:none" id="personal1" > 
					   <div class="col-lg-2">
						 <label>Citizen ID :<span style="color: #ff0000">*</span></label>
                             </div>            
                       
                               <div class="col-lg-3">
				             	<div class="input-group">
                                     <html:text property="citizenID" styleClass="form-control" styleId="citizenID" readonly="true"></html:text>
                                       <span class="input-group-btn">
							      	    <a href="#"  onclick="openModal('personalModal')" class="btn btn-purple btn-sm">
										<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
										</a>
									</span>
					        	</div>
					      </div>
					      <div class="col-lg-3">
					      	<label style="display:none;color: #ff0000" id="citizenIDValidation">Select a personal information</label>
					      </div>
					 </div>
			<div class="form-group"  style="display:none" id="organization1" > 
			   <div class="col-lg-2">
				   <label>Owner ID :<span style="color: #ff0000">*</span></label>
                 </div>
				   <div class="col-lg-3">
			             <div class="input-group">   
                                    <html:text property="ownerID" styleClass="form-control" readonly="true" styleId="ownerID" ></html:text>
                                            <span class="input-group-btn">
						      	  <a href="#" onclick="openModal('organizationModal')" class="btn btn-purple btn-sm">
									<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
								</span> 
					        </div>
					      </div> 
					      <div class="col-lg-3">
					      	<label style="display:none;color: #ff0000" id="ownerIDValidation">Select a organization information</label>
					      </div>    
			             </div>
			           </div>
			           <div class="alert alert-danger col-lg-12" id="message" style="display:none"> </div>
		    	      </div>  
		            </div>
		          </div>
		         </div>
				 <div class="widget-box"  id="personal" style="display:none">
						<div class="widget-header widget-header-small">
							<h5 class="widget-title lighter">Personal Information</h5>
						 </div>
						    <div class="widget-body">
							   <div class="widget-main">
			
			                               <div class="form-group">
			                               	  <div class="col-lg-2">
                                              	<label class="control-label" for="Owner Name"> Owner Name : </label>
                                              </div>
                                              <div class="col-lg-4" id="displayOwnerName"></div>
                                              <div class="col-lg-2">
                                              	<label class="control-label" for="Dzongkhag"> Dzongkhag : </label>
                                              </div>
                                              <div class="col-lg-4" id="displayDzongkhag"></div>
                                          </div>
								          <div class="form-group">
								          	<div class="col-lg-2">
						                       <label class="control-label" for="Gewog"> Gewog : </label>
						                    </div>
						                    <div class="col-lg-4" id="displayGewog"></div>
						                    <div class="col-lg-2">
						                    	<label class="control-label" for="Address"> Address : </label>
						                    </div>
						                    <div class="col-lg-4" id="displayAddress"></div>
						                  </div>	
			                          </div>	
			                      </div>	
			                  </div>	
			               <div class="space-6"></div>
					          <div class="widget-box" id="organization" style="display:none">
						        <div class="widget-header widget-header-small">
							       <h5 class="widget-title lighter">Organisation Information</h5>
						        </div>
						          <div class="widget-body">
							        <div class="widget-main">
			  
			                               <div class="form-group">
			                               	  <div class="col-lg-2">
                                              	<label class="control-label" for="Owner Name"> Owner Name : </label>
                                              </div>
                                              <div class="col-lg-4" id="organisationOwnerName"></div>
                                              <div class="col-lg-2">
                                              	<label class="control-label" for="Dzongkhag"> Dzongkhag : </label>
                                              </div>
                                              <div class="col-lg-4" id="organisationDzongkhag"></div>
                                          </div>
									
								          <div class="form-group">
								          	<div class="col-lg-2">
						                       <label class="control-label" for="Address"> Address : </label>
						                    </div>
						                    <div class="col-lg-4" id="organisationAddress"></div>
						                  </div>	
								     </div>	
								</div>	
							</div>	
		    <div class="widget-box">
			 	<div class="widget-header widget-header-small">
				  <h5 class="widget-title lighter">Registration Details</h5>
				</div>						
		      <div class="widget-body">
			    <div class="widget-main">
						<div class="form-group ">
							<div class="col-lg-2">
								<label>Registration Type<span style="color: #ff0000">*</span>:</label>
							</div>	
                              <div class="col-lg-3">
							       <html:select property="registrationType" styleClass="form-control" styleId="registrationType" >
								      <html:option value="">--SELECT--</html:option>
						   		      <html:optionsCollection name="registrationTypeList" label="headerName" value="headerId"/>
						          </html:select>
                              </div> 
                               <div class="col-lg-2">
								  <label>Status <span style="color: #ff0000">*</span>:</label>
					           </div>	
		                       <div class="col-sm-3"> 	
		                      	<input type="text" value="Active" class="form-control"  disabled="disabled">
								<html:hidden property="status"  value="1"></html:hidden>
							  </div>
		                 </div>
		                 <div class="form-group " style='display:none' id="quotaDiv">
	                            <div class="col-lg-2">
								   <label>Quota Holder Moblie Number<span style="color: #ff0000">*</span>:</label>
								</div>
								 <div class="col-lg-3">
	                            	<html:text property="quotaMobileNumber"  styleClass="form-control" styleId="quotaMobileNumber" value="0"></html:text>
	                            </div>
	                            <div class="col-lg-2">
								   <label>Vehicle Owner Moblie Number<span style="color: #ff0000">*</span>:</label>
								</div>
								 <div class="col-lg-3">
	                            	<html:text property="ownerMobileNumber"  styleClass="form-control" styleId="ownerMobileNumber" value="0"></html:text>
	                            </div>		
                            </div>
		                 
		                <div class="form-group">
							<div class="col-lg-2">
								<label>Vehicle Rgn. Code<span style="color: #ff0000">*</span>:</label>
							</div>	
	                       	<div class="col-lg-3">
	                               <html:select property="vehicleRegistrationId" styleClass="form-control" onchange="changeAttribute(this.value,'vehicleRegionId')" styleId="vehicleRegistrationId">
										<html:option value="">--SELECT--</html:option>
								   		<html:optionsCollection name="vehicleCodeList" label="headerName" value="headerId"/>
								   </html:select>
                            </div>
		                    <div class="col-lg-2">
								<label>Region<span style="color: #ff0000">*</span>:</label>
					        </div>	
		                    <div class="col-lg-3">
								   <html:hidden property="region" styleId="vehicleRegionId" value="<%=regionId %>"></html:hidden>
								   <input type="text" disabled="disabled" id="regionName" class="form-control" value="<%=regionName %>"/>
                           </div>
		                </div>
		               	<div class="form-group" id="diplomatDiv" style="display:none;">
							 <div class="col-lg-2">
								<label>Diplomat<span style="color: #ff0000">*</span>:</label>
							</div>	
	                       	<div class="col-lg-3">
	                               <html:select property="diplomatCode" styleClass="form-control"  styleId="diplomatCode">
										<html:option value="0">--SELECT--</html:option>
								   		<html:optionsCollection name="diplomatList" label="headerName" value="headerId"/>
								   </html:select>
                            </div> 
		                </div>
		                <div class="form-group ">
							<div class="col-lg-2">
							   <label>Vehicle Type<span style="color: #ff0000">*</span>:</label>
							</div>	
			                <div class="col-lg-3">
				            	<html:select property="vehicleType" styleClass="form-control" styleId="vehicleType" onchange="onChangeVehicleType(),populateRenewalDurationDropdown()">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
								</html:select>
                            </div> 
                            <div style='display:none' id="busTypeDiv">
	                            <div class="col-lg-2">
								   <label>Bus Type<span style="color: #ff0000">*</span>:</label>
								</div>	
				                <div class="col-lg-3">
					            	<html:select property="busType" styleId="busType" styleClass="form-control">
										<html:option value="0">--SELECT--</html:option>
										<html:optionsCollection name="busTypeList" label="headerName" value="headerId"/>
									</html:select>
	                            </div> 
                            </div>
                         </div>
                         
                         <div class="form-group " style='display:none' id="wheelDiv">
	                            <div class="col-lg-2">
								   <label>Number Of Wheels<span style="color: #ff0000">*</span>:</label>
								</div>
								 <div class="col-lg-3">
	                            	<html:text property="wheelNos"  styleClass="form-control" styleId="wheelNos" value="0"></html:text>
	                            </div>
	                            <div class="col-lg-2">
								   <label>Number Of Axle<span style="color: #ff0000">*</span>:</label>
								</div>
								 <div class="col-lg-3">
	                            	<html:text property="numberAxle"  styleClass="form-control" styleId="numberAxle" value="0"></html:text>
	                            </div>		
                            </div>
                            
                            <div class="form-group">
	                    		<div class="col-lg-2">
								    <label>Purchase Type:</label>
					          	</div>	
		                        <div class="col-sm-3"> 	
							       <html:select property="purchaseType" styleClass="form-control" styleId="purchaseType">
								   <html:option value="">SELECT</html:option>
								   <html:option value="1">Normal</html:option>
	                                  <html:option value="2">Imported</html:option>
								   </html:select>
							    </div>
							    <div class="col-lg-2">
                                   <label>Purchase Date<span style="color: #ff0000">*</span>:</label>
	                            </div>	
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="purchaseDate" styleClass="form-control date-picker" styleId="purchaseDate"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>	
	                    	</div>
		                 
		                <div class="form-group">
		                  <div class="col-lg-2">
								<label>Vehicle Company<span style="color: #ff0000">*</span>:</label>
							</div>	
		                       <div class="col-lg-3">
			                       <html:select property="vehicleCompany" styleClass="form-control" styleId="vehicleCompany" onchange="populateDependentDropDown(this.value, 'vehicleModel', '', 'VECHILE_MODEL_LIST', 'N')">
											<html:option value="">SELECT--</html:option>
									   		<html:optionsCollection name="vehicleCompanyList" label="headerName" value="headerId"/>
								 </html:select>
                              </div> 
		                     <div class="col-lg-2">
								<label>Vehicle Model<span style="color: #ff0000">*</span>:</label>
							 </div>	
		                       <div class="col-lg-3">
			                       <html:select property="vehicleModel" styleClass="form-control" styleId="vehicleModel" onchange="onChangeModel(this.value)">
											<html:option value="">SELECT--</html:option>
								 </html:select>
                               </div> 
		                  </div>
		                <div class="form-group">
		                	<div class="col-lg-2">
								<label>Engine Type<span style="color: #ff0000">*</span>:</label>
							</div>	
                            <div class="col-lg-3">
			                	<html:select property="engineType" styleClass="form-control" styleId="engineType" onchange="onChangeVehicleType()">
									<html:option value="">SELECT--</html:option>
							   		<html:optionsCollection name="engineTypeList" label="headerName" value="headerId"/>
							   	</html:select>
                            </div> 
                            <div style="display:none;">
			                	<div class="col-lg-2" >
									<label>Vanity Number<span style="color: #ff0000">*</span>:</label>
						        </div>	
		                        <div class="col-lg-3">
	                            	<html:text property="vanityNumber"  styleClass="form-control" styleId="vanityNumber"></html:text>
	                            </div>
	                    	</div>
	                    	<div class="col-lg-2">
								<label>Engine/Motor No<span style="color: #ff0000">*</span>:</label>
					     	</div>	
                            <div class="col-sm-3"> 	
								<html:text property="engineNumber" styleClass="form-control" onchange="validateChasisEngineNo()" styleId="engineNumber"></html:text>
								<div class="error engine-chassis-error engineNo-message hidden"></div> 
						   	</div>
		             	</div>
		             	<script>
		             		var chasisEningNoValid = 0;
		             		function validateChasisEngineNo()
		             		{
		             			var engineNumber = $("#engineNumber").val();
		             			var chasisNumber = $("#chasisNumber").val();
		             			$(".engine-chassis-error").addClass("hidden");
		             			$.ajax
		             			({
		             				async: false,
		             				type: 'POST',
		             				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=validateChasisEngineNo&engineNumber='+engineNumber+'&chasisNumber='+chasisNumber,
		             				success: function(xml)
		             				{
		             					$(xml).find('xml-response').each(function()
		             					{
		             						engineNoStatus = $(this).find('engineNoStatus').text();
		             						chasisStatus = $(this).find('chasisStatus').text();
											if(engineNoStatus=="ALREADY_EXIST" && engineNumber!="")
											{
												chasisEningNoValid = 1;
												$(".engineNo-message").removeClass("hidden");
												$(".engineNo-message").text("Engine No. already existed");
											}
											if(chasisStatus=="ALREADY_EXIST" && chasisNumber!="")
											{
												chasisEningNoValid = 1;
												$(".chasis-message").removeClass("hidden");
												$(".chasis-message").text("Chasis already existed");
											}
											if(engineNoStatus!="ALREADY_EXIST" && chasisStatus!="ALREADY_EXIST")
											{
												chasisEningNoValid = 0;
											}
		             					});
		             				}
		             			});
		             		}
		             	</script>
		                <div class="form-group">
		             		<div class="col-lg-2">
								<label>Chasis Number<span style="color: #ff0000">*</span>:</label>
							</div>	
                           	<div class="col-sm-3"> 	
								<html:text property="chasisNumber" styleClass="form-control" onchange="validateChasisEngineNo()" styleId="chasisNumber"></html:text>
								<div class="error engine-chassis-error chasis-message hidden"></div>  
							</div>
							<div class="col-lg-2">
								<label>Dealers Name<span style="color: #ff0000">*</span>:</label>
				           	</div>	
		                  	<div class="col-sm-3"> 	
						       <html:select property="dealersName" styleClass="form-control" styleId="dealersName">
							      <html:option value="">SELECT--</html:option>
					   		      <html:optionsCollection name="dealersNameList" label="headerName" value="headerId"/>
					          </html:select>
							</div>
		          		</div>
		            	<div class="form-group">
		            		<div id="loadCapacityDiv" style="display: none;">
			                	<div class="col-lg-2">
									<label>Gross Vehicle Weight(KG)<span style="color: #ff0000">*</span>:</label>
								</div>	
		                        <div class="col-sm-3"> 	
									<html:text property="loadCapacity" styleClass="form-control" readonly="true" styleId="loadCapacity"></html:text> 
								</div>
							</div>
		                	<div id="vehicleHorsePowerDiv" style="display: none;">
			                	<div class="col-lg-2">
									<label>Horse Power<span style="color: #ff0000">*</span>:</label>
								</div>	
		                        <div class="col-sm-3"> 	
									<html:text property="vehicleHorsePower" styleClass="form-control" readonly="true" styleId="vehicleHorsePower"></html:text> 
								</div>
							</div>
							<div id="vehicleKiloWattDiv" style="display: none;">
		                      	<div class="col-lg-2">
							 		<label>KiloWatt<span style="color: #ff0000">*</span>:</label>
						      	</div>	
	                            <div class="col-sm-3"> 	
									<html:text property="vehicleKiloWatt" styleClass="form-control" readonly="true" styleId="vehicleKiloWatt"></html:text> 
								</div>
							</div>
							<div id="engineCCDiv" style="display: none;">
		               			<div class="col-lg-2" >
								     <label>Engine CC<span style="color: #ff0000">*</span>:</label>
							    </div>	
	                            <div class="col-sm-3"> 	
									<html:text property="engineCC" styleClass="form-control" readonly="true" styleId="engineCC"></html:text> 
								</div>
							</div>
							<div id="unladenWeightDiv" style="display: none;">
			                   	<div class="col-lg-2">
									 <label>Unladen Weight<span style="color: #ff0000">*</span>:</label>
							  	</div>	
	                              	<div class="col-sm-3"> 	
							     	<html:text property="unladenWeight" styleClass="form-control" styleId="unladenWeight"></html:text> 
						     	</div>
						     </div>
	                   	</div>
		                  <div class="form-group">
		                  	   <div class="col-lg-2">
							 		<label> Seat Capacity<span style="color: #ff0000">*</span>:</label>
							   	</div>	
		                       	<div class="col-sm-3"> 	
									<html:text property="seatCapacity" styleClass="form-control" readonly="true" styleId="seatCapacity"></html:text> 
								</div>
		                       <div class="col-lg-2">
								   <label>Colour<span style="color: #ff0000">*</span>:</label>
							   </div>
							    <div class="col-lg-3">
				            	<html:select property="colour" styleClass="form-control" styleId="colour">
									<html:option value="">SELECT--</html:option>
									<html:optionsCollection name="colourList" label="headerName" value="headerId"/>
								</html:select>
                            </div> 	
<!--                                <div class="col-sm-3"> 	-->
<!--								   <html:text property="colour" styleClass="form-control" styleId="colour"></html:text> -->
<!--							    </div>-->
		                   </div>
		                  <div class="form-group">
		                  	<div class="col-lg-2">
								<label>Price<span style="color: #ff0000">*</span>:</label>
					        </div>	
	                        <div class="col-sm-3"> 	
							  <html:text property="price" styleClass="form-control" styleId="price"></html:text> 
						    </div>
		                    <div class="col-lg-2">
								<label>Manufacture Year<span style="color: #ff0000">*</span>:</label>
						    </div>	
                            <div class="col-sm-3"> 	
								<html:text property="manufactureYear" styleClass="form-control" styleId="manufactureYear"></html:text> 
						    </div>
		                  </div>
		                <div class="form-group">
		                	<div class="col-lg-2">
								<label>Manufacture Country<span style="color: #ff0000">*</span>:</label>
					        </div>	
	                        <div class="col-sm-3"> 	
							  <html:select property="manufactureCountry" styleClass="form-control" styleId="manufactureCountry">
							      <html:option value="">SELECT--</html:option>
					   		      <html:optionsCollection name="manufactureCountryList" label="headerName" value="headerId"/>
					          </html:select>
						    </div>
						    <div class="col-lg-2">
							    <label>Validity Duration<span style="color: #ff0000">*</span>:</label>
				          	</div>	
	                        <div class="col-sm-3"> 	
						       <html:select property="renewalDuration" styleClass="form-control" styleId="renewalDuration">
							   		<html:option value="">---SELECT-----</html:option>
							   </html:select>
						    </div>
                    	</div>
                    	
                 </div>
                </div>
            </div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Attachments</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label> Invoice :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="invoice" styleId="invoice" styleClass="form-control fileupload" onchange="validation('vehicleForm','invoiceTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
								</div>
								<div class="col-lg-2">
									<label> Challan :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="challan" styleId="challan" styleClass="form-control fileupload" onchange="validation('vehicleForm','challanTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="challanValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="challanTypeValidation">Please attach your file here</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label> Letter of Authenticity :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="letterOfAuthenticity" styleId="letterOfAuthenticity" styleClass="form-control fileupload" onchange="validation('vehicleForm','letterTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="letterValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="letterTypeValidation"></label>
								</div>
								<div class="col-lg-2">
									<label> Emission :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="emission" styleId="emission" styleClass="form-control fileupload" onchange="validation('vehicleForm','emissionTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="emissionValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="emissionTypeValidation">Please attach your file here</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label> Exemption Certificate :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="exemptionCertificate" styleId="exemptionCertificate" styleClass="form-control fileupload" onchange="validation('vehicleForm','exemptionTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="exemptionValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="exemptionTypeValidation">Please attach your file here</label>
								</div>
								<div class="col-lg-2">
									<label> Excise Invoice :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="exciseInvoice" styleId="exciseInvoice" styleClass="form-control fileupload" onchange="validation('vehicleForm','exciseTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="exciseValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="exciseTypeValidation">Please attach your file here</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label> Custom Declaration :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="customDeclaration" styleId="customDeclaration" styleClass="form-control fileupload" onchange="validation('vehicleForm','customTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="customValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="customTypeValidation">Please attach your file here</label>
								</div>
								<div class="col-lg-2">
									<label> Vehicle Picture :</label>
								</div>
								<div class="col-lg-3">
									<html:file property="vehiclePicture" styleId="vehiclePicture" styleClass="form-control fileupload" onchange="validation('vehicleForm','vehiclePicTypeValidation',this)"></html:file>
									<label style="display:none;color: #ff0000" id="vehiclePicValidation">Please attach your file here</label>
									<label style="display:none;color: #ff0000" id="vehiclePicTypeValidation">Please attach your file here</label>
								</div>
							</div>
						  </div>
					   </div>
				     </div>
		         </div>
	         </div>
			<div id="registrationMsgDiv"></div>
			<div class="pull-left">
				   <html:hidden property="customerId" styleId="customerID"></html:hidden>
				   <html:hidden property="vehicleRegistrationType" styleId="vehicleRegistrationType"></html:hidden>
				   <html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
				   <logic:equal value="Y" name="priviledge" property="isNew">
				   <button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
				</logic:equal>
       	   </div>
	</html:form>
</div>
  <div id="personalModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			 <div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CID"> Citizen ID : </label>
                            	<div class="col-sm-4">
                                     <input type="text" id="cidPersonalModal" placeholder="CID"  />
                                </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CustomerID"> Customer ID : </label>
                                <div class="col-sm-4">
                                     <input type="text" id="customerIdPersonalModal" placeholder="CustomerID"  />
                                </div>
							</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal-footer">
				<button type="button" class="btn btn-sm" name="search" onclick="searchPersonalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
		  </div>
	    <div id="personalListTable">
	  </div>
    </div>
 </div>
</div><!-- PAGE CONTENT ENDS -->
<div id="organizationModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			 <div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form" id="reset_form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Code"> Code : </label>
                            <div class="col-sm-4">
                                 <input type="text" id="codeOrganisationModal" placeholder="Code"  />
                            </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Name"> Name : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="nameOrganisationModal" placeholder="Name"  />
                                 </div>
						</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm" name="search" onclick="searchOrganisationInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			 </div>
			<div id="organisationListTable">
		   </div>
	  </div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/select2.full.min.js"></script>
<script type="text/javascript">


$(function () {
   $('select').select2();
   $( "select" ).removeClass( "form-control" );
   $('.select2-container').width(70 + '%');
   $('.select2-choice').height(32);
 //  $('.select2-choice').paddingTop(4);
});
		
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});

	var fileError;
	var context = "<%=request.getContextPath()%>";
	var engineCC = "";
	var seatCapacity = "";
	var unladenWeight = "";
	
	$('.fileupload').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:false,
		onchange:null,
		thumbnail:false,
		whitelist:'gif|png|jpg|jpeg',
		blacklist:'exe|php'
	});

	function onChangeRegistrationType()
	{
		var registrationType = $("#registrationType option:selected").text();
		$("#quotaDiv").hide();
		if(registrationType=="Quota")
		{
			$("#quotaDiv").show();
		}
		
	}
	
	function showPersonal()
	{
		$("#organization").hide(); 
	    $("#personal").show(); 
		$("#organization1").hide(); 
	    $("#personal1").show(); 
	    $('#ownerID').val("NA");
	    $('#citizenID').val("");
	}
	function showOrganisation()
	{
		$("#organization").show(); 
	    $("#personal").hide(); 
		$("#organization1").show(); 
	    $("#personal1").hide(); 
	    $('#citizenID').val("NA");
	    $('#ownerID').val("");
	}

	$("#vehicleRegionId").attr("disabled", true);
	function changeAttribute(regCode,elementId)
	{

		if(regCode>0){
			$("#"+elementId).attr("disabled", false);
			}else{
				$("#"+elementId).attr("disabled", true);
			}
		
		$("#diplomatDiv").hide();
		var vehicleRegistrationId = $("#vehicleRegistrationId :selected").text();
		if(vehicleRegistrationId == 'BT')
		{

			$('#vehicleType').children('option').hide();
			$('#vehicleType option:contains(Taxi)').show();
			//$('#vehicleType').find('option :contains(Taxi)').hide();
		}
		else if(vehicleRegistrationId == 'CD')
		{
			$("#diplomatDiv").show();
			$('#vehicleType').children('option').show();
			$('#vehicleType option:contains(Taxi)').hide();
		}
		else
		{
			$('#vehicleType').children('option').show();
			$('#vehicleType option:contains(Taxi)').hide();
		}
		
		
	}
	
	//$("#vehicleType").attr("disabled", true);
	/* function changeAttribute(vehicleType,elementId)
	{
		if(vehicleType>0){
			$("#"+elementId).attr("disabled", false);
			}else{
				$("#"+elementId).attr("disabled", true);
			}
	} */

	function searchPersonalInfo()
	{
		var cidNumber = $('#cidPersonalModal').val();
		var customerId = $('#customerIdPersonalModal').val();
		if(cidNumber == "")
			cidNumber = "NA";
		if(customerId == "")
			customerId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList&cid="+cidNumber+"&customerId="+customerId+"&searchType=VEHICLE_REGISTRATION",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#personalListTable").html(responseText);
				$("#personalListTable").show();
			}
		});
	}

	function searchOrganisationInfo()
	{  
		var code = $('#codeOrganisationModal').val();
		var name = $('#nameOrganisationModal').val();
		var type = $('#typeOrganisationModal').val();
		var regionId = $('#regionOrganisationModal').val();
		var dzongkhagId = $('#dzongkhagOrganisationModal').val();	
		
		if(code == "")
			code = "NA";
		if(name == "")
			name = "NA";
		if(type == "")
			type = "NA";
		if(regionId == "")
			regionId = "NA";
		if(dzongkhagId == "")
			dzongkhagId = "NA";
	
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getOrganisationInfoList&code="+code+"&name="+name+"&type="+type+"&regionId="+regionId+"&dzongkhagId="+dzongkhagId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#organisationListTable").html(responseText);
				$("#organisationListTable").show();
			}
		});
	  }

	$(document).ready(function()
	{
		$('#personalRadio').attr('checked', true);
		$('#personal').show();
		$('#personal1').show();
	     
	 	$("#vehicleForm").validate({
		 invalidHandler: function(form, validator) {
	         var errors = validator.numberOfInvalids();
	         if (errors) {                    
	             var firstInvalidElement = $(validator.errorList[0].element);
	             $('html,body').scrollTop(firstInvalidElement.offset().top);
	             firstInvalidElement.focus();
	         }
	     	},
			rules:
			{	
				citizenID:{
					required:true
					},
				ownerID:{
					required:true
					},
				vehicleType:{
					required:true
					},
					registrationType:{
						required:true
						},
				amount:{
					required:true,
					number:true
					},
				vehicleRegistrationId:{
					required:true
						},
				vehicleRegionId:{
				    required:true
					},
				vehicleType:{
					required:true
					},
				vehiclePrefix:{
					required:true
					},
				vanityNumber:{
					required:true,
					number:true
						},
				vehicleCompany:{
				    required:true
					},
				vehicleModel:{
					required:true
					},
				engineType:{
					required:true
						},
				receiptDate:{
				    required:true
				},
				engineNumber:{
					required:true
				},
				receiptNo:{
				    required:true
				},
				status:{
				    required:true
				},
				colour:{
					required:true
					},
				price:{
					required:true,
					number: true
					},
				dealersName:{
				    required:true
					},
				seatCapacity:{
					required:true
						}, 
				unladenWeight:{
				    required:true,
				    number:true
					},
				manufactureYear:{
					required:true,
					number:true,
					maxlength:4,
					minlength:4
					},
				manufactureCountry:{
					    required:true
						},
				purchaseDate:{
				    required:true
							},
				chasisNumber:{
				    required:true
							},
				engineCC:{
				    required:true,
				    number: true
							},
				seatCapacity: {
					required:true,
					number:true
				},
				vehicleHorsePower:{
					required:true,
					number:true
				},
				vehicleKiloWatt:{
					required:true,
					number:true
				},
				loadCapacity:{
					required:true,
					number:true
				},
				renewalDuration:{
					required:true,
				},
			},    
			messages:
			{
				citizenID:{required: "Please search personal information"},
				ownerID:{required: "Please search organization information"},
				vehicleType:{required: "Please Enter Vehilce Type"},
				chasisNumber:{required: "Please Enter Chasis Number"},
				purchaseDate:{required: "Please Enter Purchase Date"},
				registrationType:{required: "Please Select a Registration Type"},
				amount:{required: "Please Provide a Amount"},
				vehicleRegistrationId:{required: "Please Select a Vehicle Code"},
				vehicleRegionId:{required: "Please Select a Region"},
				vehicleType:{required: "Please Select a Vehicle Type"},
				vehiclePrefix:{required: "Please Provide Vehicle Prefix"},
				vanityNumber:{required: "Please Provide a Vanity Number"},
				vehicleCompany:{required: "Please Select Vehicle Company"},
				vehicleModel:{required: "Please Select a Vehicle Model"},
				engineType:{required: "Please Select a Engine Type"},
				receiptDate:{required: "Please Select Date"},
				engineNumber:{required: "Please Provide Engine Number"},
				receiptNo:{required: "Please Provide Receipt Number"},
				status:{required: "Please Select a Status"},
				colour:{required: "Please Provide a Colour"},
				price:{required: "Please Provide a Price"},
				dealersName:{required: "Please Select a Dealers Name"},
				seatCapacity:{required: "Please provide seat capacity"},
				unladenWeight:{required: "Please provide unladen weight"},
				manufactureYear:{required: "Please provide manufacture year"},
				manufactureCountry:{required: "Please select a manufacture country"},
				engineCC:{required: "Please enter engine CC"},
				seatCapacity:{required: "Please enter seating capacity"},
				vehicleHorsePower:{required: "Please enter horse power"},
				vehicleKiloWatt:{required: "Please enter kilowatt"},
				loadCapacity:{required: "Please enter gross vehicle weight"},
				renewalDuration:{required: "Please select validity duration"},
			}
		});
	
		$('#submitBtn').click(function()
		{
			if($('#vehicleForm').valid() && chasisEningNoValid==0) 
			{
				var result = true;//validateAttachments();
		
				if(result)
				{
					requestType = "VEHICLE";
					serviceType = "REGISTRATION";
					var identityNo = "";
					if( $("#engineType").val() == "Electric")
						identityTypeId = '0'; 
					else
						identityTypeId = $('#vehicleType').val();
					var loadingCapacity = $('#loadCapacity').val();
					var seatingCapacity = $('#seatCapacity').val();
					var vehicleHP = $('#vehicleHorsePower').val(); 
					var kilowatts = $('#vehicleKiloWatt').val();
					var engineCC = $('#engineCC').val();
					var purchaseDate = $('#purchaseDate').val();
					var saleDeedAmount = "";
					var saleDeedDate = "";
					var renewalDuration = $('#renewalDuration').val();
					var vehicleRegistrationCode = $('#vehicleRegistrationId').val();
		
					getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration, vehicleRegistrationCode);
				}
				else
				{
					return false;
				}
			}
			else 
			{
				return false;
			}
		});
	});
	
	function validation(thisform,msgId,fileObj)
	{
		var fileId = fileObj.id;
		with(thisform)
		{
			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","JPG","PDF","JPEG","GIF","PNG")) == false)
			{
				document.getElementById(fileId).value = "";
				return false;
			}
			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
			{
				document.getElementById(fileId).value = "";
				return false;
			}
		}
	}

	function validateAttachments()
	{
		if($('#invoice').val() == "")
		{
			$('#invoiceValidation').show();
			$('#invoiceValidation').get(0).scrollIntoView();
			return false;
		}
		if($('#challan').val() == "")
		{
			$('#challanValidation').show();
			$('#challanValidation').get(0).scrollIntoView();
			return false;
		}	
		if($('#letterOfAuthenticity').val() == "")
		{
			$('#letterValidation').show();
			$('#letterValidation').get(0).scrollIntoView();
			return false;
		}	
		if($('#emission').val() == "")
		{
			$('#emissionValidation').show();
			$('#emissionValidation').get(0).scrollIntoView();
			return false;
		}	
		if($('#exemptionCertificate').val() == "")
		{
			$('#exemptionValidation').show();
			$('#exemptionValidation').get(0).scrollIntoView();
			return false;
		}
		if($('#exciseInvoice').val() == "")
		{
			$('#exciseValidation').show();
			$('#exciseValidation').get(0).scrollIntoView();
			return false;
		}
		if($('#customDeclaration').val() == "")
		{
			$('#customValidation').show();
			$('#customValidation').get(0).scrollIntoView();
			return false;
		}
		if($('#vehiclePicture').val() == "")
		{
			$('#vehiclePicValidation').show();
			$('#vehiclePicValidation').get(0).scrollIntoView();
			return false;
		}
		else
			return true;
	}

	function formSubmit()
	{
		$.blockUI
    	({ 
    		css: 
    		{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
    		} 
    	});
		var options = {target:'#registrationMsgDiv',url:context+'/vehicle.html?method=new_registration',type:'POST',data: $("#vehicleForm").serialize()}; 
		$("#vehicleForm").ajaxSubmit(options);
		$('#registrationMsgDiv').show();
		setTimeout('hideStatus("registrationMsgDiv")',10000);
	    setTimeout($.unblockUI, 1000);
	}

	function engineTypeOnChange()
	{	
		var engineType	=	$('#engineType option:selected').text();
		var vehicleType	=	$('#vehicleType option:selected').text();
		
		
		if(vehicleType=='Tractor')
		{
			if(engineType=='Electric' || engineType=='Hybrid'){
				 $("#engineCC").prop("readonly", true);
				 $("#vehicleHorsePower").prop("readonly", false);
				 $("#vehicleKiloWatt").prop("readonly", false);
			}
			else{
				 $("#engineCC").prop("readonly", false);
				 $("#vehicleHorsePower").prop("readonly", false);
				 $("#vehicleKiloWatt").prop("readonly", true);
			}
		}
		else
		{
			if(engineType=='Electric' || engineType=='Hybrid'){
				 $("#engineCC").prop("readonly", true);
				 $("#vehicleHorsePower").prop("readonly", true);
				 $("#vehicleKiloWatt").prop("readonly", false);
			}
			else{
			 $("#engineCC").prop("readonly", false);
			 $("#vehicleHorsePower").prop("readonly", true);
			 $("#vehicleKiloWatt").prop("readonly", true);
			}
		}
	}
	
	function onChangeVehicleType()
	{
		var vehicleType	=	$("#vehicleType :selected").text();
		var engineType	=	$("#engineType :selected").text();
		$("#wheelDiv").hide();
		$('#loadCapacity').val('0');
		$('#loadCapacityDiv').hide();
		$('#vehicleHorsePower').val('0');
		$('#vehicleHorsePowerDiv').hide();
		$('#vehicleKiloWatt').val('0');
		$('#vehicleKiloWattDiv').hide();
		$('#engineCC').val('0');
		$('#engineCCDiv').hide();
		$('#unladenWeight').val(unladenWeight);
		$('#unladenWeightDiv').hide();
		if(vehicleType=='Heavy Vehicle' || vehicleType=='Medium Vehicle')
		{
			if(vehicleType=='Heavy Vehicle')
			{
				$("#wheelDiv").show();
			}
			$('#loadCapacityDiv').show();
			$('#loadCapacity').val(engineCC);
			$('#unladenWeight').val(unladenWeight);
			$('#unladenWeightDiv').show();
			$("#busTypeDiv").hide();
			$("#busType").val('0');
		}
		else if(vehicleType=='Tractor')
		{
			$('#vehicleHorsePowerDiv').show();
			$('#vehicleHorsePower').val(engineCC);
			$("#busTypeDiv").hide();
			$("#busType").val('0');
		}
		else if(vehicleType=='Heavy Bus' || vehicleType=='Medium Bus')
		{
			$("#busTypeDiv").show();
		}
		else
		{
			$("#busTypeDiv").hide();
			$("#busType").val('0');
		}
		if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType == 'Diesel')
		{
			$('#engineCCDiv').show();
			$('#engineCC').val(engineCC);
		}
		else if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType == 'Electric')
		{
			$('#vehicleKiloWattDiv').show();
			$('#vehicleKiloWatt').val(engineCC);
		}
		else if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType=='Hybrid')
		{
			$('#engineCCDiv').show();
			$('#engineCC').val(engineCC);
		}
		else if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType=='Petrol')
		{
			$('#engineCCDiv').show();
			$('#engineCC').val(engineCC);
		}
	}

	function populateRenewalDurationDropdown()
	{
		$('#renewalDuration').empty();
		var vehicleTypeDesc	=	$("#vehicleType :selected").text();
	
		if(vehicleTypeDesc == "Two Wheeler" || vehicleTypeDesc == "Light Vehicle")
		{
			$('#renewalDuration').append("<option value='12'>1 Year</option>");
			$('#renewalDuration').append("<option value='24'>2 Years</option>");
			$('#renewalDuration').append("<option value='36'>3 Years</option>");
			$('#renewalDuration').append("<option value='48'>4 Years</option>");
			$('#renewalDuration').append("<option value='60'>5 Years</option>");
		}
		if(vehicleTypeDesc == "Power Tiller")
		{
			$('#renewalDuration').append("<option value='120'>10 Years</option>");
		}
		else
		{
			$('#renewalDuration').append("<option value='6'>6 Months</option>");
			$('#renewalDuration').append("<option value='12'>1 Year</option>");
		}
	}
	
	function onChangeModel(vehicleModelId)
	{
		$.ajax
		({
			async: false,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getVehicleModelDtls&vehicleModelId='+vehicleModelId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					unladenWeight = $(this).find('unladenWeight').text();
					engineCC = $(this).find('engineCC').text();
					seatCapacity = $(this).find('seatCapacity').text();
					$("#engineCC").val(engineCC);
					$("#seatCapacity").val(seatCapacity);
					$("#unladenWeight").val(unladenWeight);
					$('#loadCapacity').val(engineCC);
				});
			}
		});
	}
	
	function checkPersonalOutstanding()
	{
		
	}
 
</script>
	
<style>
	#vehicleForm .error
	{
		 color: red; 
	 }
</style>
