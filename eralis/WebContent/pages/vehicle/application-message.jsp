<%
	String applicationNo = (String) request.getAttribute("APPLICATION_NO");
	String message = (String) request.getAttribute("MESSAGE");
	if(message.equalsIgnoreCase("DUPLICATE"))
	{
%>
		<div class="alert alert-warning">
			<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
			<b>Already Exists!!</b><br>
			Vehicle with the same <b>Engine Number, Chassis Number</b> and <b>Vehicle Type</b> for the same <b>Owner</b> has already
			been registered in your system. Please verify the information properly and try again.
		</div>
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else if(message.equalsIgnoreCase("ALREADY_APPLIED"))
	{
%>
		<div class="alert alert-warning">
			<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
			<b>Already Applied!!</b><br>
			Application for the Vehicle with the same <b>Engine Number, Chassis Number</b> and <b>Vehicle Type</b> for the same <b>Owner</b> has already
			been applied in your system and is still pending for DISPATCH.
		</div>
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else if(message.equalsIgnoreCase("VEHICLE_DUPLICATE_TRANSACTION"))
	{
%>
		<div class="alert alert-warning">
			<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
			<b>Already Applied!!</b><br>
			Application for this VEHICLE has already been applied in the system and is still pending for DISPATCH.<br/>
			Please verify the information properly and try again.
		</div>
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else if(message.equalsIgnoreCase("LICENSE_DUPLICATE_TRANSACTION"))
	{
%>
		<div class="alert alert-warning">
			<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
			<b>Already Applied!!</b><br>
			Application for this LICENSE has already been applied in the system and is still pending for DISPATCH.<br/>
			Please verify the information properly and try again.
		</div>
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else if(message.equalsIgnoreCase("LEARNER_DUPLICATE_TRANSACTION"))
	{
%>
		<div class="alert alert-warning">
			<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
			<b>Already Applied!!</b><br>
			Application for this LEARNER LICENSE has already been applied in the system and is still pending for DISPATCH.<br/>
			Please verify the information properly and try again.
		</div>
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else if(message.equalsIgnoreCase("RESUBMIT-COMPLETE"))
	{
%>
		<div class="well">
		Application has been successfully re-submitted and Application Number is 
		<span class="label label-success arrowed-in arrowed-in-right"><%=applicationNo %></span>
		</div>
		
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else if(message.equalsIgnoreCase("RESUBMIT-INCOMPLETE"))
	{
%>
		<div class="well">
		Sorry!Application could not re-submitted. Please contact ICT officer.
		</div>
		
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
	else
	{
%>
		<div class="well">
		Thank you <b><%=request.getAttribute("USER_NAME") %></b>!!<br>
		Application has been successfully submitted and Application Number is 
		<span class="label label-success arrowed-in arrowed-in-right"><%=applicationNo %></span>
		</div>
		
		<script>
			setTimeout('reloadPage()',10000);		
		</script>
<%
	}
%>