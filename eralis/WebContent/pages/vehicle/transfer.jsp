<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		  Transfer
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Transfer of Vehicle
		</small>
	</h1>
 </div><!-- /.page-header -->
 <div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
		<div id="incompleteValidationMsg"></div>
		<div id="Msg"></div>
	 <div class="widget-box">
	  <div class="widget-body">
		<div class="widget-main">
		  <div class="row">
			<div class="col-lg-12">
			  <jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
			  <div class="form-group">
				<div class="col-lg-2">
					<label>Vehicle Number:<span style="color: #ff0000">*</span></label>
				</div>
					<div class="col-lg-3">
						 <div class="input-group">
							<html:text property="vehicleNumber" styleClass="form-control" styleId="displayVehicleNumber" readonly="true"></html:text>
							<html:hidden property="vehicleId" styleClass="form-control" styleId="vehicleId" ></html:hidden>
							  <span class="input-group-btn">
						      	 <a href="#" onclick="openModalForm('transfer1')" class="btn btn-purple btn-sm">
							  		<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
							  </span>
						 </div>
					  </div>
					  <div class="col-lg-3">
					  	<label style="display:none;color: #ff0000" id="vehicleNumberValidation">Please search a vehicle number</label>
					  </div>
				    </div>
			     </div>
			   </div>
			</div>
		</div>
	</div>
		 
  <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Transferor Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main" id="Personal">
				<div class="form-group">
					<div class="col-lg-2">
						 <label class=control-label>Owner Type:</label>
					</div>
	                        <div class="col-lg-3" id="displayOwnerType">
	                              </div>
				    <div class="col-lg-2">
						<label class=control-label>Owner ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayOwnerID">
	                            </div> 
	                      </div> 
	                    <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Owner Name:</label>
					</div>
	                       <div class="col-lg-3" id="displayOwnerName">
	                             </div>
				    <div class="col-lg-2">
					   <label class=control-label>Citizen ID :</label>
					</div>
	                    <div class="col-lg-3" id="displayCitizenID">
	                             </div> 
	                      </div>    
	                   <div class="form-group">
					<div class="col-lg-2">
						<label class=control-label>Phone:</label>
					</div>
	                     <div class="col-lg-3" id="displayPhone">
	                           </div>
	                   </div>   
	                   <div id="nationalDIV">
	            	 		<h4>Address (National)</h4>
	                  <div class="form-group">
	                     <div class="col-lg-2">
					  <label class=control-label>Dzongkhag:</label>
				   </div>
		                <div class="col-lg-3" id="displayDzongkhag">
	                       </div>
	                   <div class="col-lg-2">
					<label class=control-label>Gewog:</label>
				 </div>
		                <div class="col-lg-3" id="displayGewog">
	                       </div>
	                   </div>
	                  <div class="form-group">
	                    <div class="col-lg-2">
				  <label class=control-label>Village:</label>
			   </div>
	               <div class="col-lg-3" id="displayVillage">
	                     </div>  
	               	</div>  
                   </div>
                  <div id="internationalDIV">
                  <h4>Address (Foreign National)</h4>
                     <div class="form-group">
                       <div class="col-lg-2">
						 <label class=control-label>Country:</label>
					   </div>
			                <div class="col-lg-3" id="displayCountry">
	                        </div>
                     <div class="col-lg-2">
						<label class=control-label>Address:</label>
					 </div>
			                <div class="col-lg-3" id="displayAddress">
	                        </div>
                        </div>     
                   </div>              
       			</div>
       			<div class="widget-main" id="Organization" style="display:none">
					<div class="form-group">
						<div class="col-lg-2">
							 <label class=control-label>Owner Type:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgOwnerType"></div>
					    <div class="col-lg-2">
							<label class=control-label>Agency :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAgency"></div> 
		           	</div> 
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Department/Name:</label>
						</div>
		               <div class="col-lg-3" id="displayOrgOwnerName"></div>
					    <div class="col-lg-2">
						   <label class=control-label>Address :</label>
						</div>
		                <div class="col-lg-3" id="displayOrgAddress"></div> 
		            </div>    
		            <div class="form-group">
						<div class="col-lg-2">
							<label class=control-label>Phone:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgPhone"></div>
		                <div class="col-lg-2">
							<label class=control-label>Dzongkhag:</label>
						</div>
		                <div class="col-lg-3" id="displayOrgDzongkhag"></div>
		            </div>   
       			</div>
	          </div>
           </div>
	  <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Registration Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Last Registration Date:</label>
							</div>	
		                   <div class="col-lg-3" id="displayLastRegistrationDate">
                          </div>
		                    <div class="col-lg-2">
								<label class="control-label">Expiry Date:</label>
					        </div>	
		                    <div class="col-sm-3" id="displayExpiryDate">
                           </div>
		                 </div>
		              </div>
                   </div>
                 </div>
               </div>
             </div>
        <div class="widget-box">
		     <div class="widget-header widget-header-small">
			      <h5 class="widget-title lighter">Transfer History Details</h5>
		     </div>						
		         <div class="widget-body">
			        <div class="widget-main">
                        <div id="transferHistoryTable">
			            </div>
                    </div>
		       </div>
            </div>
       <div class="widget-box">
		     <div class="widget-header widget-header-small">
			      <h5 class="widget-title lighter">Transferree Details</h5>
		     </div>						
	         <div class="widget-body">
		        <div class="widget-main">
                      <div class="row">
				      <div class="col-lg-12">
					    <div class="form-group">
						   <div class="col-lg-3">
							<label>
								<html:radio property="personalRadio" styleClass="ace" styleId="personalRadio" value="p" onclick="showPersonal()"></html:radio>	
								<span class="lbl"> Personal</span>
							</label>
							
							<label>
								<html:radio property="personalRadio" styleClass="ace" styleId="organisationRadio" value="o" onclick="showOrganisation()"></html:radio>	
								<span class="lbl"> Organisation</span>
							</label>
					    	</div>  
					    	<div class="col-lg-3">
					    		<label style="display:none;color: #ff0000" id="radioValidation">Please select atleast one option</label>
					    	</div>
                          </div>  
                         <div class="form-group" style="display:none" id="personal1"> 
							  <div class="col-lg-2">
							     <label>Citizen ID :<span style="color: #ff0000">*</span></label>
						      </div>
							  <div class="col-lg-3">                                         
							     <div class="input-group"> 
							       <html:text property="citizenID" styleClass="form-control" styleId="citizenID" readonly="true"></html:text>
									<span class="input-group-btn">
										 <a href="#" onclick="openModalForm('citizen')" class="btn btn-purple btn-sm">
											<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
										</a>
									</span>
					           	  </div>
					          </div>
					          <div class="col-lg-3">
					          	<label style="display:none;color: #ff0000" id="citizenIDValidation">Please search for transferee details</label>
					          </div>
						</div>	  
					    <div class="form-group" style="display:none" id="organization1"> 
						       <div class="col-lg-2">
							     <label> Owner ID :<span style="color: #ff0000">*</span></label>
						       </div>
						         <div class="col-lg-3">
						         	<div class="input-group">
                                       <html:text property="ownerID" styleClass="form-control" styleId="ownerID" readonly="true"></html:text>
                                    
						              <span class="input-group-btn">
						      	         <a href="#" onclick="openModalForm('owner')" class="btn btn-purple btn-sm">	
									   		<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								         </a>
										</span>
									</div>
								 </div>
								 <div class="col-lg-3">
						          	<label style="display:none;color: #ff0000" id="ownerIDValidation">Please search for transferee details</label>
						          </div>
	                           </div>
					        </div>
	                     </div>
		             </div>
		          </div>
                  </div>
     <div class="widget-box" id="personal" style="display:none">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Owner Name :</label>
							</div>
		                     <div class="col-lg-3" id="TransferOwnerName">
                   
                             </div>
						   
						    <div class="col-lg-2">
								<label class="control-label">Dzongkhag :</label>
							</div>
			                <div class="col-lg-3" id="TransferDzongkhag">
                 
                           </div> 
                         </div> 
                       <div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Gewog:</label>
							</div>
		                     <div class="col-lg-3" id="TransferGewog">
                   
                             </div>
						   
						    <div class="col-lg-2">
								<label class="control-label">Address :</label>
							</div>
			                <div class="col-lg-3" id="TransferAddress">
                 
                           </div> 
                        </div>    
                    </div> 				
				</div>
			</div> 
		</div>
	</div>
	 <div class="widget-box" id="organization" style="display:none">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Organisation Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Owner Name:</label>
							</div>
		                     <div class="col-lg-3" id="transferOwnerName">
                   
                             </div>
						   
						    <div class="col-lg-2">
								<label class="control-label">Dzongkhag :</label>
							</div>
			                <div class="col-lg-3" id="transferDzongkhag">
                 
                           </div> 
                         </div> 
                       <div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Region :</label>
							</div>
		                     <div class="col-lg-3" id="transferRegion">
                   
                             </div>
						   
						    <div class="col-lg-2">
								<label class="control-label">Address :</label>
							</div>
			                <div class="col-lg-3" id="transferAddress">
                           </div> 
                       </div>    
                   </div> 				
				</div>
			</div> 
		</div>
	</div>	
   		<div class="widget-box">
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
					
						<div class="form-group">
							<div class="col-lg-2">
								<label>Transfer Type :<span style="color: #ff0000">*</span></label>
							</div>
							<div class="col-lg-3">
								 <html:select property="transferType" styleClass="form-control" styleId="transferType"  onchange="populateRemarks(this.id)">
								      <html:option value="">SELECT</html:option>
								      <html:option value="NORMAL">Normal</html:option>
								      <html:option value="AUCTIONED">Auctioned</html:option>
								      <html:option value="PARENT_TO_CHILD">Parents to child</html:option>
								      <html:option value="WITHIN_SPOUSE">Within Spouse</html:option>
								      <html:option value="PRIVILEGE">Privilege</html:option>
						          </html:select>
						          <!-- 
								<select class="form-control" id="transferType" onchange="populateRemarks(this.id)">
									<option value="NORMAL">Normal</option>
									<option value="PARENT_TO_CHILD">Parents to child</option>
									<option value="WITHIN_SPOUSE">Within Spouse</option>
									<option value="AUCTIONED">Auctioned</option>
									<option value="PRIVILEGE">Privilege</option>
								</select> -->
							</div>
						</div>
			
			       <div class="form-group">
							<div class="col-lg-2">
								<label>Sale Deed Date :<span style="color: #ff0000">*</span></label>
							</div>
		                     <div class="col-lg-3">
                                 <div class="input-group">
									<html:text property="saleDeedDate" styleClass="form-control date-picker" styleId="saleDeedDate" readonly="true"></html:text>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
								<label style="display:none;color: #ff0000" id="saleDeedDateValidation">Please enter sale deed date</label>
                             </div>
							<div class="col-lg-2">
								<label>Sale Deed Amount :<span style="color: #ff0000">*</span></label>
							</div>
		                     <div class="col-lg-3">
                                 <html:text property="saleDeedAmount" styleClass="form-control" styleId="saleDeedAmount"></html:text>
                                 <label style="display:none;color: #ff0000" id="saleDeedAmountValidation">Please enter sale deed amount</label>
                             </div>
	                   </div>  
	                   <div class="form-group">
						<div class="col-lg-2">
								<label>Region :</label>
					    </div>	
		                <div class="col-sm-3">
                         			<html:hidden property="region" styleId="vehicleRegionId" value="<%=regionId %>"></html:hidden>
								   <input type="text" disabled="disabled" id="regionName" class="form-control" value="<%=regionName %>"/>
                         </div>
                         <div >
				     		<div class="col-lg-2">
								<label>Remarks :</label>
					 		</div>
                     		<div class="col-lg-3">
                               <html:textarea property="remarks" styleClass="form-control" styleId="remarks" value="Normal"></html:textarea>
                           	</div>
                         </div>
	                       </div> 
			              </div>
	                    </div> 
	                  </div>
	                </div> 
                  </div>
				<div class="widget-box">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Attachments</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<div class="row">
								<div class="col-lg-12">
									
									<div class="form-group">
										<div class="col-lg-2">
											<label> Supporting Document:</label>
										</div>
										<div class="col-lg-3">
											<html:file property="supportingDocument" styleId="supportingDocument" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
											<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
										</div>
						              </div>
		                     		 </div> 
		                   		</div>
		                	 </div>
						</div>
					</div>	
					<div class="row">
						<div id="displayMsgDiv"></div>
					</div>
					<div class="pull-left">
		                <html:hidden property="engineType" styleId="engineType"></html:hidden>
						<html:hidden property="vehicleType" styleId="vehicleType"></html:hidden>
						<html:hidden property="loadCapacity" styleId="loadCapacity"></html:hidden>
						<html:hidden property="seatCapacity" styleId="seatCapacity"></html:hidden>
						<html:hidden property="vehicleHorsePower" styleId="vehicleHorsePower"></html:hidden>
						<html:hidden property="vehicleKiloWatt" styleId="vehicleKiloWatt"></html:hidden>
						<html:hidden property="engineCC" styleId="engineCC"></html:hidden>
		                <html:hidden property="transferorCustomerId" styleId="transferorCustomerId"></html:hidden>
						<html:hidden property="transfereeCustomerId" styleId="transfereeCustomerId1"></html:hidden>
						<html:hidden property="vehicleRegistrationType" styleId="ownerType"></html:hidden>
						<html:hidden property="vehicleRegistrationType" styleId="vehicleRegistrationType"></html:hidden>	
						<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
					    <logic:equal value="Y" name="priviledge" property="isNew">
						   <button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
						</logic:equal>
					</div>
			
		</html:form>
		</div>

 <div id="citizen" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					

						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="cidPersonalModal" placeholder="CID"  />
                                 </div>
						</div>
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="CustomerID"> Customer ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="customerIdPersonalModal" placeholder="CustomerID"  />
                                 </div>
						</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="personalListTable1">
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<div id="owner" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Code"> Code : </label>
                            <div class="col-sm-4">
                                 <input type="text" id="codeOrganisationModal" placeholder="Code"  />
                            </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Name"> Name : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="nameOrganisationModal" placeholder="Name"  />
                                 </div>
						</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchOrganisationInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			
			<div id="organisationListTable">
		   </div>
	  </div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

 <div id="transfer1" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
								 <label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
						  </div>
						 <div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
						  </div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="renewalListTable">
		  </div>
			
		</div>
	</div>
 </div><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript">

	function openModalForm(modalId)
	{
		$('#'+modalId).modal('show');
		$("#vehicleNumberValidation").hide();
		$("#citizenIDValidation").hide();
		$("#ownerIDValidation").hide();
	}
	   var pageIdentifier = "<%=pageIdentifier%>";
	   var pageId = "<%=pageId%>";
	
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})

		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function() {
			$(this).prev().focus();
		});

		$('.fileupload').ace_file_input({
			no_file : 'No File ...',
			btn_choose : 'Choose',
			btn_change : 'Change',
			droppable : false,
			onchange : null,
			thumbnail : false,
			whitelist:'png|jpg|jpeg',
			blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
		});

		$(document).ready(function()
		{
			$('#personalRadio').attr('checked', true);
			$('#personal').show();
			$('#personal1').show();
		});
		
		function showPersonal()
		{
			$("#organization").hide(); 
		    $("#personal").show(); 
			$("#organization1").hide(); 
		    $("#personal1").show(); 
		}
			
		function showOrganisation()
		{
			$("#organization").show(); 
		    $("#personal").hide(); 
			$("#organization1").show(); 
		    $("#personal1").hide(); 
		}

		function populateRemarks(id)
		{
			$('#remarks').val("");
			$('#saleDeedAmount').attr('readonly',false);
			
			var selectedOption = $("#transferType option:selected").text();
			$('#remarks').val(selectedOption);

			if($('#transferType').val() == "NORMAL" ||
					$('#transferType').val() == "AUCTIONED"){
				$('#saleDeedAmount').attr('readonly',false);
				$('#saleDeedAmount').val('');
			}
			if($('#transferType').val() == "PARENT_TO_CHILD" ||
					$('#transferType').val() == "WITHIN_SPOUSE" ||
					$('#transferType').val() == "PRIVILEGE"){
				$('#saleDeedAmount').attr('readonly',true);
				$('#saleDeedAmount').val('0');
			}
			
			
		}

		function searchRenewalInfo()
		{ 
			var vehicleNumber = $('#vehicleNumberRenewalModal').val();
			var engineNumber = $('#engineNumberRenewalModal').val();
			var chasisNumber = $('#chasisNumberRenewalModal').val();
			var cidNumber = $('#citizenIdRenewalModal').val();
	
			if(vehicleNumber == "")
				vehicleNumber = "NA";
			if(engineNumber == "")
				engineNumber = "NA";
			if(chasisNumber == "")
				chasisNumber = "NA";
			if(cidNumber == "")
				cidNumber = "NA";
	
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=NA",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#renewalListTable").html(responseText);
					$("#renewalListTable").show();
				}
			});
		 }

		function searchPersonalInfo()
		{
			var name = $('#namePersonalModal').val();
			var cidNumber = $('#cidPersonalModal').val();
			var customerId = $('#customerIdPersonalModal').val();
			var regionId = $('#regionPersonalModal').val();
			var dzongkhagId = $('#dzongkhagPersonalModal').val();
	 
			if(name == "")
				name = "NA";
			if(cidNumber == "")
				cidNumber = "NA";
			if(customerId == "")
				customerId = "NA";
			if(regionId == "")
				regionId = "NA";
			if(dzongkhagId == "")
				dzongkhagId = "NA";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList1&name="+name+"&cid="+cidNumber+"&customerId="+customerId+"&region="+regionId+"&dzongkhag="+dzongkhagId+"&searchType=VEHICLE_TRANSFER",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#personalListTable1").html(responseText);
					$("#personalListTable1").show();
				}
			});
		}

		function searchOrganisationInfo()
		{  
			var code = $('#codeOrganisationModal').val();
			var name = $('#nameOrganisationModal').val();
			var type = $('#typeOrganisationModal').val();
			
			if(code == "")
				code = "NA";
			if(name == "")
				name = "NA";
			if(type == "")
				type = "NA";
	
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getOrganisationInfoList1&code="+code+"&name="+name+"&type="+type,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#organisationListTable").html(responseText);
					$("#organisationListTable").show();
				}
			});
		  }

		  function searchVehicleNumber()
		  { 
				var vehicleId = $('#vehicleId').val();
				if(vehicleId == "")
					vehicleId = "NA";
				
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getTransferHistoryList&vehicleId="+vehicleId,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#transferHistoryTable").html(responseText);
						$("#transferHistoryTable").show();
					}
				});
		   }

            $(document).ready(function()
	     {
			$('#submitBtn').click(function()
			{
				var returnVal = validateForm();

				if(returnVal == "1")
				{
					var identityTypeId;
				    var requestType = "VEHICLE";
					var serviceType = "TRANSFER";
					var identityNo = $('#vehicleId').val();
					if( $("#engineType").val() == "Electric")
						   identityTypeId = '0'; 
					else
						identityTypeId = $('#vehicleType').val();
					var loadingCapacity = $('#loadCapacity').val();
					var seatingCapacity = $('#seatCapacity').val();
					var vehicleHP = $('#vehicleHorsePower').val();
					var kilowatts = $('#vehicleKiloWatt').val();
					var engineCC = $('#engineCC').val();
					var purchaseDate = "";
					var saleDeedAmount = $('#saleDeedAmount').val();
					var saleDeedDate = $('#saleDeedDate').val();
					var transferType = $('#transferType').val();
					$("#trasacationType").val("transfer");
					
					getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, "", transferType);
				}
				else
					return false;
			});
		});

		function formSubmit()
		{
			$('#buttonSubmit').attr('disabled', true);
	
			$.blockUI
	    	({ 
	    		css: 
	    		{ 
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px', 
		            '-moz-border-radius': '10px', 
		            opacity: .5, 
		            color: '#fff' 
	    		} 
	    	});
	
			var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=new_transfer',type:'POST',data: $("#vehicleForm").serialize()}; 
		    $("#vehicleForm").ajaxSubmit(options);
	        $('#displayMsgDiv').show();
	        setTimeout('hideStatus("displayMsgDiv")',10000);
	
	        setTimeout($.unblockUI, 1000);
		}

		function validateForm()
		{
			var vehicleNumber = $('#displayVehicleNumber').val();
			var citizenID = $('#citizenID').val();
			var ownerID = $('#ownerID').val();
			var saleDeedDate = $('#saleDeedDate').val();
			var saleDeedAmount = $('#saleDeedAmount').val();
	
			$('#vehicleNumberValidation').hide();
			$('#radioValidation').hide();
			$('#citizenIDValidation').hide();
			$('#ownerIDValidation').hide();
			$('#saleDeedDateValidation').hide();
			$('#saleDeedAmountValidation').hide();
	
			if(vehicleNumber == "")
			{
				$('#vehicleNumberValidation').show();
				$('#vehicleNumberValidation').get(0).scrollIntoView();
				return "0";
			}
			if ($('input[type=radio].ace:checked').length == '0')
			{
				$('#radioValidation').show();
				$('#radioValidation').get(0).scrollIntoView();
				return "0";
			}
			if ($('input[type=radio].ace:checked').val() == 'p')
			{
				if(citizenID == "")
				{
					$('#citizenIDValidation').show();
					$('#citizenIDValidation').get(0).scrollIntoView();
					return "0";
				}
			}
			if ($('input[type=radio].ace:checked').val() == 'o')
			{
				if(ownerID == "")
				{
					$('#ownerIDValidation').show();
					$('#ownerIDValidation').get(0).scrollIntoView();
					return "0";
				}
			}
			if(saleDeedDate == "")
			{
				$('#saleDeedDateValidation').show();
				$('#saleDeedDateValidation').get(0).scrollIntoView();
				return "0";
			}
			if(saleDeedAmount == "")
			{
				$('#saleDeedAmountValidation').show();
				$('#saleDeedAmountValidation').get(0).scrollIntoView();
				return "0";
			}
			else
			{
				return "1";
			}
		}

		var fileError;
       	function validation(thisform,msgId,fileObj)
       	{
       		var fileId = fileObj.id;
       		with(thisform)
       		{
       			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
       			{
       				document.getElementById(fileId).value = "";
       				return false;
       			}
       			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
       			{
       				document.getElementById(fileId).value = "";
       				return false;
       			}
       		}
       	}

    	var context = "<%=request.getContextPath()%>";
    
	    function calTranserAmount()
		{
			var penalty	=	$("#penalty").val();
			var taxedAmount	=	$("#taxedAmount").val();
			var transferFees	=	$("#transferFees").val();
	
			if(amount=="null")
				{amount=0;}
			if(penalty=="null")
				{penalty=0;}
			if(taxedAmount=="null")
				{taxedAmount=0;}
			if(transferFees=="null")
				{transferFees=0;}
			var totalAmount	=	Number(penalty)+Number(taxedAmount)+Number(transferFees);
			$("#totalAmount").val(totalAmount);
		}

 </script>
<style>
  #vehicleForm .error { color: red; }
</style>