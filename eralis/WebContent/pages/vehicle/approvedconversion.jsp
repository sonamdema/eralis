<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");
%>
<script>
	function formSubmit()
	{
		$('#approveBTN').attr('disabled', true);

		$.blockUI
        ({ 
        	css: 
        	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
        	} 
        });
        
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=conversion_application_approval&applicationNo='+application,type:'POST',data: $("#learnerForm").serialize()}; 
	
	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}
	
	function formReject()
	{
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=conversion_application_reject&applicationNo='+application,type:'POST',data: $("#vehicleForm").serialize()}; 
	
		$("#vehicleForm").ajaxSubmit(options);
		$('#displayMsgDiv').show();
		setTimeout('hideStatus("displayMsgDiv")',4000);
		setTimeout('showTaskList()',2000);
	}	
	
	function dispatch()
	{
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=VEHICLE&serviceType=CONVERSION',type:'POST',data: $("#vehicleForm").serialize()}; 
	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	}
		
	var a="<%=request.getAttribute("applicationNo")%>";
	$("#applicationNo").val(a);

		
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Conversion
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			     conversion of Vehicle
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	
	 <logic:equal value="DISPATCH" name="param">
	     <div class="row">
	     	<div class="col-lg-12">
	     		<div class="alert alert-block alert-success">
					<i class="ace-icon fa fa-check green"></i>
					Generated Vehicle Number:
					<strong class="green">
						<%=request.getAttribute("NEW_VEHICLE_NUMBER") %>
					</strong>
				</div>
	     	</div>
	     </div>
     </logic:equal>
	 <div class="row">
	 	<div class="col-lg-12">
	 		<span>Vehicle Conversion Application Details for Application No: <strong><%=request.getAttribute("applicationNo")%></strong></span>
	 	</div>
	 </div>
	 <div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Previous Vehicle Number & Owner Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
					<div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Vehicle Number:</label>
						    </div>	
                         <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNumber() %></label></div> 
					</div>	
					<%
                	if(dto.getVehicleRegistrationType().equals("Personal"))
                	{
                %>
						 <div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Owner Type:</label>
							</div>	
                               <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleRegistrationType()%></label></div> 
                            <div class="col-lg-2">
								<label class="control-label">Owner ID :</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getCustomerId() %></label></div>
		                    </div>
		                   <div class="form-group ">
		                    <div class="col-lg-2">
								<label class="control-label">Owner Name :</label>
					        </div>	
		                       <div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Phone:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div> 
		                  	</div>
		                   <div class="form-group ">
		                   <%
		                   		if(dto.getIsInternational().equals("N"))
		                   		{
		                   %> 
		                    <div class="col-lg-2">
								 <label class="control-label">Dzongkhag:</label>
							</div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div> 
                            <div class="col-lg-2">
								<label class="control-label">vGewog:</label>
					        </div>	
	                       	<div class="col-lg-4"><label class="control-label"><%=dto.getGewog() %></label></div>
                             <%
		                   		}
                             %>
		                     </div>
		                    <div class="form-group ">
		                    <%
			                    if(dto.getIsInternational().equals("N"))
		                   		{
		                    %>
		                    <div class="col-lg-2">
								<label class="control-label">Village:</label>
							</div>	
                              <div class="col-lg-4"><label class="control-label"><%=dto.getVillage() %></label></div> 
                             <%
		                   		}
			                    if(dto.getIsInternational().equals("I"))
		                   		{
                             %>
		                    <div class="col-lg-2">
								<label class="control-label">Country:</label>
					        </div>	
		                        <div class="col-lg-4"><label class="control-label"><%=dto.getCountry() %></label></div>
                            <%
		                   		}
                            %>
                            	<div class="col-lg-2">
								 <label class="control-label">Address:</label>
							    </div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div> 
		                	</div> 
		         <%
                	}
                	else
                	{
		         %>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Owner Type</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getOwner() %></label></div>
				 			<div class="col-lg-2"><label class="control-label">Agency</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getMinistry() %></label></div>
				 		</div>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Department/Name</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
				 			<div class="col-lg-2"><label class="control-label">Address</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div>
				 		</div>
				 		<div class="form-group">
				 			<div class="col-lg-2"><label class="control-label">Phone No</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div>
				 			<div class="col-lg-2"><label class="control-label">Dzongkhag</label></div>
				 			<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div>
				 		</div>
		         <%
                	}
		         %>
                           </div>
                         </div>   
                            <div class="widget-header widget-header-small">
			                   <h5 class="widget-title lighter">Vehicle Details</h5>
		                   </div>						
		                    <div class="widget-body">
			                 <div class="widget-main">  
                              <div class="form-group ">  
	                              <div class="col-lg-2">
									 <label class="control-label">Company:</label>
						          </div>	
		                          <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleCompany()%></label></div>
			                     <div class="col-lg-2">
									<label class="control-label">Model:</label>
								 </div>	
                               	<div class="col-lg-4"><label class="control-label"><%=dto.getVehicleModel() %></label></div> 
		                      </div>    
				              <div class="form-group ">
		                            <div class="col-lg-2">
										<label class="control-label">Color:</label>
							        </div>	
			                        <div class="col-lg-4"><label class="control-label"><%=dto.getColour()%></label></div>
				                    <div class="col-lg-2">
										<label class="control-label">Chasis Number:</label>
									</div>	
	                                <div class="col-lg-4"><label class="control-label"><%=dto.getChasisNumber()%></label></div> 
				               </div>
			                   <div class="form-group">
				                    <div class="col-lg-2">
										<label class="control-label">Engine Number:</label>
							         </div>	
		                              <div class="col-lg-4"><label class="control-label"><%=dto.getEngineNumber() %></label></div>
				                 	<div class="col-lg-2">
										<label class="control-label">Engine Type:</label>
									</div>	
                                  <div class="col-lg-4"><label class="control-label"><%=dto.getEngineType() %></label></div> 
	                           </div>
	                           <div class="form-group"> 
		                            <div class="col-lg-2">
										<label class="control-label">Seat Capacity:</label>
							        </div>	
			                         <div class="col-lg-4"><label class="control-label"><%=dto.getSeatCapacity() %></label></div>
				                     <div class="col-lg-2">
										<label class="control-label">Load Capacity:</label>
									 </div>	
	                                  <div class="col-lg-4"><label class="control-label"><%=dto.getLoadCapacity() %></label></div> 
			                       </div>   
		                       </div>  
		                   </div>  
		                 <div class="widget-header widget-header-small">
	                        <h5 class="widget-title lighter">Conversion Details</h5>
                         </div>
	                      <div class="widget-body">
                            <div class="widget-main">
				                 <div class="form-group">
			                            <div class="col-lg-2">
											<label class="control-label">Conversion Date:</label>
								        </div>	
			                            <div class="col-lg-2"><label class="control-label"><%=dto.getConversionDate() %></label></div>
					                    <div class="col-lg-2">
											<label class="control-label">Conversion Reason:</label>
										</div>	
		                                 <div class="col-lg-2"><label class="control-label"><%=dto.getConversionReason() %></label></div> 
					                     <div class="col-lg-2">
										     <label class="control-label">Vehicle Rgn. Code:</label>
							              </div>	
			                              <div class="col-lg-2"><label class="control-label"><%=dto.getVehicleCode() %></label></div>
					               </div>   
				                  <div class="form-group">
				                  		<div class="col-lg-2">
											<label class="control-label">Region:</label>
										</div>	
		                                 <div class="col-lg-2"><label class="control-label"><%=dto.getRegion() %></label></div> 
					                     <div class="col-lg-2">
										     <label class="control-label">Vehicle Type:</label>
							              </div>	
			                              <div class="col-lg-2"><label class="control-label"><%=dto.getVehicleType() %></label></div>
		                              <div class="col-lg-2">
										 <label class="control-label">Receipt Date:</label>
									   </div>	
		                                  <div class="col-lg-2"><label class="control-label"><%=dto.getReceiptDate() %></label></div> 
				                   </div>   
		                           <div class="form-group">
		                           		<div class="col-lg-2">
											<label class="control-label">Receipt No:</label>
								       </div>	
					                       <div class="col-lg-2"><label class="control-label"><%=dto.getReceiptNo() %></label></div> 
			                               <div class="col-lg-2">
										 		<label class="control-label">Conversion Amount:</label>
									  		</div>	
		                                  	<div class="col-lg-2"><label class="control-label"><%=dto.getAmount() %></label></div> 
		                              	<div class="col-lg-2">
											<label class="control-label">Penality:</label>
									   </div>	
		                                  <div class="col-lg-2"><label class="control-label"><%=dto.getPenalty() %></label></div> 
				                   </div>   
		                       <div class="form-group">
		                       	<logic:equal value="APPROVE" name="param">
								   <jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
								</logic:equal>
								   <html:hidden property="vehicleId" value="<%=dto.getVehicleId() %>"></html:hidden>
							   </div>
		                  </div> 
		                </div> 
		           </div>        
        </html:form>
        <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	 </div>
  <input type="hidden" id="applicationNo"/>
  <div id="displayMsgDiv"></div>

<div>
	<logic:equal value="APPROVE" name="param">
		<button type="button" class="btn btn-sm" id="approveBTN" onclick="formSubmit()">
			<i class="ace-icon fa fa-check"  ></i>
			Approve
		 </button>
		 <button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
			<i class="ace-icon fa fa-times red2"></i>
			Reject
		</button>
   </logic:equal>
   <logic:equal value="DISPATCH" name="param">
   <button type="button" class="btn btn-sm" onclick="dispatch()">
		<i class="ace-icon fa fa-check"  ></i>
		Dispatch
	 </button>
   </logic:equal>
</div>