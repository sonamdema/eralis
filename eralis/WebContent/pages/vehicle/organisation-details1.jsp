<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Customer Id</th>
			<th>Name</th>
			<th>Region</th>
			<th>Dzongkhag</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="ORGANISATION_LIST">
			<logic:iterate id="organisation" name="ORGANISATION_LIST">
				<tr>
					<td>
						<input type="radio" name="organisationListRadio" id='<bean:write name="organisation" property="organizationInfoId"/>' onclick="addSelected(this.id)"/>
					</td>
					<td><bean:write name="organisation" property="customerID"/></td>
					<td><bean:write name="organisation" property="name"/></td>
					<td><bean:write name="organisation" property="region"/></td>
					<td><bean:write name="organisation" property="dzongkhag"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="ORGANISATION_LIST">
			<tr>
				<td colspan="3" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>
<div>
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>

<script>
	var globalId = "";
	function addSelected(id)
	{
		globalId = id;
	}

	function getSelected()
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOrganisationDtls&organisationInfoId='+globalId,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var name = $(this).find('name').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var region = $(this).find('region').text();
					var phone = $(this).find('phone').text();
					var email=$(this).find('email').text();
					var remarks=$(this).find('remarks').text();

					$('#organisationOwnerName').html("<label class='control-label'>"+name+"</label>");
					$('#organisationDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
					$('#organisationGewog').html("<label class='control-label'>"+gewog+"</label>");
					$('#organizationAddress').html("<label class='control-label'>"+address+"</label>");


					$('#name').val(name);
					$('#dzongkhag').val(dzongkhag);
					$('#region').val(region);
					$('#address').val(address);
					$('#phone').val(phone);
					$('#email').val(email);
					$('#remarks').val(remarks);
					
				});
			}
		});
	}

</script>