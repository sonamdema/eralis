<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<%

	VehicleDTO dto = (VehicleDTO) request.getAttribute("VEHICLE_DETAILS");
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
	String vehicleNo = (String) request.getAttribute("VEHICLE_NUMBER");
	
	String registrationType = dto.getRegistrationType();
	String registrationCode = dto.getVehicleRegistrationId();
	String vehicleType = dto.getVehicleType();
	String vehicleCompany = dto.getVehicleCompany();
	String vehicleModel = dto.getVehicleModel();
	String engineType = dto.getEngineType();
	String dealer = dto.getDealersName();
	String manufactureCountry = dto.getManufactureCountry();
	String purchaseType = dto.getPurchaseType();
	String busType = dto.getBusType();
%>

<script>

	$("#remarks").val('<%=dto.getRemarks()%>');
	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";
	
	var registrationType = "<%=registrationType%>";
	var registrationCode = "<%=registrationCode%>";
	var vehicleType = "<%=vehicleType%>";
	var vehicleCompany = "<%=vehicleCompany%>";
	var vehicleModel = "<%=vehicleModel%>";
	var engineType = "<%=engineType%>";
	var dealer = "<%=dealer%>";
	var manufactureCountry = "<%=manufactureCountry%>";
	var purchaseType = "<%=purchaseType%>";	
	var region = "<%=dto.getRegion()%>";
	var vehicleNo = "<%=vehicleNo%>";
	var busType	=	 "<%=busType%>";
	$(document).ready(function()
	{
		$('#registrationType').val(registrationType);
		$('#vehicleRegistrationId').val(registrationCode);
		$('#vehicleType').val(vehicleType);
		$('#vehicleCompany').val(vehicleCompany);
		$('#vehicleModel').val(vehicleModel);
		$('#engineType').val(engineType);
		$('#dealersName').val(dealer);
		$('#manufactureCountry').val(manufactureCountry);
		$('#purchaseType').val(purchaseType);
		$('#region').val(region);
		$('#vehicleNumber').val(vehicleNo);
		if(busType>0)
		{
			$("#busTypeDiv").show();
			$("#busType").val(busType);
		}
	});


</script>

<html:form action="/vehicle.html" method="POST" styleClass="form-horizontal" styleId="vehicleForm">
  <div class="row">
	<div class="widget-box">
	 	<div class="widget-header widget-header-small">
		  <h5 class="widget-title lighter">Vehicle Details</h5>
		</div>						
      <div class="widget-body">
	    <div class="widget-main">
				<div class="form-group ">
					<div class="col-lg-2">
						<label>Registration Type<span style="color: #ff0000">*</span>:</label>
					</div>	
                   
                            <div class="col-lg-3">
							       <html:select property="registrationType" styleClass="form-control" styleId="registrationType">
								      <html:option value="">--SELECT--</html:option>
						   		      <html:optionsCollection name="registrationTypeList" label="headerName" value="headerId"/>
						          </html:select>
                             </div> 
                            <div class="col-lg-2">
						  <label>Status <span style="color: #ff0000">*</span>:</label>
			           </div>	
                      <div class="col-sm-3"> 	
                      	<input type="text" value="Active" class="form-control"  disabled="disabled">
						<html:hidden property="status"  value="1"></html:hidden>
					  </div>
                 </div>
                <div class="form-group">
					<div class="col-lg-2">
						<label>Vehicle Rgn. Code<span style="color: #ff0000">*</span>:</label>
					</div>	
                          <div class="col-lg-3">
                               <html:select property="vehicleRegistrationId" styleClass="form-control" onchange="changeAttribute(this.value,'vehicleRegionId')" styleId="vehicleRegistrationId">
									<html:option value="">--SELECT--</html:option>
							   		<html:optionsCollection name="vehicleCodeList" label="headerName" value="headerId"/>
							   </html:select>
                             </div> 
                    <div class="col-lg-2">
						<label>Region<span style="color: #ff0000">*</span>:</label>
			        </div>	
                    <div class="col-lg-3">
						<html:select property="region" styleClass="form-control" styleId="region">
							<html:option value="">--SELECT--</html:option>
					   		<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
					   </html:select>
                    </div>
                </div>
                <div class="form-group ">
						<div class="col-lg-2">
						   <label>Vehicle Type<span style="color: #ff0000">*</span>:</label>
					    </div>	
	                    <div class="col-lg-3">
	                        <html:select property="vehicleType" styleClass="form-control" styleId="vehicleType" onchange="onChangeVehicleType(),populateRenewalDurationDropdown()">
								   <html:option value="">--SELECT--</html:option>
								   <html:optionsCollection name="vehicleTypeList" label="headerName" value="headerId"/>
						   </html:select>
                         </div> 
                         <div style='display:none' id="busTypeDiv">
                            <div class="col-lg-2">
							   <label>Bus Type<span style="color: #ff0000">*</span>:</label>
							</div>	
			                <div class="col-lg-3">
				            	<html:select property="busType" styleId="busType" styleClass="form-control">
									<html:option value="0">--SELECT--</html:option>
									<html:optionsCollection name="busTypeList" label="headerName" value="headerId"/>
								</html:select>
                          	</div> 
                          </div>
                        </div>
                       <div class="form-group">
	                        <div class="col-lg-2">
	                              <label>Registration Date<span style="color: #ff0000">*</span>:</label>
	                        </div>	
							<div class="col-lg-3">
								<div class="input-group">
									<html:text property="registrationDate" styleClass="form-control" styleId="registrationDate" readonly="true" value="<%=dto.getRegistrationDate() %>"></html:text>
									<span class="input-group-addon" style="cursor:pointer;">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
							<div class="col-lg-2">
								<label>Dealers Name<span style="color: #ff0000">*</span>:</label>
				           	</div>	
		                  	<div class="col-sm-3"> 	
						       <html:select property="dealersName" styleClass="form-control" styleId="dealersName">
							      <html:option value="">--SELECT--</html:option>
					   		      <html:optionsCollection name="dealersNameList" label="headerName" value="headerId"/>
					          </html:select>
							</div>	
                 		</div>
                <div class="form-group">
                  <div class="col-lg-2">
						<label>Vehicle Company<span style="color: #ff0000">*</span>:</label>
					</div>	
                       <div class="col-lg-3">
	                       <html:select property="vehicleCompany" styleClass="form-control" styleId="vehicleCompany" onchange="populateDependentDropDown(this.value, 'vehicleModel', '', 'VECHILE_MODEL_LIST', 'N')">
									<html:option value="">--SELECT--</html:option>
							   		<html:optionsCollection name="vehicleCompanyList" label="headerName" value="headerId"/>
						 	</html:select>
                     </div> 
                     <div class="col-lg-2">
						<label>Vehicle Model<span style="color: #ff0000">*</span>:</label>
					 </div>	
                       <div class="col-lg-3">
	                       <html:select property="vehicleModel" styleClass="form-control" styleId="vehicleModel">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="modelList" label="headerName" value="headerId"/>
						 	</html:select>
                       </div> 
                  </div>
                <div class="form-group">
                	<div class="col-lg-2">
						<label>Engine Type<span style="color: #ff0000">*</span>:</label>
					</div>	
                          <div class="col-lg-3">
	                	<html:select property="engineType" styleClass="form-control" styleId="engineType" onchange="onChangeVehicleType()">
							<html:option value="">--SELECT--</html:option>
					   		<html:optionsCollection name="engineTypeList" label="headerName" value="headerId"/>
					   	</html:select>
                          </div> 
                	<div class="col-lg-2" >
						<label>Vanity Number:</label>
			        </div>	
                       <div class="col-lg-3">
                          	<html:text property="vanityNumber" styleClass="form-control" styleId="vanityNumber" value="<%=dto.getVanityNumber() %>"></html:text>
                          </div>
             	</div>
                <div class="form-group">
                	<div class="col-lg-2">
						<label>Engine/Motor No<span style="color: #ff0000">*</span>:</label>
			     	</div>	
                          <div class="col-sm-3"> 	
						<html:text property="engineNumber" styleClass="form-control" styleId="engineNumber" value="<%=dto.getEngineNumber() %>" readonly="true"></html:text> 
				   	</div>
             		<div class="col-lg-2">
						<label>Chasis Number<span style="color: #ff0000">*</span>:</label>
					</div>	
                         	<div class="col-sm-3"> 	
						<html:text property="chasisNumber" styleClass="form-control" styleId="chasisNumber" value="<%=dto.getChasisNumber() %>" readonly="true"></html:text> 
					</div>
          		</div>
            	<div class="form-group">
            		
					<div id="loadCapacityDiv">
	                	<div class="col-lg-2">
							<label>Gross Vehicle Weight<span style="color: #ff0000">*</span>:</label>
						</div>	
	                       <div class="col-sm-3" > 	
							<html:text property="loadCapacity" styleClass="form-control" styleId="loadCapacity" value="<%=dto.getLoadCapacity() %>"></html:text> 
						</div>
					</div>
                  	  </div>
                  	  
                  	  <div class="form-group">
                  	  	<div id="vehicleHorsePowerDiv">
	                  	  	<div class="col-lg-2">
								<label>Horse Power<span style="color: #ff0000">*</span>:</label>
							</div>	
		                    <div class="col-sm-3"> 	
								<html:text property="vehicleHorsePower" styleClass="form-control" styleId="vehicleHorsePower" value="<%=dto.getVehicleHorsePower() %>"></html:text> 
							</div>
						</div>
						<div id="vehicleKiloWattDiv">
		                    <div class="col-lg-2">
						 		<label>KiloWatt<span style="color: #ff0000">*</span>:</label>
					      	</div>	
	                        <div class="col-sm-3"> 	
								<html:text property="vehicleKiloWatt" styleClass="form-control" styleId="vehicleKiloWatt" value="<%=dto.getVehicleKiloWatt() %>"></html:text> 
							</div>
						</div>
                  	  </div>
                  	  <div class="form-group">
                  	  	<div id="engineCCDiv">
		                  	<div class="col-lg-2" >
							     <label>Engine CC<span style="color: #ff0000">*</span>:</label>
						    </div>	
		                    <div class="col-sm-3"> 	
								<html:text property="engineCC" styleClass="form-control" styleId="engineCC" value="<%=dto.getEngineCC() %>"></html:text> 
							</div>
						</div>
						<div id="unladenWeightDiv">
			                   	<div class="col-lg-2">
									 <label>Unladen Weight<span style="color: #ff0000">*</span>:</label>
							  	</div>	
			                          <div class="col-sm-3"> 	
							     	<html:text property="unladenWeight" styleClass="form-control" styleId="unladenWeight" value="<%=dto.getUnladenWeight() %>"></html:text> 
						     	</div>
						</div>		
                 </div>
                  <div class="form-group">
                  	   <div class="col-lg-2">
					 		<label> Seat Capacity<span style="color: #ff0000">*</span>:</label>
					   	</div>	
                       	<div class="col-sm-3"> 	
							<html:text property="seatCapacity" styleClass="form-control" styleId="seatCapacity" value="<%=dto.getSeatCapacity() %>"></html:text> 
						</div>
                       <div class="col-lg-2">
						   <label>Colour<span style="color: #ff0000">*</span>:</label>
					   </div>	
                               <div class="col-sm-3"> 	
                               <html:select property="colour" styleClass="form-control" styleId="colour">
							      <html:option value="">--SELECT--</html:option>
					   		      <html:optionsCollection name="colourList" label="headerName" value="headerId"/>
					          </html:select>
						    </div>
                   </div>
                  <div class="form-group">
                  	<div class="col-lg-2">
						<label>Price<span style="color: #ff0000">*</span>:</label>
			        </div>	
                       <div class="col-sm-3"> 	
					  <html:text property="price" styleClass="form-control" styleId="price" value="<%=dto.getPrice() %>"></html:text> 
				    </div>
                    <div class="col-lg-2">
						<label>Manufacture Year<span style="color: #ff0000">*</span>:</label>
				    </div>	
                     <div class="col-sm-3"> 	
						<html:text property="manufactureYear" styleClass="form-control" styleId="manufactureYear" value="<%=dto.getManufactureYear() %>"></html:text> 
				    </div>
                  </div>
                <div class="form-group">
                	<div class="col-lg-2">
						<label>Manufacture Country<span style="color: #ff0000">*</span>:</label>
			        </div>	
                       <div class="col-sm-3"> 	
					  <html:select property="manufactureCountry" styleClass="form-control" styleId="manufactureCountry">
					      <html:option value="">--SELECT--</html:option>
			   		      <html:optionsCollection name="manufactureCountryList" label="headerName" value="headerId"/>
			          </html:select>
				    </div>
				    <div class="col-lg-2">
					    <label>Purchase Type:</label>
		          	</div>	
                       <div class="col-sm-3"> 	
				       <html:select property="purchaseType" styleClass="form-control" styleId="purchaseType">
					   <html:option value="">-----SELECT-----</html:option>
					   <html:option value="1">Normal</html:option>
                                <html:option value="2">Imported</html:option>
					   </html:select>
				    </div>
                  </div>
                  <div class="form-group">
                	<div class="col-lg-2">
						<label>Remarks:</label>
			        </div>	
                    <div class="col-sm-3"> 	
					   <html:textarea property="remarks" styleId="remarks" styleClass="form-control">ssssssss<%=dto.getRemarks()%></html:textarea>
				    </div>
                  </div>
               	</div>
              </div>
          </div>
	</div>
	<div id="registrationMsgDiv"></div>
	<div class="row">
		<div class="pull-left">
			<html:hidden property="vehicleId" styleClass="form-control" styleId="vehicleId" value="<%=dto.getVehicleId() %>"></html:hidden>
			<html:hidden property="vehicleTypeId" styleClass="form-control" styleId="vehicleTypeId" value="<%=dto.getVehicleType() %>"></html:hidden>
			<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
			<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Update</button>
     	</div>
	</div>
</html:form>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script>

$('#engineCCDiv').hide();
$('#vehicleKiloWattDiv').hide();
$('#engineCCDiv').hide();
$('#vehicleHorsePowerDiv').hide();
$('#loadCapacityDiv').hide();
$('#unladenWeightDiv').hide();


	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	engineTypeOnChange();
	onChangeVehicleType();
	function engineTypeOnChange()
	{	
		var engineType	=	$('#engineType option:selected').text();
		var vehicleType	=	$('#vehicleType option:selected').text();
	
	
		if(vehicleType=='Tractor')
		{
			if(engineType=='Electric' || engineType=='Hybrid')
			{
				 $("#engineCC").prop("readonly", true);
				 $("#vehicleHorsePower").prop("readonly", false);
				 $("#vehicleKiloWatt").prop("readonly", false);
			}
			else
			{
				 $("#engineCC").prop("readonly", false);
				 $("#vehicleHorsePower").prop("readonly", false);
				 $("#vehicleKiloWatt").prop("readonly", true);
			}
		}
		else
		{
			if(engineType=='Electric' || engineType=='Hybrid')
			{
				 $("#engineCC").prop("readonly", true);
				 $("#vehicleHorsePower").prop("readonly", true);
				 $("#vehicleKiloWatt").prop("readonly", false);
			}
			else
			{
				 $("#engineCC").prop("readonly", false);
				 $("#vehicleHorsePower").prop("readonly", true);
				 $("#vehicleKiloWatt").prop("readonly", true);
			}
		}
		
	}
	function onChangeVehicleType()
	{
		var vehicleType	=	$("#vehicleType :selected").text();
		var engineType	=	$("#engineType :selected").text();
		
		$("#busTypeDiv").hide();
		
		if(vehicleType=='Heavy Vehicle' || vehicleType=='Medium Vehicle')
		{
			$('#loadCapacityDiv').show();
			//$('#loadCapacity').val('');
			//$('#unladenWeight').val('');
			$('#unladenWeightDiv').show();
			$("#busType").val('0');
		}
		else if(vehicleType=='Tractor')
		{
			$('#vehicleHorsePowerDiv').show();
			$('#vehicleHorsePower').prop("readonly", false);
			$("#busType").val('0');
		}
		
		if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType == 'Diesel')
		{
			$('#engineCCDiv').show();
			//$('#engineCC').val('');
			$("#busType").val('0');
		}
		else if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType == 'Electric')
		{
			$('#vehicleKiloWattDiv').show();
			$("#busType").val('0');
			//$('#vehicleKiloWatt').val('');
		}
		else if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType=='Hybrid')
		{
			$('#engineCCDiv').show();
			//$('#engineCC').val('');
			$("#busType").val('0');
		}
		else if((vehicleType == 'Light Vehicle' || vehicleType == "Taxi") && engineType=='Petrol')
		{
			$('#engineCCDiv').show();
			//$('#engineCC').val('');
			$("#busType").val('0');
		}
		else if(vehicleType=='Heavy Bus' || vehicleType=='Medium Bus')
		{
			$("#busTypeDiv").show();
		}
		else
		{
			
			$("#busType").val('0');
		}
	}	

	$(document).ready(function()
	{
		var colorId = '<%=dto.getColour() %>';
      	$("#colour").val(colorId);
      	
	   	$("#vehicleForm").validate({
	  	 invalidHandler: function(form, validator) {
	           var errors = validator.numberOfInvalids();
	           if (errors) {                    
	               var firstInvalidElement = $(validator.errorList[0].element);
	               $('html,body').scrollTop(firstInvalidElement.offset().top);
	               firstInvalidElement.focus();
	           }
	       	},
			rules:
			{	
				registrationType:{
					required:true
				},
				status:{
					required:true,
				},
				vehicleRegistrationId:{
					required:true
				},
				region:{
				    required:true
				},
				vehicleType:{
					required:true
				},
				vehicleCompany:{
				    required:true
				},
				vehicleModel:{
					required:true
				},
				engineType:{
					required:true
				},
				dealersName:{
				    required:true
				},
				loadCapacity:{
					required:true,
					number:true
				},
				vehicleHorsePower:{
					required:true,
					number:true
				},
				vehicleKiloWatt:{
					required:true,
					number:true
				},
				engineCC:{
				    required:true,
				    number: true
				},
				unladenWeight:{
				    required:true,
				    number:true
				},
				seatCapacity:{
					required:true
				},
				colour:{
					required:true
				},
				price:{
					required:true,
					number: true
				},
				manufactureYear:{
					required:true,
					number:true,
					maxlength:4,
					minlength:4
				},
				manufactureCountry:{
					required:true
				},
			},    
			messages:
			{
				registrationType:{required: "Please select registration type"},
				status:{required: "Please enter status"},
				vehicleRegistrationId:{required: "Please select registration code"},
				region:{required: "Please select region"},
				vehicleType:{required: "Please select vehicle type"},
				vehicleCompany:{required: "Please select vehicle company"},
				vehicleModel:{required: "Please select vehicle model"},
				engineType:{required: "Please select engine type"},
				dealersName:{required: "Please select dealer"},
				loadCapacity:{required: "Please enter gross vehicle weight"},
				vehicleHorsePower:{required: "Please enter engine horse power"},
				vehicleKiloWatt:{required: "Please enter engine kilo watt"},
				engineCC:{required: "Please enter engine cc"},
				unladenWeight:{required: "Please enter unladen weight"},
				seatCapacity:{required: "Please enter seating capacity"},
				colour:{required: "Please enter vehicle color"},
				price:{required: "Please enter vehicle price"},
				manufactureYear:{required: "Please enter manufacture year"},
				manufactureCountry:{required: "Please select manufacture country"},
			}
		});

		$('#submitBtn').click(function()
		{
			if($('#vehicleForm').valid()) 
			{
				var options = {target:'#registrationMsgDiv',url:context+'/vehicle.html?method=update_vehicle_data',type:'POST',data: $("#vehicleForm").serialize()}; 
			    $("#vehicleForm").ajaxSubmit(options);
		        $('#registrationMsgDiv').show();
		        setTimeout('hideStatus("registrationMsgDiv")',10000);
		        setTimeout('reloadPage()',3000);
			}
			else 
			{
				return false;
			}
		});
		
	});

</script>
<style>
	#vehicleForm .error { color: red; }
</style>