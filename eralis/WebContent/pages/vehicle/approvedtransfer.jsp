<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
VehicleDTO dto = (VehicleDTO)request.getAttribute("Vehicledto");
%>

<script>
function formSubmit()
	{
		$('#approveBTN').attr('disabled', true);
	
		$.blockUI
	    ({ 
	    	css: 
	    	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	    	} 
	    });
    
		var application=$("#applicationNo").val();
		var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=transfer_application_approval&applicationNo='+application,type:'POST',data: $("#vehicleForm").serialize()}; 

	    $("#vehicleForm").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    setTimeout('showTaskList()',2000);
	    setTimeout($.unblockUI, 1000);
	}

function formReject()
   {
	var application=$("#applicationNo").val();
	var options = {target:'#displayMsgDiv',url:context+'/vehicle.html?method=transfer_application_reject&applicationNo='+application,type:'POST',data: $("#learnerForm").serialize()}; 

		$("#vehicleForm").ajaxSubmit(options);
		$('#displayMsgDiv').show();
		setTimeout('hideStatus("displayMsgDiv")',4000);
		setTimeout('showTaskList()',2000);
     }	
     
function dispatch()
{
	var application=$("#applicationNo").val();
	var options = {target:'#displayMsgDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=VEHICLE&serviceType=TRANSFER',type:'POST',data: $("#vehicleForm").serialize()}; 
    $("#vehicleForm").ajaxSubmit(options);
    $('#displayMsgDiv').show();
    setTimeout('hideStatus("displayMsgDiv")',4000);
    setTimeout('showTaskList()',2000);
}

var a="<%=request.getAttribute("applicationNo")%>";
$("#applicationNo").val(a);
		
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    Transfer
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			     Transfer of Vehicle
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/vehicle.html" styleId="vehicleForm">
	 <div class="row">
	 	<div class="col-lg-12">
	 		<span>Vehicle Transfer Details for Application No: <strong><%=request.getAttribute("applicationNo")%></strong></span>
	 	</div>
	 </div>
	 <div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">Transferor Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
					<div class="form-group ">
							<div class="col-lg-2">
								<label class="control-label">Vehicle Number:</label>
						    </div>	
                         <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNumber() %></label></div> 
					</div>
					<%
	                	if(dto.getVehicleRegistrationType().equals("Personal"))
	                	{
                	%>
					   <div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">Owner Type:</label>
							</div>	
                                  <div class="col-lg-4"><label class="control-label"><%=dto.getVehicleRegistrationType()%></label></div> 
                            <div class="col-lg-2">
								<label class="control-label">Owner ID :</label>
					        </div>	
		                           <div class="col-lg-4"><label class="control-label"><%=dto.getTransferorCustomerId()%></label></div>
		                   </div>
		                 <div class="form-group">   
		                    <div class="col-lg-2">
								<label class="control-label">Owner Name :</label>
					        </div>	
		                          <div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
		                    <div class="col-lg-2">
								<label class="control-label">Citizen ID:</label>
							</div>	
                            <div class="col-lg-4"><label class="control-label"><%=dto.getCitizenID() %></label></div>
		                   </div>
		                  <div class="form-group">
			       				<div class="col-lg-2">
									<label class="control-label">Phone:</label>
								</div>
		          				<div class="col-lg-4"><label class="control-label"><%=dto.getPhone()%></label></div>
		                   </div>
		                   <%
		                   		if(dto.getIsInternational().equals("N"))
		                   		{
							%>
		                 <h4>Address (National)</h4>
		                 <div class="form-group">
		                    <div class="col-lg-2">
								 <label class="control-label">Dzongkhag:</label>
							</div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag()%></label></div> 
                            <div class="col-lg-2">
								<label class="control-label">Gewog:</label>
					        </div>	
		                    <div class="col-lg-4"><label class="control-label"><%=dto.getGewog()%></label></div>
		                 </div>
		                 <div class="form-group"> 
		                    <div class="col-lg-2">
								<<label class="control-label">Village:</label>
							</div>	
                             <div class="col-lg-4"><label class="control-label"><%=dto.getVillage()%></label></div> 
		                 </div>
		                  <%
	                   		}
	                   		else
	                   		{
		                  %>
		                  <h4>Address (Non_National)</h4> 
		                   <div class="form-group ">
			                    <div class="col-lg-2">
									<label class="control-label">Country:</label>
						        </div>	
	                           	<div class="col-lg-4"><label class="control-label"><%=dto.getCountry() %></label></div>
			                    <div class="col-lg-2">
									<label class="control-label">Address:</label>
								</div>	
	                            <div class="col-lg-4"><label class="control-label"><%=dto.getAddress()%></label></div>   
                           </div> 
                             <%
		                   		}
	                	}
	                	else
	                	{
                       %>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Owner Type</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getOwner() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Agency</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getMinistry() %></label></div>
					 		</div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Department/Name</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getName() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Address</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getAddress() %></label></div>
					 		</div>
					 		<div class="form-group">
					 			<div class="col-lg-2"><label class="control-label">Phone No</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getPhone() %></label></div>
					 			<div class="col-lg-2"><label class="control-label">Dzongkhag</label></div>
					 			<div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag() %></label></div>
					 		</div>
                       <%
	                	}  
                       %>
                           </div> 
                        </div>  
                   <div class="widget-header widget-header-small">
			          <h5 class="widget-title lighter">Registration Details</h5>
	                </div>						
		                <div class="widget-body">
			              <div class="widget-main">
                            <div class="form-group">
                              <div class="col-lg-2">
								 <label class="control-label">Last Registration Date:</label>
					           </div>	
                             	<div class="col-lg-4"><label class="control-label"><%=dto.getLastRegistrationDate() %></label></div>
		                     <div class="col-lg-2">
								<label class="control-label">Expiry Date :</label>
							 </div>	
                             <div class="col-lg-4"><label class="control-label"><%=dto.getExpiryDate() %></label></div> 
		                    </div>    
		                  </div> 
		               </div> 
		               <div class="widget-header widget-header-small">
			              <h5 class="widget-title lighter">Transferee Details</h5>
		                </div>						
		                <div class="widget-body">
			              <div class="widget-main">
		                    <div class="form-group">
                                <div class="col-lg-2">
								   <label class="control-label">Owner Name:</label>
					            </div>	
		                            <div class="col-lg-4"><label class="control-label"><%=dto.getTransfereeName() %></label></div>
		                         <div class="col-lg-2">
								     <label class="control-label">Dzongkhag :</label>
							    </div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getTransfereeDzongkhag() %></label></div> 
		                     </div>
		                    <div class="form-group">
			                 	<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
						        </div>	
		                         <div class="col-lg-4"><label class="control-label"><%=dto.getTransfereeGewog() %></label></div>
			                 	<div class="col-lg-2">
									<label class="control-label">Address:</label>
								</div>	
                                <div class="col-lg-4"><label class="control-label"><%=dto.getTransfereeAddress() %></label></div> 
                            </div> 
		                  </div> 
		                   <div class="widget-header widget-header-small">
				              <h5 class="widget-title lighter">Vehicle Transfer Details</h5>
			                </div>						
			                <div class="widget-body">
				              	<div class="widget-main">  
				              		<div class="form-group"> 
			                            <div class="col-lg-2">
											<label class="control-label">Transfer Type:</label>
								        </div>	
			                            <div class="col-lg-4"><label class="control-label"><%=dto.getTransferType() %></label></div>
			                             <div class="col-lg-2">
									           <label class="control-label">Transfer Date :</label>
						                 </div>	
			                             <div class="col-lg-4"><label class="control-label"><%=dto.getTransferDate() %></label></div>  
					                 </div>    
					                 <div class="form-group"> 
			                             <div class="col-lg-2">
											 <label class="control-label">Transfer Amount :</label>
								         </div>	
				                         <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getAmount() %></label></div>
			                              <div class="col-lg-2">
									           <label class="control-label">Penality :</label>
						                  </div>	
			                             <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getPenalty() %></label></div>  
						              </div>    
					                  <div class="form-group">
					                      <div class="col-lg-2">
											 <label class="control-label">Receipt Date:</label>
										  </div>	
		                                   <div class="col-lg-4"><label class="control-label"><%= dto.getReceiptDate() %></label></div> 
			                             <div class="col-lg-2">
											 <label class="control-label">Receipt No:</label>
								         </div>	
				                          <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo() %></label></div>
		                              </div>
					                  <div class="form-group">
					                      
			                            <div class="col-lg-2">
											<label class="control-label">Remarks:</label>
								        </div>	
			                             <div class="col-lg-4"><label class="control-label"><%=dto.getRemarks() %></label></div>
		                              </div>
		                              <div class="form-group">
					                     	<logic:equal value="APPROVE" name="param">
												<jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
											</logic:equal>
											<html:hidden property="vehicleId" value="<%=dto.getVehicleId() %>"></html:hidden>
									    </div>
				              	</div>
				            </div>
		               </div>   
		            </div>      
          </html:form>
          <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	  </div>
  <input type="hidden" id="applicationNo"/>


  <div id="displayMsgDiv"></div>
<div>
	<logic:equal value="APPROVE" name="param">
	<button type="button" class="btn btn-sm" id="approveBTN" onclick="formSubmit()">
		<i class="ace-icon fa fa-check" ></i>
		Approve
	</button>
	<button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
		<i class="ace-icon fa fa-times red2"></i>
		Reject
	</button>
	</logic:equal>
	<logic:equal value="DISPATCH" name="param">
	<button type="button" class="btn btn-sm" onclick="dispatch()">
		<i class="ace-icon fa fa-check" ></i>
		Dispatch
	</button>
	</logic:equal>
</div>
