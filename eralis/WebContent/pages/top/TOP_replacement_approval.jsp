<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
%>
<script>
function formSubmit()
{
	var application=$("#applicationNo").val();
	var regionId=$("#regionId").val();
	
	var options = {target:'#messageDiv',url:context+'/license.html?method=top_replacement_approval&applicationNo='+application+'&regionId='+regionId,type:'POST',data: $("#learnerForm").serialize()}; 

    $("#learnerForm").ajaxSubmit(options);
    $('#messageDiv').show();
    setTimeout('hideStatus("messageDiv")',4000);
    setTimeout('showTaskList()',2000);
}

function formReject()
{
	var application=$("#applicationNo").val();
	var regionId=$("#regionId").val();
	var options = {target:'#messageDiv',url:context+'/license.html?method=top_application_reject&applicationNo='+application+'&regionId='+regionId,type:'POST',data: $("#learnerForm").serialize()}; 

    $("#learnerForm").ajaxSubmit(options);
    $('#messageDiv').show();
    setTimeout('hideStatus("messageDiv")',4000);
    setTimeout('showTaskList()',2000);
}

function dispatch()
{
	var application=$("#applicationNo").val();
	var options = {target:'#messageDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=TOP&serviceType=REPLACEMENT',type:'POST',data: $("#learnerForm").serialize()}; 
    $("#learnerForm").ajaxSubmit(options);
    $('#messageDiv').show();
    setTimeout('hideStatus("messageDiv")',4000);
    setTimeout('showTaskList()',2000);
}
		
		var a="<%=request.getAttribute("applicationNo")%>";
		$("#applicationNo").val(a);
		
		var b="<%=dto.getRegionId()%>";
		$("#regionId").val(b);
				
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		    TOP Replacement Application
		     <small>
			   <i class="ace-icon fa fa-angle-double-right"></i>
			    replacement application for Taxi Operating Permit
		    </small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
	
	 <div class="widget-box">
		 <div class="widget-header widget-header-small">
			  <h5 class="widget-title lighter">TOP Replacement Application Details</h5>
		</div>						
		<div class="widget-body">
			<div class="widget-main">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">TOP Holder Name:</label>
								</div>
								<div class="col-lg-4" ><label class="control-label"><%=dto.getName()%></label></div>
							</div>
						
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">License Number:</label>
								</div>
								<div class="col-lg-4" ><label class="control-label"><%=dto.getLicenseNo()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Vehicle Number:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getVehicleNo()%></label></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">TOP Number:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getTopNo()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getRegion()%></label></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Receipt Number:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Receipt Date:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getReceiptDate()%></label></div>
							</div>	
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Amount Paid:</label>
								</div>
								<div class="col-lg-4"><label class="control-label"><%=dto.getAmount()%></label></div>
							</div>
		                </div>      
                    </div>
               </div>
        </html:form>
        <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
	 </div>
	 
	 <input type="hidden" id="applicationNo"/>
	 
	<div id="messageDiv"></div> 
<div>
	<logic:equal value="APPROVE" name="param">
		<button type="button" class="btn btn-sm" onclick="formSubmit()">
			<i class="ace-icon fa fa-check"  ></i>
			Approve
		</button>
		<!--<button type="button" class="btn btn-sm" onclick="openModal('licensemodalform')">
			<i class="ace-icon fa fa-times red2"></i>
			Reject
		</button>-->
	</logic:equal>
	<logic:equal value="DISPATCH" name="param">
		<button type="button" class="btn btn-sm" onclick="dispatch()">
			<i class="ace-icon fa fa-check"  ></i>
			Dispatch
		</button>
	</logic:equal>
</div>