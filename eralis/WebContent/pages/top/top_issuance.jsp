<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			Issuance of Taxi Operating Permit 
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>	
				apply for taxi operating permit
			</small>
		</h1>
	</div><!-- /.page-header -->
	
	<div class="row">
		<div class="col-lg-12">
			<div id="messageDiv" style="display: none;"></div>
			<div class="form-group">
				<div class="col-lg-2">
					<label>CID Number</label>
				</div>
				<div class="col-lg-4">
					<div class="input-group">
						<input type="text" class="form-control" id="cidNo"/>
						<span class="input-group-btn">
							<button type="button" class="btn btn-purple btn-sm" onclick="getTOPDtls()">
								<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
							</button>
						</span>
					</div>
				</div>
				<div class="col-lg-3">
					<label style="display:none;color: #ff0000" id="cidValidation">Please enter a CID Number</label>
				</div>
			</div>
		</div>
	</div>
	<div id="topDTSL" style="display: none;"></div>
	<br>
	
	<script type="text/javascript">
		var context = "<%=request.getContextPath()%>";
		
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
		
		function getTOPDtls()
		{  
			$('#cidValidation').hide();
			var cid	=	$("#cidNo").val();

			if(cid == "")
			{
				$('#cidValidation').show();
			}
			else
			{
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=gettopDtls&cid="+cid+"&page_id="+pageId+"&pageIdentifier="+pageIdentifier,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#topDTSL").html(responseText);
					}
				});
			}
		}
	</script>