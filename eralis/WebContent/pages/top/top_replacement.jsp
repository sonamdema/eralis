<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
  <div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Replacement
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Replacement of TOP
		</small>
	</h1>
</div>
<!-- /.page-header -->
 <div class="row">
  <div class="col-lg-12">
  <div id="Msg"></div>
	<html:form styleClass="form-horizontal" action="/license.html" styleId="topForm">
	<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
		<div class="widget-box">
		  <div class="widget-body">
			<div class="widget-main">
				<div class="form-group">
					<div class="col-lg-2">
						<label>TOP Number:<span style="color: #ff0000">*</span></label>
					</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="topNo" styleClass="form-control" styleId="topNumber" readonly="true"></html:text>
								  <span class="input-group-btn">
							        <a href="#" onclick="openModal('cancellation')" class="btn btn-purple btn-sm">
										<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
									</a>
								  </span>
							 </div>
						  </div>
						  <div class="col-lg-3">
						  	<label style="display:none;color: #ff0000" id="topNumberValidation">Please search a TOP number</label>
						  </div>
					    </div>
				     </div>
				   </div>
				</div>
	  <div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Owner & TOP Details</h5>
		</div>						
		 <div class="widget-body">
			<div class="widget-main">
                       <div class="form-group">
							<div class="col-lg-2">
								<label class=control-label>Owner Name :</label>
							</div>
		                    <div class="col-lg-3" id="ownerName"></div>
						    <div class="col-lg-2">
								<label class=control-label>CID No :</label>
							</div>
			                <div class="col-lg-3" id="cidNo"></div> 
                        </div>    
                      	<div class="form-group">
							<div class="col-lg-2">
								<label class=control-label>Phone No :</label>
							</div>
		                    <div class="col-lg-3" id="phoneNo"></div>
                        </div>   
                		<h4><u>Permanent Address</u></h4>
                     	<div class="form-group">
                       		<div class="col-lg-2">
						  		<label class=control-label>Dzongkhag :</label>
					   		</div>
		                	<div class="col-lg-3" id="displayDzongkhag"></div>
                     		<div class="col-lg-2">
								<label class=control-label> Gewog :</label>
					 		</div>
		               		<div class="col-lg-3" id="displayGewog"></div>
                       </div>
                       <div class="form-group">
                       		<div class="col-lg-2">
						  		<label class=control-label> Village :</label>
					   		</div>
		                	<div class="col-lg-3" id="displayVillage"></div>  
                       </div> 
                       <h4><u>TOP Details</u></h4> 
                       <div class="form-group">
                       		<div class="col-lg-2">
						  		<label class=control-label>Region :</label>
					   		</div>
		                	<div class="col-lg-3" id="displayRegion"></div>
                     		<div class="col-lg-2">
								<label class=control-label> Dzongkhag :</label>
					 		</div>
		               		<div class="col-lg-3" id="displayTOPDzongkhag"></div>
                       </div>
                       <div class="form-group">
                       		<div class="col-lg-2">
						  		<label class=control-label>Exact Location :</label>
					   		</div>
		                	<div class="col-lg-3" id="displayExactLocation"></div>
		                	<div class="col-lg-2">
		                		<label class="control-label">Driving License No:</label>
		                	</div>
		                	<div class="col-lg-3" id="displayLicenseNo"></div>
                       </div>
                     </div>
			       </div>
			     </div>
			
	  			<div class="widget-box">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title lighter">Vehicle Details</h5>
					</div>						
					<div class="widget-body">
						<div class="widget-main">
							<div class="form-group">
							   <div class="col-lg-2">
								   <label class=control-label> Vehicle No :</label>
							   </div>
		                       <div class="col-lg-3" id="displayVehicleNo"></div>
							   <div class="col-lg-2">
								   <label class=control-label> Company :</label>
							   </div>
		                       <div class="col-lg-3" id="displayCompany"></div>
                         	</div> 
                       		<div class="form-group">
                       			<div class="col-lg-2">
								 	<label class=control-label> Model :</label>
							    </div>
			                   <div class="col-lg-3" id="displayModel"></div> 
								<div class="col-lg-2">
									<label class=control-label> Color :</label>
								</div>
		                      	<div class="col-lg-3" id="displayColor"></div>
                         	</div>    
                        	<div class="form-group">
                        		<div class="col-lg-2">
									<label class=control-label> Chasis Number :</label>
								</div>
			                 	<div class="col-lg-3" id="displayChasisNumber"></div>
								<div class="col-lg-2">
									<label class=control-label> Engine Number :</label>
								</div>
		                      	<div class="col-lg-3" id="displayEngineNumber"></div>
                     		</div> 	
			        		<div class="form-group">
			        			<div class="col-lg-2">
									<label class=control-label> Engine Type :</label>
						 		</div>
			                 	<div class="col-lg-3" id="displayEngineType"></div> 
								<div class="col-lg-2">
									<label class=control-label> Engine CC :</label>
								</div>
		                      	<div class="col-lg-3" id="displayEngineCC"></div>
                           </div> 	
                           <div class="form-group">
                           		<div class="col-lg-2">
									<label class=control-label> Seating Capacity :</label>
						 		</div>
			                 	<div class="col-lg-3" id="displaySeatCapacity"></div> 
                           </div>
			             </div>
            	       </div>
            	    </div>
  
				<div class="row">
					<div id="displayMsgDiv"></div>
				</div>
				<div class="pull-left">
					<html:hidden property="customerId" styleId="customerID"></html:hidden>
					<html:hidden property="region" styleId="regionId"></html:hidden>
					<html:hidden property="pageId" styleId="pageId" value="<%=pageId %>"></html:hidden>
					<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
				</div>
		</html:form>
	</div>
  </div>

<div id="cancellation" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
	                    <form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Owner Type"> TOP Number : </label>
		
	                            <div class="col-sm-3">
	                                <input type="text" id="topNumberModal" placeholder="TOP Number"  />
	                             </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	                            <div class="col-sm-4">
	                                 <input type="text" id="vehicleNumberModal" placeholder="Vehicle Number"  />
	                            </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID">Citizen ID : </label>
	                            <div class="col-sm-4">
	                                 <input type="text" id="citizenIdModal" placeholder="Citizen ID"  />
	                            </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> License Number : </label>
	                            <div class="col-sm-4">
	                                 <input type="text" id="licenseNumberModal" placeholder="License Number"  />
	                            </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchTopInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			  </div>
			 <div id="topSearchTable">
		  </div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script type="text/javascript">

	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	function searchTopInfo()
	{ 
		var topNumber = $('#topNumberModal').val();
		var vehicleNumber = $('#vehicleNumberModal').val();
		var citizenId = $('#citizenIdModal').val();
		var licenseNumber = $('#licenseNumberModal').val();
	
		if(topNumber == "")
			topNumber = "NA";
		if(vehicleNumber == "")
			vehicleNumber = "NA";
		if(citizenId == "")
			citizenId = "NA";
		if(licenseNumber == "")
			licenseNumber = "NA";
	
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getTOPSearchList&topNumber="+topNumber+"&vehicleNumber="+vehicleNumber+"&citizenId="+citizenId+"&licenseNumber="+licenseNumber+"&searchType=REPLACEMENT",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#topSearchTable").html(responseText);
				$("#topSearchTable").show();
			}
		});
	}

	$(document).ready(function()
	{
		$('#submitBtn').click(function()
		{
			var topNumber = $('#topNumber').val();
			
			if(topNumber == "")
			{
				$('#topNumberValidation').show();
				$('#topNumberValidation').get(0).scrollIntoView();
				return false;
			}
			else
			{
				requestType = "TOP";
				serviceType = "REPLACEMENT";
				var identityNo = "";
				var	identityTypeId = "T"; 
				var loadingCapacity = "";
				var seatingCapacity = "";
				var vehicleHP = ""; 
				var kilowatts = "";
				var engineCC = "";
				var purchaseDate = "";
				var saleDeedAmount = "";
				var saleDeedDate = "";
				var renewalDuration = "";
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
			}
		});
	});

	function formSubmit()
	{
		var options = {target:'#displayMsgDiv',url:context+'/license.html?method=top_replacement',type:'POST',data: $("#topForm").serialize()};  
	    $("#topForm").ajaxSubmit(options);
        $('#displayMsgDiv').show();
        setTimeout('hideStatus("displayMsgDiv")',10000);
        setTimeout('reloadPage()',10000);
	}

   var context = "<%=request.getContextPath()%>";
   var pageIdentifier = "<%=pageIdentifier%>";
   var pageId = "<%=pageId%>";
   
</script>
<style>
	#topForm .error { color: red; }
 </style>
           
           
