<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
	<% String searchType = (String) request.getAttribute("SEARCH_TYPE"); %>
	<table id="top-search-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<%if(searchType.equalsIgnoreCase("TOP_DTLS")){%>
					<th>TOP Number</th>
				<%}else{%>
					<th>License Number</th>
				<%} %>
				<th>Owner Name</th>
				<th>Region</th> 
				<th>Dzongkhag</th> 
				<th>Exact Location</th> 
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="TOP_LIST">
				<logic:iterate id="top" name="TOP_LIST">
					<tr>
						<td>
						<%if(searchType.equalsIgnoreCase("TOP_DTLS")){%>
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="top" property="topNo"/>' onclick="getSelected(this.id)">SELECT</button>
						<%}else{%>
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="top" property="customerId"/>' onclick="getSelected(this.id)">SELECT</button>
						<%} %>
						</td>
						<td>
							<%if(searchType.equalsIgnoreCase("TOP_DTLS")){%>
								<bean:write name="top" property="topNo"/>
							<%}else{%>
								<bean:write name="top" property="licenseNo"/>
							<%} %>
						</td>
						<td>
							<bean:write name="top" property="name"/>
						</td>
						<td>
							<bean:write name="top" property="region"/>
						</td>
						<td>
							<bean:write name="top" property="dzongkhag"/>
						</td>
						<td>
							<bean:write name="top" property="exactLocation"/>
						</td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function() 
	{
   		$('#top-search-table').DataTable({
            responsive: true
    	});
	});

	function getSelected(globalId)
	{  	
		var searchType = '<%=searchType%>';
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getTopDtls&topNumber='+globalId+'&searchType='+searchType,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var topNo = $(this).find('topno').text();
					var name = $(this).find('name').text();
					var region = $(this).find('region').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					var exactLocation = $(this).find('exactLocation').text();
					var customerId = $(this).find('customerId').text();
					var phoneNo = $(this).find('phoneNo').text();
					var cid = $(this).find('cid').text();
					var permDzongkhag = $(this).find('permDzongkhag').text();
					var permGewog = $(this).find('permGewog').text();
					var permVillage = $(this).find('permVillage').text();
					var vehicleNo = $(this).find('vehicleNo').text();
					var vehicleCompany = $(this).find('vehicleCompany').text();
					var vehicleModel = $(this).find('vehicleModel').text();
					var vehicleColor = $(this).find('vehicleColor').text();
					var engineNo = $(this).find('engineNo').text();
					var engineCC = $(this).find('engineCC').text();
					var chassisNo = $(this).find('chassisNo').text();
					var engineType = $(this).find('engineType').text();
					var seatingCapacity = $(this).find('seatingCapacity').text();
					var regionId = $(this).find('regionId').text();
					var licenseNo = $(this).find('licenseNo').text();
					var status = $(this).find('status').text();
					var reason = $(this).find('reason').text();
					
					$('#topNumber').val(topNo);
					$('#customerID').val(customerId);
					$('#regionId').val(regionId);
					$("#drivingLicenseNo").val(licenseNo);
					$('#ownerName').html("<label class='control-label'>"+name+"</label>");
					$('#cidNo').html("<label class='control-label'>"+cid+"</label>");
					$('#phoneNo').html("<label class='control-label'>"+phoneNo+"</label>");
					$('#displayDzongkhag').html("<label class='control-label'>"+permDzongkhag+"</label>");
					$('#displayGewog').html("<label class='control-label'>"+permGewog+"</label>");
					$('#displayVillage').html("<label class='control-label'>"+permVillage+"</label>");
					$('#displayVehicleNo').html("<label class='control-label'>"+vehicleNo+"</label>");
					$('#displayCompany').html("<label class='control-label'>"+vehicleCompany+"</label>");
					$('#displayModel').html("<label class='control-label'>"+permVillage+"</label>");
					$('#displayColor').html("<label class='control-label'>"+vehicleColor+"</label>");
					$('#displayChasisNumber').html("<label class='control-label'>"+chassisNo+"</label>");
					$('#displayEngineNumber').html("<label class='control-label'>"+engineNo+"</label>");
					$('#displayEngineType').html("<label class='control-label'>"+engineType+"</label>");
					$('#displayEngineCC').html("<label class='control-label'>"+engineCC+"</label>");
					$('#displaySeatCapacity').html("<label class='control-label'>"+seatingCapacity+"</label>");
					$('#displayModel').html("<label class='control-label'>"+vehicleModel+"</label>");
					$('#displayRegion').html("<label class='control-label'>"+region+"</label>");
					$('#displayTOPDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
					$('#displayExactLocation').html("<label class='control-label'>"+exactLocation+"</label>");
					$('#displayLicenseNo').html("<label class='control-label'>"+licenseNo+"</label>");
					
					$("#submitBtn").prop('disabled', false);
					$('#Msg').hide();
					if(searchType=='TOP_HISTORY')
					{
						getListOfTop(customerId);
						getTopReplacementHistory(customerId,'Customer_Id');
					}
					else if(searchType=='REPLACEMENT')
					{
						//getTopReplacementHistory(topNumber,'Top_Number');
					}
					if(status=='PENDING_APPLICATION')
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+reason+"</div>");
						$('#Msg').show();
					}
					
	
				});
			}
		});
	}
	
	function getTopReplacementHistory(indentifer,searchBy)
	{
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getTopReplacementHistory&searchBy="+searchBy+"&indentifer="+indentifer,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			async: false,
			success : function(responseText) 
			{
				$("#topList").html(responseText);
				$("#topList").show();
			}
		});
	}
	
	
	
</script>