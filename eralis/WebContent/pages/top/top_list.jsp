<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>TOP Number</th>
			<th>Issue Date</th>
			<th>Expiry Date</th>
			<th>Amount</th>
			<th>Receipt Date</th>
			<th>Receipt No</th>
			<th>Vehicle Number</th>
			<th>Status</th>
			<th>Region</th>
			<th>Base</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="TOP_LIST">
			<logic:iterate id="topList" name="TOP_LIST">
				<tr>
					<td><bean:write name="topList" property="topNo"/></td>
					
					<td><bean:write name="topList" property="issuedate"/></td>
					<td><bean:write name="topList" property="expiryDate"/></td>
					<td><bean:write name="topList" property="amount"/></td>
					<td><bean:write name="topList" property="receiptDate"/></td>
					<td><bean:write name="topList" property="receiptNo"/></td>
					<td><bean:write name="topList" property="vehicleNo"/></td>
					<td><bean:write name="topList" property="status"/></td>
					<td><bean:write name="topList" property="region"/></td>
					<td><bean:write name="topList" property="baseoffice"/></td>
					
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
