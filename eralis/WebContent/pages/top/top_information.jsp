<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.vehicle.VehicleDTO"%>

<%
	EralisCommonDTO dto = (EralisCommonDTO)request.getAttribute("EralisCommonDTO");
	String regionId = (String)request.getAttribute("REGION_ID");
	String regionName = (String)request.getAttribute("REGION_NAME");
	VehicleDTO dto1 = (VehicleDTO)request.getAttribute("VehicleDTO");
	
	String pageId = (String) request.getAttribute("pageId");
	String pageIdentifier = (String) request.getAttribute("pageIdentifier");
%>
<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
	<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Personal Information</h5>
		</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">Name:</label>
						</div>
						<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getName()%></label></div>
						
						<div class="col-lg-2">
							<label class="control-label">Blood Group:</label>
						</div>
						<div class="col-lg-4" id="bloodGroup"><label class="control-label"><%=dto.getBloodGroup()%></label></div>
					</div>
					</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Date of Birth:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getDOB()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Gender:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getGender()%></label></div>
							</div>
					</div></div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Fathers Name:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getFathersName()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Present Address:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getAddress()%></label></div>
							</div>
					</div></div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Phone:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getPhone()%></label></div>
							</div>
					</div></div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getDzongkhag()%></label></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getGewog()%></label></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" id="name"><label class="control-label"><%=dto.getVillage()%></label></div>
						</div>
					</div></div>
			</div>
		</div>
	</div>
<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Vehicle Information</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
				<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th></th>
						<th>Vehicle Number</th>
						<th>Manufacture Year</th>
						<th>Owner Name</th>
						<th>Model</th>
						<th>Company Name</th>
						
						
					</tr>
				</thead>
				<tbody>
					<logic:notEmpty name="VEHICLE_LIST">
						<logic:iterate id="vehicle" name="VEHICLE_LIST">
							<tr>
							<td>
									<input type="radio" name="vehicleRadio" id='<bean:write name="vehicle" property="vehicleId"/>' value='<bean:write name="vehicle" property="vehicleId"/>' class="ace" onclick="fetchVehicleRenewalHistory(this.value)"/>
									<span class="lbl"></span>
							</td>
								<td><bean:write name="vehicle" property="vehicleNo"/></td>
								<td><bean:write name="vehicle" property="manufactureYear"/></td>
								<td><bean:write name="vehicle" property="ownerName"/></td> 
								<td><bean:write name="vehicle" property="model"/></td>
								<td><bean:write name="vehicle" property="company"/></td>
								
								
							</tr>
						</logic:iterate>
					</logic:notEmpty>
					<logic:empty name="VEHICLE_LIST">
						<tr>
							<td colspan="7" align="center">
								<font color='red'>NO RECORD FOUND</font>
							</td>
						</tr>
					</logic:empty>
				</tbody>
			</table>
					
			</div>
		</div>
	</div>
</div>	
	<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Driving License Renewal History</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Renewal Date</th>
					<th>Expiry Date</th>
					<th>Delivered On</th>
					
					
				</tr>
			</thead>
			<tbody>
				<logic:notEmpty name="RENEWAL_LICENSE_LIST">
					<logic:iterate id="renewal" name="RENEWAL_LICENSE_LIST">
						<tr>
							<td><bean:write name="renewal" property="renewaldate"/></td>
							<td><bean:write name="renewal" property="expiryDate"/></td>
							<td><bean:write name="renewal" property="deliveredon"/></td>
							
							
						</tr>
					</logic:iterate>
				</logic:notEmpty>
				<logic:empty name="RENEWAL_LICENSE_LIST">
					<tr>
						<td colspan="7" align="center">
							<font color='red'>NO RECORD FOUND</font>
						</td>
					</tr>
				</logic:empty>
			</tbody>
		</table>
			</div>
		</div>
	</div>
</div>
<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="widget-title lighter">Vehicle Renewal History</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div id="vehicleRenewalHistory"></div>
		</div>
	</div>
</div>

<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">TOP Details</h5>
		</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label>Region:</label>
							</div>
							<div class="col-lg-3">
								<html:hidden property="region" styleId="region" value="<%=regionId %>"></html:hidden>
								<input type="text" disabled="disabled" value="<%=regionName %>"/>
							</div>
								<div class="col-lg-2">
									<label>Dzongkhag:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="dzongkhag" styleClass="form-control" styleId="dzongkhag">
										<html:option value="">--SELECT--</html:option>
									</html:select>
								</div>
					    	</div>
					    </div>
				</div>
					    <div class="row">
							<div class="col-lg-12"> 
								<div class="form-group">
									<div class="col-lg-2">
										<label>Exact Location:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="exactLocation" styleClass="from-control" styleId="exactLocation"></html:text>
					   				</div>
					   				<div class="col-lg-2">
										<label>TOP Number:</label>
									</div>
									<div class="col-lg-3">
										<html:text property="topNo" styleClass="from-control" styleId="topNo"></html:text>
					   				</div>
					   				<div class="col-lg-2">
										<div class="checkbox">
	                                      	<label>
	                                            <html:checkbox property="topNo" styleClass="ace" styleId="autoGenerateCheck" onclick="generateTOPNo()" ></html:checkbox>
											<span class="lbl">Auto generate</span>
	                                        </label>
			                             </div>
									</div> 	
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<div id="displayMsgDiv"></div>
		<div class="pull-right">
			<html:hidden property="drivinglicenseId" styleId="drivinglicenseId" value="<%=dto.getDrivinglicenseId() %>"></html:hidden>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal>
		</div>
 		<html:hidden property="customerId" value="<%=dto.getCustomerID()%>"></html:hidden>
 		<html:hidden property="licenseNo" value="<%=dto.getLicenseNo()%>"></html:hidden>
 		<html:hidden property="vehicleNo" styleId="vehicleNo"></html:hidden>
 		<html:hidden property="vehicleId" styleId="vehicleId"/>
 		<html:hidden property="pageId" styleId="pageId"></html:hidden>
 		<html:hidden property="status" styleId="status" value="<%=dto.getStatus() %>"></html:hidden>
</html:form>
<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>
<script type="text/javascript"><!--

		var pageId = "<%=pageId%>";
		var pageIdentifier = "<%=pageIdentifier%>";

		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})

		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function() {
			$(this).prev().focus();
		});

		$(document).ready(function()
		{
			$('#submitBtn').click(function()
			{
				requestType = "TOP";
				serviceType = "NEW";
				var identityNo = "";
				var	identityTypeId = "T"; 
				var loadingCapacity = "";
				var seatingCapacity = "";
				var vehicleHP = ""; 
				var kilowatts = "";
				var engineCC = "";
				var purchaseDate = "";
				var saleDeedAmount = "";
				var saleDeedDate = "";
				var renewalDuration = "";
				
				getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration);
			});
		});
		
		function formSubmit()
		{
			$('#pageId').val(pageId);
			var selectedVehicleNo = $('input[type=radio].ace:checked').val();
			$('#vehicleId').val(selectedVehicleNo);
			var options = {target:'#displayMsgDiv',url:context+'/license.html?method=top_issuance',type:'POST',data: $("#learnerForm").serialize()}; 
			$("#learnerForm").ajaxSubmit(options);
			$('#displayMsgDiv').show();
			setTimeout('hideStatus("displayMsgDiv")',10000);
			setTimeout('reloadPage()',10000);
		}

		function fetchVehicleRenewalHistory(vehicleId)
		{
			if(vehicleId == "")
				vehicleId = "NA";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getRenewalHistoryList&vehicleId="+vehicleId,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#vehicleRenewalHistory").html(responseText);
					$("#vehicleRenewalHistory").show();
				}
			  });
		}
		
		function generateTOPNo()
		{
			$('#topNo').attr('readonly', false);
			var regionId	=	$('#region').val();
			if(document.getElementById('autoGenerateCheck').checked)
			{ 
				$.ajax
				({
						async: true,
						type: 'POST',
						url: '<%=request.getContextPath()%>/EralisCommonServlet?q=generateTOPNoFormat&regionId='+regionId,
						success: function(xml)
						{
							$(xml).find('xml-response').each(function()
							{
								var topNo = $(this).find('top-no').text();
								$('#topNo').val(topNo);
								$('#topNo').attr('readonly', true);
							});
						}
					});
				//$('#topNo').val('Auto');
				//$('#topNo').attr('readonly', true);
			}
			else
			{
				$('#topNo').val('');
			}
		}
		
		$(document).ready(function()
			{
				var regionId = $('#region').val();
				populateDependentDropDown(regionId, 'dzongkhag', '', 'DZONGKHAG_LIST', 'N');
				
				$('#topDTSL').hide();
				var status = $("#status").val();
				
				
				if(status == "TAXI_SOLD")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>Not eligible for TOP due to Taxi Sold.</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "NO_CID")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>No customer registered against this CID No.</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "NO_TAXI")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>No taxi registered for this CID </div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "NO_COMMERCIAL")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>No commercial license issued for this CID </div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "ISSUED")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>TOP is already issued against this CID</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "CANCELLED")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>TOP registered with this CID has been cancelled</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "NO_RECORD")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>No Taxi or Commercial license has been registered against this CID holder</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "PENDING_APPLICATION")
				{
					$("#messageDiv").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+'<%=dto.getReason()%>'+"</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else if(status == "APPLICATION_UNDER_PROCESS")
				{
					$("#messageDiv").html("<div class='alert alert-danger'>Application has already been submitted, you can reapply only if the application is rejected</div>");
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',5000);
				}
				else
				{
					$("#topDTSL").show();
				}
			});
</script>