	<!-- favicon -->
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@page import="bt.gov.rsta.framework.util.Constants"%>
<link rel="shortcut icon" href="<%=request.getContextPath() %>/images/favicon.ico" type="image/x-icon" />

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/fonts/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

	<!--[if lte IE 9]>
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-part2.min.css" class="ace-main-stylesheet" />
	<![endif]-->

	<!--[if lte IE 9]>
	  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-ie.min.css" />
	<![endif]-->

	<!-- ace settings handler -->
	<script src="<%=request.getContextPath() %>/js/ace-extra.min.js"></script>

	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
	<!--[if lte IE 8]>
	<script src="<%=request.getContextPath() %>/js/html5shiv.min.js"></script>
	<script src="<%=request.getContextPath() %>/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- application specific css -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/dataTables.bootstrap.css" rel="stylesheet">
  	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/dataTables.responsive.css" rel="stylesheet">
	
	
	<body>
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>
	
			<div class="navbar-container" id="navbar-container">
			
				<div class="navbar-header pull-left">
					<a class="navbar-brand">
						<small>
							<img src="<%=request.getContextPath() %>/images/eRaLIS-Logo.png" width="25px" height="25px"/>
							Online Theory Testing Lab
						</small>
					</a>
				</div>
			
			</div><!-- /.navbar-container -->
		</div>
	</body>