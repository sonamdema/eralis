<%@page import="bt.gov.rsta.eralis.dto.etest.OptionsDTO"%>
<%@page import="java.util.List"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%
		ETestDTO dto = (ETestDTO)request.getAttribute("QUESTION_DETAILS");
		List<OptionsDTO> optionsList = dto.getOptionsList();
		String nextQuestionId = dto.getQuestionId();
		
		StringBuffer firstRow = new StringBuffer();
		StringBuffer secondRow = new StringBuffer();
		
		for(int i = 0; i < optionsList.size(); i++)
		{
			if(i <= 1)
			{
				if(i == 0)
				{
					firstRow.append("<div class='row'>");
						firstRow.append("<div class='col-lg-6'>");
							firstRow.append("<div class='well well-sm'>");
								firstRow.append("<input type=\"radio\" class=\"ace\" name=\"options\" value=\""+optionsList.get(i).getOptionId()+"\"/>");
								firstRow.append("<span class=\"lbl\" id=\"answer\">&nbsp;&nbsp;&nbsp;&nbsp;"+optionsList.get(i).getOptionName()+"</span>");
							firstRow.append("</div>");
						firstRow.append("</div>");
				}
				else
				{
						firstRow.append("<div class='col-lg-6'>");
							firstRow.append("<div class='well well-sm'>");
								firstRow.append("<input type=\"radio\" class=\"ace\" name=\"options\" value=\""+optionsList.get(i).getOptionId()+"\"/>");
								firstRow.append("<span class=\"lbl\" id=\"answer\">&nbsp;&nbsp;&nbsp;&nbsp;"+optionsList.get(i).getOptionName()+"</span>");
							firstRow.append("</div>");
						firstRow.append("</div>");
					firstRow.append("</div>");
				}
			}
			else
			{
				if(i == 2)
				{
					secondRow.append("<div class='row'>");
						secondRow.append("<div class='col-lg-6'>");
							secondRow.append("<div class='well well-sm'>");
								secondRow.append("<input type=\"radio\" class=\"ace\" name=\"options\" value=\""+optionsList.get(i).getOptionId()+"\"/>");
								secondRow.append("<span class=\"lbl\" id=\"answer\">&nbsp;&nbsp;&nbsp;&nbsp;"+optionsList.get(i).getOptionName()+"</span>");
							secondRow.append("</div>");
						secondRow.append("</div>");
				}
				else
				{
						secondRow.append("<div class='col-lg-6'>");
							secondRow.append("<div class='well well-sm'>");
								secondRow.append("<input type=\"radio\" class=\"ace\" name=\"options\" value=\""+optionsList.get(i).getOptionId()+"\"/>");
								secondRow.append("<span class=\"lbl\" id=\"answer\">&nbsp;&nbsp;&nbsp;&nbsp;"+optionsList.get(i).getOptionName()+"</span>");
							secondRow.append("</div>");
						secondRow.append("</div>");
					secondRow.append("</div>");
				}
			}
		}
		
	%>
	<script type="text/javascript">
		var nextQuestionId = "<%=nextQuestionId%>";

		$(document).ready(function() 
		{
			$('#questionIdTextBox').val(nextQuestionId);

			var currentQuestionCount = $('#questionCounter').val();
			var incrementCounter = parseInt(currentQuestionCount)+1;
			$('#questionCounter').val(incrementCounter);

			$('#questionNo').html(incrementCounter);

			var questionCounterVal = $('#questionCounter').val();
			var maxQuestionsInt = parseInt(maxQuestions);
			var questionCounterValInt = parseInt(questionCounterVal);
			

			if(maxQuestionsInt == questionCounterValInt)
			{
				$('#nextBtn').hide();
				$('#submitBtn').show();
			}
			else
			{
				$('#nextBtn').show();
			}
		});
		
	</script>
<div class="row">
	<div class="col-lg-12">
		<div id="question">
			<div id="questionInner" style="top: 17px;">
				<span id="questionNo"></span>.&nbsp;<%=dto.getQuestion() %>
				<%if(dto.getPath()!=null && !dto.getPath().equalsIgnoreCase("")){%>
					<img class="editable img-responsive" id="testImg" style=" margin:8px 0px 0px 0px;max-height: 206px;" src="<%=request.getContextPath() %>/ImageServlet?url=<%=dto.getPath() %>" />
					<button type="button" class="btn btn-primary" id="reloadImgBtn" onclick="reloadImage('<%=dto.getPath() %>')" style="margin-top:3px;margin-bottom:30px;margin-left:20px;">
						<i class="fa fa-refresh"></i>&nbsp;Click to reload image
					</button>				
				<%} %>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<%=firstRow.toString() %>
		<%=secondRow.toString() %>
	</div>
</div>
<div class="row">
	<span id="errorMsg" class="alert alert-danger" style="display: none;">
	</span>
	<span class="pull-right" id="nextBtn">
		<button type="button" class="btn btn-sm btn-primary btn-white btn-round" onclick="getQuestion('NEXT')">
			<span class="bigger-110">NEXT QUESTION</span>
			<i class="icon-on-right ace-icon fa fa-arrow-right"></i>
		</button>
	</span>
	<span class="pull-right" id="submitBtn" style="display:none;">
		<button type="button" class="btn btn-sm btn-primary btn-white btn-round" onclick="submitTest()">
			<i class="ace-icon fa fa-check"></i>
			<span class="bigger-110">SUBMIT</span>
		</button>
	</span>
</div>