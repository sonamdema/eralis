<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
	ETestDTO dto = (ETestDTO)request.getAttribute("RESULT");
%>
<script>
	setTimeout($.unblockUI, 1000);
</script>
<div class="page-content">
	<!-- PAGE CONTENT BEGINS -->
	<div id="contentDisplayDiv">
		<div class="page-header">
			<h1>ce
				<i class="ace-icon fa fa-comment blue"></i> Test Summary
			</h1>
		</div><!-- /.page-header -->
		<div class="row">
			<div class="col-lg-12">
				
				<div class="profile-user-info profile-user-info-striped">
	
					<div class="profile-info-row">
						<div class="profile-info-name"> Name </div>
				
						<div class="profile-info-value">
							<span class="editable editable-click" id="username"><%=dto.getName() %></span>
						</div>
					</div>
				
					<div class="profile-info-row">
						<div class="profile-info-name"> Learner License </div>
				
						<div class="profile-info-value">
							<span class="editable editable-click" id="age"><%=dto.getLearnerNo() %></span>
						</div>
					</div>
				
					<div class="profile-info-row">
						<div class="profile-info-name"> Test Date </div>
				
						<div class="profile-info-value">
							<span class="editable editable-click" id="signup"><%=dto.getTestDate() %></span>
						</div>
					</div>
				
					<div class="profile-info-row">
						<div class="profile-info-name"> Test Location </div>
				
						<div class="profile-info-value">
							<i class="fa fa-map-marker light-orange bigger-110"></i>
							<span class="editable editable-click" id="country"><%=dto.getTestLocation() %></span>
						</div>
					</div>
					
					<div class="profile-info-row">
						<div class="profile-info-name"> Questions Attempted </div>
				
						<div class="profile-info-value">
							<span class="editable editable-click" id="login"><%=dto.getQuestionsAttempted() %></span>
						</div>
					</div>
					
					<div class="profile-info-row">
						<div class="profile-info-name"> Full Marks </div>
				
						<div class="profile-info-value">
							<span class="editable editable-click" id="country"><%=dto.getTheoryFullMarks() %></span>
						</div>
					</div>
				
					<div class="profile-info-row">
						<div class="profile-info-name"> Marks Obtained </div>
				
						<div class="profile-info-value">
							<span class="editable editable-click" id="about"><%=dto.getMarksObtained() %></span>
						</div>
					</div>
					
					<div class="profile-info-row">
						<div class="profile-info-name"> Status </div>
				
						<div class="profile-info-value">
							<span>
								<logic:equal value="P" name="RESULT" property="testStatus">
									<span class="label label-success arrowed-in arrowed-in-right">Passed</span>
								</logic:equal>
								<logic:equal value="F" name="RESULT" property="testStatus">
									<span class="label label-danger arrowed-in arrowed-in-right">Failed</span>
								</logic:equal>
							</span>
						</div>
					</div>
				</div>
				
				<div class="row" align="center">
					<a href="<%=request.getContextPath()%>/etest.html?method=exit_test" class="btn btn-sm btn-primary btn-white btn-round">
						<span class="bigger-110">EXIT TEST</span>
						<i class="icon-on-right ace-icon fa fa-power"></i>
					</a>
				</div>
				
			</div>
		</div>
	</div><!-- /.contentDisplayDiv -->
</div><!-- /.page-content -->