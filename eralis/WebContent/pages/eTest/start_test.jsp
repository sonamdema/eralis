<%@page import="java.util.HashMap"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.OptionsDTO"%>
<%@page import="java.util.List"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/etest.css" />
<script>
	<%
		ETestDTO dto = (ETestDTO)request.getAttribute("TEST_DETAILS");
		String testDuration = dto.getTestDuration();
		String questionId = dto.getQuestionId();
		String lastQuestionId = dto.getLastQuestionId();
		String maxQuestions = dto.getMaxQuestions();
	%>
	
	var questionId = "<%=questionId%>";
	var testDuration = "<%=testDuration%>";
	var lastQuestionId = "<%=lastQuestionId%>";
	var maxQuestions = "<%=maxQuestions%>";
</script>
<style>
	
	.timer-time {
	    line-height:1em;
	    font-size: 30px;
	}

	.timer-container {
	  height: 1em;
	  overflow: hidden;
	  position: relative;
	}

	.timer-box {
	  height: 1em;
	  margin: auto;
	  position: relative;
	}
	
	.timer-box > span {
	  position: relative;
	}
	
</style>
<form action="/etest.html" id="testForm">
	<div class="row" id="messageDiv" style="display: none">
	</div>
	<div class="row pull-left">
		<div class="alert alert-info">
			You will have <%=maxQuestions %>. Each question carries 2 points.
		</div>
	</div>
	<div class="row pull-right">
		<span class="h4">You Have Left: </span>
		<div class="well well-sm">
			<div class="timer-time timer-container">
	          <div class="timer-time-set timer-box" id="currentTime">
	            <span id="hoursValue">00</span>
	            <span>:</span>
	            <span id="minutesValue">00</span>
	            <span>:</span>
	            <span id="secondsValue">00</span>
	          </div>
	          <div class="timer-time-set timer-box" id="nextTime">
	            <span id="hoursNext">00</span>
	            <span>:</span>
	            <span id="minutesNext">00</span>
	            <span>:</span>
	            <span id="secondsNext">00</span>
	          </div>
	        </div>
        </div>
	</div>
	<input type="hidden" id="questionIdTextBox"/>
	<input type="hidden" id="questionCounter" value="0" name="questionCounter"/>
	<input type="hidden" id="resultTextBox" name="resultTextBox"/>
	<input type="hidden" id="name" name="name" value="<%=request.getAttribute("NAME")%>"/>
	<input type="hidden" name="learnerNo" value="<%=request.getAttribute("LEARNER_NO")%>"/>
	<input type="hidden" name="applicationNo" value="<%=request.getAttribute("APPLICATION_NO") %>"/>
	<div id="questionDisplayDiv">
	</div>
</form>

<div id="confirmation-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="blue bigger">Your time is up! Please submit your test.</h4>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-success" onclick="submitTest()">
					<i class="ace-icon fa fa-check"></i> 
					Submit
				</button>
			</div>
		</div>
	</div>
</div>
	
	<script src="<%=request.getContextPath() %>/js/pomodoro-timer.js"></script>
	
	<script>

		function disableF5(e) 
		{ 
			if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) 
				e.preventDefault(); 
		}

		$(document).ready(function() 
		{
			$(document).on("keydown", disableF5);
			 
			onPomodoroTimer(0, parseInt(testDuration), 0);
			onStartTimer();
			$('#questionIdTextBox').val(questionId);
			getQuestion('INITIAL');
		});

		function getQuestion(identifier)
		{
			var currentQuestionId = $('#questionIdTextBox').val();

			if(identifier == "NEXT")
			{
				if ($('input[type=radio].ace:checked').length == '0')
				{
				    $('#errorMsg').html("Atleast select one option");
				    $('#errorMsg').show();
				    setTimeout('hideStatus("errorMsg")',2000);
				}
				else
				{
					$.blockUI
			        ({ 
			        	css: 
			        	{ 
				            border: 'none', 
				            padding: '15px', 
				            backgroundColor: '#000', 
				            '-webkit-border-radius': '10px', 
				            '-moz-border-radius': '10px', 
				            opacity: .5, 
				            color: '#fff' 
			        	} 
			        });

					$('#nextBtn').hide();
					
					var textBoxVal = $('#resultTextBox').val();
					var selectedOptionVal = $('input[type=radio].ace:checked').val();

					if(textBoxVal == "")
						$('#resultTextBox').val(currentQuestionId+'~'+selectedOptionVal);	
					else
						$('#resultTextBox').val(textBoxVal+'#'+currentQuestionId+'~'+selectedOptionVal);				
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/etest.html?method=get_question&questionId="+currentQuestionId+"&identifier="+identifier,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html", 
						success : function(responseText) 
						{
							$("#questionDisplayDiv").html(responseText);
							$('#questionDisplayDiv').show();
							setTimeout($.unblockUI, 100); 
						}
					});
				}
			}
			else 
			{
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/etest.html?method=get_question&questionId="+currentQuestionId+"&identifier="+identifier,
					
					data : $('form').serialize(),
					cache : false,
					dataType : "html", 
					success : function(responseText) 
					{
						$("#questionDisplayDiv").html(responseText);
						$('#questionDisplayDiv').show();
					}
				});
			}
		}

		function showConfirmationDialog()
		{
			$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
		}

		function submitTest()
		{
			$.blockUI
	        ({ 
	        	css: 
	        	{ 
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px', 
		            '-moz-border-radius': '10px', 
		            opacity: .5, 
		            color: '#fff' 
	        	} 
	        });
	        $("#submitBtn").hide();
			var currentQuestionId = $('#questionIdTextBox').val();
			var textBoxVal = $('#resultTextBox').val();
			var selectedOptionVal = $('input[type=radio].ace:checked').val();
			$('#resultTextBox').val(textBoxVal+'#'+currentQuestionId+'~'+selectedOptionVal);		

			var options = {target:'#contentDisplayDiv',url:context+'/etest.html?method=process_result',type:'POST',data: $("#testForm").serialize()}; 
		    $("#testForm").ajaxSubmit(options);
	        $('#contentDisplayDiv').show();
	       
		}

		function reloadImage(imagePath){
			var timestamp = new Date().getTime();
			$('#testImg').attr("src", $('#testImg').attr('src')+'?'+timestamp);
		}

	</script>