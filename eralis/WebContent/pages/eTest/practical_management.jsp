<%@page import="bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO"%>
<%@page import="java.util.List"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%
	ETestDTO dto = (ETestDTO)request.getAttribute("TEST_DETAILS");
%>

<form id="drivingMarksheetForm" action="<%=request.getContextPath() %>/etest.html" class="form-horizontal">
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			Driving Test Marksheet
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage driving test marksheet
			</small>
		</h1>
	</div><!-- /.page-header -->
	
	<div class="row" id="messageDiv" style="display:none"></div>
	
	<div class="row" id="applicantDiv">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Applicant's Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div id="applicantMsg" style="display:none" class="alert alert-danger"></div>
						<div class="row">
						    <div class="col-lg-12">
						    	
						    	<div class="form-group">
						    		<div class="col-lg-3">
						    			<label>Learner issued region:</label>
						    		</div>
						    		<div class="col-lg-3">
										<select class="form-control" id="region" onchange="formatLearnerNo(this.value)">
											<option value="">--SELECT--</option>
											<option value="T">Thimphu</option>
											<option value="P">Phuentsholing</option>
											<option value="G">Gelephu</option>
											<option value="S">Samdrup Jongkhar</option>
											<option value="M">Mongar</option>
										</select>
						    		</div>
						    	</div>
						    	<div class="form-group">
						    		<div class="col-lg-3">
						    			<label>Learner/License No/CID:</label>
						    		</div>
						    		<div class="col-lg-3">
						    			<div class="input-group">
											<input type="text" id="learnerNo" class="form-control">
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="getLearnerTestDetails()">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>
										</div>
						    		</div>
						    	</div>
						    	<div class="form-group">
						    		<div class="col-lg-3">
						    			<label>Name of Applicant:</label>
						    		</div>
						    		<div class="col-lg-3">
						    			<input type="text" class="form-control" id="name" readonly="readonly"/>
						    		</div>
						    		<div class="col-lg-3">
						    			<label>Citizenship ID No:</label>
						    		</div>
						    		<div class="col-lg-3">
						    			<input type="text" class="form-control" id="cid" readonly="readonly"/>
						    		</div>
						    	</div>
						    	<div class="form-group">
						    		<div class="col-lg-3">
						    			<label>Drive Type:</label>
						    		</div>
						    		<div class="col-lg-3">
						    			<input type="text" class="form-control" id="driveType" readonly="readonly"/>
						    			<input type="hidden" id="type">
						    			<select style="display: none" id="cidDriveType" class="form-control" onchange="hideReversing(this.value)">
								          	<option value="">--SELECT--</option>
								          	<logic:iterate id="driveType" name="DRIVE_TYPE_LIST">
								          		<option value='<bean:write name="driveType" property="headerId"/>'>
								          			<bean:write name="driveType" property="headerName"/>
								          		</option>
								          	</logic:iterate>
							          	</select>
						    		</div>
						    	</div>
						    </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="table-responsive" id="theoryResult" style="display:none">
		<table id="dynamic-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Sl.No.</th>
					<th>Particulars</th>
					<th>Full Points</th>
					<th>Pass Points</th>
					<th>Points Scored</th>
					<th>Remarks</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Written/Oral Test</td>
					<td><%=dto.getTheoryFullMarks() %></td>
					<td><%=dto.getTheoryPassMarks() %></td>
					<td>
						<div id="theoryResultMarks"> </div>
					</td>
					<td rowspan="3">
						<div id="theoryResultRemarks"> </div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="row" id="criteriaDiv" style="display:none">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Selection criteria for obtaining Driving License (Practical Test)</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div id="criteriaMsg" style="display:none" class="alert alert-danger"></div>
						<div class="table-responsive">
							<table id="dynamic-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Sl.No.</th>
										<th>Description</th>
										<th>Full Point</th>
										<th>Points Scored</th>
									</tr>
								</thead>
								<tbody>
									<logic:notEmpty name="PRACTICAL_CRITERIA_LIST">
										<logic:iterate id="criteria" name="PRACTICAL_CRITERIA_LIST" type="bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO" indexId="index">
											<%
												int a = index.intValue();
											%>
											<tr id='CRITERIA_<%=criteria.getCriteriaId() %>'>
												<td><%=++a %></td>
												<td>
													<bean:write name="criteria" property="criteriaName"/>
												</td>
												<td align="center">
													<input type="text" class="form-control" style="width:70px" id="practicalFullMarks_<%=a%>" value='<bean:write name="criteria" property="criteriaPoint"/>' readonly="readonly">
												</td>
												<td align="center">
													<input type="number" class="form-control" style="width: 70px" id="practicalMarksObtained_<%=a%>" onchange="checkMarks(this.value, this.id)" value="5" readonly="readonly"/>
												</td>
											</tr>
										</logic:iterate>
									</logic:notEmpty>
									<tr>
										<td></td>
										<td>TOTAL</td>
										<td id="totalPointTD" align="center"><%=dto.getPracticalFullMarks() %></td>
										<td align="center">
											<input type="text" class="form-control" style="width: 70px" id="totalPracticalMarksObtained" value="50" onchange="checkFullPracticalMarks(this.value)"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-right">
				<button type="button" class="btn btn-primary btn-sm" onclick="calculatePassPercentage()">Evaluate</button>
			</div>
		</div>
	</div>
	<div class="row" id="evaluationDiv" style="display: none">
		<div id="evaluationMsg" style="display:none"></div>
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Evaluation</h5>
					<span class="pull-right">
						Pass Mark:&nbsp;<%=dto.getOverAllPassPercent()%>%
					</span>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						 
						<div class="table-responsive">
							<table id="dynamic-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Sl.No.</th>
										<th>Particulars</th>
										<th>Full Points</th>
										<th>Pass Points</th>
										<th>Points Scored</th>
										<th>Remarks</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Practical Driving Test</td>
										<td><%=dto.getPracticalFullMarks() %></td>
										<td><%=dto.getPracticalPassMarks() %></td>
										<td>
											<input type="text" readonly="readonly" id="practicalTotalMarksObtained"/>
										</td>
										<td rowspan="3">
											<div id="PASS" style="display:none;">
												<span class="label label-success arrowed-in arrowed-in-right">PASSED</span>
											</div>
											<div id="FAIL" style="display:none;">
												<span class="label label-danger arrowed-in arrowed-in-right">FAILED</span>
											</div>
										</td>
									</tr>
									<tr class="theory-mark-result">
										<td>2</td>
										<td>Written/Oral Test</td>
										<td><%=dto.getTheoryFullMarks() %></td>
										<td><%=dto.getTheoryPassMarks() %></td>
										<td>
											<input type="text" id="theoryTotalMarksObtained" onchange="calculateOverAllMarks()"/>
										</td>
									</tr>
									
									<tr>
										<td colspan="2">TOTAL POINTS</td>
										<td></td>
										<td></td>
										<td>
											<input type="text" readonly="readonly" id="totalMarksObtained"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-right">
				<input type="hidden" id="appNo"/>
				<input type="hidden" id="isViva" value="N"/>
				<button type="button" class="btn btn-primary btn-sm" onclick="back()">Back</button>
				<button type="button" class="btn btn-primary btn-sm" onclick="submitMarkSheet()">Submit</button>
			</div>
		</div>
	</div>
	
</form>
	
	<script type="text/javascript">

		<%
			List<PracticalCriteriaDTO> practicalList = (List<PracticalCriteriaDTO>)request.getAttribute("PRACTICAL_CRITERIA_LIST");
			int listSize = practicalList.size();
		%>
		var type  = "";
		var context = "<%=request.getContextPath()%>";
		var listSize = "<%=listSize%>";
		var overAllPassPercent = "<%=dto.getOverAllPassPercent()%>";
		var practicalPassMarks = "<%=dto.getPracticalPassMarks()%>";
		var theoryPassMarks = "<%=dto.getTheoryPassMarks()%>";

		function formatLearnerNo(regionCode){
			$('#learnerNo').val(regionCode+"/LL-");
		}

		function checkFullPracticalMarks(val){
			if(parseInt(val) > 60){
				$('#totalPracticalMarksObtained').val("50");
			}
		}

		function hideReversing(val){
			if(val == "4"){
				$('#CRITERIA_6').hide();
			} else {
				$('#CRITERIA_6').show();
			}
		}

		function getLearnerTestDetails()
		{
			$("#applicantMsg").hide();
			var learnerNo = $('#learnerNo').val();
			$("#theoryResult").show();
			if(learnerNo == "")
			{
				$('#applicantMsg').html("Please enter a learner/license number");
				$('#applicantMsg').show();
		        setTimeout('hideStatus("applicantMsg")',2000);
			}
			else
			{
				$.ajax
				({
					async: true,
					type: 'POST',
					url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getLearnerTestDetails&learnerNo='+learnerNo,
					success: function(xml)
					{
						$(xml).find('xml-response').each(function()
						{
							var name = $(this).find('name').text();
							var cid = $(this).find('cid').text();
							var driveType = $(this).find('drive-type').text();
							var theoryMarksObtained = $(this).find('marks-obtained').text();
							var appNo = $(this).find('app-no').text();
							var status = $(this).find('status').text();
							type = $(this).find('type').text();
							var isTest = $(this).find('isTest').text();
							var testStatus = $(this).find('testStatus').text();
							if(status == "SUCCESS")
							{
								$("#theoryResultMarks").html(theoryMarksObtained);
								if(testStatus=='F')
								{
									$("#theoryResultRemarks").html("FAILED");
								}
								else if(testStatus=='P')
								{
									$("#theoryResultRemarks").html("PASSED");
								}
								
								if(isTest=='Y' && testStatus=='F')
								{
									$("#criteriaDiv").hide();
								}
								else{
									if(isTest=='Y')
									{
										$("#theoryTotalMarksObtained").attr('readonly', true);
									}
									else
									{
										$("#theoryTotalMarksObtained").attr('readonly', false);
									}
									$("#criteriaDiv").show();
								}
								
								$('#name').val(name);
								$('#cid').val(cid);
								$('#driveType').val(driveType);
								$('#appNo').val(appNo);
								if(type=='CID')
								{
									$("#type").val(type);
									$('#cidDriveType').show();
									$('#driveType').hide();
								}
								else
								{
									if(type=='ENDORSEMENT')
									{
										$("#theoryResult").hide();
										$(".theory-mark-result").hide();
										
									}
									else if(type == 'NEW')
									{
										$(".theory-mark-result").show();
										$("#theoryResult").show();
									}
									$('#cidDriveType').hide();
									$('#driveType').show();

									var driveTypeVal = $('#driveType').val();
									if(driveTypeVal == "Two Wheeler"){
										$('#CRITERIA_6').hide();
									} else {
										$('#CRITERIA_6').show();
									}
								}								
									$('#isViva').val("Y");	
								
								$('#theoryTotalMarksObtained').val(theoryMarksObtained);
							}
							/*else if(status == "THEORY_FAILED")
							{
								$('#applicantMsg').html("This learner has failed in theory test");
								$('#applicantMsg').show();
						        setTimeout('hideStatus("applicantMsg")',2000);
							}*/
							else if(status == "LICENSE_DOESNOT_EXIST")
							{
								$('#applicantMsg').html("Learner/license number doesn't exist in the Test list");
								$('#applicantMsg').show();
						        setTimeout('hideStatus("applicantMsg")',2000);
						        $("#theoryResult").hide();
							}
							else if(status == "FAILURE")
							{
								$('#applicantMsg').html("Something went wrong, please try again");
								$('#applicantMsg').show();
						        setTimeout('hideStatus("applicantMsg")',5000);
							}
						});
					}
				});
			}
		}

		function enableTheoryMarksObtained()
		{
			$('#theoryTotalMarksObtained').attr("readyonly", false);
		}

		function checkMarks(val, id)
		{
			var pattern=/[0-9]+/;
			var no = id.match(pattern);
			var fullPoint = $('#practicalFullMarks_'+no).val();

			if(parseInt(val) > parseInt(fullPoint))
			{
				$('#criteriaMsg').html("Marks obtained cannot be greater then full marks");
				$('#criteriaMsg').show();
		        setTimeout('hideStatus("criteriaMsg")',2000);
		        $('#'+id).val("");
			}
			else
			{
				var marksObtained = 0;
				for(var i=1; i<=parseInt(listSize); i++)
				{
					marksObtained += parseInt($('#practicalMarksObtained_'+i).val());
				}

				$('#totalPracticalMarksObtained').val(marksObtained);
			}
		}

		function back()
		{
			$('#applicantDiv').show();
			$('#criteriaDiv').show();
			$('#evaluationDiv').hide();
		}

		function calculateOverAllMarks()
		{
			$('#totalMarksObtained').val("0");
			var totalTheoryMarksObtained = $('#theoryTotalMarksObtained').val();
			var totalPracticalMarksObtained = $('#practicalTotalMarksObtained').val();
			var overallTotal = parseInt(totalPracticalMarksObtained)+parseInt(totalTheoryMarksObtained);
			$('#totalMarksObtained').val(overallTotal);

			if(overallTotal >= parseInt(overAllPassPercent))
			{
				$('#PASS').show();
				$('#FAIL').hide();
			}
			else 
			{
				$('#FAIL').show();
				$('#PASS').hide();
			}
		}

		function calculatePassPercentage()
		{
			$("#theoryResult").hide();
			$('#applicantDiv').hide();
			$('#criteriaDiv').hide();
			$('#evaluationDiv').show();

			$('#practicalTotalMarksObtained').val($('#totalPracticalMarksObtained').val());
			var totalPracticalMarksObtained = $('#practicalTotalMarksObtained').val();
			var totalTheoryMarksObtained = $('#theoryTotalMarksObtained').val();
			var overallTotal = parseInt(totalPracticalMarksObtained)+parseInt(totalTheoryMarksObtained);
			$('#totalMarksObtained').val(overallTotal);

			if(type == 'NEW')
			{
				if(overallTotal >= parseInt(overAllPassPercent))
				{
					$('#PASS').show();
					$('#FAIL').hide();
				}
				else 
				{
					$('#FAIL').show();
					$('#PASS').hide();
				}
				
			}
			else if (type == 'ENDORSEMENT')
			{
				if(overallTotal >= parseInt(practicalPassMarks))
				{
					$('#PASS').show();
					$('#FAIL').hide();
				}
				else 
				{
					$('#FAIL').show();
					$('#PASS').hide();
				}
				
			}
			
			
		}

		function submitMarkSheet()
		{
			var practicalTestStatus = "F";
			var theoryTestStatus = "F";
			var learnerNo = $('#learnerNo').val();
			var theoryMarksObtained = $('#theoryTotalMarksObtained').val();
			var practicalMarksObtained = $('#practicalTotalMarksObtained').val();
			var overAllMarksObtained = $('#totalMarksObtained').val();
			var driveType = $('#cidDriveType').val();
			var type = $('#type').val();
			
			var appNo = $('#appNo').val();
			var isViva = $('#isViva').val();

			if(parseInt(practicalMarksObtained) >= parseInt(practicalPassMarks))
				practicalTestStatus = "P";
			if(parseInt(theoryMarksObtained) >= parseInt(theoryPassMarks))
				theoryTestStatus = "P";
			
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/etest.html?method=submit_marksheet&learnerNo="+learnerNo+"&practicalMarksObtained="+practicalMarksObtained+"&overAllMarksObtained="+overAllMarksObtained+"&appNo="+appNo+"&practicalTestStatus="+practicalTestStatus+"&theoryMarksObtained="+theoryMarksObtained+"&isViva="+isViva+"&theoryTestStatus="+theoryTestStatus+"&type="+type+"&driveType="+driveType,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#evaluationMsg").html(responseText);
					$("#evaluationMsg").show();
					setTimeout('hideStatus("evaluationMsg")',2000);
					setTimeout('reloadPage()',2000);
				}
			});
		}
		
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
		
	</script>