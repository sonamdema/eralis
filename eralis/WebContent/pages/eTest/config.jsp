<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<style>
	.datepicker{z-index:1151 !important;}
</style>

	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			Configure e-Test 
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; configure e-Test module
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h4 class="widget-title"></h4>
					<logic:equal value="Y" name="priviledge" property="isNew">
						<span class="widget-toolbar">
							<a href="#add-test-modal" title="Add eTest" data-toggle="modal">
								<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
							</a>
						</span>
					</logic:equal>
				</div>
				<div class="widget-body">
					<div id="messageDiv" style="display:none"></div>
					<div class="widget-main">
					    <div class="table-responsive">
							<table id="dynamic-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Test Date</th>
										<th>Test Duration (Minutes)</th>
										<th>Max Applicants</th>
										<th>Test Location</th>
										<th>Status</th>
										<th>Test Type</th>
										<th>Institute Name</th>
										<th></th>
									</tr>
								</thead>
								
								<tbody>
									<logic:notEmpty name="etestDtls">
										<logic:iterate id="etest" name="etestDtls" type=" bt.gov.rsta.eralis.dto.etest.ETestDTO" indexId="index">
											<tr>
												<td>
													<bean:write name="etest" property="testDate"/>
												</td>
												<td>
													<bean:write name="etest" property="testDuration"/>
												</td>
												<td>
													<bean:write name="etest" property="maxApplicants"/>
												</td>
												<td>
													<bean:write name="etest" property="testLocation"/>
												</td>
												<logic:equal value="Y" name="etest" property="isActive">
													<td>
														<span class="label label-sm label-success">Active</span>
													</td>
												</logic:equal>
												<logic:equal value="N" name="etest" property="isActive">
													<td>
														<span class="label label-sm label-warning">Inactive</span>
													</td>
												</logic:equal>
												<td>
													<bean:write name="etest" property="testType"/>
												</td>
												<td>
													<bean:write name="etest" property="instituteName"/>
												</td>
												<td align="center">
												
													<div class="hidden-sm hidden-xs action-buttons">
														<logic:equal value="Y" name="priviledge" property="isEdit">
															<a class="green" href="#" onclick="populateEditForm('<bean:write name="etest" property="slNo"/>','<bean:write name="etest" property="testDate"/>','<bean:write name="etest" property="maxApplicants"/>','<bean:write name="etest" property="isActive"/>','<bean:write name="etest" property="testDuration"/>','<bean:write name="etest" property="testType"/>','<bean:write name="etest" property="instituteName"/>')">
																<i class="ace-icon fa fa-pencil bigger-130"></i>
															</a>
														</logic:equal>
													</div>
													
													<div class="hidden-md hidden-lg">
														<div class="inline pos-rel">
															<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
															</button>
			
															<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																
																<logic:equal value="Y" name="priviledge" property="isEdit">
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="etest" property="slNo"/>','<bean:write name="etest" property="testDate"/>','<bean:write name="etest" property="maxApplicants"/>','<bean:write name="etest" property="isActive"/>','<bean:write name="etest" property="testDuration"/>','<bean:write name="etest" property="testType"/>','<bean:write name="etest" property="instituteName"/>')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																</logic:equal>
			
															</ul>
														</div>
													</div>
													
												</td>
											</tr>
										</logic:iterate>
									</logic:notEmpty>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<html:form action="/etest.html" method="post" styleId="testAddForm">
		<div id="add-test-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Test Details</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div id="addSubmitMsgDiv" style="display: none"></div>
									<div class="form-group">
										<label>Test Date:</label>
										<span class="pull-right">
											<input class="form-control date-picker" id="testDateVal" type="text" data-date-format="dd-mm-yyyy" style="width: 200px" readonly="readonly"/>
										</span>
									</div>
									<div class="form-group">
										<label>Test Duration (in minutes):</label>
										<span class="pull-right">
											<html:text property="testDuration" styleId="testDuration" maxlength="2" style="width: 50px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Max Applicants:</label>
										<span class="pull-right">
											<html:text property="maxApplicants" styleClass="form-control" styleId="maxApplicants" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Test Type:</label>
										<span class="pull-right">
											<html:select property="testType" styleClass="col-xs-12" styleId="testType">
											   <html:option value="">--SELECT--</html:option>
											   <html:option value="ORDINARY">Ordinary</html:option>
											   <html:option value="DRIVING_INSTITUTE">Institute</html:option>
										   </html:select>
										</span>
									</div>
									<div class="form-group" id="instituteName">
										<label>Name of Institute:</label>
										<span class="pull-right">
											<html:text property="instituteName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Is Active:</label>
										<span class="pull-right">
											<label>
												<html:checkbox property="isActive" styleClass="ace" styleId="addIsActive"></html:checkbox>
												<span class="lbl"></span>
											</label>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<html:hidden property="addTestDate" styleId="addTestDate"></html:hidden>
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
			
						<button type="button" class="btn btn-sm btn-primary" onclick="addTestDtls()">
							<i class="ace-icon fa fa-check"></i> 
							Add
						</button>
					</div>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
	</html:form>
	
	<html:form action="/etest.html" method="post" styleId="testEditForm">
		<div id="edit-test-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Test Details</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div id="editSubmitMsgDiv" style="display: none"></div>
									<div class="form-group">
										<label>Test Date:</label>
										<span class="pull-right">
											<input class="form-control date-picker" id="editTestDateVal" type="text" data-date-format="dd-mm-yyyy" style="width: 200px" readonly="readonly"/>
										</span>
									</div>
									<div class="form-group">
										<label>Test Duration (in minutes):</label>
										<span class="pull-right">
											<html:text property="testDuration" styleId="editTestDuration" maxlength="2" style="width: 50px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Max Applicants:</label>
										<span class="pull-right">
											<html:text property="maxApplicants" styleClass="form-control" styleId="editMaxApplicants" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Test Type:</label>
										<span class="pull-right">
											<html:select property="testType" styleClass="col-xs-12" styleId="editTestType">
											   <html:option value="">--SELECT--</html:option>
											   <html:option value="ORDINARY">Ordinary</html:option>
											   <html:option value="DRIVING_INSTITUTE">Institute</html:option>
										   </html:select>
										</span>
									</div>
									<div class="form-group" id="editInstituteName">
										<label>Name of Institute:</label>
										<span class="pull-right">
											<html:text property="instituteName" styleClass="form-control" styleId="editInstitute"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Is Active:</label>
										<span class="pull-right">
											<label>
												<html:checkbox property="isActive" styleClass="ace" styleId="editIsActive"></html:checkbox>
												<span class="lbl"></span>
											</label>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<html:hidden property="slNo" styleClass="form-control" styleId="editSlNo"></html:hidden>
						<html:hidden property="testDate" styleId="editTestDate"></html:hidden>
					
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
			
						<button type="button" class="btn btn-sm btn-primary" onclick="editTestDtls()">
							<i class="ace-icon fa fa-check"></i> 
							Update
						</button>
					</div>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
	</html:form>
	
	<script type="text/javascript">

		var context = "<%=request.getContextPath()%>";

		$(document).ready(function() 
		{
			
			$("#testType").change(function(event){
				$("#instituteName").hide();
				var testType	=	$("#testType").val();
				if(testType=='DRIVING_INSTITUTE'){
					$("#instituteName").show();
				}
			});
			$("#editTestType").change(function(event){
				$("#editInstituteName").hide();
				var testType	=	$("#editTestType").val();
				if(testType=='DRIVING_INSTITUTE'){
					$("#editInstituteName").show();
				}
			});
		});
		
		$(document).ready(function() 
		{
		    $('#dynamic-table').DataTable({
		            responsive: true
		    });
  
		    $('.date-picker').datepicker({
				autoclose: true,
				todayHighlight: true
			});

		    /********************************/
			//add tooltip for small view action buttons in dropdown menu
			$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
			
			//tooltip placement on right or left
			function tooltip_placement(context, source) {
				var $source = $(source);
				var $parent = $source.closest('table');
				var off1 = $parent.offset();
				var w1 = $parent.width();
		
				var off2 = $source.offset();
				//var w2 = $source.width();
		
				if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
				return 'left';
			}
		});

		function editTestDtls(loginId)
		{
			$('#editTestDate').val($('#editTestDateVal').val());
			var options = {target:'#editSubmitMsgDiv',url:context+'/etest.html?method=edit_test_details',type:'POST',data: $("#testEditForm").serialize()}; 
		    $("#testEditForm").ajaxSubmit(options);
	        $('#editSubmitMsgDiv').show();
	        setTimeout('hideStatus("editSubmitMsgDiv")',2000);
	        setTimeout('reloadPage()',2000);
		}

		function addTestDtls(loginId)
		{
			$('#addTestDate').val($('#testDateVal').val());
			var options = {target:'#editSubmitMsgDiv',url:context+'/etest.html?method=add_test_details',type:'POST',data: $("#testAddForm").serialize()}; 
		    $("#testAddForm").ajaxSubmit(options);
	        $('#addSubmitMsgDiv').show();
	        setTimeout('hideStatus("addSubmitMsgDiv")',2000);
	        setTimeout('reloadPage()',2000);
		}

		function populateEditForm(slNo, testDate, maxApplicants, isActive, testDuration, testType,instituteName)
		{ 
			$('#editSlNo').val(slNo);
			$('#editTestDateVal').val(testDate);
			$('#editMaxApplicants').val(maxApplicants);
			$('#editTestDuration').val(testDuration);
			$('#editTestType').val(testType);
			$('#editInstitute').val(instituteName);

			$("#editInstituteName").hide();
			if(testType=='DRIVING_INSTITUTE'){
				$("#editInstituteName").show();
			}
			
			if(isActive == "Y")
				$('#editIsActive').attr('checked',true);

			$('#edit-test-modal').modal('show');
		}

		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
		
	</script>