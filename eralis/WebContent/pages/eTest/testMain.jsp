<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Home - eRaLIS</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
</head>
<body class="no-skin">
		<!-- header include -->
		<jsp:include page="/pages/eTest/test_header.jsp"></jsp:include>
		<!-- ./header include -->

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<div class="nav-search" id="nav-search">
							<i class="ace-icon fa fa-calendar fa-lg"></i> 
							<%
							 	String DATE_FORMAT_NOW = "dd-MM-yyyy";
								Calendar cal = Calendar.getInstance();

								SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
							    String day = sdfDay.format(cal.getTime());
							    
							    SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
							    String year = sdfYear.format(cal.getTime());
							    
							    Calendar mCalendar = Calendar.getInstance();    
							    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
							%>
							<span class="blue"><%=day%>&nbsp;<%=month %>,&nbsp;<%=year %></span>
						</div>
					</div>

					<div class="page-content">
						<!-- PAGE CONTENT BEGINS -->
						<div id="contentDisplayDiv">
							<div class="page-header">
								<h1>
									<i class="ace-icon fa fa-comment blue"></i> Welcome&nbsp;<%=request.getAttribute("NAME") %>
								</h1>
							</div><!-- /.page-header -->
							<div class="row">
								<div class="col-lg-12">
									<div class="widget-box transparent">
										<div class="widget-header widget-header-flat">
											<h4 class="widget-title lighter">
												<i class="ace-icon fa fa-star orange"></i>
												Test Modules
											</h4>
										</div>
										<div class="widget-body">
											<div class="widget-main no-padding">
												<table class="table table-bordered table-striped">
													<thead class="thin-border-bottom">
														<tr>
															<th align="center">
																Test Name
															</th>
															<th align="center">
																Status
															</th>
															<th align="center">
																Action
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Driving License Theory Test</td>
															<td>
																<span class="label label-success arrowed-in arrowed-in-right">Active</span>
															</td>
															<td align="center">
																<button class="btn btn-app btn-success btn-xs" onclick="startTest()">
																	<i class="ace-icon fa fa-clock-o bigger-100"></i>
																	Start
																</button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div><!-- /.contentDisplayDiv -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<!-- footer include -->
			<jsp:include page="/pages/common/footer.jsp"></jsp:include>
			<!-- /.footer include -->
			
		</div><!-- /.main-container -->
		<script>
			var context = "<%=request.getContextPath()%>";
			<%
				String learnerNo = (String) request.getAttribute("LEARNER_NO");
				String name = (String)request.getAttribute("NAME");
				String applicationNo = (String)request.getAttribute("APPLICATION_NO");
				request.setAttribute("NAME", name);
			%>

			var learnerNo = "<%=learnerNo%>";
			var name = "<%=name%>";
			var applicationNo = "<%=applicationNo%>";
			
			function startTest()
			{
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/etest.html?method=start_test&learnerNo="+learnerNo+"&name="+name+"&appNo="+applicationNo,
					
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#contentDisplayDiv").html(responseText);
						$('#contentDisplayDiv').show();
					}
				});
			}

		</script>
		
	</body>
</html>