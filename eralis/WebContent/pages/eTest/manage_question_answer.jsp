<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/etest.css" />
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			Manage Questions and Answers for Test
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; configure questions and answers for e-Test
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h4 class="widget-title" id="title"></h4>
					<logic:equal value="Y" name="priviledge" property="isNew">
						<span class="widget-toolbar">
							<a href="#" title="Add New Questions" onclick="showForm('ADD')">
								<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
							</a>
						</span>
					</logic:equal>
				</div>
				<div class="widget-body">
					<div id="messageDiv" style="display:none"></div>
					<div class="widget-main">
						
						<div id="tableDiv">
							<div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Questions</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<logic:notEmpty name="questionList">
											<logic:iterate id="question" name="questionList" type="bt.gov.rsta.framework.dto.DropDownDTO" indexId="index">
												<%
													int i = index.intValue();
												%>
												<tr>
													<td><%=++i %></td>
													<td>
														<bean:write name="question" property="headerName"/>
													</td>
													<td align="center">
														
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="question" property="headerId"/>')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="confirmDelete('<bean:write name="question" property="headerId"/>')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<logic:equal value="Y" name="priviledge" property="isEdit">
																		<li>
																			<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="question" property="headerId"/>')">
																				<span class="green">
																					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</logic:equal>
																	<logic:equal value="Y" name="priviledge" property="isDelete">
																		<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" id="bootbox-confirm" onclick="confirmDelete('<bean:write name="question" property="headerId"/>')">
																				<span class="red">
																					<i class="ace-icon fa fa-trash-o bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</logic:equal>
				
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					
						<div id="addFormDiv" style="display: none">
						    <html:form styleId="addForm" action="/etest.html" styleClass="form-horizontal">
						    	<div class="row">
						    		<div class="col-lg-12">
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Question</label>
								    		</div>
								    		<div class="col-lg-9">
								    			<html:textarea property="question" styleClass="form-control" styleId="question"></html:textarea>
								    		</div>
								    	</div>
								    	<div class="form-group">
											<div class="col-lg-3">
												<label>Picture Question:</label>
											</div>
											<div class="col-lg-4">
												<html:file property="upload" styleClass="form-control" styleId="id-input-file-2"></html:file>
											</div>
										</div>
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Option 1</label>
								    		</div>
								    		<div class="col-lg-7">
								    			<input type="text" class="form-control" id="answer" name="options"/>
								    		</div>
								    		<div class="col-lg-2">
								    			<html:radio property="isAnswer" value="0" styleClass="ace"></html:radio>
								    			<span class="lbl"></span>
								    		</div>
								    	</div>
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Option 2</label>
								    		</div>
								    		<div class="col-lg-7">
								    			<input type="text" class="form-control" id="answer" name="options"/>
								    		</div>
								    		<div class="col-lg-2">
								    			<html:radio property="isAnswer" value="1" styleClass="ace"></html:radio>
								    			<span class="lbl"></span>
								    		</div>
								    	</div>
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Option 3</label>
								    		</div>
								    		<div class="col-lg-7">
								    			<input type="text" class="form-control" id="answer" name="options"/>
								    		</div>
								    		<div class="col-lg-2">
								    			<html:radio property="isAnswer" value="2" styleClass="ace"></html:radio>
								    			<span class="lbl"></span>
								    		</div>
								    	</div>
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Option 4</label>
								    		</div>
								    		<div class="col-lg-7">
								    			<input type="text" class="form-control" id="answer" name="options"/>
								    		</div>
								    		<div class="col-lg-2">
								    			<html:radio property="isAnswer" value="3" styleClass="ace"></html:radio>
								    			<span class="lbl"></span>
								    		</div>
								    	</div>
								    	<div class="form-group">
								    		<div class="col-lg-12">
								    			<span class="pull-right">
										    		<button type="button" class="btn btn-sm btn-primary" onclick="addQuestions()">
														<i class="ace-icon fa fa-check"></i> 
														Add
													</button>
												</span>
											</div>
								    	</div>
								    </div>
						    	</div>
						    </html:form>
					    </div>
					    
					    <div id="editFormDiv" style="display: none">
					    </div>
					    
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="delete-question-modal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Are you sure you want to delete the selected item?</h4>
				</div>
	
				<div class="modal-footer">
					<input type="hidden" id="deleteQuestionId"/>
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						No
					</button>
	
					<button type="button" class="btn btn-sm btn-danger" onclick="deleteQuestion()">
						<i class="ace-icon fa fa-trash"></i> 
						Yes
					</button>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">

		var context = "<%=request.getContextPath()%>";
		
		$(document).ready(function() 
		{
		    $('#dynamic-table').DataTable({
		            responsive: true
		    });

		    $('#title').html("List of Questions");

		});

		function addQuestions()
		{
			var options = {target:'#messageDiv',url:context+'/etest.html?method=add_questions',type:'POST',data: $("#addForm").serialize()}; 
		    $("#addForm").ajaxSubmit(options);
	        $('#messageDiv').show();
	        setTimeout('hideStatus("messageDiv")',2000);
	        setTimeout('reloadPage()',4000);
		}

		function editQuestion()
		{
			var options = {target:'#messageDiv',url:context+'/etest.html?method=edit_question',type:'POST',data: $("#editForm").serialize()}; 
		    $("#editForm").ajaxSubmit(options);
	        $('#messageDiv').show();
	        setTimeout('hideStatus("messageDiv")',2000);
	        setTimeout('reloadPage()',4000);
		}

		function showForm(identifier)
		{
			if(identifier == "ADD")
			{
				 $('#title').html("Add New Question Details"); 
				$('#addFormDiv').show();
				$('#editFormDiv').hide();
				$('#tableDiv').hide();
			}
		}

		function confirmDelete(questionId)
		{
			$('#deleteQuestionId').val(questionId);
			$('#delete-question-modal').modal('show');
		}

		function populateEditForm(questionId)
		{
			 $('#title').html("Edit Question Details");
			 
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/etest.html?method=get_edit_dtls&questionId="+questionId,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#editFormDiv").html(responseText);
					$('#editFormDiv').show();
					$('#addFormDiv').hide();
					$('#tableDiv').hide();
				}
			});
		}

		function deleteQuestion()
		{
			$('#delete-question-modal').modal('hide');
			var questionId = $('#deleteQuestionId').val();

			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/etest.html?method=delete_question&questionId="+questionId,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#messageDiv").html(responseText);
					$('#messageDiv').show();
			        setTimeout('hideStatus("messageDiv")',2000);
			        setTimeout('reloadPage()',4000);
				}
			});
		}

		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
		
	</script>