<%@page import="bt.gov.rsta.eralis.dto.etest.OptionsDTO"%>
<%@page import="java.util.List"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%

	ETestDTO dto = (ETestDTO) request.getAttribute("editDtls");
	String question = dto.getQuestion();
	String isAnswer = dto.getIsAnswer();
	String imagePath = dto.getPath();
	List<OptionsDTO> optionsList = dto.getOptionsList();
	request.setAttribute("optionList", optionsList);
	
	String questionId = (String)request.getAttribute("questionId");
	
%>
<html:form styleId="editForm" action="/etest.html" styleClass="form-horizontal">
   	<div class="row">
   		<div class="col-lg-12">
   			<logic:notEmpty name="editDtls">
	   			<div class="form-group">
		    		<div class="col-lg-3">
		    			<label>Question</label>
		    		</div>
		    		<div class="col-lg-9">
		    			<html:textarea property="question" styleClass="form-control" styleId="question" value="<%=question %>"></html:textarea>
		    		</div>
		    	</div>
		    	<div class="form-group">
					<div class="col-lg-3">
						<label>Picture Question:</label>
					</div>
					<div class="col-lg-4">
						<img class="editable img-responsive" id="avatar2" src="<%=request.getContextPath() %>/ImageServlet?url=<%=imagePath %>" />
						<html:file property="upload" styleClass="form-control" styleId="id-input-file-2"></html:file>
					</div>
				</div>
		    	<logic:iterate id="option" name="optionList" type="bt.gov.rsta.eralis.dto.etest.OptionsDTO" indexId="index">
		    		<%
		    			int a = index.intValue();
		    		%>
		    		<div class="form-group">
			    		<div class="col-lg-3">
			    			<label>Option</label>
			    		</div>
			    		<div class="col-lg-7">
			    			<input type="text" class="form-control" id="answer" name="options" value="<%=option.getOptionName()%>"/>
			    		</div>
			    		<div class="col-lg-2">
			    			<html:radio property="isAnswer" value="<%=Integer.toString(a) %>" styleClass="ace" styleId='<%="isAnswer_"+a %>'></html:radio>
			    			<span class="lbl"></span>
			    			<logic:equal value="<%=isAnswer %>" name="option" property="optionId">
				    			<script type="text/javascript">
			  		                  $("#isAnswer_<%=a%>").attr('checked', 'checked');
			                    </script>
	                    	</logic:equal>
			    		</div>
			    	</div>
		    	</logic:iterate>
		    	<div class="form-group">
		    		<div class="col-lg-12">
		    			<span class="pull-right">
		    				<html:hidden property="questionId" value="<%=questionId %>"/>
				    		<button type="button" class="btn btn-sm btn-primary" onclick="editQuestion()">
								<i class="ace-icon fa fa-check"></i> 
								Update
							</button>
						</span>
					</div>
		    	</div>
   			</logic:notEmpty>
	    </div>
   	</div>
   </html:form>