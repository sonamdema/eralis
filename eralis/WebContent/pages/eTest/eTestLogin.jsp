<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
    <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
	<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login - eTest</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- favicon -->
		<link rel="shortcut icon" href="<%=request.getContextPath() %>/images/favicon.ico" type="image/x-icon" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/fonts/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/ace-ie.min.css" />
		<![endif]-->
		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

		<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="<%=request.getContextPath() %>/js/html5shiv.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/respond.min.js"></script>
		<![endif]-->
		
</head>
<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<span class="green">Electronic Theory Test</span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; Road Safety and Transport Authority</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-lock green"></i>
												Please Enter Your Information
											</h4>
											<div class="space-6"></div>
											<form action="<%=request.getContextPath() %>/Authorize" method="POST" onsubmit="return validateForm()">
												<div class="alert alert-danger" id="errorMsg" style="display:none"></div>
												<div class="alert alert-success" id="successMsg" style="display:none"></div>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Learner License No" id="learnerNo" name="learnerNo" autocomplete="off"/>
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input class="form-control date-picker" id="dob" type="text" data-date-format="dd-mm-yyyy" placeholder="Date of Birth" autocomplete="off" name="dob"/>
															<i class="ace-icon fa fa-calendar"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->
										<div class="toolbar clearfix">
											<div>
											</div>
											 <div align="center">
												<a href="#" style="color: white;" data-toggle="modal" data-target="#instruction-modal">
													<i class="ace-icon fa fa-info-circle"></i>
													Read Instructions
												</a>
											</div>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
		</div><!-- /.main-container -->
        </div>
        
        <div id="instruction-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="blue bigger">
							<i class="ace-icon fa fa-info-circle"></i>
							&nbsp;Electronic Theory Test Instructions
						</h4>
					</div>
					<div class="modal-body">
						<ul>
							<li> Use the learner license number and your date of birth to 
								login as reflected in the learner license book. (Learner License no format  T/LL-12345)</li>
							<br/>
							<li> Click the "Start" button to start the test: the timer count down will 
								begin and test must be completed within 30 minutes.</li>
							<br/>
							<li>You must answer all the 20 questions and must obtain minimum of 24 marks,
							 which means at least 12 questions must be answered correctly.</li>
						 	<br/>
							<li>Answers cannot be re-corrected once answered.</li>
							<br/>
							<li>Once you finish the test, on clicking the submit button, 
								you will see your marks. Inform the RSTA officials on duty  
								once you submit the test.</li>
							<br/>
							<li>Once the RSTA Officials give their remarks on your learner 
								license booklet, please click on "Exit Test".</li>
							<br/>
							<li> You cannot re-attempt the test if you fail. You need to register for test, next time.</li>
						</ul>
					</div>
					<div class="modal-footer">
						<button class="btn btn-sm btn-success" data-dismiss="modal">
							<i class="ace-icon fa fa-check"></i>
							Done
						</button>
					</div>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
        
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script src="<%=request.getContextPath() %>/js/jquery.1.11.1.min.js"></script>
		<![endif]-->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='<%=request.getContextPath() %>/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath() %>/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/commonUtil.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">

			$('.date-picker').datepicker({
				autoclose: true,
				todayHighlight: true
			});
			
			$('document').ready(function()
			{
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
			});
			
			jQuery(function($) {
			 $('#btn-login').on('click', function(e) {
				$(location).attr('href', '<%=request.getContextPath()%>/pages/common/main.jsp');
				e.preventDefault();
			 });
			});

		</script>
		
		<script>

			<%
				String msg = null;
				if(request.getAttribute("MESSAGE") != null)
					msg = (String)request.getAttribute("MESSAGE");
			%>

			var msg = "<%=msg%>";
			$(document).ready(function()
			{
				if(msg == "LOGIN_FAILURE")
				{
			    	 $('#errorMsg').html("Invalid login credentials");
			    	 $('#errorMsg').show();
			    	 setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "NOT_REGISTERED")
				{
					$('#errorMsg').html("Learner no or date of birth is incorrect");
			    	$('#errorMsg').show();
			    	setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "LICENSE_ALREADY_ISSUED")
				{
					$('#errorMsg').html("Your license has been already processed");
			    	$('#errorMsg').show();
			    	setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "THEORY_ALREADY_PASSED")
				{
					$('#errorMsg').html("You have already passed your theory test");
			    	$('#errorMsg').show();
			    	setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "TEST_DATE_MISMATCH")
				{
					$('#errorMsg').html("Test date does not match with todays date");
			    	$('#errorMsg').show();
			    	setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "THEORY_ALREADY_ATTEMPTED_FAILED")
				{
					$('#errorMsg').html("You have already attempted the test and you failed, please try again in later test dates");
			    	$('#errorMsg').show();
			    	setTimeout('hideStatus("errorMsg")',5000);
				}
				else if(msg == "TEST_COMPLETED")
				{
					$('#successMsg').html("You have completed your test, Thank You!");
			    	$('#successMsg').show();
			    	setTimeout('hideStatus("successMsg")',5000);
				}
			});

			function validateForm()
			{
				var learnerNo = $('#learnerNo').val();
				var dob = $('#dob').val();

				if(learnerNo == "")
				{
					$('#learnerNo').focus();
					return false;
				}
				if(dob == "")
				{
					$('#dob').focus();
					return false;
				}
				else
					return;
			}

			function openModal(id)
			{
				$("#"+id).modal('show');
			}

		</script>
	</body>
</html>