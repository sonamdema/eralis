<%@page import="bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO"%>
<%@page import="java.util.List"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/select2.min.css">
<%
	String jurisId = (String) request.getAttribute("JURIS_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
	String jurisTypeId = (String) request.getAttribute("JURIS_TYPE_ID");
%>

<form id="candidateListForm" action="<%=request.getContextPath() %>/etest.html" class="form-horizontal">
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			List of Candidate
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				view list of candidates who has booked the driving test
			</small>
		</h1>
	</div><!-- /.page-header -->
	
	<div class="row" id="messageDiv" style="display:none"></div>
	
	<div class="row" id="searchDIV">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Search Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							 <div class="col-lg-12">
							 
							 	<div class="form-group">
							 		<div class="col-lg-3">
						    			<label>Test Date <span style="color: #ff0000">*</span></label>
						    		</div>
						    		<div class="col-lg-3">
						    			<select id="testDate" class="select2" id="testDate" style="width: 250px!important;">
						    				<option value="">--SELECT--</option>
						    				<logic:iterate id="test" name="TEST_DATE_LIST">
						    					<option value='<bean:write name="test" property="headerId"/>'>
						    						<bean:write name="test" property="headerName"/>
						    					</option>
						    				</logic:iterate>
						    			</select>
						    		</div>
						    		<div class="col-lg-3">
						    			<label>Test Location <span style="color: #ff0000">*</span></label>
						    		</div>
						    		<div class="col-lg-3">
						    			<input type="text" disabled="disabled" value="<%=regionName%>" class="form-control"/>
						    			<input type="hidden" id="jurisId" value="<%=jurisId%>"/>
						    			<input type="hidden" id="jurisTypeId" value="<%=jurisTypeId%>"/>
						    		</div>
							 	</div>
							 	
							 	<div class="form-group">
							 		<div class="col-lg-12">
							 			<div class="pull-right">
							 				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Generate</button>
							 			</div>
							 		</div>
							 	</div>
							 
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="resultDIV"></div>
</form>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>	
<script type="text/javascript" src="<%=request.getContextPath() %>/js/select2.full.min.js"></script>
	<script type="text/javascript">

		$(function () {
		   $('.select2').select2();
		});

		$(document).ready(function()
	    {
			$("#candidateListForm").validate({
				invalidHandler: function(form, validator) {
		             var errors = validator.numberOfInvalids();
		             if (errors) {                    
		                 var firstInvalidElement = $(validator.errorList[0].element);
		                 $('html,body').scrollTop(firstInvalidElement.offset().top);
		                 firstInvalidElement.focus();
		             }
		         	},
					rules:
					{
						testDate:{
							required:true
						},
					},    
					messages:
					{
						testDate:{required: "Please select a test date"}
					}
			});

			$('#submitBtn').click(function()
			{
				if($('#candidateListForm').valid()) 
				{
					var testDate = $('#testDate').val();
					var jurisId = $('#jurisId').val();
					var jurisTypeId = $('#jurisTypeId').val();
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/etest.html?method=candidate_list&testDate="+testDate+"&jurisId="+jurisId+"&jurisTypeId="+jurisTypeId,
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#resultDIV").html(responseText);
						}
					});
				}
				else 
				{
					return false;
				}
			});	
	    });
		
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
		
	</script>