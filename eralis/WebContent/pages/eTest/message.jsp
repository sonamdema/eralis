<%
	String msg = (String) request.getAttribute("MESSAGE");

	if("ADD_SUCCESS".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-success">
			<i class="ace-icon fa fa-check"></i>
			 Details successfully saved
		</div>
	<%
	}
	else if("ADD_FAILURE".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-danger">
			<i class="ace-icon fa fa-times"></i>
			Details couldn't be saved, try again
		</div>
	<%
	}

	if("EDIT_SUCCESS".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-success">
			<i class="ace-icon fa fa-check"></i>
			 Details successfully updated
		</div>
	<%
	}
	else if("EDIT_FAILURE".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-danger">
			<i class="ace-icon fa fa-times"></i>
			Details couldn't be updated, try again
		</div>
	<%
	}
	if("DELETE_SUCCESS".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-success">
			<i class="ace-icon fa fa-check"></i>
			 Details has been deleted
		</div>
	<%
	}
	else if("DELETE_FAILURE".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-danger">
			<i class="ace-icon fa fa-times"></i>
			Details couldn't be deleted, please try again
		</div>
	<%
	}
	if("MARKSHEET_SUCCESS".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-success">
			<i class="ace-icon fa fa-check"></i>
			 Marksheet has been successfully updated
		</div>
	<%
	}
	else if("MARKSHEET_FAILURE".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-danger">
			<i class="ace-icon fa fa-times"></i>
			Marksheet details couldnot be updated, try again
		</div>
	<%
	}
	else if("SUCCESS".equalsIgnoreCase(msg))
	{
	%>
		<div class="alert alert-success">
			<i class="ace-icon fa fa-times"></i>
			Successfully updated
		</div>
	<%
	}
	else
	{
	%>
		<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<p>
				<%=msg %>
			</p>

			<p>
				<button class="btn btn-sm btn-success">Back To Home</button>
			</p>
		</div>
	<%
	}
	%>