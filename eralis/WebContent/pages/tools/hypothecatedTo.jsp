<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />


<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Hypothecated To
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Hypothecated To
		</small>
	</h1>
          </div><!-- /.page-header -->
          <div class="row">
	       <div class="col-lg-12 col-sm-4">
	       <html:form styleClass="form-horizontal" action="/tools.html?method=hypothecated_To">
			   <div class="row">
			     <div class="col-sm-12">
			       <div class="row">
			         <div class="col-xs-12 col-sm-7">
			           <div class="form-group" > 
								  <label class="col-sm-4 control-label" for="Vehicle Number"> Vehicle Number :<span style="color: #ff0000">*</span>  </label>

                                           <div class="col-sm-5 no-padding";>
                                             <html:text property="vehicleNumber" styleClass="col-xs-12" styleId="vehicleNumber"></html:text>
                                           </div>
										<span class="input-group-btn">
									      	<a href="#vehicle-number" class="btn btn-purple btn-sm" data-toggle="modal">
												<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
											</a>
										</span> 
							</div>
			            </div>
			         </div>
			      </div>
			   </div>		
			<div class="space-6"></div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
			   <h5 class="widget-title lighter">Personal Information</h5>
			</div>
			 <div class="widget-body">
			   <div class="widget-main">
			     <div class="row">
			       <div class="col-lg-12">
			         <div class="form-group">
                        <label class="col-lg-2 control-label no-padding-right" for="Owner Name"> Owner Name : </label>

                      <div class="col-lg-4">
                   
                      </div>
                         <label class="col-lg-2 control-label no-padding-right" for="Dzongkhag"> Dzongkhag : </label>

                      <div class="col-lg-4">
                 
                     </div>
                 </div>
									
		     <div class="form-group">
                <label class="col-lg-2 control-label no-padding-right" for=" Gewog"> Gewog : </label>

                  <div class="col-lg-4">
                 
                  </div>
              
                  <label class="col-lg-2 control-label no-padding-right" for="Addresss"> Address : </label>

                 <div class="col-lg-4">
                 
                 </div>
               </div>
             </div>
		   </div>
			
		     </div>
	        </div>
           	</div>                       
             
             
        <div class="space-6"></div>
		     <div class="widget-box">
		       <div class="widget-header widget-header-small">
		         <h5 class="widget-title lighter">Registration Details</h5>
		       </div>
		   <div class="widget-body">
		       <div class="widget-main">
		         <div class="row">
		          <div class="col-xs-12"> 
                                            
                    <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Region"> Region : </label>
                       <div class="col-sm-4">
			               <html:select property="region" styleClass="col-xs-10 col-sm-5" styleId="region">
						     <html:option value="">--------SELECT--------</html:option>
						   </html:select>
					   </div>
                       <label class="col-sm-2 control-label no-padding-right" for="Vehicle Rgn. Code"> Vehicle Rgn. Code : </label>

                        <div class="col-sm-4">
                           <html:select property="vehicleCode" styleClass="col-xs-10 col-sm-5" styleId="vehicleCode">
				              <html:option value="">--------SELECT--------</html:option>
				           </html:select>
              
                      </div>
                    </div>
                   <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Vehicle Number"> Vehicle Number :<span style="color: #ff0000">*</span> </label>

                       <div class="col-sm-4">
                         <html:text property="vehicleNumber" styleClass="col-xs-10 col-sm-5" styleId="vehicleNumber"></html:text>
                      </div>
            
                        <label class="col-sm-2 control-label no-padding-right" for="Vehicle Number"> Registration Date :<span style="color: #ff0000">*</span> </label>

                         <div class="col-sm-4">
                            
                      </div>
                    </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Vehicle Type"> Vehicle Type :<span style="color: #ff0000">*</span> </label>

                     <div class="col-sm-4">
                     <html:select property="vehicleType" styleClass="col-xs-10 col-sm-5" styleId="vehicleType">
					 <html:option value="">--------SELECT--------</html:option>
					 </html:select>
					 </div>
                    
                     </div> 
                  <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Chasis Number"> Chasis Number :<span style="color: #ff0000">*</span> </label>

                       <div class="col-sm-4">
                         
                      </div>
            
                        <label class="col-sm-2 control-label no-padding-right" for="Engine Number"> Engine Number :<span style="color: #ff0000">*</span> </label>

                         <div class="col-sm-4">
                            
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Engine Type"> Engine Type :<span style="color: #ff0000">*</span> </label>
                       <div class="col-sm-4">
			               <html:select property="engineType" styleClass="col-xs-10 col-sm-5" styleId="engineType">
						     <html:option value="">--------SELECT--------</html:option>
						   </html:select>
					   </div>
                       <label class="col-sm-2 control-label no-padding-right" for="Colour"> Colour : </label>

                        <div class="col-sm-4">
                           
                      </div>
                    </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Company"> Vehicle Company :<span style="color: #ff0000">*</span> </label>
                       <div class="col-sm-4">
			               <html:select property="vehicleCompany" styleClass="col-xs-10 col-sm-5" styleId="vehicleCompany">
						       <html:option value="">--------SELECT--------</html:option>
						   </html:select>
					   </div>
                       <label class="col-sm-2 control-label no-padding-right" for="Vehicle Model"> Vehicle Model :<span style="color: #ff0000">*</span> </label>

                        <div class="col-sm-4">
                            <html:select property="vehicleModel" styleClass="col-xs-10 col-sm-5" styleId="vehicleModel">
						        <html:option value="">--------SELECT--------</html:option>
						   </html:select>
                      </div>
                    </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Status"> Status :<span style="color: #ff0000">*</span> </label>
                       <div class="col-sm-4">
			               <html:select property="status" styleClass="col-xs-10 col-sm-5" styleId="status">
						       <html:option value="">--------SELECT--------</html:option>
						   </html:select>
					   </div>
                  </div>
                 <div class="radio" >
								<label>
									<html:radio property="hypothecatedRadio" styleClass="ace" styleId="hypothecatedRadio" value="p" onclick="showHypothecated()"></html:radio>	
									<span class="lbl"> Hypothecated </span>
								</label>
								<label>
									<html:radio property="hypothecatedRadio" styleClass="ace" styleId="unhypothecatedRadio" value="o" onclick="showUnhypothecated()"></html:radio>	
									<span class="lbl"> Remove Hypothecated</span>
								</label>
								 
					</div>  
                  <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Hypothecated To"> Hypothecated To :<span style="color: #ff0000">*</span> </label>
                       <div class="col-sm-4">
			               <html:select property="hypothecatedTo" styleClass="col-xs-10 col-sm-5" styleId="hypothecatedTo">
						       <html:option value="">--------SELECT--------</html:option>
						   </html:select>
					  </div>
                 </div>
                 <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Letter No"> Letter No : </label>
                       <div class="col-sm-4">
			               <html:text property="letterNo" styleClass="col-xs-10 col-sm-5" styleId="letterNo"></html:text>
					   </div>
                       <label class="col-sm-2 control-label no-padding-right" for="Letter Date"> Letter Date : </label>

                        <div class="col-sm-4">
                            <div class="input-group">
									<html:text property="letterDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
                      </div>
                  </div>   
                  <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Remove Date"> Remove Date : </label>
                       <div class="col-sm-4">
			               <div class="input-group">
									<html:text property="removeDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
					  </div>
                   </div>
                   <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Remarks"> Remarks : </label>
                       <div class="col-sm-4">
			               <html:textarea property="remarks" styleClass="col-xs-10 col-sm-5" styleId="remarks"></html:textarea>
					  </div>
                       </div>
                   
                   
                    </div>
                    
                    
                    </div>
                  </div>
                 </div>
                </div>
           
			
			
			<div class="pull-left">
			
				<html:button property="save" styleClass="btn btn-primary btn-sm">Save</html:button>
				<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
				<html:button property="close" styleClass="btn btn-primary btn-sm">Close</html:button>
								
				 
			</div>
			
		</html:form>
		</div>
		</div>
		
  <div id="vehicle-number" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Owner Type"> Owner Type : </label>
	
                            <div class="col-sm-4">
                                <select class="col-xs-10 col-sm-7" id="form-field-select-1">
										 <option value=""></option>
							    </select> 
                            </div>
							
						</div>

						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Type"> Vehicle Type : </label>
	
                                 <div class="col-sm-4">
                                      <select class="col-xs-10 col-sm-7" id="form-field-select-1">
										 <option value=""></option>
							         </select> 
                                 </div>
								
						</div>
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Vehicle Number" placeholder="Vehicle Number"  />
                                 </div>
								
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Owner's Name"> Owner's Name : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="region" placeholder="Owner's Name"  />
                                 </div>
								
						</div>
					
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Citizen ID" placeholder="Citizen ID"  />
                                 </div>
				
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Engine Number" placeholder="Engine Number"  />
                                 </div>
				
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Chasis Number" placeholder="Chasis Number"  />
                                 </div>
				
						</div>
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-check"></i>
					Select
				</button>
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
		
	
<script>

	
	$(document).ready(function()
	{
		$('#hypothecatedRadio').attr('checked', true);
		$('#hypothecated').show();
		
		
	});
	function showPersonal(){

		$('#hypothecatedRadio').attr('checked', true);
		$('#unhypothecatedRadio').attr('checked', false);

	}
	function showOrganisation(){

		$('#hypothecatedRadio').attr('checked', false);
		$('#unhypothecatedRadio').attr('checked', true);

		}

</script>
   
   <script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
            <script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>
 
