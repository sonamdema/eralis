<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Replace IID
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		 Replace License and Endorsement IID	
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
		<html:form styleClass="form-horizontal" styleId="replaceIID" action="/tools.html?method=replace_IID">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-xs-12 col-sm-7">
							<div class="form-group" >
						  		<label class="col-sm-4 control-label" for="License Number"> License Number :<span style="color: #ff0000">*</span></label>
								<div class="col-sm-5 no-padding";>
                                   <html:text property="licenseNumber" readonly="true" styleClass="col-xs-12" styleId="licenseNO"></html:text>
                                </div>
								<span class="input-group-btn">
							      	<a href="#modal-form" class="btn btn-purple btn-sm" data-toggle="modal">
										<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
									</a>
								</span> 
							</div>
						</div>
					</div>
				</div>
			</div>		
			<div class="space-6"></div>
		  	<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Personal Information</h5>
			  	</div>
			    <div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
	                            	<label class="col-lg-2 control-label no-padding-right" for="Owner Type"><b> Name : </b></label>
									<div class="col-lg-4" id="displayOwnerName"></div>
	                        		<label class="col-lg-2 control-label no-padding-right" for="Owner ID"><b> Gender : </b></label>
									<div class="col-lg-4" id="gender"></div>
	                  			</div>
								<div class="form-group">
		                       		<label class="col-lg-2 control-label no-padding-right" for="Owner Name"><b> Citizenship ID : </b></label>
									<div class="col-lg-4" id="cid"></div> 
	                         		<label class="col-lg-2 control-label no-padding-right" for="ID"><b> Date of birth : </b></label>
									<div class="col-lg-4" id="displayDob"></div>
	                 			</div>
	            				<h4> Permanent Address </h4>
	                            <div class="form-group">
		                      		<label class="col-lg-2 control-label no-padding-right" for="Dzongkhag"> Dzongkhag : </label>
									<div class="col-lg-4" id="displayDzongkhag"></div>
	                       			<label class="col-lg-2 control-label no-padding-right" for="Gewog"> Gewog : </label>
									<div class="col-lg-4" id="displayGewog"></div>
	                			</div> 
	                			<div class="form-group">
	                      			<label class="col-lg-2 control-label no-padding-right" for="Village"> Village : </label>
	                        		<div class="col-lg-4"></div>
		                    		<label class="col-lg-2 control-label no-padding-right" for="Country"> Country : </label>
		                    		<div class="col-lg-4" id="displayCountry"></div>
	                			</div> 
			            		<div class="form-group">
					            	<label class="col-lg-2 control-label no-padding-right" for="Address"> Address : </label>
					            	<div class="col-lg-4" id="displayAddress"></div>
								</div>                              
			    			</div>	
			   			</div>
					</div>
				</div>
			</div>	
         	<div class="space-6"></div>
	  		<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">IID List</h5>
			  	</div>
			    <div class="widget-body">
			   		<div class="widget-main">
				  		<div class="row">
					  		<div class="col-lg-12" id="IIDList"> 
				  			</div>
		        		</div>
		   			</div>
        		</div>
        	</div>	
			<div class="space-6"></div>
  			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Replacement Details</h5>
			  	</div>
			    <div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
                       				<label class="col-lg-2 control-label no-padding-right" for="Old IID"> Old IID : </label>
									<div class="col-lg-4">
										<html:text property="oldIID" readonly="true"  styleId="old_IID"></html:text>
              						</div>
                				</div>
	       						<div class="form-group">
                          			<label class="col-sm-2 control-label no-padding-right" for="New IID"> New IID :<span style="color: #ff0000">*</span> </label>
									<div class="col-sm-4">
		                             		<html:text property="newIID"   styleId="newIID" ></html:text>
		                             		
		                           	</div>
                    			</div> 
		 					</div>
        				</div>
   					</div>
      			</div>
      		</div>				
			<div class="col-xs-3 pull-left">  
				<html:hidden property="customerId" styleId="IID_customerId"/>
				<html:hidden property="tableName" styleId="table_name"/>
				<html:hidden property="row_id" styleId="row_id"/>
				<html:button property="replace" styleClass="btn btn-primary btn-sm" onclick="replaceIID()">Replace</html:button>
			</div><div class="col-xs-12" id="displayMsgDiv"></div>
		</html:form>
	</div>
</div>
<div id="modal-form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                        <form class="form-horizontal" id="reset_form" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
		                       	<div class="col-sm-4">
		                            <input type="text" id="cidPersonalModal" placeholder="Citizen ID" class="modal_input"   />
		                       	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> License No : </label>
		                       	<div class="col-sm-4">
		                            <input type="text" id="licenseNoModal" placeholder="License No" class="modal_input"   />
		                       	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Customer ID"> Customer ID : </label>
                                <div class="col-sm-4">
                                	<input type="text" id="customerIdPersonalModal" placeholder="Customer ID"  class="modal_input"  />
                                </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal" onClick="inputReset('reset_form')">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script>
function replaceIID()
{  
	var customerID =	$("#IID_customerId").val();
	$(document).ready(function()
	{
		$("#replaceIID").validate({
			rules:{
				
				newIID:{required:true}
					},    
			messages:
				{
				
				newIID:{required: "Please Search"}
				},
		});
	});
	if($('#replaceIID').valid()) 
	{
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/tools.html?method=replace_IID&customerID="+customerID,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#displayMsgDiv").html(responseText); 
				$("#displayMsgDiv").show();
				setTimeout('hideStatus("displayMsgDiv")',2000);
			    setTimeout('reloadPage()',2000);
			    getToolsDetails(customerID);
			}
		});
	}
}
function oldData(table,row_id,IID,endorsedate,customerId)
{ 

	$("#IID_customerId").val(customerId);
	$("#old_IID").val(IID);
	$("#endorse_date").val(endorsedate); 
	$("#table_name").val(table); 
	$("#row_id").val(row_id); 

}

function searchPersonalInfo()
{
	var cidNumber 	= $('#cidPersonalModal').val();
	var customerId 	= $('#customerIdPersonalModal').val();
	var licenseNo	= $('#licenseNoModal').val();
	
	if(cidNumber == "")
		cidNumber = "NA";
	if(customerId == "")
		customerId = "NA";
	if(licenseNo == "")
		licenseNo = "NA";
 
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/tools.html?method=getLicenseHolderList&cid="+cidNumber+"&customerId="+customerId+"&licenseNo="+licenseNo,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#licenseDetailList").html(responseText);
			$("#licenseDetailList").show();
			
		}
	});
	 
} 
function getToolsDetails(customerId)
{
	$.ajax
	({
		async: true,
		type: 'POST',
		url: '<%=request.getContextPath()%>/tools.html?method=getEndorsementLicenseDetails&customerId='+customerId,
		success: function(xml)
		{	 
			$("#IIDList").html(xml); 
		}
	});
}
</script>

