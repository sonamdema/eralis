<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<style>
	.datepicker{z-index:1151 !important;}
</style>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Pending Application
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Pending Application Information
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
		<html:form styleClass="form-horizontal" action="/tools.html?method=license_Punch" styleId="formSubmission">
	  		<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-2">
								<label>CID Number:</label>
							</div>
							<div class="col-lg-3">
								<div class="input-group">
									<html:text property="originalCID" styleId="cid" styleClass="form-control" readonly="true"></html:text>
									<span class="input-group-btn">
										<button type="button" class="btn btn-purple btn-sm" onclick="openModal('personalModal')">
											<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
										</button>
									</span>	
								</div>
							</div>
							<html:hidden property="customerId" styleId="customerID"></html:hidden>
						</div>
					</div>
				</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="labelCustomerName"></div>
								<div class="col-lg-2">
									<label class="control-label">Customer ID:</label>
								</div>
								<div class="col-lg-4" id="labelCustomerId"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="labelCID"></div>
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="labelDOB"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Phone Number:</label>
								</div>
								<div class="col-lg-4" id="labelContactNo"></div>
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="labelGender"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Pending List</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div id="pendingLIst"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Pending Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Remarks:</label>
								</div>
								<div class="col-lg-7">
									<html:textarea styleClass="col-lg-12" styleId="pendingRemarks" property="remarks"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-primary btn-sm pull-right" onClick="savePending()">Save Pending</button>
		<div class="col-lg-12" id='displayMsgDiv'></div>
	</div>
	</html:form>
</div>

<div id="personalModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Citizen ID:</label>
									<span class="pull-right">
										<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
									</span>
								</div>
								<div class="form-group">
									<label>Customer ID:</label>
									<span class="pull-right">
										<input type="text" id="customerIdPersonalModal" placeholder="CustomerID" style="width: 200px"  />
									</span>
								</div>
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button name="search" class="btn btn-sm" onclick="searchCommonInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			
			<div id="commonListTable">
			</div>
		</div>
	</div>
</div>
<script>
	function searchCommonInfo()
	{
		$('#submitBtn').hide();
		$('#updateBtn').show();
		var cidNumber = $('#cidPersonalModal').val();
		var customerId = $('#customerIdPersonalModal').val();
		if(cidNumber == "")
			cidNumber = "NA";
		if(customerId == "")
			customerId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoForEdit&cid="+cidNumber+"&customerId="+customerId+"&searchType=VEHICLE_REGISTRATION",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#commonListTable").html(responseText);
				$("#commonListTable").show();
			}
		});
	}
	function savePending()
	{
		var customerId = $('#customerID').val();
		var options = {target:'#displayMsgDiv',url:context+'/tools.html?method=save_pending_task',async:false, type:'POST',data: $("#formSubmission").serialize()}; 
	    $("#formSubmission").ajaxSubmit(options);
	    $('#displayMsgDiv').show();
	    $("#pendingRemarks").val('');
	    setTimeout('hideStatus("displayMsgDiv")',4000);
	    pendingList(customerId);
	}

	function pendingList(customerId)
	{ 
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPendingList&customerId="+customerId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#pendingLIst").html(responseText);
				$("#pendingLIst").show();
			}
		});
	}
	function getSelectedPendingTask(pendingId)
	{
		var customerID	=	$("#customerID").val();
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=clearPendingTask&pendingId="+pendingId+"&customerID="+customerID,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#pendingLIst").html(responseText);
				$("#pendingLIst").show();
			}
		});
	}
</script>