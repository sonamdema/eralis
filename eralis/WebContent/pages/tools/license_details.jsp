<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%><div class="table-responsive">
<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr> 
			<th></th>
			<th>CID</th> 
			<th>Name</th> 
			<th>License Number</th> 
			<th>Region</th> 
			<th>Dzongkhag</th> 
			<th>Issue Date</th>
			<th>Expiry Date</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="LICENSE_HOLDER_LIST">
			<logic:iterate id="license" name="LICENSE_HOLDER_LIST">
				<tr> 
					<td>
						<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="license" property="licenseId"/>' onclick="getSelected(this.id)">SELECT</button>
						<!-- -<input type="radio" name="customerIdRadio" id='<bean:write name="license" property="customerId"/>_<bean:write name="license" property="personalInfoId"/>' onclick="addSelected(this.id)"/>
					 --></td>
					<td><bean:write name="license" property="citizenID"/></td>
					<td><bean:write name="license" property="customerName"/></td>
					<td><bean:write name="license" property="licenseNumber"/></td>
					<td><bean:write name="license" property="region"/></td>
					<td><bean:write name="license" property="dzongkhag"/></td>
					<td><bean:write name="license" property="issuedDate"/></td>
					<td><bean:write name="license" property="expiryDate"/></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="LICENSE_HOLDER_LIST">
			<tr>
				<td colspan="8" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>
</div>
<div>
	<input type="hidden" name="column_name" id="column_name">
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>

<script>
	var globalId = "";
	var personalInfoId = "";
	var customerId = "";
	function addSelected(id)
	{
		globalId =  id.split("_");
		customerId	= globalId[0];
		personalInfoId	= globalId[1];
		//alert(customerId+"/"+personalInfoId);
	}
																													
	function getSelected(drivinglicenseId)
	{	
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getSuspensionDetails&drivinglicenseId='+drivinglicenseId+'&searchType=DELETE_DRIVE_TYPE',
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					var name = $(this).find('name').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					
					var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var titleOfCourtesy = $(this).find('courtesy').text();
					var occupation = $(this).find('occupation').text();
					var customerId = $(this).find('customerId').text();
					var bloodgroup = $(this).find('bloodgroup').text();
					var bloodgroupId = $(this).find('bloodgroupId').text();
					var dob = $(this).find('dob').text();
					var nationality=$(this).find('nationality').text();
					var CID=$(this).find('CID').text();
					var fathersname=$(this).find('fathersname').text();
					var identificationMarks=$(this).find('identificationMarks').text();
					var remarks=$(this).find('remarks').text();
					var gender = $(this).find('gender').text();
					var country = $(this).find('country').text();
					var personalInfoId = $(this).find('personalInfoId').text();
					var village = $(this).find('village').text();
					var licenseNO = $(this).find('licenseNO').text();
					var licenseId	=	$(this).find('drivingLicenseId').text();
					$("#licenseId").val(licenseId);
					//vehicle registration
					$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
					$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
					$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
					$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
					$('#citizenID').val(customerId);

					//personal information
					$('#customerID').val(customerId);
					$('#titleOfcourtesy').val(titleOfCourtesy);
					$('#name').val(name);
					$('#occupation').val(occupation);
					$('#nationality').val(nationality);
					$('#cid').val(CID);
					$('#bloodGroup').val(bloodgroupId);
					$('#displayDob').val(dob);
					$('#fathersName').val(fathersname);
					$('#identificationMarks').val(identificationMarks);
					$('#remarks').val(remarks);
					$('#licenseNO').val(licenseNO);

					if(gender == "M")
						$('#maleRadio').attr('checked', true);
					else if(gender == "F")
						$('#femaleRadio').attr('checked', true);
					
					//new learner license
					$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
					$('#displayDOB').html("<label class='control-label'>"+dob+"</label>");
					
					//non-commercial
					$('#customer_ID').html("<label class='control-label'>"+customerId+"</label>");
					$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
					$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
					$('#gender').html("<label class='control-label'>"+gender+"</label>");
					$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
					$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
					$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
					$('#displayCountry').html("<label class='control-label'>"+country+"</label>");

					/*TOOLS START*/
					$('#cid').html("<label class='control-label'>"+CID+"</label>");
					$("#duplicateCID").val(CID);
					$("#personalInfoId").val(personalInfoId); 
					$('#village').html("<label class='control-label'>"+village+"</label>");
					/*TOOLS END*/
					getToolsDetails(customerId);
				});
			}
		});  
	}
</script>
 