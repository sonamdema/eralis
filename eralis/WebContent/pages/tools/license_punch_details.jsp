<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%int j=1;int i=0; %>
<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>    
			<th>Punch Date</th> 
			<th>Letter No</th> 
			<th>Letter Date</th> 
			<th>Action</th> 
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="LICENSE_PUNCH_DETAIL">
			<logic:iterate id="punch" name="LICENSE_PUNCH_DETAIL"  type="bt.gov.rsta.eralis.dto.tools.ToolsDTO" indexId="index">
			<%
				  i = index.intValue()+1;
			%>
				<tr>  
					<td> 
						<%=punch.getPunchDate() %>
					</td>
					<td>
						<%=punch.getLetterNo()%>
					</td>
					<td>
						<%=punch.getLetterDate() %>
					</td>
					<td>
						<button type="button" class="btn btn-danger btn-sm" onClick="modalOpen('<%=punch.getRow_id()%>','withdrawnModal')">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Withdraw
						</button>
					</td>
				 </tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>   
<input id="punchRowCount" type="hidden" value="<%=i%>">

<input type="hidden" id="punchId">
<script type="text/javascript">
 
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	function modalOpen(punchId,modalId)
	{
		$("#punchId").val(punchId);
		$('#'+modalId).modal('show');
	}
</script>
