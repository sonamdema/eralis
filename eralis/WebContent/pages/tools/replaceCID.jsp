<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Replace CID 
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Replace Existing CID
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<html:form styleClass="form-horizontal" styleId="replaceCID" action="/tools.html?method=replace_CID">
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Search</h5>
		  	</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="row"> 
								<div class="form-group"> 
									<div class="col-lg-3">
										<label class="col-lg-9 control-label no-padding-right" for="Duplicate/Temporary CID"><b> Duplicate/Temporary CID* :  </b></label>
									</div> 
									<div class="col-lg-3">
										<div class="input-group">
		                                	<html:text property="duplicateCID" styleClass="col-xs-12" styleId="duplicateCID" readonly="true" value=""></html:text>
		                                	<html:hidden property="personalInfoId" styleClass="col-xs-12" styleId="personalInfoId" ></html:hidden>
		                                	<span class="input-group-btn" id="searchDuplicate">
										      	<a href="#modal-form" class="btn btn-purple btn-sm" data-toggle="modal" onClick="inputReset('reset_form')">
													<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
												</a>
											</span> 
		                                </div>
									</div> 
								</div>
								<div class="form-group" > 
									<div class="col-lg-3">
										<label class="col-lg-9 control-label no-padding-right" for="Original CID"><b> Original CID* :  </b></label>
									</div> 
									<div class="col-lg-3">
	                                	<html:text property="originalCID" styleClass="col-xs-12" styleId="originalCID"  ></html:text>
									</div>  		
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>	
	  	<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
		  	</div>
		    <div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
                            	<label class="col-lg-2 control-label no-padding-right" for="Owner Type"><b> Name : </b></label>
								<div class="col-lg-4" id="displayOwnerName"></div>
                        		<label class="col-lg-2 control-label no-padding-right" for="Owner ID"><b> Gender : </b></label>
								<div class="col-lg-4" id="gender"></div>
                  			</div>
							<div class="form-group">
	                       		<label class="col-lg-2 control-label no-padding-right" for="Owner Name"><b> Citizenship ID : </b></label>
								<div class="col-lg-4" id="cid"></div> 
                         		<label class="col-lg-2 control-label no-padding-right" for="ID"><b> Date of birth : </b></label>
								<div class="col-lg-4" id="displayDob"></div>
                 			</div>
            				<h4> Permanent Address </h4>
                            <div class="form-group">
	                      		<label class="col-lg-2 control-label no-padding-right" for="Dzongkhag"> Dzongkhag : </label>
								<div class="col-lg-4" id="displayDzongkhag">
                           	</div>
                       		<label class="col-lg-2 control-label no-padding-right" for="Gewog"> Gewog : </label>
							<div class="col-lg-4" id="displayGewog">
                    	</div>
                	</div> 
                			<div class="form-group">
                      			<label class="col-lg-2 control-label no-padding-right" for="Village"> Village : </label>
                        		<div class="col-lg-4"></div>
	                    		<label class="col-lg-2 control-label no-padding-right" for="Country"> Country : </label>
	                    		<div class="col-lg-4" id="displayCountry"></div>
                			</div> 
		            		<div class="form-group">
				            	<label class="col-lg-2 control-label no-padding-right" for="Address"> Address : </label>
				            	<div class="col-lg-4" id="displayAddress"></div>
							</div>                              
		    			</div>	
		   			</div>
				</div>
			</div>
		</div>	
		<div id="displayMsgDiv"></div>	
		<div class="pull-left">
			<html:button property="replace" styleClass="btn btn-primary btn-sm" onclick="replaceCID()">Replace</html:button>
		</div>
	</html:form>
</div>
<div id="modal-form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                        <form class="form-horizontal" id="reset_form" role="form">
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Name"> Name : </label>
	                            <div class="col-sm-4">
	                                <input type="text" id="namePersonalModal" placeholder="Name" class="modal_input"  />
	                            </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
		                       	<div class="col-sm-4">
		                            <input type="text" id="cidPersonalModal" placeholder="Citizen ID" class="modal_input"   />
		                       	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Customer ID"> Customer ID : </label>
                                <div class="col-sm-4">
                                	<input type="text" id="customerIdPersonalModal" placeholder="Customer ID"  class="modal_input"  />
                                </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Region"> Region : </label>
                                <div class="col-sm-4">
                                   <select class="col-sm-12 modal_input"  id="regionPersonalModal" onchange="populateDependentDropDown(this.value, 'dzongkhagPersonalModal', '', 'DZONGKHAG_LIST', 'N')">
										<option value="">SELECT</option>
										<logic:iterate id="region" name="regionList">
											<option value='<bean:write name="region" property="headerId"/>'>
												<bean:write name="region" property="headerName"/>
											</option>
										</logic:iterate>
									</select> 
                                </div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Dzongkhag"> Dzongkhag : </label>
                               	<div class="col-sm-4">
                                	<select class="col-sm-12" id="dzongkhagPersonalModal">
                                    	<option value="">SELECT</option>
                                    </select>
                               </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" onclick="searchPersonalInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary" onClick="inputReset('reset_form')">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal" onClick="inputReset('reset_form')">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="personalListTable" class="reset_form">
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script>
	
	function searchPersonalInfo()
	{
		var name = $('#namePersonalModal').val();
		var cidNumber = $('#cidPersonalModal').val();
		var customerId = $('#customerIdPersonalModal').val();
		var regionId = $('#regionPersonalModal').val();
		var dzongkhagId = $('#dzongkhagPersonalModal').val();
	
		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(customerId == "")
			customerId = "NA";
		if(regionId == "")
			regionId = "NA";
		if(dzongkhagId == "")
			dzongkhagId = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList&name="+name+"&cid="+cidNumber+"&customerId="+customerId+"&region="+regionId+"&dzongkhag="+dzongkhagId+"&searchType=REPLACE_CID",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#personalListTable").html(responseText);
				$("#personalListTable").show();
			}
		});
	}
	function replaceCID()
	{
		$(document).ready(function()
		{
			$("#replaceCID").validate({
				rules:{
					originalCID:{required:true,number:true},
					duplicateCID:{required:true},
						},    
				messages:
					{
						originalCID:{required: "Please Provide Original CID"},
						duplicateCID:{required: "Please Search"}
					},
			});
		});
		if($('#replaceCID').valid()) 
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/tools.html?method=replace_CID",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#displayMsgDiv").html(responseText);
					$("#displayMsgDiv").show();
					setTimeout('hideStatus("displayMsgDiv")',2000);
				    setTimeout('reloadPage()',2000);
								 
				}
			});
		}
	}
</script>