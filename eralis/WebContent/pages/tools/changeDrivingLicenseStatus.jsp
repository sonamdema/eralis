<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Change Driving License Status
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Change License Process Status
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
	  <html:form styleClass="form-horizontal" action="/tools.html?method=change_Driving_License_Status">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-xs-12 col-sm-7">
							<div class="form-group" > 
								  <label class="col-sm-4 control-label" for="License No"> License Number :<span style="color: #ff0000">*</span>  </label>

                                           <div class="col-sm-5 no-padding";>
                                             <html:text property="licenseNumber" styleClass="col-xs-12" styleId="licenseNumber"></html:text>
                                           </div>
										<span class="input-group-btn">
									      	<a href="#license-number" class="btn btn-purple btn-sm" data-toggle="modal">
												<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
											</a>
										</span> 
							</div>
							
						</div>
					</div>
				</div>
			</div>		
					<div class="space-6"></div>
					  <div class="widget-box">
						<div class="widget-header widget-header-small">
							<h5 class="widget-title lighter"> Change License Process Status</h5>
						  </div>
						    <div class="widget-body">
							   <div class="widget-main">
								  <div class="row">
		                          
		                           <div class="col-sm-4 col-xs-6">
					                 
					                 <div class="radio control-label " >
								       <label>
									         <html:radio property="postRadio" styleClass="ace" styleId="postRadio" value="p" onclick="showPost()"></html:radio>	
									         <span class="lbl"> Post </span>
								       </label>
								       <label>
									         <html:radio property="postRadio" styleClass="ace" styleId="printRadio" value="o" onclick="showPrint()"></html:radio>	
									         <span class="lbl"> Print </span>
								      </label>
								 
							       </div>
					
					
							<div class="form-group" > 
						    
						      <label class="col-sm-6">
                                  
                                <html:checkbox property="drivingLicense" styleClass="ace"></html:checkbox>
                                <span class="lbl">Driving License</span>
                              
                              
                              </label>
                   
                            <label class="col-sm-6">
                                  <html:checkbox property="duplicateLicense" styleClass="ace"></html:checkbox>
                                <span class="lbl">Duplicate License</span>
                            </label>
                         </div>
                        
                          <div class="form-group" > 
						
                            <label class="col-sm-6">
                           	     <html:checkbox property="endorsement" styleClass="ace"></html:checkbox>
                                  <span class="lbl">Endorsement</span>
                           	     
                            </label>
                          
                           <label class="col-sm-6">
                           	   <html:checkbox property="renewal" styleClass="ace"></html:checkbox>
                                  <span class="lbl">Renewal</span>
                           	   
                           </label>
	                     
	                 </div>
			      </div>                        
			   </div>
		     </div>
	      </div>	
       </div>		
		
			
			  <div class="pull-left">
			
				  <html:button property="ok" styleClass="btn btn-primary btn-sm">OK</html:button>
				  <html:button property="cancel" styleClass="btn btn-primary btn-sm">Cancel</html:button>
				
		    </div>
		</html:form>
	</div>
</div>

<script>

	
	$(document).ready(function()
	{
		$('#postRadio').attr('checked', true);
		$('#post').show();
		
		
	});
	function showPersonal(){

		$('#postRadio').attr('checked', true);
		$('#printRadio').attr('checked', false);

	}
	function showOrganisation(){

		$('#postRadio').attr('checked', false);
		$('#printRadio').attr('checked', true);

		}

</script>



<div id="license-number" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Name"> Name : </label>
	
                            <div class="col-sm-4">
                                <input type="text" id="Name" placeholder="Name"  />
                            </div>
							
						</div>

						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Citizen ID" placeholder="Citizen ID"  />
                                 </div>
								
						</div>
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="License Number"> License Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="License Number" placeholder="License Number"  />
                                 </div>
								
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Region"> Region : </label>
	
                                 <div class="col-sm-4">
                                      <select class="col-xs-10 col-sm-7" id="form-field-select-1">
												<option value="">----ALL----</option>
									 </select>
                                 </div>
								
						</div>
					
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="License Type"> License Type : </label>
	
                                 <div class="col-sm-4">
                                      <select class="col-xs-10 col-sm-7" id="form-field-select-1">
												<option value="">----ALL----</option>
									 </select>
                                 </div>
				
						</div>
						
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-check"></i>
					Select
				</button>
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
