<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr> 
			<th></th>
			<th>Issue Date</th> 
			<th>Driving License No</th> 
			<th>IID</th> 
		</tr>
	</thead>
	<tbody> 
		<logic:notEmpty name="LICENSE_HOLDER_LIST">
			<logic:iterate id="license" name="LICENSE_HOLDER_LIST">
				<tr> 
					<td>
						<button type="button" class="btn btn-sm"   onclick="oldData('<bean:write name="license" property="tableName"/>','<bean:write name="license" property="row_id"/>','<bean:write name="license" property="newIID"/>','<bean:write name="license" property="endorseDate"/>','<bean:write name="license" property="customerId"/>')">
							<i class="ace-icon fa fa-plus"></i>
						</button>
					</td>
					<td><bean:write name="license" property="issuedDate"/></td>
					<td><bean:write name="license" property="licenseNumber"/></td>
					<td><bean:write name="license" property="newIID"/></td>
				 </tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="LICENSE_HOLDER_LIST">
			<tr>
				<td colspan="8" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table> 