<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="widget-box">
	<div class="widget-header widget-header-small">
	<h5 class="widget-title lighter">Road Block Lists</h5>
	</div>						
	<div class="widget-body">
		<div class="widget-main">
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr> 
						<th></th>
						<th>Location</th> 
						<th>Block Date</th> 
						<th>Route From</th> 
						<th>Route To</th> 
						<th>Open Date</th>    
					</tr>
				</thead>
				<tbody> 
					<logic:notEmpty name="ROAD_BLOCK_LIST">
						<logic:iterate id="roadBlock" name="ROAD_BLOCK_LIST">
							<tr> 
								<td>
									<button type="button" class="btn btn-sm" onclick="blockData('<bean:write name="roadBlock" property="blockLocation"/>','<bean:write name="roadBlock" property="blockDate"/>','<bean:write name="roadBlock" property="routeFromId"/>','<bean:write name="roadBlock" property="routeToId"/>','<bean:write name="roadBlock" property="openDate"/>','<bean:write name="roadBlock" property="row_id"/>','<bean:write name="roadBlock" property="photoUrl1"/>','<bean:write name="roadBlock" property="photoUrl2"/>','<bean:write name="roadBlock" property="photoUrl3"/>','<bean:write name="roadBlock" property="photoId"/>')"  >  
										<i class="ace-icon fa fa-plus"></i>
									</button>
								</td>
								<td><bean:write name="roadBlock" property="blockLocation"/></td>
								<td><bean:write name="roadBlock" property="blockDate"/></td>
								<td><bean:write name="roadBlock" property="routeFrom"/></td>
								<td><bean:write name="roadBlock" property="routeTo"/></td>
								<td><bean:write name="roadBlock" property="openDate"/></td> 
							 </tr>
						</logic:iterate>
					</logic:notEmpty>
					<logic:empty name="ROAD_BLOCK_LIST">
						<tr>
							<td colspan="8" align="center">
								<font color='red'>NO RECORD FOUND</font>
							</td>
						</tr>
					</logic:empty>
				</tbody>
			</table>
		</div>
	</div>
</div>