<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />


<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Refresh Course Entry
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Refresher Course Entry
		</small>
	</h1>
          </div><!-- /.page-header -->
          <div class="row">
	       <div class="col-lg-12 col-sm-4">
	       <html:form styleClass="form-horizontal" action="/tools.html?method=refresh_Course_Entry">
			   <div class="row">
			     <div class="col-sm-12">
			       <div class="row">
			         <div class="col-xs-12 col-sm-7">
			           <div class="form-group" > 
								  <label class="col-sm-4 control-label" for="Learner License No"> Learner License No :  </label>

                                           <div class="col-sm-5 no-padding";>
                                             <html:text property="learnerLicenseNo" styleClass="col-xs-12" styleId="learnerLicenseNo"></html:text>
                                           </div>
										<span class="input-group-btn">
									      	<a href="#modal-form" class="btn btn-purple btn-sm" data-toggle="modal">
												<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
											</a>
										</span> 
							</div>
							<div>
			
									
									<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
									<html:button property="search" styleClass="btn btn-primary btn-sm">Search</html:button>
							</div>	
				 
			          
			            </div>
			         </div>
			      </div>
			   </div>		
			<div class="space-6"></div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
			   <h5 class="widget-title lighter">Personal Information</h5>
			</div>
			 <div class="widget-body">
			   <div class="widget-main">
			     <div class="row">
			       <div class="col-lg-12">
			         <div class="form-group">
                        <label class="col-lg-2 control-label no-padding-right" for="Citizen No"> Citizen No : </label>

                      <div class="col-lg-4">
                   
                      </div>
                         <label class="col-lg-2 control-label no-padding-right" for="Date of Birth"> Date of Birth : </label>

                      <div class="col-lg-4">
                 
                     </div>
                 </div>
									
		     <div class="form-group">
                <label class="col-lg-2 control-label no-padding-right" for="Name"> Name : </label>

                  <div class="col-lg-4">
                 
                  </div>
              
                  <label class="col-lg-2 control-label no-padding-right" for="Blood Group"> Blood Group : </label>

                 <div class="col-lg-4">
                 
                 </div>
               </div>
              <h4> Permanent Address </h4>
                                        
                   <div class="form-group">
                       <label class="col-lg-2 control-label no-padding-right" for="Dzongkhag"> Dzongkhag : </label>

                           <div class="col-lg-4">
                           
                       </div>
                        
                       <label class="col-lg-2 control-label no-padding-right" for="Gewog"> Gewog : </label>

                           <div class="col-lg-4">
                           
                       
                      </div>
                 </div> 
                                         
               <div class="form-group">
                       <label class="col-lg-2 control-label no-padding-right" for="Village"> Village : </label>

                           <div class="col-lg-4">
                           
                       </div>
                        
                       <label class="col-lg-2 control-label no-padding-right" for="Country"> Country : </label>

                           <div class="col-lg-4">
                           
                       
                      </div>
                      </div> 
                      <div class="form-group">
                       <label class="col-lg-2 control-label no-padding-right" for="Address"> Address : </label>

                           <div class="col-lg-4">
                           
                       </div>
                      </div>  
                 
             
             </div>
		   </div>
			
		     </div>
	        </div>
           	</div>                       
             
             
        <div class="space-6"></div>
		     <div class="widget-box">
		       <div class="widget-header widget-header-small">
		         <h5 class="widget-title lighter">Refresher Course Entry Details</h5>
		       </div>
		   <div class="widget-body">
		       <div class="widget-main">
		         <div class="row">
		          <div class="col-xs-12"> 
                    <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Issued Date"> Issued Date :<span style="color: #ff0000">*</span> </label>
                       <div class="col-sm-4">
		               <div class="input-group">
								<html:text property="issuedDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
					   </div>
                       <label class="col-sm-2 control-label no-padding-right" for="Expiry Date"> Expiry Date :<span style="color: #ff0000">*</span> </label>

                        <div class="col-sm-4">
                           <div class="input-group">
								<html:text property="expiryDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
             
                      </div>
                    </div>                        
                    <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Region"> Region :<span style="color: #ff0000">*</span> </label>
                       <div class="col-sm-4">
			               <html:select property="region" styleClass="col-xs-10 col-sm-5" styleId="region">
						     <html:option value="">--------SELECT--------</html:option>
						   </html:select>
					   </div>
                       <label class="col-sm-2 control-label no-padding-right" for="Base"> Base :<span style="color: #ff0000">*</span> </label>

                        <div class="col-sm-4">
                           <html:select property="base" styleClass="col-xs-10 col-sm-5" styleId="base">
				              <html:option value="">--------SELECT--------</html:option>
				           </html:select>
              
                      </div>
                    </div>
                   <div class="form-group">
                      <label class="col-sm-2 control-label no-padding-right" for="Name of the Conductor"> Name of the Conductor : </label>
                       <div class="col-sm-4">
			               <html:text property="nameoftheConductor" styleClass="col-xs-10 col-sm-5" styleId="nameoftheConductor"></html:text>
					   </div>
                    </div>
                   </div>
                   </div>
                  </div>
                 </div>
                </div>  
                  
           
			<div class="pull-left">
			
				<html:button property="save" styleClass="btn btn-primary btn-sm">Save</html:button>
				<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
				<html:button property="delete" styleClass="btn btn-primary btn-sm">Delete</html:button>
				<html:button property="New" styleClass="btn btn-primary btn-sm">New</html:button>
				<html:button property="close" styleClass="btn btn-primary btn-sm">Close</html:button>
								
				 
			</div>
			
		</html:form>
		</div>
		</div>
	

<div id="modal-form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Owner Type"> Owner Type : </label>
	
                            <div class="col-sm-4">
                                <select class="col-xs-10 col-sm-7" id="form-field-select-1">
										 <option value=""></option>
							    </select> 
                            </div>
							
						</div>

						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Type"> Vehicle Type : </label>
	
                                 <div class="col-sm-4">
                                      <select class="col-xs-10 col-sm-7" id="form-field-select-1">
										 <option value=""></option>
							         </select> 
                                 </div>
								
						</div>
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Vehicle Number" placeholder="Vehicle Number"  />
                                 </div>
								
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Owner's Name"> Owner's Name : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="region" placeholder="Owner's Name"  />
                                 </div>
								
						</div>
					
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Citizen ID" placeholder="Citizen ID"  />
                                 </div>
				
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Engine Number" placeholder="Engine Number"  />
                                 </div>
				
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Chasis Number" placeholder="Chasis Number"  />
                                 </div>
				
						</div>
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-check"></i>
					Select
				</button>
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

            <script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
            <script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>
				
					