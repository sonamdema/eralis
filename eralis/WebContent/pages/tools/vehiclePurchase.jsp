<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />


<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Vehicle Purchase
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Vehicle Purchase
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
	<html:form styleClass="form-horizontal" action="/tools.html?method=vehicle_Purchase">
			<div class="row">
				<div class="col-sm-12">
					<div class="widget-body">
					  <div class="widget-main">
						<div class="row">
						  <div class="col-xs-12"> 
                                            
                                 <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Dealer Name"> Dealer Name : </label>

                                           <div class="col-sm-4">
                                             <html:select property="dealerName" styleClass="col-xs-10 col-sm-5" styleId="dealerName">
												<html:option value="">--------SELECT--------</html:option>
											</html:select>
                                           </div>
                                 </div>
                                 <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Vehicle Company"> Vehicle Company : </label>

                                           <div class="col-sm-4">
                                             <html:select property="vehicleCompany" styleClass="col-xs-10 col-sm-5" styleId="vehicleCompany">
												<html:option value="">--------SELECT--------</html:option>
											</html:select>
                                           </div>
                                 </div>
                               <div class="pull-left">
			
				                  <html:button property="search" styleClass="btn btn-primary btn-sm">Search</html:button>
				                  <html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
				
			                  </div>
                               
                            </div>   
				         </div>
			         </div>		
					</div>
				 </div>
			   </div>		
					
			<div class="space-6"></div>
			  <div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Vehicle Purchase Lists</h5>
				  </div>
				    <div class="widget-body">
					   <div class="widget-main">
						  <div class="row">
							  <div class="col-lg-12">
	
               
				               </div>                        
					        </div>
					      </div>
					     </div>
				       </div>  
         
		             <div class="space-6"></div>
					  <div class="widget-box">
						<div class="widget-header widget-header-small">
							<h5 class="widget-title lighter">Vehicle Purchase Details </h5>
						  </div>
						    <div class="widget-body">
							   <div class="widget-main">
								  <div class="row">
									  <div class="col-lg-12">
			                            
			                             <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Dealer Name"> Dealer Name :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             <html:select property="dealerName" styleClass="col-xs-10 col-sm-5" styleId="dealerName">
												<html:option value="">--------SELECT--------</html:option>
											</html:select>
                                           </div>
                                       
                                         <label class="col-sm-2 control-label no-padding-right" for="Engine CC"> Engine CC :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             
                                             <html:text property="engineCC" styleClass="col-xs-10 col-sm-5" styleId="engineCC"></html:text>
										</div>
                                           </div>
                                        <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Customer Name"> Customer Name :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             <html:text property="customerName" styleClass="col-xs-10 col-sm-5" styleId="customerName"></html:text>
                                           </div>
                                       
                                         <label class="col-sm-2 control-label no-padding-right" for="CID"> Citizen ID :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             
                                             <html:text property="citizenID" styleClass="col-xs-10 col-sm-5" styleId="citizenID"></html:text>
										</div>
                                           </div>
                                        <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Vehicle Company"> Vehicle Company :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             <html:select property="vehicleCompany" styleClass="col-xs-10 col-sm-5" styleId="vehicleCompany">
												<html:option value="">--------SELECT--------</html:option>
											</html:select>
                                           </div>
                                       
                                         <label class="col-sm-2 control-label no-padding-right" for="Vehicle Model"> Vehicle Model :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             
                                             <html:select property="vehicleModel" styleClass="col-xs-10 col-sm-5" styleId="vehicleModel">
												<html:option value="">--------SELECT--------</html:option>
											</html:select>
										</div>
                                           </div>
                                           <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Engine No"> Engine No :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             <html:text property="engineNo" styleClass="col-xs-10 col-sm-5" styleId="engineNo"></html:text>
                                           </div>
                                       
                                         <label class="col-sm-2 control-label no-padding-right" for="Chasis No"> Chasis No :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             
                                             <html:text property="chasisNo" styleClass="col-xs-10 col-sm-5" styleId="chasisNo"></html:text>
										</div>
                                           </div>
                                           <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Year of Manufacture"> Year of Manufacture :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             <html:text property="yearofManufacture" styleClass="col-xs-10 col-sm-5" styleId="yearofManufacture"></html:text>
                                           </div>
                                       
                                         <label class="col-sm-2 control-label no-padding-right" for="Country of Manufacture"> Country of Manufacture :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             
                                             <html:text property="countryofManufacture" styleClass="col-xs-10 col-sm-5" styleId="countryofManufacture"></html:text>
										</div>
                                           </div>
                                           <div class="form-group">
                                         <label class="col-sm-2 control-label no-padding-right" for="Invoice No"> Invoice No :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             <html:text property="invoiceNo" styleClass="col-xs-10 col-sm-5" styleId="invoiceNo"></html:text>
                                           </div>
                                       
                                         <label class="col-sm-2 control-label no-padding-right" for="Invoice Date"> Invoice Date :<span style="color: #ff0000">*</span> </label>

                                           <div class="col-sm-4">
                                             
                                             <div class="input-group">
									           <html:text property="invoiceDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
									          <span class="input-group-addon">
										      <i class="fa fa-calendar bigger-110"></i>
									        </span>
								        </div>
										</div>
                                           </div>
                                     
                                     </div>
                 
						           </div>                        
							     </div>
							   </div>
							</div>
		
			
			
			<div class="pull-left">
			
				<html:button property="replace" styleClass="btn btn-primary btn-sm">Save</html:button>
				<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
				<html:button property="delete" styleClass="btn btn-primary btn-sm">Delete</html:button>
				<html:button property="new" styleClass="btn btn-primary btn-sm">New</html:button>
				<html:button property="close" styleClass="btn btn-primary btn-sm">Close</html:button>
								
				 
			</div>
		</html:form>
	</div>
</div>

            <script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
            <script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>