<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Road Block Entry
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			 Road Block Entry
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
		<html:form styleClass="form-horizontal" styleId="roadBlock" action="/tools.html?method=road_Block_Entry" >
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Search Road Block</h5>
				</div>						
				<div class="widget-body">
					<div class="widget-main">
				 		<div class=" "> 
                          	<div class="form-group">
                              	<label class="col-sm-2 control-label no-padding-right" for="Route From"> Route From : </label>
                               	<div class="col-sm-3">
									<select class="col-sm-8 modal_input"  id="routeFrom"   >
										<option value="">SELECT</option>
										<logic:iterate id="route" name="routeList">
											<option value='<bean:write name="route" property="headerId"/>'>
												<bean:write name="route" property="headerName"/>
											</option>
										</logic:iterate>
									</select> 
                         		</div>
                                <label class="col-sm-1 control-label no-padding-right" for="To">Route To : </label>
                                <div class="col-sm-3">
                               		<select class="col-sm-8 modal_input"  id="routeTo"   >
										<option value="">SELECT</option>
										<logic:iterate id="route" name="routeList">
											<option value='<bean:write name="route" property="headerId"/>'>
												<bean:write name="route" property="headerName"/>
											</option>
										</logic:iterate>
									</select> 
                           		</div>
	                         	<div class="col-sm-2">
	                				<html:button property="search" styleClass="btn btn-primary btn-sm" onclick="getroadBlock()">Search</html:button>
	               				</div>
	                		</div>
	                    </div> 
	            	</div>
	            </div>
           	</div>  
			<div id="road_block_list"></div>
			<div class="space-6"></div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Entry Road Block</h5>
				</div>	
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
				               	<div class="form-group">
		        	             	<label class="col-sm-2 control-label no-padding-right" for="Block Location"> Block Location :<span style="color: #ff0000">*</span> </label>
                	              	<div class="col-sm-4">
                                     	<html:text property="blockLocation" styleClass="col-xs-12" styleId="blockLocation"></html:text>
                                	</div>
	                        	</div>
        		                <div class="form-group">
                                  	<label class="col-sm-2 control-label no-padding-right" for="Block Date"> Block Date :<span style="color: #ff0000">*</span> </label>
                                  	<div class="col-sm-4">
                                       	<div class="input-group">
								        	<html:text property="blockDate" styleClass="form-control date-picker" styleId="blockDate"></html:text>
								      		<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
										</div>
                              		</div>
                          		</div>     
                                <div class="form-group">
                                	<label class="col-sm-2 control-label no-padding-right" for="Route From"> Route From : </label>
	                                <div class="col-sm-4">
                                    	<select class="col-sm-8"  id="routeFrom1"   >
											<option value="">SELECT</option>
											<logic:iterate id="route" name="routeList">
											<option value='<bean:write name="route" property="headerId"/>'>
											<bean:write name="route" property="headerName"/>
											</option>
											</logic:iterate>
										</select> 
                               		</div>
                                    <label class="col-sm-1 control-label no-padding-right" for="To"> To : </label>
	                               	<div class="col-sm-4">
	                                	<select class="col-sm-8 modal_input"  id="routeTo1"   >
											<option value="">SELECT</option>
											<logic:iterate id="route" name="routeList">
												<option value='<bean:write name="route" property="headerId"/>'>
												<bean:write name="route" property="headerName"/>
												</option>
											</logic:iterate>
										</select> 
	                           		</div>
                            	</div>
                           		<div class="form-group">
                                 	<label class="col-sm-2 control-label no-padding-right" for="Open Date"> Open Date :<span style="color: #ff0000">*</span> </label>
                                   	<div class="col-sm-4">
                                    	<div class="input-group">
								       		<html:text property="openDate" styleClass="form-control date-picker" styleId="openDate"></html:text>
							     			<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
										</div>
                      				</div>
                      			</div>
		                   		<div class="form-group">
                                	<label class="col-sm-2 control-label no-padding-right" for="UpLoad"> UpLoad : </label>
                                	<div class="col-sm-4">
                                    	<label id="photo1"></label>
                                     	<html:file property="photoUrl1" styleClass="form-control" styleId="id-input-file-2"></html:file>
                                   	</div>
                               	</div> 
                         	    <div class="form-group">
                                	<label class="col-sm-2 control-label no-padding-right" for="UpLoad"> UpLoad : </label>
                                    <div class="col-sm-4">
                                    	<label id="photo2"></label>
                                        <html:file property="photoUrl2" styleClass="form-control" styleId="id-input-file-2"></html:file>
                               		</div>
                               	</div> 
                               	<div class="form-group">
                                	<label class="col-sm-2 control-label no-padding-right" for="UpLoad"> UpLoad : </label>
                                    <div class="col-sm-4">
	                                	<label id="photo3"></label>
	                                    <html:file property="photoUrl3" styleClass="form-control" styleId="id-input-file-2"></html:file>
                                    </div>
                               	</div>  
				            </div>                        
					  	</div>
					</div>
				</div>
			</div>
	       	<div class="pull-left">
				<html:hidden property="photoId"  styleId="photoId"></html:hidden>
				<html:hidden property="row_id"  styleId="row_id"></html:hidden>
				<html:button property="new" styleClass="btn btn-primary btn-sm">New</html:button>
				<button type="button" class="btn btn-primary btn-sm" onclick="editRoadBlock()" id="editButton" style="display:none;">Save Edited</button>
				<html:button property="new" styleClass="btn btn-primary btn-sm" onclick="formSubmit()">Save</html:button>
				<html:button property="delete" styleClass="btn btn-primary btn-sm" onclick="deleteRoadBlock()">Delete</html:button>
				<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button> 
				<html:button property="close" styleClass="btn btn-primary btn-sm">Close</html:button>
			</div>
		</html:form>
	</div>
</div>
<script type="text/javascript">
<!--
$(document).ready(function() 
{
	$(' #id-input-file-2').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:false,
		onchange:null,
		thumbnail:false 
	});
});
//-->
</script>

            <script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
            <script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 
$('.date-picker').datepicker({
	autoclose: true,
	todayHighlight: true
})
.next().on(ace.click_event, function(){
	$(this).prev().focus();
});

</script>
<script>
	function getroadBlock()
	{ 
		var routeTo 	=	$("#routeTo").val();
		var routeFrom	=	$("#routeFrom").val();
	
		if(routeTo=='')routeTo="Na";
		if(routeFrom=='')routeFrom="Na"; 
	
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/tools.html?method=getRoadBlockList&routeTo="+routeTo+"&routeFrom="+routeFrom,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#road_block_list").html(responseText);
				$("#road_block_list").show();
			}
		});
	
	}		
	function blockData(blockLocation,blockDate,routeFrom,routeTo,openDate,row_id,photo1,photo2,photo3,photoId){ 
		 
		$("#blockLocation").val(blockLocation);
		$("#blockDate").val(blockDate);
		$("#openDate").val(openDate);  
		$("#routeFrom1").val(routeFrom);
		$("#routeTo1").val(routeTo);
		$("#row_id").val(row_id);
	
		$("#photo1").html(photo1);
		$("#photo2").html(photo2);
		$("#photo3").html(photo3);
		$("#photoId").html(photoId);
		$("#editButton").show();
		$("#saveButton").hide();
	}
	function formSubmit()
	{	var routeFrom	=	$("#routeFrom1").val();
		var routeTo		=	$("#routeTo1").val();
		
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/tools.html?method=road_Block_Entry&routeFrom='+routeFrom+'&routeTo='+routeTo,type:'POST',data: $("#roadBlock").serialize()}; 
	    $("#roadBlock").ajaxSubmit(options);
	    setTimeout('hideStatus("messageDiv")',4000);
	}
	function editRoadBlock()
	{	var routeFrom	=	$("#routeFrom1").val();
		var routeTo	=	$("#routeTo1").val();
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/tools.html?method=edit_Road_Block_Entry&routeFrom='+routeFrom+"&routeTo="+routeTo,type:'POST',data: $("#roadBlock").serialize()}; 
	    $("#roadBlock").ajaxSubmit(options);
	    setTimeout('hideStatus("messageDiv")',4000);
	 
	}
	function deleteRoadBlock()
	{ 
		var row_id=$("#row_id").val();
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/tools.html?method=delete_Road_Block_Entry&row_id='+row_id,type:'POST',data: $("#roadBlock").serialize()}; 
		$("#roadBlock").ajaxSubmit(options);
		setTimeout('hideStatus("messageDiv")',4000);
	}
</script>
				

