<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Delete Drive Type
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
	 	Delete Drive Type
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-lg-12 col-sm-4">
	<html:form styleClass="form-horizontal" action="/tools.html?method=delete_Drive_Type" styleId="driveTypeForm">
		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-xs-12 col-sm-7">
						<div class="form-group" > 
						  <label class="col-sm-4 control-label" for="License Number"> License Number :<span style="color: #ff0000">*</span></label>
							<div class="col-sm-5 no-padding";>
           			        	<html:text property="licenseNumber" readonly="true"  styleClass="col-xs-12" styleId="licenseNO"></html:text>
                        	</div>
							<span class="input-group-btn">
								<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
									<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>		
		<div class="space-6"></div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Personal Information</h5>
			  	</div>
			    <div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
         						<div class="form-group">
                  					<label class="col-lg-2 control-label no-padding-right" for="Owner Type"> Name : </label>
                      				<div class="col-lg-4" id="displayOwnerName"></div>
                  					<label class="col-lg-2 control-label no-padding-right" for="Customer ID"> Customer ID : </label>
				                    <div class="col-lg-4" id="customerID" > </div>
                				</div>
         						<div class="form-group">
				                    <label class="col-lg-2 control-label no-padding-right" for="Citizenship ID"> Citizenship ID : </label>
				                    <div class="col-lg-4" id="cid"></div> 
				                    <label class="col-lg-2 control-label no-padding-right" for="Date of birth"> Date of birth : </label>
				                    <div class="col-lg-4" id="displayDob"></div>
              					</div>
					            <div class="form-group">
					            	<label class="col-lg-2 control-label no-padding-right" for="Identification Marks"> Identification Marks : </label>
						                <div class="col-lg-4" id="identificationMarks">
					                    </div>
					                    <label class="col-lg-2 control-label no-padding-right" for="Father's Name"> Father's Name : </label>
						                <div class="col-lg-4" id="fathersname"></div>
               					</div>                    
           						<div class="form-group">
                    				<label class="col-lg-2 control-label no-padding-right" for="Nationality"> Nationality : </label>
                    				<div class="col-lg-4" id="nationality"></div>
                       				<label class="col-lg-2 control-label no-padding-right" for="Gender"> Gender : </label>
									<div class="col-lg-4" id="gender"></div>
               					</div>      
     							<h4> Permanent Address(National) </h4>
                 				<div class="form-group">
                    				<label class="col-lg-2 control-label no-padding-right" for="Dzongkhag"> Dzongkhag : </label>
									<div class="col-lg-4" id="displayDzongkhag"></div>
                     				<label class="col-lg-2 control-label no-padding-right" for="Gewog"> Gewog : </label>
	                        		<div class="col-lg-4" id="displayGewog"></div>
              					</div> 
                                <div class="form-group">
                    				<label class="col-lg-2 control-label no-padding-right" for="Village"> Village : </label>
                        			<div class="col-lg-4" id="village">
                    				</div>
             					</div> 
                				<h4> Permanent Address(Non-National) </h4>  
                				<div class="form-group">
                    				<label class="col-lg-2 control-label no-padding-right" for="Dzongkhag"> Country : </label>
									<div class="col-lg-4" id="displayCountry"></div>
                     				<label class="col-lg-2 control-label no-padding-right" for="Gewog"> Address : </label>
									<div class="col-lg-4" id="displayAddress"></div>
             					</div> 
                                <div class="form-group">
                    				<label class="col-lg-2 control-label no-padding-right" for="Village"> Drive Type :<span style="color: #ff0000">*</span> </label>
           							<div class="col-lg-4"></div>
								</div>               
 								<div id="Panel2" style="background-color:White;border-color:CornflowerBlue;border-width:1px;border-style:Solid;height:68px;width:1030px;overflow-y: scroll">
                  					<table id="chkDriveTypeList" border="0">
									</table>
            					</div>                       
             				</div>                        
   						</div>
  					</div>
 				</div>
     		</div>   
			<div class="pull-left">  
				<html:hidden property="driveTypeId"   styleClass="col-xs-12" styleId="driveTypeId"></html:hidden>
				<html:button property="delete" styleClass="btn btn-primary btn-sm" onclick="formSubmit()">Delete</html:button> 
				<html:button property="refresh" styleClass="btn btn-primary btn-sm">Refresh</html:button>
				<html:button property="close" styleClass="btn btn-primary btn-sm">Close</html:button>
			</div>
			<div id="displayMsgDiv"></div>
		</html:form>
	</div>
</div>
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>License No:</label>
								<span class="pull-right">
									<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
								</span>
							</div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
		</div>
	</div>
</div>
<script>
var customerId="";

 
function searchPersonalInfo()
{
	var cidNumber 	= $('#cidPersonalModal').val();
	var licenseNo	= $('#licenseNoModal').val();
	
	if(cidNumber == "")
		cidNumber = "NA";
	if(licenseNo == "")
		licenseNo = "NA";
	
	$.ajax
	({
		type : "POST",
		url : "<%=request.getContextPath()%>/tools.html?method=getLicenseHolderList&cid="+cidNumber+"&licenseNo="+licenseNo,
		data : $('form').serialize(),
		cache : false,
		dataType : "html",
		success : function(responseText) 
		{
			$("#licenseDetailList").html(responseText);
			$("#licenseDetailList").show();
		}
	});
} 
function formSubmit()
{   customerId	=	$('.driveTypeCustomerId').val();
	
	getCheckedVals(); 
	var options = {target:'#displayMsgDiv',url:context+'/tools.html?method=delete_Drive_Type&customerId='+customerId,type:'POST',data: $("#driveTypeForm").serialize()}; 
	 
    $("#driveTypeForm").ajaxSubmit(options);
    $('#displayMsgDiv').show();
    setTimeout('hideStatus("displayMsgDiv")',4000);
    setTimeout("getToolsDetails(customerId)",1000);
}
function getToolsDetails(customerId)
{  
	$.ajax
	({
		async: true,
		type: 'POST',
		url: '<%=request.getContextPath()%>/tools.html?method=getDriveType&customerId='+customerId,
		success: function(xml)
		{	 
			$("#chkDriveTypeList").html(xml); 
		}
	}); 
}
</script>
