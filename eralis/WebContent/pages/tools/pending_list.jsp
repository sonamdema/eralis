<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="table-responsive">
	<table id="pending-application-list" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Action</th>
				<th>Pending Date</th>
				<th>Created By</th>
				<th>Remarks</th>
			</tr>
		</thead>
		<tbody>
			<logic:iterate id="pending" name="PENDING_LIST">
				<tr>
					<td>
						<button type="button"   class="btn btn-minier btn-primary" id='<bean:write name="pending" property="pendingId"/>' onclick="getSelectedPendingTask(this.id)">Clear</button>
					</td>
					<td><bean:write name="pending" property="dateOfissue"/></td>
					<td><bean:write name="pending" property="name"/></td>
					<td><bean:write name="pending" property="remarks"/></td>
				</tr>
			</logic:iterate>
		</tbody>
	</table>
</div>
<script>
$(document).ready(function() 
{
  		$('#pending-application-list').DataTable({
           responsive: true
   	});
});
</script>