<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="dynamic-table1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>  
			<!--<th></th>-->
			<th>Punch No</th> 
			<th>Punch Date</th> 
			<th>Letter No</th> 
			<th>Letter Date</th> 
			<th>Withdrawn Date</th> 
			<th>Withdrawn Letter No</th> 
			<th>Withdrawn Letter Date</th>
			<th>Remarks</th> 
			<th>Status</th> 
		</tr>
	</thead>
	<tbody> 
		<logic:notEmpty name="LICENSE_PUNCH_LIST">
			<logic:iterate id="punch" name="LICENSE_PUNCH_LIST"  indexId="index">
			<%
				int i = index.intValue()+1;
			%>
				<tr> 
					<td><%=i%></td> 
					<td><bean:write name="punch" property="punchDate"/></td>
					<td><bean:write name="punch" property="letterNo"/></td>
					<td><bean:write name="punch" property="letterDate"/></td>
					
					<td><bean:write name="punch" property="withdrawnDate"/></td>
					<td><bean:write name="punch" property="withdrawnLetterNo"/></td>
					<td><bean:write name="punch" property="withdrawnLetterDate"/></td>
					<td><bean:write name="punch" property="remarks"/></td>
					
					<td><bean:write name="punch" property="isWithDrawn"/></td>
				 </tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table> 
<script>
$(document).ready(function() 
{
    $('#dynamic-table1').DataTable({
            responsive: true
    });
});
</script>