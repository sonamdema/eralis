<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		License Print
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Driving License Print Information
		</small>
	</h1>
          </div><!-- /.page-header -->
          <div class="row">
	       <div class="col-lg-12 col-sm-4">
	       <html:form styleClass="form-horizontal" action="/tools.html?method=license_Print">
			   <div class="row">
			      <div class="col-sm-12">
			         <div class="row">
			            <div class="col-xs-12">
			               <div class="form-group" > 
								  <label class="col-sm-2 control-label" for="License Number"> License Number :  </label>

                                           <div class="col-sm-3 no-padding";>
                                             <html:text property="licenseNumber" styleClass="col-xs-12" styleId="licenseNumber"></html:text>
                                           </div>
										<span class="input-group-btn">
									      	<a href="#modal-form" class="btn btn-purple btn-sm" data-toggle="modal">
												<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
											</a>
										</span> 
							</div>
							 <div class="form-group">
                     
                         <div class="checkbox">
                             <label class="col-sm-3 control-label no-padding-right">
                                <html:checkbox property="isWithDrawn" styleClass="ace"></html:checkbox>
                                <span class="lbl">Long Name</span>
                            
                             </label>
                          </div>     
                          </div>   
			            </div>
			         </div>
			      </div>
			   </div>		
		
           	 <div class="space-6"></div>
		   <div class="widget-box">
			<div class="widget-header widget-header-small">
			   <h5 class="widget-title lighter"> License Print Information </h5>
			</div>
			 <div class="widget-body">
			   <div class="widget-main">
			     <div class="row">
			       <div class="col-lg-12"> 
			       
			     </div>
		    </div>
	     </div>
	   </div> 
	  </div>         
             
       
			<div class="pull-right">
			
				<html:button property="print" styleClass="btn btn-primary btn-sm">Print</html:button>
					
				 
			</div>
			
		</html:form>
		</div>
		</div>
	

 <div id="modal-form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">

					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right" for="Owner Type"> Owner Type : </label>
	
                            <div class="col-sm-4">
                                <select class="col-xs-10 col-sm-7" id="form-field-select-1">
										 <option value=""></option>
							    </select> 
                            </div>
							
						</div>

						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Type"> Vehicle Type : </label>
	
                                 <div class="col-sm-4">
                                      <select class="col-xs-10 col-sm-7" id="form-field-select-1">
										 <option value=""></option>
							         </select> 
                                 </div>
								
						</div>
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Vehicle Number" placeholder="Vehicle Number"  />
                                 </div>
								
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Owner's Name"> Owner's Name : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="region" placeholder="Owner's Name"  />
                                 </div>
								
						</div>
					
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Citizen ID" placeholder="Citizen ID"  />
                                 </div>
				
						</div>
						
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Engine Number" placeholder="Engine Number"  />
                                 </div>
				
						</div>
						<div class="form-group">
								
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="Chasis Number" placeholder="Chasis Number"  />
                                 </div>
				
						</div>
						</form>
						
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>

				<button class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i>
					Reset
				</button>
				
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-check"></i>
					Select
				</button>
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	