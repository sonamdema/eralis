<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
 <logic:notEmpty name="DRIVE_TYPE">
	<logic:iterate id="driveType" name="DRIVE_TYPE" indexId="index">
		<%
			int i = index.intValue();
		%> 
		<label>
			<input type="checkbox"  class="ace" id="driveTypeId_<%=i%>" value="<bean:write name="driveType" property="driveTypeId"/>"/>
			<span class="lbl">
				<bean:write name="driveType" property="driveTypeName"/> 
				<input type="hidden" value="<bean:write name="driveType" property="tableName"/>" id="tableName_<%=i%>">
				<input type="hidden" value="<bean:write name="driveType" property="customerId"/>" class="driveTypeCustomerId">
			</span>
		</label>
	</logic:iterate>
</logic:notEmpty>    
<script>


function getCheckedVals()
{	 
	var listSize = "<%= request.getAttribute("DRIVE_TYPE_LIST_SIZE")%>";
	var arrLen = listSize; 
	var vals = '';
	var countFlag = 0;

 	for(var i=0;i<arrLen;i++)
  	{
	  	var driveTypeId = "0";
	  	var tableName = "";
	  	
	 	if($('#driveTypeId_'+i).is(':checked'))
	 		driveTypeId = $('#driveTypeId_'+i).val();
	 		tableName 	= $('#tableName_'+i).val();

 		if(parseInt(countFlag) > 0){
			vals = vals + '#';
	 	}

	 	vals = vals + driveTypeId+"_"+tableName;
	 	countFlag++;
  	}
  	
	$('#driveTypeId').val(vals+"#0_0");
}
</script>