<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table class="table table-striped table-bordered table-hover">
	<caption><h5>Vehicle Renewal History</h5></caption>
	<thead>
		<tr>
			<th>Sl No</th>
			<th>Renewal Date</th>
			<th>Expiry Date</th> 
			<th>Receipt No</th> 
			<th>Receipt Date</th> 
			
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="RENEWAL_HISTORY">
			<logic:iterate id="renewal" name="RENEWAL_HISTORY" indexId="index">
				<%
					int a = index.intValue();
				%>
				<tr>
					<td>
						<%=++a %>
					</td>
					<td><bean:write name="renewal" property="renewalDate"/></td>
					<td><bean:write name="renewal" property="expiryDate"/></td>
					<td><bean:write name="renewal" property="receiptNo"/></td> 
					<td><bean:write name="renewal" property="receiptDate"/></td> 
				</tr>
			</logic:iterate>
		</logic:notEmpty>
		<logic:empty name="RENEWAL_HISTORY">
			<tr>
				<td colspan="7" align="center">
					<font color='red'>NO RECORD FOUND</font>
				</td>
			</tr>
		</logic:empty>
	</tbody>
</table>