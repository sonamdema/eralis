<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<div id="NOCDIV">
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			Issuance of No Objection Certificate
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				issue NOC for a vehicle sold to foreign nationals
			</small>
		</h1>
	</div><!-- /.page-header -->
	
	<html:form styleId="nocForm" action="/eralis_common.html?method=noc_issuance" method="POST" styleClass="form-horizontal">
		
		<div class="row">
			<div id="incompleteValidationMsg"></div>
			<div id="Msg"></div>
			<div class="col-lg-12">
				<div id="messageDiv" style="display: none;"></div>
				<div class="form-group">
				 	<div class="col-lg-2">
						<label>Vehicle Number:</label>
				 	</div>
					<div class="col-lg-3">
						<div class="input-group">
							<input type="text" name="vehicleNumber" class="form-control" id="displayVehicleNumber" readonly="readonly"/>
							  <span class="input-group-btn">
						      	 <a href="#" onclick="openModal('renewal')" class="btn btn-purple btn-sm">
							  		<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
								</a>
							  </span>
						 </div>
					 </div>
				 </div>
			</div>
		</div>
	
		<div class="row">
			<div class="col-xs-12">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title">Vehicle Information</h4>
					</div>
					<div class="widget-body">
						<div class="widget-main">

							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="col-lg-3">
											<label>Owner Name</label>
										</div>
										<div class="col-lg-3">
											<input type="text" class="form-control" id="ownerName" readonly="readonly"/>
										</div>
										<div class="col-lg-3">
											<label>Model</label>
										</div>
										<div class="col-lg-3">
											<input type="text" class="form-control" id="model" readonly="readonly"/>
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-3">
											<label>Make</label>
										</div>
										<div class="col-lg-3">
											<input type="text" class="form-control" id="make" readonly="readonly"/>
										</div>
										<div class="col-lg-3">
											<label>Engine Number</label>
										</div>
										<div class="col-lg-3">
											<input type="text" class="form-control" id="engineNo" readonly="readonly"/>
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-3">
											<label>Chassis Number</label>
										</div>
										<div class="col-lg-3">
											<input type="text" class="form-control" id="chassisNo" readonly="readonly"/>
										</div>
										<div class="col-lg-3">
											<label>Manufacture Year</label>
										</div>
										<div class="col-lg-3">
											<input type="text" class="form-control" id="manufactureYear" readonly="readonly"/>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-lg-12">
											<div id="renewalHistoryDiv"></div>
										</div>
									</div>
									
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title">Buyer Information</h4>
					</div>
					<div class="widget-body">
						<div class="widget-main">

							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="col-lg-3">
											<label>Buyer's Name :<span style="color: #ff0000">*</span></label>
										</div>
										<div class="col-lg-3">
											<html:text property="buyerName" styleClass="form-control" styleId="buyerName"></html:text>
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-3">
											<label>Buyer's CID :<span style="color: #ff0000">*</span></label>
										</div>
										<div class="col-lg-3">
											<html:text property="buyerCID" styleClass="form-control" styleId="buyerCID"></html:text>
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-3">
											<label>Buyer's Address :<span style="color: #ff0000">*</span></label>
										</div>
										<div class="col-lg-3">
											<html:textarea property="buyerAddress" styleClass="form-control" styleId="buyerAddress" rows="5" cols="20"></html:textarea>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title">NOC Information</h4>
					</div>
					<div class="widget-body">
						<div class="widget-main">

							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="col-lg-3">
											<label>Sale Deed Date :<span style="color: #ff0000">*</span></label>
										</div>
					                     <div class="col-lg-3">
			                                 <div class="input-group">
												<html:text property="saleDeedDate" styleClass="form-control date-picker" styleId="saleDeedDate" readonly="true"></html:text>
												<span class="input-group-addon" style="cursor:pointer;">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
			                             </div>
										<div class="col-lg-3">
											<label>Sale Deed Amount :<span style="color: #ff0000">*</span></label>
										</div>
					                     <div class="col-lg-3">
			                                 <html:text property="saleDeedAmount" styleClass="form-control" styleId="saleDeedAmount"></html:text>
			                             </div>
				                    </div>
									<div class="form-group">
										<div class="col-lg-3">
											<label>NOC Fees :<span style="color: #ff0000">*</span></label>
										</div>
										<div class="col-lg-3">
											<html:text property="nocFees" styleClass="form-control" styleId="nocFees"></html:text>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					<html:hidden property="customerID" styleId="customerID"/>
					<html:hidden property="ownerType" styleId="ownerType"/>
					<html:hidden property="vehicleId" styleId="vehicleId"/>
					<input type="hidden" name="engineType" id="engineType">
					<input type="hidden" name="vehicleType" id="vehicleType">
					<input type="hidden" name="loadCapacity" id="loadCapacity">
					<input type="hidden" name="seatCapacity" id="seatCapacity">
					<input type="hidden" name="vehicleHorsePower" id="vehicleHorsePower">
					<input type="hidden" name="vehicleKiloWatt" id="vehicleKiloWatt">
					<input type="hidden" name="engineCC" id="engineCC">
					<button type="button" class="btn btn-primary btn-sm" onclick="issueNOC()">Calculate Payment</button>
				</div>
			</div>
		</div>
		<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
	</html:form>
	
	<div id="renewal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12"> 
                          <form class="form-horizontal" role="form">
					
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Vehicle Number"> Vehicle Number : </label>
	
                                 <div class="col-sm-4">
                                      <input type="text" id="vehicleNumberRenewalModal" placeholder="Vehicle Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Citizen ID"> Citizen ID : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="citizenIdRenewalModal" placeholder="Citizen ID"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Engine Number"> Engine Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="engineNumberRenewalModal" placeholder="Engine Number"  />
                                 </div>
						</div>
						<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Chasis Number"> Chasis Number : </label>
                                 <div class="col-sm-4">
                                      <input type="text" id="chasisNumberRenewalModal" placeholder="Chasis Number"  />
                                 </div>
						   </div>
						</form>
					</div>
				</div>
			 </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm" name="search" onclick="searchRenewalInfo()">
						<i class="ace-icon fa fa-search"></i>
						Search
					</button>
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>
				  </div>
				 <div id="renewalListTable">
			   </div>
			 </div>
		  </div>
	   </div><!-- PAGE CONTENT ENDS -->
	
</div>
	
	<script>

		var context = "<%=request.getContextPath()%>";

		$(document).ready(function()
		{
			$('#personalRadio').attr('checked', true);

			//datepicker plugin
			$('.date-picker').datepicker({
				autoclose : true,
				todayHighlight : true
			})
			.next().on(ace.click_event, function() {
				$(this).prev().focus();
			});
		});

		function showSearchDiv(identifier)
		{
			if(identifier == "P")
			{
				$('#personalDiv').show();
				$('#organizationDiv').hide();
			}
			else
			{
				$('#personalDiv').hide();
				$('#organizationDiv').show();
			}
		}

		function getOwnerInformation()
		{
			var vehicleNo = $('#vehicleNumber').val();
			$.ajax
			({
				async: true,
				type: 'POST',
				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getOwnerInformation&vehicleNo='+vehicleNo,
				success: function(xml)
				{
					$(xml).find('xml-response').each(function()
					{
						var name = $(this).find('name').text();
						var customerId = $(this).find('customer-id').text();
						var ownerType = $(this).find('owner-type').text();
						var model = $(this).find('model').text();
						var make = $(this).find('make').text();
						var engineNo = $(this).find('engine-number').text();
						var chassisNo = $(this).find('chassis-number').text();
						var manufactureYear = $(this).find('manufacture-year').text();
						var status = $(this).find('status').text();

						if(status == "CANCELLED")
						{
							$("#messageDiv").html("<div class='alert alert-danger'>This vehicle is already cancelled</div>");
							$('#messageDiv').show();
					        setTimeout('hideStatus("messageDiv")',3000);
						}
						else if(status == "FAILED")
						{
							$("#messageDiv").html("<div class='alert alert-danger'>Something went wrong, please try again</div>");
							$('#messageDiv').show();
					        setTimeout('hideStatus("messageDiv")',3000);
						}
						else
						{
							$('#ownerName').val(name);
							$('#model').val(model);
							$('#make').val(make);
							$('#engineNo').val(engineNo);
							$('#chassisNo').val(chassisNo);
							$('#manufactureYear').val(manufactureYear);
							$('#customerId').val(customerId);
							$('#ownerType').val(ownerType);

							getVehicleRenewalHistory(vehicleNo);
						}
					});
				}
			});
		}

		function getVehicleRenewalHistory(vehicleNo)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getVehicleRenewalHistory&vehicleNo="+vehicleNo,
				
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#renewalHistoryDiv").html(responseText);
					$('#renewalHistoryDiv').show();
				}
			});
		}

		function issueNOC()
		{
			var identityTypeId;
		    var requestType = "VEHICLE";
			var serviceType = "TRANSFER";
			var identityNo = $('#vehicleId').val();
			if( $("#engineType").val() == "Electric")
				   identityTypeId = '0'; 
			else
				identityTypeId = $('#vehicleType').val();
			var loadingCapacity = $('#loadCapacity').val();
			var seatingCapacity = $('#seatCapacity').val();
			var vehicleHP = $('#vehicleHorsePower').val();
			var kilowatts = $('#vehicleKiloWatt').val();
			var engineCC = $('#engineCC').val();
			var purchaseDate = "";
			var saleDeedAmount = $('#saleDeedAmount').val();
			var saleDeedDate = $('#saleDeedDate').val();
			$("#trasacationType").val("nocIssuance");
			
			getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate,"","NORMAL");
		}
		

		function formSubmit()
		{
	        $.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/eralis_common.html?method=noc_issuance",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					var mywindow = window.open('', 'my div', 'height=400,width=600');
					mywindow.document.write(responseText);

					setTimeout(function() {
						mywindow.print();
						mywindow.close();
					}, 400);

					$('#NOCDIV').show();
				}
			});
		}

		function searchRenewalInfo()
		{ 
			var vehicleNumber = $('#vehicleNumberRenewalModal').val();
			var engineNumber = $('#engineNumberRenewalModal').val();
			var chasisNumber = $('#chasisNumberRenewalModal').val();
			var cidNumber = $('#citizenIdRenewalModal').val();
			if(vehicleNumber == "")
				vehicleNumber = "NA";
			if(engineNumber == "")
				engineNumber = "NA";
			if(chasisNumber == "")
				chasisNumber = "NA";
			if(cidNumber == "")
				cidNumber = "NA";

			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getRenewalInfoList&vehicleNumber="+vehicleNumber+"&engineNumber="+engineNumber+"&chasisNumber="+chasisNumber+"&cidNumber="+cidNumber+"&type=NA",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#renewalListTable").html(responseText);
					$("#renewalListTable").show();
				}
			});
		}
		 
		function searchVehicleNumber()
		{
			var vehicleId = $('#vehicleId').val();
		}
		
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";

	</script>