<%@page import="bt.gov.rsta.eralis.dto.noc.NOCDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
	NOCDTO dto = (NOCDTO)request.getAttribute("NOC_DETAILS");
%>
	<div class="row">
		<div class="col-xs-12">
			<span class="pull-right">
				<a class="btn btn-app btn-yellow btn-xs" href="javascript:void(0)" id="exportBTN">
					<i class="ace-icon fa fa-file-word-o bigger-160"></i>
					Export
				</a>
			</span>
		</div>
	</div>
	<br><br>
	<div id="nocLetterDiv">
	
		<div class="row">
			<div class="col-xs-12">
			
				<div class="row">
					<div class="col-lg-12">
						<span class="pull-left"><strong><%=dto.getNocNumber() %></strong></span>
						<span class="pull-right"><strong>Date:&nbsp;<%=dto.getCancellationDate() %></strong></span>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-12" align="center">
						<span><strong><u>NO OBJECTION CERTIFICATE</u></strong></span>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-12">
						<p style="text-align: justify;">
							This is to certify that vehicle bearing registration number <%=dto.getVehicleNo() %> (<%=dto.getModel() %>) with chassis no.
							<%=dto.getChassisNo() %>, Engine no. <%=dto.getEngineNo() %> and manufacture year <%=dto.getManufactureYear() %> has been
							cancelled with effect from <%=dto.getCancellationDate() %> under the ownership of <%=dto.getSellerName() %> and sold to <%=dto.getBuyerName() %>
							bearing CID no <%=dto.getBuyerCID() %> of <%=dto.getBuyerAddress().trim() %>.
							<br><br>
							All the taxes have been cleared up to <%=dto.getLastRenewalDate() %> and 5% property transfer tax has also been paid in Bhutan. Therefore,
							the Royal Government of Bhutan has no objection to <%=dto.getBuyerName() %> to register the said vehicle outside Bhutan. The NOC fees 
							Nu.&nbsp;<%=dto.getAmountPaid() %> has been collected vide receipt no. <%=dto.getReceiptNo() %> dated <%=dto.getReceiptDate() %> and the
							registration of the above vehicle has been duly cancelled in our record.
						</p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<span class="pull-left">
							Regional Transport Officer<br>
							Road Safety & Transport Authority<br>
							<%=dto.getOfficeName() %>:Bhutan
						</span>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-12">
						<span class="pull-left">
							<strong>Copy to:</strong><br>
							1.&nbsp;The Regional Transport Officer, <%=dto.getBuyerAddress() %>.<br>
							2.&nbsp;<%=dto.getBuyerName().trim() %> for necessary action.<br>
							3.&nbsp;Office copy.
						</span>
					</div>
				</div>
				
			</div>
		</div>
	
	</div>

<script>

	var vehicleNo = "<%=dto.getVehicleNo() %>";

	$(document).ready(function()
	{
		$("#exportBTN").click(function(event) {
			$("#nocLetterDiv").wordExport("noc-letter("+vehicleNo+")");
		});
	});

</script>