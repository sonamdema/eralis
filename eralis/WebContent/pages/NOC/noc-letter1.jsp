<%@page import="bt.gov.rsta.framework.util.Constants"%>
<%@page import="bt.gov.rsta.framework.dto.Role"%>
<%@page import="bt.gov.rsta.framework.dto.EralisUserRolePriviledge"%>
<%@page import="bt.gov.rsta.eralis.dto.noc.NOCDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
	NOCDTO dto = (NOCDTO)request.getAttribute("NOC_DETAILS");

	String regionName = null;
	EralisUserRolePriviledge userRolePriv = null;
	if(session.getAttribute(Constants.USER_DETAILS) != null)
	{
		userRolePriv = (EralisUserRolePriviledge) session.getAttribute(Constants.USER_DETAILS);
		regionName = userRolePriv.getRegionName();
	}
%>
<table width="100%" border="0" style="border-collapse:collapse">
  <tr>
    <td colspan="2">
    	<img id="Image1" style="height:120px;width:100%;" src="<%=request.getContextPath() %>/images/header.png" border="0">
        <div align="center">
        	<h2><strong><%=regionName %>:Bhutan</strong></h2>
        </div>
    </td>
  </tr>
  <tr>
  	<td colspan="2" align="center">
    	<hr/>
    </td>
  </tr>
  <tr>
	<td align="left"><strong><%=dto.getNocNumber() %></strong></td>
	<td align="right"><strong>Date:&nbsp;<%=dto.getCancellationDate() %></strong></td>
  </tr>
  <tr>
	<td colspan="2" align="center">
		<strong><u>NO OBJECTION CERTIFICATE</u></strong>
	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
	<td colspan="2">
		<p style="text-align: justify;">
			This is to certify that vehicle bearing registration number <%=dto.getVehicleNo() %> (<%=dto.getModel() %>) with chassis no.
			<%=dto.getChassisNo() %>, Engine no. <%=dto.getEngineNo() %> and manufacture year <%=dto.getManufactureYear() %> has been
			cancelled with effect from <%=dto.getCancellationDate() %> under the ownership of <%=dto.getSellerName() %> and sold to <%=dto.getBuyerName() %>
			bearing CID no <%=dto.getBuyerCID() %> of <%=dto.getBuyerAddress().trim() %>.
			<br><br>
			All the taxes have been cleared up to <%=dto.getLastRenewalDate() %> and 5% property transfer tax has also been paid in Bhutan. Therefore,
			the Royal Government of Bhutan has no objection to <%=dto.getBuyerName() %> to register the said vehicle outside Bhutan. The NOC fees 
			Nu.&nbsp;<%=dto.getAmountPaid() %> has been collected vide receipt no. <%=dto.getReceiptNo() %> dated <%=dto.getReceiptDate() %> and the
			registration of the above vehicle has been duly cancelled in our record.
		</p>
	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
	<td colspan="2" class="left">
		Regional Transport Officer<br>
		Road Safety & Transport Authority<br>
		<%=dto.getOfficeName() %>:Bhutan
	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
	<td colspan="2">
		<strong>Copy to:</strong><br>
		1.&nbsp;The Regional Transport Officer, <%=dto.getBuyerAddress() %>.<br>
		2.&nbsp;<%=dto.getBuyerName().trim() %> for necessary action.<br>
		3.&nbsp;Office copy.
	<td>
  </tr>
</table>