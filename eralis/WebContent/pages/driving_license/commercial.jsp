<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Commercial Driving License
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			issue commercial driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
						<div class="row">
							<div class="col-lg-12">
									
									<div id="Msg" style="display:none;"></div>
									<div id="validationMsg" style="display:none;"></div>
									<div class="form-group">
										<div class="col-lg-3">
											<label class="control-label">Non-commercial License No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
												<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>	
											</div>
										</div>
										<!--<div class="col-lg-2">
											<label class="control-label">Citizen ID:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="CID" styleClass="form-control" styleId="customerID" readonly="true"></html:text>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('cidModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
												
											</div>
										</div>-->
									</div>
									<!--<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Issued Date:</label>
										</div>
										<div class="col-lg-3">
											
										</div>
									</div>
							--></div>
						</div>
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label class="control-label">Customer ID:</label>
								</div>
								<div class="col-lg-4" id="customerID"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Occupation:</label>
								</div>
								<div class="col-lg-4" id="occupation"></div>
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
								
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">CID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
								<div class="col-lg-2">
									<label class="control-label">Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"></div>
							</div>
							<div id="nationalDIV">							
							<h4>Permanent Address(National)</h4>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-3" id="displayVillage">
								</div>
							</div>
							</div>
							<div id="internationalDIV">
							<h4>Permanent Address(Foreign National)</h4>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-4" id="displayCountry"></div>
								<div class="col-lg-2">
									<label class="control-label">Address:</label>
								</div>
								<div class="col-lg-4" id="displayAddress"></div>
							</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drivetypes:</label>
								</div>
								<div id="drivetypelist">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driving License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Region:</label>
								</div>
								<div class="col-lg-3">
									<input type="text" disabled="disabled" value="<%=regionName %>" class="form-control"/>
									<html:hidden property="region" value="<%=regionId %>"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Based on Certificate<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<select onChange="basedOnCertificate()" class="form-control" id="basedOnCertificate">
										<option value="No">No</option>
										<option value="Yes">Yes</option>
									</select>
								</div>
  							</div>
  							<div class="form-group">
								<div class="col-lg-2">
									<label>Drive Types<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="drivetype" onchange="validateCommercialCriteria(this.value)"
										styleClass="form-control" styleId="drivetype">
										<html:option value="">--SELECT--</html:option>
										<html:optionsCollection name="COMMERCIAL_DRIVE_TYPE_LIST"
											label="headerName" value="headerId"  /> 
									</html:select>
								</div>
								<div id="msgDIV" style="display:none;" class="col-lg-7 alert alert-danger"></div>
  							</div>
  							<div class="form-group">
								<div class="col-lg-2">
									<label>Renewal Duration<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
		                           		<html:option value="36">3 Years</html:option>
		                           		<html:option value="12">1 Year</html:option>
	                           		</html:select>
										<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select renewal duration</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
								
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="licensetype" styleId="licensetype1" /> 
			<html:hidden property="pageId" styleId="pageId" /> 
			<html:hidden property="customerId" styleId="learnerCustomerId"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal><!-- 
			<button type="button" class="btn btn-primary btn-sm"  >Refresh</button>
			<button type="button" class="btn btn-primary btn-sm"  >Cancel</button> -->
			<!--<logic:equal value="Y" name="priviledge" property="isEdit">
				<button class="btn btn-primary btn-sm">Update</button>
			</logic:equal>
			<logic:equal value="Y" name="priviledge" property="isDelete">
				<button class="btn btn-primary btn-sm">Delete</button>
			</logic:equal>
		--></div>
		</html:form>
	</div>
</div>
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									
									
									<div class="form-group">
										<label>License No:</label>
										<span class="pull-right">
											<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
										</span>
									</div>
									
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>

<div id="cidModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
							<div class="col-xs-12 col-sm-12">
									
									<div class="form-group">
										<label>Name:</label>
										<span class="pull-right">
											<input type="text" id="namePersonalModal" placeholder="Name"  style="width: 200px" />
										</span>
									</div>
									
									<div class="form-group">
										<label>Citizen ID:</label>
										<span class="pull-right">
											<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
										</span>
									</div>
									
									
									<div class="form-group">
										<label>Customer ID:</label>
										<span class="pull-right">
											<input type="text" id="customerIdPersonalModal" placeholder="CustomerID" style="width: 200px"  />
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<select id="regionPersonalModal" style="width: 200px" onchange="populateDependentDropDown(this.value, 'dzongkhagPersonalModal', '', 'DZONGKHAG_LIST', 'N')">
											<option value="">SELECT</option>
											<logic:iterate id="region" name="regionList">
												<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
											</logic:iterate>
										</select> 
										</span>
									</div>
									<div class="form-group">
										<label>Dzongkhag:</label>
										<span class="pull-right">
											 <select id="dzongkhagPersonalModal" style="width: 200px">
                                      			<option value="">SELECT</option>
                                      		</select>
										</span>
									</div>
									
							</div>
						</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>

				<button type="reset" class="btn btn-sm btn-primary">
					<i class="ace-icon fa fa-check"></i> Reset
				</button>

				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			
			<div id="commercialListTable">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
			</script>
			<script>
	function searchCommonInfo()
	{
		var name = $('#namePersonalModal').val();
		var cidNumber = $('#cidPersonalModal').val();
		var customerId = $('#customerIdPersonalModal').val();
		var regionId = $('#regionPersonalModal').val();
		var dzongkhagId = $('#dzongkhagPersonalModal').val();

		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(customerId == "")
			customerId = "NA";
		if(regionId == "")
			regionId = "NA";
		if(dzongkhagId == "")
			dzongkhagId = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList&name="+name+"&cid="+cidNumber+"&customerId="+customerId+"&region="+regionId+"&dzongkhag="+dzongkhagId,
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#commercialListTable").html(responseText);
				$("#commercialListTable").show();
			}
		});
	}
	
	function searchPersonalInfo()
	{
		var name 		= $('#namePersonalModal').val();
		var cidNumber 	= $('#cidPersonalModal').val();
		var regionId 	= $('#regionPersonalModal').val();
		var licenseNo	= $('#licenseNoModal').val();
		var licenseType	= $('#licenseTypeModal').val();
		
		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		
			regionId = "NA";
		if(licenseNo == "")
			licenseNo = "NA";
		
			licenseType = "NA";
	 
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&name="+name+"&cid="+cidNumber+"&region="+regionId+"&licenseNo="+licenseNo+"&licenseType="+licenseType+"&searchType=LICENSE_COMMERCIAL",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#licenseDetailList").html(responseText);
				$("#licenseDetailList").show();
			}
		});
		 
	} 
	function validateCommercialCriteria(driveTypeId)
	{
		$("#msgDIV").hide();
		
		var customerID	=	$("#learnerCustomerId").val();
		var drivinglicenseId	=	$("#drivinglicenseId").val();
		var basedOnCertificate	=	$("#basedOnCertificate").val();
		var url	=	null;
		if(basedOnCertificate=='No')
		{
			url='<%=request.getContextPath()%>/EralisCommonServlet?q=testValidation&driveTypeId='+driveTypeId+'&drivinglicenseId='+drivinglicenseId+'&customerID='+customerID;		
		}
		else
		{
			url='<%=request.getContextPath()%>/EralisCommonServlet?q=validateCommercialCriteria&driveTypeId='+driveTypeId+'&drivinglicenseId='+drivinglicenseId+'&customerID='+customerID;
		}
		//validateCommercialCriteria
		$.ajax
		({
			async: true,
			type: 'POST',
			url: url,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var testStatus = $(this).find('testStatus').text();
					var reason = $(this).find('testReason').text();
					if(testStatus=='0')
					{
						$("#msgDIV").show();
						$('#msgDIV').html("<label>"+reason+"</label>");
						$('#submitBtn').attr('disabled',true);
					}
					else
					{
						 $('#submitBtn').attr('disabled',false);
					}
				});
			}
		});
	}
	
	function searchsuspensionlist()
	{
		var customerId	=	$("#learnerCustomerId").val();
		$.ajax
		({
			
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getdrivinglicensedrivetype&customerId="+customerId,
			
			data : $('form').serialize(),
			cache : false,
			async: true,
			dataType : "html",
			success : function(responseText) 
			{
				$("#drivetypelist").html("<label class='control-label'>"+responseText+"</label>");
				$("#drivetypelist").show();
			}
		});
	}
	
	//validation	
	$(document).ready(function()
			{
				$("#learnerForm").validate({
					rules:{
						receiptNo:{
										required:true 
								   },
						receiptDate:{
										required:true,
									}
							},    
				
						messages:
							{
							receiptNo:{required: "Please Provide Receipt Number"},
							receiptDate:{required: "Please Provide Receipt Date"},
							},
				});

				
				
				
				$('#submitBtn').click(function(){

					$("#msgDIV").hide();
					var driveType	=	$("#drivetype").val();
					if(driveType=="")
					{
						$("#msgDIV").show();
						$('#msgDIV').html("<label>Please Select Drive Type</label>");
						return false;
					}
					else
					{
						requestType = "LICENSE";
						var serviceType = "NEW";
						var identityNo = $('#drivinglicenseId').val();
						var identityTypeId = "C";//if non-commercial N else C
						//alert($('#licensetype').val());
						var renewalDuration	=	$("#renewalDuration").val();
						getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '',renewalDuration);
					}
				});
				
		});
	
	function formSubmit()
	{
		var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_commerical',type:'POST',data: $("#learnerForm").serialize()}; 
	    $("#learnerForm").ajaxSubmit(options);
        $('#displayMsgDiv').show();
        setTimeout('hideStatus("displayMsgDiv")',10000);
        //setTimeout('reloadPage()',10000);
	}
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>

var pageIdentifier = "<%=pageIdentifier%>";
var pageId = "<%=pageId%>";
	</script>
	<link rel="stylesheet" media="screen" href="css/screen.css">
		<style>
			#learnerForm .error { color: red; }
		</style>
