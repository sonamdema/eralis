<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="duplication-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<% int i = 1; %>
			<th>Sl</th>
			<th>Duplication Date</th>
			<th>Delivered On</th>
			<th>Receipt No</th>
			<th>Receipt Date</th>
			<th>Supporting Doc</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="DUPLICATE_LICENSE_LIST">
			<logic:iterate id="duplicate" name="DUPLICATE_LICENSE_LIST">
				<tr>
					<td><%=i++ %></td>
					<td><bean:write name="duplicate" property="duplicationdate"/></td>
					<td><bean:write name="duplicate" property="deliveredon"/></td>
					<td><bean:write name="duplicate" property="receiptNo"/></td> 
					<td><bean:write name="duplicate" property="receiptDate"/></td>
					<td>
						<a href="#" onclick="downloadFile('<bean:write name="duplicate" property="fileUUID" filter="false"/>','<bean:write name="duplicate" property="fileName" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="duplicate" property="fileName" filter="false"/>
	  					</a>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
<script>

	$(document).ready(function() 
	{
	    $('#duplication-history-table').DataTable({
	            responsive: true
	    });
	});

</script>

