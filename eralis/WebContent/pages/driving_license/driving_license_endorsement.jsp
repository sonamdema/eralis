<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		License Endorsement
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			endorsement driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			
						<div class="row">
							<div class="col-lg-12">
								<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
									<div id="Msg"></div>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">License No<span style="color: #ff0000">*</span>:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
												<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-lg-3">
										  	<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a license number</label>
										</div>
										
									</div>
									
							</div>
						</div>
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label>Customer ID:</label>
								</div>
								<div class="col-lg-4" id="displayCustomerId"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
								<div class="col-lg-2">
									<label class="control-label">Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Identification Marks:</label>
								</div>
								<div class="col-lg-4">
									
								</div>
								<div class="col-lg-2">
									<label>Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"></div>
							</div>
								
							<div class="form-group">
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
							<div id="nationalDIV">
								<h4>Permanent Address(National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Dzongkhag:</label>
									</div>
									<div class="col-lg-4" id="displayDzongkhag"></div>
									<div class="col-lg-2">
										<label class="control-label">Gewog:</label>
									</div>
									<div class="col-lg-4" id="displayGewog"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Village:</label>
									</div>
									<div class="col-lg-4" id="displayVillage"></div>
									
								</div>
							</div>
							<div id="internationalDIV">
							<h4>Permanent Address(Foreign National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Country:</label>
									</div>
									<div class="col-lg-3" id="displayCountry">
									</div>
									<div class="col-lg-2">
										<label>Address:</label>
									</div>
									<div class="col-lg-3" id="displayAddress">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Customer Drive Type</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drivetypes:</label>
								</div>
								<div id="drivetypelist">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Endorsement Lists</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div id="endorsementlistTable">
					</div>
				</div>
			</div>
		</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="col-lg-12 form-group">
								<div class="col-lg-3">
									<label>Is TCB Endorsement?</label>
								</div>
								<div class="col-lg-3">
									<html:radio property="tcbEndorsement" value="N" styleId="tcbN" styleClass="ace" onclick="showDriveTypeList(this.value)">
										<span class="lbl">No</span>
									</html:radio>
									&nbsp;
									<html:radio property="tcbEndorsement" value="Y" styleId="tcbY" styleClass="ace" onclick="showDriveTypeList(this.value)">
										<span class="lbl">Yes</span>
									</html:radio>
								</div>
							</div>
							<!--<div class="col-lg-3">-->
								<!--<html:select property="drivetype" styleClass="form-control" styleId="drivetype" onchange="getDriveTypeTestMarks(this.value)">
									<html:option value="">--SELECT--</html:option>
									<html:optionsCollection name="DRIVE_TYPE_LIST"
										label="headerName" value="headerId"  /> 
								</html:select>
								-->
								<div class="form-group">
									<div class="col-lg-3">
										<label>Based on Certificate<span style="color: #ff0000">*</span>:</label>
									</div>
									<div class="col-lg-3">
										<select class="form-control" id="basedOnCertificate">
											<option value="No">No</option>
											<option value="Yes">Yes</option>
										</select>
									</div>
		  						</div>
  								<div class="form-group" id="ordinaryDriveTypeList">
									<div class="col-lg-3">
										<label>Drive Types:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="drivetype" styleClass="form-control" onchange="getDriveTypeTestMarks(this.value)" styleId="drivetype">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="DRIVE_TYPE_LIST" label="headerName" value="headerId"  /> 
										</html:select>
									</div>
									<div class="col-lg-6">
									 	<label style="display:none;color: #ff0000" id="driveTypeValidation">Please select a drive type</label>
									 	<div id="msgDIV" style="display:none;" class="alert alert-danger">
										</div>
									</div>
								</div>
								
  								<div class="form-group">
									<div class="col-lg-3">
										<label>Do you want to process for renewal?</label>
									</div>
									<div class="col-lg-3">
										<select onChange="isRenewalProcess()" class="form-control" id="isRenewalProcessId" name="isRenewalProcessId">
											<option value="N">No</option>
											<option value="Y">Yes</option>
										</select>
									</div>
									<div class="col-lg-6">
									 	<label style="display:none;color: #ff0000" id="driveTypeValidation">Please select a drive type</label>
									 	<div id="msgDIV" style="display:none;" class="alert alert-danger">
										</div>
									</div>
								</div>
								<div class="form-group" id="renewalDurationDiv" style="display:none">
									<div class="col-lg-3">
										<label>Renewal Duration:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
		                           			<html:option value="">--SELECT--</html:option>
		                           		</html:select>
										<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select renewal duration</label>
									</div>
								</div>
							<div class="form-group" id="tcbDriveTypeList">
								<div class="col-lg-3">
									<label>Drive Types:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="drivetypeTCB" styleClass="form-control" styleId="drivetypeTCB" multiple="true">
										<html:option value="">--SELECT--</html:option>
									</html:select>
								</div>
								<div class="col-lg-3">
								 	<label style="display:none;color: #ff0000" id="driveTypeValidationTCB">Please select a drive type</label>
								</div>
							</div>
							<!--<div class="form-group">
								<div class="col-lg-2">
									<label>Test Marks:</label>
								</div>
								<div class="col-lg-3">
									<input type="number" class="form-control" id="testMark">
								</div>
							</div>
							--><div class="form-group">
								<div class="col-lg-3">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks"  styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('vehicleForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="licensetype" styleId="licensetype1" />
			<html:hidden property="pageId" styleId="pageId" /> 
			<html:hidden property="region" styleId="region"/> 
			<html:hidden property="customerId" styleId="customerId"/> 
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal>
			<!--<logic:equal value="Y" name="priviledge" property="isEdit">
				<button class="btn btn-primary btn-sm">Update</button>
			</logic:equal>
			<logic:equal value="Y" name="priviledge" property="isDelete">
				<button class="btn btn-primary btn-sm">Delete</button>
			</logic:equal>
		--></div>
		</html:form>
	</div>
</div>

<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>
	
		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";

			$('.fileupload').ace_file_input({
				no_file : 'No File ...',
				btn_choose : 'Choose',
				btn_change : 'Change',
				droppable : false,
				onchange : null,
				thumbnail : false,
				whitelist:'png|jpg|jpeg',
				blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
			});
			
			//datepicker plugin
			$('.date-picker').datepicker({
				autoclose: true,
				todayHighlight: true
			})
			//show datepicker when clicking on the icon
			.next().on(ace.click_event, function(){
				$(this).prev().focus();
			});
		
			function showDriveTypeList(val)
			{
				$('#ordinaryDriveTypeList').hide();
				$('#tcbDriveTypeList').hide();
				
				if(val == "N")
					$('#ordinaryDriveTypeList').show();
				else 
				{
					$('#submitBtn').attr('disabled',false);
					$('#tcbDriveTypeList').show();
					
					var drivinglicenseId = $('#drivinglicenseId').val();

					if(drivinglicenseId == "")
					{
						$('#licenseNoValidation').show();
						$('#licenseNoValidation').get(0).scrollIntoView();
						return false;
					}
					else
					{
						$('#drivetypeTCB').empty();
						
						var url = '<%=request.getContextPath()%>/common.html?method=getTCBDriveTypeList&drivingLicenseId='+drivinglicenseId;
						
						$.ajax
						({	
							type: "GET",
							url : url,
							cache: false,
							async: false,
							dataType : "json",
							success : function(data) 
							{
								$('#drivetypeTCB').append("<option value='0' selected>--SELECT--</option>");
								
								for (var i = 0; i < data.length; i++) {
									$('#drivetypeTCB').append("<option value=" + data[i].headerId + ">"+ data[i].headerName + "</option>");
							    }
							},
							error : function(jqXHR, textStatus, errorThrown) {		
							}
						});
					}
				}
			}
			
			function searchPersonalInfo()
			{
				var cidNumber 	= $('#cidPersonalModal').val();
				var licenseNo	= $('#licenseNoModal').val();
				
				if(cidNumber == "")
					cidNumber = "NA";
				if(licenseNo == "")
					licenseNo = "NA";
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=LICENSE_ENDORSEMENT",
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#licenseDetailList").html(responseText);
						$("#licenseDetailList").show();

						$('#tcbN').attr('checked', true);
						$('#ordinaryDriveTypeList').show();
						$('#tcbDriveTypeList').hide();
						$('#drivetypeTCB').empty();
					}
				});
			} 
			
			function getdrivetype()
			{
				var customerId	=	$("#learnerCustomerId").val();

				$.ajax
				({
					
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getdrivinglicensedrivetype&customerId="+customerId,
					
					data : $('form').serialize(),
					cache : false,
					async: true,
					dataType : "html",
					success : function(responseText) 
					{
						
						$("#drivetypelist").html("<label class='control-label'>"+responseText+"</label>");
						$("#drivetypelist").show();
					}
				});
			}
			
			function getDriveTypeTestMarks(driveTypeId)
			{
				$("#msgDIV").hide();
				
				var customerID	=	$("#customerId").val();
				var licenseNO	=	$("#licenseNO").val();
				var drivinglicenseId = $('#drivinglicenseId').val();

				var basedOnCertificate	=	$("#basedOnCertificate").val();
				var url	=	null;
				if(basedOnCertificate=='No')
				{
					url='<%=request.getContextPath()%>/EralisCommonServlet?q=testValidation&driveTypeId='+driveTypeId+'&drivinglicenseId='+drivinglicenseId+'&customerID='+customerID;		
				}
				else
				{
					url='<%=request.getContextPath()%>/EralisCommonServlet?q=getDriveTypeTestMarks&driveTypeId='+driveTypeId+'&licenseNO='+licenseNO+'&formType=booking&testDate=null&customerID='+customerID;
					
				}
				
				$.ajax
				({
					async: true,
					type: 'POST',
					url: url,
					success: function(xml)
					{ 
						$(xml).find('xml-response').each(function()
						{
							var testStatus = $(this).find('testStatus').text();
							var reason = $(this).find('testReason').text();
							if(testStatus=='0')
							{
								$("#msgDIV").show();
								$('#msgDIV').html("<label>"+reason+"</label>");
								$('#submitBtn').attr('disabled',true);
							}
							else
							{
								 $('#submitBtn').attr('disabled',false);
							}
						});
					}
				});
			}
			
			function searchlicenseendorsementlist(drivingLicenseId,age,totalDuration,dateDifference)
			{
				var drivinglicenseId = $('#drivinglicenseId').val();
				
				if(drivinglicenseId == "")
					drivinglicenseId = "NA";
				
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getlicenseendorsementlists&drivinglicenseId="+drivinglicenseId,
					
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#endorsementlistTable").html(responseText);
						$("#endorsementlistTable").show();
						getdrivetype();
					}
				});
				populateRenewalDurationDropdown(age,totalDuration,dateDifference);
			}
			
			$(document).ready(function()
			{
				$('#tcbN').attr('checked','checked');
				$('#ordinaryDriveTypeList').show();
				$('#tcbDriveTypeList').hide();
				
				$('#submitBtn').click(function()
				{ 
					var licenseNo = $('#licenseNO').val();
					var drivetype = $('#drivetype').val();
					var drivetypeTCB = $('#drivetypeTCB').val();
					var isTCBEndorsement = $('input[type=radio].ace:checked').val();
					var isRenewalProcessId = $("#isRenewalProcessId").val();
					if(licenseNo == "")
					{
						$('#licenseNoValidation').show();
						$('#licenseNoValidation').get(0).scrollIntoView();
						return false;
					}
					if(isTCBEndorsement == "N" && (drivetype == "" || drivetype == "0"))
					{
						$('#driveTypeValidation').show();
						$('#driveTypeValidation').get(0).scrollIntoView();
						return false;
					}
					else if(isTCBEndorsement == "Y" && drivetypeTCB == "0")
					{
						$('#driveTypeValidationTCB').show();
						$('#driveTypeValidationTCB').get(0).scrollIntoView();
						return false;
					}
					else
					{
						if(isRenewalProcessId=="Y")
						{
							requestType = "LICENSE";
							var serviceType = "ENDORSEMENT";
							var identityNo = $('#licenseNO').val();
							var identityTypeId = $('#licensetype1').val(); //if non-commercial N else C
							getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '');
							
							requestType = "LICENSE";
							var serviceType = "ENDORSEMENT_RENEWAL";
							var identityNo = $('#drivinglicenseId').val();
							var identityTypeId = $('#licensetype1').val(); //if non-commercial N else C
							var renewalDuration = $('#renewalDuration').val();
							getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '', renewalDuration);
						}
						else
						{
							requestType = "LICENSE";
							var serviceType = "ENDORSEMENT";
							var identityNo = $('#licenseNO').val();
							var identityTypeId = $('#licensetype1').val(); //if non-commercial N else C
							getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '');
						}
					}
				});
			});
			
			function formSubmit()
			{
				var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_endorsement',type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDiv').show();
		        setTimeout('hideStatus("displayMsgDiv")',10000);
			}

			var fileError;
           	function validation(thisform,msgId,fileObj)
           	{
           		var fileId = fileObj.id;
           		with(thisform)
           		{
           			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           		}
           	}
           	
			
			function isRenewalProcess()
			{
				$("#renewalDurationDiv").hide();
				var isRenewalProcessId = $("#isRenewalProcessId").val();
				if(isRenewalProcessId=="Y")
				{
					$("#renewalDurationDiv").show();
				}
			}


			function populateRenewalDurationDropdown(age,totalDuration,dateDifference)
			{
				$('#renewalDuration').empty();
				var licenseType = $('#licensetype1').val();
				if(licenseType == "N")
				{ 
					//if 70 n above
					if(age>=70)
					{
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
					}
					//if above 69
					else if(age>69)
					{
						$('#renewalDuration').append("<option value='"+totalDuration+"'>"+dateDifference+"</option>");
					}
					//if above 65
					else if(age>65)
					{
						$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
						$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
					}
					//if above 60
					else if(age>60)
					{
						$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
						$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
						$('#renewalDuration').append("<option value='48_month'>4 Years</option>");
						$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
						$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
						
					}
					else
					{
						$('#renewalDuration').append("<option value='120_month'>10 Years</option>");
						$('#renewalDuration').append("<option value='108_month'>9 Years</option>");
						$('#renewalDuration').append("<option value='96_month'>8 Years</option>");
						$('#renewalDuration').append("<option value='84_month'>7 Years</option>");
						$('#renewalDuration').append("<option value='72_month'>6 Years</option>");
						$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
						$('#renewalDuration').append("<option value='48_month'>4 Years</option>");
						$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
						$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
					}
				}
				else
				{
					if(age>=65)
					{
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
					}
					else if(age>64)
					{
						$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
					}
					else if(age>63)
					{
						$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
						if(totalDuration>=365)
						{
							$('#renewalDuration').append("<option value='12_month'>1 Years</option>");
						}
					}
					else
					{
						$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
						$('#renewalDuration').append("<option value='24_month'>2 Year</option>");
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
					}
				}
			}

			function calEnodrsementRenewalAmount()
			{
				var dlEndorsementAmount	=	$("#dlEndorsementAmount").val();
				var dlRenewalAmount	=	$("#dlRenewalAmount").val();
				var totalAmount	="";

				totalAmount	=	Number(dlEndorsementAmount)+Number(dlRenewalAmount);
				$("#totalAmount").val(totalAmount);
				$("#amount").val(totalAmount);

			}
</script>
<style>
	#learnerForm .error { color: red; }
</style>