<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
	String Type = (String)request.getAttribute("SEARCH_TYPE");
	System.out.println("type::"+Type); 
%>

<div class="table-responsive">
	<table id="commercial-search-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr> 
				<th></th>
				<th>Name</th> 
				<th>Permit No</th> 
				<th>License Number</th> 
				<th>Issue Date</th>
				<th>Expiry Date</th> 
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="PERMIT_LIST">
				<logic:iterate id="license" name="PERMIT_LIST">
					<tr>
						<td>
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="license" property="permitDetailsId"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="license" property="name"/></td>
						<td><bean:write name="license" property="permitNo"/></td>
						<td><bean:write name="license" property="licenseNo"/></td>
						<td><bean:write name="license" property="issuedate"/></td>
						<td><bean:write name="license" property="expiryDate"/></td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>



<script>

	$(document).ready(function() 
	{
   		$('#commercial-search-table').DataTable({
            responsive: true
    	});
	});

	<%
	String searchType = (String)request.getAttribute("SEARCH_TYPE");
	%>
	var searchType = "<%=searchType%>";
	
	function getSelected(permitDetailsId)
	{ 
		$('#Msg').hide();
		$('#displayStatus').hide();
		$.ajax
		({
			async: false,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getpermitDetails&permitDetailsId='+permitDetailsId+'&searchType='+searchType,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					var permitDetailsId = $(this).find('permitDetailsId').text();
					var licenseNo = $(this).find('licenseNo').text();
					var registrationNo = $(this).find('registrationNo').text();
					var permitNo = $(this).find('permitNo').text();
					var name = $(this).find('name').text();
					var address = $(this).find('address').text();
					var phoneNo = $(this).find('phoneNo').text();
					var routeBetween = $(this).find('routeBetween').text();
					var to = $(this).find('routeTo').text();
					var issuedate = $(this).find('issuedate').text();
					var validity = $(this).find('validity').text();
					var vehicleTypeFV = $(this).find('vehicleType').text();
					var carryingCapacity = $(this).find('carryingCapacity').text();
					var seatCapacity = $(this).find('seatCapacity').text();
					var engineCC = $(this).find('engineCC').text();
					var regionId = $(this).find('regionId').text();
					var baseoffice = $(this).find('baseoffice').text();
					var journeyPurpose = $(this).find('journeyPurpose').text();
					var remarks = $(this).find('remarks').text();

					
					$("#permitDetailsId").val(permitDetailsId);
					$("#licenseNo").val(licenseNo);
					$("#registrationNo").val(registrationNo);
					$("#permitNo").val(permitNo);
					$("#driverName").val(name);
					$("#address1").val(address);
					$("#license_mobile").val(phoneNo);
					$("#routeBetween").val(routeBetween);
					$("#to").val(to);
					$("#dateOfissue").val(issuedate);
					$("#validity").val(validity);
					$("#vehicleTypeFV").val(vehicleTypeFV);
					$("#carryingCapacity").val(carryingCapacity);
					$("#seatCapacity").val(seatCapacity);
					$("#engineCC").val(engineCC);
					$("#journeyPurpose").val(journeyPurpose);
					$("#region1").val(regionId);
					$("#baseoffice").val(baseoffice);
					$("#remarks1").val(remarks);
					
				});	 
			}
		});
	}
</script>
 