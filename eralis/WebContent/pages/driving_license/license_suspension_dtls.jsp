<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
	<table id="commercial-search-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr> 
				<th></th>
				<th>CID</th> 
				<th>Name</th> 
				<th>License Number</th> 
				<th>Region</th> 
				<th>Dzongkhag</th> 
				<th>Issue Date</th>
				<th>Expiry Date</th> 
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="LICENSE_LIST">
				<logic:iterate id="license" name="LICENSE_LIST">
					<tr>
						<td>
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="license" property="drivinglicenseId"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="license" property="CID"/></td>
						<td><bean:write name="license" property="name"/></td>
						<td><bean:write name="license" property="licenseNo"/></td>
						<td><bean:write name="license" property="region"/></td>
						<td><bean:write name="license" property="dzongkhag"/></td>
						<td><bean:write name="license" property="issuedate"/></td>
						<td><bean:write name="license" property="expiryDate"/></td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>
<script><!--

	$(document).ready(function() 
	{
   		$('#commercial-search-table').DataTable({
            responsive: true
    	});
	});

	<%
	String searchType = (String)request.getAttribute("SEARCH_TYPE");
	%>
	var searchType = "<%=searchType%>";
	enable(identifier);
	
	function getSelected(drivinglicenseId)
	{ 
		$('#Msg').hide();
		$('#displayStatus').hide();
		$.ajax
		({
			async: false,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getSuspensionDetails&drivinglicenseId='+drivinglicenseId+'&searchType='+searchType,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					
					var status=$(this).find('status').text();
					if(status == 'VEHICLE_OUTSTANDING' && searchType!="LICENSE_HISTORY"  && searchType!="LICENSE_OFFENCE" && searchType!="LICENSE_CANCELLATION")
					{

						var vehicle_type =  $(this).find('vehicle_type').text(); 
						var vehicleNo = $(this).find('vehicleNo').text();
						$("#Msg").html("<div class='alert alert-danger'>This customer has <b>vehicle outstanding</b> against the vehicle no. : <b>"+vehicleNo+"</b> and vehicle type : <b>"+vehicle_type+"</b></div>");
						$("#Msg").show();
							return false;
					}
					else if(status == "DRIVING_LICENSE_OUTSTANDING" && searchType!="LICENSE_HISTORY"  && searchType!="LICENSE_OFFENCE" && searchType!="LICENSE_CANCELLATION")
					{
						var licenseNO = $(this).find('licenseNO').text();
						$("#Msg").html("<div class='alert alert-danger'>This customer has <b>Driving License outstanding</b> against the Driving License no. : <b>"+licenseNO+"</b>, please clear the outstanding.</div>");
						$("#Msg").show();
							return false;
					
					}
					else
					{
						if(status == 'VEHICLE_OUTSTANDING' && searchType=="LICENSE_HISTORY" )
						{
							var vehicle_type =  $(this).find('vehicle_type').text(); 
							var vehicleNo = $(this).find('vehicleNo').text();
							$("#Msg").html("<div class='alert alert-danger'>This customer has <b>vehicle outstanding</b> against the vehicle no. : <b>"+vehicleNo+"</b> and vehicle type : <b>"+vehicle_type+"</b></div>");
							$("#Msg").show();
						}
						else if(status == "DRIVING_LICENSE_OUTSTANDING" && searchType=="LICENSE_HISTORY")
						{
							var licenseNO = $(this).find('licenseNO').text();
							$("#Msg").html("<div class='alert alert-danger'>This customer has <b>Driving License outstanding</b> against the Driving License no. : <b>"+licenseNO+"</b>, please clear the outstanding.</div>");
							$("#Msg").show();
						
						}
						
					var name = $(this).find('name').text();
					var dzongkhag = $(this).find('dzongkhag').text();

					var age = $(this).find('age').text();
					var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var titleOfCourtesy = $(this).find('courtesy').text();
					var occupation = $(this).find('occupation').text();
					var customerId = $(this).find('customerId').text();
					var bloodgroup = $(this).find('bloodgroup').text();
					var bloodgroupId = $(this).find('bloodgroupId').text();
					var dob = $(this).find('dob').text();
					var nationality=$(this).find('nationality').text();
					var CID=$(this).find('CID').text();
					

					var fathersname=$(this).find('fathersname').text();
					var identificationMarks=$(this).find('identificationMarks').text();
					var remarks=$(this).find('remarks').text();
					var gender = $(this).find('gender').text();

					var country = $(this).find('country').text();
					var personalInfoId = $(this).find('personalInfoId').text();
					var village = $(this).find('village').text();
					var licenseNO = $(this).find('licenseNO').text();
					var region=$(this).find('region').text();
					var regionName = $(this).find('region-name').text();
					var learnerlicenseno=$(this).find('learnerlicenseno').text();
					var status=$(this).find('status').text();
					var expirydate=$(this).find('expirydate').text();
					var issuedate=$(this).find('issuedate').text();
					var drivingLicenseNo=$(this).find('licenseNO').text();
					var licensestatus=$(this).find('licensestatus').text();
					var licensetype=$(this).find('licensetype').text();
					var drivingLicenseId	=	$(this).find('drivingLicenseId').text();
					var isInternational = $(this).find('isInternational').text();
					var learnerLicenseId	=$(this).find('learnerLicenseId').text();
					var phoneNo = $(this).find('phoneNo').text();
					var type = $(this).find('type').text();
					//var firstName = $(this).find('phoneNo').text();
					//var middleName = $(this).find('phoneNo').text();
					//var lastName = $(this).find('phoneNo').text();
					var dateDifference = $(this).find('dateDifference').text();
					var totalDuration = $(this).find('totalDuration').text();
					var cancelledDate = $(this).find('cancelledDate').text();
					var cancelReason = $(this).find('cancelReason').text();
					var ministryId = $(this).find('ministryId').text();
					var departmentId = $(this).find('departmentId').text();
					var amount = $(this).find('amount').text();
					var receiptDate = $(this).find('receiptDate').text();
					var receiptNo = $(this).find('receiptNo').text();
					if(country=="Bhutan")
					{
						if(name=="" || name=="null" || dzongkhag=="" || dzongkhag=="null" || CID=="" || CID=="null" || gender=="" || gender=="null"
							 || phoneNo=="" || phoneNo=="null" || bloodgroup=="" || bloodgroup=="null")
						
						{
							if(searchType!="LICENSE_HISTORY")
							{
								$("#Msg").show();
								$("#Msg").addClass("alert alert-danger");
								$("#Msg").html("Incomplete information of driving license holder of <b>Customer Id : " + customerId+"</b>");
								return false;
							}
						}
						
					}
					
					
					$('#initialReceiptNo').html("<label class='control-label'>"+receiptNo+"</label>");
					$('#initialAmount').html("<label class='control-label'>"+amount+"</label>");
					$('#initialReceiptDate').html("<label class='control-label'>"+receiptDate+"</label>");
					
					//$("#firstname").val(firstName);
					//$("#middlename").val(middleName);
					//$("#lastName").val(lastName);
					
					$('#learnerlicenseno').val(learnerlicenseno);
					$('#newLicenseNo').val(licenseNO);
					$('#drivinglicenseId').val(drivingLicenseId);
					$('#newDrivinglicenseId').val(drivingLicenseId);
					
					//$('#licenseNO').val(licenseNO);
					$('#licensestatus').val(licensestatus);
					$('#licensetype1').val(licensetype);
					
					$('#newLicenseHolderName').val(name);
					$('#driverName').val(name);
					$('#address').val(address);
					$('#address1').val(address);
					$('#phone').val(phoneNo);
					$('#license_mobile').val(phoneNo);
					$("#customerId").val(customerId);

					if(isInternational == "N")
					{
						$('#nationalDIV').show();
						$('#internationalDIV').hide();
					}
					else
					{
						$('#nationalDIV').hide();
						$('#internationalDIV').show();
					} 
					if(searchType == "LICENSE_OFFENCE")
					{
						$('#newLicenseHolderName').html("<label class='control-label'>"+name+"</label>");
						$('#newLicenseNo').val(licenseNO);
						$('#drivinglicenseId').val(drivingLicenseId);
						
						//searchOffence("",drivingLicenseId);
					}
					else if(searchType == "WITHDRAW_LICENSE_CANCELLATION")
					{
						$("#message").hide();
						if(licensestatus == "CANCELLED")
						{
							$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#citizenID').val(CID);
							$('#region').val(region);
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#learnerCustomerId').val(customerId);
							$('#titleOfcourtesy').val(titleOfCourtesy);
							$('#name').val(name);
							$('#occupation').val(occupation);
							$('#nationality').val(nationality);
							$('#cid').val(CID);
							$('#bloodGroup').val(bloodgroupId);
							$('#displayDob').val(dob);
							$('#fathersName').val(fathersname);
							$('#identificationMarks').val(identificationMarks);
							$('#remarks').val(remarks);
							$('#licenseNO').val(licenseNO);
							$('#learnerlicenseno').val(learnerlicenseno);
							$('#customerId').val(customerId);
							if(gender == "M")
								$('#maleRadio').attr('checked', true);
							else if(gender == "F")
								$('#femaleRadio').attr('checked', true);
							
							
							$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
							
							$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
							$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
							$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
							$('#gender').html("<label class='control-label'>"+gender+"</label>");
							$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							
							$('#cid').html("<label class='control-label'>"+CID+"</label>");
							$("#duplicateCID").val(CID);
							$("#personalInfoId").val(personalInfoId); 
							$('#village').html("<label class='control-label'>"+village+"</label>");
							$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
							$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
							$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
							$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");



							
							$("#cancellation_date").text(cancelledDate);
							$("#cancellation_reason").text(cancelReason);
							$("#submit_button").prop('disabled', false);
						}
						else
						{
							$("#message").show();
							$("#submit_button").prop('disabled', true);
						}
					}
					else if(searchType == "UPDATE_LICENSE_OFFENCE")
					{
						$('#updateLicenseHolderName').html("<label class='control-label'>"+name+"</label>");
						$('#updateLicenseNo').val(licenseNO);
						$('#updateDrivingLicenseId').val(drivingLicenseId);
						searchOffence("",drivingLicenseId,type);
					}
					else if(searchType == "LICENSE_HISTORY")
					{
							if(licensestatus == "CANCELLED")
							{
								$('#Msg').html("<div class='alert alert-danger'>This License has been <strong>Cancelled</strong> in our record with effect from <strong>"+cancelledDate+"</strong> based on the the following reason: <strong>"+cancelReason+"</strong> </div>");
								$('#Msg').show();
							}

							if(licensestatus == "HAS_PUNCHED")
							{
								
								var licensePunchNo = $(this).find('licensePunchNo').text();
								$('#displayStatus').show();
								$('#msg').html("<div class='alert alert-danger'>License has "+licensePunchNo+" Punch</div>");
							}
							if(licensestatus == "PENDING_APPLICATION")
							{
								
								$('#displayStatus').show();
								$('#msg').html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
							}
							
							$('#status').html("<label class='control-label'>"+status+"</label>");
							$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#displayCustomerId').html("<label class='control-label'>"+customerId+"</label>");
							$('#citizenID').val(CID);
							$('#region').val(region);
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#displayCID').html("<label class='control-label'>"+CID+"</label>");
							$('#learnerCustomerId').val(customerId);
							$('#titleOfcourtesy').val(titleOfCourtesy);
							$('#name').val(name);
							$('#occupation').val(occupation);
							$('#occupation').text(occupation);
							$('#nationality').val(nationality);
							$('#cid').val(CID);
							$('#bloodGroup').val(bloodgroupId);
							//$('#displayDob').val(dob);
							$('#fathersName').val(fathersname);
							$('#identificationMarks').val(phoneNo);
							$('#remarks').val(remarks);
							$('#licenseNO').val(licenseNO);
							$('#learnerlicenseno').val(learnerlicenseno);
							$('#licensetype1').val(licensetype);
							if(gender == "M")
								$('#maleRadio').attr('checked', true);
							else if(gender == "F")
								$('#femaleRadio').attr('checked', true);
							
							$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
							$('#displayDateOfBirth').html("<label class='control-label'>"+dob+"</label>");
							$('#displayCustomerID').html("<label class='control-label'>"+customerId+"</label>");
							$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
							$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
							$('#displayPhoneNo').html("<label class='control-label'>"+phoneNo+"</displayPhoneNo>");
							$('#gender').html("<label class='control-label'>"+gender+"</label>");
							$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							
							$('#cid').html("<label class='control-label'>"+CID+"</label>");
							$("#duplicateCID").val(CID);
							$("#personalInfoId").val(personalInfoId); 
							$('#village').html("<label class='control-label'>"+village+"</label>");
							$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
							$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
							$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
							$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
							getLicenseHistory();
							get_PIS(personalInfoId);
							if(ministryId>0)
							{
								enable('Official');	
								populateDependentDropDown(ministryId, 'department', '', 'DEPARTMENT_LIST', 'N');
								$("#ministry").val(ministryId);
								$("#department").val(departmentId);
								$('#officialRadio').attr('checked', true);
								$('#privateRadio').attr('checked', false);
							}
							else
							{
								enable('Private');
								$('#privateRadio').attr('checked', true);
								$('#officialRadio').attr('checked', false);
							}
						
					}
					else if(searchType == "PASSANGER_BUS")
					{
						if(licensestatus == "CANCELLED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
							$('#Msg').show();
							}	
						else if(licensestatus == "SUSPENDED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
							$('#Msg').show();
							}
						else if(licensestatus == "HAS_OFFENCE")
						{
							$("#Msg").html("<div class='alert alert-danger'>License has offence </div>");
							$('#Msg').show();

				 			$('#').attr('disabled', false);
						}
						else if(licensestatus == "HAS_PUNCHED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License has Punched </div>");
							$('#Msg').show();
						}
						else
						{ 
							$('#status').html("<label class='control-label'>"+status+"</label>");
							$('#driverName').html("<label class='control-label'>"+name+"</label>"); 
							$('#licenseNO').val(licenseNO);
							
							$.ajax
							({
								
								type : "POST",
								url : "<%=request.getContextPath()%>/common.html?method=getdrivinglicensedrivetype&customerId="+customerId,
								
								data : $('form').serialize(),
								cache : false,
								async: true,
								dataType : "html",
								success : function(responseText) 
								{
									
									$("#drivetypelist").html("<label class='control-label'>"+responseText+"</label>");
									$("#drivetypelist").show();
								}
							});
							
						}
					}
					else if(searchType == "LICENSE_RENEWAL")
					{
						if(licensestatus == "ISSUED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License is already issued </div>");
							$('#Msg').show();
							}
						else if(licensestatus == "CANCELLED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
							$('#Msg').show();
							}	
						else if(licensestatus == "SUSPENDED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
							$('#Msg').show();
							}
						else if(licensestatus == "HAS_OFFENCE")
						{
							$("#Msg").html("<div class='alert alert-danger'>License has offence </div>");
							$('#Msg').show();

				 			$('#').attr('disabled', false);
						}
						else if(licensestatus == "HAS_PUNCHED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License has Punched </div>");
							$('#Msg').show();
						}
						else if(licensestatus == "PENDING_APPLICATION")
						{
							$("#submitBtn").prop('disabled', true);
							$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
							$('#Msg').show();
						}
						else if(licensestatus == "SUCCESS")
						{ 
							$("#submitBtn").prop('disabled', false);
							$('#status').html("<label class='control-label'>"+status+"</label>");
							$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#citizenID').val(CID);
							$('#region').val(region);
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#learnerCustomerId').val(customerId);
							$('#titleOfcourtesy').val(titleOfCourtesy);
							$('#name').val(name);
							$('#occupation').val(occupation);
							$('#nationality').val(nationality);
							$('#cid').val(CID);
							$('#bloodGroup').val(bloodgroupId);
							$('#displayDob').val(dob);
							$('#fathersName').val(fathersname);
							$('#identificationMarks').val(identificationMarks);
							$('#remarks').val(remarks);
							$('#licenseNO').val(licenseNO);
							$('#learnerlicenseno').val(learnerlicenseno);
							$('#licensetype1').val(licensetype);
							if(gender == "M")
								$('#maleRadio').attr('checked', true);
							else if(gender == "F")
								$('#femaleRadio').attr('checked', true);
							
							
							$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
							
							$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
							$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
							$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
							$('#gender').html("<label class='control-label'>"+gender+"</label>");
							$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							
							$('#cid').html("<label class='control-label'>"+CID+"</label>");
							$("#duplicateCID").val(CID);
							$("#personalInfoId").val(personalInfoId); 
							$('#village').html("<label class='control-label'>"+village+"</label>");
							$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
							$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
							$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
							$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
							searchlicenserenewallist(drivingLicenseId,age,totalDuration,dateDifference,nationality,CID);
						}
					}
				else if(searchType == "LICENSE_DUPLICATE")
				{
					if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
						$('#Msg').show();
					}	
					else if(licensestatus == "SUSPENDED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "HAS_OFFENCE")
					{
						$("#Msg").html("<div class='alert alert-danger'>License has offence </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
						$('#Msg').show();
					}
					else 
					{
						$("#submitBtn").prop('disabled', false);
						$('#status').html("<label class='control-label'>"+status+"</label>");
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
						$('#citizenID').val(CID);
						$('#region').val(region);
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#learnerCustomerId').val(customerId);
						$('#titleOfcourtesy').val(titleOfCourtesy);
						$('#name').val(name);
						$('#occupation').val(occupation);
						$('#nationality').val(nationality);
						$('#cid').val(CID);
						$('#bloodGroup').val(bloodgroupId);
						$('#displayDob').val(dob);
						$('#fathersName').val(fathersname);
						$('#identificationMarks').val(identificationMarks);
						$('#remarks').val(remarks);
						$('#licenseNO').val(licenseNO);
						$('#learnerlicenseno').val(learnerlicenseno);
						$('#licensetype1').val(licensetype);
						 
						if(gender == "M")
							$('#maleRadio').attr('checked', true);
						else if(gender == "F")
							$('#femaleRadio').attr('checked', true);
						
						
						$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
						$('#displayCustomerId').html("<label class='control-label'>"+customerId+"</label>");
						$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
						$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
						$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
						
						$('#cid').html("<label class='control-label'>"+CID+"</label>");
						$("#duplicateCID").val(CID);
						$("#personalInfoId").val(personalInfoId); 
						$('#village').html("<label class='control-label'>"+village+"</label>");
						$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
						$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
						$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
						$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
						 searchlicenseduplicationlist();
					}
				}
				else if(searchType == "LICENSE_ENDORSEMENT")
				{
					 if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
						$('#Msg').show();
					}	
					else if(licensestatus == "SUSPENDED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "HAS_OFFENCE")
					{
						$("#Msg").html("<div class='alert alert-danger'>License has offence </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
						$('#Msg').show();
					}
					else
					{
						$("#submitBtn").prop('disabled', false);
						$('#status').html("<label class='control-label'>"+status+"</label>");
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#displayCustomerId').html("<label class='control-label'>"+customerId+"</label>");
						$('#citizenID').val(CID);
						$('#region').val(region);
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#learnerCustomerId').val(customerId);
						$('#titleOfcourtesy').val(titleOfCourtesy);
						$('#name').val(name);
						$('#occupation').val(occupation);
						$('#nationality').val(nationality);
						$('#cid').val(CID);
						$('#bloodGroup').val(bloodgroupId);
						$('#displayDob').val(dob);
						$('#fathersName').val(fathersname);
						$('#identificationMarks').val(identificationMarks);
						$('#remarks').val(remarks);
						$('#licenseNO').val(licenseNO);
						$('#learnerlicenseno').val(learnerlicenseno);
						if(gender == "M")
							$('#maleRadio').attr('checked', true);
						else if(gender == "F")
							$('#femaleRadio').attr('checked', true);
						
						
						$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
						
						$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
						$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
						$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
						
						$('#cid').html("<label class='control-label'>"+CID+"</label>");
						$("#duplicateCID").val(CID);
						$("#personalInfoId").val(personalInfoId); 
						$('#village').html("<label class='control-label'>"+village+"</label>");
						$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
						$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
						$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
						$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
						searchlicenseendorsementlist(drivingLicenseId,age,totalDuration,dateDifference);
						if(licensetype=='N')
						{
							populateDependentDropDown('', 'drivetype', '', 'ORDINARY_DRIVE_TYPE', 'N');
						}
						else
						{
							populateDependentDropDown('', 'drivetype', '', 'DRIVE_TYPE_LIST', 'N');
						}
					}
				}
				else if(searchType == "LICENSE_SUSPENSION" || searchType == "DRIVETYPE_SUSPENSION" )
				{
				 if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
						$('#Msg').show();
					}	
					else if(licensestatus == "SUSPENDED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
						$('#Msg').show();
					}
					else 
					{
						$("#submitBtn").prop('disabled', false);
						$('#customerId').val(customerId);
						$('#status').html("<label class='control-label'>"+status+"</label>");
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#citizenID').val(CID);
						$('#region').val(region);
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#learnerCustomerId').val(customerId);
						$('#titleOfcourtesy').val(titleOfCourtesy);
						$('#name').val(name);
						$('#occupation').val(occupation);
						$('#nationality').val(nationality);
						$('#cid').val(CID);
						$('#bloodGroup').val(bloodgroupId);
						$('#displayDob').val(dob);
						$('#fathersName').val(fathersname);
						$('#identificationMarks').val(identificationMarks);
						$('#remarks').val(remarks);
						$('#licenseNO').val(licenseNO);
						$('#learnerlicenseno').val(learnerlicenseno);
						if(gender == "M")
							$('#maleRadio').attr('checked', true);
						else if(gender == "F")
							$('#femaleRadio').attr('checked', true);
						
						
						$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
						
						$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
						$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
						$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
						
						$('#cid').html("<label class='control-label'>"+CID+"</label>");
						$("#duplicateCID").val(CID);
						$("#personalInfoId").val(personalInfoId); 
						$('#village').html("<label class='control-label'>"+village+"</label>");
						$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
						$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
						$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
						$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
						searchsuspensionlist();
					}
				}
				else if(searchType == "DRIVETYPE_SUSPENSION")
				{
					if(licensestatus == "ISSUED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already issued </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
						$('#Msg').show();
					}	
					else if(licensestatus == "SUSPENDED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "HAS_OFFENCE")
					{
						$("#Msg").html("<div class='alert alert-danger'>License has offence </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
						$('#Msg').show();
					}
					else if(licensestatus == "SUCCESS")
					{
						$("#submitBtn").prop('disabled', false);
						$('#status').html("<label class='control-label'>"+status+"</label>");
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#citizenID').val(CID);
						$('#region').val(region);
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#learnerCustomerId').val(customerId);
						$('#titleOfcourtesy').val(titleOfCourtesy);
						$('#name').val(name);
						$('#occupation').val(occupation);
						$('#nationality').val(nationality);
						$('#cid').val(CID);
						$('#bloodGroup').val(bloodgroupId);
						$('#displayDob').val(dob);
						$('#fathersName').val(fathersname);
						$('#identificationMarks').val(identificationMarks);
						$('#remarks').val(remarks);
						$('#licenseNO').val(licenseNO);
						$('#learnerlicenseno').val(learnerlicenseno);
						$('#customerId').val(customerId);
						if(gender == "M")
							$('#maleRadio').attr('checked', true);
						else if(gender == "F")
							$('#femaleRadio').attr('checked', true);
						
						
						$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
						
						$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
						$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
						$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
						
						$('#cid').html("<label class='control-label'>"+CID+"</label>");
						$("#duplicateCID").val(CID);
						$("#personalInfoId").val(personalInfoId); 
						$('#village').html("<label class='control-label'>"+village+"</label>");
						$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
						$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
						$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
						$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
						searchdrivetypesuspensionlist();
					}
				}
				else if(searchType == "LICENSE_OFFENCE")
				{
					if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
						$('#Msg').show();
					}	
					else if(licensestatus == "SUSPENDED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
						$('#Msg').show();
					}
					else 
					{
						$('#licenseNo').val(licenseNO);
						$('#learnerLicenseId').val('0');
						$('#learnerlicenseno').val(learnerlicenseno);
						$('#LicenseHolderName').html("<label class='control-label'>"+name+"</label>");
					}
				}
				else if(searchType == "LICENSE_COMMERCIAL")
				{
					if(licensestatus == "ISSUED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already issued or already applied against this Customer</div>");
						$('#Msg').show();
					}
					else if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is alredy cancelled</div>");
						$('#Msg').show();
					}	
					else if(licensestatus == "SUSPENDED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already suspended </div>");
						$('#Msg').show();
					}
					else if(licensestatus == "HAS_OFFENCE")
					{
						$("#Msg").html("<div class='alert alert-danger'>License has offence </div>");
						$('#Msg').show();
					}

					else if(licensestatus == "LICENSE_ALREADY_ISSUED")
					{
						$("#Msg").html("<div class='alert alert-danger'>Already applied or issued </div>");
						$('#Msg').show();
					}
					
					else if(licensestatus == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
						$('#Msg').show();
					}
					else if(licensestatus == "SUCCESS")
					{
						$("#submitBtn").prop('disabled', false);
						$('#status').html("<label class='control-label'>"+status+"</label>");
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayVillage').html("<label class='control-label'>"+village+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#citizenID').val(CID);
						$('#region').val(region);
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#learnerCustomerId').val(customerId);
						$('#titleOfcourtesy').val(titleOfCourtesy);
						$('#name').val(name);
						$('#occupation').val(occupation);
						$('#nationality').val(nationality);
						$('#cid').val(CID);
						$('#bloodGroup').val(bloodgroupId);
						$('#displayDob').val(dob);
						$('#fathersName').val(fathersname);
						$('#identificationMarks').val(identificationMarks);
						$('#remarks').val(remarks);
						$('#licenseNO').val(licenseNO);
						$('#learnerlicenseno').val(learnerlicenseno);
						
						if(gender == "M")
							$('#maleRadio').attr('checked', true);
						else if(gender == "F")
							$('#femaleRadio').attr('checked', true);
						
						
						$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
						
						$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
						$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
						$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
						
						$('#cid').html("<label class='control-label'>"+CID+"</label>");
						$("#duplicateCID").val(CID);
						$("#personalInfoId").val(personalInfoId); 
						$('#village').html("<label class='control-label'>"+village+"</label>");
						$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
						$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
						$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
						$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
						searchsuspensionlist();
						 
						//getdrivetype();
					}
				}
				else if(searchType == "LICENSE_CANCELLATION")
				{
					if(licensestatus == "CANCELLED")
					{
						$("#Msg").html("<div class='alert alert-danger'>License is already cancelled</div>");
						$('#Msg').show();
					}
					else
					{
						if(status=="A")
							{$('#status').val("Active");}
						
						$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
						$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
						$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#citizenID').val(CID);
						$('#region').val(region);
						$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
						$('#learnerCustomerId').val(customerId);
						$('#titleOfcourtesy').val(titleOfCourtesy);
						$('#name').val(name);
						$('#occupation').val(occupation);
						$('#nationality').val(nationality);
						$('#cid').val(CID);
						$('#bloodGroup').val(bloodgroupId);
						$('#displayDob').val(dob);
						$('#fathersName').val(fathersname);
						$('#identificationMarks').val(identificationMarks);
						$('#remarks').val(remarks);
						$('#licenseNO').val(licenseNO);
						$('#learnerlicenseno').val(learnerlicenseno);
						$('#customerId').val(customerId);
						if(gender == "M")
							$('#maleRadio').attr('checked', true);
						else if(gender == "F")
							$('#femaleRadio').attr('checked', true);
						
						
						$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
						
						$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
						$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
						$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
						$('#gender').html("<label class='control-label'>"+gender+"</label>");
						$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
						$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
						$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
						$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
						
						$('#cid').html("<label class='control-label'>"+CID+"</label>");
						$("#duplicateCID").val(CID);
						$("#personalInfoId").val(personalInfoId); 
						$('#village').html("<label class='control-label'>"+village+"</label>");
						$('#learner').html("<label class='control-label'>"+learnerlicenseno+"</label>");
						$('#displayregion').html("<label class='control-label'>"+regionName+"</label>");
						$('#displayexpiry').html("<label class='control-label'>"+expirydate+"</label>");
						$('#displayissue').html("<label class='control-label'>"+issuedate+"</label>");
					}
				}
				
				if(licensestatus == "SUCCESS")
				{
					$('#LicenseHolderName').html("<label class='control-label'>"+name+"</label>");
				}
				else if(licensestatus == "PENDING_APPLICATION" && searchType!="LICENSE_HISTORY")
				{
					$("#submitBtn").prop('disabled', true);
					$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+cancelReason+"</div>");
					$('#Msg').show();
				}
					}
			});
			}
		});
		
	}
	
	function get_PIS(personalInfoId)
	{	
		$("#originalCID").val('');
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getPersonalDtlsForEdit&personalInfoId='+personalInfoId,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
				 	var customerId = $(this).find('customerId').text();
				 	var cid = $(this).find('cid').text();
				 	var titleCourtesy = $(this).find('title-courtesy').text();
				 	var firstName = $(this).find('firstname').text();
				 	var middleName = $(this).find('middlename').text();
				 	var lastName = $(this).find('lastname').text();
				 	var occupation = $(this).find('occupation').text();
				 	var nationality = $(this).find('nationality').text();
				 	var dob = $(this).find('dob').text();
				 	var fatherName = $(this).find('father-name').text();
				 	var bloodGroup = $(this).find('blood-group').text();
				 	var gender = $(this).find('gender').text();
				 	var remarks = $(this).find('remarks').text();
				 	var isInternational = $(this).find('is-international').text();
				 	var dzongkhag = $(this).find('dzongkhag').text();
				 	var gewog = $(this).find('gewog').text();
				 	var village = $(this).find('village').text();
				 	var country = $(this).find('country').text();
				 	var address = $(this).find('address').text();
				 	var presentDzongkhag = $(this).find('present-dzongkhag').text();
				 	var contactAddress = $(this).find('present-address').text();
				 	var phone = $(this).find('phone').text();
				 	var email = $(this).find('email').text();
				 	var personalInfoId = $(this).find('personal-info-id').text();
				 	var imagePath = $(this).find('image-path').text();
				 	var contactRadio = $(this).find('contactRadio').text();
				 	populateDependentDropDown(dzongkhag, 'gewog', '', 'GEWOG_LIST', 'N');

				 	$('#customerID').val(customerId);
				 	$('#cid').val(cid);
				 	$('#titleOfcourtesy').val(titleCourtesy);
				 	$('#firstname').val(firstName);
				 	$('#middlename').val(middleName);
				 	$('#lastName').val(lastName);
				 	$('#personalOccupation').val(occupation);
				 	$('#personalNationality').val(nationality);
				 	$('#displayDob').val(dob);
				 	$('#fathersName').val(fatherName);
				 	$('#bloodGroup').val(bloodGroup);

				 	if(gender == "M")
					 	$('#maleRadio').attr('checked', true);
				 	else
				 		$('#femaleRadio').attr('checked', true);

			 		$('#remarks').val(remarks);


			 		if(isInternational == "N")
		 			{
			 			$('#nationalRadio').attr('checked', true);
			 			enable('National');
			 		}
			 		else
			 		{
			 			$('#internationalRadio').attr('checked', true);
			 			enable('International');
			 		}
			 		if(contactRadio == "Official")
			 		{
				 		$('#officialRadio').attr('checked', true);
				 		$('#ministry').val(ministry);
			 			$('#department').val(department);
			 			enable('Official');
			 		}
			 		else
			 		{
			 			$('#privateRadio').attr('checked', true);
			 			enable('Private');
			 		}

		 			$('#dzongkhag').val(dzongkhag);
		 			$('#gewog').val(gewog);
		 			$('#village').val(village);
		 			$('#country').val(country);

		 			if(address == "" || address == "null")
		 				$('#address').val("");
		 			else
		 				$('#address').val(address);
	 				
		 			$('#presentDzongkhag').val(presentDzongkhag);
		 			$('#contactAddress').val(contactAddress);
		 			$('#phone').val(phone);
		 			$('#email').val(email);
		 			$('#personalInfoId').val(personalInfoId);
		 			$('#msgDiv').html('');

		 			if(imagePath != "null")
			 		{
		 				$('#pic').html('<span class="profile-picture"><img class="editable img-responsive" id="" style="width:150px; height: 150px;" src="<%=request.getContextPath()%>/ImageServlet?url='+imagePath+'"/></span>');
						$('#pic').show();
			 		}
		 			else
			 		{
		 				$('#pic').html('');
						$('#pic').hide();
				 	}

		 			$('#firstname').attr('readonly', false);
		 			$('#middlename').attr('readonly', false);
		 			$('#lastName').attr('readonly', false);
		 			$('#fathersName').attr('readonly', false);
		 			$('#village').attr('readonly', false);
		 			$('#country').attr('disabled', false);
		 			$('#address').attr('disabled', false);
				});
			}
		});
	}
--></script>
 