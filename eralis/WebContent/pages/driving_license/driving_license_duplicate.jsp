<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		License Replacement
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			replacement of driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			
						<div class="row">
							<div class="col-lg-12">
								<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">License Number:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
												<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-lg-3">
										  	<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a license number</label>
										</div>
									</div>
							</div>
						</div>
						<div id="Msg"></div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label class="control-label">Customer ID:</label>
								</div>
								<div class="col-lg-4" id="displayCustomerId"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
								
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"></div>
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
							<div id="nationalDIV">
								<h4>Permanent Address(National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Dzongkhag:</label>
									</div>
									<div class="col-lg-4" id="displayDzongkhag"></div>
									<div class="col-lg-2">
										<label class="control-label">Gewog:</label>
									</div>
									<div class="col-lg-4" id="displayGewog"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Village:</label>
									</div>
									<div class="col-lg-4" id="displayVillage"></div>
								</div>
							</div>
							<div id="internationalDIV">
								<h4>Permanent Address(Foreign National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Country:</label>
									</div>
									<div class="col-lg-3" id="displayCountry">
									</div>
									<div class="col-lg-2">
										<label>Address:</label>
									</div>
									<div class="col-lg-3" id="displayAddress">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drivetypes:</label>
								</div>
								<div id="drivetypelist">
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Replacement List</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div id="duplicationlistTable">
					</div>
				</div>
			</div>
		</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Replacement Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<!--<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Duplication Date <span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="duplicationdate" styleClass="form-control date-picker" styleId="duplicationdate"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								<div class="col-lg-2">
									<label class="control-label">Delivered On:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<html:text property="deliveredOn" styleClass="form-control date-picker" styleId="deliveredOn"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
								
							</div>
							--><div class="form-group">
								
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="licensetype" styleId="licensetype1" /> 
			<html:hidden property="region" styleId="region"/>
			<html:hidden property="pageId" styleId="pageId" /> 
			<html:hidden property="customerId" styleId="learnerCustomerId"/> 
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal>
			<%-- <logic:equal value="Y" name="priviledge" property="isEdit">
				<button class="btn btn-primary btn-sm">Update</button>
			</logic:equal>
			<logic:equal value="Y" name="priviledge" property="isDelete">
				<button class="btn btn-primary btn-sm">Delete</button>
			</logic:equal>  --%>
		</div>
		</html:form>
	</div>
</div>


<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
				$('.fileupload').ace_file_input({
					no_file : 'No File ...',
					btn_choose : 'Choose',
					btn_change : 'Change',
					droppable : false,
					onchange : null,
					thumbnail : false,
					whitelist:'png|jpg|jpeg',
					blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
				});
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
				function getdrivetype()
				{
					var customerId	=	$("#learnerCustomerId").val();
					$.ajax
					({
						
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getdrivinglicensedrivetype&customerId="+customerId,
						
						data : $('form').serialize(),
						cache : false,
						async: true,
						dataType : "html",
						success : function(responseText) 
						{
							
							$("#drivetypelist").html(responseText);
							$("#drivetypelist").show();
						}
					});
				}
				function searchPersonalInfo()
				{
					var cidNumber 	= $('#cidPersonalModal').val();
					var licenseNo	= $('#licenseNoModal').val();
					
					if(cidNumber == "")
						cidNumber = "NA";
					if(licenseNo == "")
						licenseNo = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=LICENSE_DUPLICATE",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#licenseDetailList").html(responseText);
							$("#licenseDetailList").show();
						}
					});
					 
				} 

			//Duplication lists
			function searchlicenseduplicationlist()
				{
					//alert("Hello");
					var drivinglicenseId = $('#drivinglicenseId').val();
					
					if(drivinglicenseId == "")
						drivinglicenseId = "NA";
					
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getlicenseduplication&drivinglicenseId="+drivinglicenseId,
						
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#duplicationlistTable").html(responseText);
							$("#duplicationlistTable").show();
							getdrivetype();
						}
					});
					
				}
			
			//validation starts
			$(document).ready(function()
			{
				$('#submitBtn').click(function()
				{
					var licenseNo = $('#licenseNO').val();
					
					if(licenseNo == "")
					{
						$('#licenseNoValidation').show();
						$('#licenseNoValidation').get(0).scrollIntoView();
						return false;
					}
					else
					{
						//For license duplicate
						requestType = "LICENSE";
						var serviceType = "DUPLICATE";
						var identityNo = $('#licenseNO').val();
						var identityTypeId = $('#licensetype1').val(); //if non-commercial N else C
						//alert($('#licensetype').val());
						
						getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '', '');
					}
				});
			});
			
			function formSubmit()
			{
				var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_duplication',type:'POST',data: $("#learnerForm").serialize()}; 
			    $("#learnerForm").ajaxSubmit(options);
		        $('#displayMsgDiv').show();
		        setTimeout('hideStatus("displayMsgDiv")',10000);
		        //setTimeout('reloadPage()',10000);
			}
			
			var fileError;
           	function validation(thisform,msgId,fileObj)
           	{
           		var fileId = fileObj.id;
           		with(thisform)
           		{
           			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
           			{
           				document.getElementById(fileId).value = "";
           				return false;
           			}
           		}
           	}
           	
			<%
				String pageIdentifier = (String) request.getAttribute("page_identifier");
				String pageId = (String) request.getAttribute("page_id");
			%>

			var pageIdentifier = "<%=pageIdentifier%>";
			var pageId = "<%=pageId%>";
</script>
<style>
	#learnerForm .error { color: red; }
</style>