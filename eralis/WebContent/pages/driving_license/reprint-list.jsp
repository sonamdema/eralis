<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>

	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-credit-card"></i>
			Approve reprint requests
		</h1>
	</div>
   <form>
	   <div class="row">
		  <div class="widget-box" >
			   <div class="widget-body">
				   <div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover col-lg-9" id="offenceTable">
	                               <thead>
	                                   <tr>
	                                       <th>#</th>
	                                       <th>Requested By</th>
	                                       <th>Requested On</th>
	                                       <th>Request Remarks</th>
	                                       <th>Action</th>
	                                   </tr>
	                               </thead>
	                               <tbody>
	                               		<logic:iterate id="reprint" name="REPRINT_LIST" type="bt.gov.rsta.eralis.dto.license.LicenseDTO" indexId="index">
	                               			<%
	                               				int a = index.intValue();
	                               			%>
	                               			<tr>
	                               				<td><%=++a %></td>
	                               				<td>
	                               					<bean:write name="reprint" property="name"/>
	                               				</td>
	                               				<td>
	                               					<bean:write name="reprint" property="appsubmissiondate"/>
	                               				</td>
	                               				<td>
	                               					<bean:write name="reprint" property="remarks"/>
	                               				</td>
	                               				<td>
	                               					<a class="btn btn-primary btn-xs" onclick="openReprintDetails('<bean:write name="reprint" property="drivinglicenseId"/>')">
	                               						<i class="fa fa-eye fa-fw"></i>&nbsp;
	                               						View
	                               					</a>
	                               				</td>
	                               			</tr>
	                               		</logic:iterate>
	                               </tbody>
	                            </table>
							</div>
						</div>			        
	                 </div>
	              </div>
	         </div>
	    </div> 
    </form>
    <script>

		function openReprintDetails(requestId)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/license.html?method=get_reprint_details&requestId="+requestId,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#contentDisplayDiv").html(responseText);
					$("#contentDisplayDiv").show();
				}
			});
		}

		$(document).ready(function() 
		{
		    $('#offenceTable').DataTable({
		            responsive: true
		    });
		});

    </script>