<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>
<%

	LicenseDTO dto = (LicenseDTO) request.getAttribute("REPRINT_DETAILS");

%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-credit-card"></i>
			Approve Request for reprint
		</h1>
	</div>
   
   <div class="row">
		<html:form styleClass="form-horizontal" action="/license.html?method=approve_request_reprint" styleId="reprintForm">
		  <div class="widget-box" >
			   <div class="widget-body">
				   <div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										Requested By:
									</div>
									<div class="col-lg-4">
										<%=dto.getName() %>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										Requested On:
									</div>
									<div class="col-lg-4">
										<%=dto.getAppsubmissiondate() %>
									</div>
								</div>	
								<div class="form-group">
									<div class="col-lg-2">
										Request Reason:
									</div>
									<div class="col-lg-4">
										<%=dto.getRemarks()%>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<table class="table table-striped table-hover col-lg-9" id="offenceTable">
	                               <thead>
	                                   <tr>
	                                       <th>#</th>
	                                       <th>
	                                       		Driving License/TOP Number
	                                       </th>
	                                   </tr>
	                               </thead>
	                               <tbody>
	                               		<logic:iterate id="license" name="REPRINT_DETAILS" property="offenceList" indexId="index">
	                               			<%
	                               				int a = index.intValue();
	                               			%>
											<tr>
												<td><%=++a %></td>
												<td>
													<bean:write name="license" property="offenceId"/>
												</td>
											</tr>
										</logic:iterate>
	                               </tbody>
	                            </table>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										Approval Remarks:
									</div>
									<div class="col-lg-4">
										<html:textarea property="remarks" styleClass="form-control"></html:textarea>
									</div>
								</div>	
							</div>
						</div>
                  </div>
                  	<div id="messageDiv"></div>
                	<div class="pull-right">
						<button type="button" class="btn btn-primary btn-sm"  onclick="formSubmit()">Approve</button>
						<html:hidden property="drivinglicenseId" styleId="requestId" value="<%=dto.getDrivinglicenseId() %>"/>
					</div>
               </div>
          </div>
      	</html:form>
    </div> 
    
    <script type="text/javascript">

    function formSubmit()
	{
    	var requestId = $('#requestId').val();
    	
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/license.html?method=approve_request_reprint&requestId='+requestId,type:'POST',data: $("#reprintForm").serialize()}; 
	    $("#reprintForm").ajaxSubmit(options);
        $('#messageDiv').show();
        setTimeout('hideStatus("messageDiv")',5000);
	}

    $(document).ready(function() 
	{
	    $('#offenceTable').DataTable({
	            responsive: true
	    });
	});
    
</script>        				