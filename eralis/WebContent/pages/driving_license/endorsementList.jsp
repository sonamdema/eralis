<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="endorsement-list-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<% int i = 1; %>
			<th>Sl</th>
			<th>License Number</th>
			<th>DriveType</th>
			<th>Endorsement Date</th>
			<th>Delivered On</th>
			<th>IID</th>
			<th>Receipt No</th>
			<th>Receipt Date</th>
			<th>Supporint Doc</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="LICENSE_ENDORSEMENT_LIST">
			<logic:iterate id="endorsement" name="LICENSE_ENDORSEMENT_LIST">
				<tr>
					<td><%=i++ %></td>
					<td><bean:write name="endorsement" property="licenseNo"/></td>
					<td><bean:write name="endorsement" property="drivetype"/></td>
					<td><bean:write name="endorsement" property="endorseddate"/></td>
					<td><bean:write name="endorsement" property="deliveredon"/></td> 
					<td><bean:write name="endorsement" property="iid"/></td>
					<td><bean:write name="endorsement" property="receiptNo"/></td> 
					<td><bean:write name="endorsement" property="receiptDate"/></td>
					<td>
						<a href="#" onclick="downloadFile('<bean:write name="endorsement" property="fileUUID" filter="false"/>','<bean:write name="endorsement" property="fileName" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="endorsement" property="fileName" filter="false"/>
	  					</a>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
   		$('#endorsement-list-table').DataTable({
            responsive: true
    	});
	});

</script>
