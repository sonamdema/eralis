<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Driving License Renewal
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			renewal driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
						<div id="Msg"></div>
						<div class="form-group">
							<div class="col-lg-2">
								<label class="control-label">License No:</label>
							</div>
							<div class="col-lg-3">
								<div class="input-group">
									<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
									<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
									<span class="input-group-btn">
										<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
											<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
										</button>
									</span>
								</div>
							</div>	
							<div class="col-lg-3">
							  	<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a license number</label>
							</div>	
						</div>
				</div>
			</div>
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label class="control-label">Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
								
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Customer ID:</label>
								</div>
								<div class="col-lg-4" id="customerID"></div>
								
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="displayBloodgroup"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"></div>	
							</div>
							
							<h4>Permanent Address</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" id="displayVillage"></div>
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-3" id="displayCountry">
									
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Address:</label>
								</div>
								<div class="col-lg-3" id="displayAddress">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-4" id="displayregion"></div>
								<div class="col-lg-2">
									<label class="control-label">Status:</label>
								</div>
								<div class="col-lg-4" id="status"></div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Issue Date:</label>
								</div>
								<div class="col-lg-4" id="displayissue"></div>
								<div class="col-lg-2">
									<label>Expiry Date:</label>
								</div>
								<div class="col-lg-4" id="displayexpiry"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drivetypes:</label>
								</div>
								<div id="drivetypelist">
								</div>
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Lists</h5>
			</div>
			<div class="widget-body">
					<div class="widget-main">
						<div id="renewallistTable">
						</div>
					</div>
			</div>
		</div>
			
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Renewal Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Region:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
									<input type="text" disabled="disabled" value="<%=regionName %>" class="form-control"/>
									<html:hidden property="region" value="<%=regionId %>"/>
									</div>
								</div>
								<div class="col-lg-2">
									<label>Renewal Duration:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
	                           			<html:option value="">--SELECT--</html:option>
	                           		</html:select>
									<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select renewal duration</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks"
										styleClass="form-control"></html:textarea>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-3">
								<label>Supporting Documents<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="supportDoc" styleId="supportDoc" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="licensetype" styleId="licensetype1" /> 
			<html:hidden property="pageId" styleId="pageId" /> 
			<html:hidden property="customerId" styleId="learnerCustomerId"/> 
			<html:hidden property="region" styleId="region"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Calculate Payment</button>
			</logic:equal>
		</div>
		</html:form>
	</div>
</div>
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>Citizen ID:</label>
								<span class="pull-right">
									<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
								</span>
							</div>
							<div class="form-group">
								<label>License No:</label>
								<span class="pull-right">
									<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
								</span>
							</div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
					$('.fileupload').ace_file_input({
						no_file : 'No File ...',
						btn_choose : 'Choose',
						btn_change : 'Change',
						droppable : false,
						onchange : null,
						thumbnail : false,
						whitelist:'png|jpg|jpeg',
						blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
					});
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

				
		
	</script>
	<script>
	
		
		function searchPersonalInfo()
		{
			var cidNumber 	= $('#cidPersonalModal').val();
			var licenseNo	= $('#licenseNoModal').val();
			
			if(cidNumber == "")
				cidNumber = "NA";
			if(licenseNo == "")
				licenseNo = "NA"; 
			var licenseId = '<%=request.getAttribute("identityNo")%>';
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=LICENSE_RENEWAL",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#licenseDetailList").html(responseText);
					$("#licenseDetailList").show();
				}
			});
		} 

	</script>
			
<script><!--
			//renewal lists
			
			function searchlicenserenewallist(drivingLicenseId,age,totalDuration,dateDifference,country,CID)
			{
				if(drivingLicenseId == "")
					drivingLicenseId = "NA";
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getlicenserenewal&drivingLicenseId="+drivingLicenseId,
					
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{
						$("#renewallistTable").html(responseText);
						$("#renewallistTable").show();
						getdrivetype();
						populateRenewalDurationDropdown(age,totalDuration,dateDifference,country,CID);
					}
				});
			}

			function populateRenewalDurationDropdown(age,totalDuration,dateDifference,country,CID)
			{
				
				var month	=	null;
				var year	=	null;
				var	duration = null;
				$('#renewalDuration').empty();
				
				var licenseType = $('#licensetype1').val();
				
				if(country.includes("Bhutan"))
				{
					
					if((CID.includes("RP") || CID.includes("R.P") ||  CID.includes("T") ||  CID.includes("OC") ||
							CID.includes("O.C") ||  CID.includes("MC") ||  CID.includes("M.C") || CID.includes("Dha") || CID.includes("DCRC"))
							&& (!CID.includes("SRP") && !CID.includes("S.R.P")))
					{
						$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
						
					}
					else{
						if(licenseType == "N")
						{ 
							//if 70 n above
							if(age>=70)
							{
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
							}
							//if above 69
							else if(age>69)
							{
								$('#renewalDuration').append("<option value='"+totalDuration+"'>"+dateDifference+"</option>");
							}
							//if above 65
							else if(age>65)
							{
								$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
								$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
							}
							//if above 60
							else if(age>60)
							{
								$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
								$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
								/* $('#renewalDuration').append("<option value='48_month'>4 Years</option>");
								$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
								$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>"); */
								
							}
							else
							{
								$('#renewalDuration').append("<option value='120_month'>10 Years</option>");
								$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
								/* $('#renewalDuration').append("<option value='108_month'>9 Years</option>");
								$('#renewalDuration').append("<option value='96_month'>8 Years</option>");
								$('#renewalDuration').append("<option value='84_month'>7 Years</option>");
								$('#renewalDuration').append("<option value='72_month'>6 Years</option>");
								$('#renewalDuration').append("<option value='60_month'>5 Years</option>");
								$('#renewalDuration').append("<option value='48_month'>4 Years</option>");
								$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
								$('#renewalDuration').append("<option value='24_month'>2 Years</option>");
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>"); */
							}
						}
						else
						{
							if(age>=65)
							{
								//$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
							}
							else if(age>=60)
							{
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
							}
							else if(age==59)
							{
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
							}
							else if(age==58)
							{
								$('#renewalDuration').append("<option value='12_month'>2 Year</option>");
							}
							else if(age==57)
							{
								$('#renewalDuration').append("<option value='12_month'>3 Year</option>");
							}
							//else if(age>64)
							//{
							//	$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
							//}
							//else if(age>63)
							//{
							//	$('#renewalDuration').append("<option value='"+totalDuration+"_days'>"+dateDifference+"</option>");
							//	if(totalDuration>=365)
							//	{
							//		$('#renewalDuration').append("<option value='12_month'>1 Years</option>");
							//	}
							else
							{
								$('#renewalDuration').append("<option value='36_month'>3 Years</option>");
								$('#renewalDuration').append("<option value='24_month'>2 Year</option>");
								$('#renewalDuration').append("<option value='12_month'>1 Year</option>");
							}
						}
						
					}
					
				} 
				else
				{
				 	 $('#renewalDuration').append("<option value='12_month'>1 Year</option>");
					 
					 
					 
				 }
				
			}
			
			function getdrivetype()
			{
				var customerId	=	$("#learnerCustomerId").val();
				$.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/common.html?method=getdrivinglicensedrivetype&customerId="+customerId,
					data : $('form').serialize(),
					cache : false,
					async: true,
					dataType : "html",
					success : function(responseText) 
					{
						$("#drivetypelist").html(responseText);
						$("#drivetypelist").show();
					}
				});
			}

		$(document).ready(function()
		{  
			$('#submitBtn').click(function()
			{
				var licenseNo = $('#licenseNO').val();
				var renewalDuration = $('#renewalDuration').val();
				
				if(licenseNo == "")
				{
					$('#licenseNoValidation').show();
					$('#licenseNoValidation').get(0).scrollIntoView();
					return false;
				}
				if(renewalDuration == "")
				{
					$('#renewalDurationValidation').show();
					$('#renewalDurationValidation').get(0).scrollIntoView();
					return false;
				}
				else
				{
					//For license renewal
					requestType = "LICENSE";
					var serviceType = "RENEWAL";
					var identityNo = $('#drivinglicenseId').val();
					var identityTypeId = $('#licensetype1').val(); //if non-commercial N else C
					var renewalDuration = $('#renewalDuration').val();
					getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '', renewalDuration);
				}
			});
		});
		
		function formSubmit()
		{
			var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_renewal',type:'POST',data: $("#learnerForm").serialize()}; 
		    $("#learnerForm").ajaxSubmit(options);
	        $('#displayMsgDiv').show();
	        setTimeout('hideStatus("displayMsgDiv")',10000);
	        //setTimeout('reloadPage()',10000);
		}

			var fileError;
         	function validation(thisform,msgId,fileObj)
         	{
         		var fileId = fileObj.id;
         		with(thisform)
         		{
         			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
         			{
         				document.getElementById(fileId).value = "";
         				return false;
         			}
         			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
         			{
         				document.getElementById(fileId).value = "";
         				return false;
         			}
         		}
         	}
           	
			<%
				String pageIdentifier = (String) request.getAttribute("page_identifier");
				String pageId = (String) request.getAttribute("page_id");
			%>

			var pageIdentifier = "<%=pageIdentifier%>";
			var pageId = "<%=pageId%>";
			
--></script>
<style>
	#learnerForm .error { color: red; }
</style>