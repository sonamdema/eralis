<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Cancellation Withdrawal
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Withdraw cancellation driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div id="Msg"></div>
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="licenseForm">
			
						<div class="row">
							<div class="col-lg-12">
									
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">License No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
												<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-lg-3">
										  	<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a license number</label>
										  </div>
									</div>
									
							</div>
						</div>
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
								<div class="col-lg-2">
									<label>Customer ID:</label>
								</div>
								<div class="col-lg-4" id="customerID"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
							</div>
							<h4>Permanent Address</h4>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" ></div>
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-3">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-3">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Licesne Cancellation Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<table  id="cancellation-search-table"  class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Cancellation Date</th>
									<th>Cancellation Reason</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="cancellation_date"></div></td>
									<td><div id="cancellation_reason"></div></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Withdraw Cancellation</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Withdrawn Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<div class="input-group">
										<html:text property="withdrawnDate" styleClass="form-control date-picker" styleId="withdrawnDate" readonly="true"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Withdrawn Reason<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="withdrawnReason" styleId="withdrawnReason" styleClass="form-control"></html:textarea>
								</div>
							</div>
							<div class="col-lg-2">
								<label> Document :</label>
							</div>
							<div class="col-lg-3">
								<html:file property="imgPath" styleId="invoice" styleClass="form-control fileupload" onchange="validation('licenseForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
								<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="displayMsgDiv">&nbsp;</div>
		<div class="pull-right">
			<html:hidden property="customerId" styleId="customerId"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" disabled="disabled" id="submit_button">Save</button>
			</logic:equal>
		</div>
		</html:form>
	</div>
</div>

<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						
						
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		 
			
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				</script>
				<script type="text/javascript">

				function searchPersonalInfo()
				{
					var cidNumber 	= $('#cidPersonalModal').val();
					var licenseNo	= $('#licenseNoModal').val();
					
					if(cidNumber == "")
						cidNumber = "NA";
					if(licenseNo == "")
						licenseNo = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=WITHDRAW_LICENSE_CANCELLATION",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#licenseDetailList").html(responseText);
							$("#licenseDetailList").show();
						}
					});
					 
				} 
				
				$(document).ready(function()
						{
							$("#licenseForm").validate({
										rules:
										{
											cancellationdate:{
												required:true,
											},
											cancellationReason:{
												required:true,
											},
											status:{
												required:true,
											},
										},    
										messages:
										{
											cancellationdate:{required: "Please Provide Receipt Date"},
											cancellationReason:{required: "Please Provide a valid reason"},
											status:{required: "Please provide status"}
										}
								});

							$('#submit_button').click(function()
							{
								var licenseNO = $('#licenseNO').val();

								if(licenseNO == "")
								{
									$('#licenseNoValidation').show();
									$('#licenseNoValidation').get(0).scrollIntoView();
									return false;
								}
								else
								{
									if($('#licenseForm').valid()) 
									{
										var options = {target:'#displayMsgDiv',url:context+'/license.html?method=withdraw_license_cancellation&FORM_TYPE=DL',type:'POST',data: $("#licenseForm").serialize()}; 
									    $("#licenseForm").ajaxSubmit(options);
								        $('#displayMsgDiv').show();
								        setTimeout('hideStatus("displayMsgDiv")',5000);
								        setTimeout('reloadPage()',5000);
									}
									else
									{
										return false;
									}
								}
							});
						});
				 	var pageIdentifier = "<%=pageIdentifier%>";
				  	var pageId = "<%=pageId%>";
					</script>
					<link rel="stylesheet" media="screen" href="css/screen.css">


					<style>
						#licenseForm .error { color: red; }
					</style>