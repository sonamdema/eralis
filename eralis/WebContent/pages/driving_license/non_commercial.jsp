<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String regionId = (String) request.getAttribute("REGION_ID");
	String regionName = (String) request.getAttribute("REGION_NAME");
%>
<script>
	var context = "<%=request.getContextPath()%>";
	var a="<%=request.getAttribute("page_id")%>";
	$("#pageId").val(a);
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Non-commercial Driving License
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			issue Non-commercial driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			
						<div class="row">
							<div class="col-lg-12">
							<jsp:include page="/pages/payment/payment-modal.jsp"></jsp:include>
							<div id="validationMsg" ></div>
							<div id="Msg" style="display:none;"></div>
									<!--<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Issued To:</label>
										</div>
										<div class="col-lg-3">
											<html:select property="issuedTo">
											<html:option value="">--SELECT--</html:option>
											</html:select>
										</div>
									</div>-->
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Learner License No:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="LLNo" styleClass="form-control" styleId="learnerlicenseno" readonly="true"></html:text>
												<html:hidden property="learnerLicenseId" styleClass="form-control" styleId="learnerLicenseId"></html:hidden>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>	
											</div>
											<div><label style="display:none;color: #ff0000" id="learnerNoValidation">Please search a learner license number</label></div>
										</div>
										<div class="col-lg-2">
											<label class="control-label">Citizen ID:</label>
										</div>
										<div class="col-lg-3">
											<div class="input-group">
												<html:text property="CID" styleClass="form-control" styleId="duplicateCID" readonly="true"></html:text>
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licensemodalform')">
														<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
													</button>
												</span>
											</div>
										</div>
									</div>
									
							</div>
						</div>
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								
								<div class="col-lg-2">
									<label class="control-label">Customer ID:</label>
								</div>
								<div class="col-lg-4" id="customerID"></div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Occupation:</label>
								</div>
								<div class="col-lg-4" id="occupation"></div>
								<div class="col-lg-2">
									<label class="control-label">Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
								<div class="col-lg-2">
									<label class="control-label">Blood Group:</label>
								</div>
								<div class="col-lg-4" id="bloodgroup"></div>
							</div>
								
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">CID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
								<div class="col-lg-2">
									<label class="control-label">Father's Name:</label>
								</div>
								<div class="col-lg-4" id="fathersname"></div>
							</div>
							<div id="nationalDIV">
								<h4>Permanent Address(National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Dzongkhag:</label>
									</div>
									<div class="col-lg-4" id="displayDzongkhag"></div>
									<div class="col-lg-2">
										<label class="control-label">Gewog:</label>
									</div>
									<div class="col-lg-4" id="displayGewog"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Village:</label>
									</div>
									<div class="col-lg-4" id="village"></div>
								</div>
							</div>
							<div id="internationalDIV">
								<h4>Permanent Address(Foreign National)</h4>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Country:</label>
									</div>
									<div class="col-lg-4" id="displayCountry"></div>
									<div class="col-lg-2">
										<label class="control-label">Address:</label>
									</div>
									<div class="col-lg-4" id="displayAddress"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Driving License Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="col-lg-2">
									<label>Region<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<input type="text" disabled="disabled" value="<%=regionName %>" class="form-control"/>
									<html:hidden property="region" value="<%=regionId %>"/>
								</div>
								<div class="col-lg-2">
									<label>Issue Type<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="issueType" styleId="issueType" styleClass="form-control" onchange="changeDriveType(this.value)">
		                           		<html:option value="">Select Issue Type</html:option>
		                           		<html:option value="AMC">AMC</html:option>
		                           		<html:option value="Arm_Force">Arm Force</html:option>
		                           		<html:option value="Driving_Institue">Driving Institue</html:option>
		                           		<html:option value="Learner_License">Learner License</html:option>
		                           		<html:option value="Foreign/International_Driving_License">Foreign/International Driving License</html:option>
		                           		<%-- <html:option value="Old_Learner_License">Old Learner License</html:option> --%>
		                           		<html:option value="VTI">VTI</html:option>
		                           		<html:option value="CDCL">CDCL</html:option>
	                           		</html:select>
										<label style="display:none;color: #ff0000" id="issueTypeValidation">Please select validity issued for</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Drive Type<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<!--<input type="text" disabled class="form-control" id="driveTypeName">-->
							          <select id="driveTypeTemp" class="form-control" onchange="validateNonCommercialCriteria(this.value)">
							          	<option value="">--SELECT--</option>
							          	<logic:iterate id="driveType" name="DRIVE_TYPE_LIST">
							          		<option value='<bean:write name="driveType" property="headerId"/>'>
							          			<bean:write name="driveType" property="headerName"/>
							          		</option>
							          	</logic:iterate>
							          </select>
							          <label style="display:none;color: #ff0000" id="driveTypeValidation">Please select a drive type</label>
								</div>
								  
								<div class="col-lg-2">
									<label>Validity Issued For<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:select property="renewalDuration" styleId="renewalDuration" styleClass="form-control">
		                           		<html:option value="120">10 Years</html:option>
		                           		<html:option value="108">9 Years</html:option>
		                           		<html:option value="96">8 Years</html:option>
		                           		<html:option value="84">7 Years</html:option>
		                           		<html:option value="72">6 Years</html:option>
		                           		<html:option value="60">5 Years</html:option>
		                           		<html:option value="84">4 Years</html:option>
		                           		<html:option value="60">3 Years</html:option>
		                           		<html:option value="24">2 Years</html:option>
		                           		<html:option value="12">1 Year</html:option>
	                           		</html:select>
										<label style="display:none;color: #ff0000" id="renewalDurationValidation">Please select validity issued for</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Test Result<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<input type="text" disabled class="form-control" id="testResult">
								</div>
								<div id="msgDIV" style="display:none;" class="col-lg-7 alert alert-danger"></div>
								<div class="col-lg-2">
									<label>Overall Test Marks:</label>
								</div>
								<div class="col-lg-3">
									<input type="text" disabled class="form-control" id="testmarks">
								</div>
								<div id="msgDIV" style="display:none;" class="col-lg-7 alert alert-danger"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Remarks:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="remarks" styleId="remarks" styleClass="form-control"></html:textarea>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
		<div class="widget-header widget-header-small">
			<h5 class="widget-title lighter">Attachments</h5>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="form-group">
							<div class="col-lg-2">
								<label>Application Form:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="fileApplForm" styleId="fileApplForm" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation"></label>
							</div>
							<div class="col-lg-3">
								<label>Refresher Course Certificate:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="rcCertificate" styleId="rcCertificate" styleClass="form-control fileupload" onchange="validation('learnerForm','supportingDocumentValidation1',this)"></html:file>
								<label style="display:none;color: #ff0000" id="supportingDocumentValidation1"></label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div id="displayMsgDiv"></div>
		</div>
		<div class="pull-right">
			<html:hidden property="drivetype" styleId="driveTypeId" /> 
			<html:hidden property="pageId" styleId="pageId" /> 
			<html:hidden property="customerId" styleId="learnerCustomerId"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" onClick="calculate" id="submitBtn">Calculate Payment</button>
			</logic:equal>
		</div>
		</html:form>
	</div>
</div>
<div id="licensemodalform" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>Customer ID:</label>
							<span class="pull-right">
								<input type="text" id="customerIdPersonalModal" placeholder="CustomerID" style="width: 200px"  />
							</span>
						</div>
							
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchCommonInfo()">
					<i class="ace-icon fa fa-search"></i>
					Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>
			</div>
			<div id="licenselisttable">
			</div>
			
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>Learner License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="LearnerLicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
		
	$('.fileupload').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});

	function validateNonCommercialCriteria(driveTypeId)
	{
		$("#testResult").val('');
		$("#testmarks").val('');
		$("#msgDIV").hide();
		var issueType	=	$("#issueType").val();
		var learnerLicenseId	=	$("#learnerLicenseId").val();
		if(learnerLicenseId=='')
		{
			learnerLicenseId	=	$("#duplicateCID").val();
		}
		if(issueType=='Old_Learner_License' || issueType=='Learner_License' || issueType=='Driving_Institue')
		{
			$.ajax
			({
				async: true,
				type: 'POST',
				url: '<%=request.getContextPath()%>/EralisCommonServlet?q=validateNonCommercialCriteria&driveTypeId='+driveTypeId+'&learnerLicenseId='+learnerLicenseId+'&issueType='+issueType,
				success: function(xml)
				{ 
					$(xml).find('xml-response').each(function()
					{
						var testMark = $(this).find('testMark').text();
						var testStatus = $(this).find('testStatus').text();
						var reason = $(this).find('testReason').text();
						var testResult = $(this).find('testResult').text();
						$("#testResult").val(testResult);
						if(testStatus=='0')
						{
							$("#msgDIV").show();
							$('#msgDIV').html("<label>"+reason+"</label>");
							$('#submitBtn').attr('disabled',true);
						}
						else if(testStatus=='1')
						{
							$("#testmarks").val(testMark);
							$('#submitBtn').attr('disabled',false);
						}
					});
				}
			});
		}
	}
	//datepicker plugin
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
		
	$('.fileupload').ace_file_input({
		no_file : 'No File ...',
		btn_choose : 'Choose',
		btn_change : 'Change',
		droppable : false,
		onchange : null,
		thumbnail : false,
		whitelist:'png|jpg|jpeg',
		blacklist:'exe|php|doc|docx|xls|ppt|pdf|mp3'
	});

	function searchPersonalInfo()
	{
		var name 		= $('#nameModal').val();
		var cidNumber 	= $('#cidModal').val();
		var learnerlicenseNo	= $('#licenseNoModal').val();
		var regionId 	= $('#regionModal').val();
		var licenseType	= $('#licenseTypeModal').val();
		
		if(name == "")
			name = "NA";
		if(cidNumber == "")
			cidNumber = "NA";
		if(learnerlicenseNo == "")
			learnerlicenseNo = "NA";

		regionId = "NA";

		licenseType = "NA";
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getLearnerLicesneInfo&name="+name+"&cid="+cidNumber+"&learnerlicenseNo="+learnerlicenseNo+"&region="+regionId+"&licenseType="+licenseType+"&formType=ISSUE_NON_COMMERCIAL_LICENSE",
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#licenseDetailList").html(responseText);
				$("#licenseDetailList").show();
			}
		});
	} 
			
	function searchCommonInfo()
	{
		$('#learnerNoValidation').hide();
		var cidNumber = $('#cidPersonalModal').val();
		var customerId = $('#customerIdPersonalModal').val();
		if(cidNumber == "")
			cidNumber = "NA";
		if(customerId == "")
			customerId = "NA";
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/common.html?method=getPersonalInfoList&name="+name+"&cid="+cidNumber+"&customerId="+customerId+"&searchType=LICENSE_NEW",
			
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#licenselisttable").html(responseText);
				$("#licenselisttable").show();
			}
		});
	}
			
	$(document).ready(function()
	{ 
		$("#learnerForm").validate({
			rules:
			{
				testmarks:{
					required:true,
				},
				drivetype:{
					required:true,
				},
				driveTypeTemp:{
					required:true,
				},
				renewalDuration:{
					required:true,
				},
			},    
			messages:
			{
				testmarks:{required: "Theory test marks is required"},
				drivetype:{required: "Please select a drive type"},
				driveTypeTemp:{required: "Please select a drive type"},
				renewalDuration:{required: "Please select duration of issuance"},
			}
		});

		$('#submitBtn').click(function()
		{
			if($('#learnerForm').valid()) 
			{
				$('#learnerNoValidation').hide();
				$('#driveTypeValidation').hide();
				$('#issueTypeValidation').hide();
				
				var CID	= $('#duplicateCID').val();
				var learnerlicenseno = $('#learnerlicenseno').val();
				var driveTypeTemp = $('#driveTypeTemp').val();
				var issueType	=	$("#issueType").val();

				if((learnerlicenseno == "" && CID=="") || driveTypeTemp == "" || issueType=="")
				{
					if(learnerlicenseno == "" && CID=="")
					{
						$('#learnerNoValidation').show();
						$('#learnerNoValidation').get(0).scrollIntoView();
					}
					if(driveTypeTemp == "")
					{
						$('#driveTypeValidation').show();
						$('#driveTypeValidation').get(0).scrollIntoView();
					}
					if(issueType=="")
					{
						$('#issueTypeValidation').show();
						$('#issueTypeValidation').get(0).scrollIntoView();
					}
					
				}
				
				else
				{
					var requestType = "LICENSE";
					var serviceType = "NEW";
					var identityNo = "";
					var identityTypeId = "N";//if non-commercial N else C
					var renewalDuration	=	$("#renewalDuration").val();
					getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, '', '', '', '', '', '', '', '',renewalDuration);
				}
			}
			else 
			{
				return false;
			}
		});
	});
		
	function formSubmit()
	{
		var driveType = $('#driveTypeTemp').val();
		$('#driveTypeId').val(driveType);
		var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_non_commerical',type:'POST',data: $("#learnerForm").serialize()}; 
	    $("#learnerForm").ajaxSubmit(options);
        $('#displayMsgDiv').show();
        setTimeout('hideStatus("displayMsgDiv")',10000);
	}

	var fileError;
   	function validation(thisform,msgId,fileObj)
   	{
   		var fileId = fileObj.id;
   		with(thisform)
   		{
   			if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","doc","docx","JPG","PDF","JPEG","GIF","PNG","DOC","DOCX")) == false)
   			{
   				document.getElementById(fileId).value = "";
   				return false;
   			}
   			if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
   			{
   				document.getElementById(fileId).value = "";
   				return false;
   			}
   		}
   	}
   	function changeDriveType(issueType)
   	{

		$('#driveTypeTemp').children('option').hide();
		
   		if(issueType=="AMC")
   		{
   			$('#driveTypeTemp option:contains(Power Tiller)').show();
   			$('#driveTypeTemp option:contains(Tractor)').show();
   		}
   		else if(issueType=="VTI")
   		{
   			$('#driveTypeTemp option:contains(Heavy Vehicle)').show();
   			$('#driveTypeTemp option:contains(Medium Vehicle)').show();	
   			$('#driveTypeTemp option:contains(Light Vehicle)').show();
   			$('#driveTypeTemp option:contains(Tourist Light Vehicle)').hide();
   		}
   		else if(issueType=="CDCL")
   		{
   			$('#driveTypeTemp option:contains(Excavator)').show();
   			$('#driveTypeTemp option:contains(Bull Dozer)').show();	
   			$('#driveTypeTemp option:contains(Pay Loader)').show();
   			$('#driveTypeTemp option:contains(Road Roller)').show();
   			$('#driveTypeTemp option:contains(Road Paver)').show();
   		}
   		else if(issueType=="Foreign/International_Driving_License")
   		{
   			$('#driveTypeTemp option:contains(Light Vehicle)').show();
   			$('#driveTypeTemp option:contains(Two Wheeler)').show();
   			$('#driveTypeTemp option:contains(Tourist Light Vehicle)').hide();
   		}
   			
   		$("#msgDIV").hide();
		$("#driveTypeTemp").val('');
		$('#submitBtn').attr('disabled',false);

   	 }
	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>

	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";
	
	
</script>
<style>
	#learnerForm .error { color: red; }
</style>
