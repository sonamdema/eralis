<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>

	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-credit-card"></i>
			Request for reprint
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				 request your higher authority to bring back the license in print list
			</small>
		</h1>
	</div>
   
   <div class="row">
		<html:form styleClass="form-horizontal" action="/license.html?method=request_reprint" styleId="reprintForm">
		  <div class="widget-box" >
			   <div class="widget-body">
				   <div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										Remarks
									</div>
									<div class="col-lg-4">
										<html:textarea property="remarks" styleClass="form-control"></html:textarea>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover col-lg-9" id="offenceTable">
	                               <thead>
	                                   <tr>
	                                       <th>#</th>
	                                       <th>
	                                       	Driving License/TOP Number
	                                       	<span class="pull-right">
												<button type="button" class="btn btn-success btn-sm" onclick="addRow('offenceTable')">
													<i class="fa fa-plus bigger-110"></i>&nbsp;Add More
												</button>
												&nbsp;
												<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow('offenceTable')">
													<i class="fa fa-times bigger-110"></i>&nbsp;Delete
												</button>
											</span>
	                                       </th>
	                                       <th></th>
	                                   </tr>
	                               </thead>
	                               <tbody>
	                               		<nested:iterate id="offence" property="offenceList" name="licenseForm" indexId="index">
											<% int a = index.intValue(); %>
											<nested:nest property="offence">
												<tr>
													<td>1</td>
													<td>
														<nested:text property="offenceId" name="offence" styleId='<%="license_"+a %>' styleClass="form-control" indexed="true"></nested:text>
													</td>
													<td>
														<div class="alert alert-danger" id='<%="msg_"+a%>' style="display: none;"></div>
													</td>
												</tr>
											</nested:nest>
										</nested:iterate>
	                               </tbody>
	                            </table>
							</div>
						</div>			        
                  </div>
                  	<div id="messageDiv"></div>
                	<div class="pull-right">
						<button type="button" class="btn btn-primary btn-sm"  onclick="formSubmit()">Request</button>
					</div>
               </div>
          </div>
      	</html:form>
    </div> 
    
    <script type="text/javascript">
    
	function addRow(tableID) 
	{
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[0].cells.length;
		for ( var i = 0; i < colCount; i++) 
		{
			var newcell = row.insertCell(i);
			var text = table.rows[1].cells[i].innerHTML;
			table.rows[rowCount].cells[0].innerHTML = rowCount;
			if (text.indexOf("[0]") > 0)
			{
				text = text.replace("[0]", "[" + (rowCount - 1) + "]");
			}
			if (text.indexOf("_0") > 0) 
			{
				text = text.replace("_0", "_" + (rowCount - 1));
			}
			newcell.innerHTML = text.replace(/CHECKED/g, '');
	
			switch (newcell.childNodes[0].type) 
			{
				case "select-one":
					newcell.childNodes[0].selectedIndex = 0;
					break;
				case "text":
					newcell.childNodes[0].value = "";
					break;
				case "checkbox":
					newcell.childNodes[0].checked = false;
					break;
			}
		}
	}	

	function deleteRow(tableID) 
	{
		try 
		{
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			
			if (rowCount > 2) 
			{
				table.deleteRow(rowCount - 1);
				$('#rowCount').val(table.rows.length-1);	
			} 
			else 
			{
				alert("All rows cannot be deleted.");
			}
		} 
		catch (e) {
			alert(e);
		}
	}

	var pattern=/[0-9]+/;
	function checkIfExists(licenseNo, id)
	{
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=checkIfExists&licenseNo='+licenseNo,
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var status = $(this).find('status').text();
					var no = id.match(pattern);

					if(status == "DOESNOT_EXISTS")
					{
						$('#msg_'+no).html("Invalid License No");
						$('#msg_'+no).show();
					}
					else
					{
						$('#msg_'+no).hide();
					}
				});
			}
		});
	}

	function formSubmit()
	{
		var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/license.html?method=request_reprint',type:'POST',data: $("#reprintForm").serialize()}; 
	    $("#reprintForm").ajaxSubmit(options);
        $('#messageDiv').show();
        setTimeout('hideStatus("messageDiv")',5000);
	}
	
</script>        				