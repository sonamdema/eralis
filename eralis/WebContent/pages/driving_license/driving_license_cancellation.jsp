<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	String pageIdentifier = (String) request.getAttribute("page_identifier");
	String pageId = (String) request.getAttribute("page_id");
%>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		License Cancellation Details
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			cancellation driving license
		</small>
	</h1>
</div><!-- /.page-header -->
<div id="Msg"></div>
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">License No:</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
											<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
											<span class="input-group-btn">
												<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
													<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-lg-3">
									  	<label style="display:none;color: #ff0000" id="licenseNoValidation">Please search a license number</label>
									  </div>
								</div>
							</div>
						</div>
						
			<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Personal Information</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Name:</label>
								</div>
								<div class="col-lg-4" id="displayOwnerName"></div>
								<div class="col-lg-2">
									<label>Gender:</label>
								</div>
								<div class="col-lg-4" id="gender"></div>
								
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Citizen ID:</label>
								</div>
								<div class="col-lg-4" id="cid"></div>
								<div class="col-lg-2">
									<label>Customer ID:</label>
								</div>
								<div class="col-lg-4" id="customerID"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Date Of Birth:</label>
								</div>
								<div class="col-lg-4" id="displayDob"></div>
								
							</div>
								
							
							<h4>Permanent Address</h4>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Dzongkhag:</label>
								</div>
								<div class="col-lg-4" id="displayDzongkhag"></div>
								<div class="col-lg-2">
									<label class="control-label">Gewog:</label>
								</div>
								<div class="col-lg-4" id="displayGewog"></div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label">Village:</label>
								</div>
								<div class="col-lg-4" ></div>
								<div class="col-lg-2">
									<label class="control-label">Country:</label>
								</div>
								<div class="col-lg-3">
									
								</div>
							</div>
							
							<div class="form-group">
								
								<div class="col-lg-2">
									<label>Address:</label>
								</div>
								<div class="col-lg-3">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title lighter">Licesne Cancellation Details</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-lg-12">
							
							
							<div class="form-group">
								<div class="col-lg-2">
									<label>Cancellation Date<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<div class="input-group">
										<html:text property="cancellationdate" styleClass="form-control date-picker" styleId="cancellationdate" readonly="true"></html:text>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
									</div>
								</div>
								<div class="col-lg-2">
									<label>Status<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:text property="status" styleId="status" value="Active" styleClass="form-control" readonly="true"></html:text>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2">
									<label>Cancellation Reason<span style="color: #ff0000">*</span>:</label>
								</div>
								<div class="col-lg-3">
									<html:textarea property="cancellationReason" styleId="cancellationReason" styleClass="form-control"></html:textarea>
								</div>
							</div>
							<div class="col-lg-2">
								<label> Document <span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-3">
								<html:file property="imgPath" styleId="invoice" styleClass="form-control fileupload" onchange="validation('learnerForm','invoiceValidation',this)"></html:file>
								<label style="display:none;color: #ff0000" id="invoiceValidation">Please attach your file here</label>
								<label style="display:none;color: #ff0000" id="invoiceTypeValidation"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="displayMsgDiv">&nbsp;</div>
		<div class="pull-right">
			<html:hidden property="customerId" styleId="customerId"/>
			<logic:equal value="Y" name="priviledge" property="isNew">
				<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Save</button>
			</logic:equal>
			<!--<logic:equal value="Y" name="priviledge" property="isEdit">
				<button class="btn btn-primary btn-sm">Update</button>
			</logic:equal>
			<logic:equal value="Y" name="priviledge" property="isDelete">
				<button class="btn btn-primary btn-sm">Delete</button>
			</logic:equal>
			
			-->
		</div>
		</html:form>
	</div>
</div>

<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="LicenseNo" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				</script>
				<script type="text/javascript">

				
				function validation(thisform,msgId,fileObj)
				{
					var fileId = fileObj.id;
					with(thisform)
					{
						if(validateFileExtension(fileObj, msgId, "pdf,word,image files are only allowed!", new Array("jpg","pdf","jpeg","gif","png","JPG","PDF","JPEG","GIF","PNG")) == false)
						{
							document.getElementById(fileId).value = "";
							return false;
						}
						if(validateFileSize(fileObj, 5242880, msgId, "Document size should be less than 5MB!") == false)
						{
							document.getElementById(fileId).value = "";
							return false;
						}
					}
				}
				
				
				function searchPersonalInfo()
				{
					var cidNumber 	= $('#cidPersonalModal').val();
					var licenseNo	= $('#licenseNoModal').val();
					
					if(cidNumber == "")
						cidNumber = "NA";
					if(licenseNo == "")
						licenseNo = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=LICENSE_CANCELLATION",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#licenseDetailList").html(responseText);
							$("#licenseDetailList").show();
						}
					});
					 
				} 
				
				$(document).ready(function()
						{
							$("#learnerForm").validate({
										rules:
										{
											cancellationdate:{
												required:true
											},
											cancellationReason:{
												required:true
											},
											status:{
												required:true
											},
											invoice:{
												required:true
											}
										},    
										messages:
										{
											cancellationdate:{required: "Please Provide Receipt Date"},
											cancellationReason:{required: "Please Provide a valid reason"},
											status:{required: "Please provide status"},
											invoice:{required: "Please provide Document"}
										}
								});

							$('#submitBtn').click(function()
							{
								var licenseNO = $('#licenseNO').val();

								if(licenseNO == "")
								{
									$('#licenseNoValidation').show();
									$('#licenseNoValidation').get(0).scrollIntoView();
									return false;
								}
								else
								{
									if($('#learnerForm').valid()) 
									{
										var invoice	=	$("#invoice").val();
										if(invoice!="")
										{
											var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_cancellation&FORM_TYPE=DL',type:'POST',data: $("#learnerForm").serialize()}; 
										    $("#learnerForm").ajaxSubmit(options);
									        $('#displayMsgDiv').show();
									        setTimeout('hideStatus("displayMsgDiv")',5000);
									        setTimeout('reloadPage()',5000);
										}
										else
										{
											$("#invoiceValidation").show();
											return false;
										}
										
									}
									else 
									{
										return false;
									}
								}
							});
						});
						var pageIdentifier = "<%=pageIdentifier%>";
					  	var pageId = "<%=pageId%>";
					</script>
					<link rel="stylesheet" media="screen" href="css/screen.css">


					<style>
						#learnerForm .error { color: red; }
					</style>