<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.license.LicenseDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO"%>
<%
   LicenseDTO dto = (LicenseDTO)request.getAttribute("LicenseDTO");
   %>
<script>
   function formSubmit()
   {
   
   	$('#approveBTN').attr('disabled', true);
   
   	$.blockUI
          ({ 
          	css: 
          	{ 
               border: 'none', 
               padding: '15px', 
               backgroundColor: '#000', 
               '-webkit-border-radius': '10px', 
               '-moz-border-radius': '10px', 
               opacity: .5, 
               color: '#fff' 
          	} 
          });
   			        
   	var application=$("#applicationNo").val();
   	
   	var options = {target:'#messageDiv',url:context+'/license.html?method=driving_license_renewal_application_approval&applicationNo='+application,type:'POST',data: $("#learnerForm").serialize()}; 
   
       $("#learnerForm").ajaxSubmit(options);
       $('#messageDiv').show();
       setTimeout('hideStatus("messageDiv")',4000);
       setTimeout('showTaskList()',2000);
   }
   
   function formReject()
   {
   	var application=$("#applicationNo").val();
   	
   	var options = {target:'#messageDiv',url:context+'/license.html?method=driving_license_renewal_application_reject&applicationNo='+application+'&regionId='+regionId,type:'POST',data: $("#learnerForm").serialize()}; 
   
       $("#learnerForm").ajaxSubmit(options);
       $('#messageDiv').show();
       setTimeout('hideStatus("messageDiv")',4000);
       setTimeout('showTaskList()',2000);
   }
   
   function dispatch()
   {
   	var application=$("#applicationNo").val();
   	var options = {target:'#messageDiv',url:context+'/common.html?method=dispatch&applicationNo='+application+'&requestType=LICENSE&serviceType=RENEWAL',type:'POST',data: $("#learnerForm").serialize()}; 
       $("#learnerForm").ajaxSubmit(options);
       $('#messageDiv').show();
       setTimeout('hideStatus("messageDiv")',4000);
       setTimeout('showTaskList()',2000);
   }
   
   var a="<%=request.getAttribute("applicationNo")%>";
   $("#applicationNo").val(a);
   
   		
</script>
<div class="page-header">
   <h1>
      <i class="ace-icon fa fa-credit-card"></i>
      Driving License
      <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      Application For License Renewal
      </small>
   </h1>
</div>
<!-- /.page-header -->
<div class="row">
   <html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
      <div class="widget-box">
         <div class="widget-header widget-header-small">
            <h5 class="widget-title lighter">License Renewal Application Details for Application No: <strong><%=request.getAttribute("applicationNo")%></strong></h5>
         </div>
         <div class="widget-body">
            <div class="widget-main">
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Driving License No:</label>
                  </div>
                  <div class="col-lg-4" ><label class="control-label"><%=dto.getLicenseNo()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Drive types:</label>
                  </div>
                  <div class="col-sm-10">
                     <label class="control-label">
                        <logic:notEmpty name="DRIVETYPE_LIST">
                           <logic:iterate id="type" name="DRIVETYPE_LIST" indexId="index" type="bt.gov.rsta.eralis.dto.license.DriveTypeDTO">
                              <bean:write name="type" property="driveType"/>
                           </logic:iterate>
                        </logic:notEmpty>
                        <logic:notEmpty name="ENDORSE_DRIVE_TYPE_LIST">
                           <logic:iterate id="type" name="ENDORSE_DRIVE_TYPE_LIST" indexId="index" type="bt.gov.rsta.eralis.dto.license.DriveTypeDTO">
                              <bean:write name="type" property="driveType"/>
                           </logic:iterate>
                        </logic:notEmpty>
                     </label>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Name:</label>
                  </div>
                  <div class="col-lg-4" ><label class="control-label"><%=dto.getName()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">CID No.:</label>
                  </div>
                  <div class="col-lg-4" ><label class="control-label"><%=dto.getCID()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Date of Birth:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getDOB()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Gender:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getGender() %></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Occupation:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getOccupation()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Blood Group:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getBloodgroup()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Dzongkhag:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getDzongkhag()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Gewog:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getGewog()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Village:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getVillage()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Region:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getRegion()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Initial Issue Date:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getIssuedate()%></label></div>
               </div>
            </div>
         </div>
         <div class="widget-header widget-header-small">
            <h5 class="widget-title lighter">Renewal Details</h5>
         </div>
         <div class="widget-body">
            <div class="widget-main">
              <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Expiry Date:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getExpiryDate()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Next Expiry Date</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getNextExpiry()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Renewal Duration</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getRenewalDuration()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Remarks:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getRemarks()%></label></div>
               </div>
            </div>
         </div>
         
         <div class="widget-header widget-header-small">
            <h5 class="widget-title lighter">Payment Details</h5>
         </div>
         <div class="widget-body">
            <div class="widget-main">
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Amount:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getAmount()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Penalty:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=dto.getPenalty()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Receipt Number:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptNo()%></label></div>
                  <div class="col-lg-2">
                     <label class="control-label">Receipt Date:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getReceiptDate()%></label></div>
               </div>
               <div class="form-group">
                  <div class="col-lg-2">
                     <label class="control-label">Total Amount:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label">Nu.&nbsp;<%=Double.parseDouble(dto.getAmount())+Double.parseDouble(dto.getPenalty())%></label></div>
                  <div class="col-lg-2">
                     <label>Application Submission Date:</label>
                  </div>
                  <div class="col-lg-4"><label class="control-label"><%=dto.getAppsubmissiondate()%></label></div>
               </div>
               <logic:equal value="APPROVE" name="param">
                  <div class="form-group">
                     <jsp:include page="/pages/common/uploadedFiles.jsp"></jsp:include>
                  </div>
               </logic:equal>
            </div>
         </div>
      </div>
      <html:hidden property="nextExpiry" value="<%=dto.getNextExpiry()%>"></html:hidden>
   </html:form>
   <jsp:include page="/pages/common/rejectionForm.jsp"></jsp:include>
</div>
<input type="hidden" id="applicationNo"/>	
<div id="messageDiv"></div>
<div>
   <logic:equal value="APPROVE" name="param">
      <button type="button" class="btn btn-sm" id="approveBTN" onclick="formSubmit()">
      <i class="ace-icon fa fa-check"  ></i>
      Approve
      </button>
      <button type="button" class="btn btn-sm" onclick="openModal('rejectionModalForm')">
      <i class="ace-icon fa fa-times red2"></i>
      Reject
      </button>
   </logic:equal>
   <logic:equal value="DISPATCH" name="param">
      <button type="button" class="btn btn-sm" onclick="dispatch()">
      <i class="ace-icon fa fa-check"  ></i>
      Dispatch
      </button>
   </logic:equal>
</div>