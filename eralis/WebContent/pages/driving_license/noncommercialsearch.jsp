<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
	<table id="noncommercial-search-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr> 
				<th></th>
				<th>CID</th> 
				<th>Name</th>
				<th>Learner License Number</th> 
				<th>Region</th> 
				<th>Dzongkhag</th> 
				<th>Issue Date</th>
				<th>Expiry Date</th> 
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="LICENSE_LIST">
				<logic:iterate id="license" name="LICENSE_LIST">
					<tr> 
						<td>
							<!-- <input type="radio" name="customerIdRadio" id='<bean:write name="license" property="customerId"/>_<bean:write name="license" property="learnerlicenseinfoid"/>' onclick="addSelected(this.id)"/> -->
							<button type="button" data-dismiss="modal" class="btn btn-minier btn-primary" id='<bean:write name="license" property="learnerlicenseinfoid"/>' onclick="getSelected(this.id)">SELECT</button>
						</td>
						<td><bean:write name="license" property="CID"/></td>
						<td><bean:write name="license" property="name"/></td>
						<td><bean:write name="license" property="LLNo"/></td>
						<td><bean:write name="license" property="region"/></td>
						<td><bean:write name="license" property="dzongkhag"/></td>
						<td><bean:write name="license" property="issuedate"/></td>
						<td><bean:write name="license" property="expiryDate"/></td>
						
						 
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>
<div>
	<input type="hidden" name="column_name" id="column_name">
	<button class="btn btn-sm" data-dismiss="modal" onclick="getSelected()">
		<i class="ace-icon fa fa-check"></i>
		Select
	</button>
</div>

<script>
	$(document).ready(function() 
	{
   		$('#noncommercial-search-table').DataTable({
            responsive: true
    	});
	});

	<%
	String searchType = (String)request.getAttribute("SEARCH_TYPE");
	%>
	var searchType = "<%=searchType%>";
																													
	function getSelected(learnerlicenseinfoid)
	{	 
		$.ajax
		({
			async: true,
			type: 'POST',
			url: '<%=request.getContextPath()%>/EralisCommonServlet?q=getlearnerDetails&searchType='+searchType+'&learnerlicenseinfoid='+learnerlicenseinfoid,
			success: function(xml)
			{
				$(xml).find('xml-response').each(function()
				{
					var name = $(this).find('name').text();
					var dzongkhag = $(this).find('dzongkhag').text();
					var age = $(this).find('age').text();
					var gewog = $(this).find('gewog').text();
					var address = $(this).find('address').text();
					var titleOfCourtesy = $(this).find('courtesy').text();
					var occupation = $(this).find('occupation').text();
					var customerId = $(this).find('customerId').text();
					var bloodgroup = $(this).find('bloodgroup').text();
					var bloodgroupId = $(this).find('bloodgroupId').text();
					var dob = $(this).find('dob').text();
					var nationality=$(this).find('nationality').text();
					var CID=$(this).find('CID').text();
					var fathersname=$(this).find('fathersname').text();
					var identificationMarks=$(this).find('identificationMarks').text();
					var remarks=$(this).find('remarks').text();
					var gender = $(this).find('gender').text();
					var country = $(this).find('country').text();
					var personalInfoId = $(this).find('personalInfoId').text();
					var village = $(this).find('village').text();
					var licenseNO = $(this).find('licenseNO').text();
					var region=$(this).find('region').text();
					var learnerlicenseno=$(this).find('learnerlicenseno').text();
					var validity=$(this).find('validity').text();
					var testmarks	=	$(this).find('testmarks').text();
					var learnerLicenseId	=	$(this).find('learnerLicenseId').text();
					var isInternational = $(this).find('isInternational').text();
					var status=$(this).find('status').text();
					var driveTypeName=$(this).find('driveTypeName').text();
					var reason=$(this).find('reason').text();

					var driveTypeId=$(this).find('driveTypeId').text();
					var Theory_Marks_Obtained = $(this).find('Theory_Marks_Obtained').text();
					var Practical_Marks_Obtained = $(this).find('Practical_Marks_Obtained').text();
					var Issue_Type = $(this).find('Issue_Type').text();
					
					$("#driveTypeName").val('');
					$("#driveTypeId").val('');
					$("#testResult").val('');
					
					$('#displayOwnerName').html("<label class='control-label'>"+''+"</label>");
					$('#displayDzongkhag').html("<label class='control-label'>"+''+"</label>");
					$('#displayGewog').html("<label class='control-label'>"+''+"</label>");
					$('#displayAddress').html("<label class='control-label'>"+''+"</label>");
					$('#citizenID').val('');
					$('#region').val('');
					$('#learnerLicenseId').val('');
					$('#learnerCustomerId').val('');
					$('#titleOfcourtesy').val('');
					$('#name').val('');
					$('#occupation').val('');
					$('#nationality').val('');
					$('#cid').val('');
					$('#bloodGroup').val('');
					$('#displayDob').val('');
					$('#fathersName').val('');
					$('#identificationMarks').val('');
					$('#remarks').val('');
					$('#licenseNO').val('');
					$('#learnerlicenseno').val('');
					 
					
					$('#displayBloodgroup').html("<label class='control-label'>"+''+"</label>");
					$('#displayDob').html("<label class='control-label'>"+''+"</label>");
					
					$('#customerID').html("<label class='control-label'>"+''+"</label>");
					$('#occupation').html("<label class='control-label'>"+''+"</label>");
					$('#identificationMarks').html("<label class='control-label'>"+''+"</label>");
					$('#gender').html("<label class='control-label'>"+''+"</label>");
					$('#nationality').html("<label class='control-label'>"+''+"</label>");
					$('#bloodgroup').html("<label class='control-label'>"+''+"</label>");
					$('#fathersname').html("<label class='control-label'>"+''+"</label>");
					$('#displayCountry').html("<label class='control-label'>"+''+"</label>");
					
					$('#cid').html("<label class='control-label'>"+''+"</label>");
					$("#duplicateCID").val('');
					$("#personalInfoId").val(''); 
					$('#village').html("<label class='control-label'>"+''+"</label>");
					$('#testmarks').val('');

					$("#issueType").val(Issue_Type);
					$("#driveTypeTemp").val(driveTypeId);
					$("#testmarks").val(testmarks);
					$("#testResult").val(status);
					

					$('#driveTypeTemp').children('option').hide();
					$('#driveTypeTemp value:contains('+driveTypeId+')').show();

					$('#issueType').children('option').hide();
					$('#issueType value:contains('+Issue_Type+')').show();
					
					
					
					
					if(searchType == "ISSUE_NON_COMMERCIAL_LICENSE")
					{
						if(status == "ISSUED")
						{
							$("#Msg").html("<div class='alert alert-danger'>License is already issued or already applied against this Customer</div>");
							$('#Msg').show();
					       // setTimeout('hideStatus("Msg")',5000);
						}
						else if(status=="FAILED")
						{
							$("#Msg").html("<div class='alert alert-danger'>Sorry, Applicant did not attend or pass the drive test</div>");
							$('#Msg').show();
						}
						else
						{$('#Msg').hide();
							if (Number(age) <17)
							{
								$('#validationMsg').html("<div class='alert alert-danger'>Sorry, Applicant is underage to apply for Driving license</div>");
								$('#validationMsg').show();
						        setTimeout('hideStatus("validationMsg")',7000);
							}
							else
							{
								//validateNonCommercialCriteria(driveTypeId);
								
								$("#driveTypeName").val(driveTypeName);
								$("#driveTypeId").val(driveTypeId);
								//$("#testResult").val('PASSED');
								$("#testMarks").val('testMarks');
								$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
								$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
								$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
								$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
								$('#citizenID').val(CID);
								$('#region').val(region);
								$('#learnerLicenseId').val(learnerLicenseId);
								$('#learnerCustomerId').val(customerId);
								$('#titleOfcourtesy').val(titleOfCourtesy);
								$('#name').val(name);
								$('#occupation').val(occupation);
								$('#nationality').val(nationality);
								$('#cid').val(CID);
								$('#bloodGroup').val(bloodgroupId);
								$('#displayDob').val(dob);
								$('#fathersName').val(fathersname);
								$('#identificationMarks').val(identificationMarks);
								$('#remarks').val(remarks);
								$('#licenseNO').val(licenseNO);
								$('#learnerlicenseno').val(learnerlicenseno);
								if(gender == "M")
									$('#maleRadio').attr('checked', true);
								else if(gender == "F")
									$('#femaleRadio').attr('checked', true);
								
								
								$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
								$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
								
								$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
								$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
								$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
								$('#gender').html("<label class='control-label'>"+gender+"</label>");
								$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
								$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
								$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
								$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
								
								$('#cid').html("<label class='control-label'>"+CID+"</label>");
								$("#duplicateCID").val(CID);
								$("#personalInfoId").val(personalInfoId); 
								$('#village').html("<label class='control-label'>"+village+"</label>");
								//$('#testmarks').val(testmarks);
		
								if(isInternational == "N")
								{
									$('#nationalDIV').show();
									$('#internationalDIV').hide();
								}
								else
								{
									$('#internationalDIV').show();
									$('#nationalDIV').hide();
								}
							}
						}
					}
					else
					{
						if (Number(age) <17)
						{
							$('#validationMsg').html("<div class='alert alert-danger'>Sorry, Applicant is underage to apply for Driving license</div>");
							$('#validationMsg').show();
					        setTimeout('hideStatus("validationMsg")',7000);
						}
						else
						{
							$('#displayOwnerName').html("<label class='control-label'>"+name+"</label>");
							$('#displayDzongkhag').html("<label class='control-label'>"+dzongkhag+"</label>");
							$('#displayGewog').html("<label class='control-label'>"+gewog+"</label>");
							$('#displayAddress').html("<label class='control-label'>"+address+"</label>");
							$('#citizenID').val(CID);
							$('#region').val(region);
							$('#learnerLicenseId').val(learnerLicenseId);
							$('#learnerCustomerId').val(customerId);
							$('#titleOfcourtesy').val(titleOfCourtesy);
							$('#name').val(name);
							$('#occupation').val(occupation);
							$('#nationality').val(nationality);
							$('#cid').val(CID);
							$('#bloodGroup').val(bloodgroupId);
							$('#displayDob').val(dob);
							$('#fathersName').val(fathersname);
							$('#identificationMarks').val(identificationMarks);
							$('#remarks').val(remarks);
							$('#licenseNO').val(licenseNO);
							$('#learnerlicenseno').val(learnerlicenseno);
							if(gender == "M")
								$('#maleRadio').attr('checked', true);
							else if(gender == "F")
								$('#femaleRadio').attr('checked', true);
							
							
							$('#displayBloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#displayDob').html("<label class='control-label'>"+dob+"</label>");
							
							$('#customerID').html("<label class='control-label'>"+customerId+"</label>");
							$('#occupation').html("<label class='control-label'>"+occupation+"</label>");
							$('#identificationMarks').html("<label class='control-label'>"+identificationMarks+"</label>");
							$('#gender').html("<label class='control-label'>"+gender+"</label>");
							$('#nationality').html("<label class='control-label'>"+nationality+"</label>");
							$('#bloodgroup').html("<label class='control-label'>"+bloodgroup+"</label>");
							$('#fathersname').html("<label class='control-label'>"+fathersname+"</label>");
							$('#displayCountry').html("<label class='control-label'>"+country+"</label>");
							
							$('#cid').html("<label class='control-label'>"+CID+"</label>");
							$("#duplicateCID").val(CID);
							$("#personalInfoId").val(personalInfoId); 
							$('#village').html("<label class='control-label'>"+village+"</label>");
							$('#testmarks').val(testmarks);
	
							if(isInternational == "N")
							{
								$('#nationalDIV').show();
								$('#internationalDIV').hide();
							}
							else
							{
								$('#internationalDIV').show();
								$('#nationalDIV').hide();
							}
						}
					}
					if(status == "PENDING_APPLICATION")
					{
						$("#submitBtn").prop('disabled', true);
						$("#Msg").html("<div class='alert alert-danger'><h4>This CID details is in pending application, Find the remarks below :</h4>"+reason+"</div>");
						$('#Msg').show();
					}
				});
			}
		});
		
		
	}
</script>
 