<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="form-group">
	<div id="REGION_WISE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Region</label>
		<div class="col-lg-3">
			<select id="region" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="region" name="regionList">
					<option value='<bean:write name="region" property="headerId"/>'>
						<bean:write name="region" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
	<div id="COMPANY_WISE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Company</label>
		<div class="col-lg-3">
			<select id="company" class="form-control" onchange="populateDependentDropDown(this.value, 'model', '', 'VECHILE_MODEL_LIST', 'N')">
				<option value="ALL">All</option>
				<logic:iterate id="company" name="vehicleCompanyList">
					<option value='<bean:write name="company" property="headerId"/>'>
						<bean:write name="company" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
	<div id="OWNERTYPE_WISE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Owner Type</label>
		<div class="col-lg-3">
			<select id="ownerType" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="owner" name="ownerTypeList">
					<option value='<bean:write name="owner" property="headerId"/>'>
						<bean:write name="owner" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
	<div id="VEHICLETYPE_WISE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Vehicle Type</label>
		<div class="col-lg-3">
			<select id="vehicleType" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="vehicleType" name="vehicleTypeList">
					<option value='<bean:write name="vehicleType" property="headerId"/>'>
						<bean:write name="vehicleType" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
	<div id="ENGINETYPE_WISE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Engine Type</label>
		<div class="col-lg-3">
			<select id="engineType" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="engineType" name="engineTypeList">
					<option value='<bean:write name="engineType" property="headerId"/>'>
						<bean:write name="engineType" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
	<div id="DEALER_WISE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Dealer</label>
		<div class="col-lg-3">
			<select id="dealer" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="dealer" name="dealersNameList">
					<option value='<bean:write name="dealer" property="headerId"/>'>
						<bean:write name="dealer" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
	<div class="form-group" id="BASE_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Base</label>
		<div class="col-lg-3">
			<select id="base" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="base" name="baseOfficeList">
						<option value='<bean:write name="base" property="headerId"/>'>
							<bean:write name="base" property="headerName"/>
						</option>
					</logic:iterate>
			</select>
		</div>
	</div>
	<div class="form-group" id="MODEL_DIV" style="display:none;">
		<label class="col-lg-2 control-label no-padding-right">Model</label>
		<div class="col-lg-3">
			<select id="model" class="form-control">
				<option value="ALL">All</option>
				<logic:iterate id="model" name="vehicleModelList">
					<option value='<bean:write name="model" property="headerId"/>'>
						<bean:write name="model" property="headerName"/>
					</option>
				</logic:iterate>
			</select>
		</div>
	</div>
</div>