<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.report.ReportDTO"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />
<%
	ReportDTO dto = (ReportDTO) request.getAttribute("DTO");
%>
<div class="row">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-header">
				<h4 class="widget-title">Report Filter</h4>
			</div>
		</div>
		<div class="widget-box">
			<div class="widget-body">
				<div class="widget-main">
					<div>
						<h5>Report Description : <%=dto.getDescription() %></h5>
					</div>
					<div id="validation_message" class="col-lg-12 ">
    					<div class="alert alert-danger">Please fill all the fields</div>
    					<br>
    				</div>
					<div id="parameter">
						
					</div>
					<div class="form-group">
						<div class="col-lg-12">
	    					<div class="input-group">
	    						<button onClick="report_validation()" type='button'class="btn btn-primary btn-sm">
									Generate Report
								</button>
								
	    						<button id="exportButton" onClick ="$('#dynamic-table').tableExport({type:'excel',escape:'false'});" type='button'class="btn btn-primary btn-sm">
									Export Report
								</button>
								<input type="hidden" id="paramLength">
							</div>
	    				</div>
	    			</div>
	    			
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script> 
	var whereParam		=	'<%=dto.getWhereParam() %>';
	var parameter		=	whereParam.split('~');
	var parameterTitle	=	'';
	var paramMasterDtls	=	'';	
	$("#paramLength").val(parameter.length);
	var parentId	=	'';
	for (var i = 0; i < parameter.length;) 
	{
		paramMasterDtls	=	parameter[i].split('#');
		paramMasterTable	=	paramMasterDtls[1];
		paramMasterColId	=	paramMasterDtls[2];
		paramMasterColVal	=	paramMasterDtls[3];
		parentId	=	paramMasterTable+"~"+paramMasterColId+"~"+paramMasterColVal;
		parameterTitle		=	parameter[i].split(' ');
		var parameterDtls		=	parameterTitle[0].split('.');
		i++;
		var operatorType	=	parameterTitle[1];
		if(paramMasterTable=='none')
		{
			if(parameterDtls[2]=='DATE')
			{
				if(operatorType=='between')
				{
					$("#parameter").append("<div class='form-group'><label class='col-lg-2 control-label no-padding-right'>"+parameterDtls[1]+":</label><div class='col-lg-10'><div class='col-lg-9'><div class='col-lg-1 control-label no-padding-right'>From</div><div class='col-lg-4'><input type='text' id="+i+'a'+" class='form-control date-picker'></div> <div class='col-lg-1 control-label no-padding-right'>To</div> <div  class='col-lg-4'><input type='text' id="+i+'b'+" class='form-control date-picker'></div></div></div></div>");
				}
				else
				{
					$("#parameter").append("<div class='form-group'><label class='col-lg-2 control-label no-padding-right'>"+parameterDtls[1]+":</label><div class='col-lg-5'><div class='col-lg-9'><input type='text' id="+i+" class='form-control date-picker'></div></div></div>");
				}
			}
			else
			{
				$("#parameter").append("<div class='form-group'><label class='col-lg-2 control-label no-padding-right'>"+parameterDtls[1]+":</label><div class='col-lg-5'><div class='col-lg-9'><input type='text' id="+i+" class='col-lg-12'></div></div></div>");
			}
			
		}
		else
		{
			$("#parameter").append("<div class='form-group'><label class='col-lg-2 control-label no-padding-right'>"+parameterDtls[1]+":</label><div class='col-lg-5'><div class='col-lg-9'><select class='form-control' id="+i+" ><option value=''>--SELECT--</option></select></div></div></div>");
			populateDependentDropDown(parentId, i, '', 'REPORT_MASTER_TABLE', 'N');
		}

	}

	
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
		

</script>