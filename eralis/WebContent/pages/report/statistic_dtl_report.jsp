<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<% String reportType = (String) request.getAttribute("REPORT_TYPE");
System.out.println(reportType);

%>
<div class="table-responsive">
	<table id="statistic-task-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<%if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED") || reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED"))
				{
				%>
				<th>CID</th> 
				<th>Customer Name</th> 
				<th>Phone No</th> 
				<th>Contact Address</th> 
				<th>Vehicle Number</th> 
				<th>Vehicle Type</th> 
				<th>Vehicle Model</th> 
				<th>Vehicle Company</th> 
				<th>Engine CC</th> 
				<th>Engine Number</th> 
				<%	if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED")){ %>
						<th>Chassis Number</th> 
						<th>Engine Name</th> 
						<th>Country Name</th> 
						<th>Dealer Name</th> 
						<th>Price</th> 
						<th>Colour</th> 
						<th>Registration Type</th> 
						<th>Remarks</th>
						<th>Registration Date</th> 
						<th>Base Office</th> 
						<th>Region Office</th> 
				 
				<%	}
					if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED")){
						
				%>
						<th>Renewal Date</th> 
						<th>Expiry Date</th> 
						<th>Receipt No</th> 
						<th>Receipt Date</th> <!-- 
						<th>Renewal Duration</th>  
						<th>Colour</th> -->
						<th>Base Office</th> 
						<th>Region</th>  
				<%
					}
				}
				else if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_ISSUED"))
				{
				%>
					<th>CID</th> 
					<th>Name</th> 
					<th>DOB</th> 
					<th>Learner License No</th> 
					<th>Driving License No</th> 
					<th>Drive Type</th> 
					<th>Date Of Issue</th> 
					<th>Date Of Expiry</th> 
					<th>Base Office</th> 
					<th>Region</th> 
				
				<%
				}
				else if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_RENEWED"))
				{
				%>
					<th>CID</th> 
					<th>Name</th> 
					<th>Driving License No</th> 
					<th>Renewal Date</th> 
					<th>Expiry Date</th> 
					<th>Receipt No</th> 
					<th>Receipt Date</th> 
					<th>Base Office</th> 
					<th>Region</th> 
					<th>Remarks</th> 
				
				<%
				}
				else if(reportType.equalsIgnoreCase("STATS_TOTAL_LL_ISSUED"))
				{
				%>
					<th>CID</th> 
					<th>Name</th> 
					<th>Learner License No</th> 
					<th>Date Of Issue</th> 
					<th>Expiry Date</th> 
					<th>Receipt No</th> 
					<th>Receipt Date</th> 
					<th>Base Office</th> 
					<th>Region</th> 
					<th>Remarks</th> 
				
				<%
				}
				else if(reportType.equalsIgnoreCase("STATS_TOTAL_DL_PRINTED"))
				{
				%>
					<th>CID</th> 
					<th>Name</th> 
					<th>DL/TOP No.</th> 
					<th>Service</th> 
				<%
				}
				%>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="STATISTIC_DTL_REPORT">
				<logic:iterate id="dtlReport" name="STATISTIC_DTL_REPORT">
					<tr>
						<%
						if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED") || reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED"))
						{
							
						%>
							<td><bean:write name="dtlReport" property="CID"/></td>
							<td><bean:write name="dtlReport" property="name"/></td>
							<td><bean:write name="dtlReport" property="phone"/></td>
							<td><bean:write name="dtlReport" property="address"/></td>
							<td><bean:write name="dtlReport" property="vehicleNo"/></td>
							<td><bean:write name="dtlReport" property="vehicleType"/></td>
							<td><bean:write name="dtlReport" property="model"/></td>
							<td><bean:write name="dtlReport" property="company"/></td>
							<td><bean:write name="dtlReport" property="engineCC"/></td>
							<td><bean:write name="dtlReport" property="engineNumber"/></td>
						<%
							if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_REGISTERED"))
							{
						%>
							<td><bean:write name="dtlReport" property="chasisNumber"/></td>
							<td><bean:write name="dtlReport" property="engineType"/></td>
							<td><bean:write name="dtlReport" property="country"/></td>
							<td><bean:write name="dtlReport" property="dealerName"/></td>
							<td><bean:write name="dtlReport" property="price"/></td>
							<td><bean:write name="dtlReport" property="color"/></td>
							<td><bean:write name="dtlReport" property="type"/></td>
							<td><bean:write name="dtlReport" property="remarks"/></td>
							<td><bean:write name="dtlReport" property="registrationDate"/></td>
							<td><bean:write name="dtlReport" property="baseoffice"/></td>
							<td><bean:write name="dtlReport" property="region"/></td>
							
						<%	} 
							if(reportType.equalsIgnoreCase("STATS_TOTAL_VEH_RENEWED")){
						%>	
							<td><bean:write name="dtlReport" property="renewalDate"/></td>
							<td><bean:write name="dtlReport" property="expiryDate"/></td>
							<td><bean:write name="dtlReport" property="receiptNo"/></td>
							<td><bean:write name="dtlReport" property="receiptDate"/></td><%-- 
							<td><bean:write name="dtlReport" property="totalDuration"/></td> --%>
							<td><bean:write name="dtlReport" property="baseoffice"/></td>
							<td><bean:write name="dtlReport" property="region"/></td>
							
						<%
							}
						}
						else if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_ISSUED"))
						{
						%>
							<td><bean:write name="dtlReport" property="CID"/></td>
							<td><bean:write name="dtlReport" property="name"/></td>
							<td><bean:write name="dtlReport" property="DOB"/></td>
							<td><bean:write name="dtlReport" property="learnerLicenseNo"/></td>
							<td><bean:write name="dtlReport" property="licenseNo"/></td>
							<td><bean:write name="dtlReport" property="drivetype"/></td>
							<td><bean:write name="dtlReport" property="dateOfissue"/></td>
							<td><bean:write name="dtlReport" property="expiryDate"/></td>
							<td><bean:write name="dtlReport" property="baseoffice"/></td>
							<td><bean:write name="dtlReport" property="region"/></td>
						<%
						}
						else if(reportType.equalsIgnoreCase("STATS_TOTAL_LIC_RENEWED"))
						{
							%>
								<td><bean:write name="dtlReport" property="CID"/></td>
								<td><bean:write name="dtlReport" property="name"/></td>
								<td><bean:write name="dtlReport" property="licenseNo"/></td>
								<td><bean:write name="dtlReport" property="renewalDate"/></td>
								<td><bean:write name="dtlReport" property="expiryDate"/></td>
								<td><bean:write name="dtlReport" property="receiptNo"/></td>
								<td><bean:write name="dtlReport" property="receiptDate"/></td>
								<td><bean:write name="dtlReport" property="baseoffice"/></td>
								<td><bean:write name="dtlReport" property="region"/></td>
								<td><bean:write name="dtlReport" property="remarks"/></td>
							<%
						}
						else if(reportType.equalsIgnoreCase("STATS_TOTAL_LL_ISSUED"))
						{
							%>
								<td><bean:write name="dtlReport" property="CID"/></td>
								<td><bean:write name="dtlReport" property="name"/></td>
								<td><bean:write name="dtlReport" property="learnerLicenseNo"/></td>
								<td><bean:write name="dtlReport" property="dateOfissue"/></td>
								<td><bean:write name="dtlReport" property="expiryDate"/></td>
								<td><bean:write name="dtlReport" property="receiptNo"/></td>
								<td><bean:write name="dtlReport" property="receiptDate"/></td>
								<td><bean:write name="dtlReport" property="baseoffice"/></td>
								<td><bean:write name="dtlReport" property="region"/></td>
								<td><bean:write name="dtlReport" property="remarks"/></td>
							<%
						}
						else if(reportType.equalsIgnoreCase("STATS_TOTAL_DL_PRINTED"))
						{
							%>
								<td><bean:write name="dtlReport" property="CID"/></td>
								<td><bean:write name="dtlReport" property="name"/></td>
								<td><bean:write name="dtlReport" property="licenseNo"/></td>
								<td><bean:write name="dtlReport" property="serviceType"/></td>
							<%
						}
						%>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</div>

<script>

	$(document).ready(function() 
	{
	    $('#statistic-task-table').DataTable({
	            responsive: true
	    });
	});

</script>