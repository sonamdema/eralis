<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="bt.gov.rsta.eralis.dto.report.ReportDTO"%>
<%
	String[][] arrayList = (String[][]) request.getAttribute("REPORT_LIST");
	ReportDTO dto = (ReportDTO) request.getAttribute("DTO");
%>
<div class="row">
	<div class="widget-box">
		<div class="widget-header">
			<h4 class="widget-title">Report</h4>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="tabbable">
			<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
				<li class="active">
					<a data-toggle="tab" href="#pie">Pie Chart</a>
				</li>
				<li>
					<a data-toggle="tab" href="#area">Area Chart</a>
				</li>
				<li>
					<a data-toggle="tab" href="#line">Line Chart</a>
				</li>
				<li>
					<a data-toggle="tab" href="#histogram">Histogram</a>
				</li>
			</ul>

			<div class="tab-content">
				<!--<div id="pie" class="tab-pane in active">
				<% int totalCols	=	dto.getTotalCol();
					for(int j=1;j<totalCols;j++)
					{
						int a = 0;
				%>
					<h3 id='toppic_<%=j%>'></h3>
					<table class="highchart" style="display:none;" data-graph-container-before="1" data-graph-type="pie" data-graph-datalabels-enabled="1">
					<caption><%=dto.getDescription() %></caption>
					<thead>
							<tr>
								<logic:iterate id="report" name="REPORT_HEADER" > 
								<%
									if(a==0)
									{
								%>
										<th><bean:write name="report" property='headerCol'/></th>
								<%	}
									if(a==j)
									{
								%>
									<th><bean:write name="report" property='headerCol'/></th>
									<script>

									$(document).ready(function() 
									{
										var	toppic	=	'<%=j%>';
										var	vale	=	'<bean:write name="report" property="headerCol"/>';
									   $('#toppic_'+toppic).html(vale);
									});
									</script>
								<% }a++;%>
							</logic:iterate>
								 
							</tr>
							
						</thead>
						<tbody>
							
							<%
								int totalRow	=	arrayList.length;
								if(totalRow>0)
								{
									for(int i=0;i<totalRow;i++)
									{
							%>
								<tr>
									<td><%=arrayList[i][0]%></td>
									<td><%=arrayList[i][j]%></td>
								</tr>
							<%		}
								}
							%>
						</tbody>
					</table>
					<% }%>
				
				</div>

				--><div id="area" class="tab-pane">
					<div class="table-responsive">
						<table class="highchart" style="display: none;" data-graph-container-before="1" data-graph-type="area" data-graph-line-shadow="0">
						<caption><%=dto.getDescription() %></caption>
						<thead>
							<tr>
							<logic:iterate id="report" name="REPORT_HEADER" indexId="index"> 
								<%
									int a = index.intValue();
								%>
								<th><bean:write name="report" property='headerCol'/></th>
							</logic:iterate>
							</tr>
							
						</thead>
						<tbody>
							<%
								int totalRow	=	arrayList.length;
								if(totalRow>0)
								{
									int totalCol	=  arrayList[1].length;
									for(int i=0;i<totalRow;i++)
									{
							%>
								<tr>
								<%		for(int j=0;j<totalCol;j++)
										{
							%>
										<td><%=arrayList[i][j]%></td>
								<% }%>
								</tr>
							<%	}
							}
							%>
						</tbody>
					</table>
					</div>
				</div>

				<div id="line" class="tab-pane">
					<div class="table-responsive">
						<table class="highchart" data-graph-container-before="1" data-graph-type="line" data-graph-datalabels-enabled="1" data-graph-datalabels-align="right" style="display:none;">
						<caption><%=dto.getDescription() %></caption>
							<thead>
								<tr>
								<logic:iterate id="report" name="REPORT_HEADER" indexId="index"> 
									<%
										int a = index.intValue();
									%>
									<th><bean:write name="report" property='headerCol'/></th>
								</logic:iterate>
								</tr>
								
							</thead>
							<tbody>
								<%
									totalRow	=	arrayList.length;
									if(totalRow>0)
									{
										int totalCol	=  arrayList[1].length;
										for(int i=0;i<totalRow;i++)
										{
								%>
									<tr>
									<%		for(int j=0;j<totalCol;j++)
											{
								%>
											<td><%=arrayList[i][j]%></td>
									<% }%>
									</tr>
								<%	}
								}
								%>
							</tbody>
						</table>
					</div>
				</div>
				
				<div id="histogram" class="tab-pane">
					<table class="highchart" data-graph-container-before="1" data-graph-type="column" style="display:none;">
						<caption><%=request.getAttribute("REPORT_NAME") %> for <%=request.getAttribute("START_DATE")%> to <%=request.getAttribute("END_DATE") %></caption>
							<thead>
								<tr>
								<logic:iterate id="report" name="REPORT_HEADER" indexId="index"> 
									<%
										int a = index.intValue();
									%>
									<th><bean:write name="report" property='headerCol'/></th>
								</logic:iterate>
								</tr>
								
							</thead>
							<tbody>
								<%
									totalRow	=	arrayList.length;
									if(totalRow>0)
									{
										int totalCol	=  arrayList[1].length;
										for(int i=0;i<totalRow;i++)
										{
								%>
									<tr>
									<%		for(int j=0;j<totalCol;j++)
											{
								%>
											<td><%=arrayList[i][j]%></td>
									<% }%>
									</tr>
								<%	}
								}
								%>
							</tbody>
						</table>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="table-responsive">
		<div class="clearfix">
		
		
			
			<!--<a href="#" 
			onClick="doExport('#dynamic-table',{type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});">
          <img src='<%=request.getContextPath() %>/images/pdf.png' alt="PDF" style="width:24px"> PDF</a>
			-->
			
			<!--<div class="pull-right tableTools-container col-lg-1">
			 <a href="#" class="list-group-item" onClick ="$('#dynamic-table').tableExport({type:'pdf',escape:'false',jspdf: {autotable: {tableWidth: 'wrap'}}});">
				<a href="#" class="list-group-item" onClick="('#dynamic-table',{type: 'pdf',jspdf: {autotable: {tableWidth: 'wrap'}}});">
        			<img src='<%=request.getContextPath() %>/images/pdf.png' alt="PDF" style="width:24px"> PDF</a>
			</div>-->
		</div>
		<table id="dynamic-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
				<logic:iterate id="report" name="REPORT_HEADER" indexId="index"> 
					<%
						int a = index.intValue();
					%>
					<th><bean:write name="report" property='headerCol'/></th>
				</logic:iterate>
				</tr>
				
			</thead>
			<tbody>
				<%
					totalRow	=	arrayList.length;
					if(totalRow>0)
					{
						int totalCol	=  arrayList[1].length;
						for(int i=0;i<totalRow;i++)
						{
				%>
					<tr>
					<%		for(int j=0;j<totalCol;j++)
							{
				%>
							<td><%=arrayList[i][j]%></td>
					<% }%>
					</tr>
				<%	}
				}
				%>
			</tbody>
		</table>
	</div>
</div>
 <script>

	$(document).ready(function() 
	{
	    $('#dynamic-table').DataTable({
	         responsive: true,
	         aLengthMenu: [[10,25, 50, 100, -1],
				           [10,25, 50, 100, "All"]],
             iDisplayLength: -1
	    });

	    $('table.highchart').highchartTable();
	  	//$('select[name=dynamic-table_length]').append($("<option></option>").attr("value","-1").text("All")); 
	    
	    
	  
	    
	});
	
	function doExport(selector, params) {
		   var options = {
		     //ignoreRow: [1,11,12,-2],
		     //ignoreColumn: [0,-1],
		     //pdfmake: {enabled: true},
		     tableName: 'example',
		     worksheetName: 'Countries by population'
		   };

		   $.extend(true, options, params);

		   $(selector).tableExport(options);
		 }

</script>