<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			Role Management
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; configure user roles &amp; their page permissions
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title"></h4>
							<logic:equal value="Y" name="priviledge" property="isNew">
								<span class="widget-toolbar">
									<a href="#" title="Add Role" onclick="openModal('add-role-modal')">
										<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
									</a>
								</span>
							</logic:equal>
						</div>
						<div class="widget-body">
							<div id="messageDiv" style="display:none"></div>
							<div class="widget-main">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Role</th>
											<th>Status</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="roleList">
											<logic:iterate id="role" name="roleList" type="bt.gov.rsta.eralis.dto.administration.RoleDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="role" property="roleName"/>
													</td>
													<logic:equal value="Y" name="role" property="roleStatus">
														<td>
															<span class="label label-sm label-success">Active</span>
														</td>
													</logic:equal>
													<logic:equal value="N" name="role" property="roleStatus">
														<td>
															<span class="label label-sm label-warning">Inactive</span>
														</td>
													</logic:equal>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="editPermissionDtls('<bean:write name="role" property="roleId"/>','<bean:write name="role" property="roleName"/>','<bean:write name="role" property="roleStatus"/>')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="role" property="roleStatus">
																<a class="red" href="#" onclick="deactivateRole('<bean:write name="role" property="roleId"/>')">
																	<i class="ace-icon fa fa-lock bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="N" name="role" property="roleStatus">
																<a class="red" href="#" onclick="activateRole('<bean:write name="role" property="roleId"/>')">
																	<i class="ace-icon fa fa-unlock bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																	<logic:equal value="Y" name="priviledge" property="isEdit">
																		<li>
																			<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="editPermissionDtls('<bean:write name="role" property="roleId"/>','<bean:write name="role" property="roleName"/>','<bean:write name="role" property="roleStatus"/>')">
																				<span class="green">
																					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</logic:equal>
				
																	<logic:equal value="Y" name="role" property="roleStatus">
																		<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Deactivate" onclick="deactivateRole('<bean:write name="role" property="roleId"/>')">
																				<span class="red">
																					<i class="ace-icon fa fa-lock bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</logic:equal>
																	<logic:equal value="N" name="role" property="roleStatus">
																		<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Activate" onclick="activateRole('<bean:write name="role" property="roleId"/>')">
																				<span class="red">
																					<i class="ace-icon fa fa-unlock bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</logic:equal>
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
										<logic:empty name="roleList">
											<tr>
												<td colspan="4" align="center">
													<font color='red'>No data available</font>
												</td>
											</tr>
										</logic:empty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
			</div>
			</div>
		</div>
	</div>
	
	<div id="add-role-modal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Add New Role</h4>
				</div>
	
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<div id="submitMsgDiv" style="display: none"></div>
							<form>
								<div class="form-group">
									<label>Role Name:</label>
									<span class="pull-right">
										<input type="text" class="form-control" id="roleName" placeholder="Role Name"/>
									</span>
								</div>
								<div class="form-group">
									<label>Is Active:</label>
									<span class="pull-right">
										<label>
											<input name="form-field-checkbox" type="checkbox" class="ace" id="isActive">
											<span class="lbl"></span>
										</label>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
	
				<div class="modal-footer">
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>
	
					<button type="button" class="btn btn-sm btn-primary" onclick="addRoleDtls()">
						<i class="ace-icon fa fa-check"></i>
						Save
					</button>
				</div>
			</div>
		</div>
	</div><!-- PAGE CONTENT ENDS -->
	
	<script type="text/javascript">

		var context = "<%=request.getContextPath()%>";
		
		$(document).ready(function() 
		{
		    $('#dynamic-table').DataTable({
		            responsive: true
		    });

		    /********************************/
			//add tooltip for small view action buttons in dropdown menu
			$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
			
			//tooltip placement on right or left
			function tooltip_placement(context, source) {
				var $source = $(source);
				var $parent = $source.closest('table');
				var off1 = $parent.offset();
				var w1 = $parent.width();
		
				var off2 = $source.offset();
				//var w2 = $source.width();
		
				if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
				return 'left';
			}
	
		    
		});

		function editPermissionDtls(roleId,roleName,isActive)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/admin.html?method=getRolePermissionDtls&roleId="+roleId+"&roleName="+roleName+"&isActive="+isActive,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#contentDisplayDiv").html(responseText);
					$("#contentDisplayDiv").show();
				}
			});
		}

		function activateRole(roleId)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/admin.html?method=activateDeactivateRole&roleId="+roleId+"&typeFlag=Y",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#messageDiv").html(responseText);
					$("#messageDiv").show();
					setTimeout('hideStatus("messageDiv")',2000);
					setTimeout('reloadPage()',2000);
				}
			});
		}

		function deactivateRole(roleId)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/admin.html?method=activateDeactivateRole&roleId="+roleId+"&typeFlag=N",
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#messageDiv").html(responseText);
					$("#messageDiv").show();
					setTimeout('hideStatus("messageDiv")',2000);
					setTimeout('reloadPage()',2000);
				}
			});
		}

		function addRoleDtls()
		{
			var role = $('#roleName').val();
			var isActive = "N";
			
			if($('#isActive').is(":checked"))
				isActive = "Y";
			
			 $.ajax
				({
					type : "POST",
					url : "<%=request.getContextPath()%>/admin.html?method=add_role&role="+role+"&isActive="+isActive,
					data : $('form').serialize(),
					cache : false,
					dataType : "html",
					success : function(responseText) 
					{ 
						$("#submitMsgDiv").html(responseText);
						$("#submitMsgDiv").show();
						setTimeout('hideStatus("submitMsgDiv")',2000);
						setTimeout('reloadPage()',2000);
					}
				});
		}

		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>
	
		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
	</script>