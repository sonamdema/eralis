<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="table-responsive">
<button id="exportButton" onClick ="$('#renewal-search-table').tableExport({type:'excel',escape:'false'});" type='button'class="btn btn-primary btn-sm">
	Export Report
</button>
<table id="renewal-search-table" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th></th>
			<th>License/Learner No</th>
			<th>CID No</th>
			<th>Name</th>
			<th>Phone No</th>
			<th>Email Id</th>
			<th>Address</th>
			<th>Issue Type</th>
			<th>Test Type</th>
			<th>Test Drive Type</th>
			<th class="action">Action</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="candidate_list">
			<logic:iterate id="candidate" name="candidate_list" indexId="index">
				<%
					int a = index.intValue();
				%>
				<tr>
					<td><%=++a %></td>
					<td>
						<bean:write name="candidate" property="learnerNo"/>
					</td>
					<td>
						<bean:write name="candidate" property="CID"/>
					</td>
					<td>
						<bean:write name="candidate" property="name"/>
					</td>
					<td>
						<bean:write name="candidate" property="phoneNo"/>
					</td>
					<td>
						<bean:write name="candidate" property="emailId"/>
					</td>
					<td>
						<bean:write name="candidate" property="contactAddres"/>
					</td>
					<td>
						<bean:write name="candidate" property="issueType"/>
					</td>
					<td>
						<bean:write name="candidate" property="testType"/>
					</td>
					<td>
						<bean:write name="candidate" property="driveType"/>
					</td>
					<td class="action">
						<button type="button" class="btn btn-primary btn-sm"
						onclick="cancelBooking('<bean:write name="candidate" property="appNo"/>')">Cancel</button>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
</div>

<script>
	function cancelBooking(applicationNo)
	{
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/etest.html?method=cancelBooking&applicationNo="+applicationNo,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#resultDIV").html(responseText);
			}
		});
		
	}

</script>