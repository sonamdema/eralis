<%@page import="bt.gov.rsta.framework.dto.EmailConfigDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%

	EmailConfigDTO dto = (EmailConfigDTO) request.getAttribute("EMAIL_CONFIG");

%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			Email Configuration
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; configure mail SMTP configurations
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title"></h4>
							<logic:equal value="Y" name="priviledge" property="isEdit">
								<span class="widget-toolbar">
									<button class="btn btn-primary btn-xs" onclick="updateEmailConfiguration()">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
										Update
									</button>
								</span>
							</logic:equal>
						</div>
						<div class="widget-body">
							<div id="messageDiv" style="display:none"></div>
							<div class="widget-main">
								<html:form styleId="emailForm" action="/admin.html" method="POST" styleClass="form-horizontal">
									<html:hidden property="mailConfigId" value="<%=dto.getConfigId() %>"/>
									<div class="row">
									    <div class="col-lg-12">
									    
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>SMTP Host:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:text property="smtpHost" styleClass="form-control" value="<%=dto.getSmtpHost() %>"></html:text>
													</div>
									    		</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>SMTP Port:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:text property="smtpPort" styleClass="form-control" value="<%=dto.getSmtpPort() %>"></html:text>
													</div>
									    		</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>Debug Flag:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:select property="debugFlag" styleClass="form-control" styleId="debugFlag">
															<html:option value="">--SELECT--</html:option>
															<html:option value="true">True</html:option>
															<html:option value="false">False</html:option>
														</html:select>
													</div>
									    		</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>Start TTLS Enable Flag:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:select property="ttlsFlag" styleClass="form-control" styleId="ttlsFlag">
															<html:option value="">--SELECT--</html:option>
															<html:option value="true">True</html:option>
															<html:option value="false">False</html:option>
														</html:select>
													</div>
									    		</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>Mail Sender Address:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:text property="senderAddress" styleClass="form-control" value="<%=dto.getFromAddress() %>"></html:text>
													</div>
									    		</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>Mail Sender Password:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:text property="senderPassword" styleClass="form-control" value="<%=dto.getPassword() %>"></html:text>
													</div>
									    		</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-3">
									    			<label>SMTP Auth Flag:</label>
									    		</div>
									    		<div class="col-lg-3">
									    			<div class="input-group">
														<html:select property="smtpAuthFlag" styleClass="form-control" styleId="smtpAuthFlag">
															<html:option value="">--SELECT--</html:option>
															<html:option value="true">True</html:option>
															<html:option value="false">False</html:option>
														</html:select>
													</div>
									    		</div>
									    	</div>
									    	
									 	</div>
									 </div>
									 
								</html:form>
							</div>
						</div>
					</div>
			</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">

		var context = "<%=request.getContextPath()%>";
		var debugFlag = "<%=dto.getMailDebug()%>";
		var startTLSFlag = "<%=dto.getSmtpStartTLS()%>";
		var smtpAuthFlag = "<%=dto.getSmtpAuth()%>";
		
		$(document).ready(function() 
		{
			$('#smtpAuthFlag').val(smtpAuthFlag);
			$('#debugFlag').val(debugFlag);
			$('#ttlsFlag').val(startTLSFlag);
		});

		function updateEmailConfiguration()
		{
			var options = {target:'#messageDiv',url:'<%=request.getContextPath()%>/admin.html?method=updateMailConfiguration',type:'POST',data: $("#emailForm").serialize()}; 
		    $("#emailForm").ajaxSubmit(options);
	        $('#messageDiv').show();
	        setTimeout('hideStatus("messageDiv")',2000);
	        setTimeout('reloadPage()',3000);
		}

		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>
	
		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
	</script>