<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="panel-body">
	<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>sl</th> 
				<th>Vehicle Type</th> 
				<th>Loading Capacity</th> 
				<th>Seating Capacity</th> 
				<th>Horse Power</th> 
				<th>Engine CC</th> 
				<th>Amount</th> 
				<th>RC Cost</th> 
				<th>Fitness Amount</th> 
				<th>Ownership Transfer Fee</th> 
				<th>Regional Transfer Fee</th> 
				<th>Conversion Fee</th> 
				<th>Transfer Tax</th> 
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="VEHICLE_CONSTANT_DTLS">
					<logic:iterate id="vehicleConstant" name="VEHICLE_CONSTANT_DTLS" indexId="index">
					<% int i = index.intValue()+1; %>
						<tr>
							<td><%=i%></td>
							<td><bean:write name="vehicleConstant" property="vehicleType"/></td>
							<td><bean:write name="vehicleConstant" property="loadCapacity"/></td>
							<td><bean:write name="vehicleConstant" property="seatCapacity"/></td>
							<td><bean:write name="vehicleConstant" property="vehicleHorsePower"/></td>
							<td><bean:write name="vehicleConstant" property="engineCC"/></td>
							<td><bean:write name="vehicleConstant" property="amount"/></td>
							<td><bean:write name="vehicleConstant" property="rcCost"/></td>
							<td><bean:write name="vehicleConstant" property="fitnessAmount"/></td>
							<td><bean:write name="vehicleConstant" property="ownershipTransferFees"/></td>
							<td><bean:write name="vehicleConstant" property="regionalTransferFee"/></td>
							<td><bean:write name="vehicleConstant" property="conversionFee"/></td>
							<td><bean:write name="vehicleConstant" property="transferTax"/></td>
							<td>
								<a class="green" href="#" onclick="vehicleContantModal('<bean:write name="vehicleConstant" property="vehicleConstantId"/>','<bean:write name="vehicleConstant" property="vehicleType"/>','<bean:write name="vehicleConstant" property="loadCapacity"/>','<bean:write name="vehicleConstant" property="seatCapacity"/>','<bean:write name="vehicleConstant" property="vehicleHorsePower"/>','<bean:write name="vehicleConstant" property="engineCC"/>','<bean:write name="vehicleConstant" property="amount"/>','<bean:write name="vehicleConstant" property="rcCost"/>','<bean:write name="vehicleConstant" property="fitnessAmount"/>','<bean:write name="vehicleConstant" property="ownershipTransferFees"/>','<bean:write name="vehicleConstant" property="regionalTransferFee"/>','<bean:write name="vehicleConstant" property="conversionFee"/>','<bean:write name="vehicleConstant" property="transferTax"/>','<bean:write name="vehicleConstant" property="flag"/>')">
								 <i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>
							</td>
						</tr>
					</logic:iterate>
				</logic:notEmpty>
				<logic:empty name="VEHICLE_CONSTANT_DTLS">
					<tr>
						<td colspan="8" align="center">
							<font color='red'>NO RECORD FOUND</font>
						</td>
					</tr>
				</logic:empty>
		</tbody>
	</table>
</div>
<script>
	function vehicleContantModal(vehicleConstantId,vehicleType,loadCapacity,
			seatCapacity,vehicleHorsePower,engineCC,amount,rcCost,
			fitnessAmount,ownershipTransferFees,regionalTransferFee,
			conversionFee,transferTax,flag)
	{ 
		$('#vehicleConstantId').val(vehicleConstantId);
		$('#vehicleType').val(vehicleType);
		$('#amount').val(amount);
		$('#rcCost').val(rcCost);
		$('#fitnessAmount').val(fitnessAmount);
		$('#ownershipTransferFees').val(ownershipTransferFees);
		$('#regionalTransferFee').val(regionalTransferFee);
		$('#conversionFee').val(conversionFee);
		$('#transferTax').val(transferTax);
		$('#flag').val(flag);
		
		$('#loadCapacityDisplay').hide();
		$('#seatCapacityDisplay').hide();
		$('#horsepowerDisplay').hide();
		$('#engineCCDisplay').hide();
		
		if(vehicleHorsePower!='')
		{
			$('#vehicleHorsePower').prop('disabled',false);
			$('#loadCapacity').prop('disabled',true);
			$('#loadCapacity').val('');
			$('#seatCapacity').prop('disabled',true);
			$('#seatCapacity').val('');
			$('#engineCC').prop('disabled',true);
			$('#engineCC').val('');
			$('#vehicleHorsePower').val(vehicleHorsePower);
			$('#horsepowerDisplay').show();
		}
		if(loadCapacity!='')
		{
			$('#vehicleHorsePower').prop('disabled',true);
			$('#vehicleHorsePower').val('');
			$('#seatCapacity').prop('disabled',true);
			$('#seatCapacity').val('');
			$('#engineCC').prop('disabled',true);
			$('#engineCC').val('');
			$('#loadCapacity').prop('disabled',false);
			$('#loadCapacity').val(loadCapacity);
			$('#loadCapacityDisplay').show();
		}
		if(seatCapacity!='')
		{	
			$('#seatCapacity').prop('disabled',false);
			$('#seatCapacity').val(seatCapacity);
			$('#vehicleHorsePower').prop('disabled',true);
			$('#vehicleHorsePower').val('');
			$('#engineCC').prop('disabled',true);
			$('#engineCC').val('');
			$('#loadCapacity').prop('disabled',true);
			$('#loadCapacity').val('');
			$('#seatCapacityDisplay').show();
		}
		if(engineCC!='')
		{	$('#engineCC').prop('disabled',false);
			$('#engineCC').val(engineCC);
			$('#seatCapacity').prop('disabled',true);
			$('#seatCapacity').val('');
			$('#vehicleHorsePower').prop('disabled',true);
			$('#vehicleHorsePower').val('');
			$('#loadCapacity').prop('disabled',true);
			$('#loadCapacity').val('');
			$('#engineCCDisplay').show();
		}
		$('#vehicleContantModal').modal('show');
	}
</script>