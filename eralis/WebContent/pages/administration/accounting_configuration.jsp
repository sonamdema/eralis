 <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<script>
	var context = "<%=request.getContextPath()%>";
</script>
<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-gears"></i>
		Accounting Configuration
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			manage &amp; configure accounts
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">

	<div class="col-xs-12">
		<div class="row">
			<div id="accordion" class="accordion-style1 panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#vehicle">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							<span data-rel="tooltip" data-placement="bottom" title="Page Privileges - This panel lists the pages and their respective privileges">
									Vehicle Accounts (Click Here To Expand)
							</span>
						</a>
					</div>
					<div class="panel-collapse collapse" id="vehicle">
						<jsp:include page="/pages/administration/constant_vehicle.jsp"></jsp:include>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#license">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							<span data-rel="tooltip" data-placement="bottom" title="Page Privileges - This panel lists the pages and their respective privileges">
									License Accounts (Click Here To Expand)
							</span>
						</a>
					</div>
					<div class="panel-collapse collapse" id="license">
						<jsp:include page="/pages/administration/constant_license.jsp"></jsp:include>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#penalty">
							<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
							<span data-rel="tooltip" data-placement="bottom" title="Page Privileges - This panel lists the pages and their respective privileges">
									Penalty Accounts (Click Here To Expand)
							</span>
						</a>
					</div>
					<div class="panel-collapse collapse" id="penalty">
						<jsp:include page="/pages/administration/constant_penalty.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="vehicleContantModal" class="modal" tabindex="-1">
	<html:form  action="/admin.html" styleId="vehicleConstant">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Edit Vehicle Accounting</h4>
				</div>
				<div class="modal-body">
					
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Vehicle Type:</label>
										<span class="pull-right">
											<input readonly="readonly" type="text" id="vehicleType"/>
										</span>
									</div>
									<div class="form-group">
										<label>:</label>
										<span class="pull-right">
											<select name="flag" id='flag'>
												<option value='0'>Less Than</option>
												<option value='1'>More Than</option>
												<option value='2'>Equals To </option>
											</select>
										</span>
									</div>
									<div class="form-group" id="loadCapacityDisplay">
										<label>Load Capacity (Ton):</label>
										<span class="pull-right">
											<html:text property="loadCapacity" styleClass="form-control" styleId="loadCapacity"></html:text>
										</span>
									</div>
									<div class="form-group" id="seatCapacityDisplay">
										<label>Seat Capacity:</label>
										<span class="pull-right">
											<html:text property="seatCapacity" styleClass="form-control" styleId="seatCapacity"></html:text>
										</span>
									</div>
									<div class="form-group" id="horsepowerDisplay">
										<label>Vehicle HorsePower:</label>
										<span class="pull-right">
											<html:text property="vehicleHorsePower" styleClass="form-control" styleId="vehicleHorsePower"></html:text>
										</span>
									</div>
									<div class="form-group" id="engineCCDisplay">
										<label>Engine CC:</label>
										<span class="pull-right">
											<html:text property="engineCC" styleClass="form-control" styleId="engineCC"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Amount:</label>
										<span class="pull-right">
											<html:text property="amount" styleClass="form-control" styleId="amount"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>RC Cost:</label>
										<span class="pull-right">
											<html:text property="rcCost" styleClass="form-control" styleId="rcCost"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Fitness Amount:</label>
										<span class="pull-right">
											<html:text property="fitnessAmount" styleClass="form-control" styleId="fitnessAmount"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Ownership TransferFees:</label>
										<span class="pull-right">
											<html:text property="ownershipTransferFees" styleClass="form-control" styleId="ownershipTransferFees"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Regional TransferFee:</label>
										<span class="pull-right">
											<html:text property="regionalTransferFee" styleClass="form-control" styleId="regionalTransferFee"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Conversion Fee:</label>
										<span class="pull-right">
											<html:text property="conversionFee" styleClass="form-control" styleId="conversionFee"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Transfer Tax:</label>
										<span class="pull-right">
											<html:text property="transferTax" styleClass="form-control" styleId="transferTax"></html:text>
										</span>
									</div> 
							</div>
						</div>
					
				</div>
				<div class="modal-footer">
					<html:hidden property="vehicleConstantId"  styleClass="form-control" styleId="vehicleConstantId"></html:hidden>
					<button class="btn btn-primary btn-sm" type="button" onClick="formSubmit()">
						Save Changes
					</button>
				</div>
			</div>
		</div>
	</html:form>
</div>
<div id="licenseContantModal" class="modal" tabindex="-1">
	<html:form  action="/admin.html" styleId="licenseConstant">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Edit License Accounting</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>License Type:</label>
									<span class="pull-right">
										<input readonly="readonly" type="text" id="licensetype"/>
									</span>
								</div>
								<div class="form-group">
									<label>Service Type:</label>
									<span class="pull-right">
										<html:text property="serviceType" styleClass="form-control" styleId="serviceType"></html:text>
									</span>
								</div> 
								<div class="form-group">
									<label>Amount:</label>
									<span class="pull-right">
										<html:text property="amount" styleClass="form-control" styleId="licenseAmount"></html:text>
									</span>
								</div> 
								<div class="form-group">
									<label>License Card Cost:</label>
									<span class="pull-right">
										<html:text property="licenseCardCost" styleClass="form-control" styleId="licenseCardCost"></html:text>
									</span>
								</div> 
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<html:hidden property="licenseConstantId"  styleClass="form-control" styleId="licenseConstantId"></html:hidden>
					<button class="btn btn-primary btn-sm" type="button" onClick="submitLicenseConstant()">
						Save Changes
					</button>
				</div>
			</div>
		</div>
	</html:form>
</div>
<div id="penaltyContantModal" class="modal" tabindex="-1">
	<html:form  action="/admin.html" styleId="penaltyConstant">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Edit Penalty Accounting</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Request Type:</label>
									<span class="pull-right">
										<input readonly="readonly" type="text" id="penaltyRequestType"/>
									</span>
								</div>
								<div class="form-group">
									<label>Service Type:</label>
									<span class="pull-right">
										<input readonly="readonly" type="text" id="penaltyServiceType"/>
									</span>
								</div>
								<div class="form-group">
									<label>Penalty Per Day:</label>
									<span class="pull-right">
										<html:text property="penaltyPerDay" styleClass="form-control" styleId="penaltyPerDay"></html:text>
									</span>
								</div> 
								<div class="form-group">
									<label>Max Penalty:</label>
									<span class="pull-right">
										<html:text property="maxPenalty" styleClass="form-control" styleId="maxPenalty"></html:text>
									</span>
								</div> 
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<html:hidden property="penaltyId"  styleClass="form-control" styleId="penaltyId"></html:hidden>
					<button class="btn btn-primary btn-sm" type="button" onClick="submitPenaltyConstant()">
						Save Changes
					</button>
				</div>
			</div>
		</div>
	</html:form>
</div>

<!-- PAGE CONTENT ENDS -->
<!-- PAGE CONTENT ENDS -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
<script>
	function formSubmit()
	{
		var options = {target:'#dispalyMessage',url:context+'/admin.html?method=edit_vehicle_constant',type:'POST',data: $("#vehicleConstant").serialize()}; 
	    $("#vehicleConstant").ajaxSubmit(options);
        $('#dispalyMessage').show();
        setTimeout('hideStatus("dispalyMessage")',2000);
        setTimeout('reloadPage()',2000);
	}
	function submitLicenseConstant()
	{
		var options = {target:'#dispalyMessage',url:context+'/admin.html?method=edit_license_constant',type:'POST',data: $("#licenseConstant").serialize()}; 
	    $("#licenseConstant").ajaxSubmit(options);
        $('#dispalyMessage').show();
        setTimeout('hideStatus("dispalyMessage")',2000);
        setTimeout('reloadPage()',2000);

	}
	function submitPenaltyConstant()
	{
		var options = {target:'#dispalyMessage',url:context+'/admin.html?method=edit_penalty_constant',type:'POST',data: $("#penaltyConstant").serialize()}; 
	    $("#penaltyConstant").ajaxSubmit(options);
        $('#dispalyMessage').show();
        setTimeout('hideStatus("dispalyMessage")',2000);
        setTimeout('reloadPage()',2000);

	}
</script>