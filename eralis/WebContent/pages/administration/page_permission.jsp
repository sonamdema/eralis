<%@page import="bt.gov.rsta.eralis.dto.administration.PagePermissionDTO"%>
<%@page import="bt.gov.rsta.eralis.dto.administration.PrivDTO"%>
<%@page import="java.util.List"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			Role Management
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; configure user roles &amp; their page permissions
			</small>
		</h1>
	</div><!-- /.page-header -->
	<html:form action="/admin.html" method="post" styleId="permissionForm">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="displayMsgDiv" style="display:none"></div>
							<div class="col-sm-4 row">
								<div class="form-group">
									<label>Role Name:</label>
									<span>
										<input type="text" class="form-control" id="roleName" value="<%=(String)request.getAttribute("roleName")%>"/>
										<input type="hidden" value="<%=(String)request.getAttribute("roleId")%>" id="roleId" name="roleId"/>
										<input type="hidden" id="checkedVals" name="checkedVals"/>
										<input type="hidden" id="serviceCheckedVals" name="serviceCheckedVals"/>
									</span>
								</div>
								<div class="form-group">
									<label>Is Active:</label>
									<span>
										<label>
											<input name="form-field-checkbox" type="checkbox" class="ace" id="isActive">
											<span class="lbl"></span>
										</label>
									</span>
								</div>
								<div class="form-group">
									<button type="button" class="btn btn-primary btn-sm" onclick="updatePermissionDtls()">Save</button>
									&nbsp;
									<button type="reset" class="btn btn-primary btn-sm">Cancel</button>
								</div>
							</div>
							
					</div>
				</div>
				</div>
				
				<div class="row">
					<div id="accordion" class="accordion-style1 panel-group">
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#service">
									<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
									<span data-rel="tooltip" data-placement="top" title="Service Privileges - This panel lists the services and their respective privileges">
											Service Privileges (Click Here To Expand)
									</span>
								</a>
							</div>
							
							<div class="panel-collapse collapse" id="service">
								<div class="panel-body">
									<table id="simple-table" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th></th>
												<th>Services</th>
												<th>Privileges</th>
											</tr>
										</thead>
										<tbody>
											<logic:notEmpty name="servicePrivList">
												<logic:iterate id="service" name="servicePrivList" type="bt.gov.rsta.eralis.dto.administration.PagePermissionDTO" indexId="index">
													<%
														int i = index.intValue();
													%>
													<tr>
														<td><%=++i %></td>
														<td>
															<input type="hidden" id='<%="serviceId_"+i %>' value="<%=service.getServiceId()%>">
															<bean:write name="service" property="serviceName"/>
														</td>
														<td>
															<logic:iterate id="priv" name="service" property="privileges" type="bt.gov.rsta.eralis.dto.administration.PrivDTO">
																<label>
																	<input type="checkbox" class="ace" id='<%=priv.getPrivName()+"_"+i %>'/>
																	<span class="lbl">
																		<bean:write name="priv" property="privName"/>
																	</span>
																</label>
																<input type="hidden" id='<%=priv.getPrivName()+"_field_"+i %>' value="<%=priv.getPrivId()%>"/>
																&nbsp;
																<% 
																	List<PrivDTO> previousPrivList = (List<PrivDTO>) request.getAttribute("previousPrivList");
																	for(int j=0; j < previousPrivList.size(); j++)
																	{
																		if(previousPrivList.get(j).getPrivId().equals(priv.getPrivId()))
																		{
																%>
																	<script type="text/javascript">
												  		                $("#<%=priv.getPrivName()%>_<%=i%>").attr('checked', true);
												                    </script>
																<% 
																		}
																	}
																%>
															</logic:iterate>
														</td>
													</tr>
												</logic:iterate>
											</logic:notEmpty>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#page">
									<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
									<span data-rel="tooltip" data-placement="bottom" title="Page Privileges - This panel lists the pages and their respective privileges">
											Page Privileges (Click Here To Expand)
									</span>
								</a>
							</div>
							
							<div class="panel-collapse collapse" id="page">
								<div class="panel-body">
									<table id="dynamic-table" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Menu Name</th>
												<th>Page Name</th>
												<th align="center">
													New&nbsp;
													<input name="isNew" type="checkbox" class="ace" id="isNew">
													<span class="lbl"></span>
												</th>
												<th align="center">
													Edit&nbsp;
													<input name="isEdit" type="checkbox" class="ace" id="isEdit">
													<span class="lbl"></span>
												</th>
												<th align="center">
													Delete&nbsp;
													<input name="isDelete" type="checkbox" class="ace" id="isDelete">
													<span class="lbl"></span>
												</th>
												<th align="center">
													Disabled&nbsp;
													<input name="isDisabled" type="checkbox" class="ace" id="isDisabled">
													<span class="lbl"></span>
												</th>
											</tr>
										</thead>
										
										<tbody>
											
											<logic:iterate id="checkbox" property="permissionList" name="adminForm" type="bt.gov.rsta.eralis.dto.administration.PagePermissionDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td>
														<input type="hidden" id='<%="pageId_"+a %>' value="<%=checkbox.getPageId()%>">
														<bean:write name="checkbox" property="menuName"/>
													</td>
													<td>
														<bean:write name="checkbox" property="pageName"/>
													</td>
													<td align="center">
														<logic:equal value="N" name="checkbox" property="isNew">
															<html:checkbox property="isNew" name="checkbox" styleClass="ace" styleId='<%="isNew_"+a %>' indexed="true"></html:checkbox>
															<span class="lbl"></span>
														</logic:equal>
														<logic:equal value="Y" name="checkbox" property="isNew">
															<html:checkbox property="isNew" name="checkbox" styleClass="ace" styleId='<%="isNew_"+a %>' indexed="true"></html:checkbox>
															<span class="lbl"></span>
															<script type="text/javascript">
										  		                  $("#isNew_<%=a%>").attr('checked', true);
										                    </script>
														</logic:equal>
													</td>
													<td align="center">
														<logic:equal value="N" name="checkbox" property="isEdit">
															<html:checkbox property="isEdit" name="checkbox" styleClass="ace" styleId='<%="isEdit_"+a %>' indexed="true"></html:checkbox>
															<span class="lbl"></span>
														</logic:equal>
														<logic:equal value="Y" name="checkbox" property="isEdit">
															<html:checkbox property="isEdit" name="checkbox" styleClass="ace" styleId='<%="isEdit_"+a %>' indexed="true"></html:checkbox>
															<span class="lbl"></span>
															<script type="text/javascript">
										  		                  $("#isEdit_<%=a%>").attr('checked', true);
										                    </script>
														</logic:equal>
													</td>
													<td align="center">
														<logic:equal value="N" name="checkbox" property="isDelete">
															<html:checkbox property="isDelete" name="checkbox" styleClass="ace" styleId='<%="isDelete_"+a %>' indexed="true"></html:checkbox>
															<span class="lbl"></span>
														</logic:equal>
														<logic:equal value="Y" name="checkbox" property="isDelete">
															<html:checkbox property="isDelete" name="checkbox" styleClass="ace" styleId='<%="isDelete_"+a %>' indexed="true"></html:checkbox>
															<span class="lbl"></span>
															<script type="text/javascript">
										  		                  $("#isDelete_<%=a%>").attr('checked', true);
										                    </script>
														</logic:equal>
													</td>
													<td align="center">
														<logic:equal value="N" name="checkbox" property="isDisabled">
															<html:checkbox property="isDisabled" name="checkbox" styleClass="ace" styleId='<%="isDisabled_"+a %>' indexed="true" onclick="checkIfChecked(this.id)"></html:checkbox>
															<span class="lbl"></span>
														</logic:equal>
														<logic:equal value="Y" name="checkbox" property="isDisabled">
															<html:checkbox property="isDisabled" name="checkbox" styleClass="ace" styleId='<%="isDisabled_"+a %>' indexed="true" onclick="checkIfChecked(this.id)"></html:checkbox>
															<span class="lbl"></span>
															<script type="text/javascript">
										  		                  $("#isDisabled_<%=a%>").attr('checked', true);
										                    </script>
														</logic:equal>
													</td>
												</tr>
											</logic:iterate>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					</div>
				
			</div>
		</div>
	</html:form>
	<script>

		<%
			List<String> pageIdList = (List<String>) request.getAttribute("pageIdList");
			String[] arr = pageIdList.toArray(new String[pageIdList.size()]);
			
			String serviceCount = (String) request.getAttribute("serviceCount");
			
			String isActive = (String)request.getAttribute("isActive");
		%>
		var arrLen = "<%=arr.length%>";
		var serviceArrLen = "<%=serviceCount%>";
		var isActive = "<%=isActive%>";

		$(document).ready(function() 
		{

		  $('[data-rel=tooltip]').tooltip();
			
		  if(isActive == "Y")
			  $('#isActive').attr('checked',true);
			
		  $('#isNew').click(function() 
		  {
			  if($('#isNew').is(':checked'))
			  {
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isNew_'+i).each(function() {
				            this.checked = true;                        
				        });
				  }
			  }
			  else
			  { 
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isNew_'+i).each(function() {
				            this.checked = false;                        
				        });
				  }
			  }
		  });

		  $('#isEdit').click(function() 
		  {
			  if($('#isEdit').is(':checked'))
			  {
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isEdit_'+i).each(function() {
				            this.checked = true;                        
				        });
				  }
			  }
			  else
			  { 
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isEdit_'+i).each(function() {
				            this.checked = false;                        
				        });
				  }
			  }
		  });

		  $('#isDelete').click(function() 
		  {
			  if($('#isDelete').is(':checked'))
			  {
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isDelete_'+i).each(function() {
				            this.checked = true;                        
				        });
				  }
			  }
			  else
			  { 
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isDelete_'+i).each(function() {
				            this.checked = false;                        
				        });
				  }
			  }
		  });

		  $('#isDisabled').click(function() 
		  {
			  if($('#isDisabled').is(':checked'))
			  {
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isDisabled_'+i).each(function() {
				            this.checked = true;                        
				        });

					  $('#isNew_'+i).each(function() {
				            this.checked = false;                        
				        });

					  $('#isEdit_'+i).each(function() {
				            this.checked = false;                        
				        });
				        
					  $('#isDelete_'+i).each(function() {
				            this.checked = false;                        
				        });
				  }
			  }
			  else
			  { 
				  for(var i=0;i<arrLen;i++)
				  {
					  $('#isDisabled_'+i).each(function() {
				            this.checked = false;                        
				        });
				  }
			  }
		  });
		  
		});

		function checkIfChecked(id)
		{
			var pattern=/[0-9]+/;
			var no = id.match(pattern);
			
			if($('#'+id).is(':checked'))
			{
				$('#isNew_'+no).attr('checked',false);
				$('#isEdit_'+no).attr('checked',false);
				$('#isDelete_'+no).attr('checked',false);
			}
		}

		function updatePermissionDtls()
		{
			getCheckedVals();
			getServiceCheckedVals();
			
			var roleId = $('#roleId').val();
			var roleName = $('#roleName').val();
			var isActive = "N";
			
			if($('#isActive').is(":checked"))
				isActive = "Y";

			var options = {target:'#displayMsgDiv',url:context+'/admin.html?method=update_permission_dtls&roleId='+roleId+'&roleName='+roleName+'&isActive='+isActive,type:'POST',data: $("#permissionForm").serialize()}; 
		    $("#permissionForm").ajaxSubmit(options);
	        $('#displayMsgDiv').show();
	        setTimeout('hideStatus("displayMsgDiv")',4000);
		}

		function getCheckedVals()
		{
			var vals = '';
			var countFlag = 0;

		 	for(var i=0;i<arrLen;i++)
		  	{
			 	var pageId = $('#pageId_'+i).val();

			 	var isNew = "N", isEdit = "N", isDelete = "N", isDisabled = "N";

			 	if($('#isNew_'+i).is(':checked'))
			 		isNew ="Y";
			 	if($('#isEdit_'+i).is(':checked'))
			 		isEdit ="Y";
			 	if($('#isDelete_'+i).is(':checked'))
			 		isDelete ="Y";
			 	if($('#isDisabled_'+i).is(':checked'))
			 		isDisabled ="Y";

		 		if(parseInt(countFlag) > 0){
					vals = vals + '#';
			 	}

			 	vals = vals + pageId + '~' + isNew + '~' + isEdit + '~' + isDelete + '~' + isDisabled;
			 	countFlag++;
		  	}
		  	
			$('#checkedVals').val(vals);
		}

		function getServiceCheckedVals()
		{
			var vals = '';
			var countFlag = 0;

		 	for(var i=1;i<=serviceArrLen;i++)
		  	{
			 	var verifierPrivId = "0", approverPrivId = "0", printerPrivId = "0", adminPrivId = "0", submitterPrivId = "0";

			 	if($('#Submitter_'+i).is(':checked'))
			 		submitterPrivId = $('#Submitter_field_'+i).val();
			 	if($('#Verifier_'+i).is(':checked'))
			 		verifierPrivId = $('#Verifier_field_'+i).val();
			 	if($('#Approver_'+i).is(':checked'))
			 		approverPrivId = $('#Approver_field_'+i).val();
			 	if($('#Printer_'+i).is(':checked'))
			 		printerPrivId = $('#Printer_field_'+i).val();
			 	if($('#Admin_'+i).is(':checked'))
			 		adminPrivId = $('#Admin_field_'+i).val();

		 		if(parseInt(countFlag) > 0){
					vals = vals + '#';
			 	}

			 	vals = vals + verifierPrivId + '~' + approverPrivId + '~' + printerPrivId + '~' + adminPrivId + '~' + submitterPrivId;
			 	countFlag++;
		  	}
		  	
			$('#serviceCheckedVals').val(vals);
		}

	</script>