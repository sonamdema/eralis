<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<html:form action="/admin.html" method="post" styleId="editForm">

	<logic:equal value="master_region" name="TYPE">
		<div id="region-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Region</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Region Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region Description:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control" styleId="description"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region Number:</label>
										<span class="pull-right">
											<html:text property="masterNumber" styleClass="form-control" styleId="number"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_REGION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_route" name="TYPE">
		<div id="route-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Route</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Route Name:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_ROUTE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	<logic:equal value="master_dzongkhag" name="TYPE">
		<div id="dzongkhag-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Dzongkhag</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Dzongkhag Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="otherId">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_DZONGKHAG')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_gewog" name="TYPE">
		<div id="gewog-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Gewog</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Gewog Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Dzongkhag:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="otherId">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_GEWOG')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_ministry" name="TYPE">
		<div id="ministry-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Ministry</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Ministry Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Description:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control" styleId="description"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_MINISTRY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_department" name="TYPE">
		<div id="department-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Department</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Department Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Ministry:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="otherId">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_DEPARTMENT')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_country" name="TYPE">
		<div id="country-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Country</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Country Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Nationality:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control" styleId="description"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_COUNTRY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_offence" name="TYPE">
		<div id="offence-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Offence</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Offence Name:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name" style="width: 450px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Amount:</label>
										<span class="pull-right">
											<html:text property="masterNumber" styleClass="form-control" styleId="number"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_OFFENCE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_accident" name="TYPE">
		<div id="accident-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Accident Cause</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Accident Cause:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name" style="width: 450px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Accident Type:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="description">
												<html:option value="">--SELECT--</html:option>
												<html:option value="DE">Driver Error</html:option>
												<html:option value="RC">Road Condition</html:option>
												<html:option value="WC">Weather Condition</html:option>
												<html:option value="MF">Mechanical Failure</html:option>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_ACCIDENT')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_action" name="TYPE">
		<div id="action-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Action Taken</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Action Taken:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_ACTION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_designation" name="TYPE">
		<div id="designation-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Designation</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Designation:</label>
										<span class="pull-right">
											<html:hidden property="masterId" styleId="id"/>
											<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_DESIGNATION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_drive" name="TYPE">
		<div id="drive-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Drive Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Drive Type:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Drive Type Category:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="description">
											<html:option value="">--SELECT--</html:option>
											<html:option value="O">Ordinary</html:option>
											<html:option value="C">Commercial</html:option>
											<html:option value="T">Taxi</html:option>
										</html:select>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_DRIVETYPE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_engine" name="TYPE">
		<div id="engine-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Engine Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Engine Type:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_ENGINETYPE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_hypo" name="TYPE">
		<div id="hypo-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Hypothecated</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Hypothecated:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_HYPOTHECATED')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_occupation" name="TYPE">
		<div id="occupation-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Occupation</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Occupation:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_OCCUPATION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_owner" name="TYPE">
		<div id="owner-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Owner Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Occupation:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_OWNER')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_courtesy" name="TYPE">
		<div id="courtesy-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Title of Courtesy</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Title of Courtesy:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_COURTESY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_company" name="TYPE">
		<div id="vehicle-company-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Vehicle Company</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Company:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_VEHICLE_COMPANY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_model" name="TYPE">
		<div id="vehicle-model-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Vehicle Model</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Model:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Vehicle Company:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="otherId">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="vehicleCompanyList" label="headerName" value="headerId"/>
										</html:select>
									</span>
								</div>
								<div class="form-group">
									<label>Engine CC/ KiloWatt/ Horse Power / GVW</label>
									<span class="pull-right">
										<html:text property="engineCC" styleClass="form-control" styleId="engineCC"></html:text>										
									</span>
								</div>
								<%-- <div class="form-group">
									<label>Unladen Weight</label>
									<span class="pull-right">
										<html:text property="unladenWeight" styleClass="form-control" styleId="unladenWeight"></html:text>										
									</span>
								</div> --%>
								<div class="form-group">
									<label>Seating Capacity</label>
									<span class="pull-right">
										<html:text property="seatCapacity" styleClass="form-control" styleId="seatCapacity"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_VEHICLE_MODEL')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_vehicle_type" name="TYPE">
		<div id="vehicle-type-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Vehicle Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Type:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Vehicle Type Number:</label>
									<span class="pull-right">
										<html:text property="masterNumber" styleClass="form-control" styleId="number"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_VEHICLE_TYPE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_registration_code" name="TYPE">
		<div id="registration-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Vehicle Registration Code</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Type:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_REGISTRATION_CODE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_dealer" name="TYPE">
		<div id="dealer-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Vehicle Dealer</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Dealer Name:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name" style="width: 450px;"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_DEALER')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_cancellation_reasons" name="TYPE">
		<div id="reason-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Cancellation Reason</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Reason:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:textarea property="masterName" styleClass="form-control" styleId="name" style="margin: 0px; height: 73px; width: 479px;"></html:textarea>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_REASON')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_base_office" name="TYPE">
		<div id="base-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Base Office</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Base Office Name:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Regional Office:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="otherId">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
										</html:select>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_BASE_OFFICE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_blood_group" name="TYPE">
		<div id="blood-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Blood Group</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Blood Group:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_BLOOD_GROUP')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_practical_test_criteria" name="TYPE">
		<div id="practical-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Practical Test Criteria</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Criteria:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Full Point:</label>
									<span class="pull-right">
										<html:text property="masterNumber" styleClass="form-control" styleId="number"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_PRACTICAL_CRITERIA')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal> 
	<logic:equal value="master_village" name="TYPE">
		<div id="village-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Village</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Village:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Gewog:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px" styleId="otherId">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="gewogList" label="headerName" value="headerId"/>
										</html:select>	
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_VILLAGE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	<logic:equal value="master_colour" name="TYPE">
		<div id="colour-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Colour</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Colour Name:</label>
									<span class="pull-right">
										<html:hidden property="masterId" styleId="id"/>
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Colour Description:</label>
									<span class="pull-right">
										<html:text property="masterDesc" styleClass="form-control" styleId="description"></html:text>	
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_COLOUR')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_diplomats" name="TYPE">
		<div id="diplomats-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Diplomats</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Diplomat's Name :</label>
									<span class="pull-right col-lg-8">
										<html:hidden property="masterId" styleId="id"/>
							          	<html:text property="masterDesc" styleClass="form-control" styleId="description"></html:text>
									</span>
								</div>
								<div class="form-group">
									<label>Diplomats Code:</label>
									<span class="pull-right col-lg-8">	
										<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_DIPLOMATS')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	<logic:equal value="master_private_company" name="TYPE">
		<div id="private-company-edit-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit Private Company</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Private Company's Name :</label>
									<span class="pull-right col-lg-8">
										<html:hidden property="masterId" styleId="id"/>
							          	<html:text property="masterName" styleClass="form-control" styleId="name"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="edit('EDIT_PRIVATE_COMPANY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="edit_master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
</html:form>

<script>

	function edit(identifier)
	
	{
		$('#edit_master_identifier').val(identifier);
		var options = {target:'#masterMsgDiv',url:context+'/admin.html?method=edit_master_data',type:'POST',data: $("#editForm").serialize()}; 
	    $("#editForm").ajaxSubmit(options);

		if(identifier == "EDIT_REGION")
			$('#region-edit-modal').modal('hide');
		else if(identifier == "EDIT_DZONGKHAG")
			$('#dzongkhag-edit-modal').modal('hide');
		else if(identifier == "EDIT_GEWOG")
			$('#gewog-edit-modal').modal('hide');
		else if(identifier == "EDIT_MINISTRY")
			$('#ministry-edit-modal').modal('hide');
		else if(identifier == "EDIT_DEPARTMENT")
			$('#department-edit-modal').modal('hide');
		else if(identifier == "EDIT_COUNTRY")
			$('#country-edit-modal').modal('hide');
		else if(identifier == "EDIT_OFFENCE")
			$('#offence-edit-modal').modal('hide');
		else if(identifier == "EDIT_ACCIDENT")
			$('#accident-edit-modal').modal('hide');
		else if(identifier == "EDIT_ACTION")
			$('#action-edit-modal').modal('hide');
		else if(identifier == "EDIT_DESIGNATION")
			$('#designation-edit-modal').modal('hide');
		else if(identifier == "EDIT_DRIVETYPE")
			$('#drive-edit-modal').modal('hide');
		else if(identifier == "EDIT_ENGINETYPE")
			$('#engine-edit-modal').modal('hide');
		else if(identifier == "EDIT_HYPOTHECATED")
			$('#hypo-edit-modal').modal('hide');
		else if(identifier == "EDIT_OCCUPATION")
			$('#occupation-edit-modal').modal('hide');
		else if(identifier == "EDIT_OWNER")
			$('#owner-edit-modal').modal('hide');
		else if(identifier == "EDIT_COURTESY")
			$('#courtesy-edit-modal').modal('hide');
		else if(identifier == "EDIT_VEHICLE_COMPANY")
			$('#vehicle-company-edit-modal').modal('hide');
		else if(identifier == "EDIT_VEHICLE_MODEL")
			$('#vehicle-model-edit-modal').modal('hide');
		else if(identifier == "EDIT_VEHICLE_TYPE")
			$('#vehicle-type-edit-modal').modal('hide');
		else if(identifier == "EDIT_REGISTRATION_CODE")
			$('#registration-edit-modal').modal('hide');
		else if(identifier == "EDIT_DEALER")
			$('#dealer-edit-modal').modal('hide');
		else if(identifier == "EDIT_REASON")
			$('#reason-edit-modal').modal('hide');
		else if(identifier == "EDIT_BASE_OFFICE")
			$('#base-edit-modal').modal('hide');
		else if(identifier == "EDIT_BLOOD_GROUP")
			$('#blood-edit-modal').modal('hide');
		else if(identifier == "EDIT_PRACTICAL_CRITERIA")
			$('#practical-edit-modal').modal('hide');
		else if(identifier == "EDIT_VILLAGE")
			$('#village-edit-modal').modal('hide');
		else if(identifier == "EDIT_COLOUR")
			$('#colour-edit-modal').modal('hide');
		else if(identifier == "EDIT_PRIVATE_COMPNAY")
			$('#private-company-edit-modal').modal('hide');
		else if(identifier == "EDIT_ROUTE")
			$('#route-edit-modal').modal('hide');
		
        $('#masterMsgDiv').show();
        setTimeout('hideStatus("masterMsgDiv")',2000);
        setTimeout('reloadPage()',2000);
	}

</script>