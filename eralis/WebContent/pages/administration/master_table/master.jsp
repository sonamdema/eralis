<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			Master Table Management
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; master data &amp; their relations
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
		
			<div id="masterMsgDiv" style="display:none"></div>
			
			<logic:equal value="master_region" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#region-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Region</th>
											<th>Description</th>
											<th>Number</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="regionList">
											<logic:iterate id="master" name="regionList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="desc"/>
													</td>
													<td>
														<bean:write name="master" property="number"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','<bean:write name="master" property="number"/>','EDIT_REGION')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_REGION')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a> 
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','<bean:write name="master" property="number"/>','EDIT_REGION')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_REGION')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_dzongkhag" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#dzongkhag-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Dzongkhag</th>
											<th>Region</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="dzongkhagList">
											<logic:iterate id="master" name="dzongkhagList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="otherName"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_DZONGKHAG')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DZONGKHAG')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_DZONGKHAG')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DZONGKHAG')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_gewog" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#gewog-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Gewog</th>
											<th>Dzongkhag</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="gewogList">
											<logic:iterate id="master" name="gewogList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="otherName"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_GEWOG')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_GEWOG')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_GEWOG')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_GEWOG')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_ministry" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#ministry-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Ministry</th>
											<th>Description</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="ministryList">
											<logic:iterate id="master" name="ministryList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="desc"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_MINISTRY')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_MINISTRY')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_MINISTRY')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_MINISTRY')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_department" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#department-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Department</th>
											<th>Ministry</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="departmentList">
											<logic:iterate id="master" name="departmentList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="otherName"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_DEPARTMENT')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DEPARTMENT')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_DEPARTMENT')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DEPARTMENT')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_country" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#country-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Country</th>
											<th>Nationality</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="countryList">
											<logic:iterate id="master" name="countryList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="desc"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_COUNTRY')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_COUNTRY')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_COUNTRY')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_COUNTRY')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_offence" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#offence-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Offence</th>
											<th>Amount</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="offenceList">
											<logic:iterate id="master" name="offenceList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="number"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','<bean:write name="master" property="number"/>','EDIT_OFFENCE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_OFFENCE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','<bean:write name="master" property="number"/>','EDIT_OFFENCE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_OFFENCE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_accident" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#accident-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Accident Cause</th>
											<th>Accident Type</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="accidentList">
											<logic:iterate id="master" name="accidentList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<logic:equal value="DE" name="master" property="desc">
															Driver error
														</logic:equal>
														<logic:equal value="RC" name="master" property="desc">
															Road condition
														</logic:equal>
														<logic:equal value="WC" name="master" property="desc">
															Weather condition
														</logic:equal>
														<logic:equal value="MF" name="master" property="desc">
															Mechnical failure
														</logic:equal>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_ACCIDENT')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_ACCIDENT')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_ACCIDENT')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_ACCIDENT')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_action" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#action-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Action Taken</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="actionList">
											<logic:iterate id="master" name="actionList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_ACTION')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_ACTION')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_ACTION')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_ACTION')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_designation" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#designation-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Designation</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="designationList">
											<logic:iterate id="master" name="designationList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_DESIGNATION')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DESIGNATION')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_DESIGNATION')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DESIGNATION')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_drive" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#drive-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Drive Type</th>
											<th>Drive Type Category</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="driveTypeList">
											<logic:iterate id="master" name="driveTypeList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<logic:equal value="O" name="master" property="desc">
															Ordinary
														</logic:equal>
														<logic:equal value="C" name="master" property="desc">
															Commercial
														</logic:equal>
														<logic:equal value="T" name="master" property="desc">
															Taxi
														</logic:equal>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_DRIVETYPE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DRIVETYPE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_DRIVETYPE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DRIVETYPE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_engine" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#engine-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Engine Type</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="engineTypeList">
											<logic:iterate id="master" name="engineTypeList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_ENGINETYPE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_ENGINETYPE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_ENGINETYPE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_ENGINETYPE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_hypo" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#hypo-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Hypothecated Name</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="hypothecatedList">
											<logic:iterate id="master" name="hypothecatedList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_HYPOTHECATED')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_HYPOTHECATED')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_HYPOTHECATED')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_HYPOTHECATED')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_occupation" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#occupation-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Occupation</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="occupationList">
											<logic:iterate id="master" name="occupationList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_OCCUPATION')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_OCCUPATION')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_OCCUPATION')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_OCCUPATION')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_owner" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#owner-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Owner Type</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="ownerTypeList">
											<logic:iterate id="master" name="ownerTypeList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_OWNER')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_OWNER')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_OWNER')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_OWNER')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_courtesy" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#courtesy-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Owner Type</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="courtesyList">
											<logic:iterate id="master" name="courtesyList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_COURTESY')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_COURTESY')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_COURTESY')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_COURTESY')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_company" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#vehicle-company-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Vehicle Company</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="vehicleCompanyList">
											<logic:iterate id="master" name="vehicleCompanyList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_VEHICLE_COMPANY')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VEHICLE_COMPANY')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_VEHICLE_COMPANY')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VEHICLE_COMPANY')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_model" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#vehicle-model-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Vehicle Model</th>
											<th>Vehicle Company</th>
											<th>Engine CC/ KiloWatt/ Horse Powe/ Gross Vehicle Weight</th>
											<!-- <th>Unladen Weight</th> -->
											<th>Seating Capacity</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="vehicleModelList">
											<logic:iterate id="master" name="vehicleModelList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="otherName"/>
													</td>
													<td>
														<bean:write name="master" property="engineCC"/>
													</td>
													<%-- <td>
														<bean:write name="master" property="unladenWeight"/>
													</td> --%>
													<td>
														<bean:write name="master" property="seatCapacity"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateModalEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="otherId"/>','<bean:write name="master" property="engineCC"/>','<bean:write name="master" property="seatCapacity"/>','<bean:write name="master" property="unladenWeight"/>')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VEHICLE_MODEL')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_VEHICLE_MODEL')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VEHICLE_MODEL')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_vehicle_type" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#vehicle-type-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Vehicle Model</th>
											<th>Vehicle Company</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="vehicleTypeList">
											<logic:iterate id="master" name="vehicleTypeList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="number"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','<bean:write name="master" property="number"/>','EDIT_VEHICLE_TYPE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VEHICLE_TYPE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','<bean:write name="master" property="number"/>','EDIT_VEHICLE_TYPE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VEHICLE_TYPE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_registration_code" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#registration-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Registration Code</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="registrationCodeList">
											<logic:iterate id="master" name="registrationCodeList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_REGISTRATION_CODE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_REGISTRATION_CODE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_REGISTRATION_CODE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_REGISTRATION_CODE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_dealer" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#dealer-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Registration Code</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="dealerList">
											<logic:iterate id="master" name="dealerList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_DEALER')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DEALER')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_DEALER')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DEALER')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_cancellation_reasons" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#reason-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Registration Code</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="reasonsList">
											<logic:iterate id="master" name="reasonsList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_REASON')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_REASON')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_REASON')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_REASON')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_base_office" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#base-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Base Office</th>
											<th>Regional Office</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="baseOfficeList">
											<logic:iterate id="master" name="baseOfficeList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="otherName"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_BASE_OFFICE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_BASE_OFFICE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_BASE_OFFICE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_BASE_OFFICE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_blood_group" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#blood-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Blood Group</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="bloodGroupList">
											<logic:iterate id="master" name="bloodGroupList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_BLOOD_GROUP')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_BLOOD_GROUP')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','','EDIT_BLOOD_GROUP')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_BLOOD_GROUP')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_practical_test_criteria" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#practical-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Criteria</th>
											<th>Full Point</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="practicalCriteriaList">
											<logic:iterate id="master" name="practicalCriteriaList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="number"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','<bean:write name="master" property="number"/>','EDIT_PRACTICAL_CRITERIA')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_PRACTICAL_CRITERIA')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','','<bean:write name="master" property="number"/>','EDIT_PRACTICAL_CRITERIA')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_PRACTICAL_CRITERIA')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			<logic:equal value="master_village" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#village-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Village</th>
											<th>Gewog</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="villageList">
											<logic:iterate id="master" name="villageList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="otherName"/>
													</td>
													<td align="center">
													
														<div class="hidden-sm hidden-xs action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_VILLAGE')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VILLAGE')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
														
														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																</button>
				
																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
				
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','','<bean:write name="master" property="otherId"/>','','EDIT_VILLAGE')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
				
																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_VILLAGE')">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																	
																</ul>
															</div>
														</div>
														
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			<logic:equal value="master_colour" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#colour-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Colour Name</th>
											<th>Colour Description</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="colourList">
											<logic:iterate id="master" name="colourList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="desc"/>
													</td>
													<td align="center">
													
														<div class="action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_COLOUR')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_COLOUR')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			<logic:equal value="master_private_company" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#private-company-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Private Company Name</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="privateCompnayMaster">
											<logic:iterate id="master" name="privateCompnayMaster" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_PRIVATE_COMPANY_MASTER')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
															<%-- <logic:equal value="Y" name="priviledge" property="isDelete">
																<a class="red" href="#" onclick="deleteMasterData('<bean:write name="master" property="id"/>','DELETE_DIPLOMATS')">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</logic:equal> --%>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			<logic:equal value="master_diplomats" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#diplomats-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Diplomat Code</th>
											<th>Diplomat Name</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="diplomatList">
											<logic:iterate id="master" name="diplomatList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td>
														<bean:write name="master" property="desc"/>
													</td>
													<td align="center">
													
														<div class="action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" onclick="populateEditForm('<bean:write name="master" property="id"/>','<bean:write name="master" property="name"/>','<bean:write name="master" property="desc"/>','','','EDIT_DIPLOMATS')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			<logic:equal value="master_route" name="TYPE">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title"><%=request.getAttribute("HEADER") %></h4>
						<logic:equal value="Y" name="priviledge" property="isNew">
							<span class="widget-toolbar">
								<a href="#route-add-modal" title="Add" data-toggle="modal">
									<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</a>
							</span>
						</logic:equal>
					</div>
					<div class="widget-body">
						<div id="messageDiv" style="display:none"></div>
						<div class="widget-main">
						    <div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sl.No.</th>
											<th>Route Name</th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										<logic:notEmpty name="routeMasterList">
											<logic:iterate id="master" name="routeMasterList" type="bt.gov.rsta.eralis.dto.master.MasterDTO" indexId="index">
												<%
													int a = index.intValue();
												%>
												<tr>
													<td align="center"><%=++a %></td>
													<td>
														<bean:write name="master" property="name"/>
													</td>
													<td align="center">
													
														<div class="action-buttons">
															<logic:equal value="Y" name="priviledge" property="isEdit">
																<a class="green" href="#" 
																onclick="populateEditForm('<bean:write name="master" property="id"/>',
																'<bean:write name="master" property="name"/>' ,'','','','ROUTE_EDIT_MODAL')">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>
															</logic:equal>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
			
			
			
		</div>
	</div>
	
	<jsp:include page="/pages/administration/master_table/add-modals.jsp"></jsp:include>
	<jsp:include page="/pages/administration/master_table/edit-modals.jsp"></jsp:include>
	
	<script type="text/javascript">

		var context = "<%=request.getContextPath()%>";
		
		$(document).ready(function() 
		{
		    $('#dynamic-table').DataTable({
		            responsive: true
		    });

		});
		
		function populateModalEditForm(id, name, otherId, engineCC, seatCapacity,unladenWeight)
		{
			$('#id').val(id);
			$('#name').val(name);
			$('#otherId').val(otherId);
			$('#engineCC').val(engineCC);
			$('#seatCapacity').val(seatCapacity);
			$('#unladenWeight').val(unladenWeight);
			$('#vehicle-model-edit-modal').modal('show');
		}
		
		function populateEditForm(id, name, desc, otherId, number, identifier)
		{
			$('#id').val(id);
			$('#name').val(name);
			$('#description').val(desc);
			$('#otherId').val(otherId);
			$('#number').val(number);

			if(identifier == "EDIT_REGION"){
				$('#region-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_DZONGKHAG"){
				$('#dzongkhag-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_GEWOG"){
				$('#gewog-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_MINISTRY"){
				$('#ministry-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_DEPARTMENT"){
				$('#department-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_COUNTRY"){
				$('#country-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_OFFENCE"){
				$('#offence-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_ACCIDENT"){
				$('#accident-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_ACTION"){
				$('#action-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_DESIGNATION"){
				$('#designation-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_DRIVETYPE"){
				$('#drive-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_ENGINETYPE"){
				$('#engine-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_HYPOTHECATED"){
				$('#hypo-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_OCCUPATION"){
				$('#occupation-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_OWNER"){
				$('#owner-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_COURTESY"){
				$('#courtesy-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_VEHICLE_COMPANY"){
				$('#vehicle-company-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_VEHICLE_MODEL"){
				$('#vehicle-model-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_VEHICLE_TYPE"){
				$('#vehicle-type-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_REGISTRATION_CODE"){
				$('#registration-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_DEALER"){
				$('#dealer-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_REASON"){
				$('#reason-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_BASE_OFFICE"){
				$('#base-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_BLOOD_GROUP"){
				$('#blood-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_PRACTICAL_CRITERIA"){
				$('#practical-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_PRACTICAL_CRITERIA"){
				$('#practical-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_VILLAGE"){
				$('#village-edit-modal').modal('show');
			}
			else if(identifier == "EDIT_COLOUR"){
				$('#colour-edit-modal').modal('show');
			} 
			else if(identifier == "EDIT_DIPLOMATS"){
				$('#diplomats-edit-modal').modal('show');
				$("#description option:contains("+desc+")").attr('selected', 'selected');
			} 
			else if(identifier == "EDIT_PRIVATE_COMPANY_MASTER"){
				$('#private-company-edit-modal').modal('show');
			} 
			else if(identifier == "ROUTE_EDIT_MODAL"){
				$('#route-edit-modal').modal('show');
			} 
			
			
		}

		function deleteMasterData(id, identifier)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/admin.html?method=delete_master_data&id="+id+"&typeFlag="+identifier,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#masterMsgDiv").html(responseText);
					$("#masterMsgDiv").show();
					setTimeout('hideStatus("masterMsgDiv")',2000);
					setTimeout('reloadPage()',2000);
				}
			});
		}
		
	</script>