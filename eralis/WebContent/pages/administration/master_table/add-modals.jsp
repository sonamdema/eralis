<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<html:form action="/admin.html" method="post" styleId="addForm">

	<logic:equal value="master_route" name="TYPE">
		<div id="route-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Route</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Route Name:</label>
									<span class="pull-right col-lg-5">
										<html:text property="masterName" styleClass="form-control"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_ROUTE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_diplomats" name="TYPE">
		<div id="diplomats-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Diplomats</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Diplomat's Name :</label>
										<span class="pull-right col-lg-5">
								          	<html:text property="masterDesc" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Diplomats Code:</label>
										<span class="pull-right col-lg-5">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_DIPLOMATS')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	<logic:equal value="master_private_company" name="TYPE">
		<div id="private-company-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Private Company</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Private Company's Name :</label>
									<span class="pull-right col-lg-5">
							          	<html:text property="masterName" styleClass="form-control"></html:text>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_PRIVATE_COMPANY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>

	<logic:equal value="master_colour" name="TYPE">
		<div id="colour-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Colour</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Colour Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Colour Description:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_COLOUR')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	<logic:equal value="master_region" name="TYPE">
		<div id="region-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Region</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Region Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region Description:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region Number:</label>
										<span class="pull-right">
											<html:text property="masterNumber" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_REGION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_dzongkhag" name="TYPE">
		<div id="dzongkhag-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Dzongkhag</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Dzongkhag Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_DZONGKHAG')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_gewog" name="TYPE">
		<div id="gewog-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Gewog</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Gewog Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Dzongkhag:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_GEWOG')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_ministry" name="TYPE">
		<div id="ministry-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Ministry</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Ministry Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Description:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_MINISTRY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_department" name="TYPE">
		<div id="department-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Department</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Department Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Ministry:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="ministryList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_DEPARTMENT')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_country" name="TYPE">
		<div id="country-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Country</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Country Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Ministry:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_COUNTRY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_offence" name="TYPE">
		<div id="offence-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Offence</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Offence Name:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control" style="width: 450px"></html:text>										</span>
									</div>
									<div class="form-group">
										<label>Amount:</label>
										<span class="pull-right">
											<html:text property="masterNumber" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_OFFENCE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_accident" name="TYPE">
		<div id="accident-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Accident Cause</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Accident Cause:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control" style="width: 450px"></html:text>										</span>
									</div>
									<div class="form-group">
										<label>Accident Type:</label>
										<span class="pull-right">
											<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
												<html:option value="">--SELECT--</html:option>
												<html:option value="DE">Driver Error</html:option>
												<html:option value="RC">Road Condition</html:option>
												<html:option value="WC">Weather Condition</html:option>
												<html:option value="MF">Mechanical Failure</html:option>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_ACCIDENT')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_action" name="TYPE">
		<div id="action-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Action Taken</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Action Taken:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>										</span>
									</div>
									<div class="form-group">
										<label>Description:</label>
										<span class="pull-right">
											<html:text property="masterDesc" styleClass="form-control"></html:text>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_ACTION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_designation" name="TYPE">
		<div id="designation-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Designation</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Designation:</label>
										<span class="pull-right">
											<html:text property="masterName" styleClass="form-control"></html:text>										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_DESIGNATION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_drive" name="TYPE">
		<div id="drive-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Drive Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Drive Type:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
								<div class="form-group">
									<label>Drive Type Category:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control">
											<html:option value="">--SELECT--</html:option>
											<html:option value="O">Ordinary</html:option>
											<html:option value="C">Commerical</html:option>
											<html:option value="T">Taxi</html:option>
										</html:select>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_DRIVETYPE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_engine" name="TYPE">
		<div id="engine-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Engine Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Engine Type:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_ENGINETYPE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_hypo" name="TYPE">
		<div id="hypo-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Hypothecated</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Hypothecated:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_HYPOTHECATED')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_occupation" name="TYPE">
		<div id="occupation-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Occupation</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Occupation:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_OCCUPATION')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_owner" name="TYPE">
		<div id="owner-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Owner Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Owner Type:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_OWNER')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_courtesy" name="TYPE">
		<div id="courtesy-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Title of Courtesy</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Title of Courtesy:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_COURTESY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_company" name="TYPE">
		<div id="vehicle-company-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Vehicle Company</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Company:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_VEHICLE_COMPANY')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_model" name="TYPE">
		<div id="vehicle-model-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Vehicle Model</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Model:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
								<div class="form-group">
									<label>Vehicle Company:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="vehicleCompanyList" label="headerName" value="headerId"/>
										</html:select>								
									</span>
								</div>
								<div class="form-group">
									<label>Engine CC/ KiloWatt/ Horse Power /GVW</label>
									<span class="pull-right">
										<html:text property="engineCC" styleClass="form-control"></html:text>										
									</span>
								</div>
								<%-- <div class="form-group">
									<label>Unladen Weight</label>
									<span class="pull-right">
										<html:text property="unladenWeight" styleClass="form-control"></html:text>										
									</span>
								</div> --%>
								<div class="form-group">
									<label>Seating Capacity</label>
									<span class="pull-right">
										<html:text property="seatCapacity" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_VEHICLE_MODEL')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_vehicle_type" name="TYPE">
		<div id="vehicle-type-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Vehicle Type</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Vehicle Type:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
								<div class="form-group">
									<label>Vehicle Type Number:</label>
									<span class="pull-right">
										<html:text property="masterNumber" styleClass="form-control"></html:text>							
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_VEHICLE_TYPE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_registration_code" name="TYPE">
		<div id="registration-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Vehicle Registration Code</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Registration Code:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_REGISTRATION_CODE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_dealer" name="TYPE">
		<div id="dealer-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Vehicle Dealer</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Dealer Name:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control" style="width: 450px;"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_DEALER')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_cancellation_reasons" name="TYPE">
		<div id="reason-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Cancellation Reason</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Reason:</label>
									<span class="pull-right">
										<html:textarea property="masterName" styleClass="form-control" style="margin: 0px; height: 73px; width: 479px;"></html:textarea>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_REASON')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_base_office" name="TYPE">
		<div id="base-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Base Office</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Base Office:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
								<div class="form-group">
									<label>Regional Office:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
										</html:select>
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_BASE_OFFICE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_blood_group" name="TYPE">
		<div id="blood-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Blood Group</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Blood Group:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_BLOOD_GROUP')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_practical_test_criteria" name="TYPE">
		<div id="practical-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Practical Test Criteria</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Criteria:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
								<div class="form-group">
									<label>Full Point:</label>
									<span class="pull-right">
										<html:text property="masterNumber" styleClass="form-control"></html:text>										
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_PRACTICAL_CRITERIA')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
	<logic:equal value="master_village" name="TYPE">
		<div id="village-add-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add Village</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Village:</label>
									<span class="pull-right">
										<html:text property="masterName" styleClass="form-control"></html:text>										
									</span>
								</div>
								<div class="form-group">
									<label>Gewog:</label>
									<span class="pull-right">
										<html:select property="masterOtherId" styleClass="form-control" style="width: 200px">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="gewogList" label="headerName" value="headerId"/>
										</html:select>									
									</span>
								</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
				
						<button type="button" class="btn btn-sm btn-primary" onclick="add('ADD_VILLAGE')">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
						
						<html:hidden property="master_identifier" styleId="master_identifier"/>
					</div>
				</div>
			</div>
		</div>
	</logic:equal>
	
</html:form>

<script>

	function add(identifier)
	{
		$('#master_identifier').val(identifier);
		var options = {target:'#masterMsgDiv',url:context+'/admin.html?method=add_master_data',type:'POST',data: $("#addForm").serialize()}; 
	    $("#addForm").ajaxSubmit(options);

		if(identifier == "ADD_REGION")
			$('#region-add-modal').modal('hide');
		else if(identifier == "ADD_DZONGKHAG")
			$('#dzongkhag-add-modal').modal('hide');
		else if(identifier == "ADD_GEWOG")
			$('#gewog-add-modal').modal('hide');
		else if(identifier == "ADD_MINISTRY")
			$('#ministry-add-modal').modal('hide');
		else if(identifier == "ADD_DEPARTMENT")
			$('#department-add-modal').modal('hide');
		else if(identifier == "ADD_COUNTRY")
			$('#country-add-modal').modal('hide');
		else if(identifier == "ADD_OFFENCE")
			$('#offence-add-modal').modal('hide');
		else if(identifier == "ADD_ACCIDENT")
			$('#accident-add-modal').modal('hide');
		else if(identifier == "ADD_ACTION")
			$('#action-add-modal').modal('hide');
		else if(identifier == "ADD_DESIGNATION")
			$('#designation-add-modal').modal('hide');
		else if(identifier == "ADD_DRIVETYPE")
			$('#drive-add-modal').modal('hide');
		else if(identifier == "ADD_ENGINETYPE")
			$('#engine-add-modal').modal('hide');
		else if(identifier == "ADD_HYPOTHECATED")
			$('#hypo-add-modal').modal('hide');
		else if(identifier == "ADD_OCCUPATION")
			$('#occupation-add-modal').modal('hide');
		else if(identifier == "ADD_OWNER")
			$('#owner-add-modal').modal('hide');
		else if(identifier == "ADD_COURTESY")
			$('#courtesy-add-modal').modal('hide');
		else if(identifier == "ADD_VEHICLE_COMPANY")
			$('#vehicle-company-add-modal').modal('hide');
		else if(identifier == "ADD_VEHICLE_MODEL")
			$('#vehicle-model-add-modal').modal('hide');
		else if(identifier == "ADD_VEHICLE_TYPE")
			$('#vehicle-type-add-modal').modal('hide');
		else if(identifier == "ADD_REGISTRATION_CODE")
			$('#registration-add-modal').modal('hide');
		else if(identifier == "ADD_DEALER")
			$('#dealer-add-modal').modal('hide');
		else if(identifier == "ADD_REASON")
			$('#reason-add-modal').modal('hide');
		else if(identifier == "ADD_BASE_OFFICE")
			$('#base-add-modal').modal('hide');
		else if(identifier == "ADD_BLOOD_GROUP")
			$('#blood-add-modal').modal('hide');
		else if(identifier == "ADD_PRACTICAL_CRITERIA")
			$('#practical-add-modal').modal('hide');
		else if(identifier == "ADD_VILLAGE")
			$('#village-add-modal').modal('hide');
		else if(identifier == "ADD_COLOUR")
			$('#colour-add-modal').modal('hide');
		else if(identifier == "ADD_PRIVATE_COMPANY")
			$('#private-company-add-modal').modal('hide');
		else if(identifier == "ADD_ROUTE")
			$('#route-add-modal').modal('hide');
		
		
		
		
        $('#masterMsgDiv').show();
        setTimeout('hideStatus("masterMsgDiv")',2000);
        setTimeout('reloadPage()',2000);
	}

	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>

	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";


</script>