<%@page import="bt.gov.rsta.eralis.dto.etest.PracticalCriteriaDTO"%>
<%@page import="java.util.List"%>
<%@page import="bt.gov.rsta.eralis.dto.etest.ETestDTO"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/select2.min.css">
<% 	String role = (String) request.getAttribute("ROLE_CODE");
 	String jurisId = (String) request.getAttribute("JURIS_ID");
%>
<form id="candidateListForm" action="<%=request.getContextPath() %>/etest.html" class="form-horizontal">
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			List of Candidate
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				view list of candidates who has booked the driving test
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row" id="messageDiv" style="display:none"></div>
	<div class="row" id="searchDIV">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h5 class="widget-title lighter">Search Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							 <div class="col-lg-12">
							 
							 	<div class="form-group">
							 		<div class="col-lg-1">
						    			<label>Region </label>
						    		</div>
						    		<div class="col-lg-3">
						    			<select class="select2 form-control" id="region" onchange="populateDependentDropDown(this.value,'base','','BASE_OFFICE_LIST','N');getTestDate('REGION')">
						    				<option value="">--SELECT--</option>
							    				<logic:iterate id="region" name="regionList">
												<option value='<bean:write name="region" property="headerId"/>'><bean:write name="region" property="headerName"/></option>
											</logic:iterate>
						    			</select>
						    		</div>
							 		<div class="col-lg-1">
						    			<label>Base </label>
						    		</div>
						    		<div class="col-lg-3">
						    			<select class="select2 form-control" id="base" onchange="getTestDate('BASE')">
						    				<option value="">--SELECT--</option>
						    			</select>
						    		</div>
						    		<div class="col-lg-1">
						    			<label>Test Date </label>
						    		</div>
						    		<div class="col-lg-3">
						    			<select id="testDate" class="select2 form-control" id="testDate" >
						    				<option value="">--SELECT--</option>
						    			</select>
						    		</div>
							 	</div>
							 	
							 	<div class="form-group">
							 		<div class="col-lg-12">
							 			<div class="pull-right">
							 				<button type="button" class="btn btn-primary btn-sm" id="submitBtn" onclick="getCandidateList()">Generate</button>
							 			</div>
							 		</div>
							 	</div>
							 
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="resultDIV"></div>
</form>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>	
<script type="text/javascript" src="<%=request.getContextPath() %>/js/select2.full.min.js"></script>
<script>
	var role = '<%=role%>';
	if(role=='RTO')
	{
		var jurisId = '<%=jurisId%>';
		$('#region').val(jurisId);
		populateDependentDropDown(jurisId,'base','','BASE_OFFICE_LIST','N');
		getTestDate('REGION');
		$('#region').attr('disabled', true);
	}
	
 	function getTestDate(jurisType)
 	{
 		var jurisTypeId =''; 
		var locationId = '';
		var base = $('#base').val();
 		if(base=='0')
 		{
 			var region = $('#region').val();
 			locationId = region;
 			jurisTypeId = 1;
 		}
 		else
 		{
 			locationId = base;
 			jurisTypeId = 2;
 		}
		if(locationId!='')
		{ 
			var url = "<%=request.getContextPath()%>/EralisCommonServlet?q=getTestDate&locationId="+locationId+"&jurisTypeId="+jurisTypeId;
			$.ajax({	
				type: "GET",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data) 
				{
					$("#testDate").empty();
					$("#testDate").append("<option value=''>SELECT TEST DATE</option>");
					for (var i = 0; i < data.length; i++)
					{
						$("#testDate").append("<option value=" + data[i].testDateId + ">"+ data[i].testDate+"</option>");
				    }
				},
				error : function(jqXHR, textStatus, errorThrown) {		
					//alert(textStatus);
				}
			});
		}
		else
		{
			$("#testDate").val('');
		}
	}
 	
 	function getCandidateList(){
		var testDate = $('#testDate').val();
		var jurisId = $('#jurisId').val();
		var jurisTypeId = $('#jurisTypeId').val();
		
		var region = $('#region').val();
		var base = $('#base').val();
		if(base=="0")
		{
			jurisId = region;
			jurisTypeId = 1;
		}
		else
		{
			jurisId = base;
			jurisTypeId = 2;
		}
		
		$.ajax
		({
			type : "POST",
			url : "<%=request.getContextPath()%>/etest.html?method=admin_candidate_list&testDate="+testDate+"&jurisId="+jurisId+"&jurisTypeId="+jurisTypeId,
			data : $('form').serialize(),
			cache : false,
			dataType : "html",
			success : function(responseText) 
			{
				$("#resultDIV").html(responseText);
				if(role=='RTO')
				{
					$(".action").hide();
				}
			}
		});
 	}
</script>


