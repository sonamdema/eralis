<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			Online Payment
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Update Unsuccessful online Payment
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-body">
					<div id="messageDiv" class="alert alert-success" style="display:none">Payment Successfully UPdadted</div>
					<div class="widget-main">
					    <div class="row text-center">
							<form>
								<div class="col-lg-12 form-group">
								    <h4>Update Online Payment</h4>
								</div>
								<div class="col-lg-12 form-group">
								    <div class="col-lg-4 text-right">
									    <label>Application No. <span class="text-danger">*</span>:</label>
									</div>									
								    <div class="col-lg-5">
								    	<input type="text" class="form-control" id="applicationNo" placeholder="Application No.">
   								    	<span class="required-field application-no-error text-danger">Application No. is required</span>
								    </div>
								</div>
								<div class="col-lg-12 form-group">
								    <div class="col-lg-4 text-right">
									    <label>Receipt No. <span class="text-danger">*</span>:</label>
									</div>									
								    <div class="col-lg-5">
								    	<input type="text" class="form-control"  id="receiptNo"  placeholder="Receipt No.">
   								    	<span class=" required-field receipt-no-error text-danger">Reciept No. is required</span>
								    </div>
								</div>
								<div class="col-lg-12 form-group">
								    <div class="col-lg-4 text-right">
									    <label>Receipt Date <span class="text-danger">*</span>:</label>
									</div>									
								    <div class="col-lg-5">
								    	<input type="text" class="form-control date-picker" readonly id="receiptDate"  placeholder="dd/mm/yyyy">
	   								    <span class="required-field receipt-date-error text-danger">Receipt Date is required</span>
								    </div>
								</div>
								<div class="col-lg-12 form-group">
								    <div class="col-lg-4 text-right">
									    <label>Amount (Nu.) <span class="text-danger">*</span>:</label>
									</div>									
								    <div class="col-lg-5">
								    	<input type="text" class="form-control" id="amount"  placeholder="Payment Amount">
 		   								<span class="required-field receipt-amount-error text-danger">Amount is required</span>
								    </div>
								</div>
								<div class="col-lg-12 form-group">
								    <div class="col-lg-12">
                                        <a class="btn btn-success" onclick="updateOnlinePayment()">Update Payment</a>
								    </div>
								</div>
							</form>
							<div id="onlineReceiptResult" class="col-lg-12 row"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<script>



	$(document).ready(function() 
	{
	   $('#dynamic-table').DataTable({
	           responsive: true
	   });
	   $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});
		$(".required-field").hide();

	});

	<%
		String pageIdentifier = (String) request.getAttribute("page_identifier");
		String pageId = (String) request.getAttribute("page_id");
	%>
	
	var pageIdentifier = "<%=pageIdentifier%>";
	var pageId = "<%=pageId%>";
	
	
	
	

	function updateOnlinePayment()
	{
		$(".required-field").hide();
		
		var checkValidation = 0;
		if($("#applicationNo").val()=="")
		{
			checkValidation = 1;
			$(".application-no-error").show();
		}
		if($("#receiptNo").val()=="")
		{
			checkValidation = 1;
			$(".receipt-no-error").show();
		}

		if($("#receiptDate").val()=="")
		{
			checkValidation = 1;
			$(".receipt-date-error").show();
		}
		if($("#amount").val()=="")
		{
			checkValidation = 1;
			$(".receipt-amount-error").show();
		}
		
		if(checkValidation==0)
		{
			
			var appNo = $("#applicationNo").val();
			var receiptNo = $("#receiptNo").val();
			var amount = $("#amount").val();
	
		    var userDate = $("#receiptDate").val();
	        var dataArray = userDate.split("/");
	        var receiptDate = dataArray[2]+"-"+dataArray[1]+"-"+dataArray[0];
		    
			$("#onlineReceiptResult").empty();
			$.ajax
			({
				async: true,
				type: 'POST',
				url : "<%=request.getContextPath()%>/onlineReceipt",
				data : "applicationNo="+appNo+"&paymentDate="+receiptDate+"&txnId="+receiptNo+"&txnAmount="+amount+"&paymentStatus=PAID",
				success: function(result)
				{ 
					$("#onlineReceiptResult").html(result);
				}
			});
		}
	}
	
</script>