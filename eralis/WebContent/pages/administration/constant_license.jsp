<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="panel-body">
	<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Sl</th>
				<th>License Type</th>
				<th>Service Type</th>
				<th>Amount</th>
				<th>License Card Cost</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="LICENSE_CONSTANT_DTLS">
				<logic:iterate id="licenseConstant" name="LICENSE_CONSTANT_DTLS" indexId="index">
				<% int i = index.intValue()+1; %>
					<tr>
						<td><%=i%></td>
						<td><bean:write name="licenseConstant" property="licensetype"/></td>
						<td><bean:write name="licenseConstant" property="serviceType"/></td>
						<td><bean:write name="licenseConstant" property="amount"/></td>
						<td><bean:write name="licenseConstant" property="licenseCardCost"/></td>
						<td>
							<a class="green" href="#" onclick="licenseContantModal('<bean:write name="licenseConstant" property="licenseConstantId"/>','<bean:write name="licenseConstant" property="licensetype"/>','<bean:write name="licenseConstant" property="serviceType"/>','<bean:write name="licenseConstant" property="amount"/>','<bean:write name="licenseConstant" property="licenseCardCost"/>')">									
							 <i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>
						</td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
			<logic:empty name="VEHICLE_CONSTANT_DTLS">
				<tr>
					<td colspan="8" align="center">
						<font color='red'>NO RECORD FOUND</font>
					</td>
				</tr>
			</logic:empty>
		</tbody>
	</table>
</div>
<script>
	function licenseContantModal(licenseConstantId,licensetype,serviceType,amount,licenseCardCost)
	{
		$('#licenseConstantId').val(licenseConstantId);
		$('#licensetype').val(licensetype);
		$('#serviceType').val(serviceType);
		$('#licenseAmount').val(amount);
		$('#licenseCardCost').val(licenseCardCost);
		$('#licenseContantModal').modal('show');
	}
</script>