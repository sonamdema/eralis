<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-users"></i>
			User Management
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; users &amp; their jurisdictions
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header">
					<h4 class="widget-title"></h4>
					<logic:equal value="Y" name="priviledge" property="isNew">
						<span class="widget-toolbar">
							<a href="#" title="Add User" onclick="openModal('add-user-modal')">
								<i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
							</a>
						</span>
					</logic:equal>
				</div>
				<div class="widget-body">
					<div id="messageDiv" style="display:none"></div>
					<div class="widget-main">
					    <div class="table-responsive">
							<table id="dynamic-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Sl.No.</th>
										<th>Login Id</th>
										<th>Name</th>
										<th>Role</th>
										<th>Region</th>
										<th>Dzongkhag</th>
										<th>Base</th>
										<th>Status</th>
										<th></th>
									</tr>
								</thead>
								
								<tbody>
									<logic:notEmpty name="userList">
										<logic:iterate id="user" name="userList" type="bt.gov.rsta.eralis.dto.administration.UserDTO" indexId="index">
											<%
												int a = index.intValue();
											%>
											<tr>
												<td align="center"><%=++a %></td>
												<td>
													<bean:write name="user" property="loginId"/>
												</td>
												<td>
													<bean:write name="user" property="userName"/>
												</td>
												<td>
													<bean:write name="user" property="roleName"/>
												</td>
												<td>
													<bean:write name="user" property="region"/>
												</td>
												<td>
													<bean:write name="user" property="dzongkhag"/>
												</td>
												<td>
													<bean:write name="user" property="baseOffice"/>
												</td>
												<logic:equal value="Y" name="user" property="isActive">
													<td>
														<span class="label label-sm label-success">Active</span>
													</td>
												</logic:equal>
												<logic:equal value="N" name="user" property="isActive">
													<td>
														<span class="label label-sm label-warning">Inactive</span>
													</td>
												</logic:equal>
												<td align="center">
												
													<div class="hidden-sm hidden-xs action-buttons">
														<logic:equal value="Y" name="priviledge" property="isEdit">
															<a class="green" href="#" onclick="populateEditForm('<bean:write name="user" property="loginId"/>','<bean:write name="user" property="userName"/>','<bean:write name="user" property="emailAddress"/>','<bean:write name="user" property="mobileNumber"/>','<bean:write name="user" property="regionId"/>','<bean:write name="user" property="dzongkhagId"/>','<bean:write name="user" property="roleId"/>','<bean:write name="user" property="isActive"/>','<bean:write name="user" property="agencyCode"/>','<bean:write name="user" property="showTaskList"/>','<bean:write name="user" property="jurisType"/>','<bean:write name="user" property="jurisTypeDesc"/>','<bean:write name="user" property="jurisTypeId"/>','<bean:write name="user" property="baseOfficeId"/>')">
																<i class="ace-icon fa fa-pencil bigger-130"></i>
															</a>
														</logic:equal>
														<logic:equal value="Y" name="priviledge" property="isDelete">
															<a class="red" href="#" onclick="userManagement('<bean:write name="user" property="loginId"/>','DELETE_USER','<bean:write name="user" property="emailAddress"/>','<bean:write name="user" property="mobileNumber"/>')">
																<i class="ace-icon fa fa-trash-o bigger-130"></i>
															</a>
														</logic:equal>
														<logic:equal value="Y" name="priviledge" property="isEdit">
															<a class="blue" href="#" onclick="userManagement('<bean:write name="user" property="loginId"/>','RESET_PASSWORD','<bean:write name="user" property="emailAddress"/>','<bean:write name="user" property="mobileNumber"/>')">
																<i class="ace-icon fa fa-key bigger-130"></i>
															</a>
														</logic:equal>
													</div>
													
													<div class="hidden-md hidden-lg">
														<div class="inline pos-rel">
															<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
															</button>
			
															<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																
																<logic:equal value="Y" name="priviledge" property="isEdit">
																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit" onclick="populateEditForm('<bean:write name="user" property="loginId"/>','<bean:write name="user" property="userName"/>','<bean:write name="user" property="emailAddress"/>','<bean:write name="user" property="mobileNumber"/>','<bean:write name="user" property="regionId"/>','<bean:write name="user" property="dzongkhagId"/>','<bean:write name="user" property="roleId"/>','<bean:write name="user" property="isActive"/>','<bean:write name="user" property="agencyCode"/>','<bean:write name="user" property="showTaskList"/>','<bean:write name="user" property="jurisType"/>','<bean:write name="user" property="jurisTypeDesc"/>','<bean:write name="user" property="jurisTypeId"/>','<bean:write name="user" property="baseOfficeId"/>')">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																</logic:equal>
																
																<logic:equal value="Y" name="priviledge" property="isDelete">
																<li>
																	<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete" onclick="userManagement('<bean:write name="user" property="loginId"/>','DELETE_USER','<bean:write name="user" property="emailAddress"/>','<bean:write name="user" property="mobileNumber"/>')">
																		<span class="red">
																			<i class="ace-icon fa fa-trash-o bigger-120"></i>
																		</span>
																	</a>
																</li>
																</logic:equal>
																
																<logic:equal value="Y" name="priviledge" property="isEdit">
																	<li>
																		<a href="#" class="tooltip-info" data-rel="tooltip" title="Reset" onclick="userManagement('<bean:write name="user" property="loginId"/>','RESET_PASSWORD','<bean:write name="user" property="emailAddress"/>','<bean:write name="user" property="mobileNumber"/>')">
																			<span class="blue">
																				<i class="ace-icon fa fa-key bigger-120"></i>
																			</span>
																		</a>
																	</li>
																</logic:equal>
																
															</ul>
														</div>
													</div>
													
												</td>
											</tr>
										</logic:iterate>
									</logic:notEmpty>
									<logic:empty name="userList">
										<tr>
											<td colspan="4" align="center">
												<font color='red'>No data available</font>
											</td>
										</tr>
									</logic:empty>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<html:form action="/admin.html" method="post" styleId="userAddForm">
		<div id="add-user-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Add New User</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div id="submitMsgDiv" style="display: none"></div>
									<div class="form-group">
										<label>Login Id:</label>
										<span class="pull-right">
											<html:text property="loginId" styleClass="form-control" styleId="loginId" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>User Name:</label>
										<span class="pull-right">
											<html:text property="userName" styleClass="form-control" styleId="userName" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Email ID:</label>
										<span class="pull-right">
											<html:text property="emailId" styleClass="form-control" styleId="emailId" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Mobile Number:</label>
										<span class="pull-right">
											<html:text property="mobileNumber" styleClass="form-control" styleId="mobileNumber" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<html:select property="region" styleClass="form-control" style="width: 200px" onchange="populateDependentDropDown(this.value,'dzongkhag','','DZONGKHAG_LIST','N'),populateDependentDropDown(this.value,'base','','BASE_OFFICE_LIST','N')">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Base:</label>
										<span class="pull-right">
											<html:select property="base" styleClass="form-control" style="width: 200px" styleId="base">
												<html:option value="0">--SELECT--</html:option>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Dzongkhag:</label>
										<span class="pull-right">
											<html:select property="dzongkhag" styleClass="form-control" style="width: 200px" styleId="dzongkhag">
												<html:option value="">--SELECT--</html:option>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Jurisdiction Type:</label>
										<span class="pull-right">
											<html:select property="jurisType" styleClass="form-control" style="width: 200px" styleId="jurisType">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="jurisTypeList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Agency Code:</label>
										<span class="pull-right">
											<html:text property="agencyCode" styleClass="form-control" styleId="agencyCode" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Show Tasklist:</label>
										<span class="pull-right">
											<label>
												<html:checkbox property="showTaskList" styleClass="ace" styleId="showTaskList"></html:checkbox>
												<span class="lbl"></span>
											</label>
										</span>
									</div>
									<div class="form-group">
										<label>Role:</label>
										<span class="pull-right">
											<html:select property="roleList" styleClass="form-control" style="width: 200px" multiple="true">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="roleList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
		
						<button type="button" class="btn btn-sm btn-primary" onclick="addUserDtls()">
							<i class="ace-icon fa fa-check"></i> 
							Save
						</button>
					</div>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
	</html:form>
	
	<html:form action="/admin.html" method="post" styleId="userEditForm">
		<div id="edit-user-modal" class="modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="blue bigger">Edit User Details</h4>
					</div>
		
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
									<div id="editSubmitMsgDiv" style="display: none"></div>
									<div class="form-group">
										<label>Login Id:</label>
										<span class="pull-right" id="loginIdSpan">
										</span>
										<html:hidden property="loginId" styleId="editLoginId"/>
									</div>
									<div class="form-group">
										<label>User Name:</label>
										<span class="pull-right">
											<html:text property="userName" styleClass="form-control" styleId="editUserName" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Email ID:</label>
										<span class="pull-right">
											<html:text property="emailId" styleClass="form-control" styleId="editEmailId" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Mobile Number:</label>
										<span class="pull-right">
											<html:text property="mobileNumber" styleClass="form-control" styleId="editMobileNumber" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Region:</label>
										<span class="pull-right">
											<html:select property="region" styleClass="form-control" style="width: 200px" onchange="populateDependentDropDown(this.value,'editDzongkhag','','DZONGKHAG_LIST','N'),populateDependentDropDown(this.value,'editBase','','BASE_OFFICE_LIST','N')" styleId="editRegion">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="regionList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Base:</label>
										<span class="pull-right">
											<html:select property="base" styleClass="form-control" style="width: 200px" styleId="editBase">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="baseOfficeList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Dzongkhag:</label>
										<span class="pull-right">
											<html:select property="dzongkhag" styleClass="form-control" style="width: 200px" styleId="editDzongkhag">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="dzongkhagList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Jurisdiction Type:</label>
										<span class="pull-right">
											<html:select property="jurisType" styleClass="form-control" style="width: 200px" styleId="editJurisType">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="jurisTypeList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
									<div class="form-group">
										<label>Agency Code:</label>
										<span class="pull-right">
											<html:text property="agencyCode" styleClass="form-control" styleId="editAgencyCode" style="width: 200px"></html:text>
										</span>
									</div>
									<div class="form-group">
										<label>Show Tasklist:</label>
										<span class="pull-right">
											<label>
												<html:checkbox property="showTaskList" styleClass="ace" styleId="editShowTaskList"></html:checkbox>
												<span class="lbl"></span>
											</label>
										</span>
									</div>
									<div class="form-group">
										<label>Is Active:</label>
										<span class="pull-right">
											<label>
												<html:checkbox property="isActiveCheckBox" styleClass="ace" styleId="isActive"></html:checkbox>
												<span class="lbl"></span>
											</label>
										</span>
									</div>
									<div class="form-group"> 
										<label>Role:</label>
										<span class="pull-right">
											<html:select property="roleList" styleClass="form-control" style="width: 200px" styleId="editRole" multiple="true">
												<html:option value="">--SELECT--</html:option>
												<html:optionsCollection name="roleList" label="headerName" value="headerId"/>
											</html:select>
										</span>
									</div>
							</div>
						</div>
					</div>
		
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Cancel
						</button>
		
						<button type="button" class="btn btn-sm btn-primary" onclick="editUserDtls()">
							<i class="ace-icon fa fa-check"></i> 
							Update
						</button>
					</div>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
	</html:form>
	
	<script type="text/javascript">

		var context = "<%=request.getContextPath()%>";
		
		$(document).ready(function() 
		{
		    $('#dynamic-table').DataTable({
		            responsive: true
		    });

		    /********************************/
			//add tooltip for small view action buttons in dropdown menu
			$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
			
			//tooltip placement on right or left
			function tooltip_placement(context, source) {
				var $source = $(source);
				var $parent = $source.closest('table');
				var off1 = $parent.offset();
				var w1 = $parent.width();
		
				var off2 = $source.offset();
				//var w2 = $source.width();
		
				if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
				return 'left';
			}
		});

		function editUserDtls(loginId)
		{
			var options = {target:'#editSubmitMsgDiv',url:context+'/admin.html?method=edit_user',type:'POST',data: $("#userEditForm").serialize()}; 
		    $("#userEditForm").ajaxSubmit(options);
	        $('#editSubmitMsgDiv').show();
	        setTimeout('hideStatus("editSubmitMsgDiv")',2000);
	        setTimeout('reloadPage()',2000);
		}

		function populateEditForm(loginId, userName, emailId, mobileNumber, regionId, dzongkhagId, roleId, isActive, agencyCode, showTaskList, jurisType, jurisTypeDesc, jurisTypeId, baseOfficeId)
		{
			$('#loginIdSpan').html("<b>"+loginId+"</b>");
			$('#editLoginId').val(loginId);
			$('#editUserName').val(userName);
			$('#editEmailId').val(emailId);
			$('#editMobileNumber').val(mobileNumber);
			$('#editRegion').val(regionId);
			$('#editDzongkhag').val(dzongkhagId);
			$('#editBase').val(baseOfficeId);
			$('#editJurisType').val(jurisTypeId);
			$('#editAgencyCode').val(agencyCode);

			if(showTaskList == "Y")
				$('#editShowTaskList').attr('checked', true);

			var roleIdArray = new Array();
			roleIdArray = roleId.split(",");
			$('#editRole').val(roleIdArray);

			if(isActive == "Y")
				$('#isActive').attr('checked',true);

			$('#edit-user-modal').modal('show');
		}

		function userManagement(loginId, identifier, emailId, mobileNumber)
		{
			$.ajax
			({
				type : "POST",
				url : "<%=request.getContextPath()%>/admin.html?method=user_management&loginId="+loginId+"&typeFlag="+identifier+"&emailId="+emailId+"&mobileNumber="+mobileNumber,
				data : $('form').serialize(),
				cache : false,
				dataType : "html",
				success : function(responseText) 
				{
					$("#messageDiv").html(responseText);
					$("#messageDiv").show();
					setTimeout('hideStatus("messageDiv")',2000);
					setTimeout('reloadPage()',2000);
				}
			});
		}

		function addUserDtls()
		{
			var options = {target:'#submitMsgDiv',url:context+'/admin.html?method=add_user',type:'POST',data: $("#userAddForm").serialize()}; 
		    $("#userAddForm").ajaxSubmit(options);
	        $('#submitMsgDiv').show();
	        setTimeout('hideStatus("submitMsgDiv")',2000);
	        setTimeout('reloadPage()',2000);
		}

		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>

		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";
		
	</script>