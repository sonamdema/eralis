<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<div class="panel-body">
	<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Sl</th>
				<th>Request Type</th>
				<th>Service Type</th>
				<th>Penalty Per Day</th>
				<th>Max Penalty</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="PENALTY_CONSTANT_DTLS">
				<logic:iterate id="penaltyConstant" name="PENALTY_CONSTANT_DTLS" indexId="index">
				<% int i = index.intValue()+1; %>
					<tr>
						<td><%=i%></td>
						<td><bean:write name="penaltyConstant" property="requestType"/></td>
						<td><bean:write name="penaltyConstant" property="serviceType"/></td>
						<td><bean:write name="penaltyConstant" property="penaltyPerDay"/></td>
						<td><bean:write name="penaltyConstant" property="maxPenalty"/></td>
						<td>
							<a class="green" href="#" onclick="penaltyConstantModal('<bean:write name="penaltyConstant" property="penaltyId"/>','<bean:write name="penaltyConstant" property="requestType"/>','<bean:write name="penaltyConstant" property="serviceType"/>','<bean:write name="penaltyConstant" property="penaltyPerDay"/>','<bean:write name="penaltyConstant" property="maxPenalty"/>')">									
							 <i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>
						</td>
					</tr>
				</logic:iterate>
			</logic:notEmpty>
			<logic:empty name="VEHICLE_CONSTANT_DTLS">
				<tr>
					<td colspan="8" align="center">
						<font color='red'>NO RECORD FOUND</font>
					</td>
				</tr>
			</logic:empty>
		</tbody>
	</table>
</div>
<script>
	function penaltyConstantModal(penaltyId,requestType,serviceType,penaltyPerDay,maxPenalty)
	{
		$('#penaltyId').val(penaltyId);
		$('#penaltyRequestType').val(requestType);
		$('#penaltyServiceType').val(serviceType);
		$('#penaltyPerDay').val(penaltyPerDay);
		$('#maxPenalty').val(maxPenalty);
		$('#penaltyContantModal').modal('show');
	}
</script>