<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="drivetype-suspension-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Drive Type</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Re Issue Date</th>
			<th>Supporting Doc</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="DRIVE_TYPE_SUSPENSION_LIST">
			<logic:iterate id="drivetypesuspension" name="DRIVE_TYPE_SUSPENSION_LIST">
				<tr>
					<td><bean:write name="drivetypesuspension" property="drivetype"/></td>
					<td><bean:write name="drivetypesuspension" property="startDate"/></td>
					<td><bean:write name="drivetypesuspension" property="endDate"/></td> 
					<td><bean:write name="drivetypesuspension" property="reissuedate"/></td> <td>
						<a href="#" onclick="downloadFile('<bean:write name="drivetypesuspension" property="fileUUID" filter="false"/>','<bean:write name="drivetypesuspension" property="fileName" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="drivetypesuspension" property="fileName" filter="false"/>
	  					</a>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>

<script>

	$(document).ready(function() 
	{
	    $('#drivetype-suspension-history-table').DataTable({
	            responsive: true
	    });
	});

</script>


