<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<table id="suspension-history-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Re Issue Date</th>
			<th>Reason For Suspension</th>
			<th>Status</th>
			<th>Suspension Reason</th>
			<th>Supporting Doc</th>
		</tr>
	</thead>
	<tbody>
		<logic:notEmpty name="LICENSE_SUSPENSION_LIST">
			<logic:iterate id="suspension" name="LICENSE_SUSPENSION_LIST">
				<tr>
					<td><bean:write name="suspension" property="startDate"/></td>
					<td><bean:write name="suspension" property="endDate"/></td>
					<td><bean:write name="suspension" property="reissuedate"/></td> 
					<td><bean:write name="suspension" property="reason"/></td>
					<td><bean:write name="suspension" property="status"/></td> 
					<td><bean:write name="suspension" property="reason"/></td> 
					<td>
						<a href="#" onclick="downloadFile('<bean:write name="suspension" property="fileUUID" filter="false"/>','<bean:write name="suspension" property="fileName" filter="false"/>')">
	  						<i class="ace-icon fa fa-file-text red"></i>&nbsp;
	  						<bean:write name="suspension" property="fileName" filter="false"/>
	  					</a>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</tbody>
</table>
<script>


	/* $(document).ready(function() 
	{
	    $('#suspension-history-table').DataTable({
	            responsive: true
	    });
	});
 */
</script>

