<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datepicker.min.css" />

<div class="page-header">
	<h1>
		<i class="ace-icon fa fa-credit-card"></i>
		Driving Type Suspension
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			suspension drive type
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<html:form styleClass="form-horizontal" action="/license.html" styleId="learnerForm">
			<div class="row">
	   		  	<div id="incompleteValidationMsg"></div>
	   		  	<div id="Msg"></div>
	   		</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-lg-2">
							<label class="control-label">License No<span style="color: #ff0000">*</span>:</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<html:text property="licenseNo" styleClass="form-control" styleId="licenseNO" readonly="true"></html:text>
								<html:hidden property="drivinglicenseId" styleClass="form-control" styleId="drivinglicenseId"></html:hidden>
								<span class="input-group-btn">
									<button type="button" class="btn btn-purple btn-sm" onclick="openModal('licenseModal')">
										<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="col-lg-3">
						  	<label style="display:none;color: #ff0000" id="licenseNoValidation">Please Search a License Number</label>
						</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">License Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Status:</label>
									</div>
									<div class="col-lg-4" id="status"></div>
									<div class="col-lg-2">
										<label class="control-label">Issue Date:</label>
									</div>
									<div class="col-lg-4" id="displayissue"></div>
								</div>
								<div class="form-group">
									
									<div class="col-lg-2">
										<label class="control-label">Expiry Date:</label>
									</div>
									<div class="col-lg-4" id="displayexpiry"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Personal Information Details</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label>Citizen ID:</label>
									</div>
									<div class="col-lg-4" id="cid"></div>
									<div class="col-lg-2">
										<label>Name:</label>
									</div>
									<div class="col-lg-4" id="displayOwnerName"></div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label>Date Of Birth:</label>
									</div>
									<div class="col-lg-4" id="displayDob"></div>
								</div>
								<div id="nationalDIV">
									<h4>Permanent Address(National)</h4>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Dzongkhag:</label>
										</div>
										<div class="col-lg-4" id="displayDzongkhag"></div>
										<div class="col-lg-2">
											<label class="control-label">Gewog:</label>
										</div>
										<div class="col-lg-4" id="displayGewog"></div>
									</div>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Village:</label>
										</div>
										<div class="col-lg-4" id="displayVillage"></div>
									</div>
								</div>
								<div id="internationalDIV">
									<h4>Permanent Address(Foreign National)</h4>
									<div class="form-group">
										<div class="col-lg-2">
											<label class="control-label">Country:</label>
										</div>
										<div class="col-lg-3" id="displayCountry">
										</div>
										<div class="col-lg-2">
											<label>Address:</label>
										</div>
										<div class="col-lg-3" id="displayAddress">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Drive Type Suspension Lists</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div id="suspensionlistTable">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="widget-title lighter">Drive Type Suspension Information</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-2">
										<label>Drive Types:</label>
									</div>
									<div class="col-lg-3">
										<html:select property="drivetype"
											styleClass="form-control" styleId="drivetype">
											<html:option value="">--SELECT--</html:option>
											<html:optionsCollection name="DRIVE_TYPE_LIST"
												label="headerName" value="headerId"  /> 
										</html:select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Start Date <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
											<html:text property="startDate"
												styleClass="form-control date-picker"
												styleId="startDate" readonly="true"></html:text>
											<span class="input-group-addon"> <i
												class="fa fa-calendar bigger-110"></i> </span>
										</div>
									</div>
									<div class="col-lg-2">
										<label class="control-label">End Date <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<div class="input-group">
												<html:text property="endDate"
													styleClass="form-control date-picker"
													styleId="endDate" readonly="true"></html:text>
												<span class="input-group-addon"> <i
													class="fa fa-calendar bigger-110"></i> </span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label class="control-label">Reason<span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<html:textarea property="reason" styleId="remarks" styleClass="form-control"></html:textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-2">
										<label> Document <span style="color: #ff0000">*</span> :</label>
									</div>
									<div class="col-lg-3">
										<html:file property="imgPath" styleId="suspenDocumentFile" styleClass="form-control fileupload" onchange="validation('licenseSuspension','suspenDocumentValidation',this)"></html:file>
										<label style="display:none;color: #ff0000" id="suspenDocument">Please attach your file here</label>
										<label style="display:none;color: #ff0000" id="suspenDocumentValidation"></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="displayMsgDiv"></div>
			<div class="pull-right">
				<html:hidden property="customerId" styleId="customerId"/>
				<logic:equal value="Y" name="priviledge" property="isNew">
					<button type="button" class="btn btn-primary btn-sm" id="submitBtn">Submit</button>
				</logic:equal>
			</div>
		</html:form>
	</div>
</div>

<div id="licenseModal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Find/Filter Customer</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<label>Citizen ID:</label>
							<span class="pull-right">
								<input type="text" id="cidPersonalModal" placeholder="CID" style="width: 200px"  />
							</span>
						</div>
						<div class="form-group">
							<label>License No:</label>
							<span class="pull-right">
								<input type="text" id="licenseNoModal" placeholder="License No" style="width: 200px"  />
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" name="search" onclick="searchPersonalInfo()" >
					<i class="ace-icon fa fa-search" ></i> Search
				</button>
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i> Cancel
				</button>
			</div>
			<div id="licenseDetailList">
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript">
		
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
		
				function searchPersonalInfo()
				{
					var cidNumber 	= $('#cidPersonalModal').val();
					var licenseNo	= $('#licenseNoModal').val();
					
					if(cidNumber == "")
						cidNumber = "NA";
					if(licenseNo == "")
						licenseNo = "NA";
					$.ajax
					({
						type : "POST",
						url : "<%=request.getContextPath()%>/common.html?method=getLicesneInfo&cid="+cidNumber+"&licenseNo="+licenseNo+"&searchType=DRIVETYPE_SUSPENSION",
						data : $('form').serialize(),
						cache : false,
						dataType : "html",
						success : function(responseText) 
						{
							$("#licenseDetailList").html(responseText);
							$("#licenseDetailList").show();
						}
					});
				} 

				function searchdrivetypesuspensionlist()
				{
						var drivinglicenseId = $('#drivinglicenseId').val();
						
						if(drivinglicenseId == "")
							drivinglicenseId = "NA";
						
						$.ajax
						({
							type : "POST",
							url : "<%=request.getContextPath()%>/common.html?method=getsearchdrivetypesuspensionlist&drivinglicenseId="+drivinglicenseId,
							
							data : $('form').serialize(),
							cache : false,
							dataType : "html",
							success : function(responseText) 
							{
								$("#suspensionlistTable").html(responseText);
								$("#suspensionlistTable").show();

							}
						});
				}
				
				$(document).ready(function()
				{
					$("#learnerForm").validate(
					{
						rules:
						{
							reason:{
								required:true,
							},
							drivetype:{
								required:true,
							},
							startDate:{
								required:true,
							},
							endDate:{
								required:true,
							},
							suspenDocumentFile:{
								required:true,
							}
							
						},    
						messages:
						{
							reason:{required: "Please Provide a Valid Reason"},
							drivetype:{required: "Please Provide a Drive Type"},
							startDate:{required: "Please Provide a start date"},
							endDate:{required: "Please Provide a end date"},
							suspenDocumentFile:{required: "Please attach your file here"}
							
						}
					});

					$('#submitBtn').click(function()
					{
						var drivinglicenseId = $('#drivinglicenseId').val();
						
						if(drivinglicenseId == "")
						{
							$('#licenseNoValidation').show();
							$('#licenseNoValidation').get(0).scrollIntoView();
							return false;
						}
						else
						{
							if($('#learnerForm').valid()) 
							{
								var options = {target:'#displayMsgDiv',url:context+'/license.html?method=license_drive_type_suspension',type:'POST',data: $("#learnerForm").serialize()}; 
							    $("#learnerForm").ajaxSubmit(options);
						        $('#displayMsgDiv').show();
						        setTimeout('hideStatus("displayMsgDiv")',4000);
							}
							else 
							{
								return false;
							}
						}
					});
				});
</script>
<style>
	#learnerForm .error { color: red; }
</style>
			