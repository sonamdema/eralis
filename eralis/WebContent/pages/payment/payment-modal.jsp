<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<style>
	.datepicker{z-index:1151 !important;}
</style>
<div id="payment-modal" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-large">
								<h3 class="widget-title grey lighter">
									<i class="ace-icon fa fa-money orange"></i>
										Payment Detail
								</h3>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-24">
									<div>
										<table class="table table-striped table-bordered" style="display:none;" id="normal">
											<tbody>
												<tr>
													<td>Amount:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<html:text property="amount" onchange="calTotalAmount()" styleClass="form-control input-mask-phone" styleId="amount"></html:text>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<table class="table table-striped table-bordered" id="endorsementRenewal" style="display: none;">
											<tbody>
												<tr>
													<td>Endorsement Amount:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" name="amount" class="form-control input-mask-phone" id="dlEndorsementAmount" onchange="calEnodrsementRenewalAmount()" />
														</div>
													</td>
												</tr>
												<tr>
													<td>Renewal Amount:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" name="amount" class="form-control input-mask-phone" id="dlRenewalAmount" onchange="calEnodrsementRenewalAmount()" />
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<table class="table table-striped table-bordered" id="transfer" style="display: none;">
											<tbody>
												<tr>
													<td style="width: 200px">Initial Cost Price:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly" onchange="calTotalAmount()"  class="form-control input-mask-phone" id="initialPrice"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Date of Registration:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly"  class="form-control input-mask-phone" id="registrationDate"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Market Value:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly"  onchange="calTotalAmount()"  class="form-control input-mask-phone" id="marketValue"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Sale Deed Value:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly"  onchange="calTotalAmount()"  class="form-control input-mask-phone" id="saleDeedValue"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Value for Transfer Tax:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly"  onchange="calTotalAmount()"  class="form-control input-mask-phone" id="valueForTT"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Ownership Transfer Tax:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly"  onchange="calTotalAmount()"  class="form-control input-mask-phone" id="taxedAmount"/>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width: 200px">Transfer Fee/RC Cost/NOC Fee :</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<input type="text" readonly="readonly" onchange="calTotalAmount()" class="form-control input-mask-phone" id="transferFees"/>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<table class="table table-striped table-bordered" id="penaltyTR">
											<tbody>
												<tr>
													<td>Penalty:</td>
													<td>
														<div class="input-group">
															<span class="input-group-addon">
															   Nu.
															</span>
															<html:text property="penalty" onchange="calTotalAmount()" styleClass="form-control input-mask-phone" styleId="penalty"></html:text>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="hr hr8 hr-double hr-dotted"></div>
									<div class="row">
										<div class="col-sm-5 pull-left">
											<h4 class="pull-left">
												Total amount :
											</h4>
										</div>
										<div class="col-sm-5 pull-right">
											<div class="input-group">
												<span class="input-group-addon">
												   Nu.
												</span>
												<input readonly="readonly" class="form-control input-mask-phone" type="text" id="totalAmount">
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="col-lg-3">
								<label>Receipt No<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-6">
								<html:text property="receiptNo" styleId="receiptNo" styleClass="form-control"></html:text>
								<label id="receiptMsg" class="error">Please enter Receipt No.</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label>Receipt Date<span style="color: #ff0000">*</span>:</label>
							</div>
							<div class="col-lg-6">
								<div class="input-group">
									<html:text property="receiptDate" styleClass="form-control date-picker" styleId="id-date-picker-1"></html:text>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
								<label id="receiptDateMsg" class="error">Please enter Receipt Date</label>
							</div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Cancel
				</button>

				<button type="button" class="btn btn-sm btn-primary" onclick="submitForm()">
					<i class="ace-icon fa fa-check"></i> 
					Submit
				</button>
				<input type="hidden" id="trasacationType" value="normal">
			</div>
			
		</div>
	</div>
</div>

<script>
$("#receiptMsg").hide();
$("#receiptDateMsg").hide();
	function submitForm()
	{
		$("#receiptMsg").hide();
		$("#receiptDateMsg").hide();
		var receiptNo	=	$("#receiptNo").val();
		var receiptDate	=	$("#id-date-picker-1").val();
		if(receiptNo=='')
		{
			$("#receiptMsg").show();
			return false;
		}
		else if(receiptDate=='')
		{
			$("#receiptDateMsg").show();
			return false;
		}
		else
		{
			formSubmit();
			$('#payment-modal').modal('hide');
		}
	}
	function calTotalAmount()
	{
		var amount	=	$("#amount").val();
		var penalty	=	$("#penalty").val();
		var taxedAmount	=	$("#taxedAmount").val();
		var transferFees	=	$("#transferFees").val();
		var trasacationType	=	$("#trasacationType").val();

		
		var totalAmount	="";
		if(amount=="null")
			{amount=0;}
		if(penalty=="null")
			{penalty=0;}
		if(taxedAmount=="null")
			{taxedAmount=0;}
		if(transferFees=="null")
			{transferFees=0;}

		if(dlEndorsementAmount=="null")
			{dlEndorsementAmount=0;}
		if(dlRenewalAmount=="null")
			{dlRenewalAmount=0;}
		
		if(trasacationType=='transfer' || trasacationType=='nocIssuance')
		{
			totalAmount	=	Number(penalty)+Number(taxedAmount)+Number(transferFees);
		}
		else
		{
			totalAmount	=	Number(amount)+Number(penalty);
		}
		$("#totalAmount").val(totalAmount);
	}

</script>