<style>
	td{
		padding: 2px;
	    font-family: sans-serif;
	}
	@media print {
		input#btnPrint {
		display: none;
		}
	}
	@media url {
		input#btnPrint {
			display: none;
		}
	}
	
	.print_btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background: #fff;
    border: 1px solid rgba(0, 0, 0, 0.31);
</style>
<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>
<script src="<%=request.getContextPath() %>/js/jsPDF/jspdf.debug.js"></script>
<script src="<%=request.getContextPath() %>/js/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>

	<%-- <div style="width: 600px;    margin: 0px auto;">
		<div id="receipt">
		<%
			String responseStr = (String) session.getAttribute("RESPONSE_STR");
		
			if(responseStr.equalsIgnoreCase("SUCCESS"))
			{
		%>
			<div style="width: 600px;height:450px" align='center'>
				<div style="z-index:1000;    position: absolute;">
					<table width="600" style="margin-bottom: 26px;" align="center">
						<tr>
							<td>
								<table id="receiptTable" style="width: 600px;">
									<tbody>
										<tr>
											<td colspan="2"><img src="<%=request.getContextPath()%>/images/rsta-logo.png" style="width: 530px;height: 103px; padding-left: 21px;"></td>
										</tr>
										<tr>
											<td colspan="2">
												<p align="center" style="font-size: 20px;font-weight: bold;">
													<u><%=session.getAttribute("Service")%> Acknowledgement</u>
												</p>
											</td>
										</tr>
										<table style="width: 462px;font-size: 18px; margin-left: 128px;">
											<tbody>
											<% 
												String serviceType = (String)session.getAttribute("SERVICE_TYPE");
												if(serviceType.equalsIgnoreCase("Offence"))
												{
											%>
												<tr>
													<td>Tin No:</td><td><%=session.getAttribute("MODULE_NO1")%></td>
												</tr>	
												<tr>
													<td>Vehicle No:</td><td><%=session.getAttribute("MODULE_NO")%></td>
												</tr>	
												<tr>
													<td>Vehicle Type:</td><td><%=session.getAttribute("VEHICLE_TYPE")%></td>
												</tr>	
											<%
												}
												if(serviceType.equalsIgnoreCase("BOOKING"))
												{
											%>
												<tr>
													<td>DL/LL Number:</td><td><%=session.getAttribute("MODULE_NO")%></td>
												</tr>	
												<tr>
													<td>Test Date:</td><td><%=session.getAttribute("CUSTOMER_NAME")%></td>
												</tr>	
												<tr>
													<td>Test Drive Type:</td><td><%=session.getAttribute("REFRENCE_NO")%></td>
												</tr>	
											<%
												}
											%>
											<% 
												if(!serviceType.equalsIgnoreCase("Offence") && !serviceType.equalsIgnoreCase("New Learner")
														&& !serviceType.equalsIgnoreCase("BOOKING"))
												{
											%>
												<tr>
													<td><%=session.getAttribute("SERVICE_TYPE")%> No:</td><td><%=session.getAttribute("MODULE_NO")%></td>
												</tr>	
											<%
												}
											%>
											
											<%
												if(!serviceType.equals("BOOKING")){
											%>
												<tr>
													<td>Name:</td><td><%=session.getAttribute("CUSTOMER_NAME")%></td>
												</tr>	
											<% 
												}
												if(serviceType.equalsIgnoreCase("Vehicle"))
												{
											%>
													<tr>
														<td>Vehicle Type:</td><td><%=session.getAttribute("VEHICLE_TYPE")%></td>
													</tr>	
											<%
												}
											%>
												
												<tr>
													<td>Receipt Number:</td><td><%=session.getAttribute("RECEIPT_NO")%></td>
												</tr>	
												<tr>
													<td>Receipt Date:</td><td><%=session.getAttribute("PAYMENT_DATE")%></td>
												</tr>	
												<tr>
													<td>Amount:</td><td><%=session.getAttribute("TXN_AMOUNT")%></td>
												</tr>	
											<% 
												if(!serviceType.equalsIgnoreCase("Offence") && !serviceType.equalsIgnoreCase("New Learner") 
														&& !serviceType.equalsIgnoreCase("BOOKING"))
												{
											%>
												<tr>
													<td>Valid Upto:</td>
													<td>
														<%=session.getAttribute("VALIDITY")%>
													</td>
												</tr>
												<% 
												if(!serviceType.equalsIgnoreCase("Learner") && !serviceType.equalsIgnoreCase("New Learner")
														&& !serviceType.equalsIgnoreCase("BOOKING"))
												{
												%>	
												<tr>
													<td>Ref:</td>
													<td>
														<%=session.getAttribute("REFRENCE_NO")%>
													</td>
												</tr>
												<tr>
													<td colspan="2"></td>
												</tr>
												<%
												}
												}
												%>
											</tbody>
										</table>
										<% 
												if(serviceType.equalsIgnoreCase("Vehicle") || serviceType.equalsIgnoreCase("License"))
												{
													String referenceNo = (String) session.getAttribute("REFRENCE_NO");
											%>
											<tr align="center">
												<td colspan="2"><img src="<%=request.getContextPath()%>/barcode?data=<%=referenceNo %>" style="width: 515px;height: 40px;"></td>
											</tr>
											<%
												}
											%>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>	
				<div style="z-index:-1000;">
					<img src="<%=request.getContextPath()%>/images/receipt_caption.png" width="600" >
				</div>
			</div>
		</div>
	<%
		}
		else
		{
	%>
		<div class="col-lg-12">
			<div class="alert alert-danger" style="background: #bf4a37; padding: 10px; text-align: center;color: white; font-weight: 500; font-size: 18px; font-family: serif;">
				Transaction could not be completed, please try again later
			</div>
		</div>
		<%
		}
		if(responseStr.equalsIgnoreCase("SUCCESS"))
		{
	%>
		<div align="center">
			<button type="button" class="print_btn" onclick="printDiv('receipt');"><i class="fa fa-print fa-fw"></i> Print</button>
		</div>
	<%
		}
	%>
	</div> --%>
	<div style="width: 600px;    margin: 0px auto;">
		<div id="receipt">
		<%
			String responseStr = (String) session.getAttribute("RESPONSE_STR");
		String service = (String)session.getAttribute("Service");
		String location = (String)session.getAttribute("LOCATION");
		String serviceType = (String)session.getAttribute("SERVICE_TYPE");
			if(responseStr.equalsIgnoreCase("SUCCESS") || responseStr.equalsIgnoreCase("ALREADY_APPLIED"))
			{
		%>
			<div style="width: 600px;height:506px" align='center'>
				<div style="z-index:1000;    position: absolute;">
					<table width="600" style="margin-bottom: 26px;margin: 0px auto;" align="center">
						<tr>
							<td>
								<table id="receiptTable" style="width: 600px; margin: 0px auto;">
									<tbody>
										<tr>
											<td colspan="2"><img src="<%=request.getContextPath()%>/images/rsta-logo.png" style="width: 530px;height: 103px; padding-left: 21px;"></td>
										</tr>
										<tr>
											<td colspan="2">
												<p align="center" style="font-size: 20px;font-weight: bold;">
													<u><%=session.getAttribute("Service")%> Acknowledgement</u>
												</p>
											</td>
										</tr>
										<table style="width: 462px;font-size: 18px; margin: 0px auto;">
											<tbody>
												<tr>
													<td style="width: 150px;">Application No:</td>
													<td>
														<%=session.getAttribute("APPLICATION_NO")%>
													</td>
												</tr>
												<%if(!serviceType.equals("BOOKING")){
													%>
													<tr>
														<td>Name:</td><td><%=session.getAttribute("CUSTOMER_NAME")%></td>
													</tr>	
												<% 
													} %>
											<% 
												
												if(serviceType.equalsIgnoreCase("Offence"))
												{
											%>
												<tr>
													<td>Tin No:</td><td><%=session.getAttribute("MODULE_NO1")%></td>
												</tr>	
												<tr>
													<td>Vehicle No:</td><td><%=session.getAttribute("MODULE_NO")%></td>
												</tr>	
												<tr>
													<td>Vehicle Type:</td><td><%=session.getAttribute("VEHICLE_TYPE")%></td>
												</tr>	
											<%
												}
												if(serviceType.equalsIgnoreCase("BOOKING"))
												{
											%>
												<tr>
													<td>DL/LL Number:</td><td><%=session.getAttribute("MODULE_NO")%></td>
												</tr>	
												<tr>
													<td>Name :</td><td><%=session.getAttribute("CUSTOMER_NAME")%></td>
												</tr>	
												<tr>
													<td>Test Drive Type:</td><td><%=session.getAttribute("REFRENCE_NO")%></td>
												</tr>
												<tr>
													<td>Test Location:</td><td><%=location%></td>
												</tr>
												<tr>
													<td>Test Date:</td><td><%=session.getAttribute("TEST_DATE")%></td>
												</tr>
											<%
												}
											%>
											<% 
												if(!serviceType.equalsIgnoreCase("Offence") && !serviceType.equalsIgnoreCase("New Learner")
														&& !serviceType.equalsIgnoreCase("BOOKING"))
												{
											%>
												<tr>
													<td><%=session.getAttribute("SERVICE_TYPE")%> No:</td><td><%=session.getAttribute("MODULE_NO")%></td>
												</tr>	
											<%
												}
											%>
											
											<%
												if(serviceType.equalsIgnoreCase("Vehicle") )
												{
											%>
													<tr>
														<td>Vehicle Type:</td><td><%=session.getAttribute("VEHICLE_TYPE")%></td>
													</tr>
													<%
														if(!service.equalsIgnoreCase("Vehicle Fitness"))
														{
													%>
													<tr>
														<td>Submitted To:</td><td><%=session.getAttribute("LOCATION")%></td>
													</tr>	
											<%
													}
												}
											%>
											<% 
												if(serviceType.equalsIgnoreCase("License") && !service.equalsIgnoreCase("Fitness"))
												{
											%>
													<tr>
														<td>Submitted To:</td><td><%=session.getAttribute("LOCATION")%></td>
													</tr>	
											<%
												}
												if(serviceType.equalsIgnoreCase("Learner") && !service.equalsIgnoreCase("Fitness"))
												{
											%>
													<tr>
														<td>Submitted To:</td><td><%=session.getAttribute("LOCATION")%></td>
													</tr>	
											<%
												}
											%>
											
												
												<tr>
													<td>Receipt Number:</td><td><%=session.getAttribute("RECEIPT_NO")%></td>
												</tr>	
												<tr>
													<td>Receipt Date:</td><td><%=session.getAttribute("PAYMENT_DATE")%></td>
												</tr>	
												<tr>
													<td>Amount:</td><td>Nu. <%=session.getAttribute("TXN_AMOUNT")%></td>
												</tr>
											<% 
												if(!serviceType.equalsIgnoreCase("Offence") && !serviceType.equalsIgnoreCase("New Learner") 
														&& !serviceType.equalsIgnoreCase("BOOKING"))
												{
											%>
												<tr>
													<td>Valid Upto:</td>
													<td>
														<%=session.getAttribute("VALIDITY")%>
													</td>
												</tr>
												<% 
												if(!serviceType.equalsIgnoreCase("Learner") && !serviceType.equalsIgnoreCase("New Learner")
														&& !serviceType.equalsIgnoreCase("BOOKING"))
												{
												%>	
												<tr>
													<td>Ref:</td>
													<td>
														<%=session.getAttribute("REFRENCE_NO")%>
													</td>
												</tr>
												<tr>
													<td colspan="2"></td>
												</tr>
												<%
												}
												}
												%>
											
												<tr>
													<td colspan="2"></td>
												</tr>
											</tbody>
										</table>
										<% 
												if(serviceType.equalsIgnoreCase("Vehicle") || serviceType.equalsIgnoreCase("License"))
												{
													String referenceNo = (String) session.getAttribute("REFRENCE_NO");
											%>
											<tr align="center">
												<td colspan="2"><img src="<%=request.getContextPath()%>/barcode?data=<%=referenceNo %>" style="width: 515px;height: 40px;"></td>
											</tr>
											<%
												}
											%>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>	
				<div style="z-index:-1000;">
					<img src="<%=request.getContextPath()%>/images/receipt_caption.png" width="600" >
				</div>
			</div>
		
			<% 	if(service.equalsIgnoreCase("Vehicle Renewal"))
			{ %>
			<div class="col-lg-12">
				<div class="alert alert-danger" style="background: #bf4a37; padding: 10px; text-align: center;color: white; font-weight: 500; font-size: 18px; font-family: serif;">
					NOTE:The online  payment you made is only for the registration certificate (blue book) renewal and as such the vehicle must  be physically inspected at any of 
					the RSTA office for Road Worthiness Test (or fitness test)  before the expiry date.
				</div>
			</div>
		
			<% }
			else if (serviceType.equalsIgnoreCase("BOOKING")){%>
				
			<div class="col-lg-12">
				<div class="alert alert-info" style="background: #0e709f; padding: 10px; color: white; font-weight: 500; font-size: 18px; font-family: serif;">
					NOTE: 
					<br>
					<ul>
						<li>Reporting Time:9.00AM.</li>
						<!--<li>Must bring the Learner License and One Day refresher course certificate for New Driving License and original license for endorsement. </li>
						<li>Everyone coming for the test must be in National Dress.</li>
						-->
						<li><a style="color: white; text-decoration: none;" target="_blank" href="http://www.rsta.gov.bt/rstaweb/load.html?id=64&field_cons=PAGE">Documents Required (click here)</a></li>
						
					</ul>
				</div>
			</div>
			<%}%>
		</div>
		<%}
		else
		{
		%>
		<div class="col-lg-12">
			<div class="alert alert-danger" style="background: #bf4a37; padding: 10px; text-align: center;color: white; font-weight: 500; font-size: 18px; font-family: serif;">
				Transaction could not be completed, please try again later
			</div>
		</div>
		<%
		}
		if(responseStr.equalsIgnoreCase("SUCCESS") || responseStr.equalsIgnoreCase("ALREADY_APPLIED"))
		{
	%>
		<div align="center">
			<button type="button" class="print_btn" onclick="printDiv('receipt');"><i class="fa fa-print fa-fw"></i> Print</button>
		</div>
	<%
		}
	%>
	</div>
	<script>
	
		function getDataUri(url, callback) 
		{
		    var image = new Image();
		
		    image.onload = function () {
		        var canvas = document.createElement('canvas');
		        canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
		        canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size
		        canvas.getContext('2d').drawImage(this, 0, 0);
		        callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));
		        callback(canvas.toDataURL('image/png'));
		    };
		
		    image.src = url;
		}
	
		var imageDataUri;
		getDataUri('<%=request.getContextPath()%>/images/rsta-logo.png', function(dataUri) {
			imageDataUri = dataUri;
		});
	
		function downloadReceipt()
		{
			var doc = new jsPDF('p','pt','a4', false);
			var imgData = imageDataUri;
			doc.addImage(imgData, 'PNG', 40, 5, 500, 40);
			var elem = document.getElementById("receiptTable");
			var res = doc.autoTableHtmlToJson(elem);
			doc.autoTable(res.columns, res.data, {theme: 'plain'});
			doc.save("acknowledgement-receipt.pdf");
		}
		function printDiv(divName) {
		     var printContents = document.getElementById(divName).innerHTML;
		     var originalContents = document.body.innerHTML;
	
		     document.body.innerHTML = printContents;
	
		     window.print();
	
		     document.body.innerHTML = originalContents;
		}
	
	</script>
