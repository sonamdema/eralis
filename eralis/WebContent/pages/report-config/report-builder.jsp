<%@page import="java.util.List"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	<div class="page-header">
		<h1>
			<i class="ace-icon fa fa-gears"></i>
			Dynamic Report Builder
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				manage &amp; configure dynamic reports
			</small>
		</h1>
	</div><!-- /.page-header -->
<html:form styleClass="form-horizontal" action="/common.html?method=publishReportQuery" styleId="reportConfiguration">
 	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Basic Information</h4>
						</div>
						<div class="widget-body">
							<div id="messageDiv" style="display:none"></div>
							<div class="widget-main">
							
								<div class="row">
								    <div class="col-lg-12">
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Title</label>
								    		</div>
								    		<div class="col-lg-9">
								    			<html:text styleClass="col-lg-10" property="title"></html:text>
								    		</div>
								    	</div>
								    	<div class="form-group">
								    		<div class="col-lg-3">
								    			<label>Description</label>
								    		</div>
								    		<div class="col-lg-9">
								    			<html:textarea styleClass="col-lg-10" property="description"></html:textarea>
								    		</div>
								    	</div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Datasource</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main">
									<div class="row">
									    <div class="col-lg-12">
									    	<div class="form-group">
										    	<div class="col-lg-1">
										    	Main Table
										    	</div>
									    		<div class="col-lg-4">
									    			<div class="input-group">
														<select class="form-control" id="tableName" onchange="getTableColumns(this.value)">
															<option value="">--SELECT--</option>
															<%
																List<String> tableList = (List<String>)request.getAttribute("tableList");
																for(int i=0; i<tableList.size(); i++)
																{
															%>
																	<option value='<%=tableList.get(i)%>'><%=tableList.get(i) %></option>
															<%
																}
															%>
														</select>
													</div>
									    		</div>
									    		<div class="col-lg-3">
													<button type="button" class="btn btn-success btn-xs" onclick="addRow()">
														<i class="ace-icon fa fa-plus"></i>
													</button>
													<button type="button" class="btn btn-danger btn-xs" onclick="deleteRow()">
														<i class="ace-icon fa fa-minus"></i>
													</button>
												</div>
									    	</div>
									    	<div class="form-group">
									    		<div class="col-lg-12">
									    		
									    			<table border="1" id="dyna" style="border-color: rgba(212, 212, 212, 0.42);" >
									    				<thead>
									    					<tr>
									    						<td align="center" style="padding: 4px;">Join Type</td>
									    						<td align="center" style="padding: 4px;">Join Table</td>
									    						<td align="center" style="padding: 4px;">Join Column</td>
									    						<td align="center" style="padding: 4px;">Join Column</td>
									    					</tr>
									    				</thead>
									    				<tbody>
									    					<tr>
									    						<td>
										    						<select class="form-control" id="joinType1"> 
												    					<option value="join">JOIN</option> 
												    					<option value="left join">LEFT JOIN</option> 
												    					<option value="right join">RIGHT JOIN</option> 
												    				</select>
									    						</td>
									    						<td>
									    							<select class="form-control" id="rightTable_1" onchange="getRightColumns(this.value,this.id)">
																		<option value="">--SELECT--</option>
																		<%
																			for(int i=0; i<tableList.size(); i++)
																			{
																		%>
																				<option value='<%=tableList.get(i)%>'><%=tableList.get(i) %></option>
																		<%
																			}
																		%>
																	</select>
									    						</td>
									    						<td>
									    							<div class="input-group">
																		<span class="input-group-addon">
																			ON
																		</span>
																		<select class="form-control" id="left_column1">
																			<option value="">--SELECT--</option>
																		</select>
																	</div>
									    						</td>
									    						<td>
									    							<div class="input-group">
																		<span class="input-group-addon">
																			Equal
																		</span>
																		<select class="form-control" id="right_column1">
																			<option value="">--SELECT--</option>
																		</select>
																	</div>
									    						</td>
									    					</tr>
									    				</tbody>
									    			</table>
									    		
									    			<!-- <div class="col-lg-2 no-padding">
									    				<select name="jointype[]" class="form-control"> 
									    					<option value="join">JOIN</option> 
									    					<option value="leftjoin">LEFT JOIN</option> 
									    					<option value="rightjoin">RIGHT JOIN</option> 
									    				</select>
									    			</div>
									    			
									    			<div class="col-lg-3">
														<select class="form-control" id="tableName" onchange="getRightColumns(this.value)">
															<option value="">--SELECT--</option>
															<%
																for(int i=0; i<tableList.size(); i++)
																{
															%>
																	<option value='<%=tableList.get(i)%>'><%=tableList.get(i) %></option>
															<%
																}
															%>
														</select>
									    			</div>
									    			
									    			<div class="col-lg-3">
									    				<div class="input-group">
															<span class="input-group-addon">
																ON
															</span>

															<select class="form-control" id="left_column">
																<option value="">--SELECT--</option>
															</select>
														</div>
									    			</div>
									    			
									    			<div class="col-lg-4">
									    				<div class="input-group">
															<span class="input-group-addon">
																Equal
															</span>
															<select class="form-control" id="right_column">
																<option value="">--SELECT--</option>
															</select>
														</div>
									    			</div> -->
									    			
									    		</div>
									    	</div>
									    	
									    </div>
									</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</br>
	</br>
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-4">
						<div class="widget-box" style="height: 400px">
							<div class="widget-header">
								<h4 class="widget-title">Column List Box</h4>
							</div>
							<div class="widget-body" style="auto;height:358px;    overflow: auto;">
								<div class="widget-main" id="columnDIV">
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="widget-box" >
							<div class="widget-header">
								<h4 class="widget-title">Select Column</h4>
							</div>
							<div class="widget-body">
								<textarea class="col-xs-12" style="height: 280px" id="selectColumnList"></textarea>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="widget-box" >
							<div class="widget-header">
								<h4 class="widget-title">Order Column</h4>
							</div>
							<div class="widget-body">
								<textarea class="col-xs-12" style="height: 280px" id="orderColumnList"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Filters</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								
								<div class="row">
									<div class="col-lg-12">
										
										<div class="form-group">
											<div class="input-group">
				    							<span class="input-group-addon">Manage Filter</span> 
												<button type="button" class="btn btn-success btn-xs" onclick="addFilter()">
													<i class="ace-icon fa fa-plus"></i>
												</button>
												<button type="button" class="btn btn-danger btn-xs" onclick="deleteFilter()">
													<i class="ace-icon fa fa-minus"></i>
												</button>
											</div>
											<br>
											<table border="1" id="filterTable">
							    				<thead>
							    					<tr>
							    						<td>Column</td>
							    						<td>Operator</td>
							    						<td>Master Table</td>
							    						<td>Column Id</td>
							    						<td>Column Value</td>
							    					</tr>
							    				</thead>
							    				<tbody>
							    					<tr>
							    						<td>
							    							<div class="input-group">
									    						<select class="form-control" id="sqlcondition1" >
																	<option value="">Select</option>
																</select>
															</div>
							    						</td>
							    						<td><div class="input-group">
								    							<select class="form-control" id='sqlop1' onChange="checkOperator(this.value,this.id)"> 
																	<option value="=">equals</option> 
																	<option value=">">greater than</option> 
																	<option value=">=">greater or equal</option> 
																	<option value="<">less than</option> 
																	<option value="<=">less or equal</option> 
																	<option value="<>">not equal to</option> 
																	<option value="beginswith">begins with</option> 
																	<option value="endswith">ends with</option> 
																	<option value="like">contains</option> 
																	<option value="not like">does not contain</option> 
																	<option value="in">includes</option> 
																	<option value="not in">excludes</option> 
																	<option value="between">between</option> 
																	<option value="not between">not between</option> 
																	<option value="is null">is null</option> 
																	<option value="is not null">is not null</option> 
																</select>
															</div>
							    						</td>
							    						<td>
							    							<div class="input-group">
																<select class="form-control" id="masterTable_1" onchange="getMasterTableCol(this.value,this.id)">
																	<option value="none">--SELECT--</option>
																	<%
																		List<String> tableList1 = (List<String>)request.getAttribute("tableList");
																		for(int i=0; i<tableList.size(); i++)
																		{
																	%>
																			<option value='<%=tableList.get(i)%>'><%=tableList.get(i) %></option>
																	<%
																		}
																	%>
																</select>
															</div>							    						
														</td>
														<td>
							    							<div class="input-group">
																<select class="form-control" id="masterColId_1">
																	<option value="">--SELECT--</option>
																</select>
															</div>							    						
														</td>
														<td>
							    							<div class="input-group">
																<select class="form-control" id="masterColVal_1">
																	<option value="">--SELECT--</option>
																</select>
															</div>							    						
														</td>
							    					</tr>
							    				</tbody>
							    			</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Query</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-3">
							    			<label>Sql Query Preview</label>
							    		</div>
										<div class="form-group col-lg-8">
											<html:textarea  styleClass="col-lg-12" property="sqlQuery" styleId="sqlQuery"></html:textarea>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="col-lg-3">
							    			<label>Sql Filter Preview</label>
							    		</div>
										<div class="form-group col-lg-8">
											<html:textarea  styleClass="col-lg-12" property="whereParam" styleId="whereParam"></html:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			<!--<span class="pull-left">
				<input type="text" id="query" value="SELECT "/>
			</span>
			--><span class="pull-right">
				<button onClick="queryPreview()" type='button' class="btn btn-white btn-default btn-round">
					<i class="ace-icon fa fa-eye red2"></i>
					Preview
				</button>
				&nbsp;
				<button onClick="publishQuery()" type='button' class="btn btn-white btn-info btn-round">
					<i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
					Publish
				</button>
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12" id="displayMsgDiv">
		</div>
	</div>
	
</html:form>
	
	<script type="text/javascript">
	function checkOperator(operator,filterId)
	{
		var split_id = filterId.split('p');
        var filterId=split_id[1];
		if(operator=='between' || operator=='not between')
		{
			$("#sqlAndvalue"+filterId).show();
			$("#sqlShowAnd"+filterId).show();
		}
		else
		{
			$("#sqlAndvalue"+filterId).hide();
			$("#sqlShowAnd"+filterId).hide();
		}
	}
	function getSelectCol(col)
	{
		var selectBox	=	$("#selectColumnList").val();
		
		if(selectBox=="")
		{
			$('#selectColumnList').append(col);
		}
		else
		{
			$('#selectColumnList').append(",\n"+col);
		}
		
	}
	function getOrderCol(col)
	{
		var orderBox	=	$("#orderColumnList").val();
		
		if(orderBox=="")
		{
			$('#orderColumnList').append(col);
		}
		else
		{
			$('#orderColumnList').append(",\n"+col);
		}
	}
		function publishQuery()
		{
			var sqlQuery	=	$("#sqlQuery").val();
			if(sqlQuery!="")
			{
			  	var options = {target:'#displayMsgDiv',url:context+'/common.html?method=publishReportQuery',async:false, type:'POST',data: $("#reportConfiguration").serialize()}; 
			    $("#reportConfiguration").ajaxSubmit(options);
			    $('#displayMsgDiv').show();
			    $("#pendingRemarks").val('');
			}
			else
			{
				alert('Please preview the query');
				return false;
			}
		}
		function queryPreview()
		{	
			//$('#selectDiv > select[name="duallistbox_demo1[]"] > option').each(function() {
			//   alert($(this).val());
			//});
			var mainTable	=	$("#tableName").val();
			var selectCol	=	$("#selectColumnList").val();
			var orderCol	=	$("#orderColumnList").val();
			var query	=	"SELECT "+selectCol+" FROM "+mainTable;
			var joinQuery	=	"";
			var filter	=	" ";
			var sqlQuery	=	"";
			var whereParam	=	"";
			var sqlParam	=	""; 
			for(var i=1;i<=rowCount;i++)
			{
				joinQuery	=	joinQuery+" "+$("#joinType"+i).val()+" "+$("#rightTable_"+i).val()+" ON "+$("#left_column"+i).val()+"="+$("#right_column"+i).val();
			}
			for(var j=1;j<=countFilter;j++)
			{
				sqlParam = $("#sqlcondition"+j).val().split('.');
				if(j==1)
				{
					
					if($("#sqlop"+j).val()=='between' || $("#sqlop"+j).val()=='not between')
					{
						
						whereParam	=	$("#sqlcondition"+j).val()+" "+$("#sqlop"+j).val()+" ? "+" AND ?"+"#"+$("#masterTable_"+j).val()+"#"+$("#masterColId_"+j).val()+"#"+$("#masterColVal_"+j).val();
						filter	=	" WHERE "+sqlParam[0]+"."+sqlParam[1]+" "+$("#sqlop"+j).val()+" ? "+" AND ?";
					}
					else
					{
						whereParam	=	$("#sqlcondition"+j).val()+" "+$("#sqlop"+j).val()+" ?"+"#"+$("#masterTable_"+j).val()+"#"+$("#masterColId_"+j).val()+"#"+$("#masterColVal_"+j).val();
						filter	=	" WHERE "+sqlParam[0]+"."+sqlParam[1]+" "+$("#sqlop"+j).val()+" ?";
					}
				}
				else
				{
					if($("#sqlop"+j).val()=='between' || $("#sqlop"+j).val()=='not between')
					{
						whereParam	=	whereParam	+"~"+$("#sqlcondition"+j).val()+" "+$("#sqlop"+j).val()+" ? "+" AND ?"+"#"+$("#masterTable_"+j).val()+"#"+$("#masterColId_"+j).val()+"#"+$("#masterColVal_"+j).val();
						filter	=	filter+" AND "+sqlParam[0]+"."+sqlParam[1]+" "+$("#sqlop"+j).val()+" ? "+" AND ?";
					}
					else
					{
						whereParam	=	whereParam	+"~"+$("#sqlcondition"+j).val()+" "+$("#sqlop"+j).val()+" ?"+"#"+$("#masterTable_"+j).val()+"#"+$("#masterColId_"+j).val()+"#"+$("#masterColVal_"+j).val();
						filter	=	filter+" AND "+sqlParam[0]+"."+sqlParam[1]+" "+$("#sqlop"+j).val()+" ?";
					}
				}
			}
			if($("#rightTable_1").val()=="")
			{
				joinQuery="";
			}
			if($("#sqlcondition1").val()=="")
			{
				filter="";
			}
			if(orderCol!="")
			{
				orderCol=" ORDER BY "+orderCol;
			}
			sqlQuery=query+" "+joinQuery+" "+filter+orderCol;
			$("#sqlQuery").val(sqlQuery);
			$("#whereParam").val(whereParam);
			
		}
		function getTableColumns(tableName)
		{
			$('#sqlcondition1').empty();
			$('#left_column1').empty();
			$('#left_column1').append("<option value=''>Select</option>");
			$('#selectColumn').empty();
			$('#orderByColumn').empty();
			$('#left_column').empty();
			$('#sqlcondition1').append("<option value=''>Select</option>");
			$("#mainTableCol").remove();
			$("#mainTableCol").html('');
			var url = context+'/admin.html?method=getColumnList&tableName='+tableName;
			$.ajax({	
				type: "POST",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data)
				{
					
					var list = $("#columnDIV").append('<ul style="margin-left: 0px;margin-top: 36px;" id="mainTableCol"></ul>').find('#mainTableCol');
					for (var i = 0; i < data.length; i++) 
					{
						$('#left_column1').append("<option value="+tableName+"." + data[i].headerName +">" +tableName+" > " + data[i].headerName + "</option>");
						if(data[i].headerId == "INTEGER"){
							list.append("<li class='list-group-item' style='padding: 10px 0px;border:none;' id='sortable1'><div class='btn-group dropup' style='width:100%'><button style='width: 358px;' data-toggle='dropdown' class='btn btn-xs btn-primary dropdown-toggle'type='button'><i class='fa fa-sort-numeric-asc'></i>&nbsp;" + tableName+" > "+data[i].headerName + "</button><ul class='dropdown-menu dropdown-info'><li><a  id='" + tableName+"."+data[i].headerName + "' onClick='getSelectCol(this.id)'>Select Column</a></li><li><a   id='" + tableName+"."+data[i].headerName + "' onClick='getOrderCol(this.id)'>Order Column</a></li></ul></div></li>");
						}
						else if(data[i].headerId == "DATE" ||
								data[i].headerId == "DATETIME" ||
								data[i].headerId == "TIMESTAMP")
						{
							list.append("<li class='list-group-item' style='padding: 10px 0px;border:none;' ><div class='btn-group dropup' style='width:100%'><button data-toggle='dropdown' style='width: 358px;'  class='btn btn-xs btn-primary dropdown-toggle'type='button'><i class='fa fa-calendar'></i>&nbsp;"  + tableName+" > "+ data[i].headerName + "</button><ul class='dropdown-menu dropdown-info'><li><a  id='" + tableName+"."+data[i].headerName + "' onClick='getSelectCol(this.id)'>Select Column</a></li><li><a   id='" + tableName+"."+data[i].headerName + "' onClick='getOrderCol(this.id)'>Order Column</a></li></ul></div></li>");
						}
						else
						{
							list.append("<li class='list-group-item' style='padding: 10px 0px;border:none;' ><div class='btn-group dropup' style='width:100%'><button data-toggle='dropdown' style='width: 358px;'  class='btn btn-xs btn-primary dropdown-toggle'type='button'><i class='fa fa-font'></i>&nbsp;"  + tableName+" > "+ data[i].headerName + "</button><ul class='dropdown-menu dropdown-info'><li><a  id='" + tableName+"."+data[i].headerName + "' onClick='getSelectCol(this.id)'>Select Column</a></li><li><a    id='" + tableName+"."+data[i].headerName + "' onClick='getOrderCol(this.id)'>Order Column</a></li></ul></div></li>");
						}
						$('#sqlcondition1').append("<option value=" +tableName+"." + data[i].headerName +"."+data[i].headerId+">" +tableName+" > " + data[i].headerName + "</option>");
						$('#left_column').append("<option value=" +tableName+"." + data[i].headerName +">" +tableName+" > " + data[i].headerName + "</option>");
						$('#selectColumn').append("<option value=" +tableName+"." + data[i].headerName +">" +tableName+" > " + data[i].headerName + "</option>");
						$('#orderByColumn').append("<option value=" +tableName+"." + data[i].headerName +">" +tableName+" > " + data[i].headerName + "</option>");
				    }
					tableColumns();
				},
				error : function(jqXHR, textStatus, errorThrown) {		
				}
			});
		}
		function getRightColumns(tableName,joinTableId)
		{
			var split_id = joinTableId.split('_');
	        var joinTableId=split_id[1];
		  	$('#right_column'+joinTableId).empty();
			$("#joinTable"+joinTableId).remove();
			$("#joinTable"+joinTableId).html('');
			
			var url = context+'/admin.html?method=getColumnList&tableName='+tableName;
			$.ajax({	
				type: "POST",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data) 
				{
					$('#right_column'+joinTableId).append("<option value=''>Select</option>");
					var list = $("#columnDIV").append('<ul style="margin-left: 0px;margin-top: 0px;"  id="joinTable'+joinTableId+'"></ul>').find('#joinTable'+joinTableId+'');
					for (var i = 0; i < data.length; i++) 
					{
						if(data[i].headerId == "INTEGER"){
							list.append("<li class='list-group-item' style='padding: 10px 0px;border:none;'  id='sortable'><div class='btn-group dropup' style='width:100%'><button data-toggle='dropdown' style='width: 358px;'  class='btn btn-xs btn-primary dropdown-toggle'type='button'><i class='fa fa-sort-numeric-asc'></i>&nbsp;" + tableName+" > "+data[i].headerName + "</button><ul class='dropdown-menu dropdown-info'><li><a  id='" + tableName+"."+data[i].headerName + "' onClick='getSelectCol(this.id)'>Select Column</a></li><li><a   id='" + tableName+"."+data[i].headerName + "' onClick='getOrderCol(this.id)'>Order Column</a></li></ul></div></li>");
						}
						else if(data[i].headerId == "DATE" ||
								data[i].headerId == "DATETIME" ||
								data[i].headerId == "TIMESTAMP")
						{
							list.append("<li class='list-group-item' style='padding: 10px 0px;border:none;' ><div class='btn-group dropup' style='width:100%'><button data-toggle='dropdown' style='width: 358px;'  class='btn btn-xs btn-primary dropdown-toggle'type='button'><i class='fa fa-calendar'></i>&nbsp;"  + tableName+" > "+ data[i].headerName + "</button><ul class='dropdown-menu dropdown-info'><li><a  id='" + tableName+"."+data[i].headerName + "' onClick='getSelectCol(this.id)'>Select Column</a></li><li><a   id='" + tableName+"."+data[i].headerName + "' onClick='getOrderCol(this.id)'>Order Column</a></li></ul></div></li>");
						}
						else
						{
							list.append("<li class='list-group-item' style='padding: 10px 0px;border:none;' ><div class='btn-group dropup' style='width:100%'><button data-toggle='dropdown' style='width: 358px;'  class='btn btn-xs btn-primary dropdown-toggle'type='button'><i class='fa fa-font'></i>&nbsp;"  + tableName+" > "+ data[i].headerName + "</button><ul class='dropdown-menu dropdown-info'><li><a  id='" + tableName+"."+data[i].headerName + "' onClick='getSelectCol(this.id)'>Select Column</a></li><li><a    id='" + tableName+"."+data[i].headerName + "' onClick='getOrderCol(this.id)'>Order Column</a></li></ul></div></li>");
						}
						
						$('#right_column'+joinTableId).append("<option value=" +tableName+"."+ data[i].headerName +">" + tableName+" > "+data[i].headerName + "</option>");
						$('#sqlcondition1').append("<option value=" +tableName+"."+ data[i].headerName +">" +tableName+" > "+ data[i].headerName + "</option>");
				 	}
					
				},
				error : function(jqXHR, textStatus, errorThrown) {		
				}
			});
			 
		}
		function getMasterTableCol(tableName,filterId)
		{
			var split_id = filterId.split('_');
	        var filterId=split_id[1];
		  	$('#masterColId_'+filterId).empty();
		  	$('#masterColVal_'+filterId).empty();
			
			var url = context+'/admin.html?method=getColumnList&tableName='+tableName;
			$.ajax({	
				type: "POST",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data) 
				{
					$('#masterColId_'+filterId).append("<option value=''>Select</option>");
					$('#masterColVal_'+filterId).append("<option value=''>Select</option>");
					for (var i = 0; i < data.length; i++) 
					{
						$('#masterColId_'+filterId).append("<option value=" +tableName+"."+ data[i].headerName +">" +data[i].headerName + "</option>");
						$('#masterColVal_'+filterId).append("<option value=" +tableName+"."+ data[i].headerName +">" +data[i].headerName + "</option>");
					 }
					
				},
				error : function(jqXHR, textStatus, errorThrown) {		
				}
			});
			 
		}
	  var rowCount=1;
	  function addRow()
	  {
		var table = $("#dyna");
	    // clone the last row in the table
	    var $tr = $(table).find("tbody tr:last").clone();
	   
	    // flushing out the values selected in the previous row
	    $tr.find('#joinType'+rowCount).val('join');
	    $tr.find('#rightTable_'+rowCount).val('');
	    $tr.find('#left_column'+rowCount).val('');
	    $tr.find('#right_column'+rowCount).val('');
	    
	    // get the name attribute for the input and select fields
	    $tr.find("input,select,img,textarea").attr("name", function()
	    {
	      // break the field name and it's number into two parts
	      var parts = this.id.match(/(\D+)(\d+)$/);
	 
	      // create a unique name for the new field by incrementing
	      // the number for the previous field by 1
	      return parts[1] + ++parts[2];
	    // repeat for id attributes
	    }).attr("id", function()
	    {
	      var parts = this.id.match(/(\D+)(\d+)$/);
	      return parts[1] + ++parts[2];
	    });
	         
	    // append the new row to the table
	    $(table).find("tbody tr:last").after($tr);
	    rowCount++;
	    addJoinCol(rowCount);
	  }
	  function deleteRow()
	  {
		  if(rowCount > 1)
		  {
			  $('#dyna tr:last').remove();
			  rowCount--;
		  }
		  else
		  {
			alert('Atleast one row is required');
		  }
	  }
	  function addJoinCol(rowId)
		{
			var lastRowId	=	rowId-1;
			var tableName	=	$("#rightTable_"+lastRowId).val();
			 
			var url = context+'/admin.html?method=getColumnList&tableName='+tableName;
			$.ajax({
				type: "POST",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(data)
				{
					$('#columnDIV').append("<ul class='list-group container'>");
					for (var i = 0; i < data.length; i++) 
					{
						$('#left_column'+rowId).append("<option value="+tableName+"." + data[i].headerName +">" +tableName+" > " + data[i].headerName + "</option>");
				    }
				},
				error : function(jqXHR, textStatus, errorThrown) {		
				}
			});
		}
	   
	  var countFilter=1;
	  function addFilter()
	  {
		var table = $("#filterTable");
		
	    // clone the last row in the table
	    var $tr = $(table).find("tbody tr:last").clone();
	   
	    // flushing out the values selected in the previous row
	    $tr.find('#sqlcondition'+countFilter).val('join');
	    $tr.find('#sqlop'+countFilter).val('');
	    $tr.find('#sqlvalue'+countFilter).val('');
	    $tr.find('#sqlAndvalue'+countFilter).val('');
	    $tr.find('#sqlShowAnd'+countFilter).val();
	    
	    
	    // get the name attribute for the input and select fields
	    $tr.find("input,select,img,textarea").attr("name", function()
	    {
	      // break the field name and it's number into two parts
	      var parts = this.id.match(/(\D+)(\d+)$/);
	 
	      // create a unique name for the new field by incrementing
	      // the number for the previous field by 1
	      return parts[1] + ++parts[2];
	    // repeat for id attributes
	    }).attr("id", function()
	    {
	      var parts = this.id.match(/(\D+)(\d+)$/);
	      return parts[1] + ++parts[2];
	    });
	         
	    // append the new row to the table
	    $(table).find("tbody tr:last").after($tr);
	    countFilter++;
	  }
	  function deleteFilter()
	  {
		  if(countFilter > 1)
		  {
			  $('#filterTable tr:last').remove();
			  countFilter--;
		  }
		  else
		  {
			alert('Atleast one row is required');
		  }
	  }

		<%
			String pageIdentifier = (String) request.getAttribute("page_identifier");
			String pageId = (String) request.getAttribute("page_id");
		%>
	
		var pageIdentifier = "<%=pageIdentifier%>";
		var pageId = "<%=pageId%>";

</script>
<script type="text/javascript">
	function tableColumns()
	{
		jQuery(function($){
		    var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
			var container1 = demo1.bootstrapDualListbox('getContainer');
			container1.find('.btn').addClass('btn-white btn-info btn-bold');
		
			/**var setRatingColors = function() {
				$(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
				$(this).find('.star-off-png').removeClass('orange2').addClass('grey');
			}*/
			
			//.find('i:not(.star-raty)').addClass('grey');
			
			
			
			//////////////////
			//select2
			$('.select2').css('width','200px').select2({allowClear:true})
			$('#select2-multiple-style .btn').on('click', function(e){
				var target = $(this).find('input[type=radio]');
				var which = parseInt(target.val());
				if(which == 2) $('.select2').addClass('tag-input-style');
				 else $('.select2').removeClass('tag-input-style');
			});
			
			//////////////////
			$('.multiselect').multiselect({
			 enableFiltering: true,
			 buttonClass: 'btn btn-white btn-primary',
			 templates: {
				button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
				ul: '<ul class="multiselect-container dropdown-menu"></ul>',
				filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
				filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
				li: '<li><a href="javascript:void(0);"><label></label></a></li>',
				divider: '<li class="multiselect-item divider"></li>',
				liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
			 }
			});
			
			
			///////////////////
				
			//typeahead.js
			//example taken from plugin's page at: https://twitter.github.io/typeahead.js/examples/
			var substringMatcher = function(strs) {
				return function findMatches(q, cb) {
					var matches, substringRegex;
				 
					// an array that will be populated with substring matches
					matches = [];
				 
					// regex used to determine if a string contains the substring `q`
					substrRegex = new RegExp(q, 'i');
				 
					// iterate through the pool of strings and for any string that
					// contains the substring `q`, add it to the `matches` array
					$.each(strs, function(i, str) {
						if (substrRegex.test(str)) {
							// the typeahead jQuery plugin expects suggestions to a
							// JavaScript object, refer to typeahead docs for more info
							matches.push({ value: str });
						}
					});
		
					cb(matches);
				};
			 };
		
			 $('input.typeahead').typeahead({
				hint: true,
				highlight: true,
				minLength: 1
			 }, {
				name: 'states',
				displayKey: 'value',
				source: substringMatcher(ace.vars['US_STATES'])
			 });
			//in ajax mode, remove remaining elements before leaving page
			$(document).one('ajaxloadstart.page', function(e) {
				$('[class*=select2]').remove();
				$('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
				$('.rating').raty('destroy');
				$('.multiselect').multiselect('destroy');
			});
		});
	}
</script>