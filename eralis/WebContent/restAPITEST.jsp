
<!DOCTYPE html>
<html lang="en">
<head>
		<title>Login - eRaLIS</title>
</head>
<body class="login-layout">
		<div class="main-container">
			<select Id="vehicleCompany"></select>
        </div>
		<script src="<%=request.getContextPath() %>/js/jquery.2.1.1.min.js"></script>
		<script>
		function populateDependentDropDown()
		{
			var url = "http://localhost:8181/StudentRestAPI/api/student/dropDownList?dropDownType=gewog&typeId=2";
			$.ajax({
				type: "GET",
				url : url,
				cache: false,
				async: false,
				dataType : "json",
				success : function(	data) 
				{
						for (var i = 0; i < data.length; i++) {
							$("#vehicleCompany").append("<option value=" + data[i].Gewog_Id + ">"+ data[i].Gewog_Name + "</option>");
					    }
						
				}
			});
		}
		populateDependentDropDown();
		
		</script>
	</body>
</html>