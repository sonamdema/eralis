	function hideStatus(id)
	{
		$('#'+id).hide();
	}
	
	function reloadPage()
	{	
		$('#contentDisplayDiv').load(context+"/redirect.html?q="+pageIdentifier+"&page_id="+pageId);
	}
	
	function showTaskList()
	{
		window.location = context+"/tasklist.html";
	}
	
	//FOR ONLINE VEHICLE RENEWAL SERVICE
	var vehicleAmount = 0; 
	var vehiclePrevAmount = 0; 
	var latestRenewalDuration = 0; 
	var previousDuration = 0; 
	
	function getPaymentDetails(requestType, serviceType, identityNo, identityTypeId, loadingCapacity, seatingCapacity, vehicleHP, kilowatts, engineCC, purchaseDate, saleDeedAmount, saleDeedDate, renewalDuration, registrationCode)
	{
		$("#amount").val('0');
		var isEndorsementRenewal = "N"; 
		if(serviceType=="ENDORSEMENT_RENEWAL")
		{
			isEndorsementRenewal = "Y";
			serviceType = "RENEWAL";
		}
		$.ajax({
			async: true,
			type: 'GET',
			url: context+'/EralisCommonServlet?q=getPaymentDetails&requestType='+requestType+'&serviceType='+serviceType+'&identityNo='+identityNo+'&identityTypeId='+identityTypeId+'&loadingCapacity='+loadingCapacity+'&seatingCapacity='+seatingCapacity+'&vehicleHP='+vehicleHP+'&kilowatt='+kilowatts+'&engineCC='+engineCC+'&purchaseDate='+purchaseDate+'&saleDeedAmount='+saleDeedAmount+'&saleDeedDate='+saleDeedDate+'&renewalDuration='+renewalDuration+'&registrationCode='+registrationCode,	
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function()
				{
					var status = $(this).find('status').text();
					var penaltyFlag = $(this).find('penalty-flag').text();
					var amount = $(this).find('amount').text();
					var penalty = $(this).find('penalty').text();
					var totalAmount = $(this).find('total').text();
					var marketValue = $(this).find('marketValue').text();
					var saleDeedValue = $(this).find('saleDeedValue').text();
					var valueForTT = $(this).find('valueForTT').text();
					var taxedAmount = $(this).find('taxedAmount').text();
					var transferFees = $(this).find('transferFees').text();
					var initialprice = $(this).find('initial-price').text();
					var registrationdate = $(this).find('registration-date').text();
					var cardCost = $(this).find('cardCost').text(); 
					var ODLRenewalCost = $(this).find('ODLRenewalCost').text(); 
					var PDRenewalCost = $(this).find('PDRenewalCost').text(); 
					
					
					vehicleAmount = amount; 

					$("#perviousPaymentDiv").hide();
					if(vehiclePrevAmount!=0)
					{
						if(vehiclePrevAmount!=vehicleAmount && previousDuration==latestRenewalDuration)
						{
							$("#perviousPaymentDiv").show();
							$("#perviousPaymentDiv").html("The payment amount do not tally with previous amount : <span class='bolder'>Nu."+vehiclePrevAmount+"</span>. Contact nearest RSTA office for payment verfication <a href='https://eralis.rsta.gov.bt/eralis/public/serviceredirection.html?q=vehicle_renewal_service' class='btn btn-danger'>Back</a>");
						}
					}
					
					if(status == "SUCCESS")
					{
						if(penaltyFlag == "FALSE")
							$('#penaltyTR').hide();
						
						if(isEndorsementRenewal=="Y")
						{
							var endorsementAmount =  $('#amount').val();
							totalAmount = Number(totalAmount) + (Number(endorsementAmount)-100);
							amount = Number(amount)-100;
							$('#amount').val(totalAmount);
							$("#dlEndorsementAmount").val(endorsementAmount);
							$("#dlRenewalAmount").val(amount);
							$('#penalty').val(penalty);
							$('#totalAmount').val(totalAmount);
							$('#cardCost').val(cardCost);
							$('#ODLRenewalCost').val(ODLRenewalCost);
							$('#PDRenewalCost').val(PDRenewalCost);
							
						}
						else
						{
							$('#amount').val(amount);
							$('#penalty').val(penalty);
							$('#totalAmount').val(totalAmount);
							$('#cardCost').val(cardCost);
							$('#ODLRenewalCost').val(ODLRenewalCost);
							$('#PDRenewalCost').val(PDRenewalCost);
						}
						
							if(requestType == "VEHICLE" && serviceType == "TRANSFER")
						{
							$('#marketValue').val(marketValue);
							$('#saleDeedValue').val(saleDeedValue);
							$('#valueForTT').val(valueForTT);
							$('#taxedAmount').val(taxedAmount);
							$('#transferFees').val(transferFees);
							$('#initialPrice').val(initialprice);
							$('#registrationDate').val(registrationdate);
							
							$('#normal').hide();
							$('#transfer').show();
						}
						else
						{
							if(isEndorsementRenewal=="Y")
							{
								$('#endorsementRenewal').show();
								$('#normal').hide();
								$('#transfer').hide();
							}
							else
							{
								$('#normal').show();
								$('#transfer').hide();
								$('#endorsementRenewal').hide();
							}
							
						}
						if($("#trasacationType").val()=='nocIssuance')
						{
							$("#transferFees").val($("#nocFees").val());
							totalAmount	=	Number(taxedAmount)+Number($("#nocFees").val())+Number(penalty);
							$('#totalAmount').val(totalAmount);
						}
						showConfirmationDialog();
					}
				});
			}
		});
	}
	
	function showConfirmationDialog()
	{
		$('#payment-modal').modal({backdrop: 'static', keyboard: false});
	}
	
	var previousCidNumber;
	function getCidDetails()
	{
		$('#submitBtn').show();
		$('#updateBtn').hide();
		var value = $('#cid').val();
		
		var manualFlag = $('#manualFlag').val();
		$('#msgDiv').html("");
		$('#msgDiv').hide();
		
		if("" == value)
		{
			$('#msgDiv').html("<div class='alert alert-danger'>Please enter a valid CID Number</div>");
			$('#errorMsg').show();
			$("#cid").focus();
			$('#firstname').val('');
			$('#middlename').val('');
			$('#lastName').val('');
			$('#displayDob').val('');
			$('#fathersName').val('');
			$('#dzongkhag').val('');
			$('#gewog').val('');
			$('#dzongkhagHidden').val('');
			$('#gewogHidden').val('');
			$('#village').val('');
		}
		else if(manualFlag == "N" || previousCidNumber != $('#cid').val())
		{
			$.blockUI
	        ({ 
	        	css: 
	        	{ 
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px', 
		            '-moz-border-radius': '10px', 
		            opacity: .5, 
		            color: '#fff' 
	        	} 
	        }); 
			
			$.ajax({
				async: true,
				type: 'GET',
				url: context+'/EralisCommonServlet?q=getCidDetails&cid='+value,	
				success: function(xml)
				{ 
						$(xml).find('xml-response').each(function()
						{ 
							var firstName = $(this).find('first-name').text();
							var middleName = $(this).find('middle-name').text();
							var lastName = $(this).find('last-name').text();
							var dob = $(this).find('dob').text();
							var fatherName = $(this).find('fatherName').text();
							var gender = $(this).find('gender').text(); 
							var dzongkhagId = $(this).find('dzongkhagId').text();
							var gewogId = $(this).find('gewogId').text();				
							var village = $(this).find('village').text();
							
							$('#firstname').val(firstName);
							$('#middlename').val(middleName);
							$('#lastName').val(lastName);
							$('#displayDob').val(dob);
							$('#fathersName').val(fatherName);
							$('#dzongkhag').val(dzongkhagId);
							$('#nationality').val(value);
							
							populateDependentDropDown(dzongkhagId, 'gewog', '', 'GEWOG_LIST', 'N');
							
							$('#gewog').val(gewogId);
							$('#village').val(village);
							$('#dzongkhagHidden').val(dzongkhagId);
							$('#gewogHidden').val(gewogId);
							 
							if(null != gender || "null" != gender || "" != gender)
							{
								if(gender == "M")
									$('#maleRadio').attr("checked", true);
								else
									$('#femaleRadio').attr("checked", true);
							}
						
							if("" == firstName || "" == dob)
							{
								$('#msgDiv').html("<div class='alert alert-info'>No Record Found, Fill up the form manually</div>");
								$('#msgDiv').show();
								$("#cid").focus();
								
								$('#firstname').val('');
								$('#middlename').val('');
								$('#lastName').val('');
								$('#displayDob').val('');
								$('#fathersName').val('');
								$('#dzongkhag').val('');
								$('#gewog').val('');
								$('#village').val('');
								$('#dzongkhagHidden').val('');
								$('#gewogHidden').val('');
								
								$('#manualFlag').val("Y");
								$('#name').attr('readonly', false);
								$('#firstname').attr('readonly', false);
								$('#middlename').attr('readonly', false);
								$('#lastName').attr('readonly', false);
								$('#displayDob').attr('readonly', false);
								$('#fathersName').attr('readonly', false);
								$('#dzongkhag').attr('disabled', false);
								$('#gewog').attr('disabled', false);
								$('#village').attr('readonly', false);
							}
							else
							{
								checkPrevApply();
							}
							
							setTimeout($.unblockUI, 1000); 
					});
			},
			error: function(data, textStatus, errorThrown) {
				previousCidNumber = $('#cid').val();
				
				checkPrevApply();
				
				$("#cid").focus();
				$('#firstname').val('');
				$('#middlename').val('');
				$('#lastName').val('');
				$('#displayDob').val('');
				$('#fathersName').val('');
				$('#dzongkhag').val('');
				$('#gewog').val('');
				$('#village').val('');
				$('#dzongkhagHidden').val('');
				$('#gewogHidden').val('');
				setTimeout($.unblockUI, 1000); 
				
				//if there is connection problem with the census webservice
				//then we implement manual process flow where students can fill in 
				//their information manually
				$('#msgDiv').html("<div class='alert alert-info'><strong>NOTE:</strong>Your CID information could not be fetched, thus we recommend you to fill up the form manually</div>");
				$('#msgDiv').show();
				$('#manualFlag').val("Y");
				$('#name').attr('readonly', false);
				$('#firstname').attr('readonly', false);
				$('#middlename').attr('readonly', false);
				$('#lastName').attr('readonly', false);
				$('#displayDob').attr('readonly', false);
				$('#fathersName').attr('readonly', false);
				$('#dzongkhag').attr('disabled', false);
				$('#gewog').attr('disabled', false);
				$('#village').attr('readonly', false);
			}
		});
	   }
	}
	
	function checkPrevApply(){
		var cid = $("#cid").val();
		$.ajax({
			async: true,
			type: 'GET',
			url: context+'/EralisCommonServlet?q=hasApplied&cid='+cid,	
			success: function(xml)
			{ 
				$(xml).find('xml-response').each(function(){
					var data = $(this).find('flag').text();	
					if(data == "TRUE"){
						$('#msgDiv').html("<div class='alert alert-danger'>You are already registered</div>");
						$('#msgDiv').show();
						$("#cid").focus();
						$('#name').val('');
						$('#displayDob').val('');
						$('#fathersName').val('');
						$('#dzongkhag').val('');
						$('#gewog').val('');
						$('#village').val('');
						$('#dzongkhagHidden').val('');
						$('#gewogHidden').val('');
						$('#newCustomer').val('N');
					}
				});
			}
		});
	}
	
	function displayHomePage()
	{
		$.blockUI
	    ({ 
	    	css: 
	    	{ 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	    	} 
	    });
		
		var url = context+"/login.html";
		window.location = url;
		
		setTimeout($.unblockUI, 1000); 
	}
	
	function openModal(id)
	{
		$("#"+id).modal('show');
	}
	
	function populateDependentDropDown(parentId, targetFieldId, nextTargetFieldId, fieldCons, isOther)
	{
		var childFieldId = '#'+targetFieldId;
		$(childFieldId).empty();
		$(childFieldId).append("<option value='0'>--SELECT--</option>");
		
		var url = context+'/jsondataloader.data?fieldCons='+fieldCons+'&parentId='+parentId;
		$.ajax({	
			type: "GET",
			url : url,
			cache: false,
			async: false,
			dataType : "json",
			success : function(data) 
			{
				
					for (var i = 0; i < data.length; i++) {
						$(childFieldId).append("<option value=" + data[i].headerId + ">"+ data[i].headerName + "</option>");
				    }
					
					if("Y" == isOther){
						$(childFieldId).append("<option value='ALL'>ALL</option>");
					}	
			},
			error : function(jqXHR, textStatus, errorThrown) {		
				//alert(textStatus);
			}
		});
	}
	
	function inputReset(formId)
	{
		$('#'+formId)[0].reset();
		$('.'+formId).hide();
	}
	
	function organisationCustomerId(organisationName,organisationType)
	{ 
		$.ajax({
			async: true,
			type: 'GET',
			url: context+'/EralisCommonServlet?q=organisationCustomerId&organisationName='+organisationName+"&organisationType="+organisationType,	
			success: function(xml)
			{  
				$(xml).find('xml-response').each(function()
				{
					var customerId = $(this).find('customerId').text();	 
					var Ministry_Id = $(this).find('Ministry_Id').text();	 
					var Department_Id = $(this).find('Department_Id').text();	 
					var Region_Id = $(this).find('Region_Id').text();	 
					var Dzongkhag_Id = $(this).find('Dzongkhag_Id').text();	 
					var Address = $(this).find('Address').text();	 
					var Phone_Number = $(this).find('Phone_Number').text();	
					var Remarks = $(this).find('Remarks').text();	
					var Email_Id = $(this).find('Email_Id').text();	 
					if(Ministry_Id=='null')
					{
						Ministry_Id='';
					}
					if(Department_Id=='null')
					{
						Department_Id='';
					}
					if(Region_Id=='null')
					{
						Region_Id='';
					}
					if(Dzongkhag_Id=='null')
					{
						Dzongkhag_Id='';
					}
					if(Address=='null')
					{
						Address='';
					}
					if(Phone_Number=='null')
					{
						Phone_Number='';
					}
					if(Remarks=='null')
					{
						Remarks='';
					}
					if(Email_Id=='null')
					{
						Email_Id='';
					}
					populateDependentDropDown(Ministry_Id, 'department', '', 'DEPARTMENT_LIST', 'N');
					populateDependentDropDown(Region_Id, 'orgDzongkhag', '', 'DZONGKHAG_LIST', 'N');
					if(customerId=='null')
					{
						$('#newCustomer').val('Y');
					}
					else{
						$('#newCustomer').val('N');
					}
					$('#customerID').val(customerId);
					$('#ministry').val(Ministry_Id);
					$('#department').val(Department_Id);
					$('#region').val(Region_Id);
					$('#orgDzongkhag').val(Dzongkhag_Id);
					$('#organisationAddress').val(Address);
					$('#organisationPhone_Number').val(Phone_Number);
					$('#organisationRemarks').val(Remarks);
					$('#organisationEmail_Id').val(Email_Id);
					if(customerId!='null')
					{	
						// $('#ministry').attr('disabled', true);
						 //$('#department').attr('disabled', true);
						 $('#region').attr('disabled', true);
						 $('#orgDzongkhag').attr('disabled', true);
						 $('#organisationAddress').attr('disabled', true);
						 $('#organisationPhone_Number').attr('disabled', true);
						 $('#organisationEmail_Id').attr('disabled', true);
						 $('#organisationRemarks').attr('disabled', true);
						 $('#organisationMessage').html("<div class='alert alert-danger'>You are already registered</div>");
					}
					else
					{
						// $('#ministry').attr('disabled', false);
						 //$('#department').attr('disabled', false);
						 $('#region').attr('disabled', false);
						 $('#orgDzongkhag').attr('disabled', false);
						 $('#organisationAddress').attr('disabled', false);
						 $('#organisationPhone_Number').attr('disabled', false);
						 $('#organisationEmail_Id').attr('disabled', false);
						 $('#organisationRemarks').attr('disabled', false);
						 $('#organisationMessage').html("<div></div>");
					}
				});
			}
		});
	}
	
	function validateFileExtension(component,msg_id,msg,extns)
	{
		var flag=0;
		with(component)
		{
			var ext = value.substring(value.lastIndexOf('.') + 1);
			for(var i=0;i<extns.length;i++)
			{
				if(ext==extns[i])
				{
					flag = 0;
					break;
				}
				else
				{
					flag=1;
				}
			}
			
			if(flag != 0)
			{
				document.getElementById(msg_id).style.display = "";
				document.getElementById(msg_id).innerHTML = msg;
				setTimeout('hideStatus('+msg_id+')',3000);
				fileError = "extensionError";
				return false;
			}
			else
			{
				document.getElementById(msg_id).style.display = "none";
				return true;
				fileError = "";
			}
		}
	}
	
	function validateFileSize(component,maxSize,msg_id,msg)
	{
		var size = 0;
		if(navigator.appName == "Microsoft Internet Explorer")
		{
			if(component.value)
			{
				var oas = new ActiveXObject("Scripting.FileSystemObject");
				var e = oas.getFile(component.value);
				size = e.size;
			}
		}
		else
		{
			if(component.files[0] != undefined)
			{
				size = component.files[0].size;
			}
		}
		if(size != undefined && size > maxSize)
		{
			document.getElementById(msg_id).style.display = "";
			document.getElementById(msg_id).innerHTML = msg;
			setTimeout('hideStatus('+msg_id+')',3000);
			fileError = "fileSizeError";
			return false;
		}
		else
		{
			document.getElementById(msg_id).style.display = "none";
			fileError = "";
			return true;
		}
	}
	

