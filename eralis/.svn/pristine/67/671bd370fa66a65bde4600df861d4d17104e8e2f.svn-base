package bt.gov.rsta.eralis.business.vehicle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bt.gov.rsta.eralis.business.license.LicenseBusiness;
import bt.gov.rsta.eralis.dao.license.LicenseDAO;
import bt.gov.rsta.eralis.dao.vehicle.VehicleDAO;
import bt.gov.rsta.eralis.dto.eralis_common.EralisCommonDTO;
import bt.gov.rsta.eralis.dto.license.LicenseDTO;
import bt.gov.rsta.eralis.dto.payment.PaymentDTO;
import bt.gov.rsta.eralis.dto.vehicle.FitnessMainDTO;
import bt.gov.rsta.eralis.dto.vehicle.VehicleDTO;
import bt.gov.rsta.framework.dao.CommonDAO;
import bt.gov.rsta.framework.dto.NotificationDTO;
import bt.gov.rsta.framework.util.ConnectionManager;
import bt.gov.rsta.framework.util.EmailUtil;
import bt.gov.rsta.framework.util.Log;
import bt.gov.rsta.framework.util.NotificationConstants;
import bt.gov.rsta.framework.util.SMSUtil;
import bt.gov.rsta.framework.vo.EmailModelVO;
import bt.gov.rsta.framework.vo.SMSModelVO;
import bt.gov.rsta.framework.vo.UserDetailsVO;
import bt.gov.rsta.framework.web.exception.ERALISException;
import bt.gov.rsta.framework.web.exception.ERALISSystemException;

public class VehicleBusiness 
{
	Connection conn = null;
	private static VehicleBusiness vehicleBusiness;
	
	public static VehicleBusiness getInstance()
	{
		if(vehicleBusiness == null)
			vehicleBusiness = new VehicleBusiness();
		
		return vehicleBusiness;
	}
	
	
	public String new_registration(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_registration(dto, userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_registration]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String new_renewal(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			if(null!=dto.getBfsNo())
			{
				conn = ConnectionManager.getOnlineDbConnection();
			}
			else
			{
				conn = ConnectionManager.getConnection();
			}
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_renewal(dto, userVo, conn);
				
				if(null != result)
				{
					conn.commit();
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_renewal]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	
	public String saveInspectionDtls(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = VehicleDAO.getInstance().saveInspectionDtls(dto, userVo, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[saveInspectionDtls]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	
	public String new_transfer(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_transfer(dto, userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_transfer]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String new_duplication(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			if(null!=dto.getBfsNo())
			{
				conn = ConnectionManager.getOnlineDbConnection();
			}
			else
			{
				conn = ConnectionManager.getConnection();
			}
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_duplication(dto,userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_duplication]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String fitnessReplacement(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = VehicleDAO.getInstance().fitnessReplacement(dto,userVo, conn);
				
				if(!result.equalsIgnoreCase("FAILURE"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_duplication]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String new_cancellation(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_cancellation(dto,userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_cancellation]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String withdraw_veh_cancellation(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().withdraw_veh_cancellation(dto,userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_cancellation]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String new_conversion(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_conversion(dto, userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_conversion]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String new_fitness(VehicleDTO dto, String checkedVals, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_fitness(dto, checkedVals, userVo, conn);
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_fitness]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String new_emission(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_emission(dto, userVo, conn);
				
				if(null != result)
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_emission]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String new_post(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_post(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_post]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	public String new_print(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().new_print(dto, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[new_print]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public List<FitnessMainDTO> getFitnessItemList() throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().getFitnessItemList();
	}

//approved vehicle
	
	public String registration_application_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().registration_application_approval(dto, userVo, conn );
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_REGISTRSTION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_REGISTRSTION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
			    }
		     } 
		 }
		catch (Exception e) 
		{e.printStackTrace();
			e.printStackTrace();
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[registration_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}
   
	public String renewal_application_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().renewal_application_approval(dto, userVo, conn);
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_RENEWAL_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_RENEWAL_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[renewal_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}

	public String conversion_application_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().conversion_application_approval(dto, userVo, conn );
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_CONVERSION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_CONVERSION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
			    }
		     } 
		 }
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[conversion_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}

	public String duplication_application_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().duplication_application_approval(dto, userVo, conn );
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_DUPLICATION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_DUPLICATION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
			    }
		     } 
		 }
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[duplication_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}

	public String rwc_duplication_application_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().rwc_duplication_application_approval(dto, userVo, conn );
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					/*if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_DUPLICATION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_DUPLICATION_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}*/
			    }
		     } 
		 }
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[rwc_duplication_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}
	
	public String duplication_application_verify(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{

		
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().duplication_application_verify(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[duplication_application_verify]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String transfer_application_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().transfer_application_approval(dto, userVo, conn );
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_TRANSFER_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_TRANSFER_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
			    }
				
				result = notifyDTO.getStatus();
		     } 
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[transfer_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}

	public String passenger_bus_route_permit_approval(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		NotificationDTO notifyDTO = new NotificationDTO();
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				notifyDTO = VehicleDAO.getInstance().passenger_bus_route_permit_approval(dto, userVo, conn );
				if(notifyDTO.getStatus().equalsIgnoreCase("SUCCESS"))
				{
					conn.commit();
					
					if(notifyDTO.getEmailAddress() != null)
					{
						EmailModelVO emailVO = new EmailModelVO();
						emailVO.setMailType(NotificationConstants.MAIL_TEMPLATE_RC_TRANSFER_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendMailToUser(emailVO, notifyDTO);
					}
					
					if(notifyDTO.getMobileNumber() != null)
					{
						SMSModelVO smsVO = new SMSModelVO();
						smsVO.setSmsType(NotificationConstants.MAIL_TEMPLATE_RC_TRANSFER_APPLICATION_APPROVAL_NOTIFICATION);
						VehicleBusiness.getInstance().sendSMSToUser(smsVO, notifyDTO);
					}
			    }
				
				result = notifyDTO.getStatus();
		     } 
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[transfer_application_approval]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
			result = notifyDTO.getStatus();
		}
		
		return result;
	}
   
	
	public String registration_application_reject(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().registration_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[registration_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}

	public String renewal_application_reject(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().renewal_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[renewal_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
    
	public String transfer_application_reject(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().transfer_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[transfer_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String duplication_application_reject(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().duplication_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[duplication_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
    
	public String conversion_application_reject(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		String result = null;
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			if(conn != null)
			{
				result = VehicleDAO.getInstance().conversion_application_reject(dto, userVo, conn);
				
				if(result.equalsIgnoreCase("SUCCESS"))
					conn.commit();
			}
		} 
		catch (Exception e) 
		{
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException("###Error at VehicleBusiness[conversion_application_reject]:: "+e);
		}
		finally
		{
			ConnectionManager.close(conn);
		}
		
		return result;
	}
	
	public String checkIfFitnessNEmissionValid(String vehicleId) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().checkIfFitnessNEmissionValid(vehicleId);
	}

	public EralisCommonDTO getVehicleModelDtls(String vehicleModelId) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().getVehicleModelDtls(vehicleModelId);
	}
	public EralisCommonDTO getLicensAndLearnerDtls(String licenseNo) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().getLicensAndLearnerDtls(licenseNo);
	}
	public EralisCommonDTO getVehicleDtls(String vehicleNo,String vehicleType) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().getVehicleDtls(vehicleNo,vehicleType);
	}
	
	public String getFitnessValidity(String testDate, String vehicleId, String vehicleTypeDesc) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().getFittnessValidity(testDate, vehicleId, vehicleTypeDesc);
	}
	
	public VehicleDTO get_vehicle_details(String vehicleNo, String vehicleType) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().get_vehicle_details(vehicleNo, vehicleType);
	}
	
	public VehicleDTO print_emission_report(VehicleDTO dto) throws ERALISException, ERALISSystemException
	{
		return VehicleDAO.getInstance().print_emission_report(dto);
	}
	public String update_vehicle_data(VehicleDTO dto, UserDetailsVO userVo) throws ERALISException, ERALISSystemException
	{
		Connection conn = null;
		String result = "FAILURE";
		
		try 
		{
			conn = ConnectionManager.getConnection();
			conn.setAutoCommit(false);
			
			result = VehicleDAO.getInstance().update_vehicle_data(dto, userVo, conn);
			
			if(result.equalsIgnoreCase("SUCCESS"))
				conn.commit();
		} 
		catch (Exception e) 
		{
			result = "FAILURE";
			ConnectionManager.rollbackConnection(conn);
			throw new ERALISException();
		}
		finally
		{
			ConnectionManager.close(conn, null, null, null);
		}
		
		return result;
	}

		/**
		 * Send mail to a user
		 * 
		 * @param EmailModelVO
		 * @param NotificationDTO
		 * @throws ERALISException
		 */
			public void sendMailToUser(EmailModelVO inputMailVO,NotificationDTO dto) throws ERALISException 
			{
				Log.info("Inside LicenseBusiness::sendMailToUser");
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(dto.getEmailAddress());
				inputMailVO.setRecipentList(recipientList);
				inputMailVO.setCustomerName(dto.getCustomerName());
				inputMailVO.setApplicationNo(dto.getApplicationNo());
				inputMailVO.setApprovalDate(dto.getApprovalDate());
				inputMailVO.setVehicleNo(dto.getVehicleNo());
				
				Log.info("Mail Type ============> "+inputMailVO.getMailType());
				
				Log.info("Before calling EmailUtil.sendMail");
				EmailUtil.sendMail(inputMailVO);
				Log.info("After calling EmailUtil.sendMail");
			}

		/**
		 * @param smsObject the sms object
		 * @param NotificationDTO
		 * @throws ERALISException the system exception
		 */  
		public void sendSMSToUser(SMSModelVO smsObject, NotificationDTO dto)
				throws ERALISException {
			Log.info("Inside LicenseBusiness::sendSMSToJobseeker");
		String mobileNo = dto.getMobileNumber();
		
		if (mobileNo != null && !mobileNo.trim().isEmpty()) {
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(mobileNo);
			smsObject.setRecipentList(recipientList);
			smsObject.setCustomerName(dto.getCustomerName());
			smsObject.setApplicationNo(dto.getApplicationNo());
			smsObject.setApprovalDate(dto.getApprovalDate());
			smsObject.setVehicleNo(dto.getVehicleNo());
			
			Log.info("Before calling SMSUtil.sendSMS");
			new SMSUtil().sendSMS(smsObject);
			Log.info("After calling SMSUtil.sendSMS");
			}
		
		 }


		
		}

