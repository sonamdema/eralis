package bt.gov.rsta.framework.util;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bt.gov.rsta.framework.vo.WorkflowDetailsVO;

public class WorkflowManager 
{
	 private Connection connection = null;
	
	 private PreparedStatement preparedStatement = null;
	 private ResultSet resultSet = null;
	 
	 private static final String _SUBMITTED = "SUBMITTED";
	 private static final String _RESUBMIT = "RESUBMIT";
	 private static final String _RESUBMITTED = "RESUBMITTED";
	 private static final String _APPROVED = "APPROVED";
	 private static final String _VERIFIED = "VERIFIED";
	 private static final String _DISPATCHED = "DISPATCHED";
	 
	 private static final String _INITIATED = "INITIATED";
	 private static final String _CLAIMED = "CLAIMED";
	 private static final String _COMPLETED = "COMPLETED";

	 private static final String _WORKFLOW_FLAG = "W";
	 private static final String _TASK_FLAG = "T";
	 
	 public WorkflowManager(Connection connection) {
		this.connection = connection;
	 }
	 
	 	/**
	     * Used to log a workflow when the user submits the form during the initial
	     * application for a service.
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception, SQLException
	     */
	    public void logSubmissionWorkflow(WorkflowDetailsVO vo) throws SQLException 
	    {
			Log.debug("logSubmissionWorkflow");
			/**
			 * Check if this connection has autocommit set; If the autocommit is enabled, set it to false;
			 */
			if(connection.getAutoCommit()){
			    connection.setAutoCommit(false);
			}
			/**
			 * Insert a new record in the T_WORKFLOW_DTLS table with the status_id
			 * of SUBMITTED. STATUS_ID is fetched from T_STATUS_LOOKUP
			 */
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _SUBMITTED);
			Log.debug("logSubmissionWorkflow--statusId"+statusId);
			
			int roleId = 0; /// roleId IS SET TO 0 AT THE MOMENT BECAUSE SESSION VALUE IS NOT IMPLEMENTED TILL NOW..
			if(vo.getRoleId()!=null)
			{
				roleId = Integer.parseInt(vo.getRoleId());//should come from session
			}
			Log.debug("logSubmissionWorkflow--roleId"+roleId);
			
			int serviceId = 0; /// roleId IS SET TO 0 AT THE MOMENT BECAUSE SESSION VALUE IS NOT IMPLEMENTED TILL NOW..
			if(vo.getServiceId()!=null)
			{
				serviceId = Integer.parseInt(vo.getServiceId());//should come as an identified of the particular service
			}
			Log.debug("logSubmissionWorkflow--serviceId"+serviceId);
			
			int actorId = 0; /// roleId IS SET TO 0 AT THE MOMENT BECAUSE SESSION VALUE IS NOT IMPLEMENTED TILL NOW..
			if(vo.getActorId()!=null)
			{
				//actorId = Integer.parseInt(vo.getActorId()); //should come as an identified of the actor id
			}
			Log.debug("logSubmissionWorkflow--actorId"+actorId);	
			Log.debug("logSubmissionWorkflow--actorName"+vo.getActorName());
		
			Log.debug("workflowUpdateCount = INSERT_T_WORKFLOW_DTLS========================"+INSERT_T_WORKFLOW_DTLS);
			Log.debug(" === vo.getApplicationNo(),statusId,serviceId,actorId,vo.getActorName(),roleId,vo.getRoleName()======================="+vo.getApplicationNo()+"  ,  "+statusId+"  ,  "+serviceId+"  ,  "+actorId+"  ,  "+vo.getActorName()+"  ,  "+roleId+"  ,  "+vo.getRoleName());
			
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			preparedStatement.setInt(2, statusId);// STATUS_ID
			preparedStatement.setInt(3, serviceId);// SERVICE_ID
			preparedStatement.setString(4, vo.getActorId());// ACTOR_ID
			preparedStatement.setString(5, vo.getActorName());// ACTOR_NAME
			preparedStatement.setString(6, ""+roleId);// ROLE_ID
			preparedStatement.setString(7, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(8, vo.getJurisId());//JURISDICTION_ID
			preparedStatement.setString(9, vo.getJurisTypeId());//JURISDICTION_TYPE_ID
			
			int workflowUpdateCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
		
			// get the current workflow instance id
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			Log.debug("instanceId = " + instanceId);
			
			//Get the assigned priv Id
			String assignedPrivId = "";
			assignedPrivId = getAssignedPriviledgeID("VERIFIER", ""+serviceId);
			if(assignedPrivId == null || assignedPrivId.equals(""))
			{
			    assignedPrivId =  getAssignedPriviledgeID("APPROVER", ""+serviceId);
			}
			
			Log.debug("assignedPrivId = "+assignedPrivId);
			
			/**
			 * Insert a new record in T_TASK_DTLS with status id of INITIATED
			 */
			int seqDetailsId = this.getSeqDetailsId("SUBMISSION", serviceId);
			int taskStateId = this.getStatusId(_TASK_FLAG, _INITIATED);
		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS);
			preparedStatement.setInt(1, instanceId);// INSTANCE_ID ; THE ID OF THE RECORD IN T_WORKFLOW_DTLS
			preparedStatement.setInt(2, seqDetailsId);// SEQ_DETAILS_ID ; FETCHED FROM T_TASK_SEQ_DTLS_LOOKUP
			preparedStatement.setString(3, vo.getApplicationNo());// APPLICATION_NUMBER ; SENT IN THE PARAMETER VO
			preparedStatement.setString(4, assignedPrivId);// ASSIGNED_GROUP_ID ;
			preparedStatement.setString(5, vo.getAssignedUserId());// ASSIGNED_USER_ID
			preparedStatement.setInt(6, taskStateId);// TASK_STATE_ID ; FETCHED FROM T_STATUS_LOOKUP
			preparedStatement.setString(7, vo.getTaskRemarks());// TASK_REMARK
			preparedStatement.setDate(8, new Date(System.currentTimeMillis()));// ACTION_DATE
			int taskUpdateCount = preparedStatement.executeUpdate();
			Log.debug("taskUpdateCount = " + taskUpdateCount);
			
			/**
			 * We should not close the connection here as the connection object is managed from the calling DAO
			 */
	    }
	    
	    /**
	     * Used to log a workflow when the user rejects the form and 
	     * is sent back for resubmission of application for a service.
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception
	     */
	    public void logRejectionWorkflow(WorkflowDetailsVO vo) throws Exception 
	    {
			/**
			 * Check if this connection has autocommit set; If the autocommit is enabled, set it to false;
			 */
			//if(connection.getAutoCommit()){
			//    connection.setAutoCommit(false);
			//}
			
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _RESUBMIT);
			int serviceId = Integer.parseInt(vo.getServiceId());
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			int seqDetailsId = this.getSeqDetailsId("REJECTION", serviceId);
			
			//Get the assigned priv Id
			String assignedPrivId = getAssignedPriviledgeID("SUBMITTER", ""+serviceId);
			Log.debug("assignd priv ID = "+assignedPrivId);

			//First get the task_id
			preparedStatement = connection.prepareStatement(GET_T_TASK_DTLS_ID);
			preparedStatement.setInt(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			int taskDtlsId = resultSet.getInt("TASK_ID");
			ConnectionManager.close(null, null, resultSet, preparedStatement);
			
			/**
			 * Insert the previous T_TASK_DTLS in the T_TASK_DTLS_AUDIT table
			 */		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS_AUDIT);
			preparedStatement.setInt(1,taskDtlsId);
			preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, resultSet, preparedStatement);	
			
			/**
			 * Insert the previous T_WORKFLOW_DTLS in the T_WORKFLOW_DTLS_AUDIT table
			 */
				
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS_AUDIT);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateAuditCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateAuditCount = " + workflowUpdateAuditCount);
			
			/**
			 * UPDATE value in T_TASK_DTLS with status id of INITIATED
			 */
			int initiatedTaskId = this.getStatusId(_TASK_FLAG, _INITIATED);
			preparedStatement = connection.prepareStatement(UPDATE_T_TASK_DTLS_PRIV);
			preparedStatement.setInt(1, initiatedTaskId);
			preparedStatement.setInt(2, seqDetailsId);
			preparedStatement.setString(3, assignedPrivId);
			preparedStatement.setDate(4,new Date(System.currentTimeMillis()));
			preparedStatement.setInt(5,taskDtlsId);
			int taskUpdateCount = preparedStatement.executeUpdate();
			Log.debug("taskUpdateCount = " + taskUpdateCount);
			
			//update T_WORKFLOW_DETAILS

			int workflowStatusId = this.getStatusId(_WORKFLOW_FLAG, _RESUBMIT);//doubt _VERIFIED
			preparedStatement = connection.prepareStatement(UPDATE_T_WORKFLOW_DTLS);
			
			preparedStatement.setInt(1, workflowStatusId);// STATUS_ID
			preparedStatement.setString(2, vo.getActorId());// ACTOR_ID
			preparedStatement.setString(3, vo.getActorName());// ACTOR_NAME
			preparedStatement.setInt(4, Integer.parseInt(vo.getRoleId()));// ROLE_ID
			preparedStatement.setString(5, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(6, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
		}
	    
	    /**
	     * Used to log a workflow when the user resubmits the form and 
	     * is sent back for approval of application for a service.
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception
	     */
	    public void logResubmissionWorkflow(WorkflowDetailsVO vo) throws Exception 
	    {
			/**
			 * Check if this connection has autocommit set; If the autocommit is enabled, set it to false;
			 */
			//if(connection.getAutoCommit()){
			//    connection.setAutoCommit(false);
			//}
			
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _RESUBMITTED);
			int serviceId = Integer.parseInt(vo.getServiceId());
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			int seqDetailsId = this.getSeqDetailsId("RESUBMISSION", serviceId);
			
			//Get the assigned priv Id
			String assignedPrivId = "";
			assignedPrivId = getAssignedPriviledgeID("VERIFIER", ""+serviceId);
			if(assignedPrivId == null || assignedPrivId.equals(""))
			{
			    assignedPrivId =  getAssignedPriviledgeID("APPROVER", ""+serviceId);
			}
			
			Log.debug("assignedPrivId = "+assignedPrivId);
			//First get the task_id
			preparedStatement = connection.prepareStatement(GET_T_TASK_DTLS_ID);
			preparedStatement.setInt(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			int taskDtlsId = resultSet.getInt("TASK_ID");
			ConnectionManager.close(null, null, resultSet, preparedStatement);
			
			/**
			 * Insert the previous T_TASK_DTLS in the T_TASK_DTLS_AUDIT table
			 */		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS_AUDIT);
			preparedStatement.setInt(1,taskDtlsId);
			preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, resultSet, preparedStatement);	
			
			/**
			 * Insert the previous T_WORKFLOW_DTLS in the T_WORKFLOW_DTLS_AUDIT table
			 */
				
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS_AUDIT);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateAuditCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateAuditCount = " + workflowUpdateAuditCount);
			
			/**
			 * UPDATE value in T_TASK_DTLS with status id of INITIATED
			 */
			int initiatedTaskId = this.getStatusId(_TASK_FLAG, _INITIATED);
			preparedStatement = connection.prepareStatement(UPDATE_T_TASK_DTLS_PRIV);
			preparedStatement.setInt(1, initiatedTaskId);
			preparedStatement.setInt(2, seqDetailsId);
			preparedStatement.setString(3, assignedPrivId);
			preparedStatement.setDate(4,new Date(System.currentTimeMillis()));
			preparedStatement.setInt(5,taskDtlsId);
			int taskUpdateCount = preparedStatement.executeUpdate();
			Log.debug("taskUpdateCount = " + taskUpdateCount);
			
			//update T_WORKFLOW_DETAILS

			int workflowStatusId = this.getStatusId(_WORKFLOW_FLAG, _RESUBMITTED);//doubt _VERIFIED
			preparedStatement = connection.prepareStatement(UPDATE_T_WORKFLOW_DTLS);
			
			preparedStatement.setInt(1, workflowStatusId);// STATUS_ID
			preparedStatement.setString(2, vo.getActorId());// ACTOR_ID
			preparedStatement.setString(3, vo.getActorName());// ACTOR_NAME
			preparedStatement.setInt(4, Integer.parseInt(vo.getRoleId()));// ROLE_ID
			preparedStatement.setString(5, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(6, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
		}
	    
	    /**
	     * Used to log a workflow when the user approves the form and
	     * the workflow is completed and is ready for printing
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception
	     */
	    public void logApprovalCompletedWorkflow(WorkflowDetailsVO vo) throws SQLException 
	    {
	    	/**
			 * Check if this connection has autocommit set; If the autocommit is enabled, set it to false;
			 */
			//if(connection.getAutoCommit()){
			//    connection.setAutoCommit(false);
			//}
			
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _APPROVED);
			int serviceId = Integer.parseInt(vo.getServiceId());
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			int seqDetailsId = this.getSeqDetailsId("PRINT", serviceId);
			
			//Get the assigned priv Id
			String assignedPrivId = getAssignedPriviledgeID("SUBMITTER", ""+serviceId);
			Log.debug("assignd priv ID = "+assignedPrivId);

			//First get the task_id
			preparedStatement = connection.prepareStatement(GET_T_TASK_DTLS_ID);
			preparedStatement.setInt(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			int taskDtlsId = resultSet.getInt("TASK_ID");
			ConnectionManager.close(null, null, resultSet, preparedStatement);
			
			/**
			 * Insert the previous T_TASK_DTLS in the T_TASK_DTLS_AUDIT table
			 */		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS_AUDIT);
			preparedStatement.setInt(1,taskDtlsId);
			preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, resultSet, preparedStatement);	
			
			/**
			 * Insert the previous T_WORKFLOW_DTLS in the T_WORKFLOW_DTLS_AUDIT table
			 */
				
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS_AUDIT);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateAuditCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateAuditCount = " + workflowUpdateAuditCount);
			
			/**
			 * UPDATE value in T_TASK_DTLS with status id of INITIATED
			 */
			int initiatedTaskId = this.getStatusId(_TASK_FLAG, _INITIATED);
			preparedStatement = connection.prepareStatement(UPDATE_T_TASK_DTLS_PRIV);
			preparedStatement.setInt(1, initiatedTaskId);
			preparedStatement.setInt(2, seqDetailsId);
			preparedStatement.setString(3, assignedPrivId);
			preparedStatement.setDate(4,new Date(System.currentTimeMillis()));
			preparedStatement.setInt(5,taskDtlsId);
			int taskUpdateCount = preparedStatement.executeUpdate();
			Log.debug("taskUpdateCount = " + taskUpdateCount);
			
			//update T_WORKFLOW_DETAILS
			int workflowStatusId = this.getStatusId(_WORKFLOW_FLAG, _APPROVED);//doubt _APPROVED
			preparedStatement = connection.prepareStatement(UPDATE_T_WORKFLOW_DTLS);
			
			preparedStatement.setInt(1, workflowStatusId);// STATUS_ID
			preparedStatement.setString(2, vo.getActorId());// ACTOR_ID
			preparedStatement.setString(3, vo.getActorName());// ACTOR_NAME
			preparedStatement.setInt(4, Integer.parseInt(vo.getRoleId()));// ROLE_ID
			preparedStatement.setString(5, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(6, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
			
			preparedStatement = connection.prepareStatement(GET_SUBMITTER_ID);
			preparedStatement.setString(1, vo.getApplicationNo());
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			String actorId = resultSet.getString("Actor_Id");
			
			int claimedTaskId = this.getStatusId(_TASK_FLAG, _CLAIMED);
			preparedStatement = connection.prepareStatement(ASSIGN_TASK_DIRECTLY_TO_SUBMITTER);
			preparedStatement.setString(1, actorId);
			preparedStatement.setInt(2, claimedTaskId);
			preparedStatement.setString(3, vo.getApplicationNo());
			preparedStatement.executeUpdate();
	    }
	    
	    /**
	     * Used to log a workflow when the user dispatches the form and
	     * the workflow is completed
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception
	     */
	    public void logCompletedWorkflow(WorkflowDetailsVO vo) throws SQLException 
	    {
			Log.debug("logCompletedWorkflow");
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _DISPATCHED);
			int roleId = Integer.parseInt(vo.getRoleId());
			int serviceId = Integer.parseInt(vo.getServiceId());
		
			int seqDetailsId = this.getSeqDetailsId("APPROVAL", serviceId);Log.debug("seqDetailsId="+seqDetailsId);
			int taskStateId = this.getStatusId(_TASK_FLAG, _COMPLETED);Log.debug("taskStateId="+taskStateId);
		
			// Get the workflow id
			
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			Log.debug("instanceId="+instanceId);
			/**
			 * Insert the previous T_TASK_DTLS in the T_TASK_DTLS_AUDIT table
			 */
			//First get the task_id
			preparedStatement = connection.prepareStatement(GET_T_TASK_DTLS_ID);
			preparedStatement.setInt(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			int taskDtlsId = resultSet.getInt("TASK_ID");
			ConnectionManager.close(null, null, resultSet, preparedStatement);
		
			//Now insert this row in the audit table		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS_AUDIT);
			preparedStatement.setInt(1,taskDtlsId);
			preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, resultSet, preparedStatement);
		
			/**
			 * UPDATE value in T_TASK_DTLS with status id of COMPLETED
			 */
			preparedStatement = connection.prepareStatement(UPDATE_T_TASK_DTLS);
			preparedStatement.setInt(1, this.getStatusId(_TASK_FLAG, _COMPLETED));
			preparedStatement.setInt(2, seqDetailsId);
			preparedStatement.setDate(3,new Date(System.currentTimeMillis()));
			preparedStatement.setInt(4,taskDtlsId);
			int taskUpdateCount = preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, null, preparedStatement);
			
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS_AUDIT);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateAuditCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateAuditCount = " + workflowUpdateAuditCount);
						
			preparedStatement = connection.prepareStatement(UPDATE_T_WORKFLOW_DTLS);
			preparedStatement.setInt(1, this.getStatusId(_WORKFLOW_FLAG, _DISPATCHED));// STATUS_ID
			// TODO: set actor id properly
			preparedStatement.setInt(2, 1);// ACTOR_ID
			preparedStatement.setString(3, vo.getActorName());// ACTOR_NAME
			preparedStatement.setInt(4, Integer.parseInt(vo.getRoleId()));// ROLE_ID
			preparedStatement.setString(5, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(6, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateCount = preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, null, preparedStatement);
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
	    }
	    
	    /**
	     * Used to log a workflow when the user dispatches the form and
	     * the workflow is completed
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception
	     */
	    public void logDrivingLicenseApprovalCompletedWorkflow(WorkflowDetailsVO vo) throws SQLException 
	    {
			Log.debug("logCompletedWorkflow");
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _APPROVED);
			int roleId = Integer.parseInt(vo.getRoleId());
			int serviceId = Integer.parseInt(vo.getServiceId());
		
			int seqDetailsId = this.getSeqDetailsId("PRINT", serviceId);Log.debug("seqDetailsId="+seqDetailsId);
			int taskStateId = this.getStatusId(_TASK_FLAG, _COMPLETED);Log.debug("taskStateId="+taskStateId);
		
			// Get the workflow id
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			Log.debug("instanceId="+instanceId);
			/**
			 * Insert the previous T_TASK_DTLS in the T_TASK_DTLS_AUDIT table
			 */
			//First get the task_id
			preparedStatement = connection.prepareStatement(GET_T_TASK_DTLS_ID);
			preparedStatement.setInt(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			int taskDtlsId = resultSet.getInt("TASK_ID");
			ConnectionManager.close(null, null, resultSet, preparedStatement);
		
			//Now insert this row in the audit table		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS_AUDIT);
			preparedStatement.setInt(1,taskDtlsId);
			preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, resultSet, preparedStatement);
		
			/**
			 * UPDATE value in T_TASK_DTLS with status id of COMPLETED
			 */
			preparedStatement = connection.prepareStatement(UPDATE_T_TASK_DTLS);
			preparedStatement.setInt(1, this.getStatusId(_TASK_FLAG, _COMPLETED));
			preparedStatement.setInt(2, seqDetailsId);
			preparedStatement.setDate(3,new Date(System.currentTimeMillis()));
			preparedStatement.setInt(4,taskDtlsId);
			int taskUpdateCount = preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, null, preparedStatement);
			
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS_AUDIT);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateAuditCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateAuditCount = " + workflowUpdateAuditCount);
						
			preparedStatement = connection.prepareStatement(UPDATE_T_WORKFLOW_DTLS);
			preparedStatement.setInt(1, this.getStatusId(_WORKFLOW_FLAG, _APPROVED));// STATUS_ID
			// TODO: set actor id properly
			preparedStatement.setInt(2, 1);// ACTOR_ID
			preparedStatement.setString(3, vo.getActorName());// ACTOR_NAME
			preparedStatement.setInt(4, Integer.parseInt(vo.getRoleId()));// ROLE_ID
			preparedStatement.setString(5, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(6, vo.getApplicationNo());// APPLsICATION_NUMBER
			int workflowUpdateCount = preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, null, preparedStatement);
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
	    }
	    
	    /**
	     * Used to log a workflow when the user verifies the application for a service.
	     * 
	     * @param WorkflowDetailsVO vo
	     * @throws Exception, SQLException
	     */
	    public void logVerificationWorkFlow(WorkflowDetailsVO vo) throws SQLException 
	    {
	    	/**
			 * Check if this connection has autocommit set; If the autocommit is enabled, set it to false;
			 */
			//if(connection.getAutoCommit()){
			//    connection.setAutoCommit(false);
			//}
			
			int statusId = this.getStatusId(_WORKFLOW_FLAG, _VERIFIED);
			int serviceId = Integer.parseInt(vo.getServiceId());
			int instanceId = this.getWorkflowInstanceId(vo.getApplicationNo(), statusId, serviceId, new Date(System.currentTimeMillis()));
			int seqDetailsId = this.getSeqDetailsId("VERIFICATION", serviceId);
			
			//Get the assigned priv Id
			String assignedPrivId = getAssignedPriviledgeID("APPROVER", ""+serviceId);
			Log.debug("assignd priv ID = "+assignedPrivId);

			//First get the task_id
			preparedStatement = connection.prepareStatement(GET_T_TASK_DTLS_ID);
			preparedStatement.setInt(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			resultSet.first();
			int taskDtlsId = resultSet.getInt("TASK_ID");
			ConnectionManager.close(null, null, resultSet, preparedStatement);
			
			/**
			 * Insert the previous T_TASK_DTLS in the T_TASK_DTLS_AUDIT table
			 */		
			preparedStatement = connection.prepareStatement(INSERT_T_TASK_DTLS_AUDIT);
			preparedStatement.setInt(1,taskDtlsId);
			preparedStatement.executeUpdate();
			ConnectionManager.close(null, null, resultSet, preparedStatement);	
			
			/**
			 * Insert the previous T_WORKFLOW_DTLS in the T_WORKFLOW_DTLS_AUDIT table
			 */
				
			preparedStatement = connection.prepareStatement(INSERT_T_WORKFLOW_DTLS_AUDIT);
			preparedStatement.setString(1, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateAuditCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateAuditCount = " + workflowUpdateAuditCount);
			
			/**
			 * UPDATE value in T_TASK_DTLS with status id of INITIATED
			 */
			int initiatedTaskId = this.getStatusId(_TASK_FLAG, _INITIATED);
			preparedStatement = connection.prepareStatement(UPDATE_T_TASK_DTLS_PRIV);
			preparedStatement.setInt(1, initiatedTaskId);
			preparedStatement.setInt(2, seqDetailsId);
			preparedStatement.setString(3, assignedPrivId);
			preparedStatement.setDate(4,new Date(System.currentTimeMillis()));
			preparedStatement.setInt(5,taskDtlsId);
			int taskUpdateCount = preparedStatement.executeUpdate();
			Log.debug("taskUpdateCount = " + taskUpdateCount);
			
			//update T_WORKFLOW_DETAILS

			int workflowStatusId = this.getStatusId(_WORKFLOW_FLAG, _VERIFIED);//doubt _VERIFIED
			preparedStatement = connection.prepareStatement(UPDATE_T_WORKFLOW_DTLS);
			
			preparedStatement.setInt(1, workflowStatusId);// STATUS_ID
			preparedStatement.setString(2, vo.getActorId());// ACTOR_ID
			preparedStatement.setString(3, vo.getActorName());// ACTOR_NAME
			preparedStatement.setInt(4, Integer.parseInt(vo.getRoleId()));// ROLE_ID
			preparedStatement.setString(5, vo.getRoleName());// ROLE_NAME
			preparedStatement.setString(6, vo.getApplicationNo());// APPLICATION_NUMBER
			int workflowUpdateCount = preparedStatement.executeUpdate();
			Log.debug("workflowUpdateCount = " + workflowUpdateCount);
	    }
	    
	    /**
	     * Utility Mehods
	     */
	    private int getStatusId(String statusType, String shortDesc) throws SQLException 
	    {
				PreparedStatement preparedStatement = connection.prepareStatement(GET_T_STATUS_MASTER);
				preparedStatement.setString(1, statusType);
				preparedStatement.setString(2, shortDesc);
				resultSet = preparedStatement.executeQuery();
				resultSet.first();
				return resultSet.getInt("Status_Id");
	    }

	  /*  private int getSeqDetailsId(int seqNo, int serviceId) throws SQLException 
	    {
				PreparedStatement preparedStatement = connection.prepareStatement(GET_T_TASK_SEQ_DTLS_LOOKUP);
				preparedStatement.setInt(1, seqNo);// TASK_SEQUESCE_NO
				preparedStatement.setInt(2, serviceId);// SERVICE_ID
				resultSet = preparedStatement.executeQuery();
				resultSet.first();
				Log.debug("&&&&&&&&&&&&&&Seq_Details_Id");
			    return resultSet.getInt("Seq_Details_Id");
	    }*/
	    
	    private int getSeqDetailsId(String taskName, int serviceId) throws SQLException 
	    {
				PreparedStatement preparedStatement = connection.prepareStatement(GET_T_TASK_SEQ_DTLS_LOOKUP);
				preparedStatement.setInt(1, serviceId);// SERVICE_ID
				preparedStatement.setString(2, taskName); // TASK_NAME
				resultSet = preparedStatement.executeQuery();
				resultSet.first();
				Log.debug("&&&&&&&&&&&&&&Seq_Details_Id");
			    return resultSet.getInt("Seq_Details_Id");
	    }
	    
	    private int getWorkflowInstanceId(String applicationNo, int statusId, int serviceId, Date date) throws SQLException 
	    {
			    Log.debug("=====getWorkflowInstanceId()==============applicationNo=============================="+applicationNo);
				PreparedStatement preparedStatement = connection.prepareStatement(GET_WORKFLOW_INSTANCE_ID);
				preparedStatement.setString(1, applicationNo);	// APPLICATION_NUMBER
				resultSet = preparedStatement.executeQuery();
				resultSet.first();
				int instanceId = resultSet.getInt("INSTANCE_ID");
				
				ConnectionManager.close(null, null, resultSet, preparedStatement);
				return instanceId;
	    }
	    
	    public String getAssignedPriviledgeID(String privCode, String serviceId) throws SQLException 
		{
			PreparedStatement preparedStatement = connection.prepareStatement(GET_PRIV_ID_FROM_CODE);
			preparedStatement.setString(1, privCode);
			preparedStatement.setString(2, serviceId);
			ResultSet resultSet = preparedStatement.executeQuery();

			String privId = null;
			if(resultSet.first())
			{
				privId = resultSet.getString("PRIV_ID");
			}
			
			ConnectionManager.close(null, null, resultSet, preparedStatement);
			return privId;
		}
	    
	    /******************************************************** QUERY : START ********************************************************************/
	    
	    private static final String INSERT_T_WORKFLOW_DTLS = "INSERT INTO t_workflow_dtls ( APPLICATION_NUMBER, STATUS_ID, SERVICE_ID, ACTION_DATE,  ACTOR_ID, ACTOR_NAME, ROLE_ID, ROLE_NAME, JURIS_ID, JURIS_TYPE_ID) VALUES (?,?,?,SYSDATE(),?,?,?,?,?,?)";
	    private static final String INSERT_T_TASK_DTLS = "INSERT INTO `t_task_dtls` (INSTANCE_ID, SEQ_DETAILS_ID, APPLICATION_NUMBER, Assigned_Priv_Id, ASSIGNED_USER_ID, TASK_STATE_ID, TASK_REMARK, ACTION_DATE) VALUES(?,?,?,?, ?,?,?,?)";
	    private static final String GET_T_STATUS_MASTER = "SELECT * FROM t_status_master WHERE STATUS_TYPE = ? AND STATUS_TYPE_SHORT_DESC = ?";
	  //  private static final String GET_T_TASK_SEQ_DTLS_LOOKUP = "SELECT * FROM `t_task_seq_dtls_lookup` WHERE TASK_SEQUESCE_NO = ? AND SERVICE_ID= ?";
	    private static final String GET_T_TASK_SEQ_DTLS_LOOKUP = "SELECT `Seq_Details_Id` FROM `t_task_seq_dtls_lookup` WHERE Service_Id=? AND Task_Name=?";
	    private static final String GET_WORKFLOW_INSTANCE_ID = "SELECT INSTANCE_ID FROM t_workflow_dtls WHERE APPLICATION_NUMBER = ?";
	    private static final String GET_T_TASK_DTLS_ID = "SELECT  TASK_ID FROM  t_task_dtls WHERE INSTANCE_ID = ?";
	    private static final String INSERT_T_TASK_DTLS_AUDIT = "INSERT INTO `t_task_dtls_audit` (TASK_ID, INSTANCE_ID, SEQ_DETAILS_ID, APPLICATION_NUMBER, ASSIGNED_PRIV_ID, ASSIGNED_USER_ID, TASK_STATE_ID, ACTION_DATE, TASK_REMARK) "
															  + "SELECT TASK_ID, INSTANCE_ID, SEQ_DETAILS_ID, APPLICATION_NUMBER, ASSIGNED_PRIV_ID, ASSIGNED_USER_ID, TASK_STATE_ID, ACTION_DATE, TASK_REMARK FROM `t_task_dtls` WHERE TASK_ID = ?";
	    private static final String UPDATE_T_TASK_DTLS = "UPDATE t_task_dtls SET TASK_STATE_ID = ? , SEQ_DETAILS_ID = ? , ACTION_DATE = ? WHERE TASK_ID = ?";
	    private static final String INSERT_T_WORKFLOW_DTLS_AUDIT = " INSERT INTO t_workflow_dtls_audit(Instance_Id,Application_Number, Status_Id, Service_Id, "
																	+ "Action_Date, Actor_Id,	Actor_Name, Role_Id, Role_Name,Juris_Id ) "
																	+ "SELECT Instance_Id, Application_Number,Status_Id, Service_Id, Action_Date, Actor_Id, "
																	+ "Actor_Name, Role_Id, Role_Name, Juris_Id "
																	+ "FROM t_workflow_dtls  WHERE Application_Number = ?";
	    private static final String UPDATE_T_WORKFLOW_DTLS = "UPDATE t_workflow_dtls SET Status_Id=? ,Action_Date=sysdate(),Actor_Id=?,Actor_Name =? ,Role_Id=?, Role_Name=?  WHERE Application_Number=? ";
	    private static final String UPDATE_T_TASK_DTLS_PRIV = "UPDATE t_task_dtls SET TASK_STATE_ID = ? , SEQ_DETAILS_ID = ? ,ASSIGNED_PRIV_ID = ? , ACTION_DATE = ? WHERE TASK_ID = ?";
		
	    private static final String GET_PRIV_ID_FROM_CODE= "SELECT PRIV_ID FROM t_privilege_master WHERE  PRIV_SHORT_DESC=? AND SERVICE_ID=?";
	    
	    private static final String GET_SUBMITTER_ID = "SELECT a.`Actor_Id` FROM t_workflow_dtls_audit a WHERE a.`Application_Number`= ? AND a.`Status_Id` IN (SELECT `Status_Id` FROM `t_status_master` WHERE `Status_Type_Short_Desc`='SUBMITTED');";
	    
	    private static final String ASSIGN_TASK_DIRECTLY_TO_SUBMITTER = "UPDATE t_task_dtls a SET a.`Assigned_User_Id`=?,a.`Task_State_Id`=? WHERE a.`Application_Number`=?";
	    /******************************************************* QUERY : END ***********************************************************************/
}
